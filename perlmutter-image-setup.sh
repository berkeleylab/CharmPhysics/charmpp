#!/bin/bash

export ANALYSIS_PATH=`pwd`
cd ~/
source /global/cfs/cdirs/atlas/scripts/setupATLAS.sh
export ALRB_CONT_SWTYPE=shifter
setupATLAS -c atlas-grid-almalinux9 --postsetup " export TERM='screen';source .bashrc; cd ${ANALYSIS_PATH}; source perlmutter-setup.sh"
