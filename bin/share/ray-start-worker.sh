#!/bin/bash

HEAD_IP=${1:-$RAY_HEAD_IP}
REDIS_PASSWORD=${2:-$RAY_REDIS_PASSWORD}
REDIS_PORT=${3:-$RAY_REDIS_PORT}
NWORKERS=${4:-$RAY_NWORKERS}
TMP_DIR=${5:-$RAY_TMP_DIR}

function usage() {
    echo "Usage: $0 HEAD_IP REDIS_PASSWORD REDIS_PORT [args]..."
    echo
    echo '  HEAD_IP, REDIS_PORT NWORKERS REDIS_PASSWORD can be specified using $RAY_HEAD_IP $RAY_REDIS_PORT $RAY_NWORKERS $RAY_REDIS_PASSWORD'
    echo '  [args] contains extra arguments provided to ray start'
}

if [[ $# -ge 4 ]]; then
    shift 4
else
    shift $#
fi

if [[ -z "$HEAD_IP" ]] || [[ -z "$REDIS_PORT" ]] || [[ -z "$TMP_DIR" ]]; then
    echo 'Please specify ray tmp dir, ray head ip, redis password and redis port'
    usage
    exit 1
fi

TMP_DIR="$TMP_DIR"/"$SLURMD_NODENAME"
echo ray tmp dir: "$TMP_DIR"
mkdir -p "$TMP_DIR"

ray start --address="$HEAD_IP":"$REDIS_PORT" --redis-password="$REDIS_PASSWORD" --temp-dir "$TMP_DIR" --block --num-cpus `expr $NWORKERS - 2` "$@"
