#!/bin/bash

# Number of ray workers per node (e.g. 32 for Haswell or 124 for KNL)
if [ -z "$1" ]
then
    echo "No argument supplied; setting NUM_WORKERS to 32."
    NUM_WORKERS=32
else
    NUM_WORKERS=$1
fi

# To prevent numPy from spawning too many threads in each raylet
export OPENBLAS_NUM_THREADS=1
# export OMP_NUM_THREADS=1 ## ray gives a warning for this

# Redis configuration
export RAY_REDIS_PASSWORD=$(cat /dev/urandom | tr -dc 'a-zA-Z0-9' | fold -w 32 | head -n 1)
export RAY_HEAD_IP=`hostname -i`
export RAY_REDIS_PORT=6379

# Number of worker processes per node
export RAY_NWORKERS=$NUM_WORKERS

# Ray temp dir
export RAY_TMP_DIR=/tmp/raytmp/$SLURM_JOB_ID
if [[ ! -d $RAY_TMP_DIR ]]; then
  mkdir -p "$RAY_TMP_DIR"
fi

# start ray head node
srun -N1 -n1 -w $SLURMD_NODENAME --cpus-per-task $RAY_NWORKERS ray-start-head &

# wait for head node to start ray
ray-sync

# start ray on all other nodes
srun -x $SLURMD_NODENAME -N`expr $SLURM_JOB_NUM_NODES - 1` -n`expr $SLURM_JOB_NUM_NODES - 1` --cpus-per-task $RAY_NWORKERS \
    ray-start-worker &

# wait for worker nodes to connect to the main node
ray-sync --wait-workers
