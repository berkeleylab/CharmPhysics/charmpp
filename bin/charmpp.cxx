#include <iostream>
#include <memory>
#include <map>
#include <string>
#include <cstdlib>  // for std::strtol, std::atoll
#include <cerrno>
#include <climits>

#include "TROOT.h"
#include "TString.h"
#include "WDTruthLoop.h"
#include "WFakeRateMeasurement.h"
#include "WMETTemplate.h"
#include "WPTTemplate.h"
#include "WplusDLoop.h"
#include "ZCharmLoop.h"
#include "ZDTruthLoop.h"
#include "ZplusDLoop.h"
#include "ttbarDilep.h"
#include "ttbarDilepTruthLoop.h"

using CharmEventLoopPtr = std::unique_ptr<Charm::EventLoopBase>;

/**
 * Check if a string looks like a valid (long long) integer.
 * We will treat it as numeric if strtol/strtoll can parse the entire string
 * with no leftover characters.
 */
bool isInteger(const std::string &s) {
    if(s.empty()) return false;

    char *end = nullptr;
    errno = 0;
    long long val = std::strtoll(s.c_str(), &end, 10);

    // If 'end' is not pointing to the end of the string, it's not purely numeric.
    // Also check for overflow/underflow via errno.
    if (*end != '\0') return false;
    if ((val == LLONG_MIN || val == LLONG_MAX) && errno == ERANGE) return false;

    // Otherwise, it parsed successfully
    return true;
}

/**
 * Creates the appropriate EventLoop object depending on analysis_type.
 */
CharmEventLoopPtr createEventLoop(const TString &analysis_type,
                                  const TString &file_name,
                                  const TString &out_path) {
  if (analysis_type == "ttbarDilepTruthLoop")
    return std::make_unique<Charm::ttbarDilepTruthLoop>(file_name, out_path);
  if (analysis_type == "ttbarDilep")
    return std::make_unique<Charm::ttbarDilep>(file_name, out_path);
  if (analysis_type == "ZCharm")
    return std::make_unique<Charm::ZCharmLoop>(file_name, out_path);
  if (analysis_type == "ZDCharm")
    return std::make_unique<Charm::ZplusDLoop>(file_name, out_path);
  if (analysis_type.Contains("WDCharm"))
    return std::make_unique<Charm::WplusDLoop>(file_name, out_path);
  if (analysis_type == "WMETTemplate")
    return std::make_unique<Charm::WMETTemplate>(file_name, out_path);
  if (analysis_type == "WPTTemplate")
    return std::make_unique<Charm::WPTTemplate>(file_name, out_path);
  if (analysis_type == "WFakeRate")
    return std::make_unique<Charm::WFakeRateMeasurement>(file_name, out_path);
  if (analysis_type == "WDTruthLoop")
    return std::make_unique<Charm::WDTruthLoop>(file_name, out_path, "TruthTree");
  if (analysis_type == "ZDTruthLoop")
    return std::make_unique<Charm::ZDTruthLoop>(file_name, out_path, "TruthTree");

  return nullptr;
}

int main(int argc, char **argv) {
  if (argc < 4) {
    std::cerr << "Usage: " << argv[0]
              << " <file_name> <out_path> <analysis_type> "
                 "[sys_config_name] [config_name] [start_event] [end_event] [overrides...]\n"
              << "Note:\n"
              << "  - sys_config_name (default='default')\n"
              << "  - config_name (default='default')\n"
              << "  - start_event (default=0)\n"
              << "  - end_event (default=-1)\n"
              << "  - overrides are of the form key=value\n"
              << std::endl;
    return 1;
  }

  // Mandatory arguments
  TString file_name     = argv[1];
  TString out_path      = argv[2];
  TString analysis_type = argv[3];

  // Optional arguments (with default values)
  std::string sys_config_name = (argc > 4) ? argv[4] : "default";
  std::string config_name     = (argc > 5) ? argv[5] : "default";

  long long start_event = 0;  // default
  long long end_event   = -1; // default

  // We'll parse arguments from index=6 onward to see which might be start_event, end_event, or overrides
  int currentIndex = 6;

  // If we have an argument at currentIndex and it is purely integer, that's start_event
  if (currentIndex < argc && isInteger(argv[currentIndex])) {
    start_event = std::atoll(argv[currentIndex]);
    currentIndex++;
  }

  // If we have an argument at currentIndex and it is purely integer, that's end_event
  if (currentIndex < argc && isInteger(argv[currentIndex])) {
    end_event = std::atoll(argv[currentIndex]);
    currentIndex++;
  }

  // Now, everything after currentIndex is considered an override of the form key=value
  std::map<std::string, std::string> overrides;
  for (int i = currentIndex; i < argc; i++) {
    std::string arg = argv[i];
    // We only interpret something as an override if it contains '='
    auto delimiter_pos = arg.find('=');
    if (delimiter_pos == std::string::npos) {
      std::cerr << "WARNING: argument '" << arg
                << "' doesn't look like key=value; ignoring.\n";
      continue;
    }
    std::string key = arg.substr(0, delimiter_pos);
    std::string value = arg.substr(delimiter_pos + 1);
    overrides[key] = value;
  }

  // Print out some info
  std::cout << "input file     : " << file_name << "\n"
            << "out_path       : " << out_path << "\n"
            << "analysis_type  : " << analysis_type << "\n"
            << "sys_config_name: " << sys_config_name << "\n"
            << "config_name    : " << config_name << "\n"
            << "start_event    : " << start_event << "\n"
            << "end_event      : " << end_event << "\n";

  if (!overrides.empty()) {
    std::cout << "Overrides:\n";
    for (auto &ov : overrides) {
      std::cout << "  " << ov.first << " = " << ov.second << "\n";
    }
  }

  // Create and run the event loop
  auto el = createEventLoop(analysis_type, file_name, out_path);
  if (!el) {
    std::cerr << "Invalid analysis type '" << analysis_type << "' specified... exiting.\n";
    return 1;
  }

  el->set_config(config_name);
  el->apply_overrides(overrides);
  el->set_sys_config(sys_config_name);
  el->run(start_event, end_event);

  return 0;
}