#!/usr/bin/env python

from glob import glob
from multiprocessing import Pool
import os
import ROOT
import subprocess
import sys
import utils

ROOT.gROOT.SetBatch(True)


def wrapper(args):
    # launch hadd
    p = subprocess.Popen(["hadd"] + ["-T"] + ["-j"] + ["8"] + [args[0]] + args[1],
                         stderr=subprocess.STDOUT)
    p.communicate()
    return

jobs = []

# r=root, d=directories, f=files

input_name = "jobs/"

samples = {}

for r, d, f in os.walk(input_name):
    for file in f:

        if '.root' in file:
            
            if len(file.split(".")) < 3:
                print("Skipping %s" % file)
                continue

            base = file.split(".")[0]
            dsid = file.split(".")[2]

            if not dsid.isalnum:
                print("Skipping %s" % file)
                continue

            for s in utils.split:
                if base == s:
                    for subsample in utils.split[s]:
                        if dsid in utils.split[s][subsample]:
                            base = subsample
                            break

            if base not in samples:
                samples[base] = []
            samples[base] += [os.path.join(r, file)]

jobs += [(f"./{x}.root", samples[x]) for x in samples]

p = Pool(4)
for i, _ in enumerate(p.imap_unordered(wrapper, jobs)):
    print("done processing job %s/%s" % (i+1, len(jobs)))

# final merging
files = ["MG_Wjets_light_emu.root", "MG_Wjets_cjets_emu.root", "MG_Wjets_bjets_emu.root"]
if all([os.path.isfile(file) for file in files]):
    jobs = [("MG_Wjets_emu.root", files)]
    p = Pool(1)
    for i, _ in enumerate(p.imap_unordered(wrapper, jobs)):
        print("done processing job %s/%s" % (i+1, len(jobs)))

files = ["Sh_2211_Zjets_light_emu.root", "Sh_2211_Zjets_cjets_emu.root", "Sh_2211_Zjets_bjets_emu.root", "Sh_2211_Zjets_tau.root"]
if all([os.path.isfile(file) for file in files]):
    jobs = [("Sherpa2211_Zjets.root", files)]
    p = Pool(1)
    for i, _ in enumerate(p.imap_unordered(wrapper, jobs)):
        print("done processing job %s/%s" % (i+1, len(jobs)))

jobs = []
files1 = ["SPG_D0.root", "SPG_Dstar_plus.root"]
files2 = ["SPG_D0bar.root", "SPG_Dstar_minus.root"]
if all([os.path.isfile(file) for file in files1]):
    jobs += [("SPG_D0_Dstar.root", files1)]
if all([os.path.isfile(file) for file in files2]):
    jobs += [("SPG_D0_Dstar_bar.root", files2)]
p = Pool(2)
for i, _ in enumerate(p.imap_unordered(wrapper, jobs)):
    print("done processing job %s/%s" % (i+1, len(jobs)))
