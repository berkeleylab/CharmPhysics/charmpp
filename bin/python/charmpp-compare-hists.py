#!/usr/bin/env python

import ROOT
import argparse
import sys
import os
import numpy as np
import random
import time

GREEN = '\033[92m'
RED = '\033[91m'
YELLOW = '\033[93m'
END = '\033[0m'

def get_bin_contents(hist):
    n_bins = hist.GetNcells()
    bin_contents = np.fromiter((hist.GetBinContent(i) for i in range(n_bins)), dtype=np.float64, count=n_bins)
    return bin_contents

def histograms_are_identical(hist1, hist2):
    if not hist1 or not hist2:
        return False

    if hist1.GetNbinsX() != hist2.GetNbinsX():
        return False
    if hist1.GetXaxis().GetXmin() != hist2.GetXaxis().GetXmin():
        return False
    if hist1.GetXaxis().GetXmax() != hist2.GetXaxis().GetXmax():
        return False

    bin_contents1 = get_bin_contents(hist1)
    bin_contents2 = get_bin_contents(hist2)

    return np.array_equal(bin_contents1, bin_contents2)

def main(file1_path, file2_path, plot_mode, list_histograms, output_dir, max_histograms):
    start_time = time.time()
    ROOT.gROOT.SetBatch(True)

    file1 = ROOT.TFile(file1_path, "READ")
    if file1.IsZombie():
        print(f"{RED}Error: Cannot open file {file1_path}{END}")
        sys.exit(1)

    file2 = ROOT.TFile(file2_path, "READ")
    if file2.IsZombie():
        print(f"{RED}Error: Cannot open file {file2_path}{END}")
        sys.exit(1)

    # Only consider 1D histograms (TH1F, TH1D, TH1I)
    histogram_types = {"TH1F", "TH1D", "TH1I"}
    keys1 = set([key.GetName() for key in file1.GetListOfKeys() if key.GetClassName() in histogram_types])
    keys2 = set([key.GetName() for key in file2.GetListOfKeys() if key.GetClassName() in histogram_types])

    print(f"{file1_path} has {len(keys1)} histograms.")
    print(f"{file2_path} has {len(keys2)} histograms.")

    all_keys = list(keys1.union(keys2))

    total_histograms = len(all_keys)
    num_histograms_to_check = min(max_histograms, total_histograms)

    # Sample histogram keys if max_histograms < total_histograms
    if num_histograms_to_check < total_histograms:
        sampled_keys = random.sample(all_keys, num_histograms_to_check)
        print(f"{YELLOW}Note: Sampling {num_histograms_to_check} histograms from {total_histograms} total histograms.{END}")
    else:
        sampled_keys = all_keys

    comparison_results = []
    non_identical_histograms = []
    result = True

    progress_threshold = 10  # Initialize progress threshold to 10%

    for i, key in enumerate(sampled_keys, start=1):
        result_message = ""
        if key in keys1 and key in keys2:
            hist1 = file1.Get(key)
            hist2 = file2.Get(key)
            identical = histograms_are_identical(hist1, hist2)
            if not identical:
                result = False
                non_identical_histograms.append(f"{RED}Different{END} {key}")

            if plot_mode == "all" or (plot_mode == "diff" and not identical):
                if not os.path.exists(output_dir):
                    os.makedirs(output_dir)

                canvas = ROOT.TCanvas("Canvas", f"Histogram Comparison: {key}", 800, 600)

                max_y = max(hist1.GetMaximum(), hist2.GetMaximum())
                hist1.SetMaximum(max_y * 1.3)

                hist1.SetLineColor(ROOT.kBlue)
                hist2.SetLineColor(ROOT.kRed)

                hist1.SetStats(0)
                hist2.SetStats(0)

                hist1.Draw()
                hist2.Draw("SAME")

                file1_name = os.path.basename(file1.GetName())
                file2_name = os.path.basename(file2.GetName())

                legend = ROOT.TLegend(0.4, 0.8, 0.9, 0.9)
                legend.SetBorderSize(0)
                legend.SetFillStyle(0)
                legend.AddEntry(hist1, file1_name)
                legend.AddEntry(hist2, file2_name)
                legend.Draw()

                canvas.SaveAs(os.path.join(output_dir, f"{key}.pdf"))
                canvas.Close()
                del canvas  # Explicitly delete canvas

            # Delete histograms only when they are defined
            del hist1
            del hist2

        else:
            result = False
            missing_in = '2nd' if key in keys1 else '1st'
            result_message = f"{YELLOW}Missing in the {missing_in} file{END}"
            non_identical_histograms.append(f"{result_message} {key}")

        comparison_results.append(f"{result_message} {key}")

        # Progress update
        percent_done = int((i / num_histograms_to_check) * 100)
        if percent_done >= progress_threshold:
            print(f"({i}/{num_histograms_to_check}) histograms checked. {percent_done}% completed.")
            progress_threshold += 10  # Increase threshold for next 10%

    print()

    if list_histograms:
        for message in comparison_results:
            print(message)
    else:
        for message in non_identical_histograms:
            print(message)

    if result:
        final_result_message = f"{GREEN}All histograms are identical.{END}"
    else:
        final_result_message = f"{RED}There are differences in the histograms.{END}"
        if not list_histograms and not plot_mode:
            final_result_message += " Use '-l' to see the list and '-p diff' to make plots."
        elif not plot_mode:
            final_result_message += " Use '-p diff' to make plots."

    print(final_result_message)

    total_time = time.time() - start_time
    print(f"Total execution time: {total_time:.2f} seconds")

    sys.exit(0 if result else 1)

# Setup argument parser
parser = argparse.ArgumentParser(description='Compare 1D histograms from two ROOT files.')
parser.add_argument('file1', help='Path to the first ROOT file')
parser.add_argument('file2', help='Path to the second ROOT file')
parser.add_argument('-p', '--plot', choices=['all', 'diff'], help='Plot mode: "all" to plot all histograms, "diff" to plot only different ones')
parser.add_argument('-l', '--list', action='store_true', help='List all histograms and their comparison results')
parser.add_argument('-o', '--output', default='fig_comp', help='Output directory for plots')
parser.add_argument('--max', type=int, default=10000, help='Maximum number of histograms to check (default: 10k)')

# Parse arguments
args = parser.parse_args()

# Run comparison
main(args.file1, args.file2, args.plot, args.list, args.output, args.max)
