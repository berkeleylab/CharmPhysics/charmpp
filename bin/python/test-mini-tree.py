#!/usr/bin/env python
import ROOT

VARS = [
    "Dmeson_m",
    "Dmeson_pt",
    "Dmeson_eta",
    "Dmeson_phi",
    "truth_Dmeson_m",
    "truth_Dmeson_pt",
    "truth_Dmeson_eta",
    "Dmeson_track_jet_10_pt",
    "Dmeson_track_jet_10_zt",
    "Dmeson_track_jet_8_pt",
    "Dmeson_track_jet_8_zt",
    "Dmeson_track_jet_6_pt",
    "Dmeson_track_jet_6_zt",
    "truth_Dmeson_track_jet_10_pt",
    "truth_Dmeson_track_jet_10_zt",
    "truth_Dmeson_track_jet_8_pt",
    "truth_Dmeson_track_jet_8_zt",
    "truth_Dmeson_track_jet_6_pt",
    "truth_Dmeson_track_jet_6_zt",
]

CATS = [
    "Matched",
    "MisMatched",
    "411MisMatched",
    "421MisMatched",
    "Other",
    "HardMisMatched",
]

def main(args):
    for f_name in args:
      print(f"Checking {f_name}")
      f = ROOT.TFile(f_name)
      t = f.Get("MiniCharm")

      if "data" in f_name:
          cats = [""]
      else:
          cats = CATS

      for charge in ["OS", "SS"]:
          for bjet in ["0tag", "1tag"]:
              for lepton in ["el", "mu"]:
                  for lep_charge in ["plus", "minus"]:
                      for category in cats:
                        for var in VARS:
                          h_name = f"{lepton}_{lep_charge}_SR_{bjet}_Dplus_{charge}"
                          if category:
                              h_name += f"_{category}"
                          h1 = f.Get(h_name + f"__{var}")
                          if h1:
                              h2 = h1.Clone(f"{h_name}_clone")
                              if "data" in f_name:
                                t.Draw(f"{var} >> {h_name}_clone", f"weight*(channel==\"{h_name}\")", "goff")
                                n_zero = t.GetEntries(f"weight==0&&(channel==\"{h_name}\")")
                              else:
                                t.Draw(f"{var} >> {h_name}_clone", f"weight*(channel==\"{h_name}\")", "goff")
                                n_zero = t.GetEntries(f"weight==0&&(channel==\"{h_name}\")")
                              assert h1.GetEntries() == h2.GetEntries() + n_zero, f"{h1.GetEntries()} != {h2.GetEntries() + n_zero} for {h_name} and {var}"
                              if h1.Integral():
                                assert abs(h1.Integral() - h2.Integral()) / h1.Integral() < 1e-4, f"{h1.Integral()} != {h2.Integral()} for {h_name} and {var}"
                              else:
                                assert h1.Integral() == h2.Integral(), f"{h1.Integral()} != {h2.Integral()} for {h_name} and {var}"
                              print(f"{h_name}__{var} OK")

if __name__ == "__main__":
    import optparse
    parser = optparse.OptionParser()

    # ----------------------------------------------------
    # arguments
    # ----------------------------------------------------
    # parser.add_option('-i', '--input',
    #                   action="store", dest="input",
    #                   help="input file")

    # parse input arguments
    _, args = parser.parse_args()

    main(args)

