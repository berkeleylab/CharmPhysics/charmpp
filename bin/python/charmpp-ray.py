#!/usr/bin/env python

from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import ray
import shutil
import subprocess
import sys, os
import time

import utils


CHARMPP_BUILD_PATH = os.environ['CHARMPP_BUILD_PATH']
CHARMPP_NTUPLE_PATH = os.environ['CHARMPP_NTUPLE_PATH']
CHARMPP_RUN_PATH = os.environ['CHARMPP_RUN_PATH']

@ray.remote
def execute_dry(job):
    print (job)
    return

@ray.remote
def execute(job):
    outfile = os.path.join(f"logs_{options.sys_config_name}", os.path.basename(job[0]).replace(".root", ".txt"))
    outfile = outfile.replace(".txt", ".%s.txt" % options.sys_config_name)
    if len(job) > 5:
        outfile = outfile.replace(".txt", ".%s.%s.txt" % (job[-2], job[-1]))
    OUTPUT = open(outfile, 'w')
    p = subprocess.call(["charmpp"] + job, stdout=OUTPUT)
    if p > 0:
        print ("Job ", job, " exited with ", p)
        return None

    # copy finished file
    outfile_root = outfile.replace(".txt", ".root").replace(f"logs_{options.sys_config_name}/", "")
    if os.path.isfile(outfile_root):
        shutil.move(outfile_root, os.path.join(f"output_{options.sys_config_name}", outfile_root))
    else:
        print (f"WARNING: output file not found for {outfile_root}")

    return outfile_root

def print_progress(progress, total):
    sys.stdout.write("Job progress: %d/%d [%d%%]   \r" % (progress, total, 100*progress/total) )
    sys.stdout.flush()

def run(options, args):

    # input files
    jobs_file = os.path.join(CHARMPP_NTUPLE_PATH, 'sum_of_weights.txt')
    if not os.path.isfile(jobs_file):
        print ("no sum_of_weights.txt file found in %s" % CHARMPP_NTUPLE_PATH)
        print ("call charmpp-prepare first")
        sys.exit(1000)

    # define input tasks
    tasks = utils.get_jobs(jobs_file, options, args)
    print ("number of jobs: ", len(tasks))

    # save tasks to file and exit
    if options.export_tasks:
        with open('task_list.txt', 'a') as f:
            for task in tasks:
                f.write(f"charmpp {' '.join(task)}\n")
        return

    # dir for finished output
    if not os.path.exists(f"./output_{options.sys_config_name}"):
        os.makedirs(f"./output_{options.sys_config_name}")

    # dir for log files
    if not os.path.exists(f"./logs_{options.sys_config_name}"):
        os.makedirs(f"./logs_{options.sys_config_name}")

    # initialize Ray
    if not options.local:
        # running on cluster
        ray.init(address="%s" % options.redis_address,
                 _redis_password="%s" % options.redis_password)
    else:
        # running locally
        print ("running locally")
        ray.init(num_cpus=int(options.num_processes), ignore_reinit_error=True)

    # total number of jobs
    total = len(tasks)

    # real or fake executable
    if options.dry_run:
        exe = execute_dry
    else:
        exe = execute

    # launch tasks with ray
    remaining_ids = []
    for task in tasks:
        remaining_ids += [exe.remote(task)]

    # wait for jobs to finish
    progress = 0
    timeline_count = 0
    print_progress(progress, total)
    while remaining_ids:
        ready_ids, remaining_ids = ray.wait(remaining_ids, timeout=10.0)
        progress += len(ready_ids)
        print_progress(progress, total)

        # timeline
        if options.timeline:
            if progress > (total/10.)*(1+timeline_count):
                ray.timeline(filename="timeline.json")
                timeline_count += 1

    # final timeline
    if options.timeline:
        ray.timeline(filename="timeline.json")

if __name__ == "__main__":
    import optparse
    parser = optparse.OptionParser(usage=globals()['__doc__'])

    # ----------------------------------------------------
    # arguments
    # ----------------------------------------------------
    parser.add_option('-a', '--analysis-type',
                      action="store", dest="analysis_type",
                      help="analysis type (options: WCharm, ZCharm, WDCharm)")
    parser.add_option('-m', '--max-events',
                      action="store", dest="max_events",
                      help="maximum number of events per job",
                      default=-1)
    parser.add_option('-l', '--local',
                      action="store_true", dest="local",
                      help="rune locally on one node",
                      default=False)
    parser.add_option('-p', '--num-processes',
                      action="store", dest="num_processes",
                      help="number of processes for local running")
    parser.add_option('-f', '--force',
                      action="store_true", dest="force",
                      help="override if existing output file",
                      default=False)
    parser.add_option('-c', '--config-name',
                      action="store", dest="config_name",
                      help="name of the config file",
                      default="default")
    parser.add_option('-s', '--sys-config-name',
                      action="store", dest="sys_config_name",
                      help="name of the systematics config file",
                      default="default")
    parser.add_option('--export-tasks',
                      action="store_true", dest="export_tasks",
                      help="only print out tasks to a txt file, no actual running",
                      default=False)
    parser.add_option('--dry-run',
                      action="store_true", dest="dry_run",
                      help="only print out tasks, no actual running",
                      default=False)
    parser.add_option('--redis-address',
                      action="store", dest="redis_address",
                      help="redis-address of the head node")
    parser.add_option('--redis-password',
                      action="store", dest="redis_password",
                      help="password for the resdis server")
    parser.add_option('--timeline',
                      action="store_true", dest="timeline",
                      help="save Ray timeline")
    parser.add_option('--truth-tree',
                      action="store_true", dest="truth_tree",
                      help="running on the TruthTree instead of the CharmAnalysis tree")

    # parse input arguments
    options, args = parser.parse_args()

    # set default ip and password for non-local running
    if not options.export_tasks:
        if not options.local:
            if not options.redis_address:
                options.redis_address = "%s:%s" % (os.environ["RAY_HEAD_IP"], os.environ["RAY_REDIS_PORT"])
                options.redis_password = os.environ["RAY_REDIS_PASSWORD"]
        else:
            if not options.num_processes:
                options.num_processes = os.environ["RAY_NWORKERS"]

    run(options, args)
