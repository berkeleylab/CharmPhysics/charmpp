import os

split = {
    'Top_Signal': {
        'Top_ttbar_nonallhad': ['410470'],
        'Top_ttbar_dil': ['410472'],
    },
    'Top': {
        'Top_single_top': ['410644', '410645', '410646', '410647', '410658', '410659'],
        'Top_single_top_DS': ['410644', '410645', '410654', '410655', '410658', '410659'],
        'Top_ttx': ['410155', '410156', '410157', '410218', '410219', '410220'],
    },
    'Top_sys': {
        'Top_ttbar_HS': ['410464', '410465'],
        'Top_ttbar_ISR': ['410480', '410482'],
    },
    'Top_sys_Herwig': {
        'Top_ttbar_Shower': ['411233', '411234'],
    },
    'Powheg_Wjets': {
        'Ph_Wjets_emu': ['361100', '361101', '361103', '361104'],
        'Ph_Wjets_tau': ['361102', '361105'],
    },
    'Powheg_Zjets': {
        'Ph_Zjets_emu': ['361106', '361107'],
        'Ph_Zjets_tau': ['361108'],
    },
    'MGPy8EG_Zjets': {
        'MG_Zjets_light_emu': ['363123', '363126', '363129', '363132', '363135', '363138', '363141', '363144', '363147', '363150', '363153', '363156', '363159', '363162', '363165', '363168'],
        'MG_Zjets_cjets_emu': ['363124', '363127', '363130', '363133', '363136', '363139', '363142', '363145', '363148', '363151', '363154', '363157', '363160', '363163', '363166', '363169'],
        'MG_Zjets_bjets_emu': ['363125', '363128', '363131', '363134', '363137', '363140', '363143', '363146', '363149', '363152', '363155', '363158', '363161', '363164', '363167', '363170'],
        'MG_Zjets_tau': ['361510', '361511', '361512', '361513', '361514'],
    },
    'MGPy8EG_FxFx_Zjets': {
        'MG_FxFx_Zjets_light_emu': ['506195','506198'],
        'MG_FxFx_Zjets_cjets_emu': ['506194','506197'],
        'MG_FxFx_Zjets_bjets_emu': ['506193','506196'],
        'MG_FxFx_Zjets_tau': ['512198','512199','512200'],
    },
    'MGPy8EG_Wjets': {
        'MG_Wjets_light_emu': ['363600', '363603', '363606', '363609', '363612', '363615', '363618', '363621', '363624', '363627', '363630', '363633', '363636', '363639', '363642', '363645'],
        'MG_Wjets_cjets_emu': ['363601', '363604', '363607', '363610', '363613', '363616', '363619', '363622', '363625', '363628', '363631', '363634', '363637', '363640', '363643', '363646'],
        'MG_Wjets_bjets_emu': ['363602', '363605', '363608', '363611', '363614', '363617', '363620', '363623', '363626', '363629', '363632', '363635', '363638', '363641', '363644', '363647'],
        'MG_Wjets_tau': ['363648', '363651', '363654', '363657', '363660', '363663', '363666', '363669', '363649', '363652', '363655', '363658', '363661', '363664', '363667', '363670', '363650', '363653', '363656', '363659', '363662', '363665', '363668', '363671'],
    },
    'MGPy8EG_FxFx_Wjets': {
        'MG_FxFx_Wjets_light_emu': ['508981', '508984'],
        'MG_FxFx_Wjets_cjets_emu': ['508980', '508983'],
        'MG_FxFx_Wjets_bjets_emu': ['508979', '508982'],
        'MG_FxFx_Wjets_tau': ['509751','509752','509753','509754','509755','509756'],
    },
    'Sherpa_Wjets': {
        'Sh_Wjets_light_emu': ['700340', '700343'],
        'Sh_Wjets_cjets_emu': ['700339', '700342'],
        'Sh_Wjets_bjets_emu': ['700338', '700341'],
        # 'Sh_Wjets_light_700343': ['700343'],
        # 'Sh_Wjets_cjets_700342': ['700342'],
        # 'Sh_Wjets_bjets_700341': ['700341'],
        # 'Sh_Wjets_light_700340': ['700340'],
        # 'Sh_Wjets_cjets_700339': ['700339'],
        # 'Sh_Wjets_bjets_700338': ['700338'],
        'Sh_Wjets_tau': ['700344','700345','700346','700347','700348','700349'],
    },
    'Sherpa_Zjets': {
        'Sh_Zjets_light_emu': ['700322', '700325'],
        'Sh_Zjets_cjets_emu': ['700321', '700324'],
        'Sh_Zjets_bjets_emu': ['700320', '700323'],
        'Sh_Zjets_tau': ['700792','700793','700794'],
    },
    'Sherpa_WplusD': {
        'Sh_WplusD': ['700366', '700367'],
    },
    'MGPy8EG_NLO_WplusD': {
        'MG_NLO_WplusD': ['507704', '507705'],
    },
    'MGPy8EG_FxFx_WplusD': {
        'MG_FxFx_WplusD': ['501716', '501717'],
    },
    'Sherpa_WplusDsLambdaC': {
        'Sh_WplusDsLambdaC': ['700684', '700685'],
    },
    'MGPy8EG_NLO_WplusDsLambdaC': {
        'MG_NLO_WplusDsLambdaC': ['523138', '523139'],
    },
    'Sherpa_ZplusD': {
        'Sh_ZplusD': ['700686', '700687'],
    },
    'MGPy8EG_FxFx_ZplusD': {
        'MG_FxFx_ZplusD': ['523140', '523141'],
    },
    'SPG': {
        'SPG_Dminus': ['999914'],
        'SPG_Dsminus': ['999912'],
        'SPG_Dstar_plus': ['999928'],
        'SPG_Dstar_minus': ['999918'],
        'SPG_D0': ['999921'],
        'SPG_D0bar': ['999911'],
        'SPG_Baryonbar': ['999913', '999915', '999916', '999917'],
        'SPG_Dplus': ['999924'],
        'SPG_Ds': ['999922'],
        'SPG_Baryon': ['999923', '999925', '999926', '999927'],
    },
}

def get_jobs(jobs_file, options, args):
    concurrent_jobs = {}

    output_folder = f"output_{options.output_suffix}"

    with open(jobs_file, 'r') as f:

        for l in f:
            line = l.split()

            input_file = os.path.join(os.environ['CHARMPP_NTUPLE_PATH'], line[0])

            process = False
            if len(args) > 0:
                for arg in args:
                    if arg in input_file:
                        process = True
            else:
                process = True

            if not process:
                continue

            concurrent_jobs[line[0]] = []

            if len(line) > 3:
                # new ntuples have two trees with different number of events
                if options.truth_tree:
                    n_events = int(line[-1])
                else:
                    n_events = int(line[-2])
            else:
                # old (or non-signal) ntuples have only one tree
                n_events = int(line[-1])

            if int(options.max_events) > 0 and n_events > int(options.max_events):
                for start in range(0, n_events+1, int(options.max_events)):

                    out_name = line[0].replace(".root", "") + ".%s.%s.%s.root" % (options.output_suffix, start, start+int(options.max_events))
                    out_name = os.path.join(output_folder, out_name)
                    print (out_name)
                    if os.path.isfile(out_name) and not options.force:
                        print ("file %s already exists" % (out_name))
                    else:
                        job = [
                            input_file, ".", options.analysis_type, options.sys_config_name, options.config_name,
                            str(start), str(start + int(options.max_events))
                        ]
                        if options.overrides:
                            job.append(options.overrides)
                        concurrent_jobs[line[0]].append(job)

            else:
                if os.path.isfile(os.path.join(output_folder, line[0].replace(".root", "") + ".%s.root" % (options.output_suffix))) and not options.force:
                    print ("file %s already exists" % (line[0]))
                else:
                    job = [input_file, ".", options.analysis_type, options.sys_config_name, options.config_name]
                    if options.overrides:
                        job.append(options.overrides)
                    concurrent_jobs[line[0]].append(job)

    tasks = []
    at_least_one_key = True
    while at_least_one_key:
        at_least_one_key = False
        for key in concurrent_jobs:
            if not concurrent_jobs[key]:
                continue
            tasks += [concurrent_jobs[key].pop()]
            at_least_one_key = True

        if not at_least_one_key:
            break

    return tasks
