#!/usr/bin/env python

import re
import os
import sys
import yaml
import ROOT
ROOT.gROOT.SetBatch(True)


path = os.path.join(os.environ['CHARMPP_NTUPLE_PATH'])

sum_of_weights_file = open(os.path.join(
    os.environ['CHARMPP_NTUPLE_PATH'], 'sum_of_weights.txt'), 'w')

sum_of_weights = {}

sum_of_weights_sys = {}

# r=root, d=directories, f=files
for r, d, f in os.walk(path):
    for file in f:

        if not ".root" in file:
            continue

        shortName = ".".join(file.split(".")[0:3])
        if not shortName in sum_of_weights.keys():
            sum_of_weights[shortName] = 0
            sum_of_weights_sys[shortName] = {}

        file_path = os.path.join(r, file)
        root_file = ROOT.TFile(file_path, "READ")
        for key in root_file.GetListOfKeys():
            if "CutBookkeeper" in key.GetName() and "NOSYS" in key.GetName():
                sum_of_weights[shortName] += root_file.Get(
                    key.GetName()).GetBinContent(2)
                print("Found CutBookkeeper hist ", key.GetName())
            elif "CutBookkeeper" in key.GetName() and "Alg" not in key.GetName():
                sys = re.findall(
                    "CutBookkeeper_[0-9]*_[0-9]*_(.*)", key.GetName())[0]
                events = root_file.Get(key.GetName()).GetBinContent(2)
                if sys not in sum_of_weights_sys[shortName].keys():
                    sum_of_weights_sys[shortName][sys] = 0
                sum_of_weights_sys[shortName][sys] += events
        root_file.Close()

with open(os.path.join(os.environ['CHARMPP_NTUPLE_PATH'], 'sum_of_weights_sys.yaml'), 'w') as outfile:
    yaml.dump(sum_of_weights_sys, outfile)

# r=root, d=directories, f=files
for r, d, f in os.walk(path):
    for file in f:

        if not ".root" in file:
            continue

        shortName = ".".join(file.split(".")[0:3])
        if not shortName in sum_of_weights.keys():
            sum_of_weights[shortName] = 0

        file_path = os.path.join(r, file)
        root_file = ROOT.TFile(file_path, "READ")

        # default tree
        total_events = 0
        tree = root_file.Get("CharmAnalysis")
        if tree:
            total_events = tree.GetEntries()
            if sum_of_weights[shortName] == 0:
                if tree.GetBranch("EventInfo_mcEventWeights"):
                    temp_name = "temp" + shortName
                    temp_h = ROOT.TH1D(temp_name, temp_name, 1, 0, 1)
                    tree.Draw("0.5>>" + temp_name, "EventInfo_mcEventWeights[0]", "goff")
                    sum_of_weights[shortName] = temp_h.GetSumOfWeights()
        else:
            total_events = 0
        del tree

        # truth tree
        total_truth_events = 0
        truth_tree = root_file.Get("TruthTree")
        if truth_tree:
            total_truth_events = truth_tree.GetEntries()
        else:
            total_truth_events = 0
        del truth_tree

        root_file.Close()
        print("file %s has %s events and %s sum w and %s events in the truth tree" % (
            file, total_events, sum_of_weights[shortName], total_truth_events))
        sum_of_weights_file.write("%s\t%s\t%s\t%s\n" % (
            file, sum_of_weights[shortName], total_events, total_truth_events))
