#!/usr/bin/env python

import sys
import os
import subprocess
import json
import ROOT
ROOT.gROOT.SetBatch(True)


merge_path = os.environ['CHARMPP_NTUPLE_PATH']
if not os.path.exists(merge_path):
    os.makedirs(merge_path)


def wrapper(args):

    output_file = args[0]
    if os.path.exists(output_file):
        print(f"Warning: File {output_file} already exists. Skipping.")
        return
    # launch hadd
    p = subprocess.Popen(["hadd"] + [output_file] + args[1], stderr=subprocess.STDOUT)
    p.communicate()
    return


def projecta_to_cfs(f):
    return f
    if not os.path.isfile(f):
        f = f.replace("projecta", "cfs")
        if not os.path.isfile(f):
            print(f"file not found: {f}")
        else:
            print(f"successfully found {f}")
    return f


def main(options, args):
    full_path = os.path.join(options.input)
    with open(full_path, 'r') as json_file:
        subdatasets = json.load(json_file)

    reduced_samples = {}

    # reduce size
    for sample in subdatasets:
        sample_reduced = {}
        for subdataset in subdatasets[sample]:
            subdataset_size = 0
            reduced_subdataset = []
            for f in subdatasets[sample][subdataset]:
                f = projecta_to_cfs(f)
                b = os.path.getsize(f)
                subdataset_size += b
                if subdataset_size > 50000000000.:
                    reduced_samples[sample] = {}
                    if subdataset in sample_reduced:
                        sample_reduced[subdataset] += [reduced_subdataset]
                    else:
                        sample_reduced[subdataset] = [reduced_subdataset]
                    reduced_subdataset = [f]
                    subdataset_size = 0
                else:
                    reduced_subdataset += [f]
            if subdataset in sample_reduced:
                sample_reduced[subdataset] += [reduced_subdataset]

        for s in sample_reduced:
            # no dupes
            reduced = sample_reduced[s]
            l = sum(reduced, [])
            assert set([x for x in l if l.count(x) > 1]) == set([])
            reduced_samples[sample][s] = reduced

    # replace
    for sample in reduced_samples:
        for subdataset in reduced_samples[sample]:
            del subdatasets[sample][subdataset]
            sample_reduced = reduced_samples[sample][subdataset]
            for i, x in enumerate(sample_reduced):
                subdatasets[sample][subdataset + ".%s" % i] = x

    jobs = []

    for sample in subdatasets:
        for subdataset in subdatasets[sample]:
            files = subdatasets[sample][subdataset]
            for i in range(len(files)):
                f = files[i]
                f = projecta_to_cfs(f)
                files[i] = f
            merge_file = os.path.join(merge_path, subdataset + ".root")
            jobs += [[merge_file, files]]

    # check if tqdm is available
    import imp
    try:
        imp.find_module('tqdm')
        tqdm_found = True
    except ImportError:
        print("tqdm module not found so there will be no progress bar. Install tqdm to get a progress bar with:\npip install tqdm --user")
        tqdm_found = False

    from multiprocessing import Pool
    p = Pool(int(options.threads))
    if tqdm_found:
        import tqdm
        for _ in tqdm.tqdm(p.imap_unordered(wrapper, jobs), total=len(jobs)):
            pass
    else:
        for i, _ in enumerate(p.imap_unordered(wrapper, jobs)):
            print("done processing job %s/%s" % (i+1, len(jobs)))


if __name__ == "__main__":
    import optparse
    parser = optparse.OptionParser(usage=globals()['__doc__'])

    # ----------------------------------------------------
    # arguments
    # ----------------------------------------------------
    parser.add_option('-t', '--threads',
                      action="store", dest="threads",
                      help="number of threads to run on",
                      default=8)
    parser.add_option('-i', '--input',
                      action="store", dest="input",
                      help="input json file",
                      default=-1)

    # parse input arguments
    options, args = parser.parse_args()

    main(options, args)
