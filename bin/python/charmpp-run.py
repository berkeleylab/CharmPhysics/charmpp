#!/usr/bin/env python

import ROOT
ROOT.gROOT.SetBatch(True)

import shutil
import subprocess
import sys, os

import utils

def launch_wrapper(args):
    job, options = args
    print(f"job options: {options}")

    outfile = os.path.join(f"logs_{options.output_suffix}", os.path.basename(job[0]).replace(".root", ".txt"))
    outfile = outfile.replace(".txt", ".%s.txt" % options.output_suffix)
    if len(job) > 5:
        # check if last argument is a number
        if job[-1].isdigit():
            outfile = outfile.replace(".txt", ".%s.%s.txt" % (job[-2], job[-1]))
        else:
            outfile = outfile.replace(".txt", ".%s.%s.txt" % (job[-3], job[-2]))
    OUTPUT = open(outfile, 'w')

    # Add CLI overrides to the command
    overrides = []
    if hasattr(options, 'overrides') and options.overrides:
        overrides = options.overrides.split(',')
        overrides = [f"{o}" for o in overrides]

    p = subprocess.call(["charmpp"] + job + overrides, stdout=OUTPUT)
    if p > 0:
        print ("Job ", job, " exited with ", p)
        return None

    # copy finished file
    outfile_root = outfile.replace(".txt", ".root").replace(f"logs_{options.output_suffix}/", "")
    if os.path.isfile(outfile_root):
        shutil.move(outfile_root, os.path.join(f"output_{options.output_suffix}", outfile_root))
    else:
        print (f"WARNING: output file not found for {outfile_root}")

def run(options, args):

    jobs_file = os.path.join(os.environ['CHARMPP_NTUPLE_PATH'], 'sum_of_weights.txt')
    if not os.path.isfile(jobs_file):
        print ("no sum_of_weights.txt file found in %s" % os.environ['CHARMPP_NTUPLE_PATH'])
        print ("call charmpp-prepare first")
        sys.exit(1000)

    # dir for finished output
    if not os.path.exists(f"./output_{options.output_suffix}"):
        os.makedirs(f"./output_{options.output_suffix}")

    # dir for log files
    if not os.path.exists(f"./logs_{options.output_suffix}"):
        os.makedirs(f"./logs_{options.output_suffix}")

    # get jobs
    concurrent_jobs = utils.get_jobs(jobs_file, options, args)
    print ("number of jobs: ", len(concurrent_jobs))

    # save tasks to file and exit
    if options.export_tasks:
        with open('task_list.txt', 'a') as f:
            for job in concurrent_jobs:
                f.write(f"cd {os.environ['PWD']}/output_{options.output_suffix}; charmpp {' '.join(job)}\n")
        return

    # check if tqdm is available
    import imp
    try:
        imp.find_module('tqdm')
        tqdm_found = True
    except ImportError:
        print ("tqdm module not found so there will be no progress bar. Install tqdm to get a progress bar with:\npip install tqdm --user")
        tqdm_found = False

    wrapper = launch_wrapper

    from multiprocessing import Pool
    threads = int(options.threads)
    p = Pool(threads)
    if tqdm_found:
        import tqdm
        for _ in tqdm.tqdm(p.imap_unordered(wrapper, [(job, options) for job in concurrent_jobs]), total=len(concurrent_jobs)):
            pass
    else:
        for i, _ in enumerate(p.imap_unordered(wrapper, [(job, options) for job in concurrent_jobs])):
            print ("done processing job %s/%s" % (i+1, len(concurrent_jobs)))

if __name__ == "__main__":
    import optparse
    parser = optparse.OptionParser(usage=globals()['__doc__'])

    # ----------------------------------------------------
    # required
    # ----------------------------------------------------
    parser.add_option('-t', '--threads',
                      action="store", dest="threads",
                      help="number of threads to run on",
                      default=1)
    parser.add_option('-m', '--max-events',
                      action="store", dest="max_events",
                      help="maximum number of events per job",
                      default=-1)
    parser.add_option('-a', '--analysis-type',
                      action="store", dest="analysis_type",
                      help="analysis type (options: WCharm, ZCharm, WDCharm)")
    parser.add_option('-f', '--force',
                      action="store_true", dest="force",
                      help="override if existing output file",
                      default=False)
    parser.add_option('-c', '--config-name',
                      action="store", dest="config_name",
                      help="name of the config file",
                      default="default")
    parser.add_option('-s', '--sys-config-name',
                      action="store", dest="sys_config_name",
                      help="name of the systematics config file",
                      default="default")
    parser.add_option('--truth-tree',
                      action="store_true", dest="truth_tree",
                      help="running on the TruthTree instead of the CharmAnalysis tree")
    parser.add_option('--export-tasks',
                      action="store_true", dest="export_tasks",
                      help="only print out tasks to a txt file, no actual running",
                      default=False)
    parser.add_option('-o', '--overrides',
                      action="store", dest="overrides",
                      help="comma-separated list of key=value pairs to override configuration settings",
                      default="")
    parser.add_option('-x', '--output-suffix',
                      action="store", dest="output_suffix",
                      help="suffix to append to the output file name",
                      default="")

    # parse input arguments
    options, args = parser.parse_args()

    # modify
    if options.output_suffix == "":
        options.output_suffix = options.sys_config_name

    run(options, args)
