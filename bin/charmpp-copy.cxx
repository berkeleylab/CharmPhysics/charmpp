#include <TFile.h>
#include <TH1.h>
#include <TKey.h>
#include <TTree.h>

#include <functional>
#include <iostream>
#include <string>
#include <vector>

#include "CharmAnalysis.h"
#include "Definitions.h"

bool PassDplus(CharmAnalysis& c, int i) {
  // SV χ2 requirement
  if (c.DMesons_fitOutput__Chi2_NOSYS->at(i) > 8.) {
    return false;
  }

  // min Lxy requirement
  if (c.DMesons_fitOutput__Lxy_NOSYS->at(i) * Charm::mm < 1.1) {
    return false;
  }

  // impact parameter
  if (fabs(c.DMesons_fitOutput__Impact_NOSYS->at(i)) * Charm::mm > 10.0) {
    return false;
  }

  // combinatorial background rejection
  if (c.DMesons_costhetastar_NOSYS->at(i) < -0.8) {
    return false;
  }

  // D*+ rejection
  if ((std::min(c.DMesons_m_NOSYS->at(i) - c.DMesons_mKpi1_NOSYS->at(i),
                c.DMesons_m_NOSYS->at(i) - c.DMesons_mKpi2_NOSYS->at(i))) *
          Charm::MeV <
      160) {
    return false;
  }

  // Ds rejection
  if ((std::min(c.DMesons_mPhi1_NOSYS->at(i), c.DMesons_mPhi2_NOSYS->at(i))) *
          Charm::MeV <
      8.0) {
    return false;
  }

  // D+ meson eta
  if (fabs(c.DMesons_eta_NOSYS->at(i)) > 2.2) {
    return false;
  }

  // D+ meson pt
  if (c.DMesons_pt_NOSYS->at(i) * Charm::GeV < 8.0 ||
      c.DMesons_pt_NOSYS->at(i) * Charm::GeV > 150.0) {
    return false;
  }

  // invariant mass cut
  if (c.DMesons_m_NOSYS->at(i) * Charm::GeV <= 1.7 ||
      c.DMesons_m_NOSYS->at(i) * Charm::GeV > 2.2) {
    return false;
  }

  return true;
}

bool PassDstar(CharmAnalysis& c, int i) {
  int D0Index = c.DMesons_D0Index_NOSYS->at(i);

  // SV χ2 requirement
  if (c.DMesons_fitOutput__Chi2_NOSYS->at(i) > 10.) {
    return false;
  }

  // min Lxy requirement
  if (c.DMesons_fitOutput__Lxy_NOSYS->at(D0Index) * Charm::mm < 0.0) {
    return false;
  }

  // impact parameter
  if (fabs(c.DMesons_fitOutput__Impact_NOSYS->at(i)) * Charm::mm > 10.0) {
    return false;
  }

  // D0 Mass window
  if (fabs(c.DMesons_m_NOSYS->at(D0Index) - Charm::D0_MASS) > 40) {
    return false;
  }

  // Soft pion d0
  if (fabs(c.DMesons_SlowPionD0_NOSYS->at(i)) > 1) {
    return false;
  }

  // D*+ meson eta
  if (fabs(c.DMesons_eta_NOSYS->at(i)) > 2.2) {
    return false;
  }

  // D*+ meson pt
  if (c.DMesons_pt_NOSYS->at(i) * Charm::GeV < 8.0 ||
      c.DMesons_pt_NOSYS->at(i) * Charm::GeV > 150.0) {
    return false;
  }

  // D* invariant mass cut
  if (c.DMesons_m_NOSYS->at(i) <= 1700. || c.DMesons_m_NOSYS->at(i) > 2200.) {
    return false;
  }

  // D*-D0 invariant mass cut
  if (c.DMesons_DeltaMass_NOSYS->at(i) <= 140. ||
      c.DMesons_DeltaMass_NOSYS->at(i) > 180.) {
    return false;
  }

  return true;
}

bool PassD0(CharmAnalysis& c, int i) { return true; }

bool PassDs(CharmAnalysis& c, int i) {
  // SV χ2 requirement
  if (c.DMesons_fitOutput__Chi2_NOSYS->at(i) > 6.) {
    return false;
  }

  // min Lxy requirement
  if (c.DMesons_fitOutput__Lxy_NOSYS->at(i) * Charm::mm < 0.5) {
    return false;
  }

  // impact parameter
  if (fabs(c.DMesons_fitOutput__Impact_NOSYS->at(i)) * Charm::mm > 10.0) {
    return false;
  }

  // combinatorial background rejection
  if (c.DMesons_costhetastar_NOSYS->at(i) > 0.8) {
    return false;
  }

  // D+ rejection
  if (fabs(c.DMesons_mPhi1_NOSYS->at(i) - Charm::PHI_MASS) > 8.0) {
    return false;
  }

  // D+ meson eta
  if (fabs(c.DMesons_eta_NOSYS->at(i)) > 2.2) {
    return false;
  }

  // D+ meson pt
  if (c.DMesons_pt_NOSYS->at(i) * Charm::GeV < 8.0) {
    return false;
  }

  // invariant mass cut
  if (c.DMesons_m_NOSYS->at(i) * Charm::GeV <= 1.7 ||
      c.DMesons_m_NOSYS->at(i) * Charm::GeV > 2.2) {
    return false;
  }

  return true;
}

// Define a map to associate particle IDs with pass functions
std::map<int, std::function<bool(CharmAnalysis&, int)>> particleSelections = {
    {411, PassDplus}, {413, PassDstar}, {421, PassD0}, {431, PassDs}};

// Template function to remove elements from a vector of any type
template <typename T>
void RemoveElements(std::vector<T>& vec,
                    const std::vector<int>& indicesToRemove) {
  // Create a temporary vector to hold non-removed elements
  std::vector<T> temp;
  for (size_t i = 0; i < vec.size(); i++) {
    if (std::find(indicesToRemove.begin(), indicesToRemove.end(), i) ==
        indicesToRemove.end()) {
      temp.push_back(vec[i]);
    }
  }
  // Assign the filtered vector back to the original vector
  vec = temp;
}

// Update indices after removing elements
void UpdateIndices(std::vector<int>& indices,
                   const std::vector<int>& indicesToRemove) {
  for (int& index : indices) {
    for (const int& removedIndex : indicesToRemove) {
      if (index > removedIndex) {
        // Decrement the index if it's greater than the removed index
        index--;
      }
    }
  }
}

void RemoveCharmHadrons(CharmAnalysis& c) {
  std::vector<int> toRemove;
  for (unsigned int i = 0; i < c.DMesons_pdgId_NOSYS->size(); i++) {
    int absId = std::abs(c.DMesons_pdgId_NOSYS->at(i));
    bool isCharmHadron = false;

    // Check if the current element is a charm hadron
    isCharmHadron =
        (absId == 411 || absId == 413 || absId == 421 || absId == 431);

    // Remove the element if it's not a D+, D*+, D0, or Ds+
    if (!isCharmHadron) {
      toRemove.push_back(i);
      continue;
    }

    // Apply simple selection to the rest
    if (particleSelections.find(absId) != particleSelections.end()) {
      // Call the corresponding pass function and remove if it fails
      if (!particleSelections[absId](c, i)) {
        toRemove.push_back(i);
      }
    }
  }

  // Sort the indices in descending order to avoid invalidation
  std::sort(toRemove.rbegin(), toRemove.rend());

  // Remove from all DMesons branches
  RemoveElements(*(c.DMesons_costhetastar_NOSYS), toRemove);
  RemoveElements(*(c.DMesons_D0Index_NOSYS), toRemove);
  RemoveElements(*(c.DMesons_daughterInfo__eta_NOSYS), toRemove);
  RemoveElements(*(c.DMesons_daughterInfo__passTight_NOSYS), toRemove);
  RemoveElements(*(c.DMesons_daughterInfo__pdgId_NOSYS), toRemove);
  RemoveElements(*(c.DMesons_daughterInfo__phi_NOSYS), toRemove);
  RemoveElements(*(c.DMesons_daughterInfo__pt_NOSYS), toRemove);
  RemoveElements(*(c.DMesons_daughterInfo__trackId_NOSYS), toRemove);
  RemoveElements(*(c.DMesons_daughterInfo__truthBarcode_NOSYS), toRemove);
  RemoveElements(*(c.DMesons_daughterInfo__truthDBarcode_NOSYS), toRemove);
  RemoveElements(*(c.DMesons_daughterInfo__z0SinTheta_NOSYS), toRemove);
  RemoveElements(*(c.DMesons_daughterInfo__z0SinThetaPV_NOSYS), toRemove);
  RemoveElements(*(c.DMesons_decayType_NOSYS), toRemove);
  RemoveElements(*(c.DMesons_DeltaMass_NOSYS), toRemove);
  RemoveElements(*(c.DMesons_eta_NOSYS), toRemove);
  RemoveElements(*(c.DMesons_fitOutput__Charge_NOSYS), toRemove);
  RemoveElements(*(c.DMesons_fitOutput__Chi2_NOSYS), toRemove);
  RemoveElements(*(c.DMesons_fitOutput__Impact_NOSYS), toRemove);
  RemoveElements(*(c.DMesons_fitOutput__ImpactSignificance_NOSYS), toRemove);
  RemoveElements(*(c.DMesons_fitOutput__ImpactZ0SinTheta_NOSYS), toRemove);
  RemoveElements(*(c.DMesons_fitOutput__Lxy_NOSYS), toRemove);
  RemoveElements(*(c.DMesons_fitOutput__VertexPosition_NOSYS), toRemove);
  RemoveElements(*(c.DMesons_m_NOSYS), toRemove);
  RemoveElements(*(c.DMesons_mKpi1_NOSYS), toRemove);
  RemoveElements(*(c.DMesons_mKpi2_NOSYS), toRemove);
  RemoveElements(*(c.DMesons_mPhi1_NOSYS), toRemove);
  RemoveElements(*(c.DMesons_mPhi2_NOSYS), toRemove);
  RemoveElements(*(c.DMesons_pdgId_NOSYS), toRemove);
  RemoveElements(*(c.DMesons_phi_NOSYS), toRemove);
  RemoveElements(*(c.DMesons_pt_NOSYS), toRemove);
  RemoveElements(*(c.DMesons_ptcone40_NOSYS), toRemove);
  RemoveElements(*(c.DMesons_SlowPionD0_NOSYS), toRemove);
  RemoveElements(*(c.DMesons_SlowPionZ0SinTheta_NOSYS), toRemove);
  RemoveElements(*(c.DMesons_truthBarcode_NOSYS), toRemove);

  // Update D0 indices
  UpdateIndices(*(c.DMesons_D0Index_NOSYS), toRemove);
}

int Run(const char* inputFile, const char* outputFile, bool nominalOnly) {
  // Open input file
  TFile* inFile = TFile::Open(inputFile);
  if (!inFile) {
    std::cerr << "ERROR: Failed to open input file: " << inputFile << std::endl;
    return 1;
  }

  // Get input tree
  TTree* inputTree = (TTree*)inFile->Get("CharmAnalysis");

  // Initialize the MakeClass
  CharmAnalysis c(inputTree);

  // Read only selected branches
  if (nominalOnly) {
    c.fChain->SetBranchStatus("*__1down*", 0);
    c.fChain->SetBranchStatus("*__1up*", 0);
    c.fChain->SetBranchStatus("*_GEN_*", 0);
    c.fChain->SetBranchStatus("*_JET_*", 0);
    c.fChain->SetBranchStatus("*_MET_SoftTrk_*", 0);
    c.fChain->SetBranchStatus("*_PROD_FRAC_*", 0);
  }

  // Remove all track-sys branches for now
  // TODO: implement proper handling of track-sys
  c.fChain->SetBranchStatus("*_TRK_*", 0);

  // Create a new output ROOT file
  TFile* outFile = TFile::Open(outputFile, "RECREATE");
  if (!outFile || outFile->IsZombie()) {
    std::cerr << "Error: Unable to create output ROOT file '" << outputFile
              << "'" << std::endl;
    inFile->Close();
    return 1;
  }

  // Copy all histograms directly located in the input ROOT file
  TIter next(inFile->GetListOfKeys());
  TKey* key;
  while ((key = (TKey*)next())) {
    TObject* obj = key->ReadObj();
    if (obj->IsA()->InheritsFrom("TH1")) {
      TH1* hist = (TH1*)obj->Clone();
      outFile->cd();
      hist->Write();
    }
  }

  // Create a new TTree in the output file
  TTree* outputTree = inputTree->CloneTree(0);
  if (c.fChain == 0) {
    return 1;
  }

  Long64_t nentries = c.fChain->GetEntriesFast();
  Long64_t nentries_removed = 0;
  Long64_t nbytes = 0, nb = 0;
  for (Long64_t jentry = 0; jentry < nentries; jentry++) {
    Long64_t ientry = c.LoadTree(jentry);
    if (ientry < 0) {
      throw std::runtime_error("Error loading tree");
    }

    // Read the current entry
    nb = c.fChain->GetEntry(jentry);
    nbytes += nb;

    // Remove charm hadrons other than D+ and D*+
    RemoveCharmHadrons(c);

    // Print out event number every 10000 events
    if (jentry > 0 && jentry % 10000 == 0) {
      std::cout << "Event " << jentry << std::endl;
    }

    // Remove events with zero charm hadrons
    if (c.DMesons_pdgId_NOSYS->size() == 0) {
      nentries_removed++;
      continue;
    }

    // Fill the new TTree
    outputTree->Fill();
  }

  // Write the output TTree to the output file
  outFile->cd();
  outputTree->Write();

  // Close the input and output files
  inFile->Close();
  outFile->Close();

  // Print out the number of removed events
  std::cout << "Total bytes read: " << nbytes << std::endl;
  std::cout << "Removed " << nentries_removed << " events." << std::endl;
  std::cout << "Relative fraction: "
            << (double)nentries_removed / (double)nentries << std::endl;

  // Weird deletions to avoid crashes
  c.fChain = nullptr;
  return 0;
}

int main(int argc, char* argv[]) {
  std::vector<std::string> arguments;

  // Start from index 1 to skip the program name (argv[0])
  for (int i = 1; i < argc; ++i) {
    std::string arg = argv[i];

    // Check for different command-line options
    if (arg == "-h" || arg == "--help") {
      // Display help message and exit
      std::cout << "Usage: " << argv[0] << " input_file output_file [options] "
                << std::endl;
      std::cout << "Options:" << std::endl;
      std::cout << "  -h, --help     Display this help message" << std::endl;
      std::cout << "  -n, --nominal-only     Copy only nominal events"
                << std::endl;
      return 0;
    } else {
      // Collect non-option arguments (e.g., file paths, other parameters)
      arguments.push_back(arg);
    }
  }

  if (arguments.size() < 2) {
    std::cerr << "Error: Please provide input and output file paths."
              << std::endl;
    return 1;
  }

  const char* inputFile = arguments[0].c_str();
  const char* outputFile = arguments[1].c_str();
  bool nominalOnly = arguments.size() > 2 &&
                     (arguments[2] == "-n" || arguments[2] == "--nominal-only");

  // Print out arguments
  std::cout << "Input file: " << inputFile << std::endl;
  std::cout << "Output file: " << outputFile << std::endl;
  std::cout << "Nominal only: " << (nominalOnly ? "true" : "false")
            << std::endl;

  // Run the analysis
  return Run(inputFile, outputFile, nominalOnly);
}
