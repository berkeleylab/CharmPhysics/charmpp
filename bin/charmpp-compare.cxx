#include <TFile.h>
#include <TH1.h>
#include <TKey.h>

#include <iostream>

bool AreHistogramsEqual(const TH1* h1, const TH1* h2) {
  // Check if the histograms have the same name and title
  if (std::string(h1->GetName()) != std::string(h2->GetName())) {
    std::cerr << "Histograms have different names." << std::endl;
    return false;
  }

  // Check if the histograms have the same number of bins
  if (h1->GetNbinsX() != h2->GetNbinsX()) {
    std::cerr << "Histograms have different number of bins." << std::endl;
    return false;
  }

  // Check if the bin contents are the same
  for (int bin = 1; bin <= h1->GetNbinsX(); ++bin) {
    if (h1->GetBinContent(bin) != h2->GetBinContent(bin)) {
      std::cerr << "Bin " << bin << " has different contents." << std::endl;
      return false;
    }
  }

  // If all checks pass, the histograms are considered equal
  return true;
}

bool AreRootFilesEqual(const char* file1, const char* file2) {
  // Open the two ROOT files
  TFile* inFile1 = TFile::Open(file1);
  TFile* inFile2 = TFile::Open(file2);

  if (!inFile1 || inFile1->IsZombie() || !inFile2 || inFile2->IsZombie()) {
    std::cerr << "Error: Failed to open input files." << std::endl;
    return false;
  }

  // Get the list of keys (histograms) in the first file
  TIter next1(inFile1->GetListOfKeys());
  TKey* key1;

  // Loop through the keys and compare histograms in both files
  int nHistogramsChecked = 0;
  while ((key1 = (TKey*)next1())) {
    TObject* obj1 = key1->ReadObj();

    // Check if the object is a histogram
    if (obj1->IsA()->InheritsFrom("TH1")) {
      const char* histName = key1->GetName();
      TH1* hist1 = (TH1*)obj1;

      // Try to retrieve the corresponding histogram in the second file
      TH1* hist2 = (TH1*)inFile2->Get(histName);

      // If a matching histogram is not found in the second file, they are not
      // equal
      if (!hist2) {
        std::cerr << "Histogram '" << histName
                  << "' not found in the second file." << std::endl;
        return false;
      }

      // Check if the histograms are equal
      if (!AreHistogramsEqual(hist1, hist2)) {
        std::cerr << "Histogram '" << histName
                  << "' is different in the two files." << std::endl;
        for (int bin = 1; bin <= hist1->GetNbinsX(); ++bin) {
          std::cerr << "  Bin " << bin << ": " << hist1->GetBinContent(bin)
                    << " vs. " << hist2->GetBinContent(bin) << std::endl;
        }
        return false;
      }

      // increment the number of histograms checked
      nHistogramsChecked++;
    }

    // break if the number of histograms checked is equal to 10,000
    if (nHistogramsChecked == 10000) {
      std::cout << "Checked 10,000 histograms. Stopping." << std::endl;
      break;
    }
  }

  std::cout << "All " << inFile1->GetListOfKeys()->GetEntries()
            << " histograms are equal in the two files." << std::endl;


  // Close files
  inFile1->Close();
  inFile2->Close();
  std::cout << "Files closed." << std::endl;

  // If all histograms are equal, the files are considered equal
  return true;
}

int main(int argc, char* argv[]) {
  if (argc != 3) {
    std::cerr << "Usage: " << argv[0] << " <file1.root> <file2.root>"
              << std::endl;
    return 1;
  }

  const char* file1 = argv[1];
  const char* file2 = argv[2];

  if (AreRootFilesEqual(file1, file2)) {
    std::cout << "The two ROOT files are equal." << std::endl;
    return 0;
  } else {
    std::cout << "The two ROOT files are not equal." << std::endl;
    return 1;
  }
}
