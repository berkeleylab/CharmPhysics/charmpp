#!/bin/bash

export CHARMPP_BUILD_PATH=/pscratch/sd/${USER:0:1}/$USER/build/charmpp

export CHARMPP_RUN_PATH=/pscratch/sd/${USER:0:1}/$USER/run/charmpp/v15

export CHARMPP_NTUPLE_PATH=/global/cfs/cdirs/atlas/wcharm/v15/merged

export CHARMPP_DATA_PATH=/global/cfs/cdirs/atlas/wcharm/charmpp_data

export CHARMPP_SOURCE_PATH=`pwd`

mkdir -p $CHARMPP_BUILD_PATH
mkdir -p $CHARMPP_RUN_PATH

export PATH=${CHARMPP_BUILD_PATH}/bin:$PATH
