import ROOT
import sys

# Check if a ROOT file path is provided as a command-line argument
if len(sys.argv) != 2:
    print("Usage: python script.py <input_root_file>")
    sys.exit(1)

# Open the ROOT file
input_file = sys.argv[1]
root_file = ROOT.TFile(input_file, "READ")

# Check if the file is valid
if not root_file.IsOpen():
    print(f"Error: Could not open file {input_file}")
    sys.exit(1)

# Access the "TruthTree" TTree
tree = root_file.Get("TruthTree")

# Check if the tree is valid
if not tree:
    print("Error: Could not access 'TruthTree' TTree in the ROOT file")
    sys.exit(1)

# Define the branch names to print
branches_to_print = [
    "TruthParticles_Selected_pdgId",
    "TruthParticles_Selected_barcode",
    "TruthParticles_Selected_status",
    "TruthParticles_Selected_fromBdecay",
    "TruthParticles_Selected_pt",
    "TruthParticles_Selected_eta",
    "TruthParticles_Selected_phi",
    "TruthParticles_Selected_vertexPosition",
    "TruthParticles_Selected_ImpactT",
    "TruthParticles_Selected_LxyT",
    "TruthParticles_Selected_cosThetaStarT",
    "TruthParticles_Selected_daughterInfoT__pdgId",
    "TruthParticles_Selected_daughterInfoT__barcode",
    "TruthParticles_Selected_daughterInfoT__pt",
    "TruthParticles_Selected_daughterInfoT__eta",
    "TruthParticles_Selected_daughterInfoT__phi",
]

# Iterate through entries and print the specified branch values
ientry = 0
for entry in tree:
    print(f"----- Entry {tree.GetReadEntry()} -----")
    nhadrons = len(entry.TruthParticles_Selected_barcode)
    print(f"Number of hadrons: {nhadrons}")
    for ihadron in range(nhadrons):
      print(f"----- Hadron {ihadron} -----")
      for branch_name in branches_to_print:
          branch_value = getattr(entry, branch_name)
          if branch_name == "TruthParticles_Selected_fromBdecay":
              print(f"{branch_name}: {'true' if branch_value[ihadron] else 'false'}")
          else:
              print(f"{branch_name}: {branch_value[ihadron]}")
    ientry += 1
    if ientry > 9:
        break

# Close the ROOT file
root_file.Close()
