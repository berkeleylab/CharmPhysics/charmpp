import argparse
import os
import requests
import yaml

def download_artifact(branch_name):
    # Define the path to .gitlab-ci.yml file relative to the script
    ci_yaml_path = os.path.join(os.path.dirname(__file__), "../.gitlab-ci.yml")

    if not os.path.exists(ci_yaml_path):
        print(f"{ci_yaml_path} does not exist.")
        return

    # Read and parse the .gitlab-ci.yml file
    with open(ci_yaml_path, 'r') as yaml_file:
        parsed_yaml = yaml.safe_load(yaml_file)

    for job_name, job_info in parsed_yaml.items():
        if job_name.startswith("run:"):
            variables = job_info["variables"]
            ANALYSIS_INPUT = variables["ANALYSIS_INPUT"]
            SYS_CONFIG = variables["SYS_CONFIG"]
            NEVNT = variables["NEVNT"]
            if len(job_name.split(":")) == 5:
                SYS_CONFIG = job_name.split(":")[-1]
            filename = ANALYSIS_INPUT.replace(".root", f".{SYS_CONFIG}.0.{NEVNT}.root")

            # Print job name and filename
            print(f"Job name: {job_name}")
            print(f"Filename: {filename}")

            # GitLab job artifacts URL
            base_url = "https://gitlab.cern.ch/berkeleylab/CharmPhysics/charmpp/-/jobs/artifacts/"
            job_url = f"{base_url}{branch_name}/raw/{filename}?job={job_name}"

            # Send a GET request to the job artifacts URL
            response = requests.get(job_url)

            if response.status_code == 200:
                # Specify the directory where you want to save the downloaded file
                save_dir = os.getcwd()

                # Make new dir with the branch name
                if not os.path.exists(os.path.join(save_dir, branch_name)):
                    os.mkdir(os.path.join(save_dir, branch_name))

                # Make new dir with the job name
                job_dir = job_name.replace(":", "_")
                save_dir = os.path.join(save_dir, branch_name, job_dir)
                if not os.path.exists(save_dir):
                    os.mkdir(save_dir)

                # Save the file to the specified directory
                with open(os.path.join(save_dir, filename), 'wb') as file:
                    file.write(response.content)
                    print(f"Successfully downloaded {filename}")
            else:
                print(f"Failed to download artifact. Status code: {response.status_code}")

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Download a GitLab job artifact and parse .gitlab-ci.yml")
    parser.add_argument("branch_name", help="Name of the branch")
    args = parser.parse_args()
    
    download_artifact(args.branch_name)
