#!/usr/bin/env python
import ROOT 

def set_under_over_flow(h):
    for x in range(0, h.GetNbinsX()+2):
        h.SetBinContent(x, 0, h.GetBinContent(x, 1))
        h.SetBinContent(x, h.GetNbinsY()+1, h.GetBinContent(x, h.GetNbinsY()))
    for y in range(1, h.GetNbinsY()+1):
        h.SetBinContent(0, y, h.GetBinContent(1, y))
        h.SetBinContent(h.GetNbinsX()+1, y, h.GetBinContent(h.GetNbinsX(), y))

rebin_pt = 5
rebin_eta = 2

outfile = ROOT.TFile("/global/cfs/cdirs/atlas/wcharm/SPG_systematics/new-histograms/track_eff_systematics.root", "RECREATE")
outfile.Close()

# std::map<int, std::string> pdgId_map = {
#     {+413, "999961"}, D*+
#     {+411, "999964"}, D+
#     {-413, "999971"}, D*-
#     {-411, "999974"}, D-
# };
for species in ["999961", "999964", "999971", "999974"]:

    reco_hist_name = "Dstar_RecoPtVsAbEta" if species in ["999961", "999971"] else "Dplus_RecoPtVsAbEta"
    truth_hist_name = reco_hist_name.replace("Reco", "Truth")

    f_nominal = ROOT.TFile(f"/global/cfs/cdirs/atlas/wcharm/SPG_systematics/new-histograms/{species}.ATLAS-R2-2016-01-00-01_FTFP.root", "READ")
    f_QGSP = ROOT.TFile(f"/global/cfs/cdirs/atlas/wcharm/SPG_systematics/new-histograms/{species}.ATLAS-R2-2016-01-00-01_QGSP.root", "READ")
    f_Overal = ROOT.TFile(f"/global/cfs/cdirs/atlas/wcharm/SPG_systematics/new-histograms/{species}.ATLAS-R2-2016-01-00-02_FTFP.root", "READ")
    f_IBL = ROOT.TFile(f"/global/cfs/cdirs/atlas/wcharm/SPG_systematics/new-histograms/{species}.ATLAS-R2-2016-01-00-03_FTFP.root", "READ")
    f_PP0 = ROOT.TFile(f"/global/cfs/cdirs/atlas/wcharm/SPG_systematics/new-histograms/{species}.ATLAS-R2-2016-01-00-04_FTFP.root", "READ")

    h_nominal = f_nominal.Get(reco_hist_name).Clone(f"{species}_nominal")
    h_QGSP = f_QGSP.Get(reco_hist_name).Clone(f"{species}_QGSP")
    h_IBL = f_IBL.Get(reco_hist_name).Clone(f"{species}_IBL")
    h_PP0 = f_PP0.Get(reco_hist_name).Clone(f"{species}_PP0")
    h_Overal = f_Overal.Get(reco_hist_name).Clone(f"{species}_Overal")

    h_nominal_truth = f_nominal.Get(truth_hist_name).Clone(f"{species}_nominal")
    h_QGSP_truth = f_QGSP.Get(truth_hist_name).Clone(f"{species}_QGSP")
    h_IBL_truth = f_IBL.Get(truth_hist_name).Clone(f"{species}_IBL")
    h_PP0_truth = f_PP0.Get(truth_hist_name).Clone(f"{species}_PP0")
    h_Overal_truth = f_Overal.Get(truth_hist_name).Clone(f"{species}_Overal")

    h_nominal = h_nominal.Rebin2D(rebin_eta, rebin_pt, f"{h_nominal.GetName()}_rebinned")
    h_QGSP = h_QGSP.Rebin2D(rebin_eta, rebin_pt, f"{h_QGSP.GetName()}_rebinned")
    h_IBL = h_IBL.Rebin2D(rebin_eta, rebin_pt, f"{h_IBL.GetName()}_rebinned")
    h_PP0 = h_PP0.Rebin2D(rebin_eta, rebin_pt, f"{h_PP0.GetName()}_rebinned")
    h_Overal = h_Overal.Rebin2D(rebin_eta, rebin_pt, f"{h_Overal.GetName()}_rebinned")

    h_nominal_truth = h_nominal_truth.Rebin2D(rebin_eta, rebin_pt, f"{h_nominal_truth.GetName()}_rebinned")
    h_QGSP_truth = h_QGSP_truth.Rebin2D(rebin_eta, rebin_pt, f"{h_QGSP_truth.GetName()}_rebinned")
    h_IBL_truth = h_IBL_truth.Rebin2D(rebin_eta, rebin_pt, f"{h_IBL_truth.GetName()}_rebinned")
    h_PP0_truth = h_PP0_truth.Rebin2D(rebin_eta, rebin_pt, f"{h_PP0_truth.GetName()}_rebinned")
    h_Overal_truth = h_Overal_truth.Rebin2D(rebin_eta, rebin_pt, f"{h_Overal_truth.GetName()}_rebinned")

    h_nominal.Divide(h_nominal_truth)
    h_QGSP.Divide(h_QGSP_truth)
    h_IBL.Divide(h_IBL_truth)
    h_PP0.Divide(h_PP0_truth)
    h_Overal.Divide(h_Overal_truth)

    h_QGSP.Divide(h_nominal)
    h_IBL.Divide(h_nominal)
    h_PP0.Divide(h_nominal)
    h_Overal.Divide(h_nominal)

    set_under_over_flow(h_QGSP)
    set_under_over_flow(h_IBL)
    set_under_over_flow(h_PP0)
    set_under_over_flow(h_Overal)

    h_QGSP.SetMaximum(1.3)
    h_IBL.SetMaximum(1.3)
    h_PP0.SetMaximum(1.3)
    h_Overal.SetMaximum(1.3)
    h_QGSP.SetMinimum(0.7)
    h_IBL.SetMinimum(0.7)
    h_PP0.SetMinimum(0.7)
    h_Overal.SetMinimum(0.7)

    outfile = ROOT.TFile("/global/cfs/cdirs/atlas/wcharm/SPG_systematics/new-histograms/track_eff_systematics.root", "UPDATE")
    outfile.cd()

    h_QGSP.Write()
    h_IBL.Write()
    h_PP0.Write()
    h_Overal.Write()
    outfile.Close()

dic = {
    "Dplus": 999964,
    "Dminus": 999974,
    "DstarPlus": 999961,
    "DstarMinus": 999971,
}

for species in ["Dplus", "Dminus", "DstarPlus", "DstarMinus"]:

    f = ROOT.TFile(f"/global/cfs/cdirs/atlas/shapiro/Charm-v10/macros/trackIPSys{species}.root", "READ")

    h_denominator = f.Get("TruthPtVsAbEta").Clone(f"TruthPtVsAbEta_{species}")
    h_NOSYS = f.Get("NOSYS_RecoPtVsAbEta").Clone(f"NOSYS_RecoPtVsAbEta_{species}")
    h_TRK_RES_D0_DEAD = f.Get("TRK_RES_D0_DEAD_RecoPtVsAbEta").Clone(f"{dic[species]}_TRK_RES_D0_DEAD")
    h_TRK_RES_D0_MEAS = f.Get("TRK_RES_D0_MEAS_RecoPtVsAbEta").Clone(f"{dic[species]}_TRK_RES_D0_MEAS")
    h_TRK_RES_Z0_DEAD = f.Get("TRK_RES_Z0_DEAD_RecoPtVsAbEta").Clone(f"{dic[species]}_TRK_RES_Z0_DEAD")
    h_TRK_RES_Z0_MEAS = f.Get("TRK_RES_Z0_MEAS_RecoPtVsAbEta").Clone(f"{dic[species]}_TRK_RES_Z0_MEAS")

    print(species)
    print(f"h_NOSYS {h_NOSYS.Integral() / h_denominator.Integral()}")
    print(f"h_TRK_RES_D0_DEAD {h_TRK_RES_D0_DEAD.Integral() / h_denominator.Integral()}")
    print(f"h_TRK_RES_D0_MEAS {h_TRK_RES_D0_MEAS.Integral() / h_denominator.Integral()}")
    print(f"h_TRK_RES_Z0_DEAD {h_TRK_RES_Z0_DEAD.Integral() / h_denominator.Integral()}")
    print(f"h_TRK_RES_Z0_MEAS {h_TRK_RES_Z0_MEAS.Integral() / h_denominator.Integral()}")

    h_denominator = h_denominator.Rebin2D(rebin_eta, rebin_pt, f"{h_denominator.GetName()}_rebinned")
    h_NOSYS = h_NOSYS.Rebin2D(rebin_eta, rebin_pt, f"{h_NOSYS.GetName()}_rebinned")
    h_TRK_RES_D0_DEAD = h_TRK_RES_D0_DEAD.Rebin2D(rebin_eta, rebin_pt, f"{h_TRK_RES_D0_DEAD.GetName()}_rebinned")
    h_TRK_RES_D0_MEAS = h_TRK_RES_D0_MEAS.Rebin2D(rebin_eta, rebin_pt, f"{h_TRK_RES_D0_MEAS.GetName()}_rebinned")
    h_TRK_RES_Z0_DEAD = h_TRK_RES_Z0_DEAD.Rebin2D(rebin_eta, rebin_pt, f"{h_TRK_RES_Z0_DEAD.GetName()}_rebinned")
    h_TRK_RES_Z0_MEAS = h_TRK_RES_Z0_MEAS.Rebin2D(rebin_eta, rebin_pt, f"{h_TRK_RES_Z0_MEAS.GetName()}_rebinned")

    h_NOSYS.Divide(h_denominator)
    h_TRK_RES_D0_DEAD.Divide(h_denominator)
    h_TRK_RES_D0_MEAS.Divide(h_denominator)
    h_TRK_RES_Z0_DEAD.Divide(h_denominator)
    h_TRK_RES_Z0_MEAS.Divide(h_denominator)

    h_TRK_RES_D0_DEAD.Divide(h_NOSYS)
    h_TRK_RES_D0_MEAS.Divide(h_NOSYS)
    h_TRK_RES_Z0_DEAD.Divide(h_NOSYS)
    h_TRK_RES_Z0_MEAS.Divide(h_NOSYS)

    set_under_over_flow(h_TRK_RES_D0_DEAD)
    set_under_over_flow(h_TRK_RES_D0_MEAS)
    set_under_over_flow(h_TRK_RES_Z0_DEAD)
    set_under_over_flow(h_TRK_RES_Z0_MEAS)

    h_TRK_RES_D0_DEAD.SetMaximum(1.2)
    h_TRK_RES_D0_MEAS.SetMaximum(1.2)
    h_TRK_RES_Z0_DEAD.SetMaximum(1.2)
    h_TRK_RES_Z0_MEAS.SetMaximum(1.2)
    h_TRK_RES_D0_DEAD.SetMinimum(0.8)
    h_TRK_RES_D0_MEAS.SetMinimum(0.8)
    h_TRK_RES_Z0_DEAD.SetMinimum(0.8)
    h_TRK_RES_Z0_MEAS.SetMinimum(0.8)

    outfile = ROOT.TFile("/global/cfs/cdirs/atlas/wcharm/SPG_systematics/new-histograms/track_eff_systematics.root", "UPDATE")
    outfile.cd()

    h_TRK_RES_D0_DEAD.Write()
    h_TRK_RES_D0_MEAS.Write()
    h_TRK_RES_Z0_DEAD.Write()
    h_TRK_RES_Z0_MEAS.Write()
    outfile.Close()

outfile.Close()
print(f"created sys file {outfile.GetName()}")
