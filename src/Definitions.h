#ifndef CHARM_DEFINITIONS
#define CHARM_DEFINITIONS

namespace Charm {

static const double GeV = 0.001;
static const double MeV = 1.0;

static const double mm = 1.0;
static const double mum = 1000;

static const double ELECTRON_MASS = 0.5109989461;
static const double MUON_MASS = 105.6583745;
static const double PION_MASS = 139.57018;
static const double PION0_MASS = 134.976;
static const double KAON_MASS = 493.677;
static const double PHI_MASS = 1019.461;
static const double ETA_MASS = 547.853;
static const double PROTON_MASS = 938.272;

static const double LUMI_2015 = 3244.54;
static const double LUMI_2016 = 33402.2;
static const double LUMI_2017 = 44630.6;
static const double LUMI_2018 = 58791.6;
static const double LUMI_RUN2 = 140068.94;

static const float D0_MASS = 1864.83;
static const float DP_MASS = 1870.;
static const float DS_MASS = 1968.30;
static const float D0_MASS_SAT = 1600.;
static const float DSTAR_MASS = 2010.27;

// zT bins
static const int NRANGES = 4;
static const int NBINSPERRANGE = 25;
static const double jetBinHighEdge[] = {20.0, 25.0, 50.0, 150.0};
static const double pTRelBinHighEdge[] = {
    0.3, 0.6, 0.9, 1.2, 1.5, 1.8, 2.1, 2.4, 2.7, 3.0, 3.3, 3.6, 3.9,
    4.2, 4.5, 4.8, 5.1, 5.4, 5.7, 6.0, 6.9, 7.8, 8.7, 9.6, 15.0};

static const std::vector<std::string> TRACK_JET_RADIUS = {"10", "8", "6"};

// reduced set of GEN variations
static const std::vector<std::string> GEN_VARIATIONS = {
  "GEN_muR_050_muF_050",
  "GEN_muR_050_muF_100",
  "GEN_muR_050_muF_200",
  "GEN_muR_100_muF_050",
  "GEN_muR_100_muF_200",
  "GEN_muR_200_muF_050",
  "GEN_muR_200_muF_100",
  "GEN_muR_200_muF_200",
  "GEN_muR_05_muF_05",
  "GEN_muR_05_muF_10",
  "GEN_muR_05_muF_20",
  "GEN_muR_10_muF_05",
  "GEN_muR_10_muF_20",
  "GEN_muR_20_muF_05",
  "GEN_muR_20_muF_10",
  "GEN_muR_20_muF_20",
  "GEN_Var3cDown",
  "GEN_Var3cUp",
  "GEN_isrmuRfac10_fsrmuRfac20",
  "GEN_isrmuRfac10_fsrmuRfac05",
  "GEN_MUR05_MUF05_PDF325100",
  "GEN_MUR05_MUF10_PDF325100",
  "GEN_MUR05_MUF20_PDF325100",
  "GEN_MUR10_MUF05_PDF325100",
  "GEN_MUR10_MUF10_PDF325100",
  "GEN_MUR10_MUF20_PDF325100",
  "GEN_MUR20_MUF05_PDF325100",
  "GEN_MUR20_MUF10_PDF325100",
  "GEN_MUR20_MUF20_PDF325100",
  "GEN_ME_ONLY_MUR05_MUF05_PDF303200_PSMUR05_PSMUF05",
  "GEN_ME_ONLY_MUR05_MUF1_PDF303200_PSMUR05_PSMUF1",
  "GEN_ME_ONLY_MUR1_MUF05_PDF303200_PSMUR1_PSMUF05",
  "GEN_ME_ONLY_MUR1_MUF1_PDF303200",
  "GEN_ME_ONLY_MUR1_MUF1_PDF303200_ASSEW",
  "GEN_ME_ONLY_MUR1_MUF1_PDF303200_EXPASSEW",
  "GEN_ME_ONLY_MUR1_MUF1_PDF303200_MULTIASSEW",
  "GEN_ME_ONLY_MUR1_MUF2_PDF303200_PSMUR1_PSMUF2",
  "GEN_ME_ONLY_MUR2_MUF1_PDF303200_PSMUR2_PSMUF1",
  "GEN_ME_ONLY_MUR2_MUF2_PDF303200_PSMUR2_PSMUF2",
  "GEN_MUR05_MUF05_PDF303200_PSMUR05_PSMUF05",
  "GEN_MUR05_MUF1_PDF303200_PSMUR05_PSMUF1",
  "GEN_MUR1_MUF05_PDF303200_PSMUR1_PSMUF05",
  "GEN_MUR1_MUF1_PDF303200",
  "GEN_MUR1_MUF1_PDF303200_ASSEW",
  "GEN_MUR1_MUF1_PDF303200_EXPASSEW",
  "GEN_MUR1_MUF1_PDF303200_MULTIASSEW",
  "GEN_MUR1_MUF2_PDF303200_PSMUR1_PSMUF2",
  "GEN_MUR2_MUF1_PDF303200_PSMUR2_PSMUF1",
  "GEN_MUR2_MUF2_PDF303200_PSMUR2_PSMUF2",
};

static const std::vector<std::string> TRACK_CONTAINER_VARIATIONS = {
  "",
  "TRK_BIAS_D0_WM",
  "TRK_BIAS_QOVERP_SAGITTA_WM",
  "TRK_BIAS_Z0_WM",
  "TRK_EFF_LOOSE_GLOBAL",
  "TRK_EFF_LOOSE_IBL",
  "TRK_EFF_LOOSE_PHYSMODEL",
  "TRK_EFF_LOOSE_PP0",
  "TRK_FAKE_RATE_LOOSE",
  "TRK_RES_D0_DEAD",
  "TRK_RES_D0_MEAS",
  "TRK_RES_Z0_DEAD",
  "TRK_RES_Z0_MEAS",
};
}  // namespace Charm

#endif  // CHARM_DEFINITIONS
