#include <math.h>

#include <iostream>

#include "CharmLoopBase.h"
#include "HelperFunctions.h"
#include "TVector2.h"

namespace Charm {

void CharmLoopBase::connect_output_tree_branches() {
  // connect D meson branches
  DMesonRec::connect_output_tree_branches(this, m_is_mc, m_track_jet_selection,
                                          m_extended_output_branches,
                                          m_track_jet_radius);
}

void CharmLoopBase::connect_branches() {
  if (m_do_PV) {
    // vertex variables
    add_presel_br("CharmEventInfo_PV_X", &m_CharmEventInfo_PV_X);
    add_presel_br("CharmEventInfo_PV_Y", &m_CharmEventInfo_PV_Y);
    add_presel_br("CharmEventInfo_PV_Z", &m_CharmEventInfo_PV_Z);
  }

  // TODO: make this dirty code nicer
  // need event weight even in some cases without lepton stuff
  if (!m_do_lep_presel) {
    if (m_is_mc && !m_no_weights) {
      add_presel_br("EventInfo_generatorWeight_NOSYS",
                    &m_EventInfo_generatorWeight_NOSYS);
    } else if (m_data_period == "ForcedDecay") {
      add_presel_br("CharmEventInfo_EventWeight",
                    &m_EventInfo_generatorWeight_NOSYS);
    }
    return;
  }

  if (m_do_lep_presel) {
    // event info
    add_presel_br("METInfo_MET_NOSYS", &m_METInfo_MET_NOSYS);
    add_presel_br("METInfo_METPhi_NOSYS", &m_METInfo_METPhi_NOSYS);
    if (m_use_anti_tight_met &&
        ((m_eval_fake_rate || m_fake_rate_measurement) && !m_is_mc)) {
      add_presel_br("METInfoAntiTight_NOSYS_MET",
                    &m_METInfoAntiTight_NOSYS_MET);
      add_presel_br("METInfoAntiTight_NOSYS_METPhi",
                    &m_METInfoAntiTight_NOSYS_METPhi);
    }
    add_presel_br("EventInfo_trigPassed_HLT_e24_lhmedium_L1EM20VH",
                  &m_EventInfo_trigPassed_HLT_e24_lhmedium_L1EM20VH);
    add_presel_br("EventInfo_trigPassed_HLT_e60_lhmedium",
                  &m_EventInfo_trigPassed_HLT_e60_lhmedium);
    add_presel_br("EventInfo_trigPassed_HLT_e120_lhloose",
                  &m_EventInfo_trigPassed_HLT_e120_lhloose);
    add_presel_br("EventInfo_trigPassed_HLT_e26_lhtight_nod0_ivarloose",
                  &m_EventInfo_trigPassed_HLT_e26_lhtight_nod0_ivarloose);
    add_presel_br("EventInfo_trigPassed_HLT_e60_lhmedium_nod0",
                  &m_EventInfo_trigPassed_HLT_e60_lhmedium_nod0);
    add_presel_br("EventInfo_trigPassed_HLT_e140_lhloose_nod0",
                  &m_EventInfo_trigPassed_HLT_e140_lhloose_nod0);
    add_presel_br("EventInfo_trigPassed_HLT_mu20_iloose_L1MU15",
                  &m_EventInfo_trigPassed_HLT_mu20_iloose_L1MU15);
    add_presel_br("EventInfo_trigPassed_HLT_mu26_ivarmedium",
                  &m_EventInfo_trigPassed_HLT_mu26_ivarmedium);
    add_presel_br("EventInfo_trigPassed_HLT_mu50",
                  &m_EventInfo_trigPassed_HLT_mu50);
    add_presel_br(Charm::get_run_number_string(m_is_mc),
                  &m_EventInfo_runNumber);
    add_presel_br(Charm::get_pileup_string(m_is_mc, m_mc_period, m_data_period),
                  &m_EventInfo_correctedScaled_averageInteractionsPerCrossing);
    add_presel_br("EventInfo_eventNumber", &m_EventInfo_eventNumber);

    // muons
    add_presel_br("AnalysisMuons_pt_NOSYS", &m_AnalysisMuons_pt_NOSYS);
    add_presel_br("AnalysisMuons_eta", &m_AnalysisMuons_eta);
    add_presel_br("AnalysisMuons_phi", &m_AnalysisMuons_phi);
    add_presel_br("AnalysisMuons_charge", &m_AnalysisMuons_charge);
    add_presel_br("AnalysisMuons_mu_selected_NOSYS",
                  &m_AnalysisMuons_mu_selected_NOSYS);
    add_presel_br("AnalysisMuons_isQuality_Tight_NOSYS",
                  &m_AnalysisMuons_isQuality_Tight_NOSYS);
    add_presel_br("AnalysisMuons_isIsolated_PflowTight_VarRad_NOSYS",
                  &m_AnalysisMuons_isIsolated_PflowTight_VarRad_NOSYS);
    add_presel_br("AnalysisMuons_matched_HLT_mu20_iloose_L1MU15",
                  &m_AnalysisMuons_matched_HLT_mu20_iloose_L1MU15);
    add_presel_br("AnalysisMuons_matched_HLT_mu26_ivarmedium",
                  &m_AnalysisMuons_matched_HLT_mu26_ivarmedium);
    add_presel_br("AnalysisMuons_matched_HLT_mu50",
                  &m_AnalysisMuons_matched_HLT_mu50);
    add_presel_br("AnalysisMuons_d0", &m_AnalysisMuons_d0);
    add_presel_br("AnalysisMuons_d0sig", &m_AnalysisMuons_d0sig);
    add_presel_br("AnalysisMuons_is_bad", &m_AnalysisMuons_is_bad);
    add_presel_br("AnalysisMuons_ptvarcone30_TightTTVA_pt500",
                  &m_AnalysisMuons_ptvarcone30_TightTTVA_pt500);
    add_presel_br("AnalysisMuons_neflowisol20", &m_AnalysisMuons_neflowisol20);
    add_presel_br("AnalysisMuons_topoetcone20", &m_AnalysisMuons_topoetcone20);
    if (m_do_extra_histograms) {
      add_presel_br("AnalysisMuons_z0sinTheta", &m_AnalysisMuons_z0sinTheta);
      add_presel_br("AnalysisMuons_ptvarcone30_TightTTVA_pt1000",
                    &m_AnalysisMuons_ptvarcone30_TightTTVA_pt1000);
    }

    // electrons
    add_presel_br("AnalysisElectrons_pt_NOSYS", &m_AnalysisElectrons_pt_NOSYS);
    add_presel_br("AnalysisElectrons_eta", &m_AnalysisElectrons_eta);
    add_presel_br("AnalysisElectrons_caloCluster_eta",
                  &m_AnalysisElectrons_caloCluster_eta);
    add_presel_br("AnalysisElectrons_phi", &m_AnalysisElectrons_phi);
    add_presel_br("AnalysisElectrons_charge", &m_AnalysisElectrons_charge);
    add_presel_br("AnalysisElectrons_likelihood_Tight",
                  &m_AnalysisElectrons_likelihood_Tight);
    add_presel_br("AnalysisElectrons_el_selected_NOSYS",
                  &m_AnalysisElectrons_el_selected_NOSYS);
    add_presel_br("AnalysisElectrons_isIsolated_Tight_VarRad_NOSYS",
                  &m_AnalysisElectrons_isIsolated_Tight_VarRad_NOSYS);
    add_presel_br("AnalysisElectrons_matched_HLT_e24_lhmedium_L1EM20VH",
                  &m_AnalysisElectrons_matched_HLT_e24_lhmedium_L1EM20VH);
    add_presel_br("AnalysisElectrons_matched_HLT_e60_lhmedium",
                  &m_AnalysisElectrons_matched_HLT_e60_lhmedium);
    add_presel_br("AnalysisElectrons_matched_HLT_e120_lhloose",
                  &m_AnalysisElectrons_matched_HLT_e120_lhloose);
    add_presel_br("AnalysisElectrons_matched_HLT_e26_lhtight_nod0_ivarloose",
                  &m_AnalysisElectrons_matched_HLT_e26_lhtight_nod0_ivarloose);
    add_presel_br("AnalysisElectrons_matched_HLT_e60_lhmedium_nod0",
                  &m_AnalysisElectrons_matched_HLT_e60_lhmedium_nod0);
    add_presel_br("AnalysisElectrons_matched_HLT_e140_lhloose_nod0",
                  &m_AnalysisElectrons_matched_HLT_e140_lhloose_nod0);
    add_presel_br("AnalysisElectrons_d0", &m_AnalysisElectrons_d0);
    add_presel_br("AnalysisElectrons_d0sig", &m_AnalysisElectrons_d0sig);
    add_presel_br("AnalysisElectrons_ptvarcone20_TightTTVA_pt1000",
                  &m_AnalysisElectrons_ptvarcone20_TightTTVA_pt1000);
    add_presel_br("AnalysisElectrons_topoetcone20",
                  &m_AnalysisElectrons_topoetcone20);
    if (m_do_extra_histograms) {
      add_presel_br("AnalysisElectrons_z0sinTheta",
                    &m_AnalysisElectrons_z0sinTheta);
      add_presel_br("AnalysisElectrons_ptvarcone30_TightTTVA_pt1000",
                    &m_AnalysisElectrons_ptvarcone30_TightTTVA_pt1000);
      // add_presel_br("AnalysisElectrons_EOverP", &m_AnalysisElectrons_EOverP);
    }

    // mc only
    if (m_is_mc) {
      add_presel_br("EventInfo_generatorWeight_NOSYS",
                    &m_EventInfo_generatorWeight_NOSYS);
      add_presel_br("EventInfo_PileupWeight_NOSYS",
                    &m_EventInfo_PileupWeight_NOSYS);
      add_presel_br("EventInfo_jvt_effSF_NOSYS", &m_EventInfo_jvt_effSF_NOSYS);
      // add_presel_br("EventInfo_fjvt_effSF_NOSYS",
      // &m_EventInfo_fjvt_effSF_NOSYS);
      add_presel_br("EventInfo_prodFracWeight_NOSYS",
                    &m_EventInfo_prodFracWeight_NOSYS);
      add_presel_br("CharmEventInfo_TopWeight", &m_CharmEventInfo_TopWeight);
      add_presel_br("AnalysisElectrons_effSF_NOSYS",
                    &m_AnalysisElectrons_effSF_NOSYS);
      add_presel_br("AnalysisElectrons_effSF_ID_Tight_NOSYS",
                    &m_AnalysisElectrons_effSF_ID_Tight_NOSYS);
      add_presel_br("AnalysisElectrons_effSF_Isol_Tight_Tight_VarRad_NOSYS",
                    &m_AnalysisElectrons_effSF_Isol_Tight_Tight_VarRad_NOSYS);
      add_presel_br(
          "AnalysisElectrons_effSF_Trig_Tight_Tight_VarRad_SINGLE_E_2015_e24_"
          "lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_"
          "lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_"
          "NOSYS",
          &m_AnalysisElectrons_effSF_Trig_Tight_Tight_VarRad_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_NOSYS);
      add_presel_br(
          "AnalysisElectrons_effSF_Trig_Tight_noIso_SINGLE_E_2015_e24_lhmedium_"
          "L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_"
          "ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_NOSYS",
          &m_AnalysisElectrons_effSF_Trig_Tight_noIso_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_NOSYS);
      add_presel_br("AnalysisElectrons_effSF_Chflip_Tight_Tight_VarRad_NOSYS",
                    &m_AnalysisElectrons_effSF_Chflip_Tight_Tight_VarRad_NOSYS);
      add_presel_br("AnalysisElectrons_truthType",
                    &m_AnalysisElectrons_truthType);
      add_presel_br("AnalysisElectrons_truthOrigin",
                    &m_AnalysisElectrons_truthOrigin);
      add_presel_br("AnalysisElectrons_firstEgMotherTruthType",
                    &m_AnalysisElectrons_firstEgMotherTruthType);
      add_presel_br("AnalysisElectrons_firstEgMotherTruthOrigin",
                    &m_AnalysisElectrons_firstEgMotherTruthOrigin);
      add_presel_br("AnalysisElectrons_firstEgMotherPdgId",
                    &m_AnalysisElectrons_firstEgMotherPdgId);
      add_presel_br("AnalysisMuons_muon_effSF_Quality_Tight_NOSYS",
                    &m_AnalysisMuons_mu_effSF_Quality_Tight_NOSYS);
      add_presel_br("AnalysisMuons_muon_effSF_TTVA_NOSYS",
                    &m_AnalysisMuons_mu_effSF_TTVA_NOSYS);
      add_presel_br("AnalysisMuons_muon_effSF_Isol_PflowTight_VarRad_NOSYS",
                    &m_AnalysisMuons_mu_effSF_Isol_PflowTight_VarRad_NOSYS);
      add_presel_br(
          "AnalysisMuons_muon_effMC_Trig_Tight_HLT_mu26_ivarmedium_OR_HLT_mu50_"
          "NOSYS",
          &m_AnalysisMuons_mu_effMC_Trig_Tight_HLT_mu26_ivarmedium_OR_HLT_mu50_NOSYS);
      add_presel_br(
          "AnalysisMuons_muon_effData_Trig_Tight_HLT_mu26_ivarmedium_OR_HLT_"
          "mu50_"
          "NOSYS",
          &m_AnalysisMuons_mu_effData_Trig_Tight_HLT_mu26_ivarmedium_OR_HLT_mu50_NOSYS);
      add_presel_br(
          "AnalysisMuons_muon_effMC_Trig_Tight_HLT_mu20_iloose_L1MU15_OR_HLT_"
          "mu50_NOSYS",
          &m_AnalysisMuons_mu_effMC_Trig_Tight_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_NOSYS);
      add_presel_br(
          "AnalysisMuons_muon_effData_Trig_Tight_HLT_mu20_iloose_L1MU15_OR_HLT_"
          "mu50_NOSYS",
          &m_AnalysisMuons_mu_effData_Trig_Tight_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_NOSYS);
      add_presel_br("AnalysisMuons_truthType", &m_AnalysisMuons_truthType);
      add_presel_br("AnalysisMuons_truthOrigin", &m_AnalysisMuons_truthOrigin);

      // sys branches
      if (m_do_systematics) {
        // event weights
        init_sys_br(&m_sys_br_GEN, "EventInfo_generatorWeight_%s");
        init_sys_br(&m_sys_br_PU, "EventInfo_PileupWeight_%s");
        init_sys_br(&m_sys_br_JVT, "EventInfo_jvt_effSF_%s");
        // init_sys_br(&m_sys_br_FTAG,
        // "EventInfo_ftag_effSF_DL1r_FixedCutBEff_70_%s");
        init_sys_br(&m_sys_br_TOP, "CharmEventInfo_TopWeight_%s");
        init_sys_br(&m_sys_br_PROD_FRAC, "EventInfo_prodFracWeight_%s");

        // met
        init_sys_br(&m_sys_br_MET_met, "METInfo_MET_%s");
        init_sys_br(&m_sys_br_MET_phi, "METInfo_METPhi_%s");

        // electron sys branches
        init_sys_br(&m_sys_br_el_pt, "AnalysisElectrons_pt_%s");
        init_sys_br(&m_sys_br_el_selected, "AnalysisElectrons_el_selected_%s");
        init_sys_br(&m_sys_br_el_isIsolated_Tight_VarRad,
                    "AnalysisElectrons_isIsolated_Tight_VarRad_%s");
        init_sys_br(&m_sys_br_el_reco_eff, "AnalysisElectrons_effSF_%s");
        init_sys_br(&m_sys_br_el_id_eff, "AnalysisElectrons_effSF_ID_Tight_%s");
        init_sys_br(&m_sys_br_el_iso_eff,
                    "AnalysisElectrons_effSF_Isol_Tight_Tight_VarRad_%s");
        init_sys_br(&m_sys_br_el_charge_eff,
                    "AnalysisElectrons_effSF_Chflip_Tight_Tight_VarRad_%s");
        init_sys_br(&m_sys_br_el_trig_sf,
                    "AnalysisElectrons_effSF_Trig_Tight_Tight_VarRad_SINGLE_E_"
                    "2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_"
                    "lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_"
                    "lhmedium_nod0_OR_e140_lhloose_nod0_%s",
                    {"EL_EFF_TriggerEff_TOTAL_1NPCOR_PLUS_UNCOR__1up",
                     "EL_EFF_TriggerEff_TOTAL_1NPCOR_PLUS_UNCOR__1down"});
        init_sys_br(&m_sys_br_el_trig_noiso_sf,
                    "AnalysisElectrons_effSF_Trig_Tight_noIso_SINGLE_E_2015_"
                    "e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_"
                    "2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_"
                    "OR_e140_lhloose_nod0_%s",
                    {"EL_EFF_TriggerEff_TOTAL_1NPCOR_PLUS_UNCOR__1up",
                     "EL_EFF_TriggerEff_TOTAL_1NPCOR_PLUS_UNCOR__1down"});
        init_sys_br(&m_sys_br_el_calib_reco_eff, "AnalysisElectrons_effSF_%s");
        init_sys_br(&m_sys_br_el_calib_id_eff,
                    "AnalysisElectrons_effSF_ID_Tight_%s");
        init_sys_br(&m_sys_br_el_calib_iso_eff,
                    "AnalysisElectrons_effSF_Isol_Tight_Tight_VarRad_%s");
        init_sys_br(&m_sys_br_el_calib_charge_eff,
                    "AnalysisElectrons_effSF_Chflip_Tight_Tight_VarRad_%s");
        init_sys_br(&m_sys_br_el_calib_trig_sf,
                    "AnalysisElectrons_effSF_Trig_Tight_Tight_VarRad_SINGLE_E_"
                    "2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_"
                    "lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_"
                    "lhmedium_nod0_OR_e140_lhloose_nod0_%s");
        init_sys_br(&m_sys_br_el_calib_trig_noiso_sf,
                    "AnalysisElectrons_effSF_Trig_Tight_noIso_SINGLE_E_2015_"
                    "e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_"
                    "2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_"
                    "OR_e140_lhloose_nod0_%s");

        // muon sys branches
        init_sys_br(&m_sys_br_mu_pt, "AnalysisMuons_pt_%s");
        init_sys_br(&m_sys_br_mu_selected, "AnalysisMuons_mu_selected_%s");
        init_sys_br(&m_sys_br_mu_isQuality_Tight,
                    "AnalysisMuons_isQuality_Tight_%s");
        init_sys_br(&m_sys_br_mu_isIsolated_PflowTight_VarRad,
                    "AnalysisMuons_isIsolated_PflowTight_VarRad_%s");
        init_sys_br(&m_sys_br_mu_quality_eff,
                    "AnalysisMuons_muon_effSF_Quality_Tight_%s");
        init_sys_br(&m_sys_br_mu_ttva_eff, "AnalysisMuons_muon_effSF_TTVA_%s");
        init_sys_br(&m_sys_br_mu_iso_eff,
                    "AnalysisMuons_muon_effSF_Isol_PflowTight_VarRad_%s");
        init_sys_br(
            &m_sys_br_mu_trig_2015_eff_mc,
            "AnalysisMuons_muon_effMC_Trig_Tight_HLT_mu20_iloose_L1MU15_"
            "OR_HLT_mu50_%s");
        init_sys_br(&m_sys_br_mu_trig_2015_eff_data,
                    "AnalysisMuons_muon_effData_Trig_Tight_HLT_mu20_iloose_"
                    "L1MU15_OR_HLT_mu50_%s");
        init_sys_br(
            &m_sys_br_mu_trig_2018_eff_mc,
            "AnalysisMuons_muon_effMC_Trig_Tight_HLT_mu26_ivarmedium_OR_"
            "HLT_mu50_%s");
        init_sys_br(&m_sys_br_mu_trig_2018_eff_data,
                    "AnalysisMuons_muon_effData_Trig_Tight_HLT_mu26_ivarmedium_"
                    "OR_HLT_mu50_%s");
        init_sys_br(&m_sys_br_mu_calib_quality_eff,
                    "AnalysisMuons_muon_effSF_Quality_Tight_%s");
        init_sys_br(&m_sys_br_mu_calib_ttva_eff,
                    "AnalysisMuons_muon_effSF_TTVA_%s");
        init_sys_br(&m_sys_br_mu_calib_iso_eff,
                    "AnalysisMuons_muon_effSF_Isol_PflowTight_VarRad_%s");
        init_sys_br(
            &m_sys_br_mu_calib_trig_2015_eff_mc,
            "AnalysisMuons_muon_effMC_Trig_Tight_HLT_mu20_iloose_L1MU15_"
            "OR_HLT_mu50_%s");
        init_sys_br(&m_sys_br_mu_calib_trig_2015_eff_data,
                    "AnalysisMuons_muon_effData_Trig_Tight_HLT_mu20_iloose_"
                    "L1MU15_OR_HLT_mu50_%s");
        init_sys_br(
            &m_sys_br_mu_calib_trig_2018_eff_mc,
            "AnalysisMuons_muon_effMC_Trig_Tight_HLT_mu26_ivarmedium_OR_"
            "HLT_mu50_%s");
        init_sys_br(&m_sys_br_mu_calib_trig_2018_eff_data,
                    "AnalysisMuons_muon_effData_Trig_Tight_HLT_mu26_ivarmedium_"
                    "OR_HLT_mu50_%s");

        // jet sys branches
        init_sys_br(&m_sys_br_jet_pt, "AnalysisJets_pt_%s");
        init_sys_br(&m_sys_br_jet_selected, "AnalysisJets_jet_selected_%s");
        init_sys_br(&m_sys_br_jet_ftag_eff,
                    "AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_%s");
        init_sys_br(&m_sys_br_jet_calib_ftag_eff,
                    "AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_%s");
      }
    }
  }

  // jets
  if (m_jet_selection || m_dmeson_jet_matching) {
    add_presel_br("AnalysisJets_pt_NOSYS", &m_AnalysisJets_pt_NOSYS);
    add_presel_br("AnalysisJets_eta", &m_AnalysisJets_eta);
    add_presel_br("AnalysisJets_phi", &m_AnalysisJets_phi);
    add_presel_br("AnalysisJets_m", &m_AnalysisJets_m);
    add_presel_br("AnalysisJets_jet_selected_NOSYS",
                  &m_AnalysisJets_jet_selected_NOSYS);
    add_presel_br("AnalysisJets_ftag_select_DL1r_FixedCutBEff_70",
                  &m_AnalysisJets_ftag_select_DL1r_FixedCutBEff_70);
    if (m_is_mc) {
      add_presel_br("AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_NOSYS",
                    &m_AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_NOSYS);
      add_presel_br("AnalysisJets_HadronConeExclTruthLabelID", &m_AnalysisJets_HadronConeExclTruthLabelID);
    }
  }

  // track-jets
  if (m_track_jet_selection) {
    std::string sys_name =
        m_track_sys_name.empty() ? "NOSYS" : m_track_sys_name;
    add_presel_br(Form("AntiKt%sPV0TrackJets_pt_%s", m_track_jet_radius.c_str(),
                       sys_name.c_str()),
                  &m_AntiKtPV0TrackJets_pt[m_track_jet_radius]);
    add_presel_br(Form("AntiKt%sPV0TrackJets_eta_%s",
                       m_track_jet_radius.c_str(), sys_name.c_str()),
                  &m_AntiKtPV0TrackJets_eta[m_track_jet_radius]);
    add_presel_br(Form("AntiKt%sPV0TrackJets_phi_%s",
                       m_track_jet_radius.c_str(), sys_name.c_str()),
                  &m_AntiKtPV0TrackJets_phi[m_track_jet_radius]);
    add_presel_br(Form("AntiKt%sPV0TrackJets_m_%s", m_track_jet_radius.c_str(),
                       sys_name.c_str()),
                  &m_AntiKtPV0TrackJets_m[m_track_jet_radius]);
    add_presel_br(Form("AntiKt%sPV0TrackJets_daughter__pt_%s",
                       m_track_jet_radius.c_str(), sys_name.c_str()),
                  &m_AntiKtPV0TrackJets_daughter__pt[m_track_jet_radius]);
    add_presel_br(Form("AntiKt%sPV0TrackJets_daughter__eta_%s",
                       m_track_jet_radius.c_str(), sys_name.c_str()),
                  &m_AntiKtPV0TrackJets_daughter__eta[m_track_jet_radius]);
    add_presel_br(Form("AntiKt%sPV0TrackJets_daughter__phi_%s",
                       m_track_jet_radius.c_str(), sys_name.c_str()),
                  &m_AntiKtPV0TrackJets_daughter__phi[m_track_jet_radius]);
    add_presel_br(Form("AntiKt%sPV0TrackJets_daughter__trackId_%s",
                       m_track_jet_radius.c_str(), sys_name.c_str()),
                  &m_AntiKtPV0TrackJets_daughter__trackId[m_track_jet_radius]);
  }
}

void CharmLoopBase::read_sys_br() {
  // read electrons
  m_sys_el_iso = get_val_sys(m_AnalysisElectrons_isIsolated_Tight_VarRad_NOSYS,
                             &m_sys_br_el_isIsolated_Tight_VarRad);
  m_sys_el_pt = get_val_sys(m_AnalysisElectrons_pt_NOSYS, &m_sys_br_el_pt);
  m_sys_el_reco_eff =
      get_val_sys(m_AnalysisElectrons_effSF_NOSYS, &m_sys_br_el_reco_eff,
                  &m_sys_br_el_calib_reco_eff);
  m_sys_el_id_eff = get_val_sys(m_AnalysisElectrons_effSF_ID_Tight_NOSYS,
                                &m_sys_br_el_id_eff, &m_sys_br_el_calib_id_eff);
  m_sys_el_iso_eff =
      get_val_sys(m_AnalysisElectrons_effSF_Isol_Tight_Tight_VarRad_NOSYS,
                  &m_sys_br_el_iso_eff, &m_sys_br_el_calib_iso_eff);
  m_sys_el_trig_sf = get_val_sys(
      m_AnalysisElectrons_effSF_Trig_Tight_Tight_VarRad_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_NOSYS,
      &m_sys_br_el_trig_sf, &m_sys_br_el_calib_trig_sf);
  m_sys_el_trig_noiso_sf = get_val_sys(
      m_AnalysisElectrons_effSF_Trig_Tight_noIso_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_NOSYS,
      &m_sys_br_el_trig_noiso_sf, &m_sys_br_el_calib_trig_noiso_sf);
  m_sys_el_charge_sf =
      get_val_sys(m_AnalysisElectrons_effSF_Chflip_Tight_Tight_VarRad_NOSYS,
                  &m_sys_br_el_charge_eff, &m_sys_br_el_calib_charge_eff);

  // read muons
  m_sys_mu_pt = get_val_sys(m_AnalysisMuons_pt_NOSYS, &m_sys_br_mu_pt);
  m_sys_mu_quality = get_val_sys(m_AnalysisMuons_isQuality_Tight_NOSYS,
                                 &m_sys_br_mu_isQuality_Tight);
  m_sys_mu_iso = get_val_sys(m_AnalysisMuons_isIsolated_PflowTight_VarRad_NOSYS,
                             &m_sys_br_mu_isIsolated_PflowTight_VarRad);
  m_sys_mu_quality_eff =
      get_val_sys(m_AnalysisMuons_mu_effSF_Quality_Tight_NOSYS,
                  &m_sys_br_mu_quality_eff, &m_sys_br_mu_calib_quality_eff);
  m_sys_mu_ttva_eff =
      get_val_sys(m_AnalysisMuons_mu_effSF_TTVA_NOSYS, &m_sys_br_mu_ttva_eff,
                  &m_sys_br_mu_calib_ttva_eff);
  m_sys_mu_iso_eff =
      get_val_sys(m_AnalysisMuons_mu_effSF_Isol_PflowTight_VarRad_NOSYS,
                  &m_sys_br_mu_iso_eff, &m_sys_br_mu_calib_iso_eff);
  m_sys_mu_trig_2015_eff_mc = get_val_sys(
      m_AnalysisMuons_mu_effMC_Trig_Tight_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_NOSYS,
      &m_sys_br_mu_trig_2015_eff_mc, &m_sys_br_mu_calib_trig_2015_eff_mc);
  m_sys_mu_trig_2015_eff_data = get_val_sys(
      m_AnalysisMuons_mu_effData_Trig_Tight_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_NOSYS,
      &m_sys_br_mu_trig_2015_eff_data, &m_sys_br_mu_calib_trig_2015_eff_data);
  m_sys_mu_trig_2018_eff_mc = get_val_sys(
      m_AnalysisMuons_mu_effMC_Trig_Tight_HLT_mu26_ivarmedium_OR_HLT_mu50_NOSYS,
      &m_sys_br_mu_trig_2018_eff_mc, &m_sys_br_mu_calib_trig_2018_eff_mc);
  m_sys_mu_trig_2018_eff_data = get_val_sys(
      m_AnalysisMuons_mu_effData_Trig_Tight_HLT_mu26_ivarmedium_OR_HLT_mu50_NOSYS,
      &m_sys_br_mu_trig_2018_eff_data, &m_sys_br_mu_calib_trig_2018_eff_data);

  // read jets
  m_sys_jet_pt = get_val_sys(m_AnalysisJets_pt_NOSYS, &m_sys_br_jet_pt);
  m_sys_jet_ftag_eff =
      get_val_sys(m_AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_NOSYS,
                  &m_sys_br_jet_ftag_eff, &m_sys_br_jet_calib_ftag_eff);

  // branches for selecting objects
  m_sys_el_selected =
      get_val_sys(m_AnalysisElectrons_el_selected_NOSYS, &m_sys_br_el_selected);
  m_sys_mu_selected =
      get_val_sys(m_AnalysisMuons_mu_selected_NOSYS, &m_sys_br_mu_selected);
  m_sys_jet_selected =
      get_val_sys(m_AnalysisJets_jet_selected_NOSYS, &m_sys_br_jet_selected);
}

}  // namespace Charm
