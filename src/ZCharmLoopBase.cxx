#include "ZCharmLoopBase.h"

#include <math.h>

#include <iostream>

#include "HelperFunctions.h"
#include "TVector2.h"

namespace Charm {
void ZCharmLoopBase::initialize() {
  std::cout << "ZCharmLoopBase::initialize" << std::endl;

  // set config
  configure();

  // systematics
  if (m_do_systematics) {
    // GEN
    m_sys_br_GEN.init(get_sys("systematics_GEN"));

    // PRW
    m_sys_br_PU.init(get_sys("systematics_PRW"));

    // JVT
    m_sys_br_JVT.init(get_sys("systematics_JET_Calib"));
    m_sys_br_JVT.init(get_sys("systematics_JET_Jvt"));

    // Production fractions
    m_sys_br_PROD_FRAC.init(get_sys("systematics_PROD_FRAC"));

    // electron calib
    m_sys_br_el_isIsolated_Tight_VarRad.init(get_sys("systematics_EL_Calib"));
    m_sys_br_el_pt.init(get_sys("systematics_EL_Calib"));
    m_sys_br_el_selected.init(get_sys("systematics_JET_Calib"));
    m_sys_br_el_selected.init(get_sys("systematics_MUON_Calib"));
    m_sys_br_el_selected.init(get_sys("systematics_EL_Calib"));
    m_sys_br_el_calib_reco_eff.init(get_sys("systematics_EL_Calib"));
    m_sys_br_el_calib_id_eff.init(get_sys("systematics_EL_Calib"));
    m_sys_br_el_calib_iso_eff.init(get_sys("systematics_EL_Calib"));
    m_sys_br_el_calib_trig_eff.init(get_sys("systematics_EL_Calib"));
    m_sys_br_el_calib_trig_sf.init(get_sys("systematics_EL_Calib"));
    m_sys_br_el_calib_trig_noiso_eff.init(get_sys("systematics_EL_Calib"));
    m_sys_br_el_calib_trig_noiso_sf.init(get_sys("systematics_EL_Calib"));
    m_sys_br_el_calib_charge_eff.init(get_sys("systematics_EL_Calib"));

    // muon calib
    m_sys_br_mu_isQuality_Medium.init(get_sys("systematics_MUON_Calib"));
    m_sys_br_mu_pt.init(get_sys("systematics_MUON_Calib"));
    m_sys_br_mu_selected.init(get_sys("systematics_EL_Calib"));
    m_sys_br_mu_selected.init(get_sys("systematics_JET_Calib"));
    m_sys_br_mu_selected.init(get_sys("systematics_MUON_Calib"));
    m_sys_br_mu_isIsolated_PflowTight_VarRad.init(
        get_sys("systematics_MUON_Calib"));
    m_sys_br_mu_calib_quality_eff.init(get_sys("systematics_MUON_Calib"));
    m_sys_br_mu_calib_ttva_eff.init(get_sys("systematics_MUON_Calib"));
    m_sys_br_mu_calib_trig_2015_eff_data.init(
        get_sys("systematics_MUON_Calib"));
    m_sys_br_mu_calib_iso_eff.init(get_sys("systematics_MUON_Calib"));
    m_sys_br_mu_calib_trig_2015_eff_mc.init(get_sys("systematics_MUON_Calib"));
    m_sys_br_mu_calib_trig_2018_eff_data.init(
        get_sys("systematics_MUON_Calib"));
    m_sys_br_mu_calib_trig_2018_eff_mc.init(get_sys("systematics_MUON_Calib"));

    // electron weights
    m_sys_br_el_reco_eff.init(get_sys("systematics_EL_EFF_Reco"));
    m_sys_br_el_id_eff.init(get_sys("systematics_EL_EFF_ID"));
    m_sys_br_el_iso_eff.init(get_sys("systematics_EL_EFF_Iso"));
    m_sys_br_el_trig_sf.init(get_sys("systematics_EL_EFF_Trigger"));
    m_sys_br_el_trig_eff.init(get_sys("systematics_EL_EFF_Trigger"));
    m_sys_br_el_charge_eff.init(get_sys("systematics_EL_CHARGE"));
    m_sys_br_el_trig_noiso_sf.init(get_sys("systematics_EL_EFF_Trigger"));
    m_sys_br_el_trig_noiso_eff.init(get_sys("systematics_EL_EFF_Trigger"));

    // muon weights
    m_sys_br_mu_quality_eff.init(get_sys("systematics_MUON_EFF_RECO"));
    m_sys_br_mu_ttva_eff.init(get_sys("systematics_MUON_EFF_TTVA"));
    m_sys_br_mu_trig_2015_eff_data.init(get_sys("systematics_MUON_EFF_Trig"));
    m_sys_br_mu_iso_eff.init(get_sys("systematics_MUON_EFF_ISO"));
    m_sys_br_mu_trig_2015_eff_mc.init(get_sys("systematics_MUON_EFF_Trig"));
    m_sys_br_mu_trig_2018_eff_data.init(get_sys("systematics_MUON_EFF_Trig"));
    m_sys_br_mu_trig_2018_eff_mc.init(get_sys("systematics_MUON_EFF_Trig"));

    // jet calib
    m_sys_br_jet_pt.init(get_sys("systematics_JET_Calib"));
    m_sys_br_jet_selected.init(get_sys("systematics_JET_Calib"));
    m_sys_br_jet_selected.init(get_sys("systematics_EL_Calib"));
    m_sys_br_jet_selected.init(get_sys("systematics_MUON_Calib"));

    // FTAG
    m_sys_br_jet_ftag_eff.init(get_sys("systematics_FT_EFF"));
    m_sys_br_jet_calib_ftag_eff.init(get_sys("systematics_JET_Calib"));
  }
}

bool ZCharmLoopBase::preselection() {
  set_channel("inclusive");

  // skip if not ordered to do lepton preselection
  if (!m_do_lep_presel) {
    std::string prepend = "inclusive";
    m_channel_sys = std::vector<std::string>(m_nSyst, prepend);
    DMesonRec::set_lepton(nullptr);
    if (m_data_period == "ForcedDecay") {
      // mc event weight, initial sum of weights, x-sec, lumi weight
      multiply_mc_event_weight(m_EventInfo_generatorWeight_NOSYS, &m_sys_br_GEN,
                               true, false, false);
    }
    return true;
  }

  // loose D selection for lepton overlap removal
  if (m_D_plus_meson || m_D_star_meson || m_D_star_pi0_meson || m_D_s_meson) {
    get_D_plus_mesons(this, true);
    get_D_star_mesons(this, true);
    get_D_star_pi0_mesons(this, true);
    get_D_s_mesons(this, true);
  }

  // other weights
  if (m_is_mc && !m_no_weights) {
    // mc event weight, initial sum of weights, x-sec, lumi weight
    multiply_mc_event_weight(
        m_EventInfo_generatorWeight_NOSYS, &m_sys_br_GEN, m_apply_lumi, false,
        (m_mc_shower == "sherpa2211" && m_reweight_sherpa));

    // jvt weight and systematics
    multiply_nominal_and_sys_weight(m_EventInfo_jvt_effSF_NOSYS, &m_sys_br_JVT);

    // pileup weight and systematics
    multiply_nominal_and_sys_weight(m_EventInfo_PileupWeight_NOSYS,
                                    &m_sys_br_PU);

    // production fraction weight
    if (m_do_prod_fraction_rw) {
      multiply_nominal_and_sys_weight(m_EventInfo_prodFracWeight_NOSYS,
                                      &m_sys_br_PROD_FRAC);
    }
  }

  // sys branches for leptons
  read_sys_br();

  // get loose leptons
  get_loose_electrons();
  get_loose_muons();

  // require tight leptons
  get_tight_electrons();
  get_tight_muons();

  // clear all before running over systematics
  m_sys_anti_tight = std::vector<bool>(m_nSyst, 0.);
  m_sys_SS = std::vector<bool>(m_nSyst, 0.);
  m_sys_Z_m = std::vector<float>(m_nSyst, 0.);
  m_sys_Z_pt = std::vector<float>(m_nSyst, 0.);
  m_sys_lep1_pt = std::vector<float>(m_nSyst, 0.);
  m_sys_lep1_eta = std::vector<float>(m_nSyst, 0.);
  m_sys_lep1_phi = std::vector<float>(m_nSyst, 0.);
  m_sys_lep2_pt = std::vector<float>(m_nSyst, 0.);
  m_sys_lep2_eta = std::vector<float>(m_nSyst, 0.);
  m_sys_lep2_phi = std::vector<float>(m_nSyst, 0.);
  m_sys_nbjets = std::vector<float>(m_nSyst, 0.);
  bool event_pass = false;

  for (unsigned int i = 0; i < m_nSyst; i++) {
    bool nominal = i == 0;
    if (!m_do_systematics && !nominal) {
      continue;
    }

    unsigned int sys_index = i - 1;
    std::vector<std::pair<unsigned int, float>> electrons, muons;
    electrons = m_electrons_sys.at(i);
    muons = m_muons_sys.at(i);
    auto loose_electrons = m_loose_electrons_sys.at(i);
    auto loose_muons = m_loose_muons_sys.at(i);
    TLorentzVector lep1;
    TLorentzVector lep2;
    std::string channel_string = "";

    // bad muon veto
    for (unsigned int j = 0; j < loose_muons.size(); j++) {
      if (m_AnalysisMuons_is_bad->at(loose_muons.at(j).first)) {
        continue;
      }
    }

    // require exactly two same-flavor loose leptons
    if (!((loose_electrons.size() == 2 && loose_muons.size() == 0) ||
          (loose_muons.size() == 2 && loose_electrons.size() == 0))) {
      continue;
    }

    // require exactly two same-flavor tight leptons
    if (!((electrons.size() == 2 && muons.size() == 0) ||
          (muons.size() == 2 && electrons.size() == 0))) {
      // Set anti-tight event to true
      m_sys_anti_tight.at(i) = true;
      if (!m_do_mj_estimation) {
        continue;
      }
    }

    // electron channel
    if (loose_electrons.size() == 2) {
      // channel
      channel_string = "el";

      // electron index
      unsigned int index1 = loose_electrons.at(0).first;
      unsigned int index2 = loose_electrons.at(1).first;

      // order by pT
      if (m_sys_el_pt.at(i)->at(index2) > m_sys_el_pt.at(i)->at(index1)) {
        unsigned int temp = index1;
        index1 = index2;
        index2 = temp;
      }

      // charge
      if (m_AnalysisElectrons_charge->at(index1) *
              m_AnalysisElectrons_charge->at(index2) >
          0) {
        m_sys_SS.at(i) = true;
        if (!m_do_mj_estimation) {
          continue;
        }
      }

      // pass electron trigger
      if (!pass_el_trigger()) {
        continue;
      }

      // single electron trigger
      if (!el_is_trigger_matched(index1, index2)) {
        continue;
      }

      // // Leading lepton pT cut
      // if (m_sys_el_pt.at(i)->at(index1) * Charm::GeV < 27.)
      // {
      //     continue;
      // }

      // fill variables
      lep1.SetPtEtaPhiM(m_sys_el_pt.at(i)->at(index1) * Charm::GeV,
                        m_AnalysisElectrons_eta->at(index1),
                        m_AnalysisElectrons_phi->at(index1),
                        Charm::ELECTRON_MASS * Charm::GeV);
      lep2.SetPtEtaPhiM(m_sys_el_pt.at(i)->at(index2) * Charm::GeV,
                        m_AnalysisElectrons_eta->at(index2),
                        m_AnalysisElectrons_phi->at(index2),
                        Charm::ELECTRON_MASS * Charm::GeV);
      m_sys_lep1_pt.at(i) = m_sys_el_pt.at(i)->at(index1) * Charm::GeV;
      m_sys_lep1_eta.at(i) = m_AnalysisElectrons_eta->at(index1);
      m_sys_lep1_phi.at(i) = m_AnalysisElectrons_phi->at(index1);
      m_sys_lep2_pt.at(i) = m_sys_el_pt.at(i)->at(index2) * Charm::GeV;
      m_sys_lep2_eta.at(i) = m_AnalysisElectrons_eta->at(index2);
      m_sys_lep2_phi.at(i) = m_AnalysisElectrons_phi->at(index2);
      if (nominal) {
        m_Z_eta = (lep1 + lep2).Eta();
        m_Z_phi = (lep1 + lep2).Phi();
        m_lep_dPhi = lep1.DeltaPhi(lep2);
        m_lep_dR = lep1.DeltaR(lep2);
        m_lep1_d0 = m_AnalysisElectrons_d0->at(index1);
        m_lep1_d0sig = m_AnalysisElectrons_d0sig->at(index1);
        m_lep1_charge = m_AnalysisElectrons_charge->at(index1);
        m_lep2_d0 = m_AnalysisElectrons_d0->at(index2);
        m_lep2_d0sig = m_AnalysisElectrons_d0sig->at(index2);
        m_lep2_charge = m_AnalysisElectrons_charge->at(index2);
        if (m_do_extra_histograms) {
          m_lep1_z0sinTheta = m_AnalysisElectrons_z0sinTheta->at(index1);
          m_lep1_topoetcone20 =
              m_AnalysisElectrons_topoetcone20->at(index1) * Charm::GeV;
          m_lep1_ptvarcone20_TightTTVA_pt1000 =
              m_AnalysisElectrons_ptvarcone20_TightTTVA_pt1000->at(index1) *
              Charm::GeV;
          m_lep1_ptvarcone30_TightTTVA_pt1000 =
              m_AnalysisElectrons_ptvarcone30_TightTTVA_pt1000->at(index1) *
              Charm::GeV;
          m_lep1_topoetcone20_over_pt = m_lep1_topoetcone20 / lep1.Pt();
          m_lep1_ptvarcone20_TightTTVA_pt1000_over_pt =
              m_lep1_ptvarcone20_TightTTVA_pt1000 / lep1.Pt();
          m_lep1_ptvarcone30_TightTTVA_pt1000_over_pt =
              m_lep1_ptvarcone30_TightTTVA_pt1000 / lep1.Pt();
          m_lep2_z0sinTheta = m_AnalysisElectrons_z0sinTheta->at(index2);
          m_lep2_topoetcone20 =
              m_AnalysisElectrons_topoetcone20->at(index2) * Charm::GeV;
          m_lep2_ptvarcone20_TightTTVA_pt1000 =
              m_AnalysisElectrons_ptvarcone20_TightTTVA_pt1000->at(index2) *
              Charm::GeV;
          m_lep2_ptvarcone30_TightTTVA_pt1000 =
              m_AnalysisElectrons_ptvarcone30_TightTTVA_pt1000->at(index2) *
              Charm::GeV;
          m_lep2_topoetcone20_over_pt = m_lep2_topoetcone20 / lep2.Pt();
          m_lep2_ptvarcone20_TightTTVA_pt1000_over_pt =
              m_lep2_ptvarcone20_TightTTVA_pt1000 / lep2.Pt();
          m_lep2_ptvarcone30_TightTTVA_pt1000_over_pt =
              m_lep2_ptvarcone30_TightTTVA_pt1000 / lep2.Pt();
          m_lep1_ptvarcone30_plus_neflowisol20 = -999;
          m_lep2_ptvarcone30_plus_neflowisol20 = -999;
        }
      }

      // efficiency weight
      if (m_is_mc) {
        if (!m_sys_anti_tight.at(i)) {
          // TT event (SR)
          // std::cout << "TT event!" << std::endl;
          multiply_event_weight(
              m_electrons_sys.at(i).at(0).second *
                  m_electrons_sys.at(i).at(1).second *
                  get_el_trigger_weight(i, index1, index2, "TT"),
              i);
        } else {
          // Check if any tight electron is present (if not, treat this as LL)
          if (electrons.size()) {
            // TL event
            if (electrons.at(0).first == loose_electrons.at(0).first) {
              // std::cout << "TL event!" << std::endl;
              multiply_event_weight(
                  m_electrons_sys.at(i).at(0).second *
                      m_loose_electrons_sys.at(i).at(1).second *
                      get_el_trigger_weight(i, index1, index2, "TL"),
                  i);
            }
            // LT event
            else {
              // std::cout << "LT event!" << std::endl;
              multiply_event_weight(
                  m_loose_electrons_sys.at(i).at(0).second *
                      m_electrons_sys.at(i).at(0).second *
                      get_el_trigger_weight(i, index1, index2, "LT"),
                  i);
            }

          } else {
            // LL event
            // std::cout << "LL event!" << std::endl;
            multiply_event_weight(
                m_loose_electrons_sys.at(i).at(0).second *
                    m_loose_electrons_sys.at(i).at(1).second *
                    get_el_trigger_weight(i, index1, index2, "LL"),
                i);
          }
        }
      }
    } else if (loose_muons.size() == 2) {
      // channel
      channel_string = "mu";

      // muon index
      unsigned int index1 = loose_muons.at(0).first;
      unsigned int index2 = loose_muons.at(1).first;

      // order by pT
      if (m_sys_mu_pt.at(i)->at(index2) > m_sys_mu_pt.at(i)->at(index1)) {
        unsigned int temp = index1;
        index1 = index2;
        index2 = temp;
      }

      // charge
      if (m_AnalysisMuons_charge->at(index1) *
              m_AnalysisMuons_charge->at(index2) >
          0) {
        m_sys_SS.at(i) = true;
        if (!m_do_mj_estimation) {
          continue;
        }
      }

      // pass muon trigger
      if (!pass_mu_trigger()) {
        continue;
      }

      // single muon trigger
      if (!mu_is_trigger_matched(index1, index2)) {
        continue;
      }

      // // Leading lepton pT cut
      // if (m_sys_mu_pt.at(i)->at(index1) * Charm::GeV < 27.)
      // {
      //     continue;
      // }

      // fill variables
      lep1.SetPtEtaPhiM(m_sys_mu_pt.at(i)->at(index1) * Charm::GeV,
                        m_AnalysisMuons_eta->at(index1),
                        m_AnalysisMuons_phi->at(index1),
                        Charm::MUON_MASS * Charm::GeV);
      lep2.SetPtEtaPhiM(m_sys_mu_pt.at(i)->at(index2) * Charm::GeV,
                        m_AnalysisMuons_eta->at(index2),
                        m_AnalysisMuons_phi->at(index2),
                        Charm::MUON_MASS * Charm::GeV);
      m_sys_lep1_pt.at(i) = m_sys_mu_pt.at(i)->at(index1) * Charm::GeV;
      m_sys_lep1_eta.at(i) = m_AnalysisMuons_eta->at(index1);
      m_sys_lep1_phi.at(i) = m_AnalysisMuons_phi->at(index1);
      m_sys_lep2_pt.at(i) = m_sys_mu_pt.at(i)->at(index2) * Charm::GeV;
      m_sys_lep2_eta.at(i) = m_AnalysisMuons_eta->at(index2);
      m_sys_lep2_phi.at(i) = m_AnalysisMuons_phi->at(index2);
      if (nominal) {
        m_Z_eta = (lep1 + lep2).Eta();
        m_Z_phi = (lep1 + lep2).Phi();
        m_lep_dPhi = lep1.DeltaPhi(lep2);
        m_lep_dR = lep1.DeltaR(lep2);
        m_lep1_d0 = m_AnalysisMuons_d0->at(index1);
        m_lep1_d0sig = m_AnalysisMuons_d0sig->at(index1);
        m_lep1_charge = m_AnalysisMuons_charge->at(index1);
        m_lep2_d0 = m_AnalysisMuons_d0->at(index2);
        m_lep2_d0sig = m_AnalysisMuons_d0sig->at(index2);
        m_lep2_charge = m_AnalysisMuons_charge->at(index2);
        if (m_do_extra_histograms) {
          m_lep1_z0sinTheta = m_AnalysisMuons_z0sinTheta->at(index1);
          m_lep1_topoetcone20 =
              m_AnalysisMuons_topoetcone20->at(index1) * Charm::GeV;
          m_lep1_ptvarcone30_TightTTVA_pt1000 =
              m_AnalysisMuons_ptvarcone30_TightTTVA_pt1000->at(index1) *
              Charm::GeV;
          m_lep1_topoetcone20_over_pt = m_lep1_topoetcone20 / lep1.Pt();
          m_lep1_ptvarcone30_TightTTVA_pt1000_over_pt =
              m_lep1_ptvarcone30_TightTTVA_pt1000 / lep1.Pt();
          m_lep1_ptvarcone30_plus_neflowisol20 =
              (m_AnalysisMuons_ptvarcone30_TightTTVA_pt500->at(index1) +
               0.4 * m_AnalysisMuons_neflowisol20->at(index1)) /
              m_sys_mu_pt.at(i)->at(index1);
          m_lep1_neflowisol20 = m_AnalysisMuons_neflowisol20->at(index1);
          m_lep2_z0sinTheta = m_AnalysisMuons_z0sinTheta->at(index2);
          m_lep2_topoetcone20 =
              m_AnalysisMuons_topoetcone20->at(index2) * Charm::GeV;
          m_lep2_ptvarcone30_TightTTVA_pt1000 =
              m_AnalysisMuons_ptvarcone30_TightTTVA_pt1000->at(index2) *
              Charm::GeV;
          m_lep2_topoetcone20_over_pt = m_lep2_topoetcone20 / lep2.Pt();
          m_lep2_ptvarcone30_TightTTVA_pt1000_over_pt =
              m_lep2_ptvarcone30_TightTTVA_pt1000 / lep2.Pt();
          m_lep2_ptvarcone30_plus_neflowisol20 =
              (m_AnalysisMuons_ptvarcone30_TightTTVA_pt500->at(index2) +
               0.4 * m_AnalysisMuons_neflowisol20->at(index2)) /
              m_sys_mu_pt.at(i)->at(index2);
          m_lep2_neflowisol20 = m_AnalysisMuons_neflowisol20->at(index2);
          m_lep1_ptvarcone20_TightTTVA_pt1000 = -999;
          m_lep2_ptvarcone20_TightTTVA_pt1000 = -999;
          m_lep1_ptvarcone20_TightTTVA_pt1000_over_pt = -999;
          m_lep2_ptvarcone20_TightTTVA_pt1000_over_pt = -999;
        }
      }

      // efficiency weight
      if (m_is_mc) {
        if (!m_sys_anti_tight.at(i)) {
          // TT event
          // std::cout << "TT event!" << std::endl;
          multiply_event_weight(m_muons_sys.at(i).at(0).second *
                                    m_muons_sys.at(i).at(1).second *
                                    get_mu_trigger_weight(i, index1, index2),
                                i);
        } else {
          if (muons.size()) {
            if (muons.at(0).first == loose_muons.at(0).first) {
              // TL event
              // std::cout << "TL event!" << std::endl;
              multiply_event_weight(
                  m_muons_sys.at(i).at(0).second *
                      m_loose_muons_sys.at(i).at(1).second *
                      get_mu_trigger_weight(i, index1, index2),
                  i);
            } else {
              // LT event
              // std::cout << "LT event!" << std::endl;
              multiply_event_weight(
                  m_loose_muons_sys.at(i).at(0).second *
                      m_muons_sys.at(i).at(0).second *
                      get_mu_trigger_weight(i, index1, index2),
                  i);
            }
          } else {
            // LL event
            // std::cout << "LL event!" << std::endl;
            multiply_event_weight(m_loose_muons_sys.at(i).at(0).second *
                                      m_loose_muons_sys.at(i).at(1).second *
                                      get_mu_trigger_weight(i, index1, index2),
                                  i);
          }
        }
      }
    } else {
      std::cout << "wrong size" << std::endl;
      return false;
    }

    // set channel
    if (channel_string != "") {
      // base name
      m_channel_sys.at(i) = channel_string;

      // period string
      if (m_split_by_period) {
        m_channel_sys.at(i) = get_period_string() + "_" + m_channel_sys.at(i);
      }
    } else {
      continue;
    }

    // Z boson
    TLorentzVector Zboson = lep1 + lep2;
    // Wide m(Z) window for mj estimation, tighter fid window for Z+D analysis
    if (m_do_mj_estimation && Zboson.M() >= 40. && Zboson.M() <= 160.) {
      // Exclude Z+D analysis fiducial Z mass window to remove signal
      // contamination in el channel
      if (channel_string == "el") {
        if (Zboson.M() < 76. || Zboson.M() > 106.) {
          event_pass = true;
        }
      } else {
        event_pass = true;
      }
    } else if ((!m_do_mj_estimation) && Zboson.M() >= 76. &&
               Zboson.M() <= 106.) {
      event_pass = true;
    } else {
      m_channel_sys.at(i) = "";
      continue;
    }
    m_sys_Z_m.at(i) = Zboson.M();
    m_sys_Z_pt.at(i) = Zboson.Pt();
  }

  if (!event_pass) {
    return false;
  }

  // z truth selection
  m_sys_pass_ztruth_fid_cuts = false;

  if (m_is_mc) {
    if (m_fiducial_selection || m_save_truth_zjets) {
      if (get_truth_zjets()) {
        // leading lepton pt and eta cut
        if (m_truth_lep1.Pt() >= 27. && fabs(m_truth_lep1.Eta()) <= 2.5) {
          // sub-leading lepton pt and eta cut
          if (m_truth_lep2.Pt() >= 27. && fabs(m_truth_lep2.Eta()) <= 2.5) {
            // Z boson invariant mass cut
            if (m_truth_Z.M() >= 76. && m_truth_Z.M() <= 106.) {
              m_sys_pass_ztruth_fid_cuts = true;
              // fill_lepton_histograms("_fid");
            }
          }
        }
      }
    }
  }

  // accept event
  return true;
}

int ZCharmLoopBase::execute() {
  throw std::runtime_error(
      "execute() not implemented for base class ZCharmLoopBase");
  return 1;
}

float ZCharmLoopBase::get_el_trigger_weight(unsigned int sys_index,
                                            unsigned int i, unsigned int j,
                                            std::string iso_pair) {
  float effMC1, effData1, effMC2, effData2;

  if (&iso_pair[0] == "T") {
    // Tight electron sf
    effMC1 = m_sys_el_trig_eff.at(sys_index)->at(i);
    effData1 = effMC1 * m_sys_el_trig_sf.at(sys_index)->at(i);
  } else {
    // Loose electron sf
    effMC1 = m_sys_el_trig_noiso_eff.at(sys_index)->at(i);
    effData1 = effMC1 * m_sys_el_trig_noiso_sf.at(sys_index)->at(i);
  }

  if (&iso_pair[1] == "T") {
    // Tight electron sf
    effMC2 = m_sys_el_trig_eff.at(sys_index)->at(j);
    effData2 = effMC2 * m_sys_el_trig_sf.at(sys_index)->at(j);
  } else {
    // Loose electron sf
    effMC2 = m_sys_el_trig_noiso_eff.at(sys_index)->at(j);
    effData2 = effMC2 * m_sys_el_trig_noiso_sf.at(sys_index)->at(j);
  }

  float PMC = effMC1 * effMC2 + effMC1 * (1 - effMC2) + (1 - effMC1) * effMC2;
  float PData = effData1 * effData2 + effData1 * (1 - effData2) +
                (1 - effData1) * effData2;

  if (PMC > 0) {
    return PData / PMC;
  } else {
    return 0;
  }
}

float ZCharmLoopBase::get_mu_trigger_weight(unsigned int sys_index,
                                            unsigned int i, unsigned int j) {
  if (m_EventInfo_runNumber < 290000) {
    float effMC1 = m_sys_mu_trig_2015_eff_mc.at(sys_index)->at(i);
    float effMC2 = m_sys_mu_trig_2015_eff_mc.at(sys_index)->at(j);
    float effData1 = m_sys_mu_trig_2015_eff_data.at(sys_index)->at(i);
    float effData2 = m_sys_mu_trig_2015_eff_data.at(sys_index)->at(j);

    float PMC = effMC1 * effMC2 + effMC1 * (1 - effMC2) + (1 - effMC1) * effMC2;
    float PData = effData1 * effData2 + effData1 * (1 - effData2) +
                  (1 - effData1) * effData2;

    if (PMC > 0) {
      return PData / PMC;
    } else {
      return 0;
    }
  } else {
    float effMC1 = m_sys_mu_trig_2018_eff_mc.at(sys_index)->at(i);
    float effMC2 = m_sys_mu_trig_2018_eff_mc.at(sys_index)->at(j);
    float effData1 = m_sys_mu_trig_2018_eff_data.at(sys_index)->at(i);
    float effData2 = m_sys_mu_trig_2018_eff_data.at(sys_index)->at(j);

    float PMC = effMC1 * effMC2 + effMC1 * (1 - effMC2) + (1 - effMC1) * effMC2;
    float PData = effData1 * effData2 + effData1 * (1 - effData2) +
                  (1 - effData1) * effData2;

    if (PMC > 0) {
      return PData / PMC;
    } else {
      return 0;
    }
  }
}

void ZCharmLoopBase::get_tight_electrons() {
  m_electrons_sys.clear();

  for (unsigned int j = 0; j < m_nSyst; j++) {
    m_electrons_sys.push_back({});
    for (unsigned int i = 0; i < m_AnalysisElectrons_pt_NOSYS->size(); i++) {
      // nominal
      if ((m_sys_el_pt.at(j)->at(i) * Charm::GeV >= 27.) &&
          (m_sys_el_selected.at(j)->at(i)) && (m_sys_el_iso.at(j)->at(i)) &&
          (m_AnalysisElectrons_likelihood_Tight->at(i)) &&
          ((m_is_mc && m_truth_match && el_is_prompt(i)) ||
           (m_is_mc && !m_truth_match) || (!m_is_mc))) {
        float sf = 1.;
        if (m_is_mc) {
          sf *= m_sys_el_reco_eff.at(j)->at(i) * m_sys_el_id_eff.at(j)->at(i) *
                m_sys_el_iso_eff.at(j)->at(i) * m_sys_el_charge_sf.at(j)->at(i);
        }

        std::pair<unsigned int, float> pair(i, sf);
        m_electrons_sys.at(j).push_back(pair);
      }
    }
  }
}

void ZCharmLoopBase::get_tight_muons() {
  m_muons_sys.clear();
  // calibration sys
  for (unsigned int j = 0; j < m_nSyst; j++) {
    m_muons_sys.push_back({});
    for (unsigned int i = 0; i < m_AnalysisMuons_pt_NOSYS->size(); i++) {
      // nominal
      if ((m_sys_mu_pt.at(j)->at(i) * Charm::GeV >= 27.) &&
          (m_sys_mu_selected.at(j)->at(i)) && (m_sys_mu_quality.at(j)->at(i)) &&
          (m_sys_mu_iso.at(j)->at(i)) &&
          ((m_is_mc && m_truth_match && mu_is_prompt(i)) ||
           (m_is_mc && !m_truth_match) || (!m_is_mc))) {
        float sf = 1.;
        if (m_is_mc) {
          sf *= m_sys_mu_quality_eff.at(j)->at(i) *
                m_sys_mu_ttva_eff.at(j)->at(i) * m_sys_mu_iso_eff.at(j)->at(i);
        }

        std::pair<unsigned int, float> pair(i, sf);
        m_muons_sys.at(j).push_back(pair);
      }
    }
  }
}

void ZCharmLoopBase::get_loose_electrons() {
  m_loose_electrons_sys.clear();

  for (unsigned int j = 0; j < m_nSyst; j++) {
    m_loose_electrons_sys.push_back({});
    for (unsigned int i = 0; i < m_AnalysisElectrons_pt_NOSYS->size(); i++) {
      // nominal
      if ((m_sys_el_pt.at(j)->at(i) * Charm::GeV >= 27.) &&
          (m_sys_el_selected.at(j)->at(i)) &&
          (m_AnalysisElectrons_likelihood_Tight->at(i)) &&
          ((m_is_mc && m_truth_match && el_is_prompt(i)) ||
           (m_is_mc && !m_truth_match) || (!m_is_mc))) {
        // // additional requirements for Anti-Tight
        // if (!m_sys_el_iso.at(j)->at(i))
        // {
        //     if (fabs(m_AnalysisElectrons_ptvarcone30_TightTTVA_pt1000->at(i))
        //     == 0)
        //     {
        //         continue;
        //     }
        // }
        float sf = 1.;
        if (m_is_mc) {
          sf *= m_sys_el_reco_eff.at(j)->at(i) * m_sys_el_id_eff.at(j)->at(i);
          //   Add ch flip no iso sf
        }

        std::pair<unsigned int, float> pair(i, sf);
        m_loose_electrons_sys.at(j).push_back(pair);
      }
    }
  }
}

void ZCharmLoopBase::get_loose_muons() {
  m_loose_muons_sys.clear();

  // calibration sys
  for (unsigned int j = 0; j < m_nSyst; j++) {
    m_loose_muons_sys.push_back({});
    for (unsigned int i = 0; i < m_AnalysisMuons_pt_NOSYS->size(); i++) {
      // nominal
      if ((m_sys_mu_pt.at(j)->at(i) * Charm::GeV >= 27.) &&
          (m_sys_mu_selected.at(j)->at(i)) && (m_sys_mu_quality.at(j)->at(i)) &&
          ((m_is_mc && m_truth_match && mu_is_prompt(i)) ||
           (m_is_mc && !m_truth_match) || (!m_is_mc))) {
        // additional requirements for Anti-Tight
        // if (!m_sys_mu_iso.at(j)->at(i))
        // {
        //     double neflow =
        //     m_AnalysisMuons_neflowisol20->at(i)>0?m_AnalysisMuons_neflowisol20->at(i):0;
        //     if (((fabs(m_AnalysisMuons_ptvarcone30_TightTTVA_pt500->at(i)) +
        //     0.4 * neflow) / m_sys_mu_pt.at(j)->at(i)) < 0.12)
        //     {
        //         continue;
        //     }
        // }
        float sf = 1.;
        if (m_is_mc) {
          sf *= m_sys_mu_quality_eff.at(j)->at(i) *
                m_sys_mu_ttva_eff.at(j)->at(i);
        }
        std::pair<unsigned int, float> pair(i, sf);
        m_loose_muons_sys.at(j).push_back(pair);
      }
    }
  }
}

float ZCharmLoopBase::get_dr_lep_Dmeson(float lepton_eta, float lepton_phi) {
  float dR_min = 999;
  for (unsigned int j = 0; j < m_D_plus_indices.size(); j++) {
    float dR = DMesonRec::D_meson_lepton_dR(m_D_plus_indices.at(j), lepton_eta,
                                            lepton_phi);
    if (dR < dR_min) {
      dR_min = dR;
    }
  }
  for (unsigned int j = 0; j < m_D_star_indices.size(); j++) {
    float dR = DMesonRec::D_meson_lepton_dR(m_D_star_indices.at(j), lepton_eta,
                                            lepton_phi);
    if (dR < dR_min) {
      dR_min = dR;
    }
  }
  for (unsigned int j = 0; j < m_D_star_pi0_indices.size(); j++) {
    float dR = DMesonRec::D_meson_lepton_dR(m_D_star_pi0_indices.at(j),
                                            lepton_eta, lepton_phi);
    if (dR < dR_min) {
      dR_min = dR;
    }
  }
  for (unsigned int j = 0; j < m_D_s_indices.size(); j++) {
    float dR = DMesonRec::D_meson_lepton_dR(m_D_s_indices.at(j), lepton_eta,
                                            lepton_phi);
    if (dR < dR_min) {
      dR_min = dR;
    }
  }
  return dR_min;
}

void ZCharmLoopBase::get_jets() {
  m_jets_sys.clear();
  if (!m_do_lep_presel) {
    return;
  }
  // calibration sys
  for (unsigned int j = 0; j < m_nSyst; j++) {
    m_jets_sys.push_back({});
    for (unsigned int i = 0; i < m_AnalysisJets_pt_NOSYS->size(); i++) {
      bool pass_OR = true;
      if (m_D_plus_meson) {
        for (unsigned int k = 0; k < m_D_plus_indices.size(); k++) {
          float dR = get_dr_bjet_Dmeson(i, m_D_plus_indices.at(k));
          if (dR < 0.4) {
            pass_OR = false;
            break;
          }
        }
      }
      if (m_D_star_meson) {
        for (unsigned int k = 0; k < m_D_star_indices.size(); k++) {
          float dR = get_dr_bjet_Dmeson(i, m_D_star_indices.at(k));
          if (dR < 0.4) {
            pass_OR = false;
            break;
          }
        }
      }
      if (m_D_star_pi0_meson) {
        for (unsigned int k = 0; k < m_D_star_pi0_indices.size(); k++) {
          float dR = get_dr_bjet_Dmeson(i, m_D_star_pi0_indices.at(k));
          if (dR < 0.4) {
            pass_OR = false;
            break;
          }
        }
      }
      if (m_D_s_meson) {
        for (unsigned int k = 0; k < m_D_s_indices.size(); k++) {
          float dR = get_dr_bjet_Dmeson(i, m_D_s_indices.at(k));
          if (dR < 0.4) {
            pass_OR = false;
            break;
          }
        }
      }
      if (((fabs(m_AnalysisJets_eta->at(i)) < 2.4 &&
            (m_sys_jet_pt.at(j)->at(i) * Charm::GeV >= 20.)) ||
           (fabs(m_AnalysisJets_eta->at(i)) >= 2.4 &&
            fabs(m_AnalysisJets_eta->at(i)) <= 5.0 &&
            (m_sys_jet_pt.at(j)->at(i) * Charm::GeV >= 20.))) &&
          (m_sys_jet_selected.at(j)->at(i)) && pass_OR) {
        m_jets_sys.at(j).push_back(i);
      }
    }
  }
}

bool ZCharmLoopBase::pass_el_trigger() {
  if (m_EventInfo_runNumber < 290000) {
    return m_EventInfo_trigPassed_HLT_e24_lhmedium_L1EM20VH ||
           m_EventInfo_trigPassed_HLT_e60_lhmedium ||
           m_EventInfo_trigPassed_HLT_e120_lhloose;
  } else {
    return m_EventInfo_trigPassed_HLT_e26_lhtight_nod0_ivarloose ||
           m_EventInfo_trigPassed_HLT_e60_lhmedium_nod0 ||
           m_EventInfo_trigPassed_HLT_e140_lhloose_nod0;
  }
}

bool ZCharmLoopBase::pass_mu_trigger() {
  if (m_EventInfo_runNumber < 290000) {
    return m_EventInfo_trigPassed_HLT_mu50 ||
           m_EventInfo_trigPassed_HLT_mu20_iloose_L1MU15;
  } else {
    return m_EventInfo_trigPassed_HLT_mu50 ||
           m_EventInfo_trigPassed_HLT_mu26_ivarmedium;
  }
}

bool ZCharmLoopBase::el_is_trigger_matched(unsigned int i, unsigned int j) {
  if (m_EventInfo_runNumber < 290000) {
    return ((m_EventInfo_trigPassed_HLT_e24_lhmedium_L1EM20VH &&
             m_AnalysisElectrons_matched_HLT_e24_lhmedium_L1EM20VH->at(i)) ||
            (m_EventInfo_trigPassed_HLT_e60_lhmedium &&
             m_AnalysisElectrons_matched_HLT_e60_lhmedium->at(i)) ||
            (m_EventInfo_trigPassed_HLT_e120_lhloose &&
             m_AnalysisElectrons_matched_HLT_e120_lhloose->at(i))) ||
           ((m_EventInfo_trigPassed_HLT_e24_lhmedium_L1EM20VH &&
             m_AnalysisElectrons_matched_HLT_e24_lhmedium_L1EM20VH->at(j)) ||
            (m_EventInfo_trigPassed_HLT_e60_lhmedium &&
             m_AnalysisElectrons_matched_HLT_e60_lhmedium->at(j)) ||
            (m_EventInfo_trigPassed_HLT_e120_lhloose &&
             m_AnalysisElectrons_matched_HLT_e120_lhloose->at(j)));
  } else {
    return ((m_EventInfo_trigPassed_HLT_e26_lhtight_nod0_ivarloose &&
             m_AnalysisElectrons_matched_HLT_e26_lhtight_nod0_ivarloose->at(
                 i)) ||
            (m_EventInfo_trigPassed_HLT_e60_lhmedium_nod0 &&
             m_AnalysisElectrons_matched_HLT_e60_lhmedium_nod0->at(i)) ||
            (m_EventInfo_trigPassed_HLT_e140_lhloose_nod0 &&
             m_AnalysisElectrons_matched_HLT_e140_lhloose_nod0->at(i))) ||
           ((m_EventInfo_trigPassed_HLT_e26_lhtight_nod0_ivarloose &&
             m_AnalysisElectrons_matched_HLT_e26_lhtight_nod0_ivarloose->at(
                 j)) ||
            (m_EventInfo_trigPassed_HLT_e60_lhmedium_nod0 &&
             m_AnalysisElectrons_matched_HLT_e60_lhmedium_nod0->at(j)) ||
            (m_EventInfo_trigPassed_HLT_e140_lhloose_nod0 &&
             m_AnalysisElectrons_matched_HLT_e140_lhloose_nod0->at(j)));
  }
}

bool ZCharmLoopBase::mu_is_trigger_matched(unsigned int i, unsigned int j) {
  if (m_EventInfo_runNumber < 290000) {
    return ((m_EventInfo_trigPassed_HLT_mu50 &&
             m_AnalysisMuons_matched_HLT_mu50->at(i)) ||
            (m_EventInfo_trigPassed_HLT_mu20_iloose_L1MU15 &&
             m_AnalysisMuons_matched_HLT_mu20_iloose_L1MU15->at(i))) ||
           ((m_EventInfo_trigPassed_HLT_mu50 &&
             m_AnalysisMuons_matched_HLT_mu50->at(j)) ||
            (m_EventInfo_trigPassed_HLT_mu20_iloose_L1MU15 &&
             m_AnalysisMuons_matched_HLT_mu20_iloose_L1MU15->at(j)));
  } else {
    return ((m_EventInfo_trigPassed_HLT_mu50 &&
             m_AnalysisMuons_matched_HLT_mu50->at(i)) ||
            (m_EventInfo_trigPassed_HLT_mu26_ivarmedium &&
             m_AnalysisMuons_matched_HLT_mu26_ivarmedium->at(i))) ||
           ((m_EventInfo_trigPassed_HLT_mu50 &&
             m_AnalysisMuons_matched_HLT_mu50->at(j)) ||
            (m_EventInfo_trigPassed_HLT_mu26_ivarmedium &&
             m_AnalysisMuons_matched_HLT_mu26_ivarmedium->at(j)));
  }
}

// see:
// https://gitlab.cern.ch/ATLAS-IFF/IFFTruthClassifier/-/blob/master/Root/IFFTruthClassifier.cxx
// --------------------------------------------------------------------------------------------------
bool ZCharmLoopBase::el_is_prompt(unsigned int i) {
  // prompt
  if (m_AnalysisElectrons_truthType->at(i) == 2) {
    return true;
  }

  // Adding these cases from ElectronEfficiencyHelpers
  if (m_AnalysisElectrons_firstEgMotherTruthType->at(i) == 2 &&
      fabs(m_AnalysisElectrons_firstEgMotherPdgId->at(i)) == 11) {
    return true;
  }

  // FSR photons from electrons
  if (m_AnalysisElectrons_truthType->at(i) == 15 &&
      m_AnalysisElectrons_truthOrigin->at(i) == 40) {
    return true;
  }
  if (m_AnalysisElectrons_truthType->at(i) == 4 &&
      Charm::is_in(m_AnalysisElectrons_truthOrigin->at(i), {5, 7}) &&
      m_AnalysisElectrons_firstEgMotherTruthType->at(i) == 15 &&
      m_AnalysisElectrons_firstEgMotherTruthOrigin->at(i) == 40) {
    return true;
  }

  // If we reach here then it is not a prompt electron
  return false;
}

bool ZCharmLoopBase::mu_is_prompt(unsigned int i) {
  return (
      (m_AnalysisMuons_truthType->at(i) == 6) &&
      Charm::is_in(m_AnalysisMuons_truthOrigin->at(i), {10, 12, 13, 14, 43}));
}

void ZCharmLoopBase::fill_PV_histograms(std::string channel) {
  std::vector<std::string> channels =
      std::vector<std::string>(m_nSyst, channel);
  fill_PV_histograms(channels);
}

void ZCharmLoopBase::fill_PV_histograms(std::vector<std::string> channels,
                                        std::vector<bool> extra_flag,
                                        std::vector<double> extra_weight) {
  if (m_fit_variables_only) {
    return;
  }
  add_fill_hist_sys("PV_X", channels, m_CharmEventInfo_PV_X, 100, -0.8, -0.2,
                    false, extra_flag, extra_weight);
  add_fill_hist_sys("PV_Y", channels, m_CharmEventInfo_PV_Y, 100, -0.8, -0.2,
                    false, extra_flag, extra_weight);
  add_fill_hist_sys("PV_Z", channels, m_CharmEventInfo_PV_Z, 100, -200, 200,
                    false, extra_flag, extra_weight);
  // add_fill_hist_sys("truthVertex_X", channels,
  // m_CharmEventInfo_truthVertex_X,
  //                        100, -0.8, -0.2, false, extra_flag, extra_weight);
  // add_fill_hist_sys("truthVertex_Y", channels,
  // m_CharmEventInfo_truthVertex_Y,
  //                        100, -0.8, -0.2, false, extra_flag, extra_weight);
  // add_fill_hist_sys("truthVertex_Z", channels,
  // m_CharmEventInfo_truthVertex_Z,
  //                        100, -200, 200, false, extra_flag, extra_weight);
  // add_fill_hist_sys("truth_reco_vtx_diff_X", channels,
  //                        m_CharmEventInfo_truthVertex_X -
  //                        m_CharmEventInfo_PV_X, 100, -0.2, 0.2, false,
  //                        extra_flag, extra_weight);
  // add_fill_hist_sys("truth_reco_vtx_diff_Y", channels,
  //                        m_CharmEventInfo_truthVertex_Y -
  //                        m_CharmEventInfo_PV_Y, 100, -0.2, 0.2, false,
  //                        extra_flag, extra_weight);
  // add_fill_hist_sys("truth_reco_vtx_diff_Z", channels,
  //                        m_CharmEventInfo_truthVertex_Z -
  //                        m_CharmEventInfo_PV_Z, 100, -0.2, 0.2, false,
  //                        extra_flag, extra_weight);
}

void ZCharmLoopBase::fill_lepton_histograms(std::string channel) {
  std::vector<std::string> channels =
      std::vector<std::string>(m_nSyst, channel);
  fill_lepton_histograms(channels);
}

void ZCharmLoopBase::fill_lepton_histograms(std::vector<std::string> channels,
                                            std::vector<bool> extra_flag,
                                            std::vector<double> extra_weight) {
  if (m_fit_variables_only) {
    if (m_fill_zjets) {
      if (!m_do_mj_estimation) {
        add_fill_hist_sys("Z_m", channels, m_sys_Z_m, 120, 76, 106, true,
                          extra_flag, extra_weight);
      } else {
        add_fill_hist_sys("Z_m", channels, m_sys_Z_m, 120, 40, 160, true,
                          extra_flag, extra_weight);
      }
    }
    return;
  }

  add_fill_hist_sys("Z_m", channels, m_sys_Z_m, 120, 76, 106, true, extra_flag,
                    extra_weight);
  add_fill_hist_sys("Z_pt", channels, m_sys_Z_pt, 400, 0, 400, true, extra_flag,
                    extra_weight);
  add_fill_hist_sys("Z_eta", channels, m_Z_eta, 100, -8.0, 8.0, true,
                    extra_flag, extra_weight);
  add_fill_hist_sys("Z_phi", channels, m_Z_phi, 100, -M_PI, M_PI, true,
                    extra_flag, extra_weight);
  add_fill_hist_sys("lep1_pt", channels, m_sys_lep1_pt, 400, 0, 400, true,
                    extra_flag, extra_weight);
  add_fill_hist_sys("lep2_pt", channels, m_sys_lep2_pt, 400, 0, 400, true,
                    extra_flag, extra_weight);
  add_fill_hist_sys("lep1_eta", channels, m_sys_lep1_eta, 100, -3.0, 3.0, true,
                    extra_flag, extra_weight);
  add_fill_hist_sys("lep2_eta", channels, m_sys_lep2_eta, 100, -3.0, 3.0, true,
                    extra_flag, extra_weight);
  add_fill_hist_sys("lep1_phi", channels, m_sys_lep1_phi, 100, -M_PI, M_PI,
                    false, extra_flag, extra_weight);
  add_fill_hist_sys("lep2_phi", channels, m_sys_lep2_phi, 100, -M_PI, M_PI,
                    false, extra_flag, extra_weight);
  add_fill_hist_sys("lep_dphi", channels, m_lep_dPhi, 100, -M_PI, M_PI, false,
                    extra_flag, extra_weight);
  add_fill_hist_sys("lep_dR", channels, m_lep_dR, 100, 0, 6, false, extra_flag,
                    extra_weight);
  // add_fill_hist_sys("lep1_dR_Dmeson", channels, m_sys_lep1_dR_D,
  //                        120, 0, 6, true, extra_flag, extra_weight);
  // add_fill_hist_sys("lep2_dR_Dmeson", channels, m_sys_lep2_dR_D,
  //                        120, 0, 6, true, extra_flag, extra_weight);
  add_fill_hist_sys("pileup", channels,
                    m_EventInfo_correctedScaled_averageInteractionsPerCrossing,
                    80, 0, 80, false, extra_flag, extra_weight);
  add_fill_hist_sys("run_number", channels, m_EventInfo_runNumber, 150, 250000,
                    400000, false, extra_flag, extra_weight);
  if (m_fiducial_selection || m_save_truth_zjets) {
    add_fill_hist_sys("truth_lep1_pt", channels, m_truth_lep1.Pt(), 400, 0, 400,
                      false, extra_flag, extra_weight);
    add_fill_hist_sys("truth_lep2_pt", channels, m_truth_lep2.Pt(), 400, 0, 400,
                      false, extra_flag, extra_weight);
    add_fill_hist_sys("truth_lep1_eta", channels, m_truth_lep1.Eta(), 100, -3.0,
                      3.0, false, extra_flag, extra_weight);
    add_fill_hist_sys("truth_lep2_eta", channels, m_truth_lep2.Eta(), 100, -3.0,
                      3.0, false, extra_flag, extra_weight);
    add_fill_hist_sys("truth_lep1_phi", channels, m_truth_lep1.Phi(), 100,
                      -M_PI, M_PI, false, extra_flag, extra_weight);
    add_fill_hist_sys("truth_lep2_phi", channels, m_truth_lep2.Phi(), 100,
                      -M_PI, M_PI, false, extra_flag, extra_weight);
    add_fill_hist_sys("truth_Z_pt", channels, m_truth_Z.Pt(), 400, 0, 400, true,
                      extra_flag, extra_weight);
    add_fill_hist_sys("truth_Z_eta", channels, m_truth_Z.Eta(), 100, -3.0, 3.0,
                      false, extra_flag, extra_weight);
    add_fill_hist_sys("truth_Z_phi", channels, m_truth_Z.Phi(), 100, -M_PI,
                      M_PI, false, extra_flag, extra_weight);
    add_fill_hist_sys("truth_Z_m", channels, m_truth_Z.M(), 120, 76, 106, false,
                      extra_flag, extra_weight);
    add_fill_hist_sys("truth_Z_y", channels, m_truth_Z.Rapidity(), 120, -3.0,
                      3.0, true, extra_flag, extra_weight);
  }
  if (m_jet_selection) {
    add_fill_hist_sys("njets", channels, m_jets_sys.size(), 20, -0.5, 19.5,
                      true, extra_flag, extra_weight);
    add_fill_hist_sys("nbjets", channels, m_sys_nbjets, 20, -0.5, 19.5, true,
                      extra_flag, extra_weight);
  }
  if (m_do_extra_histograms) {
    add_fill_hist_sys("lep1_d0", channels, m_lep1_d0 * Charm::mum, 400, -200,
                      200, true, extra_flag, extra_weight);
    add_fill_hist_sys("lep1_d0sig", channels, m_lep1_d0sig, 100, -6.0, 6.0,
                      true, extra_flag, extra_weight);
    add_fill_hist_sys(
        "lep1_d0_fixed", channels,
        rescaled_d0(m_lep1_d0, m_EventInfo_runNumber, m_is_mc) * Charm::mum,
        400, -200, 200, true, extra_flag, extra_weight);
    add_fill_hist_sys(
        "lep1_d0sig_fixed", channels,
        m_lep1_d0sig / m_lep1_d0 *
            rescaled_d0(m_lep1_d0, m_EventInfo_runNumber, m_is_mc),
        100, -6.0, 6.0, true, extra_flag, extra_weight);
    add_fill_hist_sys("lep1_z0sinTheta", channels, m_lep1_z0sinTheta, 100, -0.6,
                      0.6, false, extra_flag, extra_weight);
    add_fill_hist_sys("lep1_topoetcone20", channels, m_lep1_topoetcone20, 100,
                      0.0, 10.0, false, extra_flag, extra_weight);
    add_fill_hist_sys("lep1_topoetcone20_over_pt", channels,
                      m_lep1_topoetcone20_over_pt, 100, 0.0, 0.16, false,
                      extra_flag, extra_weight);
    add_fill_hist_sys("lep1_ptvarcone30_TightTTVA_pt1000", channels,
                      m_lep1_ptvarcone30_TightTTVA_pt1000, 100, 0.0, 10.0,
                      false, extra_flag, extra_weight);
    add_fill_hist_sys("lep1_ptvarcone30_TightTTVA_pt1000_over_pt", channels,
                      m_lep1_ptvarcone30_TightTTVA_pt1000_over_pt, 100, 0.0,
                      0.1, false, extra_flag, extra_weight);
    add_fill_hist_sys("lep1_ptvarcone20_TightTTVA_pt1000", channels,
                      m_lep1_ptvarcone20_TightTTVA_pt1000, 100, 0.0, 10.0,
                      false, extra_flag, extra_weight);
    add_fill_hist_sys("lep1_ptvarcone20_TightTTVA_pt1000_over_pt", channels,
                      m_lep1_ptvarcone20_TightTTVA_pt1000_over_pt, 100, 0.0,
                      0.1, false, extra_flag, extra_weight);
    add_fill_hist_sys("lep1_neflowisol20_over_pt", channels,
                      m_lep1_neflowisol20, 200, 0.0, 0.5, false, extra_flag,
                      extra_weight);
    add_fill_hist_sys("lep1_ptvarcone30_plus_neflowisol20", channels,
                      m_lep1_ptvarcone30_plus_neflowisol20, 100, 0.0, 0.1,
                      false, extra_flag, extra_weight);
    // add_fill_hist_sys("lep1_is_isolated", channels, m_lep1_is_isolated,
    //                        2, 0.0, 2.0, false, extra_flag, extra_weight);
    // add_fill_hist_sys("lep1_EOverP", channels, m_lep1_EOverP,
    //                         1100, 0.0, 11.0, false, extra_flag,
    //                         extra_weight);
    add_fill_hist_sys("lep2_d0", channels, m_lep2_d0 * Charm::mum, 400, -200,
                      200, true, extra_flag, extra_weight);
    add_fill_hist_sys("lep2_d0sig", channels, m_lep2_d0sig, 100, -6.0, 6.0,
                      true, extra_flag, extra_weight);
    add_fill_hist_sys(
        "lep2_d0_fixed", channels,
        rescaled_d0(m_lep2_d0, m_EventInfo_runNumber, m_is_mc) * Charm::mum,
        400, -200, 200, true, extra_flag, extra_weight);
    add_fill_hist_sys(
        "lep2_d0sig_fixed", channels,
        m_lep2_d0sig / m_lep2_d0 *
            rescaled_d0(m_lep2_d0, m_EventInfo_runNumber, m_is_mc),
        100, -6.0, 6.0, true, extra_flag, extra_weight);
    add_fill_hist_sys("lep2_z0sinTheta", channels, m_lep2_z0sinTheta, 100, -0.6,
                      0.6, false, extra_flag, extra_weight);
    add_fill_hist_sys("lep2_topoetcone20", channels, m_lep2_topoetcone20, 100,
                      0.0, 10.0, false, extra_flag, extra_weight);
    add_fill_hist_sys("lep2_topoetcone20_over_pt", channels,
                      m_lep2_topoetcone20_over_pt, 100, 0.0, 0.16, false,
                      extra_flag, extra_weight);
    add_fill_hist_sys("lep2_ptvarcone30_TightTTVA_pt1000", channels,
                      m_lep2_ptvarcone30_TightTTVA_pt1000, 100, 0.0, 10.0,
                      false, extra_flag, extra_weight);
    add_fill_hist_sys("lep2_ptvarcone30_TightTTVA_pt1000_over_pt", channels,
                      m_lep2_ptvarcone30_TightTTVA_pt1000_over_pt, 100, 0.0,
                      0.1, false, extra_flag, extra_weight);
    add_fill_hist_sys("lep2_ptvarcone20_TightTTVA_pt1000", channels,
                      m_lep2_ptvarcone20_TightTTVA_pt1000, 100, 0.0, 10.0,
                      false, extra_flag, extra_weight);
    add_fill_hist_sys("lep2_ptvarcone20_TightTTVA_pt1000_over_pt", channels,
                      m_lep2_ptvarcone20_TightTTVA_pt1000_over_pt, 100, 0.0,
                      0.1, false, extra_flag, extra_weight);
    add_fill_hist_sys("lep2_neflowisol20_over_pt", channels,
                      m_lep2_neflowisol20, 200, 0.0, 0.5, false, extra_flag,
                      extra_weight);
    add_fill_hist_sys("lep2_ptvarcone30_plus_neflowisol20", channels,
                      m_lep2_ptvarcone30_plus_neflowisol20, 100, 0.0, 0.1,
                      false, extra_flag, extra_weight);
    // add_fill_hist_sys("lep2_is_isolated", channels, m_lep2_is_isolated,
    //                        2, 0.0, 2.0, false, extra_flag, extra_weight);
    // add_fill_hist_sys("lep2_EOverP", channels, m_lep2_EOverP,
    //                         1100, 0.0, 11.0, false, extra_flag,
    //                         extra_weight);
  }
  // if (m_fake_rate_measurement)
  // {
  //     add_fill_hist_sys("lep1_pt_calo_eta", channels, m_sys_lep1_pt,
  //     m_sys_lep1_calo_eta,
  //                            400, 0, 400, 5, m_eta_bins_el, true, extra_flag,
  //                            extra_weight);
  //     add_fill_hist_sys("lep2_pt_calo_eta", channels, m_sys_lep2_pt,
  //     m_sys_lep2_calo_eta,
  //                            400, 0, 400, 5, m_eta_bins_el, true, extra_flag,
  //                            extra_weight);
  //     add_fill_hist_sys("lep1_pt_eta", channels, m_sys_lep1_pt,
  //     m_sys_lep1_eta,
  //                            400, 0, 400, 3, m_eta_bins_mu, true, extra_flag,
  //                            extra_weight);
  //     add_fill_hist_sys("lep2_pt_eta", channels, m_sys_lep2_pt,
  //     m_sys_lep2_eta,
  //                            400, 0, 400, 3, m_eta_bins_mu, true, extra_flag,
  //                            extra_weight);
  // }
}

void ZCharmLoopBase::fill_D_plus_histograms(std::string channel) {
  // number of D mesons
  // if (m_fill_zjets && !m_fit_variables_only)
  // {
  //     add_fill_hist_sys("N_Dplus", channel, m_D_plus_indices.size(), 20,
  //     -0.5, 19.5, true);
  // }

  // single entry region with at least one D meson
  if (m_do_single_entry_D) {
    std::vector<std::string> single_entry_channel =
        std::vector<std::string>(m_nSyst, channel);
    std::vector<bool> sys_at_least_one_D = std::vector<bool>(m_nSyst, false);
    std::vector<float> multiplicity = std::vector<float>(m_nSyst, 0);
    bool has_D = false;
    for (unsigned int j = 0; j < m_nSyst; j++) {
      // loop over D plus mesons
      for (unsigned int i = 0; i < m_D_plus_indices.size(); i++) {
        unsigned int D_index = m_D_plus_indices.at(i);

        // dR cut
        float dRlep1 = 999;
        float dRlep2 = 999;
        if (m_do_lep_presel) {
          dRlep1 = DMesonRec::D_meson_lepton_dR(D_index, m_sys_lep1_eta.at(j),
                                                m_sys_lep1_phi.at(j));
          dRlep2 = DMesonRec::D_meson_lepton_dR(D_index, m_sys_lep2_eta.at(j),
                                                m_sys_lep2_phi.at(j));
        }
        if (dRlep1 >= 0.3 && dRlep2 >= 0.3) {
          sys_at_least_one_D.at(j) = true;
          multiplicity.at(j) += 1;
        }
      }
      if (multiplicity.at(j) > 0) {
        single_entry_channel.at(j) += "_Dplus";
        has_D = true;
      }
    }

    if (m_do_single_entry_D_only && m_have_one_D) {
      return;
    } else if (m_do_single_entry_D_only && !m_have_one_D && has_D) {
      m_have_one_D = true;
    }

    // if (m_do_lep_presel)
    // {
    //     fill_lepton_histograms();
    // }
    if (m_do_PV) {
      fill_PV_histograms(single_entry_channel, sys_at_least_one_D);
    }
    add_fill_hist_sys("N_Dplus", single_entry_channel, multiplicity, 20, -0.5,
                      19.5, true, sys_at_least_one_D);
    if (m_do_single_entry_D_only) {
      return;
    }
  }

  // counters for D multiplicity
  std::vector<std::unordered_map<std::string, unsigned int>>
      multiplicity_counter;
  std::set<std::string> used_channels;

  // loop over D plus mesons
  for (unsigned int i = 0; i < m_D_plus_indices.size(); i++) {
    // retreive the D meson
    unsigned int D_index = m_D_plus_indices.at(i);

    // truth category
    bool matched_signal = false;
    std::string truth_category = "";
    int truthIndex = -1;
    if (m_truth_matchD && m_is_mc) {
      truth_category = TruthInfo::get_truth_category(
          this, D_index, "Kpipi", m_fiducial_selection, m_zplusd_mode);
      truthIndex =
          TruthInfo::getMatchedBarcode(m_DMesons_truthBarcode->at(D_index));
    }
    if (truth_category == "Matched") {
      matched_signal = true;
      m_truth_D_pt = m_TruthParticles_Selected_pt->at(truthIndex) * Charm::GeV;
      m_truth_D_eta = m_TruthParticles_Selected_eta->at(truthIndex);
      m_truth_D_mass = truth_daughter_mass(truthIndex);
      m_truth_D_phi = m_TruthParticles_Selected_phi->at(truthIndex);
      if (m_bin_in_truth_D_pt) {
        truth_category +=
            "_truth_pt_bin" +
            std::to_string(
                m_zd_differential_d_pt_bins.get_pt_bin(m_truth_D_pt) + 1);
      }
    } else {
      m_truth_D_pt = 0;
      m_truth_D_eta = -999;
      m_truth_D_mass = 0;
      m_truth_D_phi = -999;
    }

    // signal only
    if (m_signal_only && !matched_signal) {
      continue;
    }

    if (m_debug_dmeson_printout && (truth_category == "HardMisMatched" ||
                                    truth_category == "MisMatched")) {
      std::cout << "~~~ " << truth_category << " detected! ~~~" << std::endl;
      debug_dmeson_printout(D_index);
    }

    // // dR w.r.t. the lepton depends on systematic
    std::vector<Float_t> sys_dRlep1 = std::vector<float>(m_nSyst, 0.);
    std::vector<Float_t> sys_dRlep2 = std::vector<float>(m_nSyst, 0.);

    // // dR w.r.t. the b-jet depends on systematic
    std::vector<Float_t> sys_dRbjet = std::vector<float>(m_nSyst, 0.);

    // D meson mass
    m_sys_dmeson_mass = std::vector<float>(m_nSyst, m_DMesons_m->at(D_index));

    // Get channel string
    std::vector<std::string> charged_channel =
        std::vector<std::string>(m_nSyst, channel);
    std::vector<bool> pass_dR_cut = std::vector<bool>(m_nSyst, false);
    std::vector<double> extra_weight = std::vector<double>(m_nSyst, 1.0);

    for (unsigned int j = 0; j < m_nSyst; j++) {
      multiplicity_counter.push_back({});
      if (!m_do_lep_presel || event_pass_sys(j)) {
        // // dR w.r.t. the lepton
        float dRlep1 = 999;
        float dRlep2 = 999;
        if (m_do_lep_presel) {
          dRlep1 = DMesonRec::D_meson_lepton_dR(D_index, m_sys_lep1_eta.at(j),
                                                m_sys_lep1_phi.at(j));
          dRlep2 = DMesonRec::D_meson_lepton_dR(D_index, m_sys_lep2_eta.at(j),
                                                m_sys_lep2_phi.at(j));
        }
        sys_dRlep1.at(j) = dRlep1;
        sys_dRlep2.at(j) = dRlep2;

        // dR cut
        if (dRlep1 < 0.3 || dRlep2 < 0.3) {
          continue;
        }

        // dR w.r.t. b-jets
        float dRbjet = 999;
        if (m_jet_selection && m_do_lep_presel) {
          auto jets = m_jets_sys.at(j);
          for (auto &jet : jets) {
            if (m_AnalysisJets_ftag_select_DL1r_FixedCutBEff_70->at(jet)) {
              float dR = get_dr_bjet_Dmeson(jet, D_index);
              if (dR < dRbjet) {
                dRbjet = dR;
              }
            }
          }
          sys_dRbjet.at(j) = dRbjet;
        }

        // sys string
        std::string sys_string = "";
        if (j > 0) {
          sys_string = m_systematics.at(j - 1);
        }

        // track efficiency sys weight
        double track_weight = m_track_sys_helper.get_track_eff_weight(
            sys_string, m_DMesons_pt->at(D_index) * Charm::GeV,
            m_DMesons_eta->at(D_index), m_DMesons_pdgId->at(D_index));
        extra_weight.at(j) *= track_weight;

        // smear mass
        double smear_mass =
            m_mass_smear_helper.get_smeared_mass(
                sys_string,
                m_zd_differential_d_pt_bins.get_pt_bin(
                    m_DMesons_pt->at(D_index) * Charm::GeV),
                m_DMesons_eta->at(D_index), m_DMesons_pdgId->at(D_index),
                m_DMesons_m->at(D_index)) *
            Charm::GeV;
        m_sys_dmeson_mass.at(j) = smear_mass;

        // modeling uncertainty
        if (matched_signal) {
          double fid_eff_weight = 1.0;
          if (m_bin_in_D_pt) {
            fid_eff_weight = m_modeling_uncertainty_helper.get_weight_pt(
                sys_string, m_truth_D_pt, "Dplus");
          }
          // else if (m_bin_in_lep_eta)
          // {
          //     fid_eff_weight =
          //     m_modeling_uncertainty_helper.get_weight_eta(sys_string,
          //     m_truth_abs_lep_eta, "Dplus");
          // }
          extra_weight.at(j) *= fid_eff_weight;
        }

        // D+ branching ratio background uncertainty
        if (truth_category == "411MisMatched" ||
            truth_category == "421MisMatched" ||
            truth_category == "431MisMatched") {
          double br_unc_weight = m_dplus_bkg_br_uncertainty.get_weight(
              sys_string, m_DMesons_m->at(D_index) * Charm::GeV, truth_category,
              "zplusd");
          extra_weight.at(j) *= br_unc_weight;
        }

        pass_dR_cut.at(j) = true;
        charged_channel.at(j) += "_Dplus";
        multiplicity_counter.at(j)[charged_channel.at(j)]++;
        used_channels.insert(charged_channel.at(j));

        // weight for SPG forced decay sample
        if (matched_signal && m_reweight_spg &&
            m_data_period == "ForcedDecay") {
          extra_weight.at(j) *= m_spg_forced_decay_helper.get_weight(
              m_DMesons_m->at(D_index) * Charm::GeV,
              m_zd_differential_d_pt_bins.get_pt_bin(m_truth_D_pt), "Dplus");
        }
      }
    }

    // lepton histograms for this channel
    if (m_do_lep_presel) {
      fill_lepton_histograms(charged_channel, pass_dR_cut, extra_weight);
    }
    if (m_do_PV) {
      fill_PV_histograms(charged_channel, pass_dR_cut, extra_weight);
    }

    // D plus meson specific histograms
    if (!m_fit_variables_only) {
      add_fill_hist_sys("Dmeson_dRlep1", charged_channel, sys_dRlep1, 120, 0.,
                        6., false, pass_dR_cut, extra_weight);
      add_fill_hist_sys("Dmeson_dRlep2", charged_channel, sys_dRlep2, 120, 0.,
                        6., false, pass_dR_cut, extra_weight);
      add_fill_hist_sys("Dmeson_dRbjet", charged_channel, sys_dRbjet, 120, 0.,
                        6., false, pass_dR_cut, extra_weight);
    }
    DMesonRec::fill_D_plus_histograms(this, charged_channel, D_index,
                                      pass_dR_cut, extra_weight);

    // D meson truth match
    if (m_is_mc && m_truth_matchD) {
      std::vector<std::string> truth_channel = charged_channel;
      std::vector<std::string> truth_channel_fid =
          std::vector<std::string>(m_nSyst, channel);

      for (unsigned int j = 0; j < m_nSyst; j++) {
        // // don't save AntiTight truth categories
        if (m_do_lep_presel && m_sys_anti_tight.at(j)) {
          pass_dR_cut.at(j) = false;
        }

        if (!m_do_lep_presel || (event_pass_sys(j) && pass_dR_cut.at(j))) {
          truth_channel.at(j) += "_" + truth_category;
          multiplicity_counter.at(j)[truth_channel.at(j)]++;
          used_channels.insert(truth_channel.at(j));
        }
      }
      if (m_do_lep_presel) {
        fill_lepton_histograms(truth_channel, pass_dR_cut, extra_weight);
      }
      if (m_do_PV) {
        fill_PV_histograms(truth_channel, pass_dR_cut, extra_weight);
      }
      if (!m_fit_variables_only) {
        add_fill_hist_sys("truth_Dmeson_pt", truth_channel, m_truth_D_pt, 150,
                          0, 150., true, pass_dR_cut, extra_weight);
        add_fill_hist_sys("truth_Dmeson_eta", truth_channel, m_truth_D_eta, 100,
                          -3.0, 3.0, true, pass_dR_cut, extra_weight);
        add_fill_hist_sys("truth_Dmeson_mass", truth_channel,
                          m_truth_D_mass * Charm::GeV, 200, 1.4, 2.4, false,
                          pass_dR_cut, extra_weight);
        add_fill_hist_sys("truth_Dmeson_phi", truth_channel, m_truth_D_phi, 100,
                          -M_PI, M_PI, false, pass_dR_cut, extra_weight);
        add_fill_hist_sys("Dmeson_dRlep1", truth_channel, sys_dRlep1, 120, 0.,
                          6., false, pass_dR_cut, extra_weight);
        add_fill_hist_sys("Dmeson_dRlep2", truth_channel, sys_dRlep2, 120, 0.,
                          6., false, pass_dR_cut, extra_weight);
        add_fill_hist_sys("Dmeson_dRbjet", truth_channel, sys_dRbjet, 120, 0.,
                          6., false, pass_dR_cut, extra_weight);
        if (m_save_truth_zjets) {
          add_fill_hist_sys("truth_ZD_deltaPhi", truth_channel,
                            (m_truth_Z.Phi() - m_truth_D_phi), 100, -2 * M_PI,
                            2 * M_PI, true, pass_dR_cut, extra_weight);
        }
      }
      if (m_unfolding_variables && truth_category == "Matched") {
        // transfer matrix for pT(D)
        add_fill_hist_sys("Dmeson_d_pt_transfer_matrix", truth_channel,
                          m_DMesons_pt->at(D_index) * Charm::GeV, m_truth_D_pt,
                          m_zd_differential_d_pt_bins.get_n_differential(),
                          m_zd_differential_d_pt_bins.get_differential_bins(),
                          m_zd_differential_d_pt_bins.get_n_differential(),
                          m_zd_differential_d_pt_bins.get_differential_bins(),
                          true, pass_dR_cut, extra_weight);
        add_fill_hist_sys("Dmeson_truth_differential_d_pt", truth_channel,
                          m_truth_D_pt,
                          m_zd_differential_d_pt_bins.get_n_differential(),
                          m_zd_differential_d_pt_bins.get_differential_bins(),
                          true, pass_dR_cut, extra_weight);
        add_fill_hist_sys("Dmeson_differential_d_pt", truth_channel,
                          m_DMesons_pt->at(D_index) * Charm::GeV,
                          m_zd_differential_d_pt_bins.get_n_differential(),
                          m_zd_differential_d_pt_bins.get_differential_bins(),
                          true, pass_dR_cut, extra_weight);

        // transfer matrix for pT(Z)
        add_fill_hist_sys("Dmeson_z_pt_transfer_matrix", truth_channel,
                          m_sys_Z_pt, m_truth_Z.Pt(),
                          m_zd_differential_z_pt_bins.get_n_differential(),
                          m_zd_differential_z_pt_bins.get_differential_bins(),
                          m_zd_differential_z_pt_bins.get_n_differential(),
                          m_zd_differential_z_pt_bins.get_differential_bins(),
                          true, pass_dR_cut, extra_weight);
        add_fill_hist_sys("Dmeson_truth_differential_z_pt", truth_channel,
                          m_truth_Z.Pt(),
                          m_zd_differential_z_pt_bins.get_n_differential(),
                          m_zd_differential_z_pt_bins.get_differential_bins(),
                          true, pass_dR_cut, extra_weight);
        add_fill_hist_sys("Dmeson_differential_z_pt", truth_channel, m_sys_Z_pt,
                          m_zd_differential_z_pt_bins.get_n_differential(),
                          m_zd_differential_z_pt_bins.get_differential_bins(),
                          true, pass_dR_cut, extra_weight);
      }
      DMesonRec::fill_D_plus_histograms(this, truth_channel, D_index,
                                        pass_dR_cut, extra_weight);

      // Add truth matched hist if requested
      if (m_truth_D && matched_signal && m_is_mc) {
        int truthIndex =
            TruthInfo::getMatchedBarcode(m_DMesons_truthBarcode->at(D_index));
        std::string truth_channel;
        // accounting for presence of neutrals in truth D+ decay mode (RM =
        // Right Mode)
        if (fabs(truth_daughter_mass(truthIndex) - DP_MASS) < 40) {
          truth_channel = channel + "_recoMatch_truth_Dplus_RM";
          TruthInfo::fill_truth_D_histograms(this, truth_channel, truthIndex);
        }
        truth_channel = channel + "_Dplus_recoMatch_truth";
        TruthInfo::fill_truth_D_histograms(this, truth_channel, truthIndex);
      }
    }
  }

  for (auto channel : used_channels) {
    std::vector<float> multiplicity;
    for (unsigned int j = 0; j < m_nSyst; j++) {
      multiplicity.push_back(multiplicity_counter.at(j)[channel]);
    }
    if (!m_fit_variables_only) {
      add_fill_hist_sys("N_Dplus", channel, multiplicity, 20, -0.5, 19.5, true);
    }
  }

  // pure D meson truth
  if (m_is_mc && m_truth_D) {
    for (unsigned int i = 0; i < m_truth_D_plus_mesons.size(); i++) {
      unsigned int index = m_truth_D_plus_mesons.at(i);
      std::string truth_channel;
      // accounting for presence of neutrals in truth D+ decay mode (RM = Right
      // Mode)
      if (fabs(truth_daughter_mass(index) - DP_MASS) < 40) {
        truth_channel = channel + "_truth_Dplus_RM";
        TruthInfo::fill_truth_D_histograms(this, truth_channel, index);
      }
      truth_channel = channel + "_Dplus_truth";
      TruthInfo::fill_truth_D_histograms(this, truth_channel, index);
      if (fabs(truth_daughter_mass(index) - DP_MASS) <
          40)  // && pass_track_truth(index))
        TruthInfo::fill_truth_D_histograms(
            this, channel + "_truth_Dplus_RM_passTrackTruth", index);
    }
  }
}

void ZCharmLoopBase::fill_D_star_histograms(std::string channel) {
  // number of D mesons
  if (m_fill_zjets && !m_fit_variables_only) {
    add_fill_hist_sys("N_Dstar", channel, m_D_star_indices.size(), 20, -0.5,
                      19.5, true);
  }

  // single entry region with at least one D meson
  if (m_do_single_entry_D && m_D_star_indices.size()) {
    std::vector<std::string> single_entry_channel =
        std::vector<std::string>(m_nSyst, channel);
    std::vector<bool> sys_at_least_one_D = std::vector<bool>(m_nSyst, false);
    std::vector<float> multiplicity = std::vector<float>(m_nSyst, 0);
    bool has_D = false;
    for (unsigned int j = 0; j < m_nSyst; j++) {
      // loop over D star mesons
      for (unsigned int i = 0; i < m_D_star_indices.size(); i++) {
        // retreive the D meson
        unsigned int D_index = m_D_star_indices.at(i);

        // dR cut
        float dRlep1 = 999;
        float dRlep2 = 999;
        if (m_do_lep_presel) {
          dRlep1 = DMesonRec::D_meson_lepton_dR(D_index, m_sys_lep1_eta.at(j),
                                                m_sys_lep1_phi.at(j));
          dRlep2 = DMesonRec::D_meson_lepton_dR(D_index, m_sys_lep2_eta.at(j),
                                                m_sys_lep2_phi.at(j));
        }
        if (dRlep1 >= 0.3 && dRlep2 >= 0.3) {
          sys_at_least_one_D.at(j) = true;
          multiplicity.at(j) += 1;
        }
      }
      if (multiplicity.at(j) > 0) {
        single_entry_channel.at(j) += "_Dstar";
        has_D = true;
      }
    }

    if (m_do_single_entry_D_only && m_have_one_D) {
      return;
    } else if (m_do_single_entry_D_only && !m_have_one_D && has_D) {
      m_have_one_D = true;
    }

    // if (m_do_lep_presel)
    // {
    //     fill_lepton_histograms();
    // }
    if (m_do_PV) {
      fill_PV_histograms(single_entry_channel, sys_at_least_one_D);
    }
    add_fill_hist_sys("N_Dstar", single_entry_channel, multiplicity, 20, -0.5,
                      19.5, true, sys_at_least_one_D);
    if (m_do_single_entry_D_only) {
      return;
    }
  }

  // counters for D multiplicity
  std::vector<std::unordered_map<std::string, unsigned int>>
      multiplicity_counter;
  std::set<std::string> used_channels;

  // loop over D star mesons
  for (unsigned int i = 0; i < m_D_star_indices.size(); i++) {
    // retreive the D meson
    const unsigned int D_index = m_D_star_indices.at(i);

    // truth category
    bool matched_signal = false;
    std::string truth_category = "";
    int truthIndex = -1;
    if (m_truth_matchD && m_is_mc) {
      truth_category = TruthInfo::get_truth_category(
          this, D_index, "Kpipi", m_fiducial_selection, m_zplusd_mode);
      truthIndex =
          TruthInfo::getMatchedBarcode(m_DMesons_truthBarcode->at(D_index));
    }
    if (truth_category == "Matched") {
      matched_signal = true;
      m_truth_D_pt = m_TruthParticles_Selected_pt->at(truthIndex) * Charm::GeV;
      m_truth_D_eta = m_TruthParticles_Selected_eta->at(truthIndex);
      m_truth_D_mass = truth_daughter_mass(truthIndex);
      m_truth_D_phi = m_TruthParticles_Selected_phi->at(truthIndex);
      if (m_bin_in_truth_D_pt) {
        truth_category +=
            "_truth_pt_bin" +
            std::to_string(
                m_zd_differential_d_pt_bins.get_pt_bin(m_truth_D_pt) + 1);
      }
    } else {
      m_truth_D_pt = 0;
      m_truth_D_eta = -999;
      m_truth_D_mass = 0;
      m_truth_D_phi = -999;
    }

    // signal only
    if (m_signal_only && !matched_signal) {
      continue;
    }

    // dR w.r.t. the lepton depends on systematic
    std::vector<Float_t> sys_dRlep1 = std::vector<float>(m_nSyst, 0.);
    std::vector<Float_t> sys_dRlep2 = std::vector<float>(m_nSyst, 0.);

    // dR w.r.t. the b-jet depends on systematic
    std::vector<Float_t> sys_dRbjet = std::vector<float>(m_nSyst, 0.);

    // D meson mass
    m_sys_dmeson_mass =
        std::vector<float>(m_nSyst, m_DMesons_DeltaMass->at(D_index));

    // OS or SS region
    std::vector<std::string> charged_channel =
        std::vector<std::string>(m_nSyst, channel);
    std::vector<bool> pass_dR_cut = std::vector<bool>(m_nSyst, false);
    std::vector<double> extra_weight = std::vector<double>(m_nSyst, 1.0);

    for (unsigned int j = 0; j < m_nSyst; j++) {
      multiplicity_counter.push_back({});
      if (!m_do_lep_presel || (event_pass_sys(j))) {
        // dR w.r.t. the lepton
        float dRlep1 = 999;
        float dRlep2 = 999;
        if (m_do_lep_presel) {
          dRlep1 = DMesonRec::D_meson_lepton_dR(D_index, m_sys_lep1_eta.at(j),
                                                m_sys_lep1_phi.at(j));
          dRlep2 = DMesonRec::D_meson_lepton_dR(D_index, m_sys_lep2_eta.at(j),
                                                m_sys_lep2_phi.at(j));
        }
        sys_dRlep1.at(j) = dRlep1;
        sys_dRlep2.at(j) = dRlep2;

        // dR cut
        if (dRlep1 < 0.3 || dRlep2 < 0.3) {
          continue;
        }

        // dR w.r.t. b-jets
        float dRbjet = 999;

        if (m_jet_selection && m_do_lep_presel) {
          auto jets = m_jets_sys.at(j);
          for (auto &jet : jets) {
            if (m_AnalysisJets_ftag_select_DL1r_FixedCutBEff_70->at(jet)) {
              float dR = get_dr_bjet_Dmeson(jet, D_index);
              if (dR < dRbjet) {
                dRbjet = dR;
              }
            }
          }
          sys_dRbjet.at(j) = dRbjet;
        }

        // sys string
        std::string sys_string = "";
        if (j > 0) {
          sys_string = m_systematics.at(j - 1);
        }

        // track efficiency sys weight
        double track_weight = m_track_sys_helper.get_track_eff_weight(
            sys_string, m_DMesons_pt->at(D_index) * Charm::GeV,
            m_DMesons_eta->at(D_index), m_DMesons_pdgId->at(D_index));
        extra_weight.at(j) *= track_weight;

        // smear mass
        double smear_mdiff = m_mass_smear_helper.get_smeared_mass(
            sys_string,
            m_zd_differential_d_pt_bins.get_pt_bin(m_DMesons_pt->at(D_index) *
                                                   Charm::GeV),
            m_DMesons_eta->at(D_index), m_DMesons_pdgId->at(D_index),
            m_DMesons_DeltaMass->at(D_index) * Charm::MeV);
        m_sys_dmeson_mass.at(j) = smear_mdiff;

        // modeling uncertainty
        if (matched_signal) {
          double fid_eff_weight = 1.0;
          if (m_bin_in_D_pt) {
            fid_eff_weight = m_modeling_uncertainty_helper.get_weight_pt(
                sys_string, m_truth_D_pt, "Dstar");
          }
          // else if (m_bin_in_lep_eta)
          // {
          //     fid_eff_weight =
          //     m_modeling_uncertainty_helper.get_weight_eta(sys_string,
          //     m_truth_abs_lep_eta, "Dstar");
          // }
          extra_weight.at(j) *= fid_eff_weight;
        }

        // replace lepton charge with the truth D charge for SPG samples
        //  - OS if they have the same charge
        //  - SS if they have the opposite charge
        // if (m_data_period == "ForcedDecay")
        // {
        //     lep_charge = -1 * m_TruthParticles_Selected_pdgId->at(0) /
        //     fabs(m_TruthParticles_Selected_pdgId->at(0));
        // }
        pass_dR_cut.at(j) = true;
        charged_channel.at(j) += "_Dstar";
        multiplicity_counter.at(j)[charged_channel.at(j)]++;
        used_channels.insert(charged_channel.at(j));
      }
    }

    if (m_do_lep_presel) {
      fill_lepton_histograms(charged_channel, pass_dR_cut, extra_weight);
    }
    if (m_do_PV) {
      fill_PV_histograms(charged_channel, pass_dR_cut, extra_weight);
    }

    // D star meson specific histograms
    if (!m_fit_variables_only) {
      add_fill_hist_sys("Dmeson_dRlep1", charged_channel, sys_dRlep1, 120, 0.,
                        6., false, pass_dR_cut, extra_weight);
      add_fill_hist_sys("Dmeson_dRlep2", charged_channel, sys_dRlep2, 120, 0.,
                        6., false, pass_dR_cut, extra_weight);
    }
    DMesonRec::fill_D_star_histograms(this, charged_channel, D_index,
                                      pass_dR_cut, extra_weight);

    // D meson truth match
    if (m_is_mc && m_truth_matchD) {
      std::vector<std::string> truth_channel = charged_channel;
      std::vector<std::string> truth_channel_fid =
          std::vector<std::string>(m_nSyst, channel);

      for (unsigned int j = 0; j < m_nSyst; j++) {
        if (!m_do_lep_presel || (event_pass_sys(j) && pass_dR_cut.at(j))) {
          truth_channel.at(j) += "_" + truth_category;
          multiplicity_counter.at(j)[truth_channel.at(j)]++;
          used_channels.insert(truth_channel.at(j));
        }
      }

      if (m_do_lep_presel) {
        fill_lepton_histograms(truth_channel, pass_dR_cut, extra_weight);
      }
      if (!m_fit_variables_only) {
        add_fill_hist_sys("truth_Dmeson_pt", truth_channel, m_truth_D_pt, 150,
                          0, 150., true, pass_dR_cut, extra_weight);
        add_fill_hist_sys("truth_Dmeson_eta", truth_channel, m_truth_D_eta, 100,
                          -3.0, 3.0, true, pass_dR_cut, extra_weight);
        add_fill_hist_sys("truth_Dmeson_mass", truth_channel,
                          m_truth_D_mass * Charm::GeV, 200, 1.4, 2.4, false,
                          pass_dR_cut, extra_weight);
        add_fill_hist_sys("truth_Dmeson_phi", truth_channel, m_truth_D_phi, 100,
                          -M_PI, M_PI, false, pass_dR_cut, extra_weight);
        add_fill_hist_sys("Dmeson_dRlep1", truth_channel, sys_dRlep1, 120, 0.,
                          6., false, pass_dR_cut, extra_weight);
        add_fill_hist_sys("Dmeson_dRlep2", truth_channel, sys_dRlep2, 120, 0.,
                          6., false, pass_dR_cut, extra_weight);
        add_fill_hist_sys("Dmeson_dRbjet", truth_channel, sys_dRbjet, 120, 0.,
                          6., false, pass_dR_cut, extra_weight);
        if (m_save_truth_zjets) {
          add_fill_hist_sys("truth_ZD_deltaPhi", truth_channel,
                            (m_truth_Z.Phi() - m_truth_D_phi), 100, -2 * M_PI,
                            2 * M_PI, true, pass_dR_cut, extra_weight);
        }
      }
      if (m_unfolding_variables && truth_category == "Matched") {
        // transfer matrix for pT(D)
        add_fill_hist_sys("Dmeson_d_pt_transfer_matrix", truth_channel,
                          m_DMesons_pt->at(D_index) * Charm::GeV, m_truth_D_pt,
                          m_zd_differential_d_pt_bins.get_n_differential(),
                          m_zd_differential_d_pt_bins.get_differential_bins(),
                          m_zd_differential_d_pt_bins.get_n_differential(),
                          m_zd_differential_d_pt_bins.get_differential_bins(),
                          true, pass_dR_cut, extra_weight);
        add_fill_hist_sys("Dmeson_truth_differential_d_pt", truth_channel,
                          m_truth_D_pt,
                          m_zd_differential_d_pt_bins.get_n_differential(),
                          m_zd_differential_d_pt_bins.get_differential_bins(),
                          true, pass_dR_cut, extra_weight);
        add_fill_hist_sys("Dmeson_differential_d_pt", truth_channel,
                          m_DMesons_pt->at(D_index) * Charm::GeV,
                          m_zd_differential_d_pt_bins.get_n_differential(),
                          m_zd_differential_d_pt_bins.get_differential_bins(),
                          true, pass_dR_cut, extra_weight);

        // transfer matrix for pT(Z)
        add_fill_hist_sys("Dmeson_z_pt_transfer_matrix", truth_channel,
                          m_sys_Z_pt, m_truth_Z.Pt(),
                          m_zd_differential_z_pt_bins.get_n_differential(),
                          m_zd_differential_z_pt_bins.get_differential_bins(),
                          m_zd_differential_z_pt_bins.get_n_differential(),
                          m_zd_differential_z_pt_bins.get_differential_bins(),
                          true, pass_dR_cut, extra_weight);
        add_fill_hist_sys("Dmeson_truth_differential_z_pt", truth_channel,
                          m_truth_Z.Pt(),
                          m_zd_differential_z_pt_bins.get_n_differential(),
                          m_zd_differential_z_pt_bins.get_differential_bins(),
                          true, pass_dR_cut, extra_weight);
        add_fill_hist_sys("Dmeson_differential_z_pt", truth_channel, m_sys_Z_pt,
                          m_zd_differential_z_pt_bins.get_n_differential(),
                          m_zd_differential_z_pt_bins.get_differential_bins(),
                          true, pass_dR_cut, extra_weight);
      }
      DMesonRec::fill_D_star_histograms(this, truth_channel, D_index,
                                        pass_dR_cut, extra_weight);
      // Add truth matched hist if requested
      if (m_truth_D && matched_signal && m_is_mc) {
        int truthIndex =
            TruthInfo::getMatchedBarcode(m_DMesons_truthBarcode->at(D_index));
        std::string truth_channel;
        // accounting for presence of neutrals in truth D+ decay mode (RM =
        // Right Mode)
        if (fabs(truth_daughter_mass(truthIndex) - DSTAR_MASS) < 40)
          truth_channel = channel + "_recoMatch_truth_Dstar_RM";
        else
          truth_channel = channel + "_Dstar_recoMatch_truth";
        +TruthInfo::fill_truth_D_histograms(this, truth_channel, truthIndex);
      }
    }
  }

  // number of D meson candidates
  for (auto channel : used_channels) {
    std::vector<float> multiplicity;
    for (unsigned int j = 0; j < m_nSyst; j++) {
      multiplicity.push_back(multiplicity_counter.at(j)[channel]);
    }
    if (!m_fit_variables_only) {
      add_fill_hist_sys("N_Dstar", channel, multiplicity, 20, -0.5, 19.5, true);
    }
  }

  // pure D meson truth
  if (m_is_mc && m_truth_D) {
    for (unsigned int i = 0; i < m_truth_D_star_mesons.size(); i++) {
      unsigned int index = m_truth_D_star_mesons.at(i);
      std::string truth_channel;
      if (fabs(truth_daughter_mass(index) - DSTAR_MASS) < 40) {
        truth_channel = channel + "_truth_Dstar_RM";
        TruthInfo::fill_truth_D_histograms(this, truth_channel, index);
      }
      truth_channel = channel + "_truth_Dstar";
      TruthInfo::fill_truth_D_histograms(this, truth_channel, index);
    }
  }
}

void ZCharmLoopBase::fill_D_star_pi0_histograms(std::string channel) {
  // number of D mesons
  if (m_fill_zjets && !m_fit_variables_only) {
    add_fill_hist_sys("N_DstarKPiPi0", channel, m_D_star_pi0_indices.size(), 20,
                      -0.5, 19.5, true);
  }

  // single entry region with at least one D meson
  if (m_do_single_entry_D && m_D_star_pi0_indices.size()) {
    std::vector<std::string> single_entry_channel =
        std::vector<std::string>(m_nSyst, channel);
    std::vector<bool> sys_at_least_one_D = std::vector<bool>(m_nSyst, false);
    std::vector<float> multiplicity = std::vector<float>(m_nSyst, 0);
    bool has_D = false;
    for (unsigned int j = 0; j < m_nSyst; j++) {
      // loop over D star D*pi0 mesons
      for (unsigned int i = 0; i < m_D_star_pi0_indices.size(); i++) {
        // retreive the D meson
        unsigned int D_index = m_D_star_pi0_indices.at(i);

        // dR cut
        float dRlep1 = 999;
        float dRlep2 = 999;
        if (m_do_lep_presel) {
          dRlep1 = DMesonRec::D_meson_lepton_dR(D_index, m_sys_lep1_eta.at(j),
                                                m_sys_lep1_phi.at(j));
          dRlep2 = DMesonRec::D_meson_lepton_dR(D_index, m_sys_lep2_eta.at(j),
                                                m_sys_lep2_phi.at(j));
        }
        if (dRlep1 >= 0.3 && dRlep2 >= 0.3) {
          sys_at_least_one_D.at(j) = true;
          multiplicity.at(j) += 1;
        }
      }
      if (multiplicity.at(j) > 0) {
        single_entry_channel.at(j) += "_DstarKPiPi0";
        has_D = true;
      }
    }
    if (m_do_single_entry_D_only && m_have_one_D)
      return;
    else if (m_do_single_entry_D_only && !m_have_one_D && has_D)
      m_have_one_D = true;

    // if (m_do_lep_presel)
    // {
    //     fill_lepton_histograms();
    // }
    if (m_do_PV) {
      fill_PV_histograms(single_entry_channel, sys_at_least_one_D);
    }
    add_fill_hist_sys("N_DstarKPiPi0", single_entry_channel, multiplicity, 20,
                      -0.5, 19.5, true, sys_at_least_one_D);
    if (m_do_single_entry_D_only) {
      return;
    }
  }

  // counters for D multiplicity
  std::vector<std::unordered_map<std::string, unsigned int>>
      multiplicity_counter;
  std::set<std::string> used_channels;

  // loop over D star mesons
  for (unsigned int i = 0; i < m_D_star_pi0_indices.size(); i++) {
    // retreive the D meson
    unsigned int D_index = m_D_star_pi0_indices.at(i);

    // truth category
    bool matched_signal = false;
    std::string truth_category = "";
    int truthIndex = -1;
    if (m_truth_matchD && m_is_mc) {
      truth_category = TruthInfo::get_truth_category(
          this, D_index, "Kpipipi0", m_fiducial_selection, m_zplusd_mode);
      truthIndex =
          TruthInfo::getMatchedBarcode(m_DMesons_truthBarcode->at(D_index));
    }
    if (truth_category == "Matched") {
      m_truth_D_pt = m_TruthParticles_Selected_pt->at(truthIndex) * Charm::GeV;
      m_truth_D_eta = m_TruthParticles_Selected_eta->at(truthIndex);
      m_truth_D_mass = truth_daughter_mass(truthIndex);
      if (m_bin_in_truth_D_pt) {
        truth_category +=
            "_truth_pt_bin" +
            std::to_string(
                m_zd_differential_d_pt_bins.get_pt_bin(m_truth_D_pt) + 1);
      }
    } else {
      m_truth_D_pt = 0;
      m_truth_D_eta = -999;
      m_truth_D_mass = 0;
    }

    // signal only
    if (m_signal_only && !matched_signal) {
      continue;
    }

    // dR w.r.t. the lepton depends on systematic
    std::vector<Float_t> sys_dRlep1 = std::vector<float>(m_nSyst, 0.);
    std::vector<Float_t> sys_dRlep2 = std::vector<float>(m_nSyst, 0.);

    // dR w.r.t. the b-jet depends on systematic
    std::vector<Float_t> sys_dRbjet = std::vector<float>(m_nSyst, 0.);

    // OS or SS region
    std::vector<std::string> charged_channel =
        std::vector<std::string>(m_nSyst, channel);
    std::vector<bool> pass_dR_cut = std::vector<bool>(m_nSyst, false);
    std::vector<double> extra_weight = std::vector<double>(m_nSyst, 1.0);

    for (unsigned int j = 0; j < m_nSyst; j++) {
      multiplicity_counter.push_back({});
      if (!m_do_lep_presel || (event_pass_sys(j))) {
        // dR w.r.t. the lepton
        float dRlep1 = 999;
        float dRlep2 = 999;
        if (m_do_lep_presel) {
          dRlep1 = DMesonRec::D_meson_lepton_dR(D_index, m_sys_lep1_eta.at(j),
                                                m_sys_lep1_phi.at(j));
          dRlep2 = DMesonRec::D_meson_lepton_dR(D_index, m_sys_lep2_eta.at(j),
                                                m_sys_lep2_phi.at(j));
        }
        sys_dRlep1.at(j) = dRlep1;
        sys_dRlep2.at(j) = dRlep2;

        // dR cut
        if (dRlep1 < 0.3 || dRlep2 < 0.3) {
          continue;
        }

        // dR w.r.t. b-jets
        float dRbjet = 999;

        if (m_jet_selection && m_do_lep_presel) {
          auto jets = m_jets_sys.at(j);
          for (auto &jet : jets) {
            if (m_AnalysisJets_ftag_select_DL1r_FixedCutBEff_70->at(jet)) {
              float dR = get_dr_bjet_Dmeson(jet, D_index);
              if (dR < dRbjet) {
                dRbjet = dR;
              }
            }
          }
          sys_dRbjet.at(j) = dRbjet;
        }

        // sys string
        std::string sys_string = "";
        if (j > 0) {
          sys_string = m_systematics.at(j - 1);
        }

        // track efficiency sys weight
        double track_weight = m_track_sys_helper.get_track_eff_weight(
            sys_string, m_DMesons_pt->at(D_index) * Charm::GeV,
            m_DMesons_eta->at(D_index), m_DMesons_pdgId->at(D_index));
        extra_weight.at(j) *= track_weight;

        // modeling uncertainty
        // if (matched_signal)
        // {
        //     double fid_eff_weight =
        //     m_modeling_uncertainty_helper.get_weight(sys_string,
        //     m_truth_D_pt, 413); extra_weight.at(j) *= fid_eff_weight;
        // }
        // // weight for SPG forced decay sample
        // if (matched_signal && m_reweight_spg && m_data_period ==
        // "ForcedDecay")
        // {
        //     extra_weight.at(j) *=
        //     m_spg_forced_decay_helper.get_weight(m_DMesons_m->at(D_index)
        //     * Charm::GeV,
        //                                                                m_zd_differential_d_pt_bins.get_pt_bin(m_truth_D_pt),
        //                                                                "Dstar");
        // }

        // replace lepton charge with the truth D charge for SPG samples
        //  - OS if they have the same charge
        //  - SS if they have the opposite charge
        // if (m_data_period == "ForcedDecay")
        // {
        //     lep_charge = -1 * m_TruthParticles_Selected_pdgId->at(0) /
        //     fabs(m_TruthParticles_Selected_pdgId->at(0));
        // }
        pass_dR_cut.at(j) = true;
        charged_channel.at(j) += "_DstarKPiPi0";
        multiplicity_counter.at(j)[charged_channel.at(j)]++;
        used_channels.insert(charged_channel.at(j));
      }
    }

    if (m_do_lep_presel) {
      fill_lepton_histograms(charged_channel, pass_dR_cut, extra_weight);
    }
    if (m_do_PV) {
      fill_PV_histograms(charged_channel, pass_dR_cut, extra_weight);
    }

    // D star pi0 meson specific histograms
    if (!m_fit_variables_only) {
      add_fill_hist_sys("Dmeson_dRlep1", charged_channel, sys_dRlep1, 120, 0.,
                        6., false, pass_dR_cut, extra_weight);
      add_fill_hist_sys("Dmeson_dRlep2", charged_channel, sys_dRlep2, 120, 0.,
                        6., false, pass_dR_cut, extra_weight);
    }
    DMesonRec::fill_D_star_pi0_histograms(this, charged_channel, D_index,
                                          pass_dR_cut, extra_weight);

    // D meson truth match
    if (m_is_mc && m_truth_matchD) {
      std::vector<std::string> truth_channel = charged_channel;
      std::vector<std::string> truth_channel_fid =
          std::vector<std::string>(m_nSyst, channel);

      for (unsigned int j = 0; j < m_nSyst; j++) {
        if (!m_do_lep_presel || (event_pass_sys(j) && pass_dR_cut.at(j))) {
          truth_channel.at(j) += "_" + truth_category;
          multiplicity_counter.at(j)[truth_channel.at(j)]++;
          used_channels.insert(truth_channel.at(j));
        }
      }

      if (!m_fit_variables_only) {
        add_fill_hist_sys("truth_Dmeson_pt", truth_channel, m_truth_D_pt, 150,
                          0, 150., false, pass_dR_cut, extra_weight);
        add_fill_hist_sys("truth_Dmeson_eta", truth_channel, m_truth_D_eta, 100,
                          -3.0, 3.0, false, pass_dR_cut, extra_weight);
        add_fill_hist_sys("truth_Dmeson_mass", truth_channel,
                          m_truth_D_mass * Charm::GeV, 200, 1.4, 2.4, false,
                          pass_dR_cut, extra_weight);
        add_fill_hist_sys("Dmeson_dRlep1", truth_channel, sys_dRlep1, 120, 0.,
                          6., false, pass_dR_cut, extra_weight);
        add_fill_hist_sys("Dmeson_dRlep2", truth_channel, sys_dRlep2, 120, 0.,
                          6., false, pass_dR_cut, extra_weight);
        add_fill_hist_sys("Dmeson_dRbjet", truth_channel, sys_dRbjet, 120, 0.,
                          6., false, pass_dR_cut, extra_weight);
      }
      DMesonRec::fill_D_star_pi0_histograms(this, truth_channel, D_index,
                                            pass_dR_cut, extra_weight);
      // Add truth matched hist if requested
      if (m_truth_D && truth_category == "Matched" && m_is_mc) {
        int truthIndex =
            TruthInfo::getMatchedBarcode(m_DMesons_truthBarcode->at(D_index));
        std::string truth_channel;
        // accounting for presence of neutrals in truth D*pi0 decay mode (RM =
        // Right Mode)
        if (fabs(truth_daughter_mass(truthIndex) - DSTAR_MASS) < 40)
          truth_channel = channel + "_recoMatch_truth_DstarKPiPi0_RM";
        else
          truth_channel = channel + "_DstarKPiPi0_recoMatch_truth";
        TruthInfo::fill_truth_D_histograms(this, truth_channel, truthIndex);
      }
    }
  }

  // number of D meson candidates
  for (auto channel : used_channels) {
    std::vector<float> multiplicity;
    for (unsigned int j = 0; j < m_nSyst; j++) {
      multiplicity.push_back(multiplicity_counter.at(j)[channel]);
    }
    if (!m_fit_variables_only) {
      add_fill_hist_sys("N_DstarKPiPi0", channel, multiplicity, 20, -0.5, 19.5,
                        true);
    }
  }

  // pure D meson truth
  if (m_is_mc && m_truth_D) {
    for (unsigned int i = 0; i < m_truth_D_star_pi0_mesons.size(); i++) {
      unsigned int index = m_truth_D_star_pi0_mesons.at(i);
      std::string truth_channel;
      if (fabs(truth_daughter_mass(index) - DSTAR_MASS) < 40) {
        truth_channel = channel + "_truth_DstarKPiPi0_RM";
        TruthInfo::fill_truth_D_histograms(this, truth_channel, index);
      }
      truth_channel = channel + "_truth_DstarKPiPi0";
      TruthInfo::fill_truth_D_histograms(this, truth_channel, index);
    }
  }
}

void ZCharmLoopBase::fill_D_s_histograms(std::string channel) {
  // number of D mesons
  if (m_fill_zjets && !m_fit_variables_only) {
    add_fill_hist_sys("N_Ds", channel, m_D_s_indices.size(), 20, -0.5, 19.5,
                      true);
  }

  // single entry region with at least one D meson
  if (m_do_single_entry_D) {
    std::vector<std::string> single_entry_channel =
        std::vector<std::string>(m_nSyst, channel);
    std::vector<bool> sys_at_least_one_D = std::vector<bool>(m_nSyst, false);
    std::vector<float> multiplicity = std::vector<float>(m_nSyst, 0);
    bool has_D = true;
    for (unsigned int j = 0; j < m_nSyst; j++) {
      // loop over D s mesons
      for (unsigned int i = 0; i < m_D_s_indices.size(); i++) {
        // retreive the D meson
        unsigned int D_index = m_D_s_indices.at(i);

        // dR cut
        float dRlep1 = 999;
        float dRlep2 = 999;
        if (m_do_lep_presel) {
          dRlep1 = DMesonRec::D_meson_lepton_dR(D_index, m_sys_lep1_eta.at(j),
                                                m_sys_lep1_phi.at(j));
          dRlep2 = DMesonRec::D_meson_lepton_dR(D_index, m_sys_lep2_eta.at(j),
                                                m_sys_lep2_phi.at(j));
        }
        if (dRlep1 >= 0.3 && dRlep2 >= 0.3) {
          sys_at_least_one_D.at(j) = true;
          multiplicity.at(j) += 1;
        }
      }
      if (multiplicity.at(j) > 0) {
        single_entry_channel.at(j) += "_Ds";
        has_D = true;
      }
    }
    if (m_do_single_entry_D_only && m_have_one_D)
      return;
    else if (m_do_single_entry_D_only && !m_have_one_D && has_D)
      m_have_one_D = true;

    // if (m_do_lep_presel)
    // {
    //     fill_lepton_histograms();
    // }
    if (m_do_PV) {
      fill_PV_histograms(single_entry_channel, sys_at_least_one_D);
    }
    add_fill_hist_sys("N_Ds", single_entry_channel, multiplicity, 20, -0.5,
                      19.5, true, sys_at_least_one_D);
    if (m_do_single_entry_D_only) {
      return;
    }
  }

  // counters for D multiplicity
  std::vector<std::unordered_map<std::string, unsigned int>>
      multiplicity_counter;
  std::set<std::string> used_channels;

  // loop over D s mesons
  for (unsigned int i = 0; i < m_D_s_indices.size(); i++) {
    // retreive the D meson
    unsigned int D_index = m_D_s_indices.at(i);

    // dR w.r.t. the lepton depends on systematic
    std::vector<Float_t> sys_dRlep1 = std::vector<float>(m_nSyst, 0.);
    std::vector<Float_t> sys_dRlep2 = std::vector<float>(m_nSyst, 0.);

    // OS or SS region
    std::vector<std::string> charged_channel =
        std::vector<std::string>(m_nSyst, channel);
    std::vector<bool> pass_dR_cut = std::vector<bool>(m_nSyst, false);
    for (unsigned int j = 0; j < m_nSyst; j++) {
      multiplicity_counter.push_back({});
      if (!m_do_lep_presel || (event_pass_sys(j))) {
        // dR w.r.t. the lepton
        float dRlep1 = 999;
        float dRlep2 = 999;
        if (m_do_lep_presel) {
          dRlep1 = DMesonRec::D_meson_lepton_dR(D_index, m_sys_lep1_eta.at(j),
                                                m_sys_lep1_phi.at(j));
          dRlep2 = DMesonRec::D_meson_lepton_dR(D_index, m_sys_lep2_eta.at(j),
                                                m_sys_lep2_phi.at(j));
        }
        sys_dRlep1.at(j) = dRlep1;
        sys_dRlep2.at(j) = dRlep2;

        // dR cut
        if (dRlep1 < 0.3 || dRlep2 < 0.3) {
          continue;
        }

        pass_dR_cut.at(j) = true;
        charged_channel.at(j) += "_Ds";
        multiplicity_counter.at(j)[charged_channel.at(j)]++;
        used_channels.insert(charged_channel.at(j));
      }
    }

    // D s meson specific histograms
    if (m_do_lep_presel && !m_fit_variables_only) {
      fill_lepton_histograms(charged_channel, pass_dR_cut);
    }
    if (m_do_PV) {
      fill_PV_histograms(charged_channel, pass_dR_cut);
    }

    if (!m_fit_variables_only) {
      add_fill_hist_sys("Dmeson_dRlep1", charged_channel, sys_dRlep1, 120, 0.,
                        6., false, pass_dR_cut);
      add_fill_hist_sys("Dmeson_dRlep2", charged_channel, sys_dRlep2, 120, 0.,
                        6., false, pass_dR_cut);
    }
    DMesonRec::fill_D_s_histograms(this, charged_channel, D_index, pass_dR_cut);

    // D meson truth match
    if (m_is_mc && m_truth_matchD) {
      std::string truth_category = TruthInfo::get_truth_category(this, D_index);
      std::vector<std::string> truth_channel = charged_channel;
      for (unsigned int j = 0; j < m_nSyst; j++) {
        if (!m_do_lep_presel || (event_pass_sys(j) && pass_dR_cut.at(j))) {
          truth_channel.at(j) += "_" + truth_category;
          multiplicity_counter.at(j)[truth_channel.at(j)]++;
          used_channels.insert(truth_channel.at(j));
        }
      }
      if (!m_fit_variables_only) {
        add_fill_hist_sys("Dmeson_dRlep1", truth_channel, sys_dRlep1, 120, 0.,
                          6., false, pass_dR_cut);
        add_fill_hist_sys("Dmeson_dRlep2", truth_channel, sys_dRlep2, 120, 0.,
                          6., false, pass_dR_cut);
      }
      DMesonRec::fill_D_s_histograms(this, truth_channel, D_index, pass_dR_cut);
      // Add truth matched hist if requested
      if (m_truth_D && truth_category == "Matched") {
        int truthIndex =
            TruthInfo::getMatchedBarcode(m_DMesons_truthBarcode->at(D_index));
        std::string truth_channel;
        // accounting for presence of neutrals in truth D+ decay mode (RM =
        // Right Mode)
        if (fabs(truth_daughter_mass(truthIndex) - DS_MASS) < 40) {
          truth_channel = channel + "_recoMatch_truth_Ds_RM";
          TruthInfo::fill_truth_D_histograms(this, truth_channel, truthIndex);
        }
        truth_channel = channel + "_Ds_recoMatch_truth";
        TruthInfo::fill_truth_D_histograms(this, truth_channel, truthIndex);
      }
      // Add truth matched hist if requested
      // if(m_truth_D && truth_category == "Matched"){
      //     int truthIndex =
      //     TruthInfo::getMatchedBarcode(m_DMesons_truthBarcode->at(D_index));
      //     std::string truth_channel;
      //     // accounting for presence of neutrals in truth D+ decay mode (RM =
      //     Right Mode) if(fabs(truth_daughter_mass(truthIndex) - DP_MASS) <
      //     40) truth_channel = channel + "_recoMatch_truth_Ds_RM"+
      //     std::to_string(Ds_pair.second); else truth_channel = channel +
      //     "_Ds_recoMatch_truth" + std::to_string(Ds_pair.second);
      //     TruthInfo::fill_truth_D_histograms(this, truth_channel,
      //     truthIndex);
      // }
    }
  }

  // counting D multiplicity
  for (auto channel : used_channels) {
    std::vector<float> multiplicity;
    for (unsigned int j = 0; j < m_nSyst; j++) {
      multiplicity.push_back(multiplicity_counter.at(j)[channel]);
    }
    if (!m_fit_variables_only) {
      add_fill_hist_sys("N_Ds", channel, multiplicity, 20, -0.5, 19.5, true);
    }
  }

  // // pure D meson truth
  if (m_is_mc && m_truth_D) {
    for (unsigned int i = 0; i < m_truth_D_s_mesons.size(); i++) {
      unsigned int index = m_truth_D_s_mesons.at(i);
      std::string truth_channel;
      // accounting for presence of neutrals in truth D+ decay mode (RM = Right
      // Mode)
      if (fabs(truth_daughter_mass(index) - DS_MASS) < 40) {
        truth_channel = channel + "_truth_Ds_RM";
        TruthInfo::fill_truth_D_histograms(this, truth_channel, index);
      }
      truth_channel = channel + "_truth_Ds";
      TruthInfo::fill_truth_D_histograms(this, truth_channel, index);
    }
  }
}

void ZCharmLoopBase::fill_histograms_with_regions() {
  if (!m_do_lep_presel) {
    fill_histograms();
    return;
  }

  // Fill histograms for MJ CR (T SS, !T SS, !T OS)
  if (m_do_mj_estimation) {
    for (unsigned int i = 0; i < m_nSyst; i++) {
      if (m_sys_anti_tight.at(i)) {
        m_channel_sys.at(i) = "AntiTight_" + m_channel_sys.at(i);
      }
      if (m_sys_SS.at(i)) {
        m_channel_sys.at(i) = m_channel_sys.at(i) + "_SS";
      }
    }
  }

  // Fill histograms for passed Z truth fiducial (Fid) and failed z truth
  // fiducial (NoFid) events
  if ((m_fiducial_selection || m_save_truth_zjets) && m_do_ztruth_fid_study) {
    if (m_sys_pass_ztruth_fid_cuts) {
      fill_histograms("_Fid");
    } else {
      fill_histograms("_NoFid");
    }
  }

  fill_histograms();
}

float ZCharmLoopBase::get_dr_bjet_Dmeson(unsigned int jet, unsigned int i) {
  float deta = m_DMesons_eta->at(i) - m_AnalysisJets_eta->at(jet);
  float dphi =
      TVector2::Phi_mpi_pi(m_DMesons_phi->at(i) - m_AnalysisJets_phi->at(jet));
  return TMath::Sqrt(deta * deta + dphi * dphi);
}

void ZCharmLoopBase::jet_selection_for_sys() {
  if (!m_do_lep_presel) {
    return;
  }
  for (unsigned int i = 0; i < m_nSyst; i++) {
    if (m_channel_sys.at(i) == "") {
      continue;
    }
    auto jets = m_jets_sys.at(i);
    for (auto &jet : jets) {
      // ftag SF
      if (m_is_mc) {
        if (fabs(m_AnalysisJets_eta->at(jet)) <= 2.5) {
          multiply_event_weight(m_sys_jet_ftag_eff.at(i)->at(jet), i);
        }
      }
      if (m_AnalysisJets_ftag_select_DL1r_FixedCutBEff_70->at(jet)) {
        m_sys_nbjets.at(i) += 1;
      }
    }
    if (m_sys_nbjets.at(i) == 0) {
      m_channel_sys.at(i) += "_0tag";
    } else if (m_sys_nbjets.at(i) >= 1) {
      m_channel_sys.at(i) += "_1tag";
    }
  }
}

void ZCharmLoopBase::fill_eventNumber_histogram() {
  // add_fill_hist_sys("eventNumber", m_EventInfo_eventNumber, 100, 0,
  // 300000000, false);
}

void ZCharmLoopBase::fill_histograms(std::string channel) {
  // Global, cross D variable used in Real Rate Measurement
  m_have_one_D = false;

  // PV histograms
  if (m_do_PV) {
    fill_PV_histograms(channel);
  }

  // fill lepton histograms
  if (m_do_lep_presel && m_fill_zjets) {
    fill_lepton_histograms(channel);
  }

  // fill D plus histograms
  if (m_D_plus_meson) {
    fill_D_plus_histograms(channel);
  }

  // fill D star histograms
  if (m_D_star_meson) {
    fill_D_star_histograms(channel);
  }

  // fill D star pi0 histograms
  if (m_D_star_pi0_meson) {
    fill_D_star_pi0_histograms(channel);
  }

  // fill D s histograms
  if (m_D_s_meson) {
    fill_D_s_histograms(channel);
  }

  // stand-alone truth
  if (m_truth_D && m_channel_sys.at(0) != "") {
    TruthInfo::fill_stand_alone_truth(this);
  }
}

std::string ZCharmLoopBase::get_period_string() {
  return get_period(m_EventInfo_runNumber);
}

void ZCharmLoopBase::debug_dmeson_printout(int index, bool printTruth) {
  if (index >= 0) {
    std::cout << "Reco D meson:" << std::endl;
    std::cout << "  pdgId: " << m_DMesons_pdgId->at(index) << std::endl;
    std::cout << "  pT: " << m_DMesons_pt->at(index) * Charm::GeV << std::endl;
    std::cout << "  eta: " << m_DMesons_eta->at(index) << std::endl;
    std::cout << "  phi: " << m_DMesons_phi->at(index) << std::endl;
    std::cout << "  m: " << m_DMesons_m->at(index) * Charm::GeV << std::endl;
    std::cout << "  Lxy: " << m_DMesons_fitOutput__Lxy->at(index) << std::endl;
    std::cout << "  Chi2: " << m_DMesons_fitOutput__Chi2->at(index)
              << std::endl;
    std::cout << "  sigma 3D: "
              << m_DMesons_fitOutput__ImpactSignificance->at(index)
              << std::endl;
    std::cout << "  truthBarcode: " << m_DMesons_truthBarcode->at(index)
              << std::endl;
    std::cout << "  num daughters: "
              << m_DMesons_daughterInfo__truthDBarcode->at(index).size()
              << std::endl;
    for (unsigned int i = 0;
         i < m_DMesons_daughterInfo__truthDBarcode->at(index).size(); i++) {
      std::cout << "  daughter track " << i << std::endl;
      std::cout << "    truthDBarcode: "
                << m_DMesons_daughterInfo__truthDBarcode->at(index).at(i)
                << std::endl;
      std::cout << "    truthBarcode: "
                << m_DMesons_daughterInfo__truthBarcode->at(index).at(i)
                << std::endl;
      std::cout << "    pT: "
                << m_DMesons_daughterInfo__pt->at(index).at(i) * Charm::GeV
                << std::endl;
      std::cout << "    eta: " << m_DMesons_daughterInfo__eta->at(index).at(i)
                << std::endl;
      std::cout << "    phi: " << m_DMesons_daughterInfo__phi->at(index).at(i)
                << std::endl;
    }
  }
  if (printTruth) {
    std::cout << "Truth Recrod: " << m_TruthParticles_Selected_pdgId->size()
              << " particles in the ntuple" << std::endl;
    for (unsigned int i = 0; i < m_TruthParticles_Selected_pdgId->size(); i++) {
      std::cout << "truth particle " << i << std::endl;
      std::cout << "  pdgId: " << m_TruthParticles_Selected_pdgId->at(i)
                << std::endl;
      std::cout << "  barcode: " << m_TruthParticles_Selected_barcode->at(i)
                << std::endl;
      std::cout << "  pT: " << m_TruthParticles_Selected_pt->at(i) * Charm::GeV
                << std::endl;
      std::cout << "  eta: " << m_TruthParticles_Selected_eta->at(i)
                << std::endl;
      std::cout << "  phi: " << m_TruthParticles_Selected_phi->at(i)
                << std::endl;
      std::cout << "  num daughters: "
                << m_TruthParticles_Selected_daughterInfoT__pdgId->at(i).size()
                << std::endl;
      for (unsigned int j = 0;
           j < m_TruthParticles_Selected_daughterInfoT__pdgId->at(i).size();
           j++) {
        std::cout << "  truth particle " << j << std::endl;
        std::cout << "    pdgId: "
                  << m_TruthParticles_Selected_daughterInfoT__pdgId->at(i).at(j)
                  << std::endl;
        std::cout << "    barcode: "
                  << m_TruthParticles_Selected_daughterInfoT__barcode->at(i).at(
                         j)
                  << std::endl;
        std::cout << "    pT: "
                  << m_TruthParticles_Selected_daughterInfoT__pt->at(i).at(j) *
                         Charm::GeV
                  << std::endl;
        std::cout << "    eta: "
                  << m_TruthParticles_Selected_daughterInfoT__eta->at(i).at(j)
                  << std::endl;
        std::cout << "    phi: "
                  << m_TruthParticles_Selected_daughterInfoT__phi->at(i).at(j)
                  << std::endl;
      }
    }
  }
}

void ZCharmLoopBase::connect_branches() {
  if (m_do_PV) {
    // vertex variables
    add_presel_br("CharmEventInfo_PV_X", &m_CharmEventInfo_PV_X);
    add_presel_br("CharmEventInfo_PV_Y", &m_CharmEventInfo_PV_Y);
    add_presel_br("CharmEventInfo_PV_Z", &m_CharmEventInfo_PV_Z);
    // add_presel_br("CharmEventInfo_truthVertex_X",
    // &m_CharmEventInfo_truthVertex_X);
    // add_presel_br("CharmEventInfo_truthVertex_Y",
    // &m_CharmEventInfo_truthVertex_Y);
    // add_presel_br("CharmEventInfo_truthVertex_Z",
    // &m_CharmEventInfo_truthVertex_Z);
  }

  // TODO: make this dirty code nicer
  // need event weight even in some cases without lepton stuff
  if (!m_do_lep_presel) {
    if (m_is_mc && !m_no_weights) {
      add_presel_br("EventInfo_generatorWeight_NOSYS",
                    &m_EventInfo_generatorWeight_NOSYS);
    } else if (m_data_period == "ForcedDecay") {
      add_presel_br("CharmEventInfo_EventWeight",
                    &m_EventInfo_generatorWeight_NOSYS);
    }
    return;
  }

  if (m_do_lep_presel) {
    // event info
    add_presel_br("EventInfo_trigPassed_HLT_e24_lhmedium_L1EM20VH",
                  &m_EventInfo_trigPassed_HLT_e24_lhmedium_L1EM20VH);
    add_presel_br("EventInfo_trigPassed_HLT_e60_lhmedium",
                  &m_EventInfo_trigPassed_HLT_e60_lhmedium);
    add_presel_br("EventInfo_trigPassed_HLT_e120_lhloose",
                  &m_EventInfo_trigPassed_HLT_e120_lhloose);
    add_presel_br("EventInfo_trigPassed_HLT_e26_lhtight_nod0_ivarloose",
                  &m_EventInfo_trigPassed_HLT_e26_lhtight_nod0_ivarloose);
    add_presel_br("EventInfo_trigPassed_HLT_e60_lhmedium_nod0",
                  &m_EventInfo_trigPassed_HLT_e60_lhmedium_nod0);
    add_presel_br("EventInfo_trigPassed_HLT_e140_lhloose_nod0",
                  &m_EventInfo_trigPassed_HLT_e140_lhloose_nod0);
    add_presel_br("EventInfo_trigPassed_HLT_mu20_iloose_L1MU15",
                  &m_EventInfo_trigPassed_HLT_mu20_iloose_L1MU15);
    add_presel_br("EventInfo_trigPassed_HLT_mu26_ivarmedium",
                  &m_EventInfo_trigPassed_HLT_mu26_ivarmedium);
    add_presel_br("EventInfo_trigPassed_HLT_mu50",
                  &m_EventInfo_trigPassed_HLT_mu50);
    add_presel_br(Charm::get_run_number_string(m_is_mc),
                  &m_EventInfo_runNumber);
    add_presel_br(Charm::get_pileup_string(m_is_mc, m_mc_period, m_data_period),
                  &m_EventInfo_correctedScaled_averageInteractionsPerCrossing);
    add_presel_br("EventInfo_eventNumber", &m_EventInfo_eventNumber);

    // muons
    add_presel_br("AnalysisMuons_pt_NOSYS", &m_AnalysisMuons_pt_NOSYS);
    add_presel_br("AnalysisMuons_eta", &m_AnalysisMuons_eta);
    add_presel_br("AnalysisMuons_phi", &m_AnalysisMuons_phi);
    add_presel_br("AnalysisMuons_charge", &m_AnalysisMuons_charge);
    add_presel_br("AnalysisMuons_mu_selected_NOSYS",
                  &m_AnalysisMuons_mu_selected_NOSYS);
    add_presel_br("AnalysisMuons_isQuality_Medium_NOSYS",
                  &m_AnalysisMuons_isQuality_Medium_NOSYS);
    add_presel_br("AnalysisMuons_isIsolated_PflowTight_VarRad_NOSYS",
                  &m_AnalysisMuons_isIsolated_PflowTight_VarRad_NOSYS);
    add_presel_br("AnalysisMuons_matched_HLT_mu20_iloose_L1MU15",
                  &m_AnalysisMuons_matched_HLT_mu20_iloose_L1MU15);
    add_presel_br("AnalysisMuons_matched_HLT_mu26_ivarmedium",
                  &m_AnalysisMuons_matched_HLT_mu26_ivarmedium);
    add_presel_br("AnalysisMuons_matched_HLT_mu50",
                  &m_AnalysisMuons_matched_HLT_mu50);
    add_presel_br("AnalysisMuons_d0", &m_AnalysisMuons_d0);
    add_presel_br("AnalysisMuons_d0sig", &m_AnalysisMuons_d0sig);
    add_presel_br("AnalysisMuons_is_bad", &m_AnalysisMuons_is_bad);
    add_presel_br("AnalysisMuons_neflowisol20", &m_AnalysisMuons_neflowisol20);
    add_presel_br("AnalysisMuons_ptvarcone30_TightTTVA_pt500",
                  &m_AnalysisMuons_ptvarcone30_TightTTVA_pt500);
    if (m_do_extra_histograms) {
      add_presel_br("AnalysisMuons_z0sinTheta", &m_AnalysisMuons_z0sinTheta);
      add_presel_br("AnalysisMuons_topoetcone20",
                    &m_AnalysisMuons_topoetcone20);
      add_presel_br("AnalysisMuons_ptvarcone30_TightTTVA_pt1000",
                    &m_AnalysisMuons_ptvarcone30_TightTTVA_pt1000);
    }

    // electrons
    add_presel_br("AnalysisElectrons_pt_NOSYS", &m_AnalysisElectrons_pt_NOSYS);
    add_presel_br("AnalysisElectrons_eta", &m_AnalysisElectrons_eta);
    add_presel_br("AnalysisElectrons_caloCluster_eta",
                  &m_AnalysisElectrons_caloCluster_eta);
    add_presel_br("AnalysisElectrons_phi", &m_AnalysisElectrons_phi);
    add_presel_br("AnalysisElectrons_charge", &m_AnalysisElectrons_charge);
    add_presel_br("AnalysisElectrons_likelihood_Tight",
                  &m_AnalysisElectrons_likelihood_Tight);
    add_presel_br("AnalysisElectrons_el_selected_NOSYS",
                  &m_AnalysisElectrons_el_selected_NOSYS);
    add_presel_br("AnalysisElectrons_isIsolated_Tight_VarRad_NOSYS",
                  &m_AnalysisElectrons_isIsolated_Tight_VarRad_NOSYS);
    add_presel_br("AnalysisElectrons_matched_HLT_e24_lhmedium_L1EM20VH",
                  &m_AnalysisElectrons_matched_HLT_e24_lhmedium_L1EM20VH);
    add_presel_br("AnalysisElectrons_matched_HLT_e60_lhmedium",
                  &m_AnalysisElectrons_matched_HLT_e60_lhmedium);
    add_presel_br("AnalysisElectrons_matched_HLT_e120_lhloose",
                  &m_AnalysisElectrons_matched_HLT_e120_lhloose);
    add_presel_br("AnalysisElectrons_matched_HLT_e26_lhtight_nod0_ivarloose",
                  &m_AnalysisElectrons_matched_HLT_e26_lhtight_nod0_ivarloose);
    add_presel_br("AnalysisElectrons_matched_HLT_e60_lhmedium_nod0",
                  &m_AnalysisElectrons_matched_HLT_e60_lhmedium_nod0);
    add_presel_br("AnalysisElectrons_matched_HLT_e140_lhloose_nod0",
                  &m_AnalysisElectrons_matched_HLT_e140_lhloose_nod0);
    add_presel_br("AnalysisElectrons_d0", &m_AnalysisElectrons_d0);
    add_presel_br("AnalysisElectrons_d0sig", &m_AnalysisElectrons_d0sig);
    add_presel_br("AnalysisElectrons_ptvarcone30_TightTTVA_pt1000",
                  &m_AnalysisElectrons_ptvarcone30_TightTTVA_pt1000);
    if (m_do_extra_histograms) {
      add_presel_br("AnalysisElectrons_z0sinTheta",
                    &m_AnalysisElectrons_z0sinTheta);
      add_presel_br("AnalysisElectrons_topoetcone20",
                    &m_AnalysisElectrons_topoetcone20);
      add_presel_br("AnalysisElectrons_ptvarcone20_TightTTVA_pt1000",
                    &m_AnalysisElectrons_ptvarcone20_TightTTVA_pt1000);
      // add_presel_br("AnalysisElectrons_EOverP", &m_AnalysisElectrons_EOverP);
    }

    // mc only
    if (m_is_mc) {
      add_presel_br("EventInfo_generatorWeight_NOSYS",
                    &m_EventInfo_generatorWeight_NOSYS);
      add_presel_br("EventInfo_PileupWeight_NOSYS",
                    &m_EventInfo_PileupWeight_NOSYS);
      add_presel_br("EventInfo_prodFracWeight_NOSYS",
                    &m_EventInfo_prodFracWeight_NOSYS);
      add_presel_br("EventInfo_jvt_effSF_NOSYS", &m_EventInfo_jvt_effSF_NOSYS);
      add_presel_br("AnalysisElectrons_effSF_NOSYS",
                    &m_AnalysisElectrons_effSF_NOSYS);
      add_presel_br("AnalysisElectrons_effSF_ID_Tight_NOSYS",
                    &m_AnalysisElectrons_effSF_ID_Tight_NOSYS);
      add_presel_br("AnalysisElectrons_effSF_Isol_Tight_Tight_VarRad_NOSYS",
                    &m_AnalysisElectrons_effSF_Isol_Tight_Tight_VarRad_NOSYS);
      add_presel_br(
          "AnalysisElectrons_effSF_Trig_Tight_Tight_VarRad_SINGLE_E_2015_e24_"
          "lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_"
          "lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_"
          "NOSYS",
          &m_AnalysisElectrons_effSF_Trig_Tight_Tight_VarRad_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_NOSYS);
      add_presel_br(
          "AnalysisElectrons_effSF_Trig_Tight_Tight_VarRad_Eff_SINGLE_E_2015_"
          "e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_"
          "lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_"
          "NOSYS",
          &m_AnalysisElectrons_effSF_Trig_Tight_Tight_VarRad_Eff_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_NOSYS);
      add_presel_br(
          "AnalysisElectrons_effSF_Trig_Tight_noIso_SINGLE_E_2015_e24_lhmedium_"
          "L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_"
          "ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_NOSYS",
          &m_AnalysisElectrons_effSF_Trig_Tight_noIso_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_NOSYS);
      add_presel_br(
          "AnalysisElectrons_effSF_Trig_Tight_noIso_Eff_SINGLE_E_2015_e24_"
          "lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_"
          "lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_"
          "NOSYS",
          &m_AnalysisElectrons_effSF_Trig_Tight_noIso_Eff_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_NOSYS);
      add_presel_br("AnalysisElectrons_effSF_Chflip_Tight_Tight_VarRad_NOSYS",
                    &m_AnalysisElectrons_effSF_Chflip_Tight_Tight_VarRad_NOSYS);
      add_presel_br("AnalysisElectrons_truthType",
                    &m_AnalysisElectrons_truthType);
      add_presel_br("AnalysisElectrons_truthOrigin",
                    &m_AnalysisElectrons_truthOrigin);
      add_presel_br("AnalysisElectrons_firstEgMotherTruthType",
                    &m_AnalysisElectrons_firstEgMotherTruthType);
      add_presel_br("AnalysisElectrons_firstEgMotherTruthOrigin",
                    &m_AnalysisElectrons_firstEgMotherTruthOrigin);
      add_presel_br("AnalysisElectrons_firstEgMotherPdgId",
                    &m_AnalysisElectrons_firstEgMotherPdgId);
      add_presel_br("AnalysisMuons_muon_effSF_Quality_Medium_NOSYS",
                    &m_AnalysisMuons_mu_effSF_Quality_Medium_NOSYS);
      add_presel_br("AnalysisMuons_muon_effSF_TTVA_NOSYS",
                    &m_AnalysisMuons_mu_effSF_TTVA_NOSYS);
      add_presel_br("AnalysisMuons_muon_effSF_Isol_PflowTight_VarRad_NOSYS",
                    &m_AnalysisMuons_mu_effSF_Isol_PflowTight_VarRad_NOSYS);
      add_presel_br(
          "AnalysisMuons_muon_effMC_Trig_Medium_HLT_mu26_ivarmedium_OR_HLT_"
          "mu50_"
          "NOSYS",
          &m_AnalysisMuons_mu_effMC_Trig_Medium_HLT_mu26_ivarmedium_OR_HLT_mu50_NOSYS);
      add_presel_br(
          "AnalysisMuons_muon_effData_Trig_Medium_HLT_mu26_ivarmedium_OR_HLT_"
          "mu50_NOSYS",
          &m_AnalysisMuons_mu_effData_Trig_Medium_HLT_mu26_ivarmedium_OR_HLT_mu50_NOSYS);
      add_presel_br(
          "AnalysisMuons_muon_effMC_Trig_Medium_HLT_mu20_iloose_L1MU15_OR_HLT_"
          "mu50_NOSYS",
          &m_AnalysisMuons_mu_effMC_Trig_Medium_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_NOSYS);
      add_presel_br(
          "AnalysisMuons_muon_effData_Trig_Medium_HLT_mu20_iloose_L1MU15_OR_"
          "HLT_"
          "mu50_NOSYS",
          &m_AnalysisMuons_mu_effData_Trig_Medium_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_NOSYS);
      add_presel_br("AnalysisMuons_truthType", &m_AnalysisMuons_truthType);
      add_presel_br("AnalysisMuons_truthOrigin", &m_AnalysisMuons_truthOrigin);

      // sys branches
      if (m_do_systematics) {
        // event weights
        init_sys_br(&m_sys_br_GEN, "EventInfo_generatorWeight_%s");
        init_sys_br(&m_sys_br_PU, "EventInfo_PileupWeight_%s");
        init_sys_br(&m_sys_br_JVT, "EventInfo_jvt_effSF_%s");
        init_sys_br(&m_sys_br_PROD_FRAC, "EventInfo_prodFracWeight_%s");

        // electron sys branches
        init_sys_br(&m_sys_br_el_pt, "AnalysisElectrons_pt_%s");
        init_sys_br(&m_sys_br_el_selected, "AnalysisElectrons_el_selected_%s");
        init_sys_br(&m_sys_br_el_isIsolated_Tight_VarRad,
                    "AnalysisElectrons_isIsolated_Tight_VarRad_%s");
        init_sys_br(&m_sys_br_el_reco_eff, "AnalysisElectrons_effSF_%s");
        init_sys_br(&m_sys_br_el_id_eff, "AnalysisElectrons_effSF_ID_Tight_%s");
        init_sys_br(&m_sys_br_el_iso_eff,
                    "AnalysisElectrons_effSF_Isol_Tight_Tight_VarRad_%s");
        init_sys_br(&m_sys_br_el_charge_eff,
                    "AnalysisElectrons_effSF_Chflip_Tight_Tight_VarRad_%s");
        init_sys_br(&m_sys_br_el_trig_eff,
                    "AnalysisElectrons_effSF_Trig_Tight_Tight_VarRad_Eff_"
                    "SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_"
                    "e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_"
                    "lhmedium_nod0_OR_e140_lhloose_nod0_%s",
                    {"EL_EFF_Trigger_TOTAL_1NPCOR_PLUS_UNCOR__1up",
                     "EL_EFF_Trigger_TOTAL_1NPCOR_PLUS_UNCOR__1down"});
        init_sys_br(&m_sys_br_el_trig_sf,
                    "AnalysisElectrons_effSF_Trig_Tight_Tight_VarRad_SINGLE_E_"
                    "2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_"
                    "lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_"
                    "lhmedium_nod0_OR_e140_lhloose_nod0_%s",
                    {"EL_EFF_TriggerEff_TOTAL_1NPCOR_PLUS_UNCOR__1up",
                     "EL_EFF_TriggerEff_TOTAL_1NPCOR_PLUS_UNCOR__1down"});
        init_sys_br(&m_sys_br_el_trig_noiso_eff,
                    "AnalysisElectrons_effSF_Trig_Tight_noIso_Eff_SINGLE_E_"
                    "2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_"
                    "lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_"
                    "lhmedium_nod0_OR_e140_lhloose_nod0_%s",
                    {"EL_EFF_Trigger_TOTAL_1NPCOR_PLUS_UNCOR__1up",
                     "EL_EFF_Trigger_TOTAL_1NPCOR_PLUS_UNCOR__1down"});
        init_sys_br(&m_sys_br_el_trig_noiso_sf,
                    "AnalysisElectrons_effSF_Trig_Tight_noIso_SINGLE_E_2015_"
                    "e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_"
                    "2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_"
                    "OR_e140_lhloose_nod0_%s",
                    {"EL_EFF_TriggerEff_TOTAL_1NPCOR_PLUS_UNCOR__1up",
                     "EL_EFF_TriggerEff_TOTAL_1NPCOR_PLUS_UNCOR__1down"});
        init_sys_br(&m_sys_br_el_calib_reco_eff, "AnalysisElectrons_effSF_%s");
        init_sys_br(&m_sys_br_el_calib_id_eff,
                    "AnalysisElectrons_effSF_ID_Tight_%s");
        init_sys_br(&m_sys_br_el_calib_iso_eff,
                    "AnalysisElectrons_effSF_Isol_Tight_Tight_VarRad_%s");
        init_sys_br(&m_sys_br_el_calib_charge_eff,
                    "AnalysisElectrons_effSF_Chflip_Tight_Tight_VarRad_%s");
        init_sys_br(&m_sys_br_el_calib_trig_eff,
                    "AnalysisElectrons_effSF_Trig_Tight_Tight_VarRad_Eff_"
                    "SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_"
                    "e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_"
                    "lhmedium_nod0_OR_e140_lhloose_nod0_%s");
        init_sys_br(&m_sys_br_el_calib_trig_sf,
                    "AnalysisElectrons_effSF_Trig_Tight_Tight_VarRad_SINGLE_E_"
                    "2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_"
                    "lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_"
                    "lhmedium_nod0_OR_e140_lhloose_nod0_%s");
        init_sys_br(&m_sys_br_el_calib_trig_noiso_eff,
                    "AnalysisElectrons_effSF_Trig_Tight_noIso_Eff_SINGLE_E_"
                    "2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_"
                    "lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_"
                    "lhmedium_nod0_OR_e140_lhloose_nod0_%s");
        init_sys_br(&m_sys_br_el_calib_trig_noiso_sf,
                    "AnalysisElectrons_effSF_Trig_Tight_noIso_SINGLE_E_2015_"
                    "e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_"
                    "2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_"
                    "OR_e140_lhloose_nod0_%s");

        // muon sys branches
        init_sys_br(&m_sys_br_mu_pt, "AnalysisMuons_pt_%s");
        init_sys_br(&m_sys_br_mu_selected, "AnalysisMuons_mu_selected_%s");
        init_sys_br(&m_sys_br_mu_isQuality_Medium,
                    "AnalysisMuons_isQuality_Medium_%s");
        init_sys_br(&m_sys_br_mu_isIsolated_PflowTight_VarRad,
                    "AnalysisMuons_isIsolated_PflowTight_VarRad_%s");
        init_sys_br(&m_sys_br_mu_quality_eff,
                    "AnalysisMuons_muon_effSF_Quality_Medium_%s");
        init_sys_br(&m_sys_br_mu_ttva_eff, "AnalysisMuons_muon_effSF_TTVA_%s");
        init_sys_br(&m_sys_br_mu_iso_eff,
                    "AnalysisMuons_muon_effSF_Isol_PflowTight_VarRad_%s");
        init_sys_br(
            &m_sys_br_mu_trig_2015_eff_mc,
            "AnalysisMuons_muon_effMC_Trig_Medium_HLT_mu20_iloose_L1MU15_"
            "OR_HLT_mu50_%s");
        init_sys_br(&m_sys_br_mu_trig_2015_eff_data,
                    "AnalysisMuons_muon_effData_Trig_Medium_HLT_mu20_iloose_"
                    "L1MU15_OR_HLT_mu50_%s");
        init_sys_br(
            &m_sys_br_mu_trig_2018_eff_mc,
            "AnalysisMuons_muon_effMC_Trig_Medium_HLT_mu26_ivarmedium_OR_"
            "HLT_mu50_%s");
        init_sys_br(
            &m_sys_br_mu_trig_2018_eff_data,
            "AnalysisMuons_muon_effData_Trig_Medium_HLT_mu26_ivarmedium_"
            "OR_HLT_mu50_%s");
        init_sys_br(&m_sys_br_mu_calib_quality_eff,
                    "AnalysisMuons_muon_effSF_Quality_Medium_%s");
        init_sys_br(&m_sys_br_mu_calib_ttva_eff,
                    "AnalysisMuons_muon_effSF_TTVA_%s");
        init_sys_br(&m_sys_br_mu_calib_iso_eff,
                    "AnalysisMuons_muon_effSF_Isol_PflowTight_VarRad_%s");
        init_sys_br(
            &m_sys_br_mu_calib_trig_2015_eff_mc,
            "AnalysisMuons_muon_effMC_Trig_Medium_HLT_mu20_iloose_L1MU15_"
            "OR_HLT_mu50_%s");
        init_sys_br(&m_sys_br_mu_calib_trig_2015_eff_data,
                    "AnalysisMuons_muon_effData_Trig_Medium_HLT_mu20_iloose_"
                    "L1MU15_OR_HLT_mu50_%s");
        init_sys_br(
            &m_sys_br_mu_calib_trig_2018_eff_mc,
            "AnalysisMuons_muon_effMC_Trig_Medium_HLT_mu26_ivarmedium_OR_"
            "HLT_mu50_%s");
        init_sys_br(
            &m_sys_br_mu_calib_trig_2018_eff_data,
            "AnalysisMuons_muon_effData_Trig_Medium_HLT_mu26_ivarmedium_"
            "OR_HLT_mu50_%s");

        // jet sys branches
        init_sys_br(&m_sys_br_jet_pt, "AnalysisJets_pt_%s");
        init_sys_br(&m_sys_br_jet_selected, "AnalysisJets_jet_selected_%s");
        init_sys_br(&m_sys_br_jet_ftag_eff,
                    "AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_%s");
        init_sys_br(&m_sys_br_jet_calib_ftag_eff,
                    "AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_%s");
      }
    }
  }

  // Get truth D meson branches
  if (m_is_mc && (m_do_prod_fraction_rw_old) &&
      !(m_truth_matchD || m_truth_D)) {
    connect_truth_D_meson_branches(this, m_fiducial_selection);
  }

  // jets
  if (m_jet_selection) {
    add_presel_br("AnalysisJets_pt_NOSYS", &m_AnalysisJets_pt_NOSYS);
    add_presel_br("AnalysisJets_eta", &m_AnalysisJets_eta);
    add_presel_br("AnalysisJets_phi", &m_AnalysisJets_phi);
    add_presel_br("AnalysisJets_m", &m_AnalysisJets_m);
    add_presel_br("AnalysisJets_jet_selected_NOSYS",
                  &m_AnalysisJets_jet_selected_NOSYS);
    add_presel_br("AnalysisJets_ftag_select_DL1r_FixedCutBEff_70",
                  &m_AnalysisJets_ftag_select_DL1r_FixedCutBEff_70);
    if (m_is_mc) {
      add_presel_br("AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_NOSYS",
                    &m_AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_NOSYS);
    }
  }
}

void ZCharmLoopBase::read_sys_br() {
  // read electrons
  m_sys_el_iso = get_val_sys(m_AnalysisElectrons_isIsolated_Tight_VarRad_NOSYS,
                             &m_sys_br_el_isIsolated_Tight_VarRad);
  m_sys_el_pt = get_val_sys(m_AnalysisElectrons_pt_NOSYS, &m_sys_br_el_pt);
  m_sys_el_reco_eff =
      get_val_sys(m_AnalysisElectrons_effSF_NOSYS, &m_sys_br_el_reco_eff,
                  &m_sys_br_el_calib_reco_eff);
  m_sys_el_id_eff = get_val_sys(m_AnalysisElectrons_effSF_ID_Tight_NOSYS,
                                &m_sys_br_el_id_eff, &m_sys_br_el_calib_id_eff);
  m_sys_el_iso_eff =
      get_val_sys(m_AnalysisElectrons_effSF_Isol_Tight_Tight_VarRad_NOSYS,
                  &m_sys_br_el_iso_eff, &m_sys_br_el_calib_iso_eff);
  m_sys_el_trig_sf = get_val_sys(
      m_AnalysisElectrons_effSF_Trig_Tight_Tight_VarRad_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_NOSYS,
      &m_sys_br_el_trig_sf, &m_sys_br_el_calib_trig_sf);
  m_sys_el_trig_eff = get_val_sys(
      m_AnalysisElectrons_effSF_Trig_Tight_Tight_VarRad_Eff_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_NOSYS,
      &m_sys_br_el_trig_eff, &m_sys_br_el_calib_trig_eff);
  m_sys_el_trig_noiso_eff = get_val_sys(
      m_AnalysisElectrons_effSF_Trig_Tight_noIso_Eff_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_NOSYS,
      &m_sys_br_el_trig_noiso_eff, &m_sys_br_el_calib_trig_noiso_eff);
  m_sys_el_trig_noiso_sf = get_val_sys(
      m_AnalysisElectrons_effSF_Trig_Tight_noIso_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_NOSYS,
      &m_sys_br_el_trig_noiso_sf, &m_sys_br_el_calib_trig_noiso_sf);
  m_sys_el_charge_sf =
      get_val_sys(m_AnalysisElectrons_effSF_Chflip_Tight_Tight_VarRad_NOSYS,
                  &m_sys_br_el_charge_eff, &m_sys_br_el_calib_charge_eff);

  // read muons
  m_sys_mu_pt = get_val_sys(m_AnalysisMuons_pt_NOSYS, &m_sys_br_mu_pt);
  m_sys_mu_quality = get_val_sys(m_AnalysisMuons_isQuality_Medium_NOSYS,
                                 &m_sys_br_mu_isQuality_Medium);
  m_sys_mu_iso = get_val_sys(m_AnalysisMuons_isIsolated_PflowTight_VarRad_NOSYS,
                             &m_sys_br_mu_isIsolated_PflowTight_VarRad);
  m_sys_mu_quality_eff =
      get_val_sys(m_AnalysisMuons_mu_effSF_Quality_Medium_NOSYS,
                  &m_sys_br_mu_quality_eff, &m_sys_br_mu_calib_quality_eff);
  m_sys_mu_ttva_eff =
      get_val_sys(m_AnalysisMuons_mu_effSF_TTVA_NOSYS, &m_sys_br_mu_ttva_eff,
                  &m_sys_br_mu_calib_ttva_eff);
  m_sys_mu_iso_eff =
      get_val_sys(m_AnalysisMuons_mu_effSF_Isol_PflowTight_VarRad_NOSYS,
                  &m_sys_br_mu_iso_eff, &m_sys_br_mu_calib_iso_eff);
  m_sys_mu_trig_2015_eff_mc = get_val_sys(
      m_AnalysisMuons_mu_effMC_Trig_Medium_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_NOSYS,
      &m_sys_br_mu_trig_2015_eff_mc, &m_sys_br_mu_calib_trig_2015_eff_mc);
  m_sys_mu_trig_2015_eff_data = get_val_sys(
      m_AnalysisMuons_mu_effData_Trig_Medium_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_NOSYS,
      &m_sys_br_mu_trig_2015_eff_data, &m_sys_br_mu_calib_trig_2015_eff_data);
  m_sys_mu_trig_2018_eff_mc = get_val_sys(
      m_AnalysisMuons_mu_effMC_Trig_Medium_HLT_mu26_ivarmedium_OR_HLT_mu50_NOSYS,
      &m_sys_br_mu_trig_2018_eff_mc, &m_sys_br_mu_calib_trig_2018_eff_mc);
  m_sys_mu_trig_2018_eff_data = get_val_sys(
      m_AnalysisMuons_mu_effData_Trig_Medium_HLT_mu26_ivarmedium_OR_HLT_mu50_NOSYS,
      &m_sys_br_mu_trig_2018_eff_data, &m_sys_br_mu_calib_trig_2018_eff_data);

  // read jets
  m_sys_jet_pt = get_val_sys(m_AnalysisJets_pt_NOSYS, &m_sys_br_jet_pt);
  m_sys_jet_ftag_eff =
      get_val_sys(m_AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_NOSYS,
                  &m_sys_br_jet_ftag_eff, &m_sys_br_jet_calib_ftag_eff);

  // branches for selecting objects
  m_sys_el_selected =
      get_val_sys(m_AnalysisElectrons_el_selected_NOSYS, &m_sys_br_el_selected);
  m_sys_mu_selected =
      get_val_sys(m_AnalysisMuons_mu_selected_NOSYS, &m_sys_br_mu_selected);
  m_sys_jet_selected =
      get_val_sys(m_AnalysisJets_jet_selected_NOSYS, &m_sys_br_jet_selected);
}

void ZCharmLoopBase::configure() {
  // set configs
  std::cout << get_config() << std::endl;

  // systematics
  m_do_systematics = get_config()["do_systematics"].as<bool>(false);

  // save only variables needed for the fit
  m_fit_variables_only = get_config()["fit_variables_only"].as<bool>(false);

  // save unfolding varaibles (transfer matrix, etc..)
  m_unfolding_variables = get_config()["unfolding_variables"].as<bool>(false);

  // lepton preseletion
  m_do_lep_presel = get_config()["do_lep_presel"].as<bool>(true);
  m_fill_zjets = get_config()["fill_zjets"].as<bool>(true);

  // truth match leptons
  m_truth_match = get_config()["truth_match"].as<bool>(true);

  // add primary vertex variables
  m_do_PV = get_config()["do_PV"].as<bool>(false);

  // channels
  m_split_by_period = get_config()["split_by_period"].as<bool>(false);

  // D-meson reconstruction
  m_D_plus_meson = get_config()["D_plus_meson"].as<bool>(true);
  m_D_star_meson = get_config()["D_star_meson"].as<bool>(false);
  m_D_star_pi0_meson = get_config()["D_star_pi0_meson"].as<bool>(false);
  m_D_s_meson = get_config()["D_s_meson"].as<bool>(false);

  // Enable Tight Primary track selection (Loose is default, ie false)
  m_D_tightPrimary_tracks =
      get_config()["D_tightPrimary_tracks"].as<bool>(false);

  // D-meson truth
  m_truth_matchD = get_config()["truth_matchD"].as<bool>(true);
  m_truth_D = get_config()["truth_D"].as<bool>(false);

  // Single entry D-meson regions
  m_do_single_entry_D = get_config()["do_single_entry_D"].as<bool>(false);
  m_do_single_entry_D_only =
      get_config()["do_single_entry_D_only"].as<bool>(false);

  // D-meson N-1
  m_do_Nminus1 = get_config()["do_Nminus1"].as<bool>(false);

  // Keep all matched D's from N-1 even if the fail 2 or more cuts
  m_do_twice_failed = get_config()["do_twice_failed"].as<bool>(false);

  // apply truth fiducial cuts
  m_fiducial_selection = get_config()["fiducial_selection"].as<bool>(false);

  // apply truth fiducial cuts for zplusd (fiducial selection must also be set
  // to true)
  // Also sets proper binning for Z+D D pT diff measurement
  m_zplusd_mode = get_config()["zplusd_mode"].as<bool>(true);

  // save truth zjets quantities
  m_save_truth_zjets = get_config()["save_truth_zjets"].as<bool>(false);

  // do z truth fiducial study
  m_do_ztruth_fid_study = get_config()["ztruth_fid_study"].as<bool>(false);

  // do mj estimation (i.e. Turn on !T and SS selection)
  m_do_mj_estimation = get_config()["mj_estimation"].as<bool>(false);

  // charm production fraction reweight from the ntuple
  m_do_prod_fraction_rw = get_config()["do_prod_fraction_rw"].as<bool>(true);

  // charm production fraction reweight implemented in charmpp
  m_do_prod_fraction_rw_old =
      get_config()["do_prod_fraction_rw_old"].as<bool>(false);

  // extra lepton histograms
  m_do_extra_histograms = get_config()["do_extra_histograms"].as<bool>(false);

  // jet multiplicity requirement
  m_jet_selection = get_config()["jet_selection"].as<bool>(false);

  // Bin D mesons in pT
  m_bin_in_D_pt = get_config()["bin_in_D_pt"].as<bool>(false);

  // Bin D mesons in truth pT
  m_bin_in_truth_D_pt = get_config()["bin_in_truth_D_pt"].as<bool>(false);

  // Signal only (only save the Matched categories)
  m_signal_only = get_config()["signal_only"].as<bool>(false);

  // apply luminosity
  m_apply_lumi = get_config()["apply_lumi"].as<bool>(true);

  // reweight spg samples
  m_reweight_spg = get_config()["reweight_spg"].as<bool>(true);

  // En/Disable Dplus Isolation Cut
  m_dplus_isolation = get_config()["dplus_isolation"].as<bool>(true);

  // reweight sherpa 2.2.11 samples (remove large Diboson weights)
  m_reweight_sherpa = get_config()["reweight_sherpa"].as<bool>(false);

  // debug print-out for D mesons
  m_debug_dmeson_printout =
      get_config()["debug_dmeson_printout"].as<bool>(false);
}

ZCharmLoopBase::ZCharmLoopBase(TString input_file, TString out_path,
                               TString tree_name)
    : EventLoopBase(input_file, out_path, tree_name),
      DMesonRec(),
      TruthInfo(),
      m_eta_bins_el{0, 0.70, 1.37, 1.52, 2.01, 2.47},
      m_eta_bins_mu{0, 1.10, 2.01, 2.5} {
  add_channel("inclusive", {"all", "one_loose_lepton", "one_tight_lepton",
                            "trigger", "trigger_match"});
}

}  // namespace Charm
