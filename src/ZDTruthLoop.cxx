#include "ZDTruthLoop.h"

namespace Charm {
void ZDTruthLoop::initialize() {
  std::cout << "ZDTruthLoop::initialize" << std::endl;

  // configure
  configure();

  // systematics
  if (m_do_systematics) {
    // GEN
    m_sys_br_GEN.init(get_sys("systematics_GEN"));

    // Production fractions
    m_sys_br_PROD_FRAC.init(get_sys("systematics_PROD_FRAC"));
  }
}

float ZDTruthLoop::dR_min(std::vector<unsigned int> *selected) {
  float dR_min = 999;
  for (unsigned int i = 0; i < selected->size(); i++) {
    for (unsigned int j = i + 1; j < selected->size(); j++) {
      unsigned int index1 = selected->at(i);
      unsigned int index2 = selected->at(j);

      float deta = m_TruthParticles_Selected_eta->at(index1) -
                   m_TruthParticles_Selected_eta->at(index2);
      float dphi =
          TVector2::Phi_mpi_pi(m_TruthParticles_Selected_phi->at(index1) -
                               m_TruthParticles_Selected_phi->at(index2));
      float dR = TMath::Sqrt(deta * deta + dphi * dphi);
      if (dR < dR_min) {
        dR_min = dR;
      }
    }
  }
  return dR_min;
}

void ZDTruthLoop::fill_Dplus_histograms(std::string channel,
                                        bool requireDecay) {
  // number of D, dR
  std::vector<unsigned int> selected = {};

  // truth D selection
  for (unsigned int i = 0; i < m_TruthParticles_Selected_decayMode->size();
       i++) {
    // require PDG ID
    if (fabs(m_TruthParticles_Selected_pdgId->at(i)) != 411) {
      continue;
    }
    // truth fiducial pT cut
    if ((m_TruthParticles_Selected_pt->at(i) * Charm::GeV) < 8.0) {
      continue;
    }
    // truth fiducial eta cut
    if (fabs(m_TruthParticles_Selected_eta->at(i)) > 2.2) {
      continue;
    }
    // require the D+ -> K pi pi decay if required
    std::string mode = "";
    if (requireDecay) {
      mode = get_decay_mode(i);
      if (mode == "") {
        continue;
      }
    }
    // number of D, dR
    selected.push_back(i);

    // channel name
    std::string name = "";
    if (channel != "") {
      name += "_" + channel;
    }
    if (requireDecay) {
      name += "_" + mode;
    }
    fill_D_histograms("Dplus" + name, i);
  }
  // overall plots
  std::string channel_name = !channel.empty() ? "_" + channel : "";
  auto channels = std::vector<std::string>(m_nSyst, channel_name);
  if (!requireDecay) {
    add_fill_hist_sys("N_Dplus", channels, selected.size(), 10, -0.5, 9.5,
                      false);
    add_fill_hist_sys("dR_min_Dplus", channels, dR_min(&selected), 240, 0.0,
                      6.0, false);
  }
}

void ZDTruthLoop::fill_Dstar_histograms(std::string channel,
                                        bool requireDecay) {
  // truth D selection
  for (unsigned int i = 0; i < m_TruthParticles_Selected_decayMode->size();
       i++) {
    // require PDG ID
    if (fabs(m_TruthParticles_Selected_pdgId->at(i)) != 413) {
      continue;
    }
    // truth fiducial pT cut
    if ((m_TruthParticles_Selected_pt->at(i) * Charm::GeV) < 8.0) {
      continue;
    }
    // truth fiducial eta cut
    if (fabs(m_TruthParticles_Selected_eta->at(i)) > 2.2) {
      continue;
    }
    // require the D+ -> K pi pi decay if required
    std::string mode = "";
    if (requireDecay) {
      mode = get_decay_mode(i);
      if (mode == "") {
        continue;
      }
    }
    // channel name
    std::string name = "";
    if (channel != "") {
      name += "_" + channel;
    }
    if (requireDecay) {
      name += "_" + mode;
    }
    fill_D_histograms("Dstar" + name, i);
  }
}

void ZDTruthLoop::fill_Dzero_histograms(std::string channel,
                                        bool requireDecay) {
  // number of D, dR
  std::vector<unsigned int> selected = {};

  // truth D selection
  for (unsigned int i = 0; i < m_TruthParticles_Selected_decayMode->size();
       i++) {
    // require PDG ID
    if (fabs(m_TruthParticles_Selected_pdgId->at(i)) != 421) {
      continue;
    }
    // truth fiducial pT cut
    if ((m_TruthParticles_Selected_pt->at(i) * Charm::GeV) < 8.0) {
      continue;
    }
    // truth fiducial eta cut
    if (fabs(m_TruthParticles_Selected_eta->at(i)) > 2.2) {
      continue;
    }
    // require the D0 -> K pi pi pi decay if required
    std::string mode = "";
    if (requireDecay) {
      mode = get_decay_mode(i);
      if (mode == "") {
        continue;
      }
    }
    // number of D, dR
    selected.push_back(i);

    // channel name
    std::string name = "";
    if (channel != "") {
      name += "_" + channel;
    }
    if (requireDecay) {
      name += "_" + mode;
    }
    fill_D_histograms("Dzero" + name, i);
  }
  // overall plots
  std::string channel_name = !channel.empty() ? "_" + channel : "";
  auto channels = std::vector<std::string>(m_nSyst, channel_name);
  if (!requireDecay) {
    add_fill_hist_sys("N_Dzero", channels, selected.size(), 10, -0.5, 9.5,
                      false);
    add_fill_hist_sys("dR_min_Dzero", channels, dR_min(&selected), 240, 0.0,
                      6.0, false);
  }
}

bool ZDTruthLoop::preselection() {
  // Weighted by the MC16a/d/e lumi fraction
  multiply_mc_event_weight(m_EventInfo_generatorWeight_NOSYS, &m_sys_br_GEN,
                           true, true, (m_mc_shower == "sherpa"));

  // production fraction weight
  if (m_do_prod_fraction_rw) {
    multiply_nominal_and_sys_weight(m_EventInfo_prodFracWeight_NOSYS,
                                    &m_sys_br_PROD_FRAC);
  }

  // Require leptons originate from a Z boson
  if (!get_truth_zjets()) {
    return false;
  }

  // channel name
  auto channels = std::vector<std::string>(m_nSyst, m_truth_channel);
  m_channel_sys = channels;

  return true;
}

int ZDTruthLoop::execute() {
  // charged leptons pT > 27 GeV
  if (m_truth_lep1.Pt() < 27. || m_truth_lep2.Pt() < 27.) {
    return 1;
  }

  // charged leptons |eta| < 2.5
  if (fabs(m_truth_lep1.Eta()) > 2.5 || fabs(m_truth_lep2.Eta()) > 2.5) {
    return 1;
  }

  // Z boson truth fiducial mass cut
  if (m_truth_Z.M() < 76 || m_truth_Z.M() > 106) {
    return 1;
  }

  // reweight
  if (m_do_prod_fraction_rw_old) {
    reweight_production_fractions(this);
  }

  // inclusive histograms
  fill_histograms();

  // truth D+ selection
  fill_Dplus_histograms();
  fill_Dplus_histograms("", true);

  // truth D* selection
  fill_Dstar_histograms();
  fill_Dstar_histograms("", true);

  // truth D0 selection
  fill_Dzero_histograms();
  fill_Dzero_histograms("", true);

  return 1;
}

void ZDTruthLoop::fill_D_histograms(std::string channel, unsigned int i) {
  std::vector<float> diphotons = get_diphoton_masses(i);
  std::string channel_name = !channel.empty() ? "_" + channel : "";
  auto channels = std::vector<std::string>(m_nSyst, channel_name);
  add_fill_hist_sys("Dmeson_pt", channels,
                    m_TruthParticles_Selected_pt->at(i) * Charm::GeV, 150, 0,
                    150, m_do_systematics);
  add_fill_hist_sys("Dmeson_eta", channels,
                    m_TruthParticles_Selected_eta->at(i), 100, -3.0, 3.0,
                    m_do_systematics);
  // add_fill_hist_sys("D_pt_eta"         , channels,
  // m_TruthParticles_Selected_pt->at(i) * Charm::GeV,
  // m_TruthParticles_Selected_eta->at(i), 200, 0, 200, 440, -2.2, 2.2, false);
  add_fill_hist_sys("Dmeson_phi", channels,
                    m_TruthParticles_Selected_phi->at(i), 100, -M_PI, M_PI,
                    false);
  add_fill_hist_sys("Dmeson_m", channels,
                    m_TruthParticles_Selected_m->at(i) * Charm::GeV, 300, 0.0,
                    3.0, false);
  add_fill_hist_sys("Dmeson_m_tracks", channels,
                    truth_daughter_mass(i) * Charm::GeV, 300, 0.0, 3.0, false);
  add_fill_hist_sys("Dmeson_m_pi0", channels,
                    diphoton_resonance_mass(diphotons, PION0_MASS), 200, 0,
                    1000, false);
  add_fill_hist_sys("Dmeson_differential_pt", channels,
                    m_TruthParticles_Selected_pt->at(i) * Charm::GeV,
                    m_zd_differential_d_pt_bins.get_n_differential(),
                    m_zd_differential_d_pt_bins.get_differential_bins(), false);
  add_fill_hist_sys("ZD_deltaPhi", channels,
                    (m_truth_Z.Phi() - m_TruthParticles_Selected_phi->at(i)),
                    100, -2 * M_PI, 2 * M_PI, m_do_systematics);
  fill_histograms(channel);
}

void ZDTruthLoop::fill_histograms(std::string channel) {
  std::string channel_name = !channel.empty() ? "_" + channel : "";
  auto channels = std::vector<std::string>(m_nSyst, channel_name);
  add_fill_hist_sys("lep1_pt", channels, m_truth_lep1.Pt(), 400, 0, 400, false);
  add_fill_hist_sys("lep1_eta", channels, m_truth_lep1.Eta(), 100, -3.0, 3.0,
                    false);
  add_fill_hist_sys("lep1_phi", channels, m_truth_lep1.Phi(), 100, -M_PI, M_PI,
                    false);
  add_fill_hist_sys("lep1_charge", channels, m_truth_lep1_charge, 2, -2, 2,
                    false);
  add_fill_hist_sys("lep2_pt", channels, m_truth_lep2.Pt(), 400, 0, 400, false);
  add_fill_hist_sys("lep2_eta", channels, m_truth_lep2.Eta(), 100, -3.0, 3.0,
                    false);
  add_fill_hist_sys("lep2_phi", channels, m_truth_lep2.Phi(), 100, -M_PI, M_PI,
                    false);
  add_fill_hist_sys("lep2_charge", channels, m_truth_lep2_charge, 2, -2, 2,
                    false);
  add_fill_hist_sys("Z_pt", channels, m_truth_Z.Pt(), 400, 0, 400,
                    m_do_systematics);
  add_fill_hist_sys("Z_eta", channels, m_truth_Z.Eta(), 100, -3.0, 3.0, false);
  add_fill_hist_sys("Z_phi", channels, m_truth_Z.Phi(), 100, -M_PI, M_PI,
                    false);
  add_fill_hist_sys("Z_m", channels, m_truth_Z.M(), 120, 76, 106, false);
  add_fill_hist_sys("Z_y", channels, m_truth_Z.Rapidity(), 120, -3.0, 3.0,
                    m_do_systematics);
  add_fill_hist_sys("Z_differential_pt", channels, std::abs(m_truth_Z.Pt()),
                    m_zd_differential_z_pt_bins.get_n_differential(),
                    m_zd_differential_z_pt_bins.get_differential_bins(), false);
}

void ZDTruthLoop::connect_branches() {
  connect_truth_D_meson_branches(this);

  // event info
  add_presel_br("EventInfo_generatorWeight_NOSYS",
                &m_EventInfo_generatorWeight_NOSYS);
  add_presel_br("EventInfo_PileupWeight_NOSYS",
                &m_EventInfo_PileupWeight_NOSYS);
  add_presel_br("EventInfo_prodFracWeight_NOSYS",
                &m_EventInfo_prodFracWeight_NOSYS);
  // add_presel_br("EventInfo_eventNumber", &m_EventInfo_eventNumber);

  // sys branches
  if (m_do_systematics) {
    // event weights
    init_sys_br(&m_sys_br_GEN, "EventInfo_generatorWeight_%s");
    init_sys_br(&m_sys_br_PROD_FRAC, "EventInfo_prodFracWeight_%s");
  }
}

void ZDTruthLoop::configure() {
  // set configs
  std::cout << get_config() << std::endl;

  // systematics
  m_do_systematics = get_config()["do_systematics"].as<bool>(false);

  // charm production fraction reweight from the ntuple
  m_do_prod_fraction_rw = get_config()["do_prod_fraction_rw"].as<bool>(true);

  // charm production fraction reweight implemented in charmpp
  m_do_prod_fraction_rw_old =
      get_config()["do_prod_fraction_rw_old"].as<bool>(false);
}

ZDTruthLoop::ZDTruthLoop(TString input_file, TString out_path,
                         TString tree_name)
    : EventLoopBase(input_file, out_path, tree_name), TruthInfo() {}

}  // namespace Charm
