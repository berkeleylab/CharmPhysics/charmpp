#include <math.h>

#include <iostream>

#include "CharmLoopBase.h"
#include "HelperFunctions.h"
#include "TVector2.h"

namespace Charm {

std::string CharmLoopBase::getSidebandStr(double mass, double leftCut,
                                          double rightCut) {
  if (mass < leftCut) {
    return "_LS";
  } else if (mass < rightCut) {
    return "_PR";
  } else {
    return "_RS";
  }
}

float CharmLoopBase::calculate_ptv_weight() {
  float w = get_ptv_weight(m_truth_v_pt);
  return w;
}

float CharmLoopBase::get_dr_bjet_Dmeson(unsigned int jet, unsigned int i) {
  float deta = m_DMesons_eta->at(i) - m_AnalysisJets_eta->at(jet);
  float dphi =
      TVector2::Phi_mpi_pi(m_DMesons_phi->at(i) - m_AnalysisJets_phi->at(jet));
  return TMath::Sqrt(deta * deta + dphi * dphi);
}

std::string CharmLoopBase::get_period_string() {
  return get_period(m_EventInfo_runNumber);
}

void CharmLoopBase::debug_dmeson_printout(int index, bool printTruth) {
  if (index >= 0) {
    std::cout << "Reco D meson:" << std::endl;
    std::cout << "  pdgId: " << m_DMesons_pdgId->at(index) << std::endl;
    std::cout << "  pT: " << m_DMesons_pt->at(index) * Charm::GeV << std::endl;
    std::cout << "  eta: " << m_DMesons_eta->at(index) << std::endl;
    std::cout << "  phi: " << m_DMesons_phi->at(index) << std::endl;
    std::cout << "  m: " << m_DMesons_m->at(index) * Charm::GeV << std::endl;
    std::cout << "  Lxy: " << m_DMesons_fitOutput__Lxy->at(index) << std::endl;
    std::cout << "  Chi2: " << m_DMesons_fitOutput__Chi2->at(index)
              << std::endl;
    std::cout << "  sigma 3D: "
              << m_DMesons_fitOutput__ImpactSignificance->at(index)
              << std::endl;
    std::cout << "  truthBarcode: " << m_DMesons_truthBarcode->at(index)
              << std::endl;
    std::cout << "  num daughters: "
              << m_DMesons_daughterInfo__truthDBarcode->at(index).size()
              << std::endl;
    for (unsigned int i = 0;
         i < m_DMesons_daughterInfo__truthDBarcode->at(index).size(); i++) {
      std::cout << "  daughter track " << i << std::endl;
      std::cout << "    trackID: "
                << m_DMesons_daughterInfo__trackId->at(index).at(i)
                << std::endl;
      std::cout << "    truthDBarcode: "
                << m_DMesons_daughterInfo__truthDBarcode->at(index).at(i)
                << std::endl;
      std::cout << "    truthBarcode: "
                << m_DMesons_daughterInfo__truthBarcode->at(index).at(i)
                << std::endl;
      std::cout << "    pT: "
                << m_DMesons_daughterInfo__pt->at(index).at(i) * Charm::GeV
                << std::endl;
      std::cout << "    eta: " << m_DMesons_daughterInfo__eta->at(index).at(i)
                << std::endl;
      std::cout << "    phi: " << m_DMesons_daughterInfo__phi->at(index).at(i)
                << std::endl;
    }
  }
  if (printTruth) {
    std::cout << "Truth Recrod: " << m_TruthParticles_Selected_pdgId->size()
              << " particles in the ntuple" << std::endl;
    for (unsigned int i = 0; i < m_TruthParticles_Selected_pdgId->size(); i++) {
      std::cout << "truth particle " << i << std::endl;
      std::cout << "  pdgId: " << m_TruthParticles_Selected_pdgId->at(i)
                << std::endl;
      std::cout << "  barcode: " << m_TruthParticles_Selected_barcode->at(i)
                << std::endl;
      std::cout << "  pT: " << m_TruthParticles_Selected_pt->at(i) * Charm::GeV
                << std::endl;
      std::cout << "  eta: " << m_TruthParticles_Selected_eta->at(i)
                << std::endl;
      std::cout << "  phi: " << m_TruthParticles_Selected_phi->at(i)
                << std::endl;
      std::cout << "  num daughters: "
                << m_TruthParticles_Selected_daughterInfoT__pdgId->at(i).size()
                << std::endl;
      for (unsigned int j = 0;
           j < m_TruthParticles_Selected_daughterInfoT__pdgId->at(i).size();
           j++) {
        std::cout << "  truth particle " << j << std::endl;
        std::cout << "    pdgId: "
                  << m_TruthParticles_Selected_daughterInfoT__pdgId->at(i).at(j)
                  << std::endl;
        std::cout << "    barcode: "
                  << m_TruthParticles_Selected_daughterInfoT__barcode->at(i).at(
                         j)
                  << std::endl;
        std::cout << "    pT: "
                  << m_TruthParticles_Selected_daughterInfoT__pt->at(i).at(j) *
                         Charm::GeV
                  << std::endl;
        std::cout << "    eta: "
                  << m_TruthParticles_Selected_daughterInfoT__eta->at(i).at(j)
                  << std::endl;
        std::cout << "    phi: "
                  << m_TruthParticles_Selected_daughterInfoT__phi->at(i).at(j)
                  << std::endl;
      }
    }
  }
}

}  // namespace Charm
