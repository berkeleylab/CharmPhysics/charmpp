#include "ZplusDLoop.h"

#include <math.h>

#include <iostream>

namespace Charm {
int ZplusDLoop::execute() {
  // lepton Event Selection
  if (m_do_lep_presel) {
    for (unsigned int i = 0; i < m_nSyst; i++) {
      // check if failed preselection
      if (m_channel_sys.at(i) == "") {
        continue;
      }
      m_channel_sys.at(i) += "_SR";
    }
  }

  // D+ reconstruction (do after W cuts to save CPU)
  if (m_D_plus_meson) {
    get_D_plus_mesons(this);
    if (m_is_mc && m_truth_D) {
      get_truth_D_mesons(411);
    }
  }

  // D*+ reconstruction (do after W cuts to save CPU)
  if (m_D_star_meson) {
    get_D_star_mesons(this);
    if (m_is_mc && m_truth_D) {
      get_truth_D_mesons(413);
    }
  }

  if (m_D_star_pi0_meson) {
    get_D_star_pi0_mesons(this);
    if (m_is_mc && m_truth_D) {
      get_truth_D_mesons(413);
    }
  }

  // Ds reconstruction (do after W cuts to save CPU)
  if (m_D_s_meson) {
    get_D_s_mesons(this);
    if (m_is_mc && m_truth_D) {
      get_truth_D_mesons(431);
    }
  }

  // production fraction reweighting
  // must do this here becaue truth branches are not available in preselection
  if (m_is_mc && m_do_prod_fraction_rw_old) {
    reweight_production_fractions(this);
  }

  // jet selection
  if (m_jet_selection) {
    get_jets();
    jet_selection_for_sys();
  }

  // fill histograms
  fill_histograms_with_regions();

  return 1;
}

void ZplusDLoop::connect_branches() {
  // connect lepton branches
  ZCharmLoopBase::connect_branches();

  // additional branches for D meson reco
  if (m_D_plus_meson || m_D_star_meson || m_D_star_pi0_meson || m_D_s_meson) {
    connect_D_meson_branches(this);
  }

  // truth branches for D
  if (m_is_mc && (m_truth_matchD || m_truth_D || m_do_Nminus1 ||
                  m_fiducial_selection || m_save_truth_zjets)) {
    if (m_save_truth_zjets) {
      connect_truth_D_meson_branches(this, true);
    } else {
      connect_truth_D_meson_branches(this, m_fiducial_selection);
    }
  }
}

ZplusDLoop::ZplusDLoop(TString input_file, TString out_path, TString tree_name)
    : ZCharmLoopBase(input_file, out_path, tree_name) {}

}  // namespace Charm