#include <math.h>

#include <iostream>

#include "CharmLoopBase.h"
#include "HelperFunctions.h"
#include "TVector2.h"

namespace Charm {

void CharmLoopBase::get_track_jets() {
  m_track_jet_container = TrackJetContainer(
      m_AntiKtPV0TrackJets_pt[m_track_jet_radius],
      m_AntiKtPV0TrackJets_eta[m_track_jet_radius],
      m_AntiKtPV0TrackJets_phi[m_track_jet_radius],
      m_AntiKtPV0TrackJets_m[m_track_jet_radius],
      m_AntiKtPV0TrackJets_daughter__pt[m_track_jet_radius],
      m_AntiKtPV0TrackJets_daughter__eta[m_track_jet_radius],
      m_AntiKtPV0TrackJets_daughter__phi[m_track_jet_radius],
      m_AntiKtPV0TrackJets_daughter__trackId[m_track_jet_radius]);
  if (m_track_jet_radius != "VR") {
    m_truth_track_jet_container =
        TrackJetContainer(m_AntiKtTruthChargedJets_pt[m_track_jet_radius],
                          m_AntiKtTruthChargedJets_eta[m_track_jet_radius],
                          m_AntiKtTruthChargedJets_phi[m_track_jet_radius],
                          m_AntiKtTruthChargedJets_m[m_track_jet_radius]);
  } else {
    m_truth_track_jet_container = TrackJetContainer();  // empty for VR
  }

}  // get_track_jet

void CharmLoopBase::calculate_track_jet_properties(
    unsigned int DMeson_index, unsigned int sys_index, int truthIndex,
    TrackJetContainer *track_jets, TrackJetContainer *truth_track_jets,
    SysTrackJet *sysMatchedTrackJet) {
  int track_jet_index = -1;
  float track_jet_dR = 999;
  float track_jet_pt = 0.0;
  float track_jet_eta = 999;
  float track_jet_phi = 999;
  float track_jet_zt = 0.0;
  float track_jet_zl = 0.0;
  float track_jet_pt_rel = 0.0;
  for (unsigned int jet = 0; jet < track_jets->pt->size(); jet++) {
    float deta = m_DMesons_eta->at(DMeson_index) - track_jets->eta->at(jet);
    float dphi = TVector2::Phi_mpi_pi(m_DMesons_phi->at(DMeson_index) -
                                      track_jets->phi->at(jet));
    float dR = TMath::Sqrt(deta * deta + dphi * dphi);
    if (dR < track_jet_dR) {
      track_jet_dR = dR;
      track_jet_index = jet;
    }
  }
  sysMatchedTrackJet->track_jet_index.at(sys_index) = track_jet_index;
  sysMatchedTrackJet->track_jet_dR.at(sys_index) = track_jet_dR;
  if (track_jet_index >= 0) {
    // Replace the track-jet tracks with the re-fitted D-meson tracks
    /* Strategy:
        - Loop over the tracks in the track-jet
        - For each track, check if it is in the D meson
        - If it is, subtract the track from the jet and add the D meson track
        - The result is the re-fitted jet
    */
    TLorentzVector jet_p4;
    jet_p4.SetPtEtaPhiM(track_jets->pt->at(track_jet_index),
                        track_jets->eta->at(track_jet_index),
                        track_jets->phi->at(track_jet_index), 0.0);
    for (unsigned int i = 0;
         i < m_DMesons_daughterInfo__trackId->at(DMeson_index).size(); i++) {
      auto it = find(track_jets->daughter_trackId->at(track_jet_index).begin(),
                     track_jets->daughter_trackId->at(track_jet_index).end(),
                     m_DMesons_daughterInfo__trackId->at(DMeson_index).at(i));
      // If track contained in the jet
      if (it != track_jets->daughter_trackId->at(track_jet_index).end()) {
        // Track from the track-jet
        int index =
            it - track_jets->daughter_trackId->at(track_jet_index).begin();
        TLorentzVector track_in_jet_p4;
        track_in_jet_p4.SetPtEtaPhiM(
            track_jets->daughter_pt->at(track_jet_index).at(index),
            track_jets->daughter_eta->at(track_jet_index).at(index),
            track_jets->daughter_phi->at(track_jet_index).at(index), 0.0);

        // Track from the D meson
        TLorentzVector track_in_D_p4;
        float track_mass = 0;
        if (abs(m_DMesons_daughterInfo__pdgId->at(DMeson_index).at(i)) == 321) {
          track_mass = Charm::KAON_MASS;
        } else if (abs(m_DMesons_daughterInfo__pdgId->at(DMeson_index).at(i)) ==
                   211) {
          track_mass = Charm::PION_MASS;
        } else {
          std::cout << "ERROR: Unknown track type" << std::endl;
        }
        track_in_D_p4.SetPtEtaPhiM(
            m_DMesons_daughterInfo__pt->at(DMeson_index).at(i),
            m_DMesons_daughterInfo__eta->at(DMeson_index).at(i),
            m_DMesons_daughterInfo__phi->at(DMeson_index).at(i), track_mass);

        // Subtract and add to jet
        jet_p4 -= track_in_jet_p4;
        jet_p4 += track_in_D_p4;

        // Can only be matched once
        continue;
      }
    }
    track_jet_pt = jet_p4.Pt();
    track_jet_eta = jet_p4.Eta();
    track_jet_phi = jet_p4.Phi();
    track_jet_zt = m_DMesons_pt->at(DMeson_index) / track_jet_pt;

    TVector3 Dmeson_p3;
    Dmeson_p3.SetPtEtaPhi(m_DMesons_pt->at(DMeson_index),
                          m_DMesons_eta->at(DMeson_index),
                          m_DMesons_phi->at(DMeson_index));
    TVector3 jet_p3;
    jet_p3.SetPtEtaPhi(track_jets->pt->at(track_jet_index),
                       track_jets->eta->at(track_jet_index),
                       track_jets->phi->at(track_jet_index));
    track_jet_zl = Dmeson_p3.Dot(jet_p3) / jet_p3.Mag2();
    track_jet_pt_rel = Dmeson_p3.Cross(jet_p3).Mag() / jet_p3.Mag();

    // Fix for numerical issues
    // if all tracks match, the zt should be 1 by definition
    int n_match = countMatchingElements(
        m_DMesons_daughterInfo__trackId->at(DMeson_index),
        track_jets->daughter_trackId->at(track_jet_index));
    if (n_match == 3 &&
        track_jets->daughter_trackId->at(track_jet_index).size() == 3) {
      if (abs(track_jet_zt - 1.) < 1e-5) {
        track_jet_zt = 1.0;
      }
    }
  }
  sysMatchedTrackJet->track_jet_pt.at(sys_index) = track_jet_pt * Charm::GeV;
  sysMatchedTrackJet->track_jet_eta.at(sys_index) = track_jet_eta;
  sysMatchedTrackJet->track_jet_phi.at(sys_index) = track_jet_phi;
  sysMatchedTrackJet->track_jet_zt.at(sys_index) = track_jet_zt;
  sysMatchedTrackJet->track_jet_zt_unfold.at(sys_index) =
      getUnfoldingVariable(track_jet_pt * Charm::GeV, track_jet_zt);
  sysMatchedTrackJet->track_jet_zl.at(sys_index) = track_jet_zl;
  sysMatchedTrackJet->track_jet_zl_unfold.at(sys_index) =
      getUnfoldingVariable(track_jet_pt * Charm::GeV, track_jet_zl);
  sysMatchedTrackJet->track_jet_pt_rel.at(sys_index) = track_jet_pt_rel * Charm::GeV;
  sysMatchedTrackJet->track_jet_pt_rel_unfold.at(sys_index) = get_ptRel_variable(track_jet_pt * Charm::GeV, track_jet_pt_rel * Charm::GeV);

  // find matching truth jet
  int truth_track_jet_index = -1;
  float truth_track_jet_dR = 999;
  float truth_track_jet_pt = 0.0;
  float truth_track_jet_eta = 999;
  float truth_track_jet_phi = 999;
  float truth_track_jet_zt = 0.0;
  float truth_track_jet_zl = 0.0;
  float truth_track_jet_pt_rel = 0.0;
  if (truthIndex >= 0 && track_jet_index >= 0) {
    for (unsigned int jet = 0; jet < truth_track_jets->pt->size(); jet++) {
      float deta =
          truth_track_jets->eta->at(jet) - track_jets->eta->at(track_jet_index);
      float dphi = TVector2::Phi_mpi_pi(truth_track_jets->phi->at(jet) -
                                        track_jets->phi->at(track_jet_index));
      float dR = TMath::Sqrt(deta * deta + dphi * dphi);
      if (dR < truth_track_jet_dR) {
        truth_track_jet_dR = dR;
        truth_track_jet_index = jet;
      }
    }
  }
  sysMatchedTrackJet->truth_track_jet_index.at(sys_index) =
      truth_track_jet_index;
  sysMatchedTrackJet->truth_track_jet_dR.at(sys_index) = truth_track_jet_dR;
  if (truth_track_jet_index >= 0) {
    truth_track_jet_pt = truth_track_jets->pt->at(truth_track_jet_index);
    truth_track_jet_eta = truth_track_jets->eta->at(truth_track_jet_index);
    truth_track_jet_phi = truth_track_jets->phi->at(truth_track_jet_index);
    truth_track_jet_zt = m_TruthParticles_Selected_pt->at(truthIndex) /
                         truth_track_jets->pt->at(truth_track_jet_index);

    // calculate truth zl
    TVector3 truth_track_jet_p3;
    truth_track_jet_p3.SetPtEtaPhi(truth_track_jet_pt, truth_track_jet_eta,
                                   truth_track_jet_phi);
    TVector3 truth_Dmeson_p3;
    truth_Dmeson_p3.SetPtEtaPhi(m_TruthParticles_Selected_pt->at(truthIndex),
                                m_TruthParticles_Selected_eta->at(truthIndex),
                                m_TruthParticles_Selected_phi->at(truthIndex));
    truth_track_jet_zl =
        truth_Dmeson_p3.Dot(truth_track_jet_p3) / truth_track_jet_p3.Mag2();
    truth_track_jet_pt_rel =
        truth_Dmeson_p3.Cross(truth_track_jet_p3).Mag() / truth_track_jet_p3.Mag();

  }
  sysMatchedTrackJet->truth_track_jet_pt.at(sys_index) =
      truth_track_jet_pt * Charm::GeV;
  sysMatchedTrackJet->truth_track_jet_eta.at(sys_index) = truth_track_jet_eta;
  sysMatchedTrackJet->truth_track_jet_phi.at(sys_index) = truth_track_jet_phi;
  sysMatchedTrackJet->truth_track_jet_zt.at(sys_index) = truth_track_jet_zt;
  sysMatchedTrackJet->truth_track_jet_zl.at(sys_index) = truth_track_jet_zl;
  sysMatchedTrackJet->truth_track_jet_pt_rel.at(sys_index) = truth_track_jet_pt_rel  * Charm::GeV;
}

}  // namespace Charm
