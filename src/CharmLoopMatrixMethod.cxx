#include <math.h>

#include <iostream>

#include "CharmLoopBase.h"
#include "HelperFunctions.h"
#include "TVector2.h"

namespace Charm {

void CharmLoopBase::matrix_method_for_sys(unsigned int i, bool anti_tight) {
  float pt = m_sys_lep_pt.at(i);
  if (m_do_el_topocone_param) {
    pt = m_sys_lep_pt_plusTopoConeET20.at(i);
  }
  float eta = fabs(m_sys_lep_eta.at(i));
  float met = m_sys_met.at(i);
  float mt = m_sys_met_mt.at(i);
  int nbjet = m_sys_nbjets.at(i);
  std::string channel = "mu";
  if (m_loose_electrons_sys.at(i).size()) {
    channel = "el";
    eta = fabs(m_sys_lep_calo_eta.at(i));
  }
  float w = 0;
  float w2 = 1;
  if (m_inclusive_fake_rate) {
    w = matrix_method_weight_inclusive(i, pt, eta, mt, anti_tight, channel);
  } else {
    w = matrix_method_weight(i, pt, eta, met, mt, nbjet, anti_tight, channel);
  }

  if (m_rw_mm_closure) {
    w2 = matrix_method_closure_reweight(i, met, nbjet, channel);
  }

  multiply_event_weight(w * w2, i);
}

float CharmLoopBase::matrix_method_weight(unsigned int i, float pt, float eta,
                                          float met, float mt, int nbjet,
                                          bool anti_tight,
                                          std::string channel) {
  std::string sys_string = "";
  if (i > 0) {
    sys_string = m_systematics.at(i - 1);
  }

  // Get fake and real rates
  std::string DspeciesMM = "Dstar";
  if (!m_D_star_meson) {
    DspeciesMM = "Dplus";
  }
  bool fake_rate_one = false;
  float fake_rate = m_matrix_method_helper.get_fake_rate(
      sys_string, channel, "", DspeciesMM, nbjet, pt, eta, met, fake_rate_one);
  float real_rate = 1.0;
  if (!m_fake_factor_method) {
    real_rate = m_matrix_method_helper.get_real_rate(
        sys_string, channel, "", DspeciesMM, nbjet, pt, eta, mt);
  }

  // Non closure sys is the baseline case, w/o non closure reweighting
  // 2015 is given 100% uncertainty
  // FR uncertainty is handled here as well for simplicity, if the FR in the bin
  // exceeds 100%
  float non_closure_w = 1.0;
  if (fake_rate_one) {
    non_closure_w *= 2.0;
  }
  non_closure_w *= m_matrix_method_helper.get_non_closure_sys(
      sys_string, channel, "", DspeciesMM, nbjet, met);

  // Calculate weight
  if (anti_tight) {
    if (!m_is_mc) {
      return real_rate * fake_rate / (real_rate - fake_rate) * non_closure_w;
    } else {
      return -1.0 * real_rate * fake_rate / (real_rate - fake_rate) *
             non_closure_w;
    }
  } else {
    if (m_fake_factor_method) {
      throw std::runtime_error(
          "should not be calculating the Tight region for the Fake Factor "
          "method");
    }
    // Sign is inverted here, to generate positive Tight distributions which are
    // later subtracted from AT distributions
    return ((1 - real_rate) * fake_rate / (real_rate - fake_rate)) *
           non_closure_w;
  }
}

float CharmLoopBase::matrix_method_weight_inclusive(unsigned int i, float pt,
                                                    float eta, float mt,
                                                    bool anti_tight,
                                                    std::string channel) {
  std::string sys_string = "";
  if (i > 0) {
    sys_string = m_systematics.at(i - 1);
  }

  bool fake_rate_one = false;
  float fake_rate = m_matrix_method_helper.get_fake_rate_incl(
      sys_string, channel, "", pt, eta, fake_rate_one);
  float real_rate = m_matrix_method_helper.get_real_rate_incl(
      sys_string, channel, "", pt, eta, mt);

  if (anti_tight) {
    return real_rate * fake_rate / (real_rate - fake_rate);
  } else {
    return (1 - real_rate) * fake_rate / (real_rate - fake_rate);
  }
}

float CharmLoopBase::matrix_method_closure_reweight(unsigned int i, float met,
                                                    int nbjet,
                                                    std::string channel) {
  std::string sys_string = "";
  if (i > 0) {
    sys_string = m_systematics.at(i - 1);
  }

  std::string DspeciesMM = "Dstar";
  if (m_D_plus_meson && !m_D_star_meson) {
    DspeciesMM = "Dplus";
  }

  return m_matrix_method_helper.get_closure_rw(sys_string, channel, "",
                                               DspeciesMM, nbjet, met);
}

}  // namespace Charm
