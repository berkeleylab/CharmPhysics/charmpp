#ifndef W_PLUS_CHARM_H
#define W_PLUS_CHARM_H

#include "CharmLoopBase.h"

namespace Charm {
class WplusDLoop : public CharmLoopBase {
 public:
  WplusDLoop(TString input_file, TString out_path,
             TString tree_name = "CharmAnalysis");

 protected:
  virtual void connect_branches() override;

 private:
  virtual int execute() override;
};

}  // namespace Charm

#endif  // W_PLUS_CHARM_H
