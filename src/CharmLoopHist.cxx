#include <math.h>

#include <iostream>

#include "CharmLoopBase.h"
#include "HelperFunctions.h"
#include "TVector2.h"

namespace Charm {

void CharmLoopBase::fill_Dmeson_variables(std::vector<std::string> &channel,
                                          std::vector<bool> &pass_dR_cut,
                                          std::vector<double> &extra_weight,
                                          int dmeson_idx,
                                          std::vector<float> *dmeson_mass,
                                          std::vector<float> *dmeson_dRlep,
                                          bool fillTruth) {
  if (!m_fit_variables_only) {
    add_fill_hist_sys("Dmeson_dRlep", channel, *dmeson_dRlep, 120, 0., 6.,
                      false, pass_dR_cut, extra_weight);

    if (fillTruth) {
      add_fill_hist_sys("truth_Dmeson_pt", channel, m_truth_D_pt, 150, 0, 150.,
                        false, pass_dR_cut, extra_weight);
      add_fill_hist_sys("truth_Dmeson_eta", channel, m_truth_D_eta, 100, -3.0,
                        3.0, false, pass_dR_cut, extra_weight);
      add_fill_hist_sys("truth_Dmeson_m", channel, m_truth_D_mass * Charm::GeV,
                        200, 1.4, 2.4, false, pass_dR_cut, extra_weight);
      add_fill_hist_sys("truth_Dmeson_lep_eta", channel, m_truth_abs_lep_eta,
                        25, 0, 2.5, false, pass_dR_cut, extra_weight);
    }
  }
}

void CharmLoopBase::fill_unfolding_variables(std::vector<std::string> &channel,
                                             std::vector<bool> &pass_dR_cut,
                                             std::vector<double> &extra_weight,
                                             int dmeson_idx,
                                             int truth_dmeson_idx) {
  // check if the truth D meson is found
  if (truth_dmeson_idx < 0) {
    return;
  }

  double dmeson_pt = m_DMesons_pt->at(dmeson_idx);
  double truth_dmeson_pt = m_TruthParticles_Selected_pt->at(truth_dmeson_idx) * Charm::GeV;
  // transfer matrix for pT(D)
  add_fill_hist_sys("Dmeson_transfer_matrix", channel, dmeson_pt * Charm::GeV,
                    truth_dmeson_pt, m_differential_bins.get_n_differential(),
                    m_differential_bins.get_differential_bins(),
                    m_differential_bins.get_n_differential(),
                    m_differential_bins.get_differential_bins(), false,
                    pass_dR_cut, extra_weight);
  add_fill_hist_sys("Dmeson_truth_differential_pt", channel, truth_dmeson_pt,
                    m_differential_bins.get_n_differential(),
                    m_differential_bins.get_differential_bins(), false,
                    pass_dR_cut, extra_weight);
  add_fill_hist_sys("Dmeson_differential_pt", channel, dmeson_pt * Charm::GeV,
                    m_differential_bins.get_n_differential(),
                    m_differential_bins.get_differential_bins(), false,
                    pass_dR_cut, extra_weight);

  // transfer matrix for eta(lepton)
  add_fill_hist_sys("Dmeson_transfer_matrix_eta", channel, m_sys_lep_abs_eta,
                    m_truth_abs_lep_eta,
                    m_differential_bins_eta.get_n_differential(),
                    m_differential_bins_eta.get_differential_bins(),
                    m_differential_bins_eta.get_n_differential(),
                    m_differential_bins_eta.get_differential_bins(), false,
                    pass_dR_cut, extra_weight);
  add_fill_hist_sys("Dmeson_truth_differential_lep_eta", channel,
                    m_sys_lep_abs_eta,
                    m_differential_bins_eta.get_n_differential(),
                    m_differential_bins_eta.get_differential_bins(), false,
                    pass_dR_cut, extra_weight);
  add_fill_hist_sys("Dmeson_differential_lep_eta", channel, m_truth_abs_lep_eta,
                    m_differential_bins_eta.get_n_differential(),
                    m_differential_bins_eta.get_differential_bins(), false,
                    pass_dR_cut, extra_weight);
}

void CharmLoopBase::fill_trackJet_variables(
    std::vector<std::string> &channel, std::vector<bool> &pass_dR_cut,
    std::vector<double> &extra_weight, unsigned int dmeson_idx,
    std::unique_ptr<SysTrackJet> &matchedTrackJet, bool fillTruth) {
  std::string r = m_track_jet_radius.c_str();
  add_fill_hist_sys(Form("Dmeson_track_jet_%s_pt", r.c_str()), channel,
                    matchedTrackJet->track_jet_pt, 30, 0, 150., true,
                    pass_dR_cut, extra_weight);
  add_fill_hist_sys(Form("Dmeson_track_jet_%s_pt_rel", r.c_str()), channel,
                    matchedTrackJet->track_jet_pt_rel, 50, 0, 50., true,
                    pass_dR_cut, extra_weight);
  add_fill_hist_sys(Form("Dmeson_track_jet_%s_pt_rel_unfold", r.c_str()), channel,
                    matchedTrackJet->track_jet_pt_rel_unfold,
                    Charm::NRANGES * Charm::NBINSPERRANGE, 0.,
                    Charm::NRANGES * Charm::NBINSPERRANGE, true,
                    pass_dR_cut, extra_weight);
  add_fill_hist_sys(Form("Dmeson_track_jet_%s_zt_unfold", r.c_str()), channel,
                    matchedTrackJet->track_jet_zt_unfold,
                    Charm::NRANGES * Charm::NBINSPERRANGE, 0.,
                    Charm::NRANGES * Charm::NBINSPERRANGE, true,
                    pass_dR_cut, extra_weight);
  add_fill_hist_sys(Form("Dmeson_track_jet_%s_zl_unfold", r.c_str()), channel,
                    matchedTrackJet->track_jet_zl_unfold,
                    Charm::NRANGES * Charm::NBINSPERRANGE, 0.,
                    Charm::NRANGES * Charm::NBINSPERRANGE, true,
                    pass_dR_cut, extra_weight);

  if (!m_fit_variables_only) {
    add_fill_hist_sys(Form("Dmeson_track_jet_%s_dR", r.c_str()), channel,
                      matchedTrackJet->track_jet_dR, 120, 0., 6., false,
                      pass_dR_cut, extra_weight);
    add_fill_hist_sys(Form("Dmeson_track_jet_%s_eta", r.c_str()), channel,
                      matchedTrackJet->track_jet_eta, 100, -3.0, 3.0, true,
                      pass_dR_cut, extra_weight);
    add_fill_hist_sys(Form("Dmeson_track_jet_%s_zt", r.c_str()), channel,
                      matchedTrackJet->track_jet_zt, 120, 0., 1.2, true,
                      pass_dR_cut, extra_weight);
    add_fill_hist_sys(Form("Dmeson_track_jet_%s_zl", r.c_str()), channel,
                      matchedTrackJet->track_jet_zl, 120, 0., 1.2, true,
                      pass_dR_cut, extra_weight);
  }  // ! m_fit_variables_only

  if (m_unfolding_variables && fillTruth) {
    // truth quantities
    add_fill_hist_sys(Form("truth_Dmeson_track_jet_%s_dR", r.c_str()),
                      channel, m_sys_matched_track_jet->truth_track_jet_dR,
                      120, 0., 6., false, pass_dR_cut, extra_weight);
    add_fill_hist_sys(Form("truth_Dmeson_track_jet_%s_pt", r.c_str()),
                      channel, m_sys_matched_track_jet->truth_track_jet_pt,
                      150, 0, 150., false, pass_dR_cut, extra_weight);
    add_fill_hist_sys(Form("truth_Dmeson_track_jet_%s_eta", r.c_str()),
                      channel, m_sys_matched_track_jet->truth_track_jet_eta,
                      100, -3.0, 3.0, false, pass_dR_cut, extra_weight);
    add_fill_hist_sys(Form("truth_Dmeson_track_jet_%s_zt", r.c_str()),
                      channel, m_sys_matched_track_jet->truth_track_jet_zt,
                      120, 0., 1.2, false, pass_dR_cut, extra_weight);
    add_fill_hist_sys(Form("truth_Dmeson_track_jet_%s_zl", r.c_str()),
                      channel, m_sys_matched_track_jet->truth_track_jet_zl,
                      120, 0., 1.2, false, pass_dR_cut, extra_weight);

    // 2D transfer marix style histograms
    add_fill_hist_sys(
        Form("Dmeson_transfer_matrix_track_jet_%s_pt", r.c_str()), channel,
        m_sys_matched_track_jet->track_jet_pt,
        m_sys_matched_track_jet->truth_track_jet_pt, 30, 0, 150., 30, 0, 150.,
        false, pass_dR_cut, extra_weight);
    add_fill_hist_sys(
        Form("Dmeson_transfer_matrix_track_jet_%s_zt", r.c_str()), channel,
        m_sys_matched_track_jet->track_jet_zt,
        m_sys_matched_track_jet->truth_track_jet_zt, 30, 0., 1.2, 30, 0., 1.2,
        false, pass_dR_cut, extra_weight);
    add_fill_hist_sys(
        Form("Dmeson_transfer_matrix_track_jet_%s_zl", r.c_str()), channel,
        m_sys_matched_track_jet->track_jet_zl,
        m_sys_matched_track_jet->truth_track_jet_zl, 30, 0., 1.2, 30, 0., 1.2,
        false, pass_dR_cut, extra_weight);
  }  // truth

}  // fill_trackJet_variables


void CharmLoopBase::fill_closest_jet_variables(
    std::vector<std::string> &channel, std::vector<bool> &pass_dR_cut,
    std::vector<double> &extra_weight, unsigned int dmeson_idx,
    std::unique_ptr<SysJet> &sysClosestJet, bool fillTruth) {
  add_fill_hist_sys("Dmeson_closest_jet_pt", channel,
                    sysClosestJet->jet_pt, 150, 0, 150., true,
                    pass_dR_cut, extra_weight);
  add_fill_hist_sys("Dmeson_closest_jet_eta", channel,
                    sysClosestJet->jet_eta, 100, -3.0, 3.0, false,
                    pass_dR_cut, extra_weight);
  add_fill_hist_sys("Dmeson_closest_jet_dR", channel,
                    sysClosestJet->jet_dR, 120, 0., 6., false,
                    pass_dR_cut, extra_weight);
  add_fill_hist_sys("Dmeson_closest_jet_phi", channel,
                    sysClosestJet->jet_phi, 100, -M_PI, M_PI, false,
                    pass_dR_cut, extra_weight);
  add_fill_hist_sys("Dmeson_closest_jet_zt", channel,
                    sysClosestJet->jet_zt, 120, 0., 1.2, true,
                    pass_dR_cut, extra_weight);
  add_fill_hist_sys("Dmeson_closest_jet_ftag_select_70", channel,
                    sysClosestJet->jet_ftag_select_70, 4, -1., 3., true,
                    pass_dR_cut, extra_weight);
  add_fill_hist_sys("Dmeson_closest_jet_ftag_effSF", channel,
                    sysClosestJet->jet_ftag_effSF, 100, 0.9, 1.1, true,
                    pass_dR_cut, extra_weight);
}


void CharmLoopBase::fill_PV_histograms(std::string channel) {
  auto channels = std::vector<std::string>(m_nSyst, channel);
  fill_PV_histograms(channels);
}

void CharmLoopBase::fill_PV_histograms(std::vector<std::string> channels,
                                       std::vector<bool> extra_flag,
                                       std::vector<double> extra_weight) {
  if (m_fit_variables_only) {
    return;
  }
  add_fill_hist_sys("PV_X", channels, m_CharmEventInfo_PV_X, 100, -0.8, -0.2,
                    false, extra_flag, extra_weight);
  add_fill_hist_sys("PV_Y", channels, m_CharmEventInfo_PV_Y, 100, -0.8, -0.2,
                    false, extra_flag, extra_weight);
  add_fill_hist_sys("PV_Z", channels, m_CharmEventInfo_PV_Z, 100, -200, 200,
                    false, extra_flag, extra_weight);
}

void CharmLoopBase::fill_lepton_histograms(std::string channel) {
  auto channels = std::vector<std::string>(m_nSyst, channel);
  fill_lepton_histograms(channels);
}

void CharmLoopBase::fill_lepton_histograms(std::vector<std::string> channels,
                                           std::vector<bool> extra_flag,
                                           std::vector<double> extra_weight) {
  if (m_fit_variables_only) {
    return;
  }
  add_fill_hist_sys("lep_pt", channels, m_sys_lep_pt, 400, 0, 400, false,
                    extra_flag, extra_weight);
  if (m_do_el_topocone_param) {
    add_fill_hist_sys("lep_pt_plusTopoConeET20", channels,
                      m_sys_lep_pt_plusTopoConeET20, 400, 0, 400, true,
                      extra_flag, extra_weight);
  }
  add_fill_hist_sys("lep_eta", channels, m_sys_lep_eta, 100, -3.0, 3.0, true,
                    extra_flag, extra_weight);
  add_fill_hist_sys("lep_phi", channels, m_sys_lep_phi, 100, -M_PI, M_PI, false,
                    extra_flag, extra_weight);
  add_fill_hist_sys("lep_charge", channels, m_sys_lep_charge, 2, -2, 2, true,
                    extra_flag, extra_weight);
  add_fill_hist_sys("met_mt", channels, m_sys_met_mt, 400, 0, 400, true,
                    extra_flag, extra_weight);
  add_fill_hist_sys("met_met", channels, m_sys_met, 400, 0, 400, true,
                    extra_flag, extra_weight);
  add_fill_hist_sys("met_dphi", channels, m_sys_met_dphi, 100, -M_PI, M_PI,
                    false, extra_flag, extra_weight);
  add_fill_hist_sys("pileup", channels,
                    m_EventInfo_correctedScaled_averageInteractionsPerCrossing,
                    80, 0, 80, false, extra_flag, extra_weight);
  add_fill_hist_sys("run_number", channels, m_EventInfo_runNumber, 150, 250000,
                    400000, false, extra_flag, extra_weight);
  if (m_fiducial_selection || m_save_truth_wjets) {
    add_fill_hist_sys("truth_lep_pt", channels, m_truth_lep1.Pt(), 400, 0, 400,
                      false, extra_flag, extra_weight);
    add_fill_hist_sys("truth_lep_eta", channels, m_truth_lep1.Eta(), 100, -3.0,
                      3.0, false, extra_flag, extra_weight);
    add_fill_hist_sys("truth_lep_phi", channels, m_truth_lep1.Phi(), 100, -M_PI,
                      M_PI, false, extra_flag, extra_weight);
    add_fill_hist_sys("truth_v_pt", channels, m_truth_v.Pt(), 400, 0, 400,
                      false, extra_flag, extra_weight);
    add_fill_hist_sys("truth_v_eta", channels, m_truth_v.Eta(), 100, -3.0, 3.0,
                      false, extra_flag, extra_weight);
    add_fill_hist_sys("truth_v_phi", channels, m_truth_v.Phi(), 100, -M_PI,
                      M_PI, false, extra_flag, extra_weight);
    add_fill_hist_sys("truth_v_m", channels, m_truth_v.M(), 400, 0, 400, false,
                      extra_flag, extra_weight);
    add_fill_hist_sys("truth_met_met", channels, m_truth_lep2.Pt(), 400, 0, 400,
                      false, extra_flag, extra_weight);
    add_fill_hist_sys("truth_met_mt", channels, m_truth_mt, 400, 0, 400, false,
                      extra_flag, extra_weight);
  }
  if (m_jet_selection) {
    add_fill_hist_sys("njets", channels, m_jets.size(), 20, -0.5, 19.5, true,
                      extra_flag, extra_weight);
    add_fill_hist_sys("nbjets", channels, m_sys_nbjets, 20, -0.5, 19.5, true,
                      extra_flag, extra_weight);
  }
  if (m_do_extra_histograms) {
    add_fill_hist_sys("lep_d0", channels, m_d0 * Charm::mum, 400, -200, 200,
                      true, extra_flag, extra_weight);
    add_fill_hist_sys("lep_d0sig", channels, m_d0sig, 100, -6.0, 6.0, true,
                      extra_flag, extra_weight);
    add_fill_hist_sys(
        "lep_d0_fixed", channels,
        rescaled_d0(m_d0, m_EventInfo_runNumber, m_is_mc) * Charm::mum, 400,
        -200, 200, true, extra_flag, extra_weight);
    add_fill_hist_sys(
        "lep_d0sig_fixed", channels,
        m_d0sig / m_d0 * rescaled_d0(m_d0, m_EventInfo_runNumber, m_is_mc), 100,
        -6.0, 6.0, true, extra_flag, extra_weight);
    add_fill_hist_sys("lep_z0sinTheta", channels, m_z0sinTheta, 100, -0.6, 0.6,
                      false, extra_flag, extra_weight);
    add_fill_hist_sys("lep_topoetcone20", channels, m_topoetcone20, 100, 0.0,
                      10.0, false, extra_flag, extra_weight);
    add_fill_hist_sys("lep_topoetcone20_over_pt", channels,
                      m_topoetcone20_over_pt, 100, 0.0, 0.16, false, extra_flag,
                      extra_weight);
    add_fill_hist_sys("lep_ptvarcone30_TightTTVA_pt1000", channels,
                      m_ptvarcone30_TightTTVA_pt1000, 100, 0.0, 10.0, false,
                      extra_flag, extra_weight);
    add_fill_hist_sys("lep_ptvarcone30_TightTTVA_pt1000_over_pt", channels,
                      m_ptvarcone30_TightTTVA_pt1000_over_pt, 100, 0.0, 0.1,
                      false, extra_flag, extra_weight);
    add_fill_hist_sys("lep_ptvarcone20_TightTTVA_pt1000", channels,
                      m_ptvarcone20_TightTTVA_pt1000, 100, 0.0, 10.0, false,
                      extra_flag, extra_weight);
    add_fill_hist_sys("lep_ptvarcone20_TightTTVA_pt1000_over_pt", channels,
                      m_ptvarcone20_TightTTVA_pt1000_over_pt, 100, 0.0, 0.1,
                      false, extra_flag, extra_weight);
    add_fill_hist_sys("lep_newflowisol_over_pt", channels, m_newflowisol, 200,
                      0.0, 0.5, false, extra_flag, extra_weight);
    add_fill_hist_sys("lep_is_isolated", channels, m_lep_is_isolated, 2, 0.0,
                      2.0, false, extra_flag, extra_weight);
    // add_fill_hist_sys("lep_EOverP", channels, m_EOverP,
    //                         1100, 0.0, 11.0, false, extra_flag,
    //                         extra_weight);
  }
  if (m_fake_rate_measurement) {
    add_fill_hist_sys("lep_pt_calo_eta", channels, m_sys_lep_pt,
                      m_sys_lep_calo_eta, 400, 0, 400, 5, m_eta_bins_el, true,
                      extra_flag, extra_weight);
    add_fill_hist_sys("lep_pt_eta", channels, m_sys_lep_pt, m_sys_lep_eta, 400,
                      0, 400, 3, m_eta_bins_mu, true, extra_flag, extra_weight);
    add_fill_hist_sys("lep_pt_calo_eta_plusTopoConeET20", channels,
                      m_sys_lep_pt_plusTopoConeET20, m_sys_lep_calo_eta, 400, 0,
                      400, 5, m_eta_bins_el, true, extra_flag, extra_weight);
    add_fill_hist_sys("lep_pt_eta_plusTopoConeET20", channels,
                      m_sys_lep_pt_plusTopoConeET20, m_sys_lep_eta, 400, 0, 400,
                      3, m_eta_bins_mu, true, extra_flag, extra_weight);
    add_fill_hist_sys("lep_pt_met", channels, m_sys_lep_pt, m_sys_met, 400, 0,
                      400, 5, m_met_bins_lep, true, extra_flag, extra_weight);
    add_fill_hist_sys("lep_pt_met_plusTopoConeET20", channels,
                      m_sys_lep_pt_plusTopoConeET20, m_sys_met, 400, 0, 400, 5,
                      m_met_bins_lep, true, extra_flag, extra_weight);
  }
}

void CharmLoopBase::fill_histograms_with_matrix_method() {
  if (!m_do_lep_presel) {
    fill_histograms();
    return;
  }
  if (m_fake_rate_measurement || m_fake_factor_method ||
      (!m_is_mc && m_eval_fake_rate)) {
    // calculate weight for anti-tight events
    for (unsigned int i = 0; i < m_nSyst; i++) {
      if (m_channel_sys.at(i) == "") {
        continue;
      }
      if (m_sys_anti_tight.at(i)) {
        m_channel_sys.at(i) = "AntiTight_" + m_channel_sys.at(i);
        if (m_eval_fake_rate) {
          matrix_method_for_sys(i, true);
        }
      } else {
        if (!m_eval_fake_rate && m_fake_rate_measurement) {
          m_channel_sys.at(i) = "Tight_" + m_channel_sys.at(i);
        }
      }
    }

    // fill histograms (anti-tight and regular SR)
    fill_histograms();

    // exit here if only doing the fake rate measurement
    if (m_fake_rate_measurement || m_fake_factor_method) {
      return;
    }

    // blank out anti-tight regions so it doesn't get filled again
    for (unsigned int i = 0; i < m_nSyst; i++) {
      if (m_sys_anti_tight.at(i)) {
        m_channel_sys.at(i) = "";
      }
    }

    // calculate weight for tight events
    for (unsigned int i = 0; i < m_nSyst; i++) {
      if (m_channel_sys.at(i) == "") {
        continue;
      }
      if (!m_sys_anti_tight.at(i)) {
        m_channel_sys.at(i) = "Tight_" + m_channel_sys.at(i);
        if (m_eval_fake_rate) {
          matrix_method_for_sys(i, false);
        }
      }
    }

    // fill histograms (only tight)
    fill_histograms();
  } else {
    // not using the matrix method
    fill_histograms();
  }
}

void CharmLoopBase::fill_histograms(std::string channel) {
  // Global, cross D variable used in Real Rate Measurement
  m_have_one_D = false;

  // PV histograms
  if (m_do_PV && m_fill_wjets) {
    fill_PV_histograms(channel);
  }

  // fill lepton histograms
  if (m_do_lep_presel && m_fill_wjets) {
    fill_lepton_histograms(channel);
  }

  // fill D plus histograms
  if (m_D_plus_meson) {
    fill_D_plus_histograms(channel);
  }

  // fill D star histograms
  if (m_D_star_meson) {
    fill_D_star_histograms(channel);
  }

  // fill D star pi0 histograms
  if (m_D_star_pi0_meson) {
    fill_D_star_pi0_histograms(channel);
  }

  // fill D s histograms
  if (m_D_s_meson) {
    fill_D_s_histograms(channel);
  }

  // fill D s histograms
  if (m_Lambda_C_baryon) {
    fill_Lambda_C_histograms(channel);
  }

  // stand-alone truth
  if (m_truth_D && m_channel_sys.at(0) != "") {
    TruthInfo::fill_stand_alone_truth(this);
  }
}

}  // namespace Charm
