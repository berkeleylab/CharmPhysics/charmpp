#ifndef W_MET_TEMPLATE_H
#define W_MET_TEMPLATE_H

#include "CharmLoopBase.h"

namespace Charm {
class WMETTemplate : public CharmLoopBase {
 public:
  WMETTemplate(TString input_file, TString out_path,
               TString tree_name = "CharmAnalysis");

 protected:
  virtual void connect_branches() override;

 private:
  virtual int execute() override;

 private:
  bool m_qcd_cr;
};

}  // namespace Charm

#endif  // MET_TEMPLATE_H
