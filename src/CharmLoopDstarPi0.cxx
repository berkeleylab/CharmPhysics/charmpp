#include <math.h>

#include <iostream>

#include "CharmLoopBase.h"
#include "HelperFunctions.h"
#include "TVector2.h"

namespace Charm {

void CharmLoopBase::fill_D_star_pi0_histograms(std::string channel) {
  // number of D mesons
  if (m_fill_wjets && !m_fit_variables_only) {
    add_fill_hist_sys("N_DstarKPiPi0", channel, m_D_star_pi0_indices.size(), 20,
                      -0.5, 19.5, true);
  }

  // single entry region with at least one D meson
  if (m_do_single_entry_D && m_D_star_pi0_indices.size()) {
    auto single_entry_channel = std::vector<std::string>(m_nSyst, channel);
    auto sys_at_least_one_D = std::vector<bool>(m_nSyst, false);
    std::vector<float> multiplicity = std::vector<float>(m_nSyst, 0);
    bool has_D = false;
    for (unsigned int j = 0; j < m_nSyst; j++) {
      // loop over D star D*pi0 mesons
      for (unsigned int i = 0; i < m_D_star_pi0_indices.size(); i++) {
        // retreive the D meson
        const auto D_index = m_D_star_pi0_indices.at(i);

        // dR cut
        float dRlep = 999;
        if (m_do_lep_presel) {
          dRlep = DMesonRec::D_meson_lepton_dR(D_index, m_sys_lep_eta.at(j),
                                               m_sys_lep_phi.at(j));
        }
        if (dRlep >= 0.3) {
          sys_at_least_one_D.at(j) = true;
          multiplicity.at(j) += 1;
        }
      }
      if (multiplicity.at(j) > 0) {
        single_entry_channel.at(j) += "_DstarKPiPi0";
        has_D = true;
      }
    }
    if (m_do_single_entry_D_only && m_have_one_D)
      return;
    else if (m_do_single_entry_D_only && !m_have_one_D && has_D)
      m_have_one_D = true;

    if (m_do_lep_presel) {
      fill_lepton_histograms(single_entry_channel, sys_at_least_one_D);
    }
    if (m_do_PV) {
      fill_PV_histograms(single_entry_channel, sys_at_least_one_D);
    }
    add_fill_hist_sys("N_DstarKPiPi0", single_entry_channel, multiplicity, 20,
                      -0.5, 19.5, true, sys_at_least_one_D);
    if (m_do_single_entry_D_only) {
      return;
    }
  }

  // counters for D multiplicity
  std::vector<std::unordered_map<std::string, unsigned int>>
      multiplicity_counter;
  std::set<std::string> used_channels;

  // loop over D star mesons
  for (unsigned int i = 0; i < m_D_star_pi0_indices.size(); i++) {
    // retreive the D meson
    const auto D_index = m_D_star_pi0_indices.at(i);

    // truth category
    bool matched_signal = false;
    std::string truth_category = "";
    int truthIndex = -1;
    if (m_truth_matchD && m_is_mc) {
      truth_category = TruthInfo::get_truth_category(this, D_index, "Kpipipi0",
                                                     m_fiducial_selection);
      truthIndex =
          TruthInfo::getMatchedBarcode(m_DMesons_truthBarcode->at(D_index));
    }
    if (truthIndex >= 0) {
      m_truth_D_pt = m_TruthParticles_Selected_pt->at(truthIndex) * Charm::GeV;
      m_truth_D_eta = m_TruthParticles_Selected_eta->at(truthIndex);
      m_truth_D_mass = truth_daughter_mass(truthIndex);
      m_truth_abs_lep_eta = std::abs(m_truth_lep1.Eta());
      if (truth_category == "Matched") {
        if (m_bin_in_truth_D_pt) {
          truth_category +=
              "_truth_pt_bin" +
              std::to_string(m_differential_bins.get_pt_bin(m_truth_D_pt) + 1);
        } else if (m_bin_in_truth_lep_eta) {
          truth_category +=
              "_truth_eta_bin" +
              std::to_string(
                  m_differential_bins_eta.get_eta_bin(m_truth_abs_lep_eta) + 1);
        }
      }
    } else {
      m_truth_D_pt = 0;
      m_truth_D_eta = -999;
      m_truth_D_mass = 0;
      m_truth_abs_lep_eta = -999;
    }

    // signal only
    if (m_signal_only && !matched_signal) {
      continue;
    }

    // dR w.r.t. the lepton depends on systematic
    auto sys_dRlep = std::vector<float>(m_nSyst, 0.);

    // dR w.r.t. the b-jet depends on systematic
    auto sys_dRbjet = std::vector<float>(m_nSyst, 0.);

    // OS or SS region
    auto charged_channel = std::vector<std::string>(m_nSyst, channel);
    auto pass_dR_cut = std::vector<bool>(m_nSyst, false);
    auto extra_weight = std::vector<double>(m_nSyst, 1.0);

    for (unsigned int j = 0; j < m_nSyst; j++) {
      multiplicity_counter.push_back({});
      if (!m_do_lep_presel ||
          (event_pass_sys(j) && m_sys_lep_charge.at(j) != 0)) {
        // dR w.r.t. the lepton
        float dRlep = 999;
        float lep_charge = 0;
        if (m_do_lep_presel) {
          dRlep = DMesonRec::D_meson_lepton_dR(D_index, m_sys_lep_eta.at(j),
                                               m_sys_lep_phi.at(j));
          lep_charge = m_sys_lep_charge.at(j);
        }
        sys_dRlep.at(j) = dRlep;

        // dR cut
        if (dRlep < 0.3) {
          continue;
        }

        // dR w.r.t. b-jets
        float dRbjet = 999;

        if (m_jet_selection && m_do_lep_presel) {
          auto jets = m_jets_sys.at(j);
          for (auto &jet : jets) {
            if (m_AnalysisJets_ftag_select_DL1r_FixedCutBEff_70->at(jet)) {
              float dR = get_dr_bjet_Dmeson(jet, D_index);
              if (dR < dRbjet) {
                dRbjet = dR;
              }
            }
          }
          sys_dRbjet.at(j) = dRbjet;
        }

        // sys string
        std::string sys_string = "";
        if (j > 0) {
          sys_string = m_systematics.at(j - 1);
        }

        // track efficiency sys weight
        double track_weight = m_track_sys_helper.get_track_eff_weight(
            sys_string, m_DMesons_pt->at(D_index) * Charm::GeV,
            m_DMesons_eta->at(D_index), m_DMesons_pdgId->at(D_index));
        extra_weight.at(j) *= track_weight;

        // modeling uncertainty
        if (matched_signal) {
          double fid_eff_weight = 1.0;
          if (m_bin_in_D_pt) {
            fid_eff_weight = m_modeling_uncertainty_helper.get_weight_pt(
                sys_string, m_truth_D_pt, "Dstar");
          } else if (m_bin_in_lep_eta) {
            fid_eff_weight = m_modeling_uncertainty_helper.get_weight_eta(
                sys_string, m_truth_abs_lep_eta, "Dstar");
          }
          extra_weight.at(j) *= fid_eff_weight;
        }
        // weight for SPG forced decay sample
        if (matched_signal && m_reweight_spg &&
            m_data_period == "ForcedDecay") {
          extra_weight.at(j) *= m_spg_forced_decay_helper.get_weight(
              m_DMesons_m->at(D_index) * Charm::GeV,
              m_differential_bins.get_pt_bin(m_truth_D_pt), "Dstar");
        }

        // replace lepton charge with the truth D charge for SPG samples
        //  - OS if they have the same charge
        //  - SS if they have the opposite charge
        if (m_data_period == "ForcedDecay") {
          lep_charge = -1 * m_TruthParticles_Selected_pdgId->at(0) /
                       fabs(m_TruthParticles_Selected_pdgId->at(0));
        }
        pass_dR_cut.at(j) = true;
        charged_channel.at(j) +=
            "_DstarKPiPi0" + get_charge_prefix(lep_charge, D_index);
        multiplicity_counter.at(j)[charged_channel.at(j)]++;
        used_channels.insert(charged_channel.at(j));
      }
    }

    // lepton histograms for this channel
    if (m_do_lep_presel) {
      fill_lepton_histograms(charged_channel, pass_dR_cut, extra_weight);
    }
    if (m_do_PV) {
      fill_PV_histograms(charged_channel, pass_dR_cut, extra_weight);
    }

    // D star pi0 meson specific histograms
    if (!m_fit_variables_only) {
      add_fill_hist_sys("Dmeson_dRlep", charged_channel, sys_dRlep, 120, 0., 6.,
                        false, pass_dR_cut, extra_weight);
    }
    DMesonRec::fill_D_star_pi0_histograms(this, charged_channel, D_index,
                                          pass_dR_cut, extra_weight);

    // D meson truth match
    if (m_is_mc && m_truth_matchD) {
      auto truth_channel = charged_channel;
      auto truth_channel_fid = std::vector<std::string>(m_nSyst, channel);

      for (unsigned int j = 0; j < m_nSyst; j++) {
        if (!m_do_lep_presel ||
            (event_pass_sys(j) && m_sys_lep_charge.at(j) != 0 &&
             pass_dR_cut.at(j))) {
          truth_channel.at(j) += "_" + truth_category;
          multiplicity_counter.at(j)[truth_channel.at(j)]++;
          used_channels.insert(truth_channel.at(j));
        }
      }

      if (m_do_lep_presel) {
        fill_lepton_histograms(truth_channel, pass_dR_cut, extra_weight);
      }
      if (!m_fit_variables_only) {
        add_fill_hist_sys("truth_Dmeson_pt", truth_channel, m_truth_D_pt, 150,
                          0, 150., false, pass_dR_cut, extra_weight);
        add_fill_hist_sys("truth_Dmeson_eta", truth_channel, m_truth_D_eta, 100,
                          -3.0, 3.0, false, pass_dR_cut, extra_weight);
        add_fill_hist_sys("truth_Dmeson_mass", truth_channel,
                          m_truth_D_mass * Charm::GeV, 200, 1.4, 2.4, false,
                          pass_dR_cut, extra_weight);
        add_fill_hist_sys("truth_Dmeson_lep_eta", truth_channel,
                          m_truth_abs_lep_eta, 25, 0, 2.5, false, pass_dR_cut,
                          extra_weight);
        // add_fill_hist_sys("Dmeson_dRlep", truth_channel, sys_dRlep, 120,
        // 0., 6., false, pass_dR_cut, extra_weight);
        // add_fill_hist_sys("Dmeson_dRbjet", truth_channel, sys_dRbjet, 120,
        // 0., 6., false, pass_dR_cut, extra_weight);
      }
      DMesonRec::fill_D_star_pi0_histograms(this, truth_channel, D_index,
                                            pass_dR_cut, extra_weight);
      // Add truth matched hist if requested
      if (m_truth_D && truth_category == "Matched" && m_is_mc) {
        int truthIndex =
            TruthInfo::getMatchedBarcode(m_DMesons_truthBarcode->at(D_index));
        std::string truth_channel;
        // accounting for presence of neutrals in truth D*pi0 decay mode (RM =
        // Right Mode)
        if (fabs(truth_daughter_mass(truthIndex) - DSTAR_MASS) < 40)
          truth_channel = channel + "_recoMatch_truth_DstarKPiPi0_RM";

        else
          truth_channel = channel + "_DstarKPiPi0_recoMatch_truth";

        TruthInfo::fill_truth_D_histograms(this, truth_channel, truthIndex);
      }
    }
  }

  // number of D meson candidates
  for (auto channel : used_channels) {
    std::vector<float> multiplicity;
    for (unsigned int j = 0; j < m_nSyst; j++) {
      multiplicity.push_back(multiplicity_counter.at(j)[channel]);
    }
    if (!m_fit_variables_only) {
      add_fill_hist_sys("N_DstarKPiPi0", channel, multiplicity, 20, -0.5, 19.5,
                        true);
    }
  }

  // pure D meson truth
  if (m_is_mc && m_truth_D) {
    for (unsigned int i = 0; i < m_truth_D_star_pi0_mesons.size(); i++) {
      unsigned int index = m_truth_D_star_pi0_mesons.at(i);
      std::string truth_channel;
      if (fabs(truth_daughter_mass(index) - DSTAR_MASS) < 40) {
        truth_channel = channel + "_truth_DstarKPiPi0_RM";
        TruthInfo::fill_truth_D_histograms(this, truth_channel, index);
      }
      truth_channel = channel + "_truth_DstarKPiPi0";
      TruthInfo::fill_truth_D_histograms(this, truth_channel, index);
    }
  }
}

}  // namespace Charm
