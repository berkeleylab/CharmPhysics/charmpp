#include "CharmLoopBase.h"

#include <math.h>

#include <iostream>

#include "HelperFunctions.h"
#include "TVector2.h"

namespace Charm {
void CharmLoopBase::initialize() {
  std::cout << "CharmLoopBase::initiaize" << std::endl;

  // set config
  configure();

  // systematics
  if (m_do_systematics) {
    systConfigure();
  }
}

int CharmLoopBase::execute() {
  throw std::runtime_error(
      "execute() not implemented for base class CharmLoopBase");
  return 1;
}

CharmLoopBase::CharmLoopBase(TString input_file, TString out_path,
                             TString tree_name)
    : EventLoopBase(input_file, out_path, tree_name),
      DMesonRec(),
      TruthInfo(),
      m_eta_bins_el{0, 0.70, 1.37, 1.52, 2.01, 2.47},
      m_eta_bins_mu{0, 1.10, 2.01, 2.5},
      m_met_bins_lep{0, 15.0, 30.0, 45.0, 60.0, 200.0} {
  add_channel("inclusive", {"all", "one_loose_lepton", "one_tight_lepton",
                            "trigger", "trigger_match"});
}

}  // namespace Charm
