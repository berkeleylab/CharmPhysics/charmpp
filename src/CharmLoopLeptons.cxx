#include <math.h>

#include <iostream>

#include "CharmLoopBase.h"
#include "HelperFunctions.h"
#include "TVector2.h"

namespace Charm {

float CharmLoopBase::get_mu_trigger_weight(unsigned int sys_index,
                                           unsigned int i) {
  if (m_EventInfo_runNumber < 290000) {
    float effMC1 = m_sys_mu_trig_2015_eff_mc.at(sys_index)->at(i);
    float effData1 = m_sys_mu_trig_2015_eff_data.at(sys_index)->at(i);

    float PMC = effMC1;
    float PData = effData1;

    if (PMC > 0) {
      return PData / PMC;
    } else {
      return 0;
    }
  } else {
    float effMC1 = m_sys_mu_trig_2018_eff_mc.at(sys_index)->at(i);
    float effData1 = m_sys_mu_trig_2018_eff_data.at(sys_index)->at(i);

    float PMC = effMC1;
    float PData = effData1;

    if (PMC > 0) {
      return PData / PMC;
    } else {
      return 0;
    }
  }
}

void CharmLoopBase::get_tight_electrons() {
  m_electrons_sys.clear();

  for (unsigned int j = 0; j < m_nSyst; j++) {
    m_electrons_sys.push_back({});
    for (unsigned int i = 0; i < m_AnalysisElectrons_pt_NOSYS->size(); i++) {
      // nominal
      if ((m_sys_el_pt.at(j)->at(i) * Charm::GeV >= 30.) &&
          (m_sys_el_selected.at(j)->at(i)) && (m_sys_el_iso.at(j)->at(i)) &&
          (m_AnalysisElectrons_likelihood_Tight->at(i)) &&
          ((m_is_mc && m_truth_match && el_is_prompt(i)) ||
           (m_is_mc && !m_truth_match) || (!m_is_mc))) {
        float sf = 1.;
        if (m_is_mc) {
          sf *= m_sys_el_reco_eff.at(j)->at(i) * m_sys_el_id_eff.at(j)->at(i) *
                m_sys_el_iso_eff.at(j)->at(i) * m_sys_el_trig_sf.at(j)->at(i) *
                m_sys_el_charge_sf.at(j)->at(i);
        }

        std::pair<unsigned int, float> pair(i, sf);
        m_electrons_sys.at(j).push_back(pair);
      }
    }
  }
}

void CharmLoopBase::get_tight_muons() {
  m_muons_sys.clear();
  // calibration sys
  for (unsigned int j = 0; j < m_nSyst; j++) {
    m_muons_sys.push_back({});
    for (unsigned int i = 0; i < m_AnalysisMuons_pt_NOSYS->size(); i++) {
      // nominal
      if ((m_sys_mu_pt.at(j)->at(i) * Charm::GeV >= 30.) &&
          (m_sys_mu_selected.at(j)->at(i)) && (m_sys_mu_quality.at(j)->at(i)) &&
          (m_sys_mu_iso.at(j)->at(i)) &&
          ((m_is_mc && m_truth_match && mu_is_prompt(i)) ||
           (m_is_mc && !m_truth_match) || (!m_is_mc))) {
        float sf = 1.;
        if (m_is_mc) {
          sf *= m_sys_mu_quality_eff.at(j)->at(i) *
                m_sys_mu_ttva_eff.at(j)->at(i) * m_sys_mu_iso_eff.at(j)->at(i) *
                get_mu_trigger_weight(j, i);
        }

        std::pair<unsigned int, float> pair(i, sf);
        m_muons_sys.at(j).push_back(pair);
      }
    }
  }
}

void CharmLoopBase::get_loose_electrons() {
  m_loose_electrons_sys.clear();

  for (unsigned int j = 0; j < m_nSyst; j++) {
    m_loose_electrons_sys.push_back({});
    for (unsigned int i = 0; i < m_AnalysisElectrons_pt_NOSYS->size(); i++) {
      // nominal
      if ((m_sys_el_pt.at(j)->at(i) * Charm::GeV >= 30.) &&
          (m_sys_el_selected.at(j)->at(i)) &&
          (m_AnalysisElectrons_likelihood_Tight->at(i)) &&
          ((m_is_mc && m_truth_match && el_is_prompt(i)) ||
           (m_is_mc && !m_truth_match) || (!m_is_mc))) {
        // additional requirements for Anti-Tight
        if (!m_sys_el_iso.at(j)->at(i)) {
          if (m_AnalysisElectrons_ptvarcone20_TightTTVA_pt1000->at(i) == 0) {
            continue;
          }
        }
        float sf = 1.;
        if (m_is_mc) {
          sf *= m_sys_el_reco_eff.at(j)->at(i) * m_sys_el_id_eff.at(j)->at(i) *
                m_sys_el_trig_noiso_sf.at(j)->at(i);
        }

        std::pair<unsigned int, float> pair(i, sf);
        m_loose_electrons_sys.at(j).push_back(pair);
      }
    }
  }
}

void CharmLoopBase::get_loose_muons() {
  m_loose_muons_sys.clear();

  // calibration sys
  for (unsigned int j = 0; j < m_nSyst; j++) {
    m_loose_muons_sys.push_back({});
    for (unsigned int i = 0; i < m_AnalysisMuons_pt_NOSYS->size(); i++) {
      // nominal
      if ((m_sys_mu_pt.at(j)->at(i) * Charm::GeV >= 30.) &&
          (m_sys_mu_selected.at(j)->at(i)) && (m_sys_mu_quality.at(j)->at(i)) &&
          ((m_is_mc && m_truth_match && mu_is_prompt(i)) ||
           (m_is_mc && !m_truth_match) || (!m_is_mc))) {
        // additional requirements for Anti-Tight
        if (!m_sys_mu_iso.at(j)->at(i)) {
          if (((m_AnalysisMuons_ptvarcone30_TightTTVA_pt500->at(i) +
                0.4 * m_AnalysisMuons_neflowisol20->at(i)) /
               m_sys_mu_pt.at(j)->at(i)) < 0.12) {
            continue;
          }
        }
        float sf = 1.;
        if (m_is_mc) {
          sf *= m_sys_mu_quality_eff.at(j)->at(i) *
                m_sys_mu_ttva_eff.at(j)->at(i) * get_mu_trigger_weight(j, i);
        }
        std::pair<unsigned int, float> pair(i, sf);
        m_loose_muons_sys.at(j).push_back(pair);
      }
    }
  }
}

float CharmLoopBase::get_dr_lep_Dmeson(float lepton_eta, float lepton_phi) {
  float dR_min = 999;
  for (unsigned int j = 0; j < m_D_plus_indices.size(); j++) {
    float dR = DMesonRec::D_meson_lepton_dR(m_D_plus_indices.at(j), lepton_eta,
                                            lepton_phi);
    if (dR < dR_min) {
      dR_min = dR;
    }
  }
  for (unsigned int j = 0; j < m_D_star_indices.size(); j++) {
    float dR = DMesonRec::D_meson_lepton_dR(m_D_star_indices.at(j), lepton_eta,
                                            lepton_phi);
    if (dR < dR_min) {
      dR_min = dR;
    }
  }
  for (unsigned int j = 0; j < m_D_star_pi0_indices.size(); j++) {
    float dR = DMesonRec::D_meson_lepton_dR(m_D_star_pi0_indices.at(j),
                                            lepton_eta, lepton_phi);
    if (dR < dR_min) {
      dR_min = dR;
    }
  }
  for (unsigned int j = 0; j < m_D_s_indices.size(); j++) {
    float dR = DMesonRec::D_meson_lepton_dR(m_D_s_indices.at(j), lepton_eta,
                                            lepton_phi);
    if (dR < dR_min) {
      dR_min = dR;
    }
  }
  return dR_min;
}

bool CharmLoopBase::pass_el_trigger() {
  if (m_EventInfo_runNumber < 290000) {
    return m_EventInfo_trigPassed_HLT_e24_lhmedium_L1EM20VH ||
           m_EventInfo_trigPassed_HLT_e60_lhmedium ||
           m_EventInfo_trigPassed_HLT_e120_lhloose;
  } else {
    return m_EventInfo_trigPassed_HLT_e26_lhtight_nod0_ivarloose ||
           m_EventInfo_trigPassed_HLT_e60_lhmedium_nod0 ||
           m_EventInfo_trigPassed_HLT_e140_lhloose_nod0;
  }
}

bool CharmLoopBase::pass_mu_trigger() {
  if (m_EventInfo_runNumber < 290000) {
    return m_EventInfo_trigPassed_HLT_mu50 ||
           m_EventInfo_trigPassed_HLT_mu20_iloose_L1MU15;
  } else {
    return m_EventInfo_trigPassed_HLT_mu50 ||
           m_EventInfo_trigPassed_HLT_mu26_ivarmedium;
  }
}

bool CharmLoopBase::el_is_trigger_matched(unsigned int i, float el_pt) {
  if (m_EventInfo_runNumber < 290000) {
    return (m_EventInfo_trigPassed_HLT_e24_lhmedium_L1EM20VH &&
            m_AnalysisElectrons_matched_HLT_e24_lhmedium_L1EM20VH->at(i)) ||
           (m_EventInfo_trigPassed_HLT_e60_lhmedium &&
            m_AnalysisElectrons_matched_HLT_e60_lhmedium->at(i) &&
            el_pt > 65.0) ||
           (m_EventInfo_trigPassed_HLT_e120_lhloose &&
            m_AnalysisElectrons_matched_HLT_e120_lhloose->at(i) &&
            el_pt > 132.0);
  } else {
    return (m_EventInfo_trigPassed_HLT_e26_lhtight_nod0_ivarloose &&
            m_AnalysisElectrons_matched_HLT_e26_lhtight_nod0_ivarloose->at(
                i)) ||
           (m_EventInfo_trigPassed_HLT_e60_lhmedium_nod0 &&
            m_AnalysisElectrons_matched_HLT_e60_lhmedium_nod0->at(i) &&
            el_pt > 65.0) ||
           (m_EventInfo_trigPassed_HLT_e140_lhloose_nod0 &&
            m_AnalysisElectrons_matched_HLT_e140_lhloose_nod0->at(i) &&
            el_pt > 132.0);
  }
}

bool CharmLoopBase::mu_is_trigger_matched(unsigned int i, float mu_pt) {
  if (m_EventInfo_runNumber < 290000) {
    return ((m_EventInfo_trigPassed_HLT_mu50 &&
             m_AnalysisMuons_matched_HLT_mu50->at(i) && mu_pt > 55.0) ||
            (m_EventInfo_trigPassed_HLT_mu20_iloose_L1MU15 &&
             m_AnalysisMuons_matched_HLT_mu20_iloose_L1MU15->at(i)));
  } else {
    return ((m_EventInfo_trigPassed_HLT_mu50 &&
             m_AnalysisMuons_matched_HLT_mu50->at(i) && mu_pt > 55.0) ||
            (m_EventInfo_trigPassed_HLT_mu26_ivarmedium &&
             m_AnalysisMuons_matched_HLT_mu26_ivarmedium->at(i)));
  }
}

// see:
// https://gitlab.cern.ch/ATLAS-IFF/IFFTruthClassifier/-/blob/master/Root/IFFTruthClassifier.cxx
// --------------------------------------------------------------------------------------------------
bool CharmLoopBase::el_is_prompt(unsigned int i) {
  // prompt
  if (m_AnalysisElectrons_truthType->at(i) == 2) {
    return true;
  }

  // Adding these cases from ElectronEfficiencyHelpers
  if (m_AnalysisElectrons_firstEgMotherTruthType->at(i) == 2 &&
      fabs(m_AnalysisElectrons_firstEgMotherPdgId->at(i)) == 11) {
    return true;
  }

  // FSR photons from electrons
  if (m_AnalysisElectrons_truthType->at(i) == 15 &&
      m_AnalysisElectrons_truthOrigin->at(i) == 40) {
    return true;
  }
  if (m_AnalysisElectrons_truthType->at(i) == 4 &&
      Charm::is_in(m_AnalysisElectrons_truthOrigin->at(i), {5, 7}) &&
      m_AnalysisElectrons_firstEgMotherTruthType->at(i) == 15 &&
      m_AnalysisElectrons_firstEgMotherTruthOrigin->at(i) == 40) {
    return true;
  }

  // If we reach here then it is not a prompt electron
  return false;
}

bool CharmLoopBase::mu_is_prompt(unsigned int i) {
  return (
      (m_AnalysisMuons_truthType->at(i) == 6) &&
      Charm::is_in(m_AnalysisMuons_truthOrigin->at(i), {10, 12, 13, 14, 43}));
}

}  // namespace Charm
