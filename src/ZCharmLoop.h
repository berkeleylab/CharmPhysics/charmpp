#ifndef Z_LOOP_H
#define Z_LOOP_H

#include "EventLoopBase.h"
#include "HelperFunctions.h"

namespace Charm {
class ZCharmLoop : public EventLoopBase {
 public:
  ZCharmLoop(TString input_file, TString out_path,
             TString tree_name = "CharmAnalysis");

 protected:
  std::string get_period_string();

  virtual void connect_branches() override;

 private:
  virtual void initialize() override;

  virtual bool preselection() override;

  virtual int execute() override;

  // virtual void finalize() override;

  std::string m_channel_string;

 private:
  void fill_histograms();
  float calculate_ptv_weight();

 private:
  std::vector<std::pair<unsigned int, float>> m_electrons;
  std::vector<std::pair<unsigned int, float>> m_muons;
  std::vector<unsigned int> m_jets;

  std::vector<std::vector<std::pair<unsigned int, float>>> m_electrons_sys;
  std::vector<std::vector<std::pair<unsigned int, float>>> m_muons_sys;
  std::vector<std::vector<unsigned int>> m_jets_sys;

 private:
  std::set<std::string> m_created_channels;
  bool m_do_extra_histograms;
  bool m_split_by_period;
  bool m_do_pt_v_rw;
  bool m_truth_match;

  float m_met_phi;
  float m_met_dphi;
  float m_met_mt;
  float m_lep1_d0;
  float m_lep1_d0sig;
  float m_lep1_d0PV;
  float m_lep1_d0sigPV;
  float m_lep1_z0sinTheta;
  float m_lep1_charge;
  float m_lep1_topoetcone20;
  float m_lep1_topoetcone20_over_pt;
  float m_lep1_ptvarcone20_TightTTVA_pt1000;
  float m_lep1_ptvarcone30_TightTTVA_pt1000;
  float m_lep1_ptvarcone20_TightTTVA_pt1000_over_pt;
  float m_lep1_ptvarcone30_TightTTVA_pt1000_over_pt;
  float m_lep1_ptvarcone30_plus_neflowisol20;
  float m_lep1_EOverP;
  float m_lep2_d0;
  float m_lep2_d0sig;
  float m_lep2_d0PV;
  float m_lep2_d0sigPV;
  float m_lep2_z0sinTheta;
  float m_lep2_charge;
  float m_lep2_topoetcone20;
  float m_lep2_topoetcone20_over_pt;
  float m_lep2_ptvarcone20_TightTTVA_pt1000;
  float m_lep2_ptvarcone30_TightTTVA_pt1000;
  float m_lep2_ptvarcone20_TightTTVA_pt1000_over_pt;
  float m_lep2_ptvarcone30_TightTTVA_pt1000_over_pt;
  float m_lep2_ptvarcone30_plus_neflowisol20;
  float m_lep2_EOverP;
  float m_Z_eta;
  float m_Z_phi;
  float m_lep_dPhi;
  float m_lep_dR;
  float m_truth_v_m;
  float m_truth_v_pt;
  int m_n_jets;
  int m_n_bjets;

  // nominal
  TLorentzVector m_lep1;
  TLorentzVector m_lep2;
  TLorentzVector m_Zboson;

  // sys
  std::vector<TLorentzVector> m_lep1_sys;
  std::vector<TLorentzVector> m_lep2_sys;
  std::vector<TLorentzVector> m_Zboson_sys;

  // variables affected by calibration systematics
  std::vector<Float_t> m_sys_met;
  std::vector<Float_t> m_sys_Z_m;
  std::vector<Float_t> m_sys_Z_pt;
  std::vector<Float_t> m_sys_lep1_pt;
  std::vector<Float_t> m_sys_lep1_eta;
  std::vector<Float_t> m_sys_lep1_phi;
  std::vector<Float_t> m_sys_lep2_pt;
  std::vector<Float_t> m_sys_lep2_eta;
  std::vector<Float_t> m_sys_lep2_phi;
  std::vector<Float_t> m_sys_njets;
  std::vector<Float_t> m_sys_nbjets;

  // electron
  std::vector<std::vector<Float_t> *> m_sys_el_pt;
  std::vector<std::vector<bool> *> m_sys_el_iso;
  std::vector<std::vector<bool> *> m_sys_el_selected;
  std::vector<std::vector<bool> *> m_sys_el_selected_other;
  std::vector<std::vector<Float_t> *> m_sys_el_reco_eff;
  std::vector<std::vector<Float_t> *> m_sys_el_id_eff;
  std::vector<std::vector<Float_t> *> m_sys_el_iso_eff;
  std::vector<std::vector<Float_t> *> m_sys_el_trig_eff;
  std::vector<std::vector<Float_t> *> m_sys_el_trig_sf;

  // muon
  std::vector<std::vector<Float_t> *> m_sys_mu_pt;
  std::vector<std::vector<bool> *> m_sys_mu_iso;
  std::vector<std::vector<bool> *> m_sys_mu_quality;
  std::vector<std::vector<bool> *> m_sys_mu_selected;
  std::vector<std::vector<bool> *> m_sys_mu_selected_other;
  std::vector<std::vector<Float_t> *> m_sys_mu_quality_eff;
  std::vector<std::vector<Float_t> *> m_sys_mu_ttva_eff;
  std::vector<std::vector<Float_t> *> m_sys_mu_iso_eff;
  std::vector<std::vector<Float_t> *> m_sys_mu_trig_2015_eff_data;
  std::vector<std::vector<Float_t> *> m_sys_mu_trig_2015_eff_mc;
  std::vector<std::vector<Float_t> *> m_sys_mu_trig_2018_eff_data;
  std::vector<std::vector<Float_t> *> m_sys_mu_trig_2018_eff_mc;

  // jets
  std::vector<std::vector<Float_t> *> m_sys_jet_pt;
  std::vector<std::vector<bool> *> m_sys_jet_selected;

 private:
  // runNumber of RandomRunNumber
  TString get_run_number_string();
  void read_sys_br();
  void get_tight_electrons();
  void get_tight_muons();
  void get_jets();
  void jet_selection_for_sys(unsigned int i);
  bool pass_el_trigger();
  bool pass_mu_trigger();
  bool el_is_trigger_matched(unsigned int i, unsigned int j);
  bool mu_is_trigger_matched(unsigned int i, unsigned int j);
  bool el_is_prompt(unsigned int i);
  bool mu_is_prompt(unsigned int i);
  float get_el_trigger_weight(unsigned int sys_index, unsigned int i,
                              unsigned int j);
  float get_mu_trigger_weight(unsigned int sys_index, unsigned int i,
                              unsigned int j);

 private:
  // branches
  UInt_t m_EventInfo_runNumber{};
  Float_t m_EventInfo_correctedScaled_averageInteractionsPerCrossing{};
  Float_t m_CharmEventInfo_EventWeight{};
  Float_t m_EventInfo_generatorWeight_NOSYS{};
  Float_t m_EventInfo_PileupWeight_NOSYS{};
  Float_t m_EventInfo_jvt_effSF_NOSYS{};
  // Float_t m_EventInfo_fjvt_effSF_NOSYS{};

  bool m_EventInfo_trigPassed_HLT_e24_lhmedium_L1EM20VH{};
  bool m_EventInfo_trigPassed_HLT_e60_lhmedium{};
  bool m_EventInfo_trigPassed_HLT_e120_lhloose{};
  bool m_EventInfo_trigPassed_HLT_e26_lhtight_nod0_ivarloose{};
  bool m_EventInfo_trigPassed_HLT_e60_lhmedium_nod0{};
  bool m_EventInfo_trigPassed_HLT_e140_lhloose_nod0{};
  bool m_EventInfo_trigPassed_HLT_mu20_iloose_L1MU15{};
  bool m_EventInfo_trigPassed_HLT_mu26_ivarmedium{};
  bool m_EventInfo_trigPassed_HLT_mu50{};

  // muons
  std::vector<Float_t> *m_AnalysisMuons_pt_NOSYS{};
  std::vector<Float_t> *m_AnalysisMuons_eta{};
  std::vector<Float_t> *m_AnalysisMuons_phi{};
  std::vector<Float_t> *m_AnalysisMuons_charge{};
  std::vector<Float_t> *m_AnalysisMuons_mu_effSF_Quality_Tight_NOSYS{};
  std::vector<Float_t> *m_AnalysisMuons_mu_effSF_TTVA_NOSYS{};
  std::vector<Float_t> *m_AnalysisMuons_mu_effSF_Isol_PflowTight_VarRad_NOSYS{};
  std::vector<Float_t> *
      m_AnalysisMuons_mu_effMC_Trig_Tight_HLT_mu26_ivarmedium_OR_HLT_mu50_NOSYS{};
  std::vector<Float_t> *
      m_AnalysisMuons_mu_effData_Trig_Tight_HLT_mu26_ivarmedium_OR_HLT_mu50_NOSYS{};
  std::vector<Float_t> *
      m_AnalysisMuons_mu_effMC_Trig_Tight_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_NOSYS{};
  std::vector<Float_t> *
      m_AnalysisMuons_mu_effData_Trig_Tight_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_NOSYS{};
  std::vector<Int_t> *m_AnalysisMuons_truthType{};
  std::vector<Int_t> *m_AnalysisMuons_truthOrigin{};
  std::vector<bool> *m_AnalysisMuons_is_bad{};
  std::vector<bool> *m_AnalysisMuons_mu_selected_NOSYS{};
  std::vector<bool> *m_AnalysisMuons_isQuality_Tight_NOSYS{};
  std::vector<bool> *m_AnalysisMuons_isIsolated_FCLoose_NOSYS{};
  std::vector<bool> *m_AnalysisMuons_isIsolated_PflowTight_VarRad_NOSYS{};
  std::vector<bool> *m_AnalysisMuons_matched_HLT_mu20_iloose_L1MU15{};
  std::vector<bool> *m_AnalysisMuons_matched_HLT_mu26_ivarmedium{};
  std::vector<bool> *m_AnalysisMuons_matched_HLT_mu50{};

  // electrons
  std::vector<Float_t> *m_AnalysisElectrons_pt_NOSYS{};
  std::vector<Float_t> *m_AnalysisElectrons_eta{};
  std::vector<Float_t> *m_AnalysisElectrons_phi{};
  std::vector<Float_t> *m_AnalysisElectrons_charge{};
  std::vector<Float_t> *m_AnalysisElectrons_effSF_NOSYS{};
  std::vector<Float_t> *m_AnalysisElectrons_effSF_ID_Tight_NOSYS{};
  std::vector<Float_t>
      *m_AnalysisElectrons_effSF_Isol_Tight_Tight_VarRad_NOSYS{};
  std::vector<Float_t> *
      m_AnalysisElectrons_effSF_Trig_Tight_Tight_VarRad_Eff_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_NOSYS{};
  std::vector<Float_t> *
      m_AnalysisElectrons_effSF_Trig_Tight_Tight_VarRad_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_NOSYS{};
  std::vector<Int_t> *m_AnalysisElectrons_truthType{};
  std::vector<Int_t> *m_AnalysisElectrons_truthOrigin{};
  std::vector<Int_t> *m_AnalysisElectrons_firstEgMotherTruthType{};
  std::vector<Int_t> *m_AnalysisElectrons_firstEgMotherTruthOrigin{};
  std::vector<Int_t> *m_AnalysisElectrons_firstEgMotherPdgId{};
  std::vector<bool> *m_AnalysisElectrons_el_selected_NOSYS{};
  std::vector<bool> *m_AnalysisElectrons_likelihood_Medium{};
  std::vector<bool> *m_AnalysisElectrons_likelihood_Tight{};
  std::vector<bool> *m_AnalysisElectrons_isIsolated_FCHighPtCaloOnly_NOSYS{};
  std::vector<bool> *m_AnalysisElectrons_isIsolated_FCLoose_NOSYS{};
  std::vector<bool> *m_AnalysisElectrons_isIsolated_FCTight_NOSYS{};
  std::vector<bool> *m_AnalysisElectrons_isIsolated_Gradient_NOSYS{};
  std::vector<bool> *m_AnalysisElectrons_isIsolated_Tight_VarRad_NOSYS{};
  std::vector<bool> *m_AnalysisElectrons_matched_HLT_e24_lhmedium_L1EM20VH{};
  std::vector<bool> *m_AnalysisElectrons_matched_HLT_e60_lhmedium{};
  std::vector<bool> *m_AnalysisElectrons_matched_HLT_e120_lhloose{};
  std::vector<bool>
      *m_AnalysisElectrons_matched_HLT_e26_lhtight_nod0_ivarloose{};
  std::vector<bool> *m_AnalysisElectrons_matched_HLT_e60_lhmedium_nod0{};
  std::vector<bool> *m_AnalysisElectrons_matched_HLT_e140_lhloose_nod0{};

  // MET
  Float_t m_METInfo_MET_NOSYS{};
  Float_t m_METInfo_NOSYS_METPhi{};
  Float_t m_CharmEventInfo_PV_Z{};
  Float_t m_CharmEventInfo_beamPosX{};
  Float_t m_CharmEventInfo_PV_X{};
  Float_t m_CharmEventInfo_beamPosY{};
  Float_t m_CharmEventInfo_PV_Y{};

  // extra stuff
  std::vector<float> *m_AnalysisElectrons_d0{};
  std::vector<float> *m_AnalysisElectrons_d0sig{};
  std::vector<float> *m_AnalysisElectrons_z0sinTheta{};
  std::vector<float> *m_AnalysisElectrons_topoetcone20{};
  std::vector<float> *m_AnalysisElectrons_ptvarcone20_TightTTVA_pt1000{};
  std::vector<float> *m_AnalysisElectrons_ptvarcone30_TightTTVA_pt1000{};
  std::vector<float> *m_AnalysisElectrons_EOverP{};
  std::vector<float> *m_AnalysisMuons_d0{};
  std::vector<float> *m_AnalysisMuons_d0sig{};
  std::vector<float> *m_AnalysisMuons_z0sinTheta{};
  std::vector<float> *m_AnalysisMuons_topoetcone20{};
  std::vector<float> *m_AnalysisMuons_ptvarcone20_TightTTVA_pt1000{};
  std::vector<float> *m_AnalysisMuons_ptvarcone30_TightTTVA_pt1000{};
  std::vector<float> *m_AnalysisMuons_ptvarcone30_TightTTVA_pt500{};
  std::vector<float> *m_AnalysisMuons_neflowisol20{};
  float m_EventInfo_ftag_effSF_MV2c10_FixedCutBEff_70_NOSYS{};

  // truth
  std::vector<float> *m_TruthLeptons_pt{};
  std::vector<float> *m_TruthLeptons_m{};
  std::vector<float> *m_TruthLeptons_eta{};
  std::vector<float> *m_TruthLeptons_phi{};
  std::vector<int> *m_TruthLeptons_barcode{};
  std::vector<int> *m_TruthLeptons_pdgId{};

  // jets
  std::vector<float> *m_AnalysisJets_pt_NOSYS{};
  std::vector<float> *m_AnalysisJets_ftag_select_DL1r_FixedCutBEff_70{};
  std::vector<float> *m_AnalysisJets_eta{};
  std::vector<float> *m_AnalysisJets_phi{};
  std::vector<float> *m_AnalysisJets_m{};
  std::vector<bool> *m_AnalysisJets_jet_selected_NOSYS{};
  // float m_AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_NOSYS{};//odd

  // sys branches
  sys_br<Float_t> m_sys_br_GEN{};
  sys_br<Float_t> m_sys_br_PU{};
  sys_br<Float_t> m_sys_br_JVT{};
  sys_br<Float_t> m_sys_br_FTAG{};
  sys_br<Float_t> m_sys_br_MET_met{};

  // electron sys branches
  sys_br<std::vector<Float_t> *> m_sys_br_el_pt{};
  sys_br<std::vector<Float_t> *> m_sys_br_el_reco_eff{};
  sys_br<std::vector<Float_t> *> m_sys_br_el_id_eff{};
  sys_br<std::vector<Float_t> *> m_sys_br_el_iso_eff{};
  sys_br<std::vector<Float_t> *> m_sys_br_el_trig_eff{};
  sys_br<std::vector<Float_t> *> m_sys_br_el_trig_sf{};
  sys_br<std::vector<bool> *> m_sys_br_el_selected{};
  sys_br<std::vector<bool> *> m_sys_br_el_selected_other_sys{};
  sys_br<std::vector<bool> *> m_sys_br_el_isIsolated_FCTight{};
  sys_br<std::vector<Float_t> *> m_sys_br_el_calib_reco_eff{};
  sys_br<std::vector<Float_t> *> m_sys_br_el_calib_id_eff{};
  sys_br<std::vector<Float_t> *> m_sys_br_el_calib_iso_eff{};
  sys_br<std::vector<Float_t> *> m_sys_br_el_calib_trig_eff{};
  sys_br<std::vector<Float_t> *> m_sys_br_el_calib_trig_sf{};

  // muon sys branches
  sys_br<std::vector<Float_t> *> m_sys_br_mu_pt{};
  sys_br<std::vector<Float_t> *> m_sys_br_mu_quality_eff{};
  sys_br<std::vector<Float_t> *> m_sys_br_mu_ttva_eff{};
  sys_br<std::vector<Float_t> *> m_sys_br_mu_iso_eff{};
  sys_br<std::vector<Float_t> *> m_sys_br_mu_trig_2015_eff_data{};
  sys_br<std::vector<Float_t> *> m_sys_br_mu_trig_2015_eff_mc{};
  sys_br<std::vector<Float_t> *> m_sys_br_mu_trig_2018_eff_data{};
  sys_br<std::vector<Float_t> *> m_sys_br_mu_trig_2018_eff_mc{};
  sys_br<std::vector<bool> *> m_sys_br_mu_selected{};
  sys_br<std::vector<bool> *> m_sys_br_mu_selected_other_sys{};
  sys_br<std::vector<bool> *> m_sys_br_mu_isIsolated_PflowTight_VarRad{};
  sys_br<std::vector<bool> *> m_sys_br_mu_isQuality_Tight{};
  sys_br<std::vector<Float_t> *> m_sys_br_mu_calib_quality_eff{};
  sys_br<std::vector<Float_t> *> m_sys_br_mu_calib_ttva_eff{};
  sys_br<std::vector<Float_t> *> m_sys_br_mu_calib_iso_eff{};
  sys_br<std::vector<Float_t> *> m_sys_br_mu_calib_trig_2015_eff_data{};
  sys_br<std::vector<Float_t> *> m_sys_br_mu_calib_trig_2015_eff_mc{};
  sys_br<std::vector<Float_t> *> m_sys_br_mu_calib_trig_2018_eff_data{};
  sys_br<std::vector<Float_t> *> m_sys_br_mu_calib_trig_2018_eff_mc{};

  // jet sys branches
  sys_br<std::vector<Float_t> *> m_sys_br_jet_pt{};
  sys_br<std::vector<bool> *> m_sys_br_jet_selected{};
  sys_br<std::vector<bool> *> m_sys_br_jet_selected_other_sys{};
};

}  // namespace Charm

#endif  // Z_LOOP_H
