#include "WFakeRateMeasurement.h"

#include <math.h>

#include <iostream>

namespace Charm {
int WFakeRateMeasurement::execute() {
  // lepton Event Selection
  for (unsigned int i = 0; i < m_nSyst; i++) {
    // Exclude low lepton pT
    if (m_sys_lep_pt.at(i) < 30) {
      m_channel_sys.at(i) = "";
      continue;
    }
    // mT control & met control
    else if (m_sys_met_mt.at(i) < 40 && m_sys_met.at(i) < 30) {
      if (m_less_closure_region) continue;
      m_channel_sys.at(i) += "_QCD_mtCtrl_metCtrl";
      // m_channel_sys.at(i) += "_QCD";
    }
    // mT validation & met control
    else if (m_sys_met_mt.at(i) >= 40 && m_sys_met_mt.at(i) < 60 &&
             m_sys_met.at(i) < 30) {
      if (m_less_closure_region) continue;
      m_channel_sys.at(i) += "_QCD_mtVal_metCtrl";
    }
    // mT signal & met control
    else if (m_sys_met_mt.at(i) >= 60 && m_sys_met.at(i) < 30) {
      if (m_less_closure_region) continue;
      m_channel_sys.at(i) += "_QCD_mtSig_metCtrl";
    }
    // mT control & met signal
    else if (m_sys_met_mt.at(i) < 40 && m_sys_met.at(i) >= 30) {
      m_channel_sys.at(i) += "_QCD_mtCtrl_metSig";
    }
    // mT validation & met signal
    else if (m_sys_met_mt.at(i) >= 40 && m_sys_met_mt.at(i) < 60 &&
             m_sys_met.at(i) >= 30) {
      m_channel_sys.at(i) += "_QCD_mtVal_metSig";
    }
    // mT signal & met signal
    else if (m_sys_met_mt.at(i) >= 60 && m_sys_met.at(i) >= 30) {
      m_channel_sys.at(i) += "_QCD_mtSig_metSig";
    } else {
      // m_channel_sys.at(i) = "";
      // continue;
      throw std::runtime_error("Bad MET/mT category");
    }
  }

  // D+ reconstruction (do after W cuts to save CPU)
  if (m_D_plus_meson) {
    get_D_plus_mesons(this, false);
    if (m_is_mc && m_truth_D) {
      get_truth_D_mesons(411);
    }
  }

  // D*+ reconstruction (do after W cuts to save CPU)
  if (m_D_star_meson) {
    get_D_star_mesons(this, false);
    if (m_is_mc && m_truth_D) {
      get_truth_D_mesons(413);
    }
  }

  if (m_D_star_pi0_meson) {
    get_D_star_pi0_mesons(this);
    if (m_is_mc && m_truth_D) {
      get_truth_D_mesons(413);
    }
  }

  // Ds reconstruction (do after W cuts to save CPU)
  if (m_D_s_meson) {
    get_D_s_mesons(this);
    if (m_is_mc && m_truth_D) {
      get_truth_D_mesons(431);
    }
  }

  // production fraction reweighting
  // must do this here becaue truth branches are not available in preselection
  if (m_is_mc && m_do_prod_fraction_rw_old) {
    reweight_production_fractions(this);
  }

  // reweight wjets samples
  if (m_is_mc && m_do_wjets_rw) {
    reweight_wjets(this);
  }

  // jet selection
  if (m_jet_selection) {
    get_jets();
    jet_selection_for_sys();
  }

  // fill histograms
  fill_histograms_with_matrix_method();

  return 1;
}

void WFakeRateMeasurement::connect_branches() {
  // connect lepton branches
  CharmLoopBase::connect_branches();

  // additional branches for D meson reco
  if (m_D_plus_meson || m_D_star_meson || m_D_star_pi0_meson || m_D_s_meson) {
    connect_D_meson_branches(this);
  }

  // truth branches for D
  if (m_is_mc && m_truth_matchD) {
    connect_truth_D_meson_branches(this);
  }
}

WFakeRateMeasurement::WFakeRateMeasurement(TString input_file, TString out_path,
                                           TString tree_name)
    : CharmLoopBase(input_file, out_path, tree_name) {}

}  // namespace Charm
