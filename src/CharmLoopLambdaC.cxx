#include <math.h>

#include <iostream>

#include "CharmLoopBase.h"
#include "HelperFunctions.h"
#include "TVector2.h"

namespace Charm {

void CharmLoopBase::fill_Lambda_C_histograms(std::string channel) {

  // loop over D plus mesons
  for (unsigned int i = 0; i < m_Lambda_C_indices.size(); i++) {
    // retreive the D meson's index
    const auto D_index = m_Lambda_C_indices.at(i);

    // truth category
    bool matched_signal = false;
    std::string truth_category = "";
    int truthIndex = -1;
    if (m_truth_matchD && m_is_mc) {
      truth_category = TruthInfo::get_truth_category(
          this, D_index, "pKpi", m_fiducial_selection, false,
          m_simplified_dmeson_truth);
      truthIndex =
          TruthInfo::getMatchedBarcode(m_DMesons_truthBarcode->at(D_index));
    }
    if (truthIndex >= 0) {
      m_truth_D_pt = m_TruthParticles_Selected_pt->at(truthIndex) * Charm::GeV;
      m_truth_D_eta = m_TruthParticles_Selected_eta->at(truthIndex);
      m_truth_D_mass = truth_daughter_mass(truthIndex);
      m_truth_abs_lep_eta = std::abs(m_truth_lep1.Eta());
      if (truth_category == "Matched") {
        matched_signal = true;
      }
    } else {
      m_truth_D_pt = 0;
      m_truth_D_eta = -999;
      m_truth_D_mass = 0;
      m_truth_abs_lep_eta = -999;
    }

    // dR w.r.t. the lepton depends on systematic
    auto sys_dRlep = std::vector<float>(m_nSyst, 0.);

    // D meson mass
    m_sys_dmeson_mass = std::vector<float>(m_nSyst, m_DMesons_m->at(D_index));

    // OS or SS region
    auto charged_channel = std::vector<std::string>(m_nSyst, channel);
    auto pass_cuts = std::vector<bool>(m_nSyst, false);
    auto extra_weight = std::vector<double>(m_nSyst, 1.0);

    for (unsigned int j = 0; j < m_nSyst; j++) {
      if (!m_do_lep_presel || (event_pass_sys(j) && m_sys_lep_charge.at(j) != 0)) {

        // dR w.r.t. the lepton
        float dRlep = 999;
        float lep_charge = 0;
        if (m_do_lep_presel) {
          dRlep = DMesonRec::D_meson_lepton_dR(D_index, m_sys_lep_eta.at(j),
                                               m_sys_lep_phi.at(j));
          lep_charge = m_sys_lep_charge.at(j);
        }
        sys_dRlep.at(j) = dRlep;

        // dR cut
        if (dRlep < m_cut_drLep_max) {
          continue;
        }

        // sys string
        std::string sys_string = "";
        if (j > 0) {
          sys_string = m_systematics.at(j - 1);
        }

        // Charge: OS or SS
        pass_cuts.at(j) = true;

        // per D-meson b-jet channel
        std::pair<std::string, double> bjet_channel =
            jet_selection_for_sys(j, D_index);

        // b-jet channel
        charged_channel.at(j) += bjet_channel.first + "_LambdaC" +
                                 get_charge_prefix(lep_charge, D_index);

        // b-jet scale factor
        extra_weight.at(j) *= bjet_channel.second;
      }
    }

    // lepton histograms for this channel
    if (m_do_lep_presel) {
      fill_lepton_histograms(charged_channel, pass_cuts, extra_weight);
    }

    if (m_save_output_tree && !m_is_mc) {
      DMesonRec::fill_D_plus_tree(D_index, m_extended_output_branches);
      fill_write_tree(charged_channel, pass_cuts, extra_weight);
    }

    // D meson truth match
    if (m_is_mc && m_truth_matchD) {
      auto truth_channel = charged_channel;
      auto truth_channel_fid = std::vector<std::string>(m_nSyst, channel);

      for (unsigned int j = 0; j < m_nSyst; j++) {
        // don't save AntiTight truth categories
        if (m_do_lep_presel && m_sys_anti_tight.at(j)) {
          pass_cuts.at(j) = false;
        }

        if (!m_do_lep_presel ||
            (event_pass_sys(j) && m_sys_lep_charge.at(j) != 0 &&
             pass_cuts.at(j))) {
          truth_channel.at(j) += "_" + truth_category;
        }
      }

      if (m_do_lep_presel) {
        fill_lepton_histograms(truth_channel, pass_cuts, extra_weight);
      }

      if (m_save_output_tree && m_is_mc) {
        DMesonRec::fill_D_plus_tree(D_index, m_extended_output_branches, true);
        fill_truth_D_branches(truthIndex);
        fill_write_tree(truth_channel, pass_cuts, extra_weight);
      }
    }
  }
}

}  // namespace Charm
