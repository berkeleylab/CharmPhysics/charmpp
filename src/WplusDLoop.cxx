#include "WplusDLoop.h"

#include <math.h>

#include <iostream>

namespace Charm {
int WplusDLoop::execute() {
  // lepton Event Selection
  if (m_do_lep_presel) {
    for (unsigned int i = 0; i < m_nSyst; i++) {
      // check if failed preselection
      if (m_channel_sys.at(i) == "") {
        continue;
      }
      if (!m_loose_w_selection_only) {
        if (m_sys_met.at(i) < 30 || m_sys_met_mt.at(i) < 60 ||
            m_sys_lep_pt.at(i) < 30) {
          if (!m_loose_w_selection) {
            m_channel_sys.at(i) = "";
            continue;
          } else {
            m_channel_sys.at(i) += "_Anti";
          }
        }
      }
      m_channel_sys.at(i) += "_SR";
    }
  }

  // D+ reconstruction (do after W cuts to save CPU)
  if (m_D_plus_meson) {
    get_D_plus_mesons(this, false, m_no_d_meson_selection, m_frag_selection);
    if (m_is_mc && m_truth_D) {
      get_truth_D_mesons(411);
    }
  }

  // D*+ reconstruction (do after W cuts to save CPU)
  if (m_D_star_meson) {
    get_D_star_mesons(this, false, m_no_d_meson_selection, m_frag_selection);
    if (m_is_mc && m_truth_D) {
      get_truth_D_mesons(413);
    }
  }

  if (m_D_star_pi0_meson) {
    get_D_star_pi0_mesons(this);
    if (m_is_mc && m_truth_D) {
      get_truth_D_mesons(413);
    }
  }

  // Ds reconstruction (do after W cuts to save CPU)
  if (m_D_s_meson) {
    get_D_s_mesons(this);
    if (m_is_mc && m_truth_D) {
      get_truth_D_mesons(431);
    }
  }

  // LambdaC
  if (m_Lambda_C_baryon) {
    get_Lambda_C_baryons(this);
    if (m_is_mc && m_truth_D) {
      get_truth_D_mesons(4122);
    }
  }

  // production fraction reweighting
  // must do this here becaue truth branches are not available in preselection
  if (m_is_mc && m_do_prod_fraction_rw_old) {
    reweight_production_fractions(this);
  }

  // reweight wjets samples
  if (m_is_mc && m_do_wjets_rw) {
    reweight_wjets(this);
  }

  if (m_track_jet_selection) {
    get_track_jets();
  }

  // jet selection
  if (m_jet_selection || m_dmeson_jet_matching) {
    get_jets();
  }

  // fill histograms
  fill_histograms_with_matrix_method();

  return 1;
}

void WplusDLoop::connect_branches() {
  // connect lepton branches
  CharmLoopBase::connect_branches();

  // output tree branches
  if (m_save_output_tree) {
    CharmLoopBase::connect_output_tree_branches();
  }

  // additional branches for D meson reco
  if (m_D_plus_meson || m_D_star_meson || m_D_star_pi0_meson || m_D_s_meson || m_Lambda_C_baryon) {
    connect_D_meson_branches(this);
  }

  // truth branches for D
  if (m_is_mc && (m_truth_matchD || m_truth_D || m_do_Nminus1 ||
                  m_fiducial_selection || m_save_truth_wjets)) {
    connect_truth_D_meson_branches(this,
                                   (m_fiducial_selection || m_save_truth_wjets),
                                   m_track_jet_selection);
  }
}

WplusDLoop::WplusDLoop(TString input_file, TString out_path, TString tree_name)
    : CharmLoopBase(input_file, out_path, tree_name) {}

}  // namespace Charm
