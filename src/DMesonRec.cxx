#include "DMesonRec.h"

#include <iostream>

#include "CharmLoopBase.h"
#include "TMath.h"
#include "TVector2.h"

namespace Charm {

void DMesonRec::get_D_plus_mesons(EventLoopBase *elb, bool loose_selection,
                                  bool no_selection, bool frag_selection) {
  get_D_presel(elb, DecayType::Dplus);

  m_D_plus_indices.clear();
  for (unsigned int index = 0; index < m_presel_Dplus.size(); index++) {
    unsigned int i = m_presel_Dplus.at(index);
    int failed_cut = 0;

    // D+ meson eta
    if (fabs(m_DMesons_eta->at(i)) > 2.2) {
      continue;
    }

    // D+ meson pt
    float pt_cut = 8.0;
    if (no_selection) {
      pt_cut = 5.0;
    }
    if (m_DMesons_pt->at(i) * Charm::GeV < pt_cut ||
        m_DMesons_pt->at(i) * Charm::GeV > 150.0) {
      continue;
    }

    // invariant mass cut
    if (m_DMesons_m->at(i) * Charm::GeV <= 1.7 ||
        m_DMesons_m->at(i) * Charm::GeV > 2.2) {
      continue;
    }

    // skip D+ selection entirely
    if (!no_selection) {
      // SV chi2 requirement
      if (m_DMesons_fitOutput__Chi2->at(i) > 8.) {
        continue;
      }

      // daughter tracks pt
      if (D_meson_min_track_pt(i) * Charm::MeV < 800.) {
        continue;
      }

      // optimized D+ selection for fragmentation fucntion measurement
      // versus the standard D+ in the W+D paper
      if (frag_selection) {

        // impact parameter
        float d0_cut = 0.04; // 40 microns
        if (loose_selection) {
          d0_cut = 10.0;
        }
        if (fabs(m_DMesons_fitOutput__Impact->at(i)) * Charm::mm > d0_cut) {
          continue;
        }

        // daughter dR separation
        if (D_meson_max_track_dR(i) >
            9.81961 / (m_DMesons_pt->at(i) * Charm::GeV) -
                40.0521 / (m_DMesons_pt->at(i) * Charm::GeV *
                           m_DMesons_pt->at(i) * Charm::GeV)) {
          continue;
        }

        // min Lxy requirement
        if (m_DMesons_fitOutput__Lxy->at(i) * Charm::mm <
            1.1 + 0.028 * m_DMesons_pt->at(i) * Charm::GeV) {
          continue;
        }
      } else {

        // impact parameter
        float d0_cut = 0.5; // 500 microns (W+D paper)
        if (loose_selection) {
          d0_cut = 10.0;
        }
        if (fabs(m_DMesons_fitOutput__Impact->at(i)) * Charm::mm > d0_cut) {
          continue;
        }

        // daughter dR separation
        if (D_meson_max_track_dR(i) > 0.6) {
          continue;
        }

        // min Lxy requirement
        if (m_DMesons_pt->at(i) * Charm::GeV < 40.0) {
          if (m_DMesons_fitOutput__Lxy->at(i) * Charm::mm < 1.1) {
            continue;
          }
        } else {
          if (m_DMesons_fitOutput__Lxy->at(i) * Charm::mm < 2.5) {
            continue;
          }
        }
      }

      // combinatorial background rejection
      if (m_DMesons_costhetastar->at(i) < -0.8) {
        continue;
      }

      // D*+ rejection
      if ((std::min(m_DMesons_m->at(i) - m_DMesons_mKpi1->at(i),
                    m_DMesons_m->at(i) - m_DMesons_mKpi2->at(i))) *
              Charm::MeV <
          160) {
        continue;
      }

      // Ds rejection
      if ((std::min(m_DMesons_mPhi1->at(i), m_DMesons_mPhi2->at(i))) *
              Charm::MeV <
          8.0) {
        continue;
      }

      // Isolation of Dplus (NEW CUT wrt Run 1)
      // No cut when measuring fake rates to increase statistics
      if (m_dplus_isolation) {
        if (!loose_selection) {
          if (m_DMesons_ptcone40->at(i) / m_DMesons_pt->at(i) > 1.) {
            continue;
          }
        }
      }

      // D+ 3D Impact Significance Cut
      // No cut when measuring fake rates to increase statistics
      if (!loose_selection) {
        if (fabs(m_DMesons_fitOutput__ImpactSignificance->at(i)) > 4.0) {
          continue;
        }
      }
    }

    // save D+ candidate index
    m_D_plus_indices.push_back(i);
  }
}

void DMesonRec::get_D_star_mesons(EventLoopBase *elb, bool loose_selection,
                                  bool no_selection, bool frag_selection) {
  get_D_presel(elb, DecayType::Dstar);
  int D0Index;
  m_D_star_indices.clear();

  for (unsigned int index = 0; index < m_presel_Dstar.size(); index++) {
    unsigned int i = m_presel_Dstar.at(index);
    int failed_cut = 0;
    D0Index = m_DMesons_D0Index->at(i);

    // D*+ meson eta
    if (fabs(m_DMesons_eta->at(i)) > 2.2) {
      continue;
    }

    // D*+ meson pt
    float pt_cut = 8.0;
    if (no_selection) {
      pt_cut = 5.0;
    }
    if (m_DMesons_pt->at(i) * Charm::GeV < pt_cut ||
        m_DMesons_pt->at(i) * Charm::GeV > 150.0) {
      continue;
    }

    // D* invariant mass cut
    if (m_DMesons_m->at(i) <= 1700. || m_DMesons_m->at(i) > 2200.) {
      continue;
    }

    // D*-D0 invariant mass cut
    if (m_DMesons_DeltaMass->at(i) <= 140. ||
        m_DMesons_DeltaMass->at(i) > 180.) {
      continue;
    }

    // skip D* selection entirely
    if (!no_selection) {
      // SV chi2 requirement
      if (m_DMesons_fitOutput__Chi2->at(i) > 10.) {
        continue;
      }

      // daughter tracks pt
      if (D_meson_min_track_pt(D0Index) * Charm::MeV < 600.) {
        continue;
      }

      // daughter dR separation
      if (D_meson_max_track_dR(D0Index) > 0.6) {
        continue;
      }

      if (!loose_selection) {
        // impact significance
        if (fabs(m_DMesons_fitOutput__ImpactSignificance->at(D0Index)) > 4.0) {
          continue;
        }
      }

      // min Lxy requirement
      float Lxy_cut = 0.0;
      if (frag_selection) {
        Lxy_cut = 0.4;
      }
      if (m_DMesons_fitOutput__Lxy->at(D0Index) * Charm::mm < Lxy_cut) {
        continue;
      }

      // impact parameter
      float d0_cut = 1.0;
      if (loose_selection) {
        d0_cut = 10.0;
      }
      if (fabs(m_DMesons_fitOutput__Impact->at(i)) * Charm::mm > d0_cut) {
        continue;
      }

      // D0 Mass window
      if (fabs(m_DMesons_m->at(D0Index) - D0_MASS) > 40) {
        continue;
      }

      // Isolation
      if (m_dplus_isolation) {
        if (!loose_selection) {
          if (m_DMesons_ptcone40->at(i) / m_DMesons_pt->at(i) > 1.0) {
            continue;
          }
        }
      }

      // soft pion pt
      if (D_meson_min_track_pt(i) * Charm::MeV < 500.) {
        continue;
      }

      // Soft Pion dR
      if (soft_pion_Dstar_separation(i, D0Index) > .3) {
        continue;
      }

      // Soft pion d0
      if (fabs(m_DMesons_SlowPionD0->at(i)) > 1) {
        continue;
      }
    }

    // save D* candidate index
    m_D_star_indices.push_back(i);
  }
}

void DMesonRec::get_D_star_pi0_mesons(EventLoopBase *elb,
                                      bool loose_selection) {
  get_D_presel(elb, DecayType::Dstar_pi0);
  int D0Index;
  m_D_star_pi0_indices.clear();

  for (unsigned int index = 0; index < m_presel_Dstar_pi0.size(); index++) {
    unsigned int i = m_presel_Dstar_pi0.at(index);

    int failed_cut = 0;

    D0Index = m_DMesons_D0Index->at(i);

    // SV chi2 requirement **CUT
    if (m_DMesons_fitOutput__Chi2->at(i) > 8.) {
      continue;
    }

    // daughter tracks pt **CUT
    if (D_meson_min_track_pt(D0Index) * Charm::MeV < 800.) {
      continue;
    }

    // daughter dR separation
    if (D_meson_max_track_dR(D0Index) > 0.6) {
      continue;
    }

    // min Lxy requirement
    if (m_DMesons_fitOutput__Lxy->at(D0Index) * Charm::mm < 0.3) {
      continue;
    }

    // max Lxy requirement
    // if (fabs(m_DMesons_fitOutput__Lxy->at(i) - 24) * Charm::mm < 2.0)
    //{
    //        continue;
    //}

    // impact parameter
    if (fabs(m_DMesons_fitOutput__Impact->at(i)) * Charm::mm > 10.0) {
      continue;
    }

    // impact significance **CUT
    if (fabs(m_DMesons_fitOutput__ImpactSignificance->at(i)) > 6.0) {
      continue;
    }

    // D0 Mass window
    if ((m_DMesons_m->at(D0Index) * Charm::GeV) < 1.5 ||
        (m_DMesons_m->at(D0Index) * Charm::GeV) > 1.7) {
      continue;
    }

    // Isolation
    if (!loose_selection) {
      if (m_DMesons_ptcone40->at(i) / m_DMesons_pt->at(D0Index) > 2.0) {
        continue;
      }
    }

    // soft pion pt
    if (D_meson_min_track_pt(i) * Charm::MeV < 500.) {
      continue;
    }

    // Soft Pion dR
    if (soft_pion_Dstar_separation(i, D0Index) > .3) {
      continue;
    }

    // Soft pion d0
    if (fabs(m_DMesons_SlowPionD0->at(i)) > 1) {
      continue;
    }

    // D+ meson eta
    if (fabs(m_DMesons_eta->at(i)) > 2.2) {
      continue;
    }

    // D+ meson pt
    if (m_DMesons_pt->at(i) * Charm::GeV < 8.0) {
      continue;
    }

    //         // invariant mass cut
    //         if (m_DMesons_m->at(i) <= 1700. ||
    //             m_DMesons_m->at(i) > 2200.)
    //             continue;

    // save D* candidate index
    m_D_star_pi0_indices.push_back(i);
  }
}

void DMesonRec::get_D_s_mesons(EventLoopBase *elb, bool loose_selection) {
  get_D_presel(elb, DecayType::Ds);

  m_D_s_indices.clear();

  for (unsigned int index = 0; index < m_presel_Ds.size(); index++) {
    unsigned int i = m_presel_Ds.at(index);
    int failed_cut = 0;

    // SV chi2 requirement
    if (m_DMesons_fitOutput__Chi2->at(i) > 6.) {
      continue;
    }

    // daughter tracks pt
    if (D_meson_min_track_pt(i) * Charm::MeV < 800.) {
      continue;
    }

    // daughter dR separation
    if (D_meson_max_track_dR(i) > 0.6) {
      continue;
    }

    if (!loose_selection) {
      // impact significance
      if (fabs(m_DMesons_fitOutput__ImpactSignificance->at(i)) > 6.0) {
        continue;
      }
    }

    // min Lxy requirement
    if (m_DMesons_fitOutput__Lxy->at(i) * Charm::mm < 0.5) {
      continue;
    }

    // max Lxy requirement
    // if (fabs(m_DMesons_fitOutput__Lxy->at(i) - 24) * Charm::mm < 2.0)
    //{
    //        continue;
    //}

    // impact parameter
    float d0_cut = 1.0;
    if (loose_selection) {
      d0_cut = 10.0;
    }
    if (fabs(m_DMesons_fitOutput__Impact->at(i)) * Charm::mm > d0_cut) {
      continue;
    }

    // combinatorial background rejection
    if (m_DMesons_costhetastar->at(i) > 0.8) {
      continue;
    }

    // D+ rejection
    if (fabs(m_DMesons_mPhi1->at(i) - PHI_MASS) > 8.0) {
      continue;
    }

    // Isolation of Ds (NEW CUT wrt Run 1)
    if (!loose_selection) {
      if (m_DMesons_ptcone40->at(i) / m_DMesons_pt->at(i) > 2.) {
        continue;
      }
    }

    // D+ meson eta
    if (fabs(m_DMesons_eta->at(i)) > 2.2) {
      continue;
    }

    // D+ meson pt
    if (m_DMesons_pt->at(i) * Charm::GeV < 8.0) {
      continue;
    }

    // invariant mass cut
    if (m_DMesons_m->at(i) * Charm::GeV <= 1.7 ||
        m_DMesons_m->at(i) * Charm::GeV > 2.2)
      continue;

    // save Ds candidate index
    m_D_s_indices.push_back(i);
  }
}

void DMesonRec::get_Lambda_C_baryons(EventLoopBase *elb, bool loose_selection) {
  get_D_presel(elb, DecayType::LambdaC);

  m_Lambda_C_indices.clear();

  for (unsigned int index = 0; index < m_presel_LambdaCs.size(); index++) {
    unsigned int i = m_presel_LambdaCs.at(index);

    // D+ meson eta
    if (fabs(m_DMesons_eta->at(i)) > 2.2) {
      continue;
    }

    // D+ meson pt
    float pt_cut = 8.0;
    if (m_DMesons_pt->at(i) * Charm::GeV < pt_cut ||
        m_DMesons_pt->at(i) * Charm::GeV > 150.0) {
      continue;
    }

    // invariant mass cut
    if (m_DMesons_m->at(i) * Charm::GeV <= 2.0 ||
        m_DMesons_m->at(i) * Charm::GeV > 2.5) {
      continue;
    }

    // SV chi2 requirement
    if (m_DMesons_fitOutput__Chi2->at(i) > 20.) {
      continue;
    }

    // daughter tracks pt
    if (D_meson_min_track_pt(i) * Charm::MeV < 800.) {
      continue;
    }

    // impact parameter
    float d0_cut = 0.1;
    if (loose_selection) {
      d0_cut = 10.0;
    }
    if (fabs(m_DMesons_fitOutput__Impact->at(i)) * Charm::mm > d0_cut) {
      continue;
    }

    if (!loose_selection) {
      // impact significance
      if (fabs(m_DMesons_fitOutput__ImpactSignificance->at(i)) > 6.0) {
        continue;
      }
    }

    // min Lxy requirement
    if (m_DMesons_fitOutput__Lxy->at(i) * Charm::mm < 0.2) {
      continue;
    }

    // daughter dR separation
    if (D_meson_max_track_dR(i) > 1.0) {
      continue;
    }

    // // combinatorial background rejection
    // if (m_DMesons_costhetastar->at(i) < -0.8) {
    //   continue;
    // }

    // D*+ rejection
    if ((std::min(m_DMesons_m->at(i) - m_DMesons_mKpi1->at(i),
                  m_DMesons_m->at(i) - m_DMesons_mKpi2->at(i))) *
            Charm::MeV <
        180) {
      continue;
    }

    // Ds rejection
    if ((std::min(m_DMesons_mPhi1->at(i), m_DMesons_mPhi2->at(i))) *
            Charm::MeV <
        8.0) {
      continue;
    }

    // save D+ candidate index
    m_Lambda_C_indices.push_back(i);
  }
}

void DMesonRec::get_D_presel(EventLoopBase *elb, DecayType decayType) {
  if (decayType == DecayType::Dplus) {
    m_presel_Dplus.clear();
  } else if (decayType == DecayType::Dstar) {
    m_presel_Dstar.clear();
  } else if (decayType == DecayType::Ds) {
    m_presel_Ds.clear();
  } else if (decayType == DecayType::Dstar_pi0) {
    m_presel_Dstar_pi0.clear();
  } else if (decayType == DecayType::LambdaC) {
    m_presel_LambdaCs.clear();
  }

  TruthInfo *truthInfo = dynamic_cast<TruthInfo *>(elb);

  for (unsigned int i = 0; i < m_DMesons_decayType->size(); i++) {
    // Correct decay type
    if (m_DMesons_decayType->at(i) != static_cast<int>(decayType)) continue;

    if (m_D_tightPrimary_tracks && !D_pass_tight(i)) continue;

    // Determines if evaluating only matched D's is necessary (eff for example)
    if (m_do_twice_failed &&
        truthInfo->TruthInfo::get_truth_category(elb, i) != "Matched") {
      continue;
    }

    // Baseline Lxy
    if (m_DMesons_fitOutput__Lxy->at(i) * Charm::mm < 0.0) continue;

    if (decayType == DecayType::Dplus) {
      m_presel_Dplus.push_back(i);
    } else if (decayType == DecayType::Dstar) {
      m_presel_Dstar.push_back(i);
    } else if (decayType == DecayType::Dstar_pi0) {
      m_presel_Dstar_pi0.push_back(i);
    } else if (decayType == DecayType::Ds) {
      m_presel_Ds.push_back(i);
    } else if (decayType == DecayType::LambdaC) {
      m_presel_LambdaCs.push_back(i);
    }
  }
}

std::pair<unsigned int, int> DMesonRec::get_best_D_candidate(
    std::vector<std::pair<unsigned int, int>> candidates) {
  unsigned int best_index = 0;
  int best_failed_cut = -1;
  float best_chi2 = -1;
  for (auto &pair : candidates) {
    unsigned int index = pair.first;
    int failed_cut = pair.second;
    if (best_chi2 < 0) {
      best_chi2 = m_DMesons_fitOutput__Chi2->at(index);
      best_index = index;
      best_failed_cut = failed_cut;
      continue;
    } else {
      if (m_DMesons_fitOutput__Chi2->at(index) < best_chi2) {
        best_chi2 = m_DMesons_fitOutput__Chi2->at(index);
        best_index = index;
        best_failed_cut = failed_cut;
      }
    }
  }
  return std::pair<unsigned int, int>{best_index, best_failed_cut};
}

float DMesonRec::D_meson_min_track_pt(unsigned int index) {
  return *std::min_element(m_DMesons_daughterInfo__pt->at(index).begin(),
                           m_DMesons_daughterInfo__pt->at(index).end());
}

float DMesonRec::D_meson_max_track_dR(unsigned int index) {
  float dRmax = 0;
  std::vector<float> *daughterInfo__eta =
      &m_DMesons_daughterInfo__eta->at(index);
  std::vector<float> *daughterInfo__phi =
      &m_DMesons_daughterInfo__phi->at(index);
  for (unsigned int i = 0; i < daughterInfo__eta->size(); i++) {
    for (unsigned int j = i + 1; j < daughterInfo__eta->size(); j++) {
      float deta = daughterInfo__eta->at(i) - daughterInfo__eta->at(j);
      float dphi = TVector2::Phi_mpi_pi(daughterInfo__phi->at(i) -
                                        daughterInfo__phi->at(j));
      float dR = TMath::Sqrt(deta * deta + dphi * dphi);
      if (dR > dRmax) dRmax = dR;
    }
  }
  return dRmax;
}

float DMesonRec::D_meson_lepton_dR(unsigned int index) {
  float deta = m_DMesons_eta->at(index) - m_lep->Eta();
  float dphi = TVector2::Phi_mpi_pi(m_DMesons_phi->at(index) - m_lep->Phi());
  float dR = TMath::Sqrt(deta * deta + dphi * dphi);
  return dR;
}

float DMesonRec::D_meson_lepton_dR(unsigned int index, float lep_eta,
                                   float lep_phi) {
  float deta = m_DMesons_eta->at(index) - lep_eta;
  float dphi = TVector2::Phi_mpi_pi(m_DMesons_phi->at(index) - lep_phi);
  float dR = TMath::Sqrt(deta * deta + dphi * dphi);
  return dR;
}

float DMesonRec::soft_pion_Dstar_separation(unsigned int index,
                                            unsigned int D0Index) {
  float eta[2] = {m_DMesons_daughterInfo__eta->at(index).back(),
                  m_DMesons_eta->at(D0Index)};
  float phi[2] = {m_DMesons_daughterInfo__phi->at(index).back(),
                  m_DMesons_phi->at(D0Index)};
  float deta = eta[0] - eta[1];
  float dphi = TVector2::Phi_mpi_pi(phi[0] - phi[1]);
  return TMath::Sqrt(deta * deta + dphi * dphi);
}

bool DMesonRec::D_pass_tight(unsigned int index) {
  if (std::count(m_DMesons_daughterInfo__passTight->at(index).begin(),
                 m_DMesons_daughterInfo__passTight->at(index).end(), false))
    return false;
  else
    return true;
}

std::string DMesonRec::get_charge_prefix(float lep_charge, unsigned int index) {
  if (lep_charge == 0) {
    if (m_DMesons_pdgId->at(index) > 0) {
      return "_per_D";
    } else {
      return "_per_antiD";
    }
  }
  int charge;
  if (m_DMesons_pdgId->at(index) > 0) {
    charge = 1;
  } else {
    charge = -1;
  }
  if (charge * lep_charge < 0) {
    return "_OS";
  } else {
    return "_SS";
  }
}

std::string DMesonRec::D_meson_pt_bin(EventLoopBase *elb, unsigned int index) {
  bool m_do_zplusd_binning = elb->m_zplusd_mode;
  float D_pt = m_DMesons_pt->at(index) * Charm::GeV;
  if (m_do_zplusd_binning) {
    if (D_pt < elb->m_zd_differential_d_pt_bins.get_differential_bins()[0]) {
      return "_pt_bin0";
    } else if (D_pt >= elb->m_zd_differential_d_pt_bins
                           .get_differential_bins()[0] &&
               D_pt < elb->m_zd_differential_d_pt_bins
                          .get_differential_bins()[1]) {
      return "_pt_bin1";
    } else if (D_pt >= elb->m_zd_differential_d_pt_bins
                           .get_differential_bins()[1] &&
               D_pt < elb->m_zd_differential_d_pt_bins
                          .get_differential_bins()[2]) {
      return "_pt_bin2";
    } else if (D_pt >= elb->m_zd_differential_d_pt_bins
                           .get_differential_bins()[2] &&
               D_pt < elb->m_zd_differential_d_pt_bins
                          .get_differential_bins()[3]) {
      return "_pt_bin3";
    } else if (D_pt >= elb->m_zd_differential_d_pt_bins
                           .get_differential_bins()[3] &&
               D_pt < elb->m_zd_differential_d_pt_bins
                          .get_differential_bins()[4]) {
      return "_pt_bin4";
    } else {
      return "_pt_bin5";
    }
  } else {
    if (D_pt < elb->m_differential_bins.get_differential_bins()[0]) {
      return "_pt_bin0";
    } else if (D_pt >= elb->m_differential_bins.get_differential_bins()[0] &&
               D_pt < elb->m_differential_bins.get_differential_bins()[1]) {
      return "_pt_bin1";
    } else if (D_pt >= elb->m_differential_bins.get_differential_bins()[1] &&
               D_pt < elb->m_differential_bins.get_differential_bins()[2]) {
      return "_pt_bin2";
    } else if (D_pt >= elb->m_differential_bins.get_differential_bins()[2] &&
               D_pt < elb->m_differential_bins.get_differential_bins()[3]) {
      return "_pt_bin3";
    } else if (D_pt >= elb->m_differential_bins.get_differential_bins()[3] &&
               D_pt < elb->m_differential_bins.get_differential_bins()[4]) {
      return "_pt_bin4";
    } else {
      return "_pt_bin5";
    }
  }
}

std::string DMesonRec::D_meson_eta_bin(EventLoopBase *elb, int i) {
  CharmLoopBase *WQCD = dynamic_cast<CharmLoopBase *>(elb);
  int eta_bin = elb->m_differential_bins_eta.get_eta_bin(
                    WQCD->get_sys_lep_abs_eta().at(i)) +
                1;
  if (eta_bin > 0) {
    return std::string("_eta_bin") + std::to_string(eta_bin);
  }
  return "_eta_bin0";
}

bool DMesonRec::fill_D_plus_histograms(
    EventLoopBase *elb, std::vector<std::string> channels, unsigned int index,
    std::vector<bool> extra_flag, std::vector<double> extra_weight,
    bool diff_bins_override, bool track_jet_selection) {
  std::vector<std::string> suffixes = channels;
  if (!diff_bins_override) {
    if (m_bin_in_D_pt) {
      for (unsigned int i = 0; i < channels.size(); i++) {
        suffixes.at(i) += D_meson_pt_bin(elb, index);
      }
    } else if (m_bin_in_lep_eta) {
      for (unsigned int i = 0; i < channels.size(); i++) {
        suffixes.at(i) += D_meson_eta_bin(elb, i);
      }
    }
  }

  elb->add_fill_hist_sys("Dmeson_m", suffixes, m_sys_dmeson_mass, 40, 1.7, 2.2,
                         true, extra_flag, extra_weight);
  if (!elb->m_fit_variables_only || track_jet_selection) {
    elb->add_fill_hist_sys("Dmeson_pt", suffixes,
                           m_DMesons_pt->at(index) * Charm::GeV, 30, 0, 150.,
                           true, extra_flag, extra_weight);
  }
  if (!elb->m_fit_variables_only) {
    elb->add_fill_hist_sys("Dmeson_eta", suffixes, m_DMesons_eta->at(index),
                           100, -3.0, 3.0, true, extra_flag, extra_weight);
    elb->add_fill_hist_sys("Dmeson_phi", suffixes, m_DMesons_phi->at(index),
                           100, -M_PI, M_PI, false, extra_flag, extra_weight);
    elb->add_fill_hist_sys(
        "Dmeson_SVr", suffixes,
        sqrt(pow(m_DMesons_fitOutput__VertexPosition->at(index)[0], 2) +
             pow(m_DMesons_fitOutput__VertexPosition->at(index)[1], 2)),
        1000, 0, 100, false, extra_flag, extra_weight);
    elb->add_fill_hist_sys(
        "Dmeson_max_trackZ0", suffixes,
        *(std::max_element(
            m_DMesons_daughterInfo__z0SinTheta->at(index).begin(),
            m_DMesons_daughterInfo__z0SinTheta->at(index).end(),
            [](float z01, float z02) { return fabs(z02) > fabs(z01); })),
        150, -150, 150., false, extra_flag, extra_weight);
    elb->add_fill_hist_sys(
        "Dmeson_max_trackZ0PV", suffixes,
        *(std::max_element(
            m_DMesons_daughterInfo__z0SinThetaPV->at(index).begin(),
            m_DMesons_daughterInfo__z0SinThetaPV->at(index).end(),
            [](float z01, float z02) { return fabs(z02) > fabs(z01); })),
        360, -6, 6., false, extra_flag, extra_weight);
    elb->add_fill_hist_sys("Dmeson_cTau", suffixes,
                           m_DMesons_fitOutput__Lxy->at(index) *
                               m_DMesons_m->at(index) / m_DMesons_pt->at(index),
                           200, 0, 2.0, false, extra_flag, extra_weight);
    elb->add_fill_hist_sys("Dmeson_z0sinTheta", suffixes,
                           m_DMesons_fitOutput__ImpactZ0SinTheta->at(index),
                           200, -2, 2, false, extra_flag, extra_weight);
    elb->add_fill_hist_sys("Dmeson_kaonPassTight", suffixes,
                           m_DMesons_daughterInfo__passTight->at(index).at(0),
                           2, 0, 2, false, extra_flag, extra_weight);
  }

  if (elb->m_fit_variables_only) {
    if ((m_bin_in_D_pt || m_bin_in_lep_eta) && !diff_bins_override) {
      fill_D_plus_histograms(elb, channels, index, extra_flag, extra_weight,
                             true);
    }
    return true;
  }

  elb->add_fill_hist_sys("Dmeson_chi2", suffixes,
                         m_DMesons_fitOutput__Chi2->at(index), 100, 0, 12.0,
                         true, extra_flag, extra_weight);
  if (m_do_Nminus1) {
    elb->add_fill_hist_sys("Dmeson_m_chi2", suffixes,
                           m_DMesons_m->at(index) * Charm::GeV, 1000, 1.4, 2.4,
                           false, extra_flag, extra_weight);
    if (fabs(m_DMesons_m->at(index) - DP_MASS) < 70.)
      elb->add_fill_hist_sys("Dmeson_chi2_inSB", suffixes,
                             m_DMesons_fitOutput__Chi2->at(index), 100, 0, 12.5,
                             false, extra_flag, extra_weight);
    else if (m_DMesons_m->at(index) - DP_MASS > 70)
      elb->add_fill_hist_sys("Dmeson_chi2_outSB_R", suffixes,
                             m_DMesons_fitOutput__Chi2->at(index), 100, 0, 12.5,
                             false, extra_flag, extra_weight);
    else
      elb->add_fill_hist_sys("Dmeson_chi2_outSB_L", suffixes,
                             m_DMesons_fitOutput__Chi2->at(index), 100, 0, 12.5,
                             false, extra_flag, extra_weight);
  }

  elb->add_fill_hist_sys("Dmeson_min_trackPt", suffixes,
                         D_meson_min_track_pt(index) * Charm::GeV, 100, 0, 10.,
                         true, extra_flag, extra_weight);
  if (m_do_Nminus1) {
    elb->add_fill_hist_sys("Dmeson_m_min_trackPt", suffixes,
                           m_DMesons_m->at(index) * Charm::GeV, 1000, 1.4, 2.4,
                           false, extra_flag, extra_weight);
  }
  elb->add_fill_hist_sys("Dmeson_max_trackdR", suffixes,
                         D_meson_max_track_dR(index), 100, 0, 1.2, true,
                         extra_flag, extra_weight);
  if (m_do_Nminus1) {
    elb->add_fill_hist_sys("Dmeson_m_max_trackdR", suffixes,
                           m_DMesons_m->at(index) * Charm::GeV, 1000, 1.4, 2.4,
                           false, extra_flag, extra_weight);
  }
  elb->add_fill_hist_sys("Dmeson_impact_sig", suffixes,
                         m_DMesons_fitOutput__ImpactSignificance->at(index),
                         100, 0, 10, true, extra_flag, extra_weight);
  if (m_do_Nminus1) {
    elb->add_fill_hist_sys("Dmeson_m_impact_sig", suffixes,
                           m_DMesons_m->at(index) * Charm::GeV, 1000, 1.4, 2.4,
                           false, extra_flag, extra_weight);
    if (fabs(m_DMesons_m->at(index) - DP_MASS) < 70.)
      elb->add_fill_hist_sys("Dmeson_impact_sig_inSB", suffixes,
                             m_DMesons_fitOutput__ImpactSignificance->at(index),
                             100, 0, 10, false, extra_flag, extra_weight);
    else if (m_DMesons_m->at(index) - DP_MASS > 70)
      elb->add_fill_hist_sys("Dmeson_impact_sig_outSB_R", suffixes,
                             m_DMesons_fitOutput__ImpactSignificance->at(index),
                             100, 0, 10, false, extra_flag, extra_weight);
    else
      elb->add_fill_hist_sys("Dmeson_impact_sig_outSB_L", suffixes,
                             m_DMesons_fitOutput__ImpactSignificance->at(index),
                             100, 0, 10, false, extra_flag, extra_weight);
  }
  elb->add_fill_hist_sys("Dmeson_Lxy", suffixes,
                         m_DMesons_fitOutput__Lxy->at(index), 300, 0, 30., true,
                         extra_flag, extra_weight);
  if (m_do_Nminus1) {
    elb->add_fill_hist_sys("Dmeson_m_Lxy", suffixes,
                           m_DMesons_m->at(index) * Charm::GeV, 1000, 1.4, 2.4,
                           false, extra_flag, extra_weight);
    if (fabs(m_DMesons_m->at(index) - DP_MASS) < 70.)
      elb->add_fill_hist_sys("Dmeson_Lxy_inSB", suffixes,
                             m_DMesons_fitOutput__Lxy->at(index), 300, 0, 30.,
                             false, extra_flag, extra_weight);
    else if (m_DMesons_m->at(index) - DP_MASS > 70)
      elb->add_fill_hist_sys("Dmeson_Lxy_outSB_R", suffixes,
                             m_DMesons_fitOutput__Lxy->at(index), 300, 0, 30.,
                             false, extra_flag, extra_weight);
    else
      elb->add_fill_hist_sys("Dmeson_Lxy_outSB_L", suffixes,
                             m_DMesons_fitOutput__Lxy->at(index), 300, 0, 30.,
                             false, extra_flag, extra_weight);
  }
  elb->add_fill_hist_sys("Dmeson_d0", suffixes,
                         m_DMesons_fitOutput__Impact->at(index) * Charm::mum,
                         200, -1100, 1100, false, extra_flag, extra_weight);
  if (m_do_Nminus1) {
    elb->add_fill_hist_sys("Dmeson_m_d0", suffixes,
                           m_DMesons_m->at(index) * Charm::GeV, 1000, 1.4, 2.4,
                           false, extra_flag, extra_weight);
  }
  elb->add_fill_hist_sys("Dmeson_cosThetaStar", suffixes,
                         m_DMesons_costhetastar->at(index), 100, -1., 1., false,
                         extra_flag, extra_weight);
  if (m_do_Nminus1) {
    elb->add_fill_hist_sys("Dmeson_m_cosThetaStar", suffixes,
                           m_DMesons_m->at(index) * Charm::GeV, 1000, 1.4, 2.4,
                           false, extra_flag, extra_weight);
  }
  elb->add_fill_hist_sys(
      "Dmeson_DstarMin", suffixes,
      std::min(m_DMesons_m->at(index) - m_DMesons_mKpi1->at(index),
               m_DMesons_m->at(index) - m_DMesons_mKpi2->at(index)) *
          Charm::GeV,
      100, 0, 1.6, false, extra_flag, extra_weight);
  if (m_do_Nminus1) {
    elb->add_fill_hist_sys("Dmeson_m_DstarMin", suffixes,
                           m_DMesons_m->at(index) * Charm::GeV, 1000, 1.4, 2.4,
                           false, extra_flag, extra_weight);
  }
  elb->add_fill_hist_sys(
      "Dmeson_DsMin", suffixes,
      std::min(m_DMesons_mPhi1->at(index), m_DMesons_mPhi2->at(index)) *
          Charm::GeV,
      250, 0., 2., false, extra_flag, extra_weight);
  if (m_do_Nminus1) {
    elb->add_fill_hist_sys("Dmeson_m_DsMin", suffixes,
                           m_DMesons_m->at(index) * Charm::GeV, 1000, 1.4, 2.4,
                           false, extra_flag, extra_weight);
  }
  elb->add_fill_hist_sys(
      "Dmeson_ptcone40_pt", suffixes,
      m_DMesons_ptcone40->at(index) / m_DMesons_pt->at(index), 100, 0, 10, true,
      extra_flag, extra_weight);
  if (m_do_Nminus1) {
    elb->add_fill_hist_sys("Dmeson_m_ptcone40_pt", suffixes,
                           m_DMesons_m->at(index) * Charm::GeV, 1000, 1.4, 2.4,
                           false, extra_flag, extra_weight);
    if (fabs(m_DMesons_m->at(index) - DP_MASS) < 70.)
      elb->add_fill_hist_sys(
          "Dmeson_ptcone40_pt_inSB", suffixes,
          m_DMesons_ptcone40->at(index) / m_DMesons_pt->at(index), 100, 0, 10,
          false, extra_flag, extra_weight);
    else if (m_DMesons_m->at(index) - DP_MASS > 70)
      elb->add_fill_hist_sys(
          "Dmeson_ptcone40_pt_outSB_R", suffixes,
          m_DMesons_ptcone40->at(index) / m_DMesons_pt->at(index), 100, 0, 10,
          false, extra_flag, extra_weight);
    else
      elb->add_fill_hist_sys(
          "Dmeson_ptcone40_pt_outSB_L", suffixes,
          m_DMesons_ptcone40->at(index) / m_DMesons_pt->at(index), 100, 0, 10,
          false, extra_flag, extra_weight);
  }

  if ((m_bin_in_D_pt || m_bin_in_lep_eta) && !diff_bins_override) {
    fill_D_plus_histograms(elb, channels, index, extra_flag, extra_weight,
                           true);
  }
  return true;
}

bool DMesonRec::fill_D_star_histograms(
    EventLoopBase *elb, std::vector<std::string> channels, unsigned int index,
    std::vector<bool> extra_flag, std::vector<double> extra_weight,
    bool diff_bins_override, bool track_jet_selection) {
  std::vector<std::string> suffixes = channels;
  if (!diff_bins_override) {
    if (m_bin_in_D_pt) {
      for (unsigned int i = 0; i < channels.size(); i++) {
        suffixes.at(i) += D_meson_pt_bin(elb, index);
      }
    } else if (m_bin_in_lep_eta) {
      for (unsigned int i = 0; i < channels.size(); i++) {
        suffixes.at(i) += D_meson_eta_bin(elb, i);
      }
    }
  }

  int D0Index = m_DMesons_D0Index->at(index);

  elb->add_fill_hist_sys("Dmeson_mdiff", suffixes, m_sys_dmeson_mass, 200, 140,
                         180, true, extra_flag, extra_weight);
  if (!elb->m_fit_variables_only || track_jet_selection) {
    elb->add_fill_hist_sys("Dmeson_pt", suffixes,
                           m_DMesons_pt->at(index) * Charm::GeV, 30, 0, 150.,
                           true, extra_flag, extra_weight);
  }
  if (!elb->m_fit_variables_only) {
    elb->add_fill_hist_sys("Dmeson_eta", suffixes, m_DMesons_eta->at(index),
                           100, -3.0, 3.0, true, extra_flag, extra_weight);
    elb->add_fill_hist_sys("Dmeson_phi", suffixes, m_DMesons_phi->at(index),
                           100, -M_PI, M_PI, false, extra_flag, extra_weight);
    elb->add_fill_hist_sys("Dmeson_m", suffixes,
                           m_DMesons_m->at(index) * Charm::GeV, 40, 1.7, 2.2,
                           true, extra_flag, extra_weight);
    elb->add_fill_hist_sys(
        "Dmeson_ptcone40_pt", suffixes,
        m_DMesons_ptcone40->at(index) / m_DMesons_pt->at(index), 100, 0, 10,
        true, extra_flag, extra_weight);
    elb->add_fill_hist_sys(
        "Dmeson_max_trackZ0", suffixes,
        *(std::max_element(
            m_DMesons_daughterInfo__z0SinTheta->at(index).begin(),
            m_DMesons_daughterInfo__z0SinTheta->at(index).end(),
            [](float z01, float z02) { return fabs(z02) > fabs(z01); })),
        150, -150, 150., false, extra_flag, extra_weight);
    elb->add_fill_hist_sys(
        "Dmeson_max_trackZ0PV", suffixes,
        *(std::max_element(
            m_DMesons_daughterInfo__z0SinThetaPV->at(index).begin(),
            m_DMesons_daughterInfo__z0SinThetaPV->at(index).end(),
            [](float z01, float z02) { return fabs(z02) > fabs(z01); })),
        360, -6, 6., false, extra_flag, extra_weight);
    elb->add_fill_hist_sys("Dmeson_cTau", suffixes,
                           m_DMesons_fitOutput__Lxy->at(index) *
                               m_DMesons_m->at(index) / m_DMesons_pt->at(index),
                           200, 0, 2.0, false, extra_flag, extra_weight);
    elb->add_fill_hist_sys(
        "Dmeson_SVr", suffixes,
        sqrt(pow(m_DMesons_fitOutput__VertexPosition->at(index)[0], 2) +
             pow(m_DMesons_fitOutput__VertexPosition->at(index)[1], 2)),
        1000, 0, 100, false, extra_flag, extra_weight);
  }

  if (elb->m_fit_variables_only) {
    if ((m_bin_in_D_pt || m_bin_in_lep_eta) && !diff_bins_override) {
      fill_D_star_histograms(elb, channels, index, extra_flag, extra_weight,
                             true);
    }
    return true;
  }

  elb->add_fill_hist_sys("Dmeson_chi2", suffixes,
                         m_DMesons_fitOutput__Chi2->at(index), 100, 0, 12.0,
                         true, extra_flag, extra_weight);
  if (m_do_Nminus1)
    elb->add_fill_hist_sys("Dmeson_mdiff_chi2", suffixes,
                           m_DMesons_DeltaMass->at(index), 1000, 140, 180,
                           false, extra_flag, extra_weight);
  elb->add_fill_hist_sys(
      "Dmeson_min_D0trackPt", suffixes,
      *(std::min_element(
          m_DMesons_daughterInfo__pt->at(D0Index).begin(),
          m_DMesons_daughterInfo__pt->at(D0Index).end()))*Charm::GeV,
      100, 0, 10., false, extra_flag, extra_weight);
  elb->add_fill_hist_sys("Dmeson_max_trackdR", suffixes,
                         D_meson_max_track_dR(index), 100, 0, 1.2, true,
                         extra_flag, extra_weight);
  elb->add_fill_hist_sys("Dmeson_impact_sig", suffixes,
                         m_DMesons_fitOutput__ImpactSignificance->at(index),
                         100, 0, 10, true, extra_flag, extra_weight);
  elb->add_fill_hist_sys("Dmeson_Lxy", suffixes,
                         m_DMesons_fitOutput__Lxy->at(index), 300, 0, 30., true,
                         extra_flag, extra_weight);
  elb->add_fill_hist_sys("Dmeson_d0", suffixes,
                         m_DMesons_fitOutput__Impact->at(index) * Charm::mum,
                         200, -1100, 1100, false, extra_flag, extra_weight);
  elb->add_fill_hist_sys("Dmeson_D0mass", suffixes,
                         m_DMesons_m->at(D0Index) * Charm::GeV, 60, 1.7, 2.0,
                         false, extra_flag, extra_weight);
  elb->add_fill_hist_sys(
      "Dmeson_D0pt_ptcone40", suffixes,
      m_DMesons_ptcone40->at(D0Index) / m_DMesons_pt->at(D0Index), 100, 0, 2,
      true, extra_flag, extra_weight);
  elb->add_fill_hist_sys(
      "Dmeson_min_trackPt", suffixes,
      *(std::min_element(
          m_DMesons_daughterInfo__pt->at(index).begin(),
          m_DMesons_daughterInfo__pt->at(index).end()))*Charm::GeV,
      100, 0, 10., true, extra_flag, extra_weight);
  elb->add_fill_hist_sys("Dmeson_softPiDR", suffixes,
                         soft_pion_Dstar_separation(index, D0Index), 100, 0.0,
                         0.6, false, extra_flag, extra_weight);
  elb->add_fill_hist_sys("Dmeson_softPiD0", suffixes,
                         m_DMesons_SlowPionD0->at(index), 100, -1.0, 1.0, false,
                         extra_flag, extra_weight);

  if ((m_bin_in_D_pt || m_bin_in_lep_eta) && !diff_bins_override) {
    fill_D_star_histograms(elb, channels, index, extra_flag, extra_weight,
                           true);
  }
  return true;
}

bool DMesonRec::fill_D_star_pi0_histograms(EventLoopBase *elb,
                                           std::vector<std::string> channels,
                                           unsigned int index,
                                           std::vector<bool> extra_flag,
                                           std::vector<double> extra_weight,
                                           bool diff_bins_override) {
  std::vector<std::string> suffixes = channels;
  if (!diff_bins_override) {
    if (m_bin_in_D_pt) {
      for (unsigned int i = 0; i < channels.size(); i++) {
        suffixes.at(i) += D_meson_pt_bin(elb, index);
      }
    } else if (m_bin_in_lep_eta) {
      for (unsigned int i = 0; i < channels.size(); i++) {
        suffixes.at(i) += D_meson_eta_bin(elb, i);
      }
    }
  }

  int D0Index = m_DMesons_D0Index->at(index);

  elb->add_fill_hist_sys("Dmeson_mdiff", suffixes,
                         m_DMesons_DeltaMass->at(index), 200, 140, 180, true,
                         extra_flag, extra_weight);
  if (!elb->m_fit_variables_only) {
    elb->add_fill_hist_sys("Dmeson_pt", suffixes,
                           m_DMesons_pt->at(index) * Charm::GeV, 30, 0, 150.,
                           true, extra_flag, extra_weight);
    elb->add_fill_hist_sys("Dmeson_eta", suffixes, m_DMesons_eta->at(index),
                           100, -3.0, 3.0, false, extra_flag, extra_weight);
    elb->add_fill_hist_sys("Dmeson_phi", suffixes, m_DMesons_phi->at(index),
                           100, -M_PI, M_PI, false, extra_flag, extra_weight);
    elb->add_fill_hist_sys("Dmeson_m", suffixes,
                           m_DMesons_m->at(index) * Charm::GeV, 40, 1.7, 2.2,
                           true, extra_flag, extra_weight);
    elb->add_fill_hist_sys(
        "Dmeson_ptcone40_pt", suffixes,
        m_DMesons_pt->at(index) / m_DMesons_ptcone40->at(index), 100, 0, 10,
        false, extra_flag, extra_weight);
    elb->add_fill_hist_sys(
        "Dmeson_max_trackZ0", suffixes,
        *(std::max_element(
            m_DMesons_daughterInfo__z0SinTheta->at(index).begin(),
            m_DMesons_daughterInfo__z0SinTheta->at(index).end(),
            [](float z01, float z02) { return fabs(z02) > fabs(z01); })),
        150, -150, 150., false, extra_flag, extra_weight);
    elb->add_fill_hist_sys(
        "Dmeson_max_trackZ0PV", suffixes,
        *(std::max_element(
            m_DMesons_daughterInfo__z0SinThetaPV->at(index).begin(),
            m_DMesons_daughterInfo__z0SinThetaPV->at(index).end(),
            [](float z01, float z02) { return fabs(z02) > fabs(z01); })),
        360, -6, 6., false, extra_flag, extra_weight);
    elb->add_fill_hist_sys("Dmeson_cTau", suffixes,
                           m_DMesons_fitOutput__Lxy->at(index) *
                               m_DMesons_m->at(index) / m_DMesons_pt->at(index),
                           200, 0, 2.0, false, extra_flag, extra_weight);
    elb->add_fill_hist_sys(
        "Dmeson_SVr", suffixes,
        sqrt(pow(m_DMesons_fitOutput__VertexPosition->at(index)[0], 2) +
             pow(m_DMesons_fitOutput__VertexPosition->at(index)[1], 2)),
        1000, 0, 100, false, extra_flag, extra_weight);
  }
  if (elb->m_fit_variables_only) {
    if ((m_bin_in_D_pt || m_bin_in_lep_eta) && !diff_bins_override) {
      fill_D_star_pi0_histograms(elb, channels, index, extra_flag, extra_weight,
                                 true);
    }
    return true;
  }
  elb->add_fill_hist_sys("Dmeson_chi2", suffixes,
                         m_DMesons_fitOutput__Chi2->at(index), 100, 0, 6.5,
                         false, extra_flag, extra_weight);
  if (m_do_Nminus1)
    elb->add_fill_hist_sys("Dmeson_mdiff_chi2", suffixes,
                           m_DMesons_DeltaMass->at(index), 200, 140, 180, false,
                           extra_flag, extra_weight);
  elb->add_fill_hist_sys(
      "Dmeson_min_D0trackPt", suffixes,
      *(std::min_element(m_DMesons_daughterInfo__pt->at(D0Index).begin(),
                         m_DMesons_daughterInfo__pt->at(D0Index).end())) *
          Charm::GeV,
      100, 0, 10., false, extra_flag, extra_weight);
  if (m_do_Nminus1)
    elb->add_fill_hist_sys("Dmeson_mdiff_D0trackPt", suffixes,
                           m_DMesons_DeltaMass->at(index), 200, 140, 180, false,
                           extra_flag, extra_weight);
  elb->add_fill_hist_sys("Dmeson_max_trackdR", suffixes,
                         D_meson_max_track_dR(index), 100, 0, 1.2, false,
                         extra_flag, extra_weight);
  if (m_do_Nminus1)
    elb->add_fill_hist_sys("Dmeson_mdiff_max_trackd", suffixes,
                           m_DMesons_DeltaMass->at(index), 200, 140, 180, false,
                           extra_flag, extra_weight);
  elb->add_fill_hist_sys("Dmeson_impact_sig", suffixes,
                         m_DMesons_fitOutput__ImpactSignificance->at(index),
                         100, 0, 10, false, extra_flag, extra_weight);
  if (m_do_Nminus1)
    elb->add_fill_hist_sys("Dmeson_m_impact_sig", suffixes,
                           m_DMesons_DeltaMass->at(index), 200, 140, 180, false,
                           extra_flag, extra_weight);
  elb->add_fill_hist_sys("Dmeson_Lxy", suffixes,
                         m_DMesons_fitOutput__Lxy->at(index), 300, 0, 30.,
                         false, extra_flag, extra_weight);
  if (m_do_Nminus1)
    elb->add_fill_hist_sys("Dmeson_mdiff_Lxy", suffixes,
                           m_DMesons_DeltaMass->at(index), 200, 140, 180, false,
                           extra_flag, extra_weight);
  elb->add_fill_hist_sys("Dmeson_d0", suffixes,
                         m_DMesons_fitOutput__Impact->at(index) * Charm::mum,
                         200, -1100, 1100, false, extra_flag, extra_weight);
  if (m_do_Nminus1)
    elb->add_fill_hist_sys("Dmeson_mdiff_d0", suffixes,
                           m_DMesons_DeltaMass->at(index), 200, 140, 180, false,
                           extra_flag, extra_weight);
  elb->add_fill_hist_sys("Dmeson_D0massDiff", suffixes,
                         fabs(m_DMesons_m->at(D0Index) - D0_MASS), 100, 30.0,
                         70., false, extra_flag, extra_weight);
  if (m_do_Nminus1)
    elb->add_fill_hist_sys("Dmeson_mdiff_D0massDiff", suffixes,
                           m_DMesons_DeltaMass->at(index), 200, 140, 180, false,
                           extra_flag, extra_weight);
  elb->add_fill_hist_sys(
      "Dmeson_D0pt_ptcone40", suffixes,
      m_DMesons_ptcone40->at(D0Index) / m_DMesons_pt->at(D0Index), 100, 0, 2,
      false, extra_flag, extra_weight);
  if (m_do_Nminus1)
    elb->add_fill_hist_sys("Dmeson_mdiff_D0pt_ptcone40", suffixes,
                           m_DMesons_DeltaMass->at(index), 200, 140, 180, false,
                           extra_flag, extra_weight);
  elb->add_fill_hist_sys(
      "Dmeson_min_trackPt", suffixes,
      *(std::min_element(m_DMesons_daughterInfo__pt->at(index).begin(),
                         m_DMesons_daughterInfo__pt->at(index).end())) *
          Charm::GeV,
      100, 0, 10., false, extra_flag, extra_weight);
  if (m_do_Nminus1)
    elb->add_fill_hist_sys("Dmeson_mdiff_min_trackPt", suffixes,
                           m_DMesons_DeltaMass->at(index), 200, 140, 180, false,
                           extra_flag, extra_weight);
  elb->add_fill_hist_sys("Dmeson_softPiDR", suffixes,
                         soft_pion_Dstar_separation(index, D0Index), 100, 0.0,
                         0.6, false, extra_flag, extra_weight);
  if (m_do_Nminus1)
    elb->add_fill_hist_sys("Dmeson_mdiff_softPiDR", suffixes,
                           m_DMesons_DeltaMass->at(index), 200, 140, 180, false,
                           extra_flag, extra_weight);
  elb->add_fill_hist_sys("Dmeson_softPiD0", suffixes,
                         m_DMesons_SlowPionD0->at(index), 100, -1.0, 1.0, false,
                         extra_flag, extra_weight);
  if (m_do_Nminus1)
    elb->add_fill_hist_sys("Dmeson_mdiff_softPiD0", suffixes,
                           m_DMesons_DeltaMass->at(index), 200, 140, 180, false,
                           extra_flag, extra_weight);

  if ((m_bin_in_D_pt || m_bin_in_lep_eta) && !diff_bins_override) {
    fill_D_star_pi0_histograms(elb, channels, index, extra_flag, extra_weight,
                               true);
  }
  return true;
}

bool DMesonRec::fill_D_s_histograms(EventLoopBase *elb,
                                    std::vector<std::string> channels,
                                    unsigned int index,
                                    std::vector<bool> extra_flag,
                                    std::vector<double> extra_weight,
                                    bool diff_bins_override) {
  std::vector<std::string> suffixes = channels;

  if (m_bin_in_D_pt) {
    for (unsigned int i = 0; i < channels.size(); i++) {
      suffixes.at(i) += D_meson_pt_bin(elb, index);
    }
  } else if (m_bin_in_lep_eta) {
    for (unsigned int i = 0; i < channels.size(); i++) {
      suffixes.at(i) += D_meson_eta_bin(elb, i);
    }
  }

  elb->add_fill_hist_sys("Dmeson_pt", suffixes,
                         m_DMesons_pt->at(index) * Charm::GeV, 30, 0, 150.,
                         true, extra_flag, extra_weight);
  elb->add_fill_hist_sys("Dmeson_eta", suffixes, m_DMesons_eta->at(index), 100,
                         -3.0, 3.0, false, extra_flag, extra_weight);
  elb->add_fill_hist_sys("Dmeson_phi", suffixes, m_DMesons_phi->at(index), 100,
                         -M_PI, M_PI, false, extra_flag, extra_weight);
  elb->add_fill_hist_sys("Dmeson_m", suffixes,
                         m_DMesons_m->at(index) * Charm::GeV, 40, 1.7, 2.2,
                         true, extra_flag, extra_weight);
  elb->add_fill_hist_sys(
      "Dmeson_SVr", suffixes,
      sqrt(pow(m_DMesons_fitOutput__VertexPosition->at(index)[0], 2) +
           pow(m_DMesons_fitOutput__VertexPosition->at(index)[1], 2)),
      1000, 0, 100, false, extra_flag, extra_weight);
  elb->add_fill_hist_sys(
      "Dmeson_max_trackZ0", suffixes,
      *(std::max_element(
          m_DMesons_daughterInfo__z0SinTheta->at(index).begin(),
          m_DMesons_daughterInfo__z0SinTheta->at(index).end(),
          [](float z01, float z02) { return fabs(z02) > fabs(z01); })),
      150, -150, 150., false, extra_flag, extra_weight);
  elb->add_fill_hist_sys(
      "Dmeson_max_trackZ0PV", suffixes,
      *(std::max_element(
          m_DMesons_daughterInfo__z0SinThetaPV->at(index).begin(),
          m_DMesons_daughterInfo__z0SinThetaPV->at(index).end(),
          [](float z01, float z02) { return fabs(z02) > fabs(z01); })),
      360, -6, 6., false, extra_flag, extra_weight);
  elb->add_fill_hist_sys("Dmeson_cTau", suffixes,
                         m_DMesons_fitOutput__Lxy->at(index) *
                             m_DMesons_m->at(index) / m_DMesons_pt->at(index),
                         200, 0, 2.0, false, extra_flag, extra_weight);
  if (m_lep) {
    elb->add_fill_hist_sys(
        "Dmeson_dPhilep", suffixes,
        TVector2::Phi_mpi_pi(m_DMesons_phi->at(index) - m_lep->Phi()), 100,
        -M_PI, M_PI, false, extra_flag, extra_weight);
    elb->add_fill_hist_sys("Dmeson_dEtalep", suffixes,
                           m_DMesons_eta->at(index) - m_lep->Eta(), 100, -5.0,
                           5.0, false, extra_flag, extra_weight);
  }

  elb->add_fill_hist_sys("Dmeson_chi2", suffixes,
                         m_DMesons_fitOutput__Chi2->at(index), 100, 0, 6.5,
                         false, extra_flag, extra_weight);
  if (m_do_Nminus1)
    elb->add_fill_hist_sys("Dmeson_m_chi2", suffixes,
                           m_DMesons_m->at(index) * Charm::GeV, 1000, 1.4, 2.4,
                           false, extra_flag, extra_weight);
  elb->add_fill_hist_sys(
      "Dmeson_min_trackPt", suffixes,
      *(std::min_element(m_DMesons_daughterInfo__pt->at(index).begin(),
                         m_DMesons_daughterInfo__pt->at(index).end())) *
          Charm::GeV,
      100, 0, 10., false, extra_flag, extra_weight);
  if (m_do_Nminus1)
    elb->add_fill_hist_sys("Dmeson_m_min_trackPt", suffixes,
                           m_DMesons_m->at(index) * Charm::GeV, 1000, 1.4, 2.4,
                           false, extra_flag, extra_weight);
  elb->add_fill_hist_sys("Dmeson_max_trackdR", suffixes,
                         D_meson_max_track_dR(index), 100, 0, 1.2, false,
                         extra_flag, extra_weight);
  if (m_do_Nminus1)
    elb->add_fill_hist_sys("Dmeson_m_max_trackdR", suffixes,
                           m_DMesons_m->at(index) * Charm::GeV, 1000, 1.4, 2.4,
                           false, extra_flag, extra_weight);
  elb->add_fill_hist_sys("Dmeson_impact_sig", suffixes,
                         m_DMesons_fitOutput__ImpactSignificance->at(index),
                         100, 0, 10, false, extra_flag, extra_weight);
  if (m_do_Nminus1)
    elb->add_fill_hist_sys("Dmeson_m_impact_sig", suffixes,
                           m_DMesons_m->at(index) * Charm::GeV, 1000, 1.4, 2.4,
                           false, extra_flag, extra_weight);
  elb->add_fill_hist_sys("Dmeson_Lxy", suffixes,
                         m_DMesons_fitOutput__Lxy->at(index), 300, 0, 30.,
                         false, extra_flag, extra_weight);
  if (m_do_Nminus1)
    elb->add_fill_hist_sys("Dmeson_m_Lxy", suffixes,
                           m_DMesons_m->at(index) * Charm::GeV, 1000, 1.4, 2.4,
                           false, extra_flag, extra_weight);
  elb->add_fill_hist_sys("Dmeson_d0", suffixes,
                         m_DMesons_fitOutput__Impact->at(index) * Charm::mum,
                         200, -1100, 1100, false, extra_flag, extra_weight);
  if (m_do_Nminus1)
    elb->add_fill_hist_sys("Dmeson_m_d0", suffixes,
                           m_DMesons_m->at(index) * Charm::GeV, 1000, 1.4, 2.4,
                           false, extra_flag, extra_weight);
  elb->add_fill_hist_sys("Dmeson_cosThetaStar", suffixes,
                         m_DMesons_costhetastar->at(index), 100, -1., 1., false,
                         extra_flag, extra_weight);
  if (m_do_Nminus1)
    elb->add_fill_hist_sys("Dmeson_m_cosThetaStar", suffixes,
                           m_DMesons_m->at(index) * Charm::GeV, 1000, 1.4, 2.4,
                           false, extra_flag, extra_weight);
  elb->add_fill_hist_sys("Dmeson_PhiMass", suffixes, m_DMesons_mPhi1->at(index),
                         100, PHI_MASS - 10., PHI_MASS + 10., false, extra_flag,
                         extra_weight);
  if (m_do_Nminus1)
    elb->add_fill_hist_sys("Dmeson_m_PhiMass", suffixes,
                           m_DMesons_m->at(index) * Charm::GeV, 1000, 1.4, 2.4,
                           false, extra_flag, extra_weight);
  elb->add_fill_hist_sys("Dmeson_dRlep", suffixes, D_meson_lepton_dR(index),
                         100, 0., 6., false, extra_flag, extra_weight);
  if (m_do_Nminus1)
    elb->add_fill_hist_sys("Dmeson_m_dRlep", suffixes,
                           m_DMesons_m->at(index) * Charm::GeV, 1000, 1.4, 2.4,
                           false, extra_flag, extra_weight);
  elb->add_fill_hist_sys(
      "Dmeson_ptcone40_pt", suffixes,
      m_DMesons_ptcone40->at(index) / m_DMesons_pt->at(index), 100, 0, 10,
      false, extra_flag, extra_weight);
  if (m_do_Nminus1)
    elb->add_fill_hist_sys("Dmeson_m_ptcone40_pt", suffixes,
                           m_DMesons_m->at(index) * Charm::GeV, 1000, 1.4, 2.4,
                           false, extra_flag, extra_weight);

  if ((m_bin_in_D_pt || m_bin_in_lep_eta) && !diff_bins_override) {
    fill_D_s_histograms(elb, channels, index, extra_flag, extra_weight, true);
  }

  return true;
}

void DMesonRec::connect_D_meson_branches(EventLoopBase *elb) {
  std::string sys_name = elb->m_track_sys_name.empty() ? "NOSYS" : elb->m_track_sys_name;
  
  elb->add_presel_br("DMesons_costhetastar_" + sys_name, &m_DMesons_costhetastar);
  elb->add_presel_br("DMesons_DeltaMass_" + sys_name, &m_DMesons_DeltaMass);
  elb->add_presel_br("DMesons_fitOutput__Chi2_" + sys_name, &m_DMesons_fitOutput__Chi2);
  elb->add_presel_br("DMesons_fitOutput__Impact_" + sys_name, &m_DMesons_fitOutput__Impact);
  elb->add_presel_br("DMesons_fitOutput__ImpactZ0SinTheta_" + sys_name, &m_DMesons_fitOutput__ImpactZ0SinTheta);
  elb->add_presel_br("DMesons_fitOutput__ImpactSignificance_" + sys_name, &m_DMesons_fitOutput__ImpactSignificance);
  elb->add_presel_br("DMesons_fitOutput__Lxy_" + sys_name, &m_DMesons_fitOutput__Lxy);
  elb->add_presel_br("DMesons_mKpi1_" + sys_name, &m_DMesons_mKpi1);
  elb->add_presel_br("DMesons_mKpi2_" + sys_name, &m_DMesons_mKpi2);
  elb->add_presel_br("DMesons_mPhi1_" + sys_name, &m_DMesons_mPhi1);
  elb->add_presel_br("DMesons_mPhi2_" + sys_name, &m_DMesons_mPhi2);
  elb->add_presel_br("DMesons_ptcone40_" + sys_name, &m_DMesons_ptcone40);
  elb->add_presel_br("DMesons_SlowPionD0_" + sys_name, &m_DMesons_SlowPionD0);
  elb->add_presel_br("DMesons_SlowPionZ0SinTheta_" + sys_name, &m_DMesons_SlowPionZ0SinTheta);
  elb->add_presel_br("DMesons_eta_" + sys_name, &m_DMesons_eta);
  elb->add_presel_br("DMesons_m_" + sys_name, &m_DMesons_m);
  elb->add_presel_br("DMesons_phi_" + sys_name, &m_DMesons_phi);
  elb->add_presel_br("DMesons_pt_" + sys_name, &m_DMesons_pt);
  elb->add_presel_br("DMesons_D0Index_" + sys_name, &m_DMesons_D0Index);
  elb->add_presel_br("DMesons_decayType_" + sys_name, &m_DMesons_decayType);
  elb->add_presel_br("DMesons_fitOutput__Charge_" + sys_name, &m_DMesons_fitOutput__Charge);
  elb->add_presel_br("DMesons_pdgId_" + sys_name, &m_DMesons_pdgId);
  elb->add_presel_br("DMesons_truthBarcode_" + sys_name, &m_DMesons_truthBarcode);
  elb->add_presel_br("DMesons_daughterInfo__passTight_" + sys_name, &m_DMesons_daughterInfo__passTight);
  elb->add_presel_br("DMesons_daughterInfo__eta_" + sys_name, &m_DMesons_daughterInfo__eta);
  elb->add_presel_br("DMesons_daughterInfo__phi_" + sys_name, &m_DMesons_daughterInfo__phi);
  elb->add_presel_br("DMesons_daughterInfo__pt_" + sys_name, &m_DMesons_daughterInfo__pt);
  elb->add_presel_br("DMesons_daughterInfo__z0SinTheta_" + sys_name, &m_DMesons_daughterInfo__z0SinTheta);
  elb->add_presel_br("DMesons_daughterInfo__z0SinThetaPV_" + sys_name, &m_DMesons_daughterInfo__z0SinThetaPV);
  elb->add_presel_br("DMesons_fitOutput__VertexPosition_" + sys_name, &m_DMesons_fitOutput__VertexPosition);
  elb->add_presel_br("DMesons_daughterInfo__pdgId_" + sys_name, &m_DMesons_daughterInfo__pdgId);
  elb->add_presel_br("DMesons_daughterInfo__truthBarcode_" + sys_name, &m_DMesons_daughterInfo__truthBarcode);
  elb->add_presel_br("DMesons_daughterInfo__truthDBarcode_" + sys_name, &m_DMesons_daughterInfo__truthDBarcode);
  elb->add_presel_br("DMesons_daughterInfo__trackId_" + sys_name, &m_DMesons_daughterInfo__trackId);
}


DMesonRec::DMesonRec() {}

}  // namespace Charm
