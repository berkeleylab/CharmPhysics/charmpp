#ifndef D_MESON_REC_H
#define D_MESON_REC_H

#include "EventLoopBase.h"
#include "HelperFunctions.h"
#include "TruthInfo.h"

namespace Charm {
// Forward declaration
class EventLoopBase;
class TruthInfo;

class DMesonRec {
 private:
  friend class TruthInfo;

 public:
  enum class DecayType {
    Dplus = 4,
    Dstar = 1,
    Dstar_pi0 = 2,
    Ds = 5,
    LambdaC = 11
  };
  DMesonRec();

  bool fill_D_plus_histograms(
      EventLoopBase *elb, std::vector<std::string> channels, unsigned int index,
      std::vector<bool> extra_flag = {}, std::vector<double> extra_weight = {},
      bool diff_bins_override = false, bool track_jet_selection = false);

  bool fill_D_star_histograms(EventLoopBase *elb,
                              std::vector<std::string> channels,
                              unsigned int index,
                              std::vector<bool> extra_flag = {},
                              std::vector<double> extra_weight = {},
                              bool diff_bins_override = false,
                              bool track_jet_selection = false);

  bool fill_D_star_pi0_histograms(EventLoopBase *elb,
                                  std::vector<std::string> channels,
                                  unsigned int index,
                                  std::vector<bool> extra_flag = {},
                                  std::vector<double> extra_weight = {},
                                  bool diff_bins_override = false);

  bool fill_D_s_histograms(EventLoopBase *elb,
                           std::vector<std::string> channels,
                           unsigned int index,
                           std::vector<bool> extra_flag = {},
                           std::vector<double> extra_weight = {},
                           bool diff_bins_override = false);

  void fill_D_plus_tree(unsigned int index, bool extended_branches = false, bool is_mc = false);

  void fill_track_jet_tree(SysTrackJet *track_jet_container,
                           std::string track_jet_radius);

  void connect_D_meson_branches(EventLoopBase *elb);

  void connect_output_tree_branches(EventLoopBase *elb, bool is_mc = false,
                                    bool track_jet_selection = false,
                                    bool extended_branches = false,
                                    std::string track_jet_radius = "10");

  std::string get_charge_prefix(float lep_charge, unsigned int index);

  std::pair<unsigned int, int> get_best_D_candidate(
      std::vector<std::pair<unsigned int, int>> candidates);

  void set_lepton(TLorentzVector *lep) { m_lep = lep; };

  float D_meson_lepton_dR(unsigned int index, float lep_eta, float lep_phi);

 private:
  float D_meson_min_track_pt(unsigned int index);
  float D_meson_max_track_dR(unsigned int index);
  float D_meson_lepton_dR(unsigned int index);
  float soft_pion_Dstar_separation(unsigned int index, unsigned int D0Index);
  std::string D_meson_pt_bin(EventLoopBase *elb, unsigned int index);
  std::string D_meson_eta_bin(EventLoopBase *elb, int i);
  bool D_pass_tight(unsigned int index);

 protected:
  bool m_D_tightPrimary_tracks;
  bool m_do_Nminus1;
  bool m_do_twice_failed;
  bool m_bin_in_D_pt;
  bool m_bin_in_lep_eta;
  bool m_bin_in_track_jet_pt;
  bool m_dplus_isolation{true};
  bool m_dmeson_jet_matching{false};

 private:
  std::set<std::string> m_created_channels;
  TLorentzVector *m_lep;

 protected:
  void get_D_presel(EventLoopBase *elb, DecayType decayType);
  void get_D_plus_mesons(EventLoopBase *elb, bool loose_selection = false,
                         bool no_selection = false, bool frag_selection = false);
  void get_D_star_mesons(EventLoopBase *elb, bool loose_selection = false,
                         bool no_selection = false, bool frag_selection = false);
  void get_D_star_pi0_mesons(EventLoopBase *elb, bool loose_selection = false);
  void get_D_s_mesons(EventLoopBase *elb, bool loose_selection = false);
  void get_Lambda_C_baryons(EventLoopBase *elb, bool loose_selection = false);

 protected:
  std::vector<unsigned int> m_presel_Dplus;
  std::vector<unsigned int> m_presel_Dstar;
  std::vector<unsigned int> m_presel_Dstar_pi0;
  std::vector<unsigned int> m_presel_Ds;
  std::vector<unsigned int> m_presel_LambdaCs;
  std::vector<unsigned int> m_D_plus_indices;
  std::vector<unsigned int> m_D_star_indices;
  std::vector<unsigned int> m_D_star_pi0_indices;
  std::vector<unsigned int> m_D_s_indices;
  std::vector<unsigned int> m_Lambda_C_indices;

 protected:
  std::vector<Float_t> *m_DMesons_costhetastar{};
  std::vector<Float_t> *m_DMesons_DeltaMass{};
  std::vector<Float_t> *m_DMesons_fitOutput__Chi2{};
  std::vector<Float_t> *m_DMesons_fitOutput__Impact{};
  std::vector<Float_t> *m_DMesons_fitOutput__ImpactError{};
  std::vector<Float_t> *m_DMesons_fitOutput__ImpactZ0{};
  std::vector<Float_t> *m_DMesons_fitOutput__ImpactZ0Error{};
  std::vector<Float_t> *m_DMesons_fitOutput__ImpactTheta{};
  std::vector<Float_t> *m_DMesons_fitOutput__ImpactZ0SinTheta{};
  std::vector<Float_t> *m_DMesons_fitOutput__ImpactSignificance{};
  std::vector<Float_t> *m_DMesons_fitOutput__Lxy{};
  std::vector<Float_t> *m_DMesons_mKpi1{};
  std::vector<Float_t> *m_DMesons_mKpi2{};
  std::vector<Float_t> *m_DMesons_mPhi1{};
  std::vector<Float_t> *m_DMesons_mPhi2{};
  std::vector<Float_t> *m_DMesons_ptcone40{};
  std::vector<Float_t> *m_DMesons_SlowPionD0{};
  std::vector<Float_t> *m_DMesons_SlowPionZ0SinTheta{};
  std::vector<Float_t> *m_DMesons_eta{};
  std::vector<Float_t> *m_DMesons_m{};
  std::vector<Float_t> *m_DMesons_phi{};
  std::vector<Float_t> *m_DMesons_pt{};
  std::vector<Int_t> *m_DMesons_D0Index{};
  std::vector<Int_t> *m_DMesons_decayType{};
  std::vector<Int_t> *m_DMesons_fitOutput__Charge{};
  std::vector<Int_t> *m_DMesons_pdgId{};
  std::vector<Int_t> *m_DMesons_truthBarcode{};
  std::vector<std::vector<bool>> *m_DMesons_daughterInfo__passTight{};
  std::vector<std::vector<Float_t>> *m_DMesons_daughterInfo__eta{};
  std::vector<std::vector<Float_t>> *m_DMesons_daughterInfo__phi{};
  std::vector<std::vector<Float_t>> *m_DMesons_daughterInfo__pt{};
  std::vector<std::vector<Float_t>> *m_DMesons_daughterInfo__z0SinTheta{};
  std::vector<std::vector<Float_t>> *m_DMesons_daughterInfo__z0SinThetaPV{};
  std::vector<std::vector<Float_t>> *m_DMesons_fitOutput__VertexPosition{};
  std::vector<std::vector<Float_t>> *m_DMesons_daughterInfo__pdgId{};
  std::vector<std::vector<Int_t>> *m_DMesons_daughterInfo__truthDBarcode{};
  std::vector<std::vector<Int_t>> *m_DMesons_daughterInfo__truthBarcode{};
  std::vector<std::vector<Float_t>> *m_DMesons_daughterInfo__trackId{};

  Float_t m_out_Dmeson_m{};
  Float_t m_out_Dmeson_mdiff{};
  Float_t m_out_Dmeson_mdzero{};
  Float_t m_out_Dmeson_pt{};
  Float_t m_out_Dmeson_eta{};
  Float_t m_out_Dmeson_phi{};
  Float_t m_out_Dmeson_Chi2{};
  Float_t m_out_Dmeson_Impact{};
  Float_t m_out_Dmeson_ImpactZ0SinTheta{};
  Float_t m_out_Dmeson_ImpactSignificance{};
  Float_t m_out_Dmeson_Lxy{};
  Float_t m_out_Dmeson_costhetastar{};
  Float_t m_out_Dmeson_maxdR{};
  Float_t m_out_Dmeson_mKpi1{};
  Float_t m_out_Dmeson_mKpi2{};
  Float_t m_out_Dmeson_ptcone40{};
  Float_t m_out_Dmeson_soft_pion_dr{};
  Float_t m_out_Dmeson_soft_pion_d0{};
  Float_t m_out_truth_Dmeson_m{};
  Float_t m_out_truth_Dmeson_pt{};
  Float_t m_out_truth_Dmeson_eta{};
  Float_t m_out_truth_Dmeson_phi{};
  Int_t m_out_truth_Dmeson_pdgId{};
  std::map<std::string, Float_t> m_out_Dmeson_track_jet_pt{};
  std::map<std::string, Float_t> m_out_Dmeson_track_jet_eta{};
  std::map<std::string, Float_t> m_out_Dmeson_track_jet_phi{};
  std::map<std::string, Float_t> m_out_Dmeson_track_jet_zt{};
  std::map<std::string, Float_t> m_out_Dmeson_track_jet_zl{};
  std::map<std::string, Float_t> m_out_truth_Dmeson_track_jet_pt{};
  std::map<std::string, Float_t> m_out_truth_Dmeson_track_jet_eta{};
  std::map<std::string, Float_t> m_out_truth_Dmeson_track_jet_phi{};
  std::map<std::string, Float_t> m_out_truth_Dmeson_track_jet_zt{};
  std::map<std::string, Float_t> m_out_truth_Dmeson_track_jet_zl{};
  std::map<unsigned int, Float_t> m_out_Dmeson_trk_pt{};
  std::map<unsigned int, Float_t> m_out_Dmeson_trk_eta{};
  std::map<unsigned int, Float_t> m_out_Dmeson_trk_phi{};
  std::map<unsigned int, Float_t> m_out_Dmeson_trk_z0SinThetaPV{};
  std::map<unsigned int, Int_t> m_out_Dmeson_trk_id{};

 protected:
  std::vector<Float_t> m_sys_dmeson_mass;
};

}  // namespace Charm

#endif  // D_MESON_REC_H
