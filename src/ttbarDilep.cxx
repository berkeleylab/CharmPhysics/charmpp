#include "ttbarDilep.h"

#include <math.h>

#include <iostream>

#include "TRegexp.h"
#include "TVector2.h"

namespace Charm {

void ttbarDilep::initialize() {
  std::cout << "ttbarDilep::initiaize" << std::endl;

  // systematics
  if (m_do_systematics) {
    // GEN
    m_sys_br_GEN.init(get_sys("systematics_GEN"));

    // PRW
    m_sys_br_PU.init(get_sys("systematics_PRW"));

    // JVT
    m_sys_br_JVT.init(get_sys("systematics_EL_Calib"));
    m_sys_br_JVT.init(get_sys("systematics_JET_Calib"));
    m_sys_br_JVT.init(get_sys("systematics_JET_Jvt"));
    m_sys_br_JVT.init(get_sys("systematics_MUON_Calib"));

    // FTAG
    m_sys_br_FTAG.init(get_sys("systematics_FT_EFF"));
    m_sys_br_FTAG.init(get_sys("systematics_EL_Calib"));
    m_sys_br_FTAG.init(get_sys("systematics_MUON_Calib"));
    m_sys_br_FTAG.init(get_sys("systematics_JET_Calib"));

    // MET
    m_sys_br_MET_met.init(get_sys("systematics_EL_Calib"));
    m_sys_br_MET_met.init(get_sys("systematics_JET_Calib"));
    m_sys_br_MET_met.init(get_sys("systematics_MET_Calib"));
    m_sys_br_MET_met.init(get_sys("systematics_MUON_Calib"));

    // electron calib
    m_sys_br_el_isIsolated_FCTight.init(get_sys("systematics_EL_Calib"));
    m_sys_br_el_pt.init(get_sys("systematics_EL_Calib"));
    m_sys_br_el_selected_other_sys.init(get_sys("systematics_JET_Calib"));
    m_sys_br_el_selected_other_sys.init(get_sys("systematics_MUON_Calib"));
    m_sys_br_el_selected.init(get_sys("systematics_EL_Calib"));
    m_sys_br_el_calib_reco_eff.init(get_sys("systematics_EL_Calib"));
    m_sys_br_el_calib_id_eff.init(get_sys("systematics_EL_Calib"));
    m_sys_br_el_calib_iso_eff.init(get_sys("systematics_EL_Calib"));
    m_sys_br_el_calib_trig_eff.init(get_sys("systematics_EL_Calib"));
    m_sys_br_el_calib_trig_sf.init(get_sys("systematics_EL_Calib"));

    // muon calib
    m_sys_br_mu_isQuality_Tight.init(get_sys("systematics_MUON_Calib"));
    m_sys_br_mu_pt.init(get_sys("systematics_MUON_Calib"));
    m_sys_br_mu_selected_other_sys.init(get_sys("systematics_EL_Calib"));
    m_sys_br_mu_selected_other_sys.init(get_sys("systematics_JET_Calib"));
    m_sys_br_mu_selected.init(get_sys("systematics_MUON_Calib"));
    m_sys_br_mu_isIsolated_PflowTight_VarRad.init(
        get_sys("systematics_MUON_Calib"));
    m_sys_br_mu_calib_quality_eff.init(get_sys("systematics_MUON_Calib"));
    m_sys_br_mu_calib_ttva_eff.init(get_sys("systematics_MUON_Calib"));
    m_sys_br_mu_calib_trig_2015_eff_data.init(
        get_sys("systematics_MUON_Calib"));
    m_sys_br_mu_calib_iso_eff.init(get_sys("systematics_MUON_Calib"));
    m_sys_br_mu_calib_trig_2015_eff_mc.init(get_sys("systematics_MUON_Calib"));
    m_sys_br_mu_calib_trig_2018_eff_data.init(
        get_sys("systematics_MUON_Calib"));
    m_sys_br_mu_calib_trig_2018_eff_mc.init(get_sys("systematics_MUON_Calib"));

    // el weights
    m_sys_br_el_reco_eff.init(get_sys("systematics_EL_EFF_Reco"));
    m_sys_br_el_id_eff.init(get_sys("systematics_EL_EFF_ID"));
    m_sys_br_el_iso_eff.init(get_sys("systematics_EL_EFF_Iso"));
    m_sys_br_el_trig_eff.init(get_sys("systematics_EL_EFF_Trigger"));
    m_sys_br_el_trig_sf.init(get_sys("systematics_EL_EFF_Trigger"));

    // mu weights
    m_sys_br_mu_quality_eff.init(get_sys("systematics_MUON_EFF_RECO"));
    m_sys_br_mu_ttva_eff.init(get_sys("systematics_MUON_EFF_TTVA"));
    m_sys_br_mu_trig_2015_eff_data.init(get_sys("systematics_MUON_EFF_Trig"));
    m_sys_br_mu_iso_eff.init(get_sys("systematics_MUON_EFF_ISO"));
    m_sys_br_mu_trig_2015_eff_mc.init(get_sys("systematics_MUON_EFF_Trig"));
    m_sys_br_mu_trig_2018_eff_data.init(get_sys("systematics_MUON_EFF_Trig"));
    m_sys_br_mu_trig_2018_eff_mc.init(get_sys("systematics_MUON_EFF_Trig"));

    // jet calib
    m_sys_br_jet_pt.init(get_sys("systematics_JET_Calib"));
    m_sys_br_jet_selected.init(get_sys("systematics_JET_Calib"));
    m_sys_br_jet_selected_other_sys.init(get_sys("systematics_EL_Calib"));
    m_sys_br_jet_selected_other_sys.init(get_sys("systematics_MUON_Calib"));
  }
}

void ttbarDilep::read_sys_br() {
  // read electrons
  m_sys_el_iso = get_val_sys(m_AnalysisElectrons_isIsolated_Tight_VarRad_NOSYS,
                             &m_sys_br_el_isIsolated_FCTight);
  m_sys_el_pt = get_val_sys(m_AnalysisElectrons_pt_NOSYS, &m_sys_br_el_pt);
  m_sys_el_selected =
      get_val_sys(m_AnalysisElectrons_el_selected_NOSYS, &m_sys_br_el_selected);
  m_sys_el_selected_other = get_val_sys(m_AnalysisElectrons_el_selected_NOSYS,
                                        &m_sys_br_el_selected_other_sys);
  m_sys_el_reco_eff =
      get_val_sys(m_AnalysisElectrons_effSF_NOSYS, &m_sys_br_el_reco_eff,
                  &m_sys_br_el_calib_reco_eff);
  m_sys_el_id_eff = get_val_sys(m_AnalysisElectrons_effSF_ID_Tight_NOSYS,
                                &m_sys_br_el_id_eff, &m_sys_br_el_calib_id_eff);
  m_sys_el_iso_eff =
      get_val_sys(m_AnalysisElectrons_effSF_Isol_Tight_Tight_VarRad_NOSYS,
                  &m_sys_br_el_iso_eff, &m_sys_br_el_calib_iso_eff);
  m_sys_el_trig_sf = get_val_sys(
      m_AnalysisElectrons_effSF_Trig_Tight_Tight_VarRad_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_NOSYS,
      &m_sys_br_el_trig_sf, &m_sys_br_el_calib_trig_sf);
  m_sys_el_trig_eff = get_val_sys(
      m_AnalysisElectrons_effSF_Trig_Tight_Tight_VarRad_Eff_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_NOSYS,
      &m_sys_br_el_trig_eff, &m_sys_br_el_calib_trig_eff);

  // read muons
  m_sys_mu_pt = get_val_sys(m_AnalysisMuons_pt_NOSYS, &m_sys_br_mu_pt);
  m_sys_mu_quality = get_val_sys(m_AnalysisMuons_isQuality_Tight_NOSYS,
                                 &m_sys_br_mu_isQuality_Tight);
  m_sys_mu_selected =
      get_val_sys(m_AnalysisMuons_mu_selected_NOSYS, &m_sys_br_mu_selected);
  m_sys_mu_selected_other = get_val_sys(m_AnalysisMuons_mu_selected_NOSYS,
                                        &m_sys_br_mu_selected_other_sys);
  m_sys_mu_iso = get_val_sys(m_AnalysisMuons_isIsolated_PflowTight_VarRad_NOSYS,
                             &m_sys_br_mu_isIsolated_PflowTight_VarRad);
  m_sys_mu_quality_eff =
      get_val_sys(m_AnalysisMuons_muon_effSF_Quality_Tight_NOSYS,
                  &m_sys_br_mu_quality_eff, &m_sys_br_mu_calib_quality_eff);
  m_sys_mu_ttva_eff =
      get_val_sys(m_AnalysisMuons_muon_effSF_TTVA_NOSYS, &m_sys_br_mu_ttva_eff,
                  &m_sys_br_mu_calib_ttva_eff);
  m_sys_mu_iso_eff =
      get_val_sys(m_AnalysisMuons_muon_effSF_Isol_PflowTight_VarRad_NOSYS,
                  &m_sys_br_mu_iso_eff, &m_sys_br_mu_calib_iso_eff);
  m_sys_mu_trig_2015_eff_mc = get_val_sys(
      m_AnalysisMuons_muon_effMC_Trig_Tight_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_NOSYS,
      &m_sys_br_mu_trig_2015_eff_mc, &m_sys_br_mu_calib_trig_2015_eff_mc);
  m_sys_mu_trig_2015_eff_data = get_val_sys(
      m_AnalysisMuons_muon_effData_Trig_Tight_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_NOSYS,
      &m_sys_br_mu_trig_2015_eff_data, &m_sys_br_mu_calib_trig_2015_eff_data);
  m_sys_mu_trig_2018_eff_mc = get_val_sys(
      m_AnalysisMuons_muon_effMC_Trig_Tight_HLT_mu26_ivarmedium_OR_HLT_mu50_NOSYS,
      &m_sys_br_mu_trig_2018_eff_mc, &m_sys_br_mu_calib_trig_2018_eff_mc);
  m_sys_mu_trig_2018_eff_data = get_val_sys(
      m_AnalysisMuons_muon_effData_Trig_Tight_HLT_mu26_ivarmedium_OR_HLT_mu50_NOSYS,
      &m_sys_br_mu_trig_2018_eff_data, &m_sys_br_mu_calib_trig_2018_eff_data);

  // read jets
  m_sys_jet_pt = get_val_sys(m_AnalysisJets_pt_NOSYS, &m_sys_br_jet_pt);
  m_sys_jet_selected =
      get_val_sys(m_AnalysisJets_jet_selected_NOSYS, &m_sys_br_jet_selected,
                  &m_sys_br_jet_selected_other_sys);
}

void ttbarDilep::get_tight_electrons() {
  m_electrons_sys.clear();
  for (unsigned int j = 0; j < m_nSyst; j++) {
    m_electrons_sys.push_back({});
    for (unsigned int i = 0; i < m_AnalysisElectrons_pt_NOSYS->size(); i++) {
      // nominal
      if ((m_sys_el_pt.at(j)->at(i) * Charm::GeV >= 30.) &&
                (m_sys_el_selected.at(j)->at(i)) &&
                (m_sys_el_selected_other.at(j)->at(i)) &&
                (m_sys_el_iso.at(j)->at(i)) &&
                (m_AnalysisElectrons_likelihood_Tight->at(i))/* &&
                ((m_is_mc && m_truth_match && el_is_prompt(i)) || (m_is_mc && !m_truth_match) || (!m_is_mc))*/)
            {
        // TODO: fix ntuples to have different SF for calibration sys!
        float sf = 1.;
        if (m_is_mc) {
          sf *= m_sys_el_reco_eff.at(j)->at(i) * m_sys_el_id_eff.at(j)->at(i) *
                m_sys_el_iso_eff.at(j)->at(i);
        }

        std::pair<unsigned int, float> pair(i, sf);
        m_electrons_sys.at(j).push_back(pair);
      }
    }
  }
}

void ttbarDilep::get_tight_muons() {
  m_muons_sys.clear();
  for (unsigned int j = 0; j < m_nSyst; j++) {
    m_muons_sys.push_back({});
    for (unsigned int i = 0; i < m_AnalysisMuons_pt_NOSYS->size(); i++) {
      // nominal
      if (
                // (m_sys_mu_pt.at(j)->at(i) * Charm::GeV >= 30.) &&
                (m_sys_mu_selected.at(j)->at(i)) &&
                (m_sys_mu_selected_other.at(j)->at(i)) &&
                (m_sys_mu_quality.at(j)->at(i)) &&
                (m_sys_mu_iso.at(j)->at(i))/* &&
                ((m_is_mc && m_truth_match && mu_is_prompt(i)) || (m_is_mc && !m_truth_match) || (!m_is_mc))*/)
            {
        // TODO: fix ntuples to have different SF for calibration sys!
        float sf = 1.;
        if (m_is_mc) {
          sf *= m_sys_mu_quality_eff.at(j)->at(i) *
                m_sys_mu_ttva_eff.at(j)->at(i) * m_sys_mu_iso_eff.at(j)->at(i);
        }

        std::pair<unsigned int, float> pair(i, sf);
        m_muons_sys.at(j).push_back(pair);
      }
    }
  }
}

void ttbarDilep::get_jets() {
  m_jets_sys.clear();
  for (unsigned int j = 0; j < m_nSyst; j++) {
    m_jets_sys.push_back({});
    for (unsigned int i = 0; i < m_AnalysisJets_pt_NOSYS->size(); i++) {
      // nominal
      // if (((fabs(m_AnalysisJets_eta->at(i)) < 2.5 &&
      // (m_sys_jet_pt.at(j)->at(i) * Charm::GeV >= 25.)) ||
      //      (fabs(m_AnalysisJets_eta->at(i)) >= 2.5 &&
      //      fabs(m_AnalysisJets_eta->at(i)) < 5.0 &&
      //      (m_sys_jet_pt.at(j)->at(i) * Charm::GeV >= 30.))) &&
      //     (m_sys_jet_selected.at(j)->at(i)))
      // {
      if (((fabs(m_AnalysisJets_eta->at(i)) < 2.4 &&
            (m_sys_jet_pt.at(j)->at(i) * Charm::GeV >= 20.)) ||
           (fabs(m_AnalysisJets_eta->at(i)) >= 2.4 &&
            fabs(m_AnalysisJets_eta->at(i)) <= 5.0 &&
            (m_sys_jet_pt.at(j)->at(i) * Charm::GeV >= 20.))) &&
          (m_sys_jet_selected.at(j)->at(i))) {
        m_jets_sys.at(j).push_back(i);
      }
    }
  }
}

void ttbarDilep::jet_selection_for_sys(unsigned int i) {
  auto jets = m_jets_sys.at(i);
  for (unsigned int j = 0; j < jets.size(); j++) {
    m_sys_njets.at(i) += 1;
    if (m_AnalysisJets_ftag_select_DL1r_FixedCutBEff_70->at(j)) {
      m_sys_nbjets.at(i) += 1;
    }
  }
}

bool ttbarDilep::pass_el_trigger() {
  if (m_EventInfo_runNumber < 290000) {
    return m_EventInfo_trigPassed_HLT_e24_lhmedium_L1EM20VH ||
           m_EventInfo_trigPassed_HLT_e60_lhmedium ||
           m_EventInfo_trigPassed_HLT_e120_lhloose;
  } else {
    return m_EventInfo_trigPassed_HLT_e26_lhtight_nod0_ivarloose ||
           m_EventInfo_trigPassed_HLT_e60_lhmedium_nod0 ||
           m_EventInfo_trigPassed_HLT_e140_lhloose_nod0;
  }
}

bool ttbarDilep::el_is_trigger_matched(unsigned int i) {
  if (m_EventInfo_runNumber < 290000) {
    return ((m_EventInfo_trigPassed_HLT_e24_lhmedium_L1EM20VH &&
             m_AnalysisElectrons_matched_HLT_e24_lhmedium_L1EM20VH->at(i)) ||
            (m_EventInfo_trigPassed_HLT_e60_lhmedium &&
             m_AnalysisElectrons_matched_HLT_e60_lhmedium->at(i)) ||
            (m_EventInfo_trigPassed_HLT_e120_lhloose &&
             m_AnalysisElectrons_matched_HLT_e120_lhloose->at(i)));
  } else {
    return (
        (m_EventInfo_trigPassed_HLT_e26_lhtight_nod0_ivarloose &&
         m_AnalysisElectrons_matched_HLT_e26_lhtight_nod0_ivarloose->at(i)) ||
        (m_EventInfo_trigPassed_HLT_e60_lhmedium_nod0 &&
         m_AnalysisElectrons_matched_HLT_e60_lhmedium_nod0->at(i)) ||
        (m_EventInfo_trigPassed_HLT_e140_lhloose_nod0 &&
         m_AnalysisElectrons_matched_HLT_e140_lhloose_nod0->at(i)));
  }
}

float ttbarDilep::get_el_trigger_weight(unsigned int sys_index,
                                        unsigned int i) {
  float PMC = m_sys_el_trig_eff.at(sys_index)->at(i);
  float PData = PMC * m_sys_el_trig_sf.at(sys_index)->at(i);

  if (PMC > 0) {
    return PData / PMC;
  } else {
    return 0;
  }
}

bool ttbarDilep::el_is_prompt(unsigned int i) {
  // prompt
  if (m_AnalysisElectrons_truthType->at(i) == 2) return true;
  // conversions
  else if (m_AnalysisElectrons_truthType->at(i) == 4 &&
           fabs(m_AnalysisElectrons_firstEgMotherPdgId->at(i)) == 11) {
    if (m_AnalysisElectrons_truthOrigin->at(i) == 5)
      return true;
    else if (m_AnalysisElectrons_truthOrigin->at(i) == 7 &&
             m_AnalysisElectrons_firstEgMotherTruthType->at(i) == 2 &&
             Charm::is_in(m_AnalysisElectrons_firstEgMotherTruthOrigin->at(i),
                          {10, 12, 13, 14, 43}))
      return true;
  }
  // fsr
  else if (m_AnalysisElectrons_truthType->at(i) == 4 &&
           Charm::is_in(m_AnalysisElectrons_truthOrigin->at(i), {5, 7}) &&
           m_AnalysisElectrons_firstEgMotherTruthOrigin->at(i) == 40)
    return true;
  else if (m_AnalysisElectrons_truthType->at(i) == 15 &&
           m_AnalysisElectrons_truthOrigin->at(i) == 40)
    return true;

  // end
  return false;
}

bool ttbarDilep::mu_is_prompt(unsigned int i) {
  return (
      (m_AnalysisMuons_truthType->at(i) == 6) &&
      Charm::is_in(m_AnalysisMuons_truthOrigin->at(i), {10, 12, 13, 14, 43}));
}

bool ttbarDilep::preselection() {
  set_channel("inclusive");
  increment_cutflow(m_channel, 1);

  // sys br for leptons
  read_sys_br();

  // electron selection
  get_tight_electrons();

  // muon selection
  get_tight_muons();

  // jets
  get_jets();

  // clear all before running over systematics
  m_sys_el_hist_pt = std::vector<float>(m_nSyst, 0.);
  m_sys_el_hist_eta = std::vector<float>(m_nSyst, 0.);
  m_sys_el_hist_phi = std::vector<float>(m_nSyst, 0.);
  m_sys_mu_hist_pt = std::vector<float>(m_nSyst, 0.);
  m_sys_mu_hist_eta = std::vector<float>(m_nSyst, 0.);
  m_sys_mu_hist_phi = std::vector<float>(m_nSyst, 0.);
  m_sys_njets = std::vector<float>(m_nSyst, 0.);
  m_sys_nbjets = std::vector<float>(m_nSyst, 0.);
  bool event_pass = false;

  for (unsigned int i = 0; i < m_nSyst; i++) {
    // bad muon veto
    for (unsigned int j = 0; j < m_muons_sys.at(i).size(); j++) {
      if (m_AnalysisMuons_is_bad->at(m_muons_sys.at(i).at(j).first)) {
        continue;
      }
    }

    // jet multiplicity
    jet_selection_for_sys(i);

    bool nominal = i == 0;
    unsigned int sys_index = i - 1;
    auto electrons = m_electrons_sys.at(i);
    auto muons = m_muons_sys.at(i);
    TLorentzVector el;
    TLorentzVector mu;

    std::string channel_string = "dilep";  // everything

    // require exactly one electron and one muon
    if (!(electrons.size() == 1 && muons.size() == 1)) {
      // std::cout << "wrong size" << std::endl;
      return false;
    }

    unsigned int elIndex = electrons.at(0).first;
    unsigned int muIndex = muons.at(0).first;

    // charge
    if (m_AnalysisElectrons_charge->at(elIndex) *
            m_AnalysisMuons_charge->at(muIndex) >
        0) {
      continue;
    }

    // pass electron trigger
    if (!pass_el_trigger()) {
      continue;
    }

    // single electron trigger
    if (!el_is_trigger_matched(elIndex)) {
      continue;
    }

    // jet multiplicity
    m_n_jets = 0;
    m_n_bjets = 0;
    for (unsigned int i = 0;
         i < m_AnalysisJets_ftag_select_DL1r_FixedCutBEff_70->size(); i++) {
      m_n_jets += 1;
      if (m_AnalysisJets_ftag_select_DL1r_FixedCutBEff_70->at(i)) {
        m_n_bjets += 1;
      }
    }

    // require 2 or more bjets
    if (m_n_bjets < 2) {
      continue;
    }

    // efficiency weight
    if (m_is_mc) {
      multiply_event_weight(m_electrons_sys.at(i).at(0).second *
                                m_muons_sys.at(i).at(0).second *
                                get_el_trigger_weight(i, elIndex),
                            i);
    }

    event_pass = true;

    // for debugging
    //  for (int j = 0; j < m_TruthLeptons_pt->size(); j++)
    //  {
    //  std::cout << m_TruthLeptons_pdgId->at(j) << "\t\t\t";
    //  std::cout << m_TruthLeptons_status->at(j) << "\t\t\t";
    //  std::cout << m_TruthLeptons_barcode->at(j) << "\t\t\t\t\t\t\t";
    //  std::cout << m_TruthLeptons_phi->at(j) << "\t\t\t\t\t\t\t";
    //  std::cout << m_TruthLeptons_eta->at(j) << "\t\t\t\t\t\t\t";
    //  std::cout << m_TruthLeptons_classifierParticleOrigin->at(j) <<
    //  "\t\t\t\t\t\t\t\t"; std::cout <<
    //  m_TruthLeptons_classifierParticleType->at(j) << std::endl;
    // }
    // std::cout << std::endl;

    // set channel to dilep
    if (m_split_by_period && channel_string != "") {
      channel_string = get_period_string() + "_" + channel_string;
    }
    m_channel_sys.at(i) = channel_string;

    // fill variables
    m_sys_el_hist_pt.at(i) = m_sys_el_pt.at(i)->at(elIndex) * Charm::GeV;
    m_sys_el_hist_eta.at(i) = m_AnalysisElectrons_eta->at(elIndex);
    m_sys_el_hist_phi.at(i) = m_AnalysisElectrons_phi->at(elIndex);

    m_sys_mu_hist_pt.at(i) = m_sys_mu_pt.at(i)->at(muIndex) * Charm::GeV;
    m_sys_mu_hist_eta.at(i) = m_AnalysisMuons_eta->at(muIndex);
    m_sys_mu_hist_phi.at(i) = m_AnalysisMuons_phi->at(muIndex);
  }

  // for (unsigned int i = 0; i < m_nSyst; i++) {
  //     std::cout << i << " channel " << m_channel_sys.at(i) << std::endl;
  // }

  if (!event_pass) {
    return false;
  }

  // event weight
  if (m_is_mc) {
    // mc event weight, initial sum of weights, x-sec, lumi weight
    multiply_mc_event_weight(m_EventInfo_generatorWeight_NOSYS, &m_sys_br_GEN);

    // jvt weight and systematics
    multiply_nominal_and_sys_weight(m_EventInfo_jvt_effSF_NOSYS, &m_sys_br_JVT);

    // pileup weight and systematics
    multiply_nominal_and_sys_weight(m_EventInfo_PileupWeight_NOSYS,
                                    &m_sys_br_PU);

    // // FTAG systematics
    // multiply_nominal_and_sys_weight(m_AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_NOSYS,
    // &m_sys_br_FTAG); //odd
  }

  m_sys_met = get_val_sys(m_METInfo_MET_NOSYS, &m_sys_br_MET_met, Charm::GeV);

  return true;
}

int ttbarDilep::execute() {
  // fill histograms
  fill_histograms();

  std::string channel_string = "";

  // non-prompt mu background estimation
  for (unsigned int i = 0; i < m_nSyst; i++) {
    unsigned int elIndex = m_electrons_sys.at(i).at(0).first;
    unsigned int muIndex = m_muons_sys.at(i).at(0).first;

    if (m_is_mc) {
      // mu from tau DK background estimation
      // this is written with the assumption that only final state particles
      // that are also emitted by a W decay are kept in truth leptons
      int tauNuID = 0;
      int numTauNeutrinos = 0;

      for (int j = 0; j < m_TruthLeptons_pt->size(); j++) {
        if (abs(m_TruthLeptons_pdgId->at(j)) == 16) {
          tauNuID = m_TruthLeptons_pdgId->at(j);
          numTauNeutrinos++;
        }
      }

      if (numTauNeutrinos == 2)  // two neutrinos from W decay means 2 taus
      {
        channel_string = "dilep_tau_DK";
      } else if (numTauNeutrinos == 1)  // 1 tau neutrino from W DK means 1 tau
      {
        if (m_AnalysisMuons_charge->at(muIndex) * tauNuID >
            0)  // antitau neutrino means tau- means mu-. This means
                // pdgid(-)*muonCharge(-) == + > 0 for muon-branch tau
        {
          channel_string = "dilep_tau_DK";
        } else {
          channel_string = "dilep_prompt_mu";  // corresponds to tau->e
        }
      } else if (!mu_is_prompt(muIndex)) {
        channel_string = "dilep_nonprompt_mu";
      } else {
        channel_string = "dilep_prompt_mu";
      }
    }
    // set channel
    if (m_split_by_period && channel_string != "") {
      channel_string = get_period_string() + "_" + channel_string;
    }
    m_channel_sys.at(i) = channel_string;
  }

  // fill histograms
  fill_histograms();

  return 1;
}

std::string ttbarDilep::get_period_string() {
  return get_period(m_EventInfo_runNumber);
}

void ttbarDilep::fill_histograms() {
  add_fill_hist_sys_fast("el_pt", m_sys_el_hist_pt, 400, 0, 400, true);
  add_fill_hist_sys_fast("el_eta", m_sys_el_hist_eta, 100, -3.0, 3.0, true);
  add_fill_hist_sys_fast("mu_pt", m_sys_mu_hist_pt, 400, 0, 400, true);
  add_fill_hist_sys_fast("mu_eta", m_sys_mu_hist_eta, 100, -3.0, 3.0, true);
  add_fill_hist_sys_fast(
      "pileup", m_EventInfo_correctedScaled_averageInteractionsPerCrossing, 80,
      0, 80, true);
  add_fill_hist_sys_fast("met_met", m_sys_met, 400, 0, 400, true);
  add_fill_hist_sys_fast("njets", m_sys_njets, 20, -0.5, 19.5, true);
  add_fill_hist_sys_fast("nbjets", m_sys_nbjets, 20, -0.5, 19.5, true);
  add_fill_hist_sys_fast("el_phi", m_sys_el_hist_phi, 100, -M_PI, M_PI);
  add_fill_hist_sys_fast("mu_phi", m_sys_mu_hist_phi, 100, -M_PI, M_PI);
  add_fill_hist_sys_fast("lep_dphi", m_lep_dPhi, 100, -M_PI, M_PI);
  add_fill_hist_sys_fast("lep_dR", m_lep_dR, 100, 0, 6);
  add_fill_hist_sys_fast("run_number", m_EventInfo_runNumber, 150, 250000,
                         400000);
  // if (m_do_extra_histograms)
  // {
  //     add_fill_hist_sys_fast("el_d0", m_el_d0 * Charm::mum,
  //                                 400, -200, 200);
  //     add_fill_hist_sys_fast("el_d0_fixed", rescaled_d0(m_el_d0,
  //     m_EventInfo_runNumber, m_is_mc) * Charm::mum,
  //                                 400, -200, 200);
  //     add_fill_hist_sys_fast("el_d0sig", m_el_d0sig,
  //                                 100, -6.0, 6.0);
  //     add_fill_hist_sys_fast("el_d0sig_fixed", m_el_d0sig / m_el_d0 *
  //     rescaled_d0(m_el_d0, m_EventInfo_runNumber, m_is_mc),
  //                                 100, -6.0, 6.0);
  //     add_fill_hist_sys_fast("el_z0sinTheta", m_el_z0sinTheta,
  //                                 100, -0.6, 0.6);
  //     add_fill_hist_sys_fast("el_topoetcone20", m_el_topoetcone20,
  //                                 100, 0.0, 10.0);
  //     add_fill_hist_sys_fast("el_topoetcone20_over_pt",
  //     m_el_topoetcone20_over_pt,
  //                                 100, 0.0, 0.16);
  //     add_fill_hist_sys_fast("el_ptvarcone30_TightTTVA_pt1000",
  //     m_el_ptvarcone30_TightTTVA_pt1000,
  //                                 100, 0.0, 10.0);
  //     add_fill_hist_sys_fast("el_ptvarcone30_TightTTVA_pt1000_over_pt",
  //     m_el_ptvarcone30_TightTTVA_pt1000_over_pt,
  //                                 100, 0.0, 0.1);
  //     add_fill_hist_sys_fast("mu_d0", m_mu_d0 * Charm::mum,
  //                                 400, -200, 200);
  //     add_fill_hist_sys_fast("mu_d0_fixed", rescaled_d0(m_mu_d0,
  //     m_EventInfo_runNumber, m_is_mc) * Charm::mum,
  //                                 400, -200, 200);
  //     add_fill_hist_sys_fast("mu_d0sig", m_mu_d0sig,
  //                                 100, -6.0, 6.0);
  //     add_fill_hist_sys_fast("mu_d0sig_fixed", m_mu_d0sig / m_mu_d0 *
  //     rescaled_d0(m_mu_d0, m_EventInfo_runNumber, m_is_mc),
  //                                 100, -6.0, 6.0);
  //     add_fill_hist_sys_fast("mu_z0sinTheta", m_mu_z0sinTheta,
  //                                 100, -0.6, 0.6);
  //     add_fill_hist_sys_fast("mu_topoetcone20", m_mu_topoetcone20,
  //                                 100, 0.0, 10.0);
  //     add_fill_hist_sys_fast("mu_topoetcone20_over_pt",
  //     m_mu_topoetcone20_over_pt,
  //                                 100, 0.0, 0.16);
  //     add_fill_hist_sys_fast("mu_ptvarcone30_TightTTVA_pt1000",
  //     m_mu_ptvarcone30_TightTTVA_pt1000,
  //                                 100, 0.0, 10.0);
  //     add_fill_hist_sys_fast("mu_ptvarcone30_TightTTVA_pt1000_over_pt",
  //     m_mu_ptvarcone30_TightTTVA_pt1000_over_pt,
  //                                 100, 0.0, 0.1);
  //     // channel specific
  //     add_fill_hist_sys_fast("el_ptvarcone20_TightTTVA_pt1000",
  //     m_el_ptvarcone20_TightTTVA_pt1000,
  //                                 100, 0.0, 10.0);
  //     add_fill_hist_sys_fast("el_ptvarcone20_TightTTVA_pt1000_over_pt",
  //     m_el_ptvarcone20_TightTTVA_pt1000_over_pt,
  //                                 100, 0.0, 0.1);
  //     add_fill_hist_sys_fast("mu_ptvarcone20_TightTTVA_pt1000",
  //     m_mu_ptvarcone20_TightTTVA_pt1000,
  //                                 100, 0.0, 10.0);
  //     add_fill_hist_sys_fast("mu_ptvarcone20_TightTTVA_pt1000_over_pt",
  //     m_mu_ptvarcone20_TightTTVA_pt1000_over_pt,
  //                                 100, 0.0, 0.1);
  //     add_fill_hist_sys_fast("el_ptvarcone30_plus_neflowisol20",
  //     m_el_ptvarcone30_plus_neflowisol20,
  //                                 100, 0.0, 0.1);
  //     add_fill_hist_sys_fast("mu_ptvarcone30_plus_neflowisol20",
  //     m_mu_ptvarcone30_plus_neflowisol20,
  //                                 100, 0.0, 0.1);
  // }
  m_histograms_created = true;
}

void ttbarDilep::connect_branches() {
  // event info
  add_presel_br("METInfo_MET_NOSYS", &m_METInfo_MET_NOSYS);
  // add_presel_br("METInfo_METPhi_NOSYS", &m_METInfo_METPhi_NOSYS);
  add_presel_br("EventInfo_trigPassed_HLT_e24_lhmedium_L1EM20VH",
                &m_EventInfo_trigPassed_HLT_e24_lhmedium_L1EM20VH);
  add_presel_br("EventInfo_trigPassed_HLT_e60_lhmedium",
                &m_EventInfo_trigPassed_HLT_e60_lhmedium);
  add_presel_br("EventInfo_trigPassed_HLT_e120_lhloose",
                &m_EventInfo_trigPassed_HLT_e120_lhloose);
  add_presel_br("EventInfo_trigPassed_HLT_e26_lhtight_nod0_ivarloose",
                &m_EventInfo_trigPassed_HLT_e26_lhtight_nod0_ivarloose);
  add_presel_br("EventInfo_trigPassed_HLT_e60_lhmedium_nod0",
                &m_EventInfo_trigPassed_HLT_e60_lhmedium_nod0);
  add_presel_br("EventInfo_trigPassed_HLT_e140_lhloose_nod0",
                &m_EventInfo_trigPassed_HLT_e140_lhloose_nod0);
  add_presel_br("EventInfo_trigPassed_HLT_mu20_iloose_L1MU15",
                &m_EventInfo_trigPassed_HLT_mu20_iloose_L1MU15);
  add_presel_br("EventInfo_trigPassed_HLT_mu26_ivarmedium",
                &m_EventInfo_trigPassed_HLT_mu26_ivarmedium);
  add_presel_br("EventInfo_trigPassed_HLT_mu50",
                &m_EventInfo_trigPassed_HLT_mu50);
  add_presel_br(Charm::get_run_number_string(m_is_mc), &m_EventInfo_runNumber);
  add_presel_br(Charm::get_pileup_string(m_is_mc, m_mc_period, m_data_period),
                &m_EventInfo_correctedScaled_averageInteractionsPerCrossing);

  // muons
  add_presel_br("AnalysisMuons_pt_NOSYS", &m_AnalysisMuons_pt_NOSYS);
  add_presel_br("AnalysisMuons_eta", &m_AnalysisMuons_eta);
  add_presel_br("AnalysisMuons_phi", &m_AnalysisMuons_phi);
  add_presel_br("AnalysisMuons_charge", &m_AnalysisMuons_charge);
  add_presel_br("AnalysisMuons_mu_selected_NOSYS",
                &m_AnalysisMuons_mu_selected_NOSYS);
  add_presel_br("AnalysisMuons_isQuality_Tight_NOSYS",
                &m_AnalysisMuons_isQuality_Tight_NOSYS);
  add_presel_br("AnalysisMuons_isIsolated_PflowTight_VarRad_NOSYS",
                &m_AnalysisMuons_isIsolated_PflowTight_VarRad_NOSYS);
  add_presel_br("AnalysisMuons_matched_HLT_mu20_iloose_L1MU15",
                &m_AnalysisMuons_matched_HLT_mu20_iloose_L1MU15);
  add_presel_br("AnalysisMuons_matched_HLT_mu26_ivarmedium",
                &m_AnalysisMuons_matched_HLT_mu26_ivarmedium);
  add_presel_br("AnalysisMuons_matched_HLT_mu50",
                &m_AnalysisMuons_matched_HLT_mu50);
  add_presel_br("AnalysisMuons_d0", &m_AnalysisMuons_d0);
  add_presel_br("AnalysisMuons_d0sig", &m_AnalysisMuons_d0sig);
  add_presel_br("AnalysisMuons_is_bad", &m_AnalysisMuons_is_bad);
  if (m_do_extra_histograms) {
    add_presel_br("AnalysisMuons_z0sinTheta", &m_AnalysisMuons_z0sinTheta);
    add_presel_br("AnalysisMuons_topoetcone20", &m_AnalysisMuons_topoetcone20);
    add_presel_br("AnalysisMuons_ptvarcone30_TightTTVA_pt1000",
                  &m_AnalysisMuons_ptvarcone30_TightTTVA_pt1000);
    add_presel_br("AnalysisMuons_ptvarcone30_TightTTVA_pt500",
                  &m_AnalysisMuons_ptvarcone30_TightTTVA_pt500);
    add_presel_br("AnalysisMuons_neflowisol20", &m_AnalysisMuons_neflowisol20);
  }

  // electrons
  add_presel_br("AnalysisElectrons_pt_NOSYS", &m_AnalysisElectrons_pt_NOSYS);
  add_presel_br("AnalysisElectrons_eta", &m_AnalysisElectrons_eta);
  add_presel_br("AnalysisElectrons_phi", &m_AnalysisElectrons_phi);
  add_presel_br("AnalysisElectrons_charge", &m_AnalysisElectrons_charge);
  add_presel_br("AnalysisElectrons_likelihood_Tight",
                &m_AnalysisElectrons_likelihood_Tight);
  add_presel_br("AnalysisElectrons_el_selected_NOSYS",
                &m_AnalysisElectrons_el_selected_NOSYS);
  add_presel_br("AnalysisElectrons_isIsolated_Tight_VarRad_NOSYS",
                &m_AnalysisElectrons_isIsolated_Tight_VarRad_NOSYS);
  add_presel_br("AnalysisElectrons_matched_HLT_e24_lhmedium_L1EM20VH",
                &m_AnalysisElectrons_matched_HLT_e24_lhmedium_L1EM20VH);
  add_presel_br("AnalysisElectrons_matched_HLT_e60_lhmedium",
                &m_AnalysisElectrons_matched_HLT_e60_lhmedium);
  add_presel_br("AnalysisElectrons_matched_HLT_e120_lhloose",
                &m_AnalysisElectrons_matched_HLT_e120_lhloose);
  add_presel_br("AnalysisElectrons_matched_HLT_e26_lhtight_nod0_ivarloose",
                &m_AnalysisElectrons_matched_HLT_e26_lhtight_nod0_ivarloose);
  add_presel_br("AnalysisElectrons_matched_HLT_e60_lhmedium_nod0",
                &m_AnalysisElectrons_matched_HLT_e60_lhmedium_nod0);
  add_presel_br("AnalysisElectrons_matched_HLT_e140_lhloose_nod0",
                &m_AnalysisElectrons_matched_HLT_e140_lhloose_nod0);
  add_presel_br("AnalysisElectrons_d0", &m_AnalysisElectrons_d0);
  add_presel_br("AnalysisElectrons_d0sig", &m_AnalysisElectrons_d0sig);
  if (m_do_extra_histograms) {
    add_presel_br("AnalysisElectrons_z0sinTheta",
                  &m_AnalysisElectrons_z0sinTheta);
    add_presel_br("AnalysisElectrons_topoetcone20",
                  &m_AnalysisElectrons_topoetcone20);
    add_presel_br("AnalysisElectrons_ptvarcone20_TightTTVA_pt1000",
                  &m_AnalysisElectrons_ptvarcone20_TightTTVA_pt1000);
    add_presel_br("AnalysisElectrons_ptvarcone30_TightTTVA_pt1000",
                  &m_AnalysisElectrons_ptvarcone30_TightTTVA_pt1000);
  }

  // jets
  add_presel_br("AnalysisJets_pt_NOSYS", &m_AnalysisJets_pt_NOSYS);
  add_presel_br("AnalysisJets_eta", &m_AnalysisJets_eta);
  add_presel_br("AnalysisJets_jet_selected_NOSYS",
                &m_AnalysisJets_jet_selected_NOSYS);
  add_presel_br("AnalysisJets_ftag_select_DL1r_FixedCutBEff_70",
                &m_AnalysisJets_ftag_select_DL1r_FixedCutBEff_70);
  if (m_is_mc) {
    // add_presel_br("AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_NOSYS",
    // &m_AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_NOSYS);//odd
  }

  // mc only
  if (m_is_mc) {
    add_presel_br("EventInfo_generatorWeight_NOSYS",
                  &m_EventInfo_generatorWeight_NOSYS);
    add_presel_br("EventInfo_PileupWeight_NOSYS",
                  &m_EventInfo_PileupWeight_NOSYS);
    add_presel_br("EventInfo_jvt_effSF_NOSYS", &m_EventInfo_jvt_effSF_NOSYS);
    add_presel_br("AnalysisElectrons_effSF_NOSYS",
                  &m_AnalysisElectrons_effSF_NOSYS);
    add_presel_br("AnalysisElectrons_effSF_ID_Tight_NOSYS",
                  &m_AnalysisElectrons_effSF_ID_Tight_NOSYS);
    add_presel_br("AnalysisElectrons_effSF_Isol_Tight_Tight_VarRad_NOSYS",
                  &m_AnalysisElectrons_effSF_Isol_Tight_Tight_VarRad_NOSYS);
    add_presel_br(
        "AnalysisElectrons_effSF_Trig_Tight_Tight_VarRad_Eff_SINGLE_E_2015_e24_"
        "lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_"
        "lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_"
        "NOSYS",
        &m_AnalysisElectrons_effSF_Trig_Tight_Tight_VarRad_Eff_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_NOSYS);
    add_presel_br(
        "AnalysisElectrons_effSF_Trig_Tight_Tight_VarRad_SINGLE_E_2015_e24_"
        "lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_"
        "lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_"
        "NOSYS",
        &m_AnalysisElectrons_effSF_Trig_Tight_Tight_VarRad_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_NOSYS);
    add_presel_br("AnalysisElectrons_truthType",
                  &m_AnalysisElectrons_truthType);
    add_presel_br("AnalysisElectrons_truthOrigin",
                  &m_AnalysisElectrons_truthOrigin);
    add_presel_br("AnalysisElectrons_firstEgMotherTruthType",
                  &m_AnalysisElectrons_firstEgMotherTruthType);
    add_presel_br("AnalysisElectrons_firstEgMotherTruthOrigin",
                  &m_AnalysisElectrons_firstEgMotherTruthOrigin);
    add_presel_br("AnalysisElectrons_firstEgMotherPdgId",
                  &m_AnalysisElectrons_firstEgMotherPdgId);
    add_presel_br("AnalysisMuons_muon_effSF_Quality_Tight_NOSYS",
                  &m_AnalysisMuons_muon_effSF_Quality_Tight_NOSYS);
    add_presel_br("AnalysisMuons_muon_effSF_TTVA_NOSYS",
                  &m_AnalysisMuons_muon_effSF_TTVA_NOSYS);
    add_presel_br("AnalysisMuons_muon_effSF_Isol_PflowTight_VarRad_NOSYS",
                  &m_AnalysisMuons_muon_effSF_Isol_PflowTight_VarRad_NOSYS);
    add_presel_br(
        "AnalysisMuons_muon_effMC_Trig_Tight_HLT_mu26_ivarmedium_OR_HLT_mu50_"
        "NOSYS",
        &m_AnalysisMuons_muon_effMC_Trig_Tight_HLT_mu26_ivarmedium_OR_HLT_mu50_NOSYS);
    add_presel_br(
        "AnalysisMuons_muon_effData_Trig_Tight_HLT_mu26_ivarmedium_OR_HLT_mu50_"
        "NOSYS",
        &m_AnalysisMuons_muon_effData_Trig_Tight_HLT_mu26_ivarmedium_OR_HLT_mu50_NOSYS);
    add_presel_br(
        "AnalysisMuons_muon_effMC_Trig_Tight_HLT_mu20_iloose_L1MU15_OR_HLT_"
        "mu50_"
        "NOSYS",
        &m_AnalysisMuons_muon_effMC_Trig_Tight_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_NOSYS);
    add_presel_br(
        "AnalysisMuons_muon_effData_Trig_Tight_HLT_mu20_iloose_L1MU15_OR_HLT_"
        "mu50_NOSYS",
        &m_AnalysisMuons_muon_effData_Trig_Tight_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_NOSYS);
    add_presel_br("AnalysisMuons_truthType", &m_AnalysisMuons_truthType);
    add_presel_br("AnalysisMuons_truthOrigin", &m_AnalysisMuons_truthOrigin);

    // truth leptons
    add_presel_br("TruthLeptons_pt", &m_TruthLeptons_pt);
    // add_presel_br("TruthLeptons_m", &m_TruthLeptons_m);
    // add_presel_br("TruthLeptons_m", &m_TruthLeptons_e);
    add_presel_br("TruthLeptons_eta", &m_TruthLeptons_eta);
    add_presel_br("TruthLeptons_phi", &m_TruthLeptons_phi);
    add_presel_br("TruthLeptons_barcode", &m_TruthLeptons_barcode);
    add_presel_br("TruthLeptons_pdgId", &m_TruthLeptons_pdgId);
    add_presel_br("TruthLeptons_status", &m_TruthLeptons_status);
    add_presel_br("TruthLeptons_classifierParticleOrigin",
                  &m_TruthLeptons_classifierParticleOrigin);
    add_presel_br("TruthLeptons_classifierParticleType",
                  &m_TruthLeptons_classifierParticleType);

    // sys branches
    if (m_do_systematics) {
      init_sys_br(&m_sys_br_GEN, "EventInfo_generatorWeight_%s");
      init_sys_br(&m_sys_br_PU, "EventInfo_%s_PileupWeight");
      init_sys_br(&m_sys_br_JVT, "EventInfo_jvt_effSF_%s");
      init_sys_br(&m_sys_br_FTAG,
                  "EventInfo_ftag_effSF_DL1_FixedCutBEff_70_%s");
      init_sys_br(&m_sys_br_MET_met, "METInfo_%s_MET");

      // electron sys branches
      init_sys_br(&m_sys_br_el_pt, "AnalysisElectrons_%s_pt");
      init_sys_br(&m_sys_br_el_selected, "AnalysisElectrons_%s_el_selected");
      init_sys_br(&m_sys_br_el_selected_other_sys,
                  "AnalysisElectrons_el_selected_%s");
      init_sys_br(&m_sys_br_el_isIsolated_FCTight,
                  "AnalysisElectrons_%s_isIsolated_FCTight");
      init_sys_br(&m_sys_br_el_reco_eff, "AnalysisElectrons_effSF_%s");
      init_sys_br(&m_sys_br_el_id_eff, "AnalysisElectrons_effSF_ID_Tight_%s");
      init_sys_br(&m_sys_br_el_iso_eff,
                  "AnalysisElectrons_effSF_Isol_Tight_Tight_VarRad_%s");
      init_sys_br(
          &m_sys_br_el_trig_eff,
          "AnalysisElectrons_effSF_Trig_Tight_Tight_VarRad_Eff_SINGLE_E_2015_"
          "e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_"
          "lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_%s",
          {"EL_EFF_Trigger_TOTAL_1NPCOR_PLUS_UNCOR__1up",
           "EL_EFF_Trigger_TOTAL_1NPCOR_PLUS_UNCOR__1down"});
      init_sys_br(
          &m_sys_br_el_trig_sf,
          "AnalysisElectrons_effSF_Trig_Tight_Tight_VarRad_SINGLE_E_2015_e24_"
          "lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_"
          "lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_%s",
          {"EL_EFF_TriggerEff_TOTAL_1NPCOR_PLUS_UNCOR__1up",
           "EL_EFF_TriggerEff_TOTAL_1NPCOR_PLUS_UNCOR__1down"});
      init_sys_br(&m_sys_br_el_calib_reco_eff, "AnalysisElectrons_%s_effSF");
      init_sys_br(&m_sys_br_el_calib_id_eff,
                  "AnalysisElectrons_%s_effSF_ID_Tight");
      init_sys_br(&m_sys_br_el_calib_iso_eff,
                  "AnalysisElectrons_%s_effSF_Isol_Tight_FCTight");
      init_sys_br(&m_sys_br_el_calib_trig_eff,
                  "AnalysisElectrons_%s_effSF_Trig_Tight_Tight_VarRad_Eff_"
                  "SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_"
                  "lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_"
                  "lhmedium_nod0_OR_e140_lhloose_nod0");
      init_sys_br(
          &m_sys_br_el_calib_trig_sf,
          "AnalysisElectrons_%s_effSF_Trig_Tight_Tight_VarRad_SINGLE_E_2015_"
          "e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_"
          "lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0");

      // muon sys branches
      init_sys_br(&m_sys_br_mu_pt, "AnalysisMuons_%s_pt");
      init_sys_br(&m_sys_br_mu_selected, "AnalysisMuons_%s_mu_selected");
      init_sys_br(&m_sys_br_mu_selected_other_sys,
                  "AnalysisMuons_muon_selected_%s");
      init_sys_br(&m_sys_br_mu_isQuality_Tight,
                  "AnalysisMuons_%s_isQuality_Tight");
      init_sys_br(&m_sys_br_mu_isIsolated_PflowTight_VarRad,
                  "AnalysisMuons_%s_isIsolated_PflowTight_VarRad");
      init_sys_br(&m_sys_br_mu_quality_eff,
                  "AnalysisMuons_muon_effSF_Quality_Tight_%s");
      init_sys_br(&m_sys_br_mu_ttva_eff, "AnalysisMuons_muon_effSF_TTVA_%s");
      init_sys_br(&m_sys_br_mu_iso_eff,
                  "AnalysisMuons_muon_effSF_Isol_PflowTight_VarRad_%s");
      init_sys_br(
          &m_sys_br_mu_trig_2015_eff_mc,
          "AnalysisMuons_muon_effMC_Trig_Tight_HLT_mu20_iloose_L1MU15_OR_"
          "HLT_mu50_%s");
      init_sys_br(
          &m_sys_br_mu_trig_2015_eff_data,
          "AnalysisMuons_muon_effData_Trig_Tight_HLT_mu20_iloose_L1MU15_"
          "OR_HLT_mu50_%s");
      init_sys_br(&m_sys_br_mu_trig_2018_eff_mc,
                  "AnalysisMuons_muon_effMC_Trig_Tight_HLT_mu26_ivarmedium_OR_"
                  "HLT_mu50_%s");
      init_sys_br(
          &m_sys_br_mu_trig_2018_eff_data,
          "AnalysisMuons_muon_effData_Trig_Tight_HLT_mu26_ivarmedium_OR_"
          "HLT_mu50_%s");
      init_sys_br(&m_sys_br_mu_calib_quality_eff,
                  "AnalysisMuons_%s_mu_effSF_Quality_Tight");
      init_sys_br(&m_sys_br_mu_calib_ttva_eff,
                  "AnalysisMuons_%s_mu_effSF_TTVA");
      init_sys_br(&m_sys_br_mu_calib_iso_eff,
                  "AnalysisMuons_%s_mu_effSF_Isol_PflowTight_VarRad");
      init_sys_br(&m_sys_br_mu_calib_trig_2015_eff_mc,
                  "AnalysisMuons_%s_mu_effMC_Trig_Tight_HLT_mu20_iloose_L1MU15_"
                  "OR_HLT_mu50");
      init_sys_br(&m_sys_br_mu_calib_trig_2015_eff_data,
                  "AnalysisMuons_%s_mu_effData_Trig_Tight_HLT_mu20_iloose_"
                  "L1MU15_OR_HLT_mu50");
      init_sys_br(&m_sys_br_mu_calib_trig_2018_eff_mc,
                  "AnalysisMuons_%s_mu_effMC_Trig_Tight_HLT_mu26_ivarmedium_OR_"
                  "HLT_mu50");
      init_sys_br(&m_sys_br_mu_calib_trig_2018_eff_data,
                  "AnalysisMuons_%s_mu_effData_Trig_Tight_HLT_mu26_ivarmedium_"
                  "OR_HLT_mu50");

      // jet sys branches
      init_sys_br(&m_sys_br_jet_pt, "AnalysisJets_%s_pt");
      init_sys_br(&m_sys_br_jet_selected, "AnalysisJets_%s_jet_selected");
      init_sys_br(&m_sys_br_jet_selected_other_sys,
                  "AnalysisJets_jet_selected_%s");
    }
  }
}

ttbarDilep::ttbarDilep(TString input_file, TString out_path, TString tree_name)
    : EventLoopBase(input_file, out_path, tree_name) {
  add_channel("inclusive", {"all", "bad_mu_veto", "two_leptons", "charge",
                            "trigger", "trigger_match"});
  add_channel("dilep", {});
  add_channel("dilep_prompt_mu", {});
  add_channel("dilep_nonprompt_mu", {});
  add_channel("dilep_tau_DK", {});

  // settings
  m_do_extra_histograms = false;
  m_split_by_period = false;
  m_truth_match = false;
  m_do_pt_v_rw = false;
  m_do_systematics = false;
}

}  // namespace Charm
