#include <math.h>

#include <iostream>

#include "CharmLoopBase.h"
#include "HelperFunctions.h"
#include "TVector2.h"

namespace Charm {

std::string validate_name(const std::string& name,
                          const std::vector<std::string>& valid_names) {
  if (std::find(valid_names.begin(), valid_names.end(), name) ==
      valid_names.end()) {
    std::cerr << "Error: Invalid name '" << name << "'." << std::endl;
    std::cerr << "Valid names are: ";
    for (const auto& name : valid_names) {
      std::cerr << name << " ";
    }
    std::cerr << std::endl;
    throw std::invalid_argument("Invalid name");
  }
  return name;
}

void CharmLoopBase::configure_container_sys(std::string& sys_name) {
  if (sys_name.empty()) {
    return;
  }
  if (m_sys_config_name != "default") {
    std::cerr << "Error: Invalid configuration!" << std::endl;
    throw std::invalid_argument("Invalid configuration");
  }
  m_output_file_name.ReplaceAll(".default.", "." + sys_name + ".");
  m_save_nominal = false;
  m_do_systematics = true;
  m_used_systematics.push_back(sys_name);
  m_systematics.push_back(sys_name);
  m_nSyst = 2;
}

void CharmLoopBase::configure() {
  // set configs
  std::cout << get_config() << std::endl;

  m_cut_drLep_max = get_config()["cut_drLep_max"].as<float>(0.3);

  // systematics
  m_do_systematics = get_config()["do_systematics"].as<bool>(false);

  // save only variables needed for the fit
  m_fit_variables_only = get_config()["fit_variables_only"].as<bool>(false);

  // save unfolding varaibles (transfer matrix, etc..)
  m_unfolding_variables = get_config()["unfolding_variables"].as<bool>(false);

  // En/Disable Dplus Isolation Cut
  m_dplus_isolation = get_config()["dplus_isolation"].as<bool>(true);

  // lepton preseletion
  m_do_lep_presel = get_config()["do_lep_presel"].as<bool>(true);
  m_fill_wjets = get_config()["fill_wjets"].as<bool>(true);

  // truth match leptons
  m_truth_match = get_config()["truth_match"].as<bool>(true);

  // add primary vertex variables
  m_do_PV = get_config()["do_PV"].as<bool>(false);

  // channels
  m_split_by_period = get_config()["split_by_period"].as<bool>(true);
  m_split_by_charge = get_config()["split_by_charge"].as<bool>(false);

  // D-meson reconstruction
  m_D_plus_meson = get_config()["D_plus_meson"].as<bool>(true);
  m_D_star_meson = get_config()["D_star_meson"].as<bool>(false);
  m_D_star_pi0_meson = get_config()["D_star_pi0_meson"].as<bool>(false);
  m_D_s_meson = get_config()["D_s_meson"].as<bool>(false);
  m_Lambda_C_baryon = get_config()["Lambda_C_baryon"].as<bool>(false);

  // Enable Tight Primary track selection (Loose is default, ie false)
  m_D_tightPrimary_tracks =
      get_config()["D_tightPrimary_tracks"].as<bool>(false);

  // D-meson truth
  m_truth_matchD = get_config()["truth_matchD"].as<bool>(true);
  m_truth_D = get_config()["truth_D"].as<bool>(false);

  // Single entry D-meson regions
  m_do_single_entry_D = get_config()["do_single_entry_D"].as<bool>(false);
  m_do_single_entry_D_only =
      get_config()["do_single_entry_D_only"].as<bool>(false);

  // D-meson N-1
  m_do_Nminus1 = get_config()["do_Nminus1"].as<bool>(false);

  // Keep all matched D's from N-1 even if the fail 2 or more cuts
  m_do_twice_failed = get_config()["do_twice_failed"].as<bool>(false);

  // Filling SB/SR Regions
  m_split_into_SB = get_config()["split_into_SB"].as<bool>(false);

  // apply truth fiducial cuts
  m_fiducial_selection = get_config()["fiducial_selection"].as<bool>(false);

  // save truth wjets quantities
  m_save_truth_wjets = get_config()["save_truth_wjets"].as<bool>(false);

  // pT(V) reweight
  m_do_pt_v_rw = get_config()["do_pt_v_rw"].as<bool>(false);

  // charm production fraction reweight from the ntuple
  m_do_prod_fraction_rw = get_config()["do_prod_fraction_rw"].as<bool>(true);

  // charm production fraction reweight implemented in charmpp
  m_do_prod_fraction_rw_old =
      get_config()["do_prod_fraction_rw_old"].as<bool>(false);

  // reweight wjets samples
  m_do_wjets_rw = get_config()["do_wjets_rw"].as<bool>(true);

  // extra lepton histograms
  m_do_extra_histograms = get_config()["do_extra_histograms"].as<bool>(false);

  // parametrize electrons with topoetcone
  m_do_el_topocone_param = get_config()["do_el_topocone_param"].as<bool>(false);

  // jet multiplicity requirement
  m_jet_selection = get_config()["jet_selection"].as<bool>(true);

  // use track-jets
  m_track_jet_selection = get_config()["track_jet_selection"].as<bool>(false);

  // use trackjet charm fragmentation D cuts
  m_frag_selection = get_config()["trackjet_fragmentation_selection"].as<bool>(false);

  // use the 2-tag region separatelly
  m_two_tag_region = get_config()["two_tag_region"].as<bool>(false);

  // Matrix Method
  m_fake_rate_measurement =
      get_config()["fake_rate_measurement"].as<bool>(false);

  // Matrix Method Eval
  m_eval_fake_rate = get_config()["eval_fake_rate"].as<bool>(true);

  // Fake Factor instead of Matrix Method
  m_fake_factor_method = get_config()["fake_factor_method"].as<bool>(false);

  // Reweight MM from Closure
  m_rw_mm_closure = get_config()["do_rw_mm_closure"].as<bool>(false);

  // Bin D mesons in pT
  m_bin_in_D_pt = get_config()["bin_in_D_pt"].as<bool>(false);

  // Bin D mesons in truth pT
  m_bin_in_truth_D_pt = get_config()["bin_in_truth_D_pt"].as<bool>(false);

  // Bin D mesons in lepton eta
  m_bin_in_lep_eta = get_config()["bin_in_lep_eta"].as<bool>(false);

  // Bin D mesons in truth lepton eta
  m_bin_in_truth_lep_eta = get_config()["bin_in_truth_lep_eta"].as<bool>(false);

  // Bin D mesons in track-jet pT
  m_bin_in_track_jet_pt = get_config()["bin_in_track_jet_pt"].as<bool>(false);

  // Signal only (only save the Matched categories)
  m_signal_only = get_config()["signal_only"].as<bool>(false);

  // inclusive fake rates (instead of fake rates for W+D)
  m_inclusive_fake_rate = get_config()["inclusive_fake_rate"].as<bool>(false);

  // split SR into SR and Anti_SR
  m_loose_w_selection = get_config()["loose_w_selection"].as<bool>(false);

  // skip MET, mT, and pT(lep) cuts entierly
  m_loose_w_selection_only =
      get_config()["loose_w_selection_only"].as<bool>(false);

  // apply luminosity
  m_apply_lumi = get_config()["apply_lumi"].as<bool>(true);

  // reweight spg samples
  m_reweight_spg = get_config()["reweight_spg"].as<bool>(true);

  // debug print-out for D mesons
  m_debug_dmeson_printout =
      get_config()["debug_dmeson_printout"].as<bool>(false);

  // use AntiTight MET
  m_use_anti_tight_met = get_config()["use_anti_tight_met"].as<bool>(false);

  // Run mT validation region in the standard analysis
  m_run_val_region = get_config()["run_val_region"].as<bool>(false);

  // Run appreviated regions in MM Closure
  m_less_closure_region = get_config()["less_closure_region"].as<bool>(false);

  // Save output tree
  m_save_output_tree = get_config()["save_output_tree"].as<bool>(false);

  // reweight sherpa 2.2.11 samples (09/2023 not recommend except for Diboson)
  m_reweight_sherpa = get_config()["reweight_sherpa"].as<bool>(false);

  // extra output branches
  m_extended_output_branches =
      get_config()["extended_output_branches"].as<bool>(false);

  // no D meson selection
  m_no_d_meson_selection = get_config()["no_d_meson_selection"].as<bool>(false);

  // simplified truth matching categories
  m_simplified_dmeson_truth =
      get_config()["simplified_dmeson_truth"].as<bool>(false);

  // track-jet radius selection
  m_track_jet_radius = get_config()["track_jet_radius"].as<std::string>("10");

  // W+D flavor tagging c-jet calibration config
  m_dmeson_jet_matching = get_config()["dmeson_jet_matching"].as<bool>(false);

  // apply FTAG scale factor
  m_apply_FTAG_SF = get_config()["apply_FTAG_SF"].as<bool>(true);
  // reduced GEN variations
  m_reduced_gen_variations = get_config()["reduced_gen_variations"].as<bool>(false);

  // special configuration for container systematics
  m_track_sys_name = validate_name(get_config()["track_sys_name"].as<std::string>(""), TRACK_CONTAINER_VARIATIONS);
  configure_container_sys(m_track_sys_name);
}

void CharmLoopBase::systConfigure() {
  // GEN
  m_sys_br_GEN.init(get_sys("systematics_GEN"));

  // PRW
  m_sys_br_PU.init(get_sys("systematics_PRW"));

  // JVT
  m_sys_br_JVT.init(get_sys("systematics_JET_Calib"));
  m_sys_br_JVT.init(get_sys("systematics_JET_Jvt"));
  // TODO: check if this is fine?
  // m_sys_br_JVT.init(get_sys("systematics_EL_Calib"));
  // m_sys_br_JVT.init(get_sys("systematics_MUON_Calib"));

  // Top weight (TODO: implement systematics for top weight)
  m_sys_br_TOP.init({});

  // Production fractions
  m_sys_br_PROD_FRAC.init(get_sys("systematics_PROD_FRAC"));

  // MET
  m_sys_br_MET_met.init(get_sys("systematics_EL_Calib"));
  m_sys_br_MET_met.init(get_sys("systematics_JET_Calib"));
  m_sys_br_MET_met.init(get_sys("systematics_MET_Calib"));
  m_sys_br_MET_met.init(get_sys("systematics_MUON_Calib"));
  m_sys_br_MET_phi.init(get_sys("systematics_EL_Calib"));
  m_sys_br_MET_phi.init(get_sys("systematics_JET_Calib"));
  m_sys_br_MET_phi.init(get_sys("systematics_MET_Calib"));
  m_sys_br_MET_phi.init(get_sys("systematics_MUON_Calib"));

  // electron calib
  m_sys_br_el_isIsolated_Tight_VarRad.init(get_sys("systematics_EL_Calib"));
  m_sys_br_el_pt.init(get_sys("systematics_EL_Calib"));
  m_sys_br_el_selected.init(get_sys("systematics_JET_Calib"));
  m_sys_br_el_selected.init(get_sys("systematics_MUON_Calib"));
  m_sys_br_el_selected.init(get_sys("systematics_EL_Calib"));
  m_sys_br_el_calib_reco_eff.init(get_sys("systematics_EL_Calib"));
  m_sys_br_el_calib_id_eff.init(get_sys("systematics_EL_Calib"));
  m_sys_br_el_calib_iso_eff.init(get_sys("systematics_EL_Calib"));
  // m_sys_br_el_calib_trig_eff.init(get_sys("systematics_EL_Calib"));
  m_sys_br_el_calib_trig_sf.init(get_sys("systematics_EL_Calib"));
  m_sys_br_el_calib_trig_noiso_sf.init(get_sys("systematics_EL_Calib"));
  m_sys_br_el_calib_charge_eff.init(get_sys("systematics_EL_Calib"));

  // muon calib
  m_sys_br_mu_isQuality_Tight.init(get_sys("systematics_MUON_Calib"));
  m_sys_br_mu_pt.init(get_sys("systematics_MUON_Calib"));
  m_sys_br_mu_selected.init(get_sys("systematics_EL_Calib"));
  m_sys_br_mu_selected.init(get_sys("systematics_JET_Calib"));
  m_sys_br_mu_selected.init(get_sys("systematics_MUON_Calib"));
  m_sys_br_mu_isIsolated_PflowTight_VarRad.init(
      get_sys("systematics_MUON_Calib"));
  m_sys_br_mu_calib_quality_eff.init(get_sys("systematics_MUON_Calib"));
  m_sys_br_mu_calib_ttva_eff.init(get_sys("systematics_MUON_Calib"));
  m_sys_br_mu_calib_trig_2015_eff_data.init(get_sys("systematics_MUON_Calib"));
  m_sys_br_mu_calib_iso_eff.init(get_sys("systematics_MUON_Calib"));
  m_sys_br_mu_calib_trig_2015_eff_mc.init(get_sys("systematics_MUON_Calib"));
  m_sys_br_mu_calib_trig_2018_eff_data.init(get_sys("systematics_MUON_Calib"));
  m_sys_br_mu_calib_trig_2018_eff_mc.init(get_sys("systematics_MUON_Calib"));

  // electron weights
  m_sys_br_el_reco_eff.init(get_sys("systematics_EL_EFF_Reco"));
  m_sys_br_el_id_eff.init(get_sys("systematics_EL_EFF_ID"));
  m_sys_br_el_iso_eff.init(get_sys("systematics_EL_EFF_Iso"));
  // m_sys_br_el_trig_eff.init(get_sys("systematics_EL_EFF_Trigger"));
  m_sys_br_el_trig_sf.init(get_sys("systematics_EL_EFF_Trigger"));
  m_sys_br_el_charge_eff.init(get_sys("systematics_EL_CHARGE"));
  m_sys_br_el_trig_noiso_sf.init(get_sys("systematics_EL_EFF_Trigger"));

  // muon weights
  m_sys_br_mu_quality_eff.init(get_sys("systematics_MUON_EFF_RECO"));
  m_sys_br_mu_ttva_eff.init(get_sys("systematics_MUON_EFF_TTVA"));
  m_sys_br_mu_trig_2015_eff_data.init(get_sys("systematics_MUON_EFF_Trig"));
  m_sys_br_mu_iso_eff.init(get_sys("systematics_MUON_EFF_ISO"));
  m_sys_br_mu_trig_2015_eff_mc.init(get_sys("systematics_MUON_EFF_Trig"));
  m_sys_br_mu_trig_2018_eff_data.init(get_sys("systematics_MUON_EFF_Trig"));
  m_sys_br_mu_trig_2018_eff_mc.init(get_sys("systematics_MUON_EFF_Trig"));

  // jet calib
  m_sys_br_jet_pt.init(get_sys("systematics_JET_Calib"));
  m_sys_br_jet_selected.init(get_sys("systematics_JET_Calib"));
  m_sys_br_jet_selected.init(get_sys("systematics_EL_Calib"));
  m_sys_br_jet_selected.init(get_sys("systematics_MUON_Calib"));

  // FTAG
  m_sys_br_jet_ftag_eff.init(get_sys("systematics_FT_EFF"));
  m_sys_br_jet_calib_ftag_eff.init(get_sys("systematics_JET_Calib"));
}
}  // namespace Charm
