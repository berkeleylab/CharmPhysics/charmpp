#include <math.h>

#include <iostream>

#include "CharmLoopBase.h"
#include "HelperFunctions.h"
#include "TVector2.h"

namespace Charm {

void CharmLoopBase::fill_truth_D_star_branches(int truthIndex) {
  m_out_truth_Dmeson_m = 0;
  m_out_truth_Dmeson_pt = 0;
  m_out_truth_Dmeson_eta = 999;
  m_out_truth_Dmeson_phi = 999;
  if (truthIndex >= 0) {
    m_out_truth_Dmeson_m = truth_daughter_mass(truthIndex) * Charm::GeV;
    m_out_truth_Dmeson_pt =
        m_TruthParticles_Selected_pt->at(truthIndex) * Charm::GeV;
    m_out_truth_Dmeson_eta = m_TruthParticles_Selected_eta->at(truthIndex);
    m_out_truth_Dmeson_phi = m_TruthParticles_Selected_phi->at(truthIndex);
  }
}

void CharmLoopBase::fill_D_star_histograms(std::string channel) {
  // number of D mesons
  if (m_fill_wjets && !m_fit_variables_only) {
    add_fill_hist_sys("N_Dstar", channel, m_D_star_indices.size(), 20, -0.5,
                      19.5, true);
  }

  // single entry region with at least one D meson
  if (m_do_single_entry_D && m_D_star_indices.size()) {
    auto single_entry_channel = std::vector<std::string>(m_nSyst, channel);
    auto sys_at_least_one_D = std::vector<bool>(m_nSyst, false);
    std::vector<float> multiplicity = std::vector<float>(m_nSyst, 0);
    bool has_D = false;
    for (unsigned int j = 0; j < m_nSyst; j++) {
      // loop over D star mesons
      for (unsigned int i = 0; i < m_D_star_indices.size(); i++) {
        // retreive the D meson
        const auto D_index = m_D_star_indices.at(i);

        // dR cut
        float dRlep = 999;
        if (m_do_lep_presel) {
          dRlep = DMesonRec::D_meson_lepton_dR(D_index, m_sys_lep_eta.at(j),
                                               m_sys_lep_phi.at(j));
        }
        if (dRlep >= 0.3) {
          sys_at_least_one_D.at(j) = true;
          multiplicity.at(j) += 1;
        }
      }
      if (multiplicity.at(j) > 0) {
        single_entry_channel.at(j) += "_Dstar";
        has_D = true;
      }
    }

    if (m_do_single_entry_D_only && m_have_one_D) {
      return;
    } else if (m_do_single_entry_D_only && !m_have_one_D && has_D) {
      m_have_one_D = true;
    }

    if (m_do_lep_presel) {
      fill_lepton_histograms(single_entry_channel, sys_at_least_one_D);
    }
    if (m_do_PV) {
      fill_PV_histograms(single_entry_channel, sys_at_least_one_D);
    }
    add_fill_hist_sys("N_Dstar", single_entry_channel, multiplicity, 20, -0.5,
                      19.5, true, sys_at_least_one_D);
    if (m_do_single_entry_D_only) {
      return;
    }
  }

  // counters for D multiplicity
  std::vector<std::unordered_map<std::string, unsigned int>>
      multiplicity_counter;
  std::set<std::string> used_channels;

  // loop over D star mesons
  for (unsigned int i = 0; i < m_D_star_indices.size(); i++) {
    // retreive the D meson
    const auto D_index = m_D_star_indices.at(i);

    // truth category
    bool matched_signal = false;
    std::string truth_category = "";
    int truthIndex = -1;
    if (m_truth_matchD && m_is_mc) {
      truth_category = TruthInfo::get_truth_category(this, D_index, "Kpipi",
                                                     m_fiducial_selection, false,
                                                     m_simplified_dmeson_truth);
      truthIndex =
          TruthInfo::getMatchedBarcode(m_DMesons_truthBarcode->at(D_index));
    }
    if (truthIndex >= 0) {
      m_truth_D_pt = m_TruthParticles_Selected_pt->at(truthIndex) * Charm::GeV;
      m_truth_D_eta = m_TruthParticles_Selected_eta->at(truthIndex);
      m_truth_D_mass = truth_daughter_mass(truthIndex);
      m_truth_abs_lep_eta = std::abs(m_truth_lep1.Eta());
      if (truth_category == "Matched") {
        matched_signal = true;
        if (m_bin_in_truth_D_pt) {
          truth_category +=
              "_truth_pt_bin" +
              std::to_string(m_differential_bins.get_pt_bin(m_truth_D_pt) + 1);
        } else if (m_bin_in_truth_lep_eta) {
          truth_category +=
              "_truth_eta_bin" +
              std::to_string(
                  m_differential_bins_eta.get_eta_bin(m_truth_abs_lep_eta) + 1);
        }
      }
    } else {
      m_truth_D_pt = 0;
      m_truth_D_eta = -999;
      m_truth_D_mass = 0;
      m_truth_abs_lep_eta = -999;
    }

    // signal only
    if (m_signal_only && !matched_signal) {
      continue;
    }

    // dR w.r.t. the lepton depends on systematic
    auto sys_dRlep = std::vector<float>(m_nSyst, 0.);

    // D meson mass
    m_sys_dmeson_mass =
        std::vector<float>(m_nSyst, m_DMesons_DeltaMass->at(D_index));

    // OS or SS region
    auto charged_channel = std::vector<std::string>(m_nSyst, channel);
    auto pass_cuts = std::vector<bool>(m_nSyst, false);
    auto extra_weight = std::vector<double>(m_nSyst, 1.0);

    // track-jet containers
    m_sys_matched_track_jet =
        std::make_unique<SysTrackJet>(SysTrackJet{m_systematics.size()+1});

    for (unsigned int j = 0; j < m_nSyst; j++) {
      multiplicity_counter.push_back({});
      if (!m_do_lep_presel ||
          (event_pass_sys(j) && m_sys_lep_charge.at(j) != 0)) {
        // dR w.r.t. the lepton
        float dRlep = 999;
        float lep_charge = 0;
        if (m_do_lep_presel) {
          dRlep = DMesonRec::D_meson_lepton_dR(D_index, m_sys_lep_eta.at(j),
                                               m_sys_lep_phi.at(j));
          lep_charge = m_sys_lep_charge.at(j);
        }
        sys_dRlep.at(j) = dRlep;

        // dR cut
        if (dRlep < 0.3) {
          continue;
        }

        // track-jet properties
        if (m_track_jet_selection) {
          calculate_track_jet_properties(
              D_index, j, truthIndex, &m_track_jet_container,
              &m_truth_track_jet_container, m_sys_matched_track_jet.get());
        }

        // sys string
        std::string sys_string = "";
        if (j > 0) {
          sys_string = m_systematics.at(j - 1);
        }

        // track efficiency sys weight
        double track_weight = m_track_sys_helper.get_track_eff_weight(
            sys_string, m_DMesons_pt->at(D_index) * Charm::GeV,
            m_DMesons_eta->at(D_index), m_DMesons_pdgId->at(D_index));
        extra_weight.at(j) *= track_weight;

        // smear mass
        double smear_mdiff = m_mass_smear_helper.get_smeared_mass(
            sys_string,
            m_differential_bins.get_pt_bin(m_DMesons_pt->at(D_index) *
                                           Charm::GeV),
            m_DMesons_eta->at(D_index), m_DMesons_pdgId->at(D_index),
            m_DMesons_DeltaMass->at(D_index));
        m_sys_dmeson_mass.at(j) = smear_mdiff;

        // modeling uncertainty
        if (matched_signal) {
          double fid_eff_weight = 1.0;
          if (m_bin_in_D_pt) {
            fid_eff_weight = m_modeling_uncertainty_helper.get_weight_pt(
                sys_string, m_truth_D_pt, "Dstar");
          } else if (m_bin_in_lep_eta) {
            fid_eff_weight = m_modeling_uncertainty_helper.get_weight_eta(
                sys_string, m_truth_abs_lep_eta, "Dstar");
          }
          extra_weight.at(j) *= fid_eff_weight;
        }
        // weight for SPG forced decay sample
        if (matched_signal && m_reweight_spg &&
            m_data_period == "ForcedDecay") {
          extra_weight.at(j) *= m_spg_forced_decay_helper.get_weight(
              m_DMesons_DeltaMass->at(D_index) * Charm::MeV,
              m_differential_bins.get_pt_bin(m_truth_D_pt), "Dstar");
        }

        // replace lepton charge with the truth D charge for SPG samples
        //  - OS if they have the same charge
        //  - SS if they have the opposite charge
        if (m_data_period == "ForcedDecay") {
          lep_charge = -1 * m_TruthParticles_Selected_pdgId->at(0) /
                       fabs(m_TruthParticles_Selected_pdgId->at(0));
        }

        // decorate channel name with pass/fail btag
        pass_cuts.at(j) = true;

        // per D-meson b-jet channel
        std::pair<std::string, double> bjet_channel =
            jet_selection_for_sys(j, D_index);

        // b-jet channel
        charged_channel.at(j) += bjet_channel.first + "_Dstar" +
                                 get_charge_prefix(lep_charge, D_index);

        // b-jet scale factor
        extra_weight.at(j) *= bjet_channel.second;

        // Code below is for the Sidebands: Left, SigEnh, and Right.
        if (m_split_into_SB) {
          charged_channel.at(j) += getSidebandStr(m_sys_dmeson_mass.at(j), 142., 148.);
        }
        multiplicity_counter.at(j)[charged_channel.at(j)]++;
        used_channels.insert(charged_channel.at(j));

        // track-jet specific cuts
        if (m_track_jet_selection) {
          float track_jet_radius = std::stoi(m_track_jet_radius) / 10.;
          if (!m_no_d_meson_selection) {
            if (m_sys_matched_track_jet->track_jet_pt.at(j) > 150) {
              pass_cuts.at(j) = false;
            }
            if (m_sys_matched_track_jet->track_jet_zt.at(j) >= 1.0) {
              pass_cuts.at(j) = false;
            }
            if (m_sys_matched_track_jet->track_jet_zt.at(j) < 0.16) {
              pass_cuts.at(j) = false;
            }
            if (fabs(m_sys_matched_track_jet->track_jet_eta.at(j)) >
                2.5 - track_jet_radius) {
              pass_cuts.at(j) = false;
            }
          }
          // TODO: Removal of b-jets inside the track-jet cone
          // if (closest_bjet(j, D_index) < track_jet_radius + 0.4) {
          //   pass_cuts.at(j) = false;
          // }

          // truth fiducial selection
          if (m_truth_matchD && m_is_mc && matched_signal) {
            if (m_sys_matched_track_jet->truth_track_jet_pt.at(j) > 150 ||
                m_sys_matched_track_jet->truth_track_jet_zt.at(j) >= 1.0 ||
                m_sys_matched_track_jet->truth_track_jet_zt.at(j) < 0.16 ||
                fabs(m_sys_matched_track_jet->truth_track_jet_eta.at(j)) > 2.5 - track_jet_radius) {
              matched_signal = false;
              truth_category = "MatchedNoFid";
            }
          }

          // track-jet differential bin
          if (m_bin_in_track_jet_pt) {
            charged_channel.at(j) +=
                "_jet_bin_" +
                std::to_string(
                    getJetBin(m_sys_matched_track_jet->track_jet_pt.at(j)));
          }
        }
      }
    }

    // lepton histograms for this channel
    if (m_do_lep_presel) {
      fill_lepton_histograms(charged_channel, pass_cuts, extra_weight);
    }
    if (m_do_PV) {
      fill_PV_histograms(charged_channel, pass_cuts, extra_weight);
    }

    // track-jet histograms
    if (m_track_jet_selection) {
      fill_trackJet_variables(charged_channel, pass_cuts, extra_weight, D_index,
                              m_sys_matched_track_jet);
    }

    // D star meson specific histograms
    DMesonRec::fill_D_star_histograms(this, charged_channel, D_index,
                                      pass_cuts, extra_weight, false,
                                      m_track_jet_selection);

    if (m_save_output_tree && !m_is_mc) {
      DMesonRec::fill_D_plus_tree(D_index, m_extended_output_branches);
      if (m_track_jet_selection) {
        DMesonRec::fill_track_jet_tree(m_sys_matched_track_jet.get(),
                                       m_track_jet_radius);
      }
      fill_write_tree(charged_channel, pass_cuts, extra_weight);
    }

    // D meson truth match
    if (m_is_mc && m_truth_matchD) {
      auto truth_channel = charged_channel;
      auto truth_channel_fid = std::vector<std::string>(m_nSyst, channel);

      for (unsigned int j = 0; j < m_nSyst; j++) {
        // don't save AntiTight truth categories
        if (m_do_lep_presel && m_sys_anti_tight.at(j)) {
          pass_cuts.at(j) = false;
        }

        if (!m_do_lep_presel ||
            (event_pass_sys(j) && m_sys_lep_charge.at(j) != 0 &&
             pass_cuts.at(j))) {
          truth_channel.at(j) += "_" + truth_category;
          multiplicity_counter.at(j)[truth_channel.at(j)]++;
          used_channels.insert(truth_channel.at(j));
        }
      }

      if (m_do_lep_presel) {
        fill_lepton_histograms(truth_channel, pass_cuts);
      }
      if (!m_fit_variables_only) {
        add_fill_hist_sys("truth_Dmeson_pt", truth_channel, m_truth_D_pt, 150,
                          0, 150., false, pass_cuts, extra_weight);
        add_fill_hist_sys("truth_Dmeson_eta", truth_channel, m_truth_D_eta, 100,
                          -3.0, 3.0, false, pass_cuts, extra_weight);
        add_fill_hist_sys("truth_Dmeson_mass", truth_channel,
                          m_truth_D_mass * Charm::GeV, 200, 1.4, 2.4, false,
                          pass_cuts, extra_weight);
        add_fill_hist_sys("truth_Dmeson_lep_eta", truth_channel,
                          m_truth_abs_lep_eta, 25, 0, 2.5, false, pass_cuts,
                          extra_weight);
        // add_fill_hist_sys("Dmeson_dRlep", truth_channel, sys_dRlep, 120,
        // 0., 6., false, pass_cuts, extra_weight);
        // add_fill_hist_sys("Dmeson_dRbjet", truth_channel, sys_dRbjet, 120,
        // 0., 6., false, pass_cuts, extra_weight);
      }
      if (m_unfolding_variables) {
        // transfer matrix
        // - 1D hists: reco D pt, truth D pt, reco lep |eta|, truth lep |eta|
        // - 2D hists: D pt reco vs truth, lep |eta| reco vs truth
        fill_unfolding_variables(truth_channel, pass_cuts, extra_weight,
                                 D_index, truthIndex);
      }
      if (m_track_jet_selection) {
        // reco quantities
        fill_trackJet_variables(truth_channel, pass_cuts, extra_weight, D_index,
                                m_sys_matched_track_jet, true);
      }
      DMesonRec::fill_D_star_histograms(this, truth_channel, D_index,
                                        pass_cuts, extra_weight, false,
                                        m_track_jet_selection);
      if (m_save_output_tree && m_is_mc) {
        DMesonRec::fill_D_plus_tree(D_index, m_extended_output_branches, true);
        if (m_track_jet_selection) {
          DMesonRec::fill_track_jet_tree(m_sys_matched_track_jet.get(),
                                         m_track_jet_radius);
        }
        fill_truth_D_star_branches(truthIndex);
        fill_write_tree(truth_channel, pass_cuts, extra_weight);
      }
    }
  }

  // number of D meson candidates
  for (auto channel : used_channels) {
    std::vector<float> multiplicity;
    for (unsigned int j = 0; j < m_nSyst; j++) {
      multiplicity.push_back(multiplicity_counter.at(j)[channel]);
    }
    if (!m_fit_variables_only) {
      add_fill_hist_sys("N_Dstar", channel, multiplicity, 20, -0.5, 19.5, true);
    }
  }
}

}  // namespace Charm
