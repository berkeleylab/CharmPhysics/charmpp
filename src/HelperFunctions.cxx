#include "HelperFunctions.h"

#include <stdlib.h>

#include <fstream>
#include <iostream>
#include <sstream>

#include "Definitions.h"

namespace Charm {
TString get_run_number_string(bool is_mc) {
  if (is_mc) {
    return "EventInfo_RandomRunNumber";
  } else {
    return "EventInfo_runNumber";
  }
}

int getJetBin(double tj) {
  if (tj >= jetBinHighEdge[NRANGES - 1]) {
    tj = 149.999;
  }
  int i = 0;
  while (tj >= jetBinHighEdge[i]) {
    i++;
  }
  return i;
}

double getUnfoldingVariable(double tj, double zt) {
  if (zt >= 1.0) {
    zt = 0.99999;
  }
  if (tj >= jetBinHighEdge[NRANGES - 1]) {
    tj = 149.999;
  }
  int i = 0;
  double j = 0.0;

  while (tj >= jetBinHighEdge[i]) {
    i++;
  }

  if (zt < 0.0) {
    zt = 0.0;
  }
  j = NBINSPERRANGE * (zt + i);
  return j;
}

int get_ptRel_variable(double jet_pt, double ptRel) {
  if (ptRel >= 15.0) {
    return -1;
  }
  if (ptRel < 0.0) {
    ptRel = 0.0;
  }
  int i = 0;
  while (ptRel >= pTRelBinHighEdge[i]) {
    i++;
  }
  int j = getJetBin(jet_pt);
  return NBINSPERRANGE * j + i;
}

TString get_pileup_string(bool is_mc, std::string mc_period,
                          std::string data_period) {
  if (is_mc) {
    if (!mc_period.compare("MC16a"))
      return "EventInfo_correctedScaled_averageInteractionsPerCrossing";
    else
      return "EventInfo_correctedScaled_actualInteractionsPerCrossing";
  } else {
    if (!data_period.compare("2015") || !data_period.compare("2016"))
      return "EventInfo_correctedScaled_averageInteractionsPerCrossing";
    else
      return "EventInfo_correctedScaled_actualInteractionsPerCrossing";
  }
}

std::string basename(std::string filePath, bool withExtension, char seperator) {
  // Get last dot position
  std::size_t dotPos = filePath.rfind('.');
  std::size_t sepPos = filePath.rfind(seperator);

  if (sepPos != std::string::npos) {
    return filePath.substr(
        sepPos + 1,
        filePath.size() -
            (withExtension || dotPos != std::string::npos ? 1 : dotPos));
  }
  return "";
}

float get_cross_section(int id) {
  std::string data_dir(std::string(getenv("CHARMPP_BUILD_PATH")) + "/data");
  std::ifstream f(data_dir + "/cross_sections.txt");

  std::string line;

  while (getline(f, line)) {
    if (line.rfind("#", 0) == 0) continue;
    if (line.length() < 6) continue;

    std::istringstream ss(line);
    int dsid;
    std::string name;
    float xsec, kfac, eff, relunc;

    ss >> dsid >> name >> xsec >> kfac >> eff >> relunc;

    if (dsid == id) return xsec * kfac * eff;
  }

  return -1;
}

float get_sum_of_weights(int id, std::string period,
                         bool normalize_to_nevents) {
  std::string data_dir(std::string(getenv("CHARMPP_NTUPLE_PATH")));
  std::ifstream f(data_dir + "/sum_of_weights.txt");

  std::string line;

  while (getline(f, line)) {
    if (line.rfind("#", 0) == 0) continue;

    std::istringstream ss(line);

    std::string sample;

    float sum_of_weights;

    float nevents;

    ss >> sample >> sum_of_weights >> nevents;

    std::stringstream test(sample);
    std::string segment;
    std::vector<std::string> seglist;

    while (std::getline(test, segment, '.')) {
      seglist.push_back(segment);
    }

    std::string ref_period = seglist.at(1);
    int dsid = atoi(seglist.at(2).c_str());

    if (dsid == id && (!ref_period.compare(period))) {
      if (!normalize_to_nevents) {
        return sum_of_weights;
      } else {
        return nevents;
      }
    }
  }

  return 1;
}

float rescaled_d0(float d0, UInt_t run, bool is_mc) {
  if (is_mc) return d0;
  // Apply the d0 shift in 2015-2016 data
  double shift = 0.;
  if (run < 296939)
    shift = 0.00422 * Charm::mm;
  else if (run <= 300908)
    shift = 0.00353 * Charm::mm;
  else if (run <= 311481)
    shift = -0.00335 * Charm::mm;
  return d0 + shift;
}

std::string get_period(UInt_t run) {
  if (run < 296939)
    return "2015";
  else if (run <= 300908)
    return "2016A-B";
  else if (run <= 311481)
    return "2016C-L";
  else if (run <= 341649)
    return "2017";
  else if (run <= 364292)
    return "2018";
  else
    throw std::runtime_error("Unrecognised run number");
}

std::string get_mc_gen(int dsid) {
  if (dsid >= 361100 && dsid <= 361108) {
    // Powheg Z+jets and W+jets samples
    return std::string("powheg");
  } else if ((dsid >= 361510 && dsid <= 361514) ||
             (dsid >= 363123 && dsid <= 363170) ||
             (dsid >= 363600 && dsid <= 363671) ||
             (dsid >= 506193 && dsid <= 506198) ||
             (dsid >= 512198 && dsid <= 512200)) {
    // MadGraph Z+jets and W+jets samples
    return std::string("madgraph");
  } else if ((dsid >= 364100 && dsid <= 364215) ||
             (dsid >= 700320 && dsid <= 700337)) {
    // Sherpa Z+jets and W+jets samples (including Sherpa 2.2.11)
    return std::string("sherpa");
  } else {
    return std::string("other");
  }
}

std::string get_mc_shower(int dsid) {
  if (dsid >= 361100 && dsid <= 361108) {
    // Powheg Z+jets and W+jets samples
    return std::string("pythia");
  } else if ((dsid >= 361510 && dsid <= 361514) ||
             (dsid >= 363123 && dsid <= 363170) ||
             (dsid >= 363600 && dsid <= 363671)) {
    // MadGraph Z+jets and W+jets samples
    return std::string("pythia");
  } else if (dsid == 410470 || dsid == 410471 || dsid == 410472 ||
             dsid == 410464 || dsid == 410465 || dsid == 410480 ||
             dsid == 410482) {
    // Nominal and alternative ttbar samples
    return std::string("pythia");
  } else if ((dsid >= 601616 && dsid <= 601631) || dsid == 410644 ||
             dsid == 410645 || dsid == 410646 || dsid == 410647 ||
             dsid == 410658 || dsid == 410659) {
    // Single top samples
    return std::string("pythia");
  } else if (dsid == 410155 || dsid == 410156 || dsid == 410157 ||
             dsid == 410218 || dsid == 410219 || dsid == 410220) {
    // ttx samples
    return std::string("pythia");
  } else if (dsid >= 364100 && dsid <= 364215) {
    // Sherpa Z+jets and W+jets samples
    return std::string("sherpa");
  } else if ((dsid >= 700320 && dsid <= 700349) ||
             (dsid >= 700320 && dsid <= 700337) || dsid == 700366 ||
             dsid == 700367) {
    // Sherpa2.2.11 Z+jets and W+jets samples
    return std::string("sherpa2211");
  } else if ((dsid >= 700488 && dsid <= 700496) ||
             (dsid >= 700587 && dsid <= 700605)) {
    // Sherpa2.2.11 diboson
    return std::string("sherpa2211");
  } else if ((dsid >= 508979 && dsid <= 508984) ||
             (dsid >= 509751 && dsid <= 509756) || dsid == 501716 ||
             dsid == 501717 || dsid == 507704 || dsid == 507705) {
    // Forced decay NLO MG and FxFx
    return std::string("pythia");
  } else if ((dsid >= 506193 && dsid <= 506198) ||
             (dsid >= 512198 && dsid <= 512200)) {
    // Z+jets FxFx
    return std::string("pythia");
  } else if (dsid == 700686 || dsid == 700687) {
    // Forced Decay Sherpa Z+D
    return std::string("sherpa2211");
  } else if (dsid == 523140 || dsid == 523141) {
    // Forced Decay MG FxFx Z+D
    return std::string("pythia");
  } else if (dsid == 700684 || dsid == 700685) {
    // Forced Decay Sherpa W+DsLambdaC
    return std::string("sherpa2211");
  } else if (dsid == 523138 || dsid == 523139) {
    // Forced Decay NLO MG W+DsLambdaC
    return std::string("pythia");
  } else if (dsid == 410557 || dsid == 410558) {
    // Herwig7.0.4 ttbar
    return std::string("herwig");
  } else {
    return std::string("other");
  }
}

std::string get_sample_type(int dsid) {
  if ((dsid >= 508979 && dsid <= 508984) ||
      (dsid >= 700338 && dsid <= 700343) || dsid == 700366 || dsid == 700367 ||
      dsid == 507704 || dsid == 507705 || dsid == 501716 || dsid == 501717) {
    return "wjets";
  } else if (dsid == 410470 || dsid == 410471 || dsid == 410472 ||
             dsid == 411233 || dsid == 411234 || dsid == 410464 ||
             dsid == 410465 || dsid == 410480 || dsid == 410482) {
    return "ttbar";
  }
  return "other";
}

bool is_forced_decay(int dsid) {
  if (dsid == 700366 || dsid == 700367 || dsid == 700684 || dsid == 700685 ||
      dsid == 523138 || dsid == 523139 || dsid == 700686 || dsid == 700687 ||
      dsid == 501716 || dsid == 501717 || dsid == 507704 || dsid == 507705 ||
      dsid == 523140 || dsid == 523141) {
    return true;
  }
  return false;
}

std::string get_D_plus_mass_region(float mass) {
  if (mass > 1700. && mass <= 1790.) {
    return "SideBand";
  } else if (mass > 1790. && mass <= 1950.) {
    return "Central";
  } else if (mass > 1950. && mass <= 2200.) {
    return "SideBand";
  } else {
    return "";
  }
}

std::string get_charge_prefix(float lep_charge, int pdgId) {
  int charge;
  if (pdgId > 0) {
    charge = 1;
  } else {
    charge = -1;
  }
  if (charge * lep_charge < 0) {
    return "_OS";
  } else {
    return "_SS";
  }
}

std::string get_lepton_charge_string(float charge) {
  if (charge > 0) {
    return "plus";
  } else {
    return "minus";
  }
}

double two_sided_cb_function(double x, const double *par) {
  Double_t alphaHi = par[0];
  Double_t alphaLo = par[1];
  Double_t mean = par[2];
  Double_t nHi = par[3];
  Double_t nLo = par[4];
  Double_t sigma = par[5];
  Double_t t = (x - mean) / sigma;

  if (t < -alphaLo) {
    Double_t a = exp(-0.5 * alphaLo * alphaLo);
    Double_t b = nLo / alphaLo - alphaLo;
    return a / TMath::Power(alphaLo / nLo * (b - t), nLo);
  } else if (t > alphaHi) {
    Double_t a = exp(-0.5 * alphaHi * alphaHi);
    Double_t b = nHi / alphaHi - alphaHi;
    return a / TMath::Power(alphaHi / nHi * (b + t), nHi);
  }
  return exp(-0.5 * t * t);
}

}  // namespace Charm
