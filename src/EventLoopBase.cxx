#include "EventLoopBase.h"

#include <TH1D.h>
#include <TROOT.h>
#include <TRegexp.h>
#include <TTreeCacheUnzip.h>

#include <chrono>
#include <memory>
#include <sstream>

namespace Charm {
// set and get config
void EventLoopBase::set_sys_config(std::string name) {
  m_sys_config_name = name;
  m_sys_config = YAML::LoadFile(std::string(getenv("CHARMPP_SOURCE_PATH")) +
                                "/config/sys/" + name + ".yaml");
  m_output_file_name.ReplaceAll(".root", "." + m_sys_config_name + ".root");
  std::cout << "Set systematics config: " << name << std::endl;
}

// set and get config
void EventLoopBase::set_config(std::string name) {
  m_config = YAML::LoadFile(std::string(getenv("CHARMPP_SOURCE_PATH")) +
                            "/config/" + name + ".yaml");
  std::cout << "Set analysis config: " << name << std::endl;
}

void EventLoopBase::apply_overrides(const std::map<std::string, std::string> &overrides) {
    for (const auto &override : overrides) {
        m_config[override.first] = YAML::Node(override.second);
    }
}

void EventLoopBase::run(long long start_event, long long end_event) {
  // application start time
  auto start = std::chrono::high_resolution_clock::now();

  // edge case
  if ((start_event >= m_num_events) ||
      ((end_event != -1) && start_event >= end_event)) {
    return;
  }

  long long first = 0;
  if (start_event > 0 && start_event < m_num_events) {
    first = start_event;
  }

  long long last = m_num_events;
  if (end_event > 0 && end_event < m_num_events) {
    last = end_event;
  }

  // output file name
  if (end_event > 0) {
    m_output_file_name.ReplaceAll(".root", "." + std::to_string(start_event) +
                                               "." + std::to_string(end_event) +
                                               ".root");
  }

  // read meta data
  read_meta_data();

  // initialize
  initialize();

  // setup histograms
  setup_histograms();

  // no events
  if (m_num_events == 0) {
    return;
  }

  // output
  TFile out_file(m_out_path + "/" + m_output_file_name, "RECREATE");
  out_file.cd();

  // initialize output tree if needed
  initialize_output_tree();

  // setup branches (need metadata first)
  connect_branches();

  // cache tree
  auto start_setup_cache = std::chrono::high_resolution_clock::now();
  setup_cache(first, last);
  auto finish_setup_cache = std::chrono::high_resolution_clock::now();
  std::chrono::duration<double> elapsed_setup_cache =
      finish_setup_cache - start_setup_cache;
  m_timing_map["setup_cache"] = elapsed_setup_cache.count();

  // loop over all events
  auto start_loop = std::chrono::high_resolution_clock::now();
  loop(first, last);
  auto finish_loop = std::chrono::high_resolution_clock::now();
  std::chrono::duration<double> elapsed_loop = finish_loop - start_loop;
  m_timing_map["loop"] = elapsed_loop.count();

  // finalize
  finalize();

  // save_cutflow_histogram();
  save_histograms(out_file);

  // application end time
  auto finish = std::chrono::high_resolution_clock::now();
  std::chrono::duration<double> elapsed = finish - start;
  m_timing_map["run_time"] = elapsed.count();

  // save timing
  // out_file.cd();
  // save_timing_histogram();

  // write output tree
  if (get_config()["save_output_tree"].as<bool>(false)) {
    m_out_tree->Write();
  }

  // close file
  out_file.Close();

  // print cache diagnostics
  m_tree->PrintCacheStats();
}

void EventLoopBase::initialize_output_tree() {
  if (!get_config()["save_output_tree"].as<bool>(false)) {
    return;
  }

  // create new tree
  m_out_tree = new TTree("MiniCharm", "MiniCharm");

  // base branches
  m_out_tree->Branch("channel", &m_out_channel);
  m_out_tree->Branch("sign", &m_out_sign);
  m_out_tree->Branch("truth_category", &m_out_truth_category);
  m_out_tree->Branch("matrix_method_category", &m_out_matrix_method_channel);
  m_out_tree->Branch("pass_cuts", &m_out_pass_cuts);
  m_out_tree->Branch("weight", &m_out_weight);
  m_out_tree->Branch("top_weight", &m_out_top_weight);
  m_out_tree->Branch("eventNumber", &m_EventInfo_eventNumber);

  return;
}

void EventLoopBase::read_meta_data() {
  // extract DSID from name
  TString input_file_name = m_input_file->GetName();
  m_dataset_id_from_file_name =
      TString(input_file_name(TRegexp("[0-9][0-9][0-9][0-9][0-9][0-9]")).Data())
          .Atoi();
  std::cout << "m_dataset_id_from_file_name " << m_dataset_id_from_file_name
            << std::endl;

  std::cout << "read_meta_data" << std::endl;
  m_save_nominal = false;

  std::cout << get_sys_config() << std::endl;

  // run only over a subset of systematics
  // if specified in the config file
  for (auto sys : get_sys_config()["systematics"]) {
    std::string val = sys.as<std::string>();
    if (val == "nominal") {
      std::cout << "save nominal" << std::endl;
      m_save_nominal = true;
    } else {
      m_do_systematics = true;
      std::cout << "systematics group " << val << std::endl;
      m_used_systematics.push_back(val);
    }
  }

  for (auto *key : *m_input_file->GetListOfKeys()) {
    TString obj_name = key->GetName();

    if (obj_name.BeginsWith("CutBookkeeper_") && obj_name.EndsWith("_NOSYS")) {
      std::cout << "Nominal CutBookkeeper: " << obj_name << std::endl;

      // MC
      m_is_mc = true;
      m_no_weights = false;

      // extract DSID
      m_dataset_id = TString(obj_name(TRegexp("[0-9]+")).Data()).Atoi();
      m_mc_gen = get_mc_gen(m_dataset_id);
      m_mc_shower = get_mc_shower(m_dataset_id);
      m_sample_type = get_sample_type(m_dataset_id);
      m_is_forced_decay = is_forced_decay(m_dataset_id);

      std::cout << "m_dataset_id " << m_dataset_id << std::endl;
      std::cout << "m_mc_gen " << m_mc_gen << std::endl;
      std::cout << "m_mc_shower " << m_mc_shower << std::endl;
      std::cout << "m_sample_type " << m_sample_type << std::endl;
      std::cout << "m_is_forced_decay " << m_is_forced_decay << std::endl;

      // extract period (MC16a/d/e)
      std::string file_name = std::string(m_output_file_name);
      std::stringstream test(file_name);
      std::string segment;
      std::vector<std::string> seglist;
      while (std::getline(test, segment, '.')) {
        seglist.push_back(segment);
      }

      m_mc_period = seglist.at(1);

      std::cout << "m_mc_period " << m_mc_period << std::endl;

      if (m_mc_period.compare("MC16a") && m_mc_period.compare("MC16d") &&
          m_mc_period.compare("MC16e"))
        throw std::runtime_error(
            "breaking execution; mc period not recognized");

      // get cross-sectuon
      m_cross_section = Charm::get_cross_section(m_dataset_id_from_file_name);

      std::cout << "m_cross_section " << m_cross_section << std::endl;

      if (m_cross_section < 0) {
        throw std::runtime_error("cross-section not found");
      }

      // get initial sum of weights
      m_init_sum_of_w =
          Charm::get_sum_of_weights(m_dataset_id_from_file_name, m_mc_period);

      std::cout << "m_init_sum_of_w " << m_init_sum_of_w << std::endl;

      // lumi
      if (!m_mc_period.compare("MC16a")) {
        m_lumi = Charm::LUMI_2015 + Charm::LUMI_2016;
      } else if (!m_mc_period.compare("MC16d")) {
        m_lumi = Charm::LUMI_2017;
      } else {
        m_lumi = Charm::LUMI_2018;
      }

      std::cout << "m_lumi " << m_lumi << std::endl;

    } else if (obj_name.BeginsWith("CutBookkeeper_")) {
      // std::cout << "Sys CutBookkeeper: " << obj_name << std::endl;
    } else if (m_do_systematics && obj_name.BeginsWith("systematics_")) {
      std::cout << "Systematics group: " << obj_name << std::endl;
      if ((std::find(m_used_systematics.begin(), m_used_systematics.end(),
                     std::string(obj_name)) == m_used_systematics.end())) {
        std::cout << "skipping..." << std::endl;
        continue;
      }
      m_systematics_groups[obj_name] = {};
      auto sys_group = std::make_unique<TH1F>(
          *dynamic_cast<TH1F *>(m_input_file->Get(obj_name)));
      for (int i = 1; i <= sys_group->GetNbinsX(); i++) {
        if (strcmp(sys_group->GetXaxis()->GetBinLabel(i), "NOSYS")) {
          std::cout << " - " << sys_group->GetXaxis()->GetBinLabel(i)
                    << std::endl;
          if ((std::string(sys_group->GetXaxis()->GetBinLabel(i))
                   .find("WeightNormalisation") != std::string::npos) ||
              (std::string(sys_group->GetXaxis()->GetBinLabel(i))
                   .find("NTrials") != std::string::npos) ||
              (std::string(sys_group->GetXaxis()->GetBinLabel(i))
                   .find("MEWeight") != std::string::npos)  //||
          ) {
            std::cout << "skipping..." << std::endl;
            continue;
          }
          // if name not in Charm::GEN_VARIATIONS vector, skip
          if (get_config()["reduced_gen_variations"].as<bool>(false) && obj_name == "systematics_GEN") {
            if (std::find(Charm::GEN_VARIATIONS.begin(),
                          Charm::GEN_VARIATIONS.end(),
                          std::string(sys_group->GetXaxis()->GetBinLabel(i))) ==
                Charm::GEN_VARIATIONS.end()) {
              std::cout << "skipping GEN variation..." << std::endl;
              continue;
            }
          }
          // print out the kept ones
          std::cout << "kept!" << std::endl;
          m_systematics.push_back(
              std::string(sys_group->GetXaxis()->GetBinLabel(i)));
          m_systematics_groups[obj_name].push_back(
              std::string(sys_group->GetXaxis()->GetBinLabel(i)));
        }
      }
    }
  }

  if (!m_is_mc) {
    // extract period (2015/2016/2017/2018)
    std::string file_name = std::string(m_output_file_name);
    std::stringstream test(file_name);
    std::string segment;
    std::vector<std::string> seglist;
    while (std::getline(test, segment, '.')) {
      seglist.push_back(segment);
    }

    m_data_period = seglist.at(1);

    // running on MinBias sample
    if (m_data_period == "2015" || m_data_period == "2016" ||
        m_data_period == "2017" || m_data_period == "2018") {
      m_is_mc = false;
    } else if (m_data_period == "ForcedDecay" ||
               m_data_period == "SingleParticleGun" ||
               m_data_period == "MinBias" || m_data_period == "999914" ||
               m_data_period == "999934" || m_data_period == "999912" ||
               m_data_period == "999918" || m_data_period == "999924" ||
               m_data_period == "999951" || m_data_period == "999953") {
      m_is_mc = true;
      m_no_weights = true;
      m_dataset_id = std::stoi(seglist.at(2));
      m_init_sum_of_w =
          Charm::get_sum_of_weights(m_dataset_id, "ForcedDecay", true);
      m_cross_section = 1.0;
      m_lumi = Charm::LUMI_RUN2;
      std::cout << "m_data_period " << m_data_period << std::endl;
      std::cout << "m_lumi " << m_lumi << std::endl;
      std::cout << "m_dataset_id " << m_dataset_id << std::endl;
      std::cout << "m_init_sum_of_w " << m_init_sum_of_w << std::endl;
    } else {
      m_is_mc = true;
      if (!m_mc_period.compare("MC16a")) {
        m_lumi = Charm::LUMI_2015 + Charm::LUMI_2016;
      } else if (!m_mc_period.compare("MC16d")) {
        m_lumi = Charm::LUMI_2017;
      } else {
        m_lumi = Charm::LUMI_2018;
      }
      m_dataset_id = std::stoi(seglist.at(2));
      m_cross_section = Charm::get_cross_section(m_dataset_id);
      m_init_sum_of_w = Charm::get_sum_of_weights(m_dataset_id, "Truth");
      m_mc_gen = get_mc_gen(m_dataset_id);
      m_mc_shower = get_mc_shower(m_dataset_id);
      m_sample_type = get_sample_type(m_dataset_id);
      m_is_forced_decay = is_forced_decay(m_dataset_id);

      if (m_init_sum_of_w == 1) {
        std::cout << "patching for v8 truth ntuples..." << std::endl;
        m_init_sum_of_w = Charm::get_sum_of_weights(m_dataset_id, "MC16e");
      }
      std::cout << "m_lumi " << m_lumi << std::endl;
      std::cout << "m_dataset_id " << m_dataset_id << std::endl;
      std::cout << "m_cross_section " << m_cross_section << std::endl;
      std::cout << "m_init_sum_of_w " << m_init_sum_of_w << std::endl;
      std::cout << "m_is_forced_decay " << m_is_forced_decay << std::endl;
    }

    std::cout << "m_data_period " << m_data_period << std::endl;
  }

  // read initial sum of weights for systematics
  if (m_is_mc && m_do_systematics) {
    YAML::Node sum_of_weights =
        YAML::LoadFile(std::string(getenv("CHARMPP_NTUPLE_PATH")) +
                       "/sum_of_weights_sys.yaml");
    size_t lastindex = std::string(m_output_file_name).find_last_of(".");
    std::string rawname = std::string(m_output_file_name).substr(0, lastindex);
    m_init_sum_of_w_sys = sum_of_weights[rawname];
    for (unsigned int i = 0; i < m_systematics.size(); i++) {
      std::cout << m_systematics.at(i) << std::endl;
      if (m_init_sum_of_w_sys[m_systematics.at(i)]) {
        float weight = m_init_sum_of_w_sys[m_systematics.at(i)].as<float>();
        std::cout << "sum of weights for sys: " << weight << std::endl;
        if (!isnan(weight)) {
          m_init_sum_of_w_sys_vec.push_back(weight);
        } else {
          m_init_sum_of_w_sys_vec.push_back(m_init_sum_of_w);
        }
      } else {
        m_init_sum_of_w_sys_vec.push_back(m_init_sum_of_w);
      }
    }
  }

  // Matrix Method systematics
  if (m_used_systematics.size() &&
      (std::find(m_used_systematics.begin(), m_used_systematics.end(),
                 "systematics_MM") != m_used_systematics.end())) {
    m_systematics_groups["systematics_MM"] = {};
    for (auto sys : m_matrix_method_helper.sys_variations) {
      m_systematics.push_back(sys);
      m_systematics_groups["systematics_MM"].push_back(sys);
    }
  }

  // Matrix Method systematics (inclusive)
  if (m_used_systematics.size() &&
      (std::find(m_used_systematics.begin(), m_used_systematics.end(),
                 "systematics_INCL_MM") != m_used_systematics.end())) {
    m_systematics_groups["systematics_INCL_MM"] = {};
    for (auto sys : m_matrix_method_helper.sys_variations_incl) {
      m_systematics.push_back(sys);
      m_systematics_groups["systematics_INCL_MM"].push_back(sys);
    }
  }

  // Track Efficiency systematics
  if (m_is_mc && m_used_systematics.size() &&
      (std::find(m_used_systematics.begin(), m_used_systematics.end(),
                 "systematics_TRACK_EFF") != m_used_systematics.end())) {
    m_systematics_groups["systematics_TRACK_EFF"] = {};
    for (auto sys : m_track_sys_helper.sys_variations) {
      m_systematics.push_back(sys);
      m_systematics_groups["systematics_TRACK_EFF"].push_back(sys);
    }
  }

  // Mass Width systematics
  if (m_is_mc && m_used_systematics.size() &&
      (std::find(m_used_systematics.begin(), m_used_systematics.end(),
                 "systematics_DMESON_RESO") != m_used_systematics.end())) {
    m_systematics_groups["systematics_DMESON_RESO"] = {};
    for (auto sys : m_mass_smear_helper.sys_variations) {
      m_systematics.push_back(sys);
      m_systematics_groups["systematics_DMESON_RESO"].push_back(sys);
    }
  }

  // Wjets theory systematics
  if (m_is_mc && m_used_systematics.size() &&
      (std::find(m_used_systematics.begin(), m_used_systematics.end(),
                 "systematics_WJETS_THEORY") != m_used_systematics.end())) {
    m_systematics_groups["systematics_WJETS_THEORY"] = {};
    for (auto sys : m_nlo_mg_sys_helper.sys_variations) {
      m_systematics.push_back(sys);
      m_systematics_groups["systematics_WJETS_THEORY"].push_back(sys);
    }
  }

  // Production fraction systematics
  if (m_is_mc && m_used_systematics.size() &&
      (std::find(m_used_systematics.begin(), m_used_systematics.end(),
                 "systematics_PROD_FRAC_OLD") != m_used_systematics.end())) {
    m_systematics_groups["systematics_PROD_FRAC_OLD"] = {};
    for (auto sys : m_prod_frac_weight_helper.sys_variations) {
      m_systematics.push_back(sys);
      m_systematics_groups["systematics_PROD_FRAC_OLD"].push_back(sys);
    }
  }

  // Modeling uncertainty systematics
  if (m_is_mc && m_used_systematics.size() &&
      (std::find(m_used_systematics.begin(), m_used_systematics.end(),
                 "systematics_FIDUCIAL_EFF") != m_used_systematics.end())) {
    m_systematics_groups["systematics_FIDUCIAL_EFF"] = {};
    for (auto sys : m_modeling_uncertainty_helper.sys_variations) {
      m_systematics.push_back(sys);
      m_systematics_groups["systematics_FIDUCIAL_EFF"].push_back(sys);
    }
  }

  // Dplus branching ratio background uncertainties
  if (m_is_mc && m_used_systematics.size() &&
      (std::find(m_used_systematics.begin(), m_used_systematics.end(),
                 "systematics_DPLUS_BKG_BR") != m_used_systematics.end())) {
    m_systematics_groups["systematics_DPLUS_BKG_BR"] = {};
    for (auto sys : m_dplus_bkg_br_uncertainty.sys_variations) {
      m_systematics.push_back(sys);
      m_systematics_groups["systematics_DPLUS_BKG_BR"].push_back(sys);
    }
  }

  std::cout << "m_systematics.size() " << m_systematics.size() << std::endl;
  m_nSyst = m_systematics.size() + 1;
}

void EventLoopBase::setup_histograms() {
  // systematics
  std::cout << "Will evaluate " << m_systematics.size() << " systematics"
            << std::endl;
  std::cout << "Found " << m_channels.size() << " channels" << std::endl;

  // initialize systematics
  for (std::string channel : m_channels) {
    m_histograms[channel] = {};
    m_histograms_sys[channel] = {};
  }

  // event weight
  for (unsigned int i = 0; i < m_systematics.size(); i++) {
    m_event_weight_sys.push_back(1.0);
  }
  m_event_weight = 1.0;
}

void EventLoopBase::setup_cache(long long first, long long last) {
  // by setting the read cache to -1 we set it to the AutoFlush value when
  // writing
  m_tree->SetCacheSize(-1);
  m_tree->SetBranchStatus("*", kFALSE);

  for (auto *br : m_preselection_branches) {
    // m_tree->AddBranchToCache(br, true);
    m_tree->SetBranchStatus(br->GetName(), kTRUE);
  }
  for (auto *br : m_branches) {
    // m_tree->AddBranchToCache(br, true);
    m_tree->SetBranchStatus(br->GetName(), kTRUE);
  }
  for (auto *br : m_other_branches) {
    // m_tree->AddBranchToCache(br, true);
    m_tree->SetBranchStatus(br->GetName(), kTRUE);
  }

  // parallel unzip
  // ROOT::EnableImplicitMT();
  // TTreeCacheUnzip::SetParallelUnzip(TTreeCacheUnzip::kEnable);
  // m_tree->SetParallelUnzip(true);

  // put branches used in the first entry to the cache
  m_tree->SetCacheLearnEntries(1);
  m_tree->SetCacheEntryRange(first, last);
  m_tree->StopCacheLearningPhase();
}

void EventLoopBase::loop(long long first, long long last) {
  double elapsed_load_tree(0);
  double elapsed_load_preselection(0);
  double elapsed_preselection(0);
  double elapsed_load_remaining(0);
  double elapsed_execute(0);
  auto load_tree = std::chrono::high_resolution_clock::now();
  auto load_preselection = std::chrono::high_resolution_clock::now();
  auto preselection = std::chrono::high_resolution_clock::now();
  auto failed_preselection = std::chrono::high_resolution_clock::now();
  auto load_remaining = std::chrono::high_resolution_clock::now();
  auto execute = std::chrono::high_resolution_clock::now();
  auto finish = std::chrono::high_resolution_clock::now();

  std::cout << "Processing events from " << first << " to " << last
            << std::endl;

  // loop over all events
  for (m_current_event = first; m_current_event < last; m_current_event++) {
    // start time
    if (m_enable_loop_timing) {
      load_tree = std::chrono::high_resolution_clock::now();
    }

    // reset channel
    m_channel = "";
    m_channel_sys = std::vector<std::string>(m_nSyst, "");
    m_event_weight_sys = std::vector<float>(m_systematics.size(), 1.0);
    // for (unsigned int i = 0; i < m_systematics.size(); i++) {
    //     m_event_weight_sys.at(i) = 1.0;
    // }
    m_event_weight = 1.0;
    m_sys_weight.clear();

    // read only pre-selection branches
    int64_t localEntry = m_tree->LoadTree(m_current_event);

    // checkpoint: load_preselection
    if (m_enable_loop_timing) {
      load_preselection = std::chrono::high_resolution_clock::now();
      elapsed_load_tree +=
          std::chrono::duration<double>(load_preselection - load_tree).count();
    }

    // read only pre-selection branches
    for (auto *br : m_preselection_branches) {
      br->GetEntry(localEntry);
    }

    // checkpoint: preselection
    if (m_enable_loop_timing) {
      preselection = std::chrono::high_resolution_clock::now();
      elapsed_load_preselection +=
          std::chrono::duration<double>(preselection - load_preselection)
              .count();
    }

    // call preselection
    if (!this->preselection()) {
      if (m_enable_loop_timing) {
        failed_preselection = std::chrono::high_resolution_clock::now();
        elapsed_preselection +=
            std::chrono::duration<double>(failed_preselection - preselection)
                .count();
      }
      continue;
    }

    // checkpoint: load_remaining
    if (m_enable_loop_timing) {
      load_remaining = std::chrono::high_resolution_clock::now();
      elapsed_preselection +=
          std::chrono::duration<double>(load_remaining - preselection).count();
    }

    // read the rest of branches
    // m_tree->GetEntry(localEntry);
    for (auto *br : m_branches) {
      br->GetEntry(localEntry);
    }

    // checkpoint: execute
    if (m_enable_loop_timing) {
      execute = std::chrono::high_resolution_clock::now();
      elapsed_load_remaining +=
          std::chrono::duration<double>(execute - load_remaining).count();
    }

    // call execute
    if (this->execute() < 0) {
      throw std::runtime_error("breaking execution");
    }

    // end timing
    if (m_enable_loop_timing) {
      finish = std::chrono::high_resolution_clock::now();
      elapsed_execute +=
          std::chrono::duration<double>(finish - execute).count();
    }
  }

  m_timing_map["load_tree"] = elapsed_load_tree;
  m_timing_map["load_preselection"] = elapsed_load_preselection;
  m_timing_map["preselection"] = elapsed_preselection;
  m_timing_map["load_remaining"] = elapsed_load_remaining;
  m_timing_map["execute"] = elapsed_execute;
}

void EventLoopBase::initialize() {}

void EventLoopBase::connect_branches() {}

bool EventLoopBase::preselection() { return true; }

int EventLoopBase::execute() { return -1; }

void EventLoopBase::finalize() {}

void EventLoopBase::add_channel(std::string channel,
                                std::vector<std::string> cuts) {
  m_channels.push_back(channel);
  int size = cuts.size();
  if (size == 0) {
    size = 1;
  }

  TString name = channel + "_cutflow";
  m_cutflow_histograms[channel] = new TH1D(name, name, size, 0, size);
  m_cutflow_histograms[channel]->SetMinimum(1e-3);

  int index = 0;
  for (TString name : cuts) {
    index++;
    std::cout << "initializing cut name: " << name << " for channel " << channel
              << std::endl;
    m_cutflow_histograms[channel]->GetXaxis()->SetBinLabel(index, name);
  }
}

void EventLoopBase::save_histograms(TFile &out_file) {
  out_file.cd();
  if (m_save_nominal) {
    for (const auto &channel : m_histograms) {
      if (!channel.second.size()) {
        continue;
      }
      for (const auto &val : channel.second) {
        val.second->Write();
      }
    }
  }
  if (m_do_systematics) {
    for (unsigned int i = 0; i < m_systematics.size(); i++) {
      for (const auto &channel : m_histograms_sys) {
        if (!channel.second.size()) {
          continue;
        }
        for (const auto &val : channel.second) {
          auto *histograms_sys = &m_histograms_sys[channel.first][val.first];
          if (histograms_sys->at(i)) {
            histograms_sys->at(i)->Write();
          }
        }
      }
    }
  }
}

void EventLoopBase::save_cutflow_histogram() {
  for (auto &channel : m_channels) {
    std::cout << ":: dumping cutflow for channel " << channel
              << " ::" << std::endl;

    for (int i = 1; i < m_cutflow_histograms[channel]->GetNbinsX() + 1; i++) {
      float value = m_cutflow_histograms[channel]->GetBinContent(i);
      std::cout << m_cutflow_histograms[channel]->GetXaxis()->GetBinLabel(i)
                << " " << value << std::endl;
    }
    m_cutflow_histograms[channel]->Write();
  }
}

void EventLoopBase::save_timing_histogram() {
  int size = m_timing_map.size();

  TH1D *timing_histogram = new TH1D("timing", "timing", size, 0, size);
  std::vector<std::string> stages = {
      "load",         "run_time",       "setup_cache",
      "loop",         "load_tree",      "load_preselection",
      "preselection", "load_remaining", "execute",
  };

  std::cout << ":: application timing ::" << std::endl;

  int index = 0;
  for (std::string &stage : stages) {
    index++;
    timing_histogram->GetXaxis()->SetBinLabel(index, stage.c_str());
    timing_histogram->SetBinContent(index, m_timing_map[stage]);
    std::cout << stage << " : " << m_timing_map[stage] << " seconds"
              << std::endl;
  }
  timing_histogram->Write();
}

void EventLoopBase::increment_cutflow(std::string channel, float index) {
  m_cutflow_histograms.at(channel)->Fill(index - 0.5);
}

void EventLoopBase::read_data_file(
    std::string path,
    std::unordered_map<std::string, std::unique_ptr<TH1>> *histogram_map) {
  TFile *file = new TFile(path.c_str(), "READ");
  for (auto *key : *file->GetListOfKeys()) {
    if (key) {
      (*histogram_map)[key->GetName()] =
          std::unique_ptr<TH1>(dynamic_cast<TH1 *>(file->Get(key->GetName())));
    }
  }
}

void EventLoopBase::read_data_file(
    std::string path,
    std::unordered_map<std::string, std::unique_ptr<TObject>> *histogram_map) {
  TFile *file = new TFile(path.c_str(), "READ");
  for (auto *key : *file->GetListOfKeys()) {
    if (key) {
      (*histogram_map)[key->GetName()] = std::unique_ptr<TObject>(
          dynamic_cast<TObject *>(file->Get(key->GetName())));
    }
  }
}

EventLoopBase::EventLoopBase(TString input_file, TString out_path,
                             TString tree_name)
    : m_tree_name(tree_name),
      m_cutflow_histograms(),
      m_out_path(out_path),
      m_timing_map(),
      m_is_mc(false),
      m_dataset_id(-1),
      m_mc_period(""),
      m_cross_section(0),
      m_lumi(0),
      m_current_event(0),
      m_init_sum_of_w(0),
      m_enable_loop_timing(false) {
  // start time
  auto start = std::chrono::high_resolution_clock::now();

  // read root file
  m_input_file = new TFile(input_file);

  // read tree
  m_tree = dynamic_cast<TTree *>(m_input_file->Get(m_tree_name));

  // number of events
  if (m_tree) {
    m_num_events = m_tree->GetEntries();
  } else {
    m_num_events = 0;
  }

  // output file name
  m_output_file_name = Charm::basename(std::string(input_file));

  // number of read events
  std::cout << "Read " << m_num_events << " events" << std::endl;

  // W+D fake rate file
  read_data_file(std::string(getenv("CHARMPP_DATA_PATH")) +
                     "/matrix_method/FakeRates_v12_all_v2.root",
                 &m_fake_rate_map);
  m_matrix_method_helper.set_fake_rate(&m_fake_rate_map);

  // W+D real rate file
  read_data_file(std::string(getenv("CHARMPP_DATA_PATH")) +
                     "/matrix_method/RealRates_v12_v2.root",
                 &m_real_rate_map);
  m_matrix_method_helper.set_real_rate(&m_real_rate_map);
  std::cout << "found " << m_real_rate_map.size()
            << " histograms in real rate file" << std::endl;

  // W+D closure hists
  read_data_file(
      std::string(getenv("CHARMPP_DATA_PATH")) + "/matrix_method/RW_Hists.root",
      &m_rw_hists_map);
  m_matrix_method_helper.set_rw_hists(&m_rw_hists_map);
  std::cout << "found " << m_rw_hists_map.size()
            << " histograms in rw hists file" << std::endl;

  // Inclusive W+jets fake rate file
  read_data_file(std::string(getenv("CHARMPP_DATA_PATH")) +
                     "/matrix_method/FakeRates_inclusive_v11_all.root",
                 &m_fake_rate_map_incl);
  m_matrix_method_helper.set_fake_rate_incl(&m_fake_rate_map_incl);

  // Inclusive W+jets real rate file
  read_data_file(std::string(getenv("CHARMPP_DATA_PATH")) +
                     "/matrix_method/RealRates_inclusive_v11.root",
                 &m_real_rate_map_incl);
  m_matrix_method_helper.set_real_rate_incl(&m_real_rate_map_incl);

  // Track Efficiency Systematic Histograms
  read_data_file(std::string(getenv("CHARMPP_DATA_PATH")) +
                     "/systematics/track_eff_systematics.v4.root",
                 &m_track_eff_systematics);
  m_track_sys_helper.set_map(&m_track_eff_systematics);

  // Mass Width Systematic Histograms
  m_mass_smear_helper.set_map(&m_track_eff_systematics);

  // NLO MG weights
  read_data_file(std::string(getenv("CHARMPP_DATA_PATH")) +
                     "/systematics/MGSherpaWeights.root",
                 &m_nlo_mg_weights);
  m_nlo_mg_sys_helper.set_map(&m_nlo_mg_weights);

  // Fiducial efficiency systematics
  read_data_file(std::string(getenv("CHARMPP_DATA_PATH")) +
                     "/systematics/FidEffSherpa.v3.root",
                 &m_dplus_fid_eff_weights);
  m_modeling_uncertainty_helper.set_map(&m_dplus_fid_eff_weights);

  // SPG Dplus and Dstar histograms
  read_data_file(
      std::string(getenv("CHARMPP_DATA_PATH")) + "/spg/Dmeson_mass_fit.root",
      &m_spg_dmeson_weights);
  m_spg_forced_decay_helper.set_map(&m_spg_dmeson_weights);

  // SPG Dplus and Dstar histograms
  read_data_file(
      std::string(getenv("CHARMPP_DATA_PATH")) + "/spg/DPlus_bkg_unc.root",
      &m_dplus_bkg_br_histos);
  m_dplus_bkg_br_uncertainty.set_map(&m_dplus_bkg_br_histos);

  // checkpoint time
  auto finish = std::chrono::high_resolution_clock::now();
  std::chrono::duration<double> elapsed = finish - start;
  m_timing_map["load"] = elapsed.count();
}

}  // namespace Charm
