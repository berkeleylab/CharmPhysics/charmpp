#include <math.h>

#include <iostream>

#include "CharmLoopBase.h"
#include "HelperFunctions.h"
#include "TVector2.h"

namespace Charm {
void CharmLoopBase::get_jets() {
  m_jets_sys.clear();
  if (!m_do_lep_presel) {
    return;
  }
  // calibration sys
  for (unsigned int j = 0; j < m_nSyst; j++) {
    m_jets_sys.push_back({});
    if (m_sys_jet_pt.at(j)->size() != m_AnalysisJets_pt_NOSYS->size()) {
      std::cout << "ERROR: jet size mismatch for event: "
                << m_EventInfo_eventNumber
                << " and sys: " << m_systematics.at(j) << std::endl;
      return;
    }
    for (unsigned int i = 0; i < m_sys_jet_pt.at(j)->size(); i++) {
      if (fabs(m_AnalysisJets_eta->at(i)) <= 5.0 &&
          m_sys_jet_pt.at(j)->at(i) * Charm::GeV >= 20. &&
          m_sys_jet_selected.at(j)->at(i)) {
        
        // for FTAG calibration remove forward jets
        if (m_dmeson_jet_matching)
        {
            if (fabs(m_AnalysisJets_eta->at(i)) > 2.5)
            {
                continue;
            }
        }

        m_jets_sys.at(j).push_back(i);

      }
    }
  }
}

void CharmLoopBase::jet_selection_for_sys() {
  std::cout << "WARNING: D-meson / b-jet OR no longer implemented\nPlease use "
               "CharmLoopBase::jet_selection_for_sys(unsigned int i, unsigned "
               "int index) instead"
            << std::endl;
  if (!m_do_lep_presel) {
    return;
  }
  for (unsigned int i = 0; i < m_nSyst; i++) {
    if (m_channel_sys.at(i) == "") {
      continue;
    }
    auto jets = m_jets_sys.at(i);
    for (auto &jet : jets) {
      if (!m_dmeson_jet_matching){
        // ftag SF
        if (m_is_mc) {
          if (fabs(m_AnalysisJets_eta->at(jet)) <= 2.5) {
            multiply_event_weight(m_sys_jet_ftag_eff.at(i)->at(jet), i);
          }
        }
      }
      if (m_AnalysisJets_ftag_select_DL1r_FixedCutBEff_70->at(jet)) {
        m_sys_nbjets.at(i) += 1;
      }
    }
    if (m_two_tag_region) {
      if (m_sys_nbjets.at(i) == 0) {
        m_channel_sys.at(i) += "_0tag";
      } else if (m_sys_nbjets.at(i) == 1) {
        m_channel_sys.at(i) += "_1tag";
      } else if (m_sys_nbjets.at(i) >= 2) {
        m_channel_sys.at(i) += "_2tag";
      }
    } else {
      if (m_sys_nbjets.at(i) == 0) {
        m_channel_sys.at(i) += "_0tag";
      } else if (m_sys_nbjets.at(i) >= 1) {
        m_channel_sys.at(i) += "_1tag";
      }
    }
  }
}

std::pair<std::string, double> CharmLoopBase::jet_selection_for_sys(
    unsigned int i, unsigned int index) {
  unsigned int n_bjet = 0;
  auto jets = m_jets_sys.at(i);
  double jet_sf = 1;
  std::string channel = "";
  for (auto &jet : jets) {
    // remove jet overlapping with the D-meson
    float dR = get_dr_bjet_Dmeson(jet, index);
    if (dR < 0.4) {
      continue;
    }
    //ftag SF
    if (m_apply_FTAG_SF){
      if (m_is_mc) {
        if (fabs(m_AnalysisJets_eta->at(jet)) <= 2.5) {
          jet_sf *= m_sys_jet_ftag_eff.at(i)->at(jet);
        }
      }
    }
    if (m_AnalysisJets_ftag_select_DL1r_FixedCutBEff_70->at(jet)) {
      n_bjet += 1;
    }
  }
  if (m_two_tag_region) {
    if (n_bjet == 0) {
      channel = "_0tag";
    } else if (n_bjet == 1) {
      channel = "_1tag";
    } else if (n_bjet >= 2) {
      channel = "_2tag";
    }
  } else {
    if (n_bjet == 0) {
      channel = "_0tag";
    } else if (n_bjet >= 1) {
      channel = "_1tag";
    }
  }

  return std::pair<std::string, double>(channel, jet_sf);
}

double CharmLoopBase::closest_bjet(unsigned int i, unsigned int index) {
  auto jets = m_jets_sys.at(i);
  float dRMin = 999;
  for (auto &jet : jets) {
    // remove jet overlapping with the D-meson
    float dR = get_dr_bjet_Dmeson(jet, index);
    if (dR < 0.4) {
      continue;
    }
    if (m_AnalysisJets_ftag_select_DL1r_FixedCutBEff_70->at(jet)) {
      if (dR < dRMin) {
        dRMin = dR;
      }
    }
  }
  return dRMin;
}

void CharmLoopBase::calculate_closest_jet_properties(
    unsigned int DMeson_index, unsigned int sys_index,
    SysJet *sysClosestJet) {
  int closest_jet_index = -1;
  float closest_jet_dR = 0.4;
  float closest_jet_pt = 0.0;
  float closest_jet_eta = 999;
  float closest_jet_phi = 999;
  float closest_jet_zt = 0.0;
  int closest_jet_truth_flavor = -1;
  float closest_jet_ftag_select_70 = 0.0;
  float closest_jet_ftag_effSF = 0.0;
  bool closest_jet_selected = false;

  auto jets = m_jets_sys.at(sys_index);

  for (auto &jet : jets) {
    float dR = get_dr_bjet_Dmeson(jet, DMeson_index); // get dR between jet and D meson
    if (dR < closest_jet_dR){
      closest_jet_dR = dR;
      closest_jet_index = jet;
    }
  }
  if (closest_jet_index >= 0) {
    closest_jet_pt = m_sys_jet_pt.at(sys_index)->at(closest_jet_index);
    closest_jet_eta =  m_AnalysisJets_eta->at(closest_jet_index);
    closest_jet_phi = m_AnalysisJets_phi->at(closest_jet_index);
    closest_jet_zt = m_DMesons_pt->at(DMeson_index) / m_sys_jet_pt.at(sys_index)->at(closest_jet_index);
    closest_jet_ftag_select_70 = m_AnalysisJets_ftag_select_DL1r_FixedCutBEff_70->at(closest_jet_index);
    if (m_is_mc) {
      closest_jet_truth_flavor = m_AnalysisJets_HadronConeExclTruthLabelID->at(closest_jet_index);
      closest_jet_ftag_effSF = m_sys_jet_ftag_eff.at(sys_index)->at(closest_jet_index);
    }
  }
  else {
    closest_jet_dR = 999;
  }

  sysClosestJet->jet_index.at(sys_index) = closest_jet_index;
  sysClosestJet->jet_dR.at(sys_index) = closest_jet_dR;
  sysClosestJet->jet_pt.at(sys_index) = closest_jet_pt;
  sysClosestJet->jet_eta.at(sys_index) = closest_jet_eta;
  sysClosestJet->jet_phi.at(sys_index) = closest_jet_phi;
  sysClosestJet->jet_zt.at(sys_index) = closest_jet_zt;
  sysClosestJet->jet_truth_flavor.at(sys_index) = closest_jet_truth_flavor;
  sysClosestJet->jet_ftag_select_70.at(sys_index) = closest_jet_ftag_select_70;
  sysClosestJet->jet_ftag_effSF.at(sys_index) = closest_jet_ftag_effSF;
}


}  // namespace Charm
