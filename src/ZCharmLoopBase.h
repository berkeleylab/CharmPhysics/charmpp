#ifndef Z_PLUS_CHARM_LOOP_H
#define Z_PLUS_CHARM_LOOP_H

#include "DMesonRec.h"
#include "EventLoopBase.h"
#include "TruthInfo.h"

namespace Charm {
class ZCharmLoopBase : public EventLoopBase,
                       public DMesonRec,
                       public TruthInfo {
 public:
  ZCharmLoopBase(TString input_file, TString out_path,
                 TString tree_name = "CharmAnalysis");

 protected:
  virtual void connect_branches() override;

 private:
  virtual void initialize() override;

  virtual int execute() override;

  virtual bool preselection() override;

 private:
  std::set<std::string> m_created_channels;
  bool m_split_by_period{};
  bool m_truth_match{};
  bool m_do_extra_histograms{};

 protected:
  bool m_do_prod_fraction_rw{};
  bool m_do_prod_fraction_rw_old{};
  bool m_fiducial_selection{};
  bool m_save_truth_zjets{};
  bool m_do_ztruth_fid_study{};
  bool m_do_mj_estimation{};
  bool m_jet_selection{};
  bool m_unfolding_variables{};
  const double m_eta_bins_el[6];
  const double m_eta_bins_mu[4];

 protected:
  bool m_D_plus_meson{};
  bool m_D_star_meson{};
  bool m_D_star_pi0_meson{};
  bool m_D_s_meson{};
  bool m_truth_matchD{};
  bool m_truth_D{};
  bool m_do_single_entry_D{};
  bool m_do_single_entry_D_only{};
  bool m_do_lep_presel{};
  bool m_fill_zjets{};
  bool m_do_PV{};
  bool m_apply_lumi{};
  bool m_reweight_spg{};
  bool m_debug_dmeson_printout{};
  bool m_bin_in_truth_D_pt;
  bool m_signal_only;
  bool m_reweight_sherpa;

 protected:
  std::string get_period_string();

  void fill_eventNumber_histogram();

  void fill_lepton_histograms(std::string channel);

  void fill_lepton_histograms(std::vector<std::string> channels,
                              std::vector<bool> extra_flag = {},
                              std::vector<double> extra_weight = {});

  void fill_PV_histograms(std::string channel);

  void fill_PV_histograms(std::vector<std::string> channels,
                          std::vector<bool> extra_flag = {},
                          std::vector<double> extra_weight = {});

  void fill_D_plus_histograms(std::string channel);

  void fill_D_star_histograms(std::string channel);

  void fill_D_star_pi0_histograms(std::string channel);

  void fill_D_s_histograms(std::string channel);

  void fill_histograms(std::string channel = "");

  void fill_histograms_with_regions();

  void jet_selection_for_sys();

  void read_sys_br();

  float get_dr_bjet_Dmeson(unsigned int jet, unsigned int i);

  float get_dr_lep_Dmeson(float eta, float phi);

  void get_jets();

  void debug_dmeson_printout(int index, bool printTruth = true);

  std::string m_channel_string;

  std::string m_jet_selection_string;

  // variables
  float m_lep1_d0;
  float m_lep1_d0sig;
  float m_lep1_d0PV;
  float m_lep1_d0sigPV;
  float m_lep1_z0sinTheta;
  float m_lep1_charge;
  float m_lep1_topoetcone20;
  float m_lep1_topoetcone20_over_pt;
  float m_lep1_ptvarcone20_TightTTVA_pt1000;
  float m_lep1_ptvarcone30_TightTTVA_pt1000;
  float m_lep1_ptvarcone20_TightTTVA_pt1000_over_pt;
  float m_lep1_ptvarcone30_TightTTVA_pt1000_over_pt;
  float m_lep1_ptvarcone30_plus_neflowisol20;
  float m_lep1_neflowisol20;
  float m_lep1_EOverP;
  float m_lep2_d0;
  float m_lep2_d0sig;
  float m_lep2_d0PV;
  float m_lep2_d0sigPV;
  float m_lep2_z0sinTheta;
  float m_lep2_charge;
  float m_lep2_topoetcone20;
  float m_lep2_topoetcone20_over_pt;
  float m_lep2_ptvarcone20_TightTTVA_pt1000;
  float m_lep2_ptvarcone30_TightTTVA_pt1000;
  float m_lep2_ptvarcone20_TightTTVA_pt1000_over_pt;
  float m_lep2_ptvarcone30_TightTTVA_pt1000_over_pt;
  float m_lep2_ptvarcone30_plus_neflowisol20;
  float m_lep2_neflowisol20;
  float m_lep2_EOverP;
  float m_Z_eta;
  float m_Z_phi;
  float m_lep_dPhi;
  float m_lep_dR;
  float m_truth_v_m;
  float m_truth_v_pt;
  float m_caloCluster_eta;
  bool m_lep1_is_isolated;
  bool m_lep2_is_isolated;
  unsigned int m_nbjets;

  // objects
  std::vector<std::pair<unsigned int, float>> m_loose_electrons;
  std::vector<std::pair<unsigned int, float>> m_loose_muons;
  std::vector<std::pair<unsigned int, float>> m_electrons;
  std::vector<std::pair<unsigned int, float>> m_muons;
  std::vector<unsigned int> m_jets;

  // variables affected by calibration systematics
  std::vector<Float_t> m_sys_Z_m;
  std::vector<Float_t> m_sys_Z_pt;
  std::vector<Float_t> m_sys_lep1_pt;
  std::vector<Float_t> m_sys_lep1_eta;
  std::vector<Float_t> m_sys_lep1_phi;
  std::vector<Float_t> m_sys_lep2_pt;
  std::vector<Float_t> m_sys_lep2_eta;
  std::vector<Float_t> m_sys_lep2_phi;

  std::vector<bool> m_sys_anti_tight;
  std::vector<bool> m_sys_SS;
  bool m_sys_pass_ztruth_fid_cuts;
  std::vector<Float_t> m_sys_nbjets;
  std::vector<Float_t> m_sys_njets;

  std::vector<std::vector<unsigned int>> m_jets_sys;

 private:
  float get_el_trigger_weight(unsigned int sys_index, unsigned int i,
                              unsigned int j, std::string iso_pair);
  float get_mu_trigger_weight(unsigned int sys_index, unsigned int i,
                              unsigned int j);
  void get_loose_electrons();
  void get_loose_muons();
  void get_tight_electrons();
  void get_tight_muons();
  bool pass_el_trigger();
  bool pass_mu_trigger();
  bool el_is_trigger_matched(unsigned int i, unsigned int j);
  bool mu_is_trigger_matched(unsigned int i, unsigned int j);
  bool el_is_prompt(unsigned int i);
  bool mu_is_prompt(unsigned int i);
  void configure();

 private:
  bool m_have_one_D;

 private:
  // runNumber of RandomRunNumber
  TString get_run_number_string();

 private:
  // branches
  UInt_t m_EventInfo_runNumber{};
  Float_t m_EventInfo_correctedScaled_averageInteractionsPerCrossing{};
  Float_t m_CharmEventInfo_EventWeight{};
  Float_t m_EventInfo_prodFracWeight_NOSYS{};
  Float_t m_EventInfo_generatorWeight_NOSYS{};
  Float_t m_EventInfo_PileupWeight_NOSYS{};
  Float_t m_EventInfo_jvt_effSF_NOSYS{};
  Float_t m_CharmEventInfo_PV_X{};
  Float_t m_CharmEventInfo_PV_Y{};
  Float_t m_CharmEventInfo_PV_Z{};
  Float_t m_CharmEventInfo_truthVertex_X{};
  Float_t m_CharmEventInfo_truthVertex_Y{};
  Float_t m_CharmEventInfo_truthVertex_Z{};

  bool m_EventInfo_trigPassed_HLT_e24_lhmedium_L1EM20VH{};
  bool m_EventInfo_trigPassed_HLT_e60_lhmedium{};
  bool m_EventInfo_trigPassed_HLT_e120_lhloose{};
  bool m_EventInfo_trigPassed_HLT_e26_lhtight_nod0_ivarloose{};
  bool m_EventInfo_trigPassed_HLT_e60_lhmedium_nod0{};
  bool m_EventInfo_trigPassed_HLT_e140_lhloose_nod0{};
  bool m_EventInfo_trigPassed_HLT_mu20_iloose_L1MU15{};
  bool m_EventInfo_trigPassed_HLT_mu26_ivarmedium{};
  bool m_EventInfo_trigPassed_HLT_mu50{};

  // muons
  std::vector<Float_t> *m_AnalysisMuons_pt_NOSYS{};
  std::vector<Float_t> *m_AnalysisMuons_eta{};
  std::vector<Float_t> *m_AnalysisMuons_phi{};
  std::vector<Float_t> *m_AnalysisMuons_charge{};
  std::vector<Float_t> *m_AnalysisMuons_mu_effSF_Quality_Medium_NOSYS{};
  std::vector<Float_t> *m_AnalysisMuons_mu_effSF_TTVA_NOSYS{};
  std::vector<Float_t> *m_AnalysisMuons_mu_effSF_Isol_PflowTight_VarRad_NOSYS{};
  std::vector<Float_t> *
      m_AnalysisMuons_mu_effMC_Trig_Medium_HLT_mu26_ivarmedium_OR_HLT_mu50_NOSYS{};
  std::vector<Float_t> *
      m_AnalysisMuons_mu_effData_Trig_Medium_HLT_mu26_ivarmedium_OR_HLT_mu50_NOSYS{};
  std::vector<Float_t> *
      m_AnalysisMuons_mu_effMC_Trig_Medium_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_NOSYS{};
  std::vector<Float_t> *
      m_AnalysisMuons_mu_effData_Trig_Medium_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_NOSYS{};
  std::vector<Int_t> *m_AnalysisMuons_truthType{};
  std::vector<Int_t> *m_AnalysisMuons_truthOrigin{};
  std::vector<bool> *m_AnalysisMuons_is_bad{};
  std::vector<bool> *m_AnalysisMuons_mu_selected_NOSYS{};
  std::vector<bool> *m_AnalysisMuons_isQuality_Medium_NOSYS{};
  std::vector<bool> *m_AnalysisMuons_isIsolated_PflowTight_VarRad_NOSYS{};
  std::vector<bool> *m_AnalysisMuons_matched_HLT_mu20_iloose_L1MU15{};
  std::vector<bool> *m_AnalysisMuons_matched_HLT_mu26_ivarmedium{};
  std::vector<bool> *m_AnalysisMuons_matched_HLT_mu50{};

  // electrons
  std::vector<Float_t> *m_AnalysisElectrons_pt_NOSYS{};
  std::vector<Float_t> *m_AnalysisElectrons_eta{};
  std::vector<Float_t> *m_AnalysisElectrons_caloCluster_eta{};
  std::vector<Float_t> *m_AnalysisElectrons_phi{};
  std::vector<Float_t> *m_AnalysisElectrons_charge{};
  std::vector<Float_t> *m_AnalysisElectrons_effSF_NOSYS{};
  std::vector<Float_t> *m_AnalysisElectrons_effSF_ID_Tight_NOSYS{};
  std::vector<Float_t>
      *m_AnalysisElectrons_effSF_Isol_Tight_Tight_VarRad_NOSYS{};
  std::vector<Float_t> *
      m_AnalysisElectrons_effSF_Trig_Tight_Tight_VarRad_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_NOSYS{};
  std::vector<Float_t> *
      m_AnalysisElectrons_effSF_Trig_Tight_Tight_VarRad_Eff_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_NOSYS{};
  std::vector<Float_t> *
      m_AnalysisElectrons_effSF_Trig_Tight_noIso_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_NOSYS{};
  std::vector<Float_t> *
      m_AnalysisElectrons_effSF_Trig_Tight_noIso_Eff_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_NOSYS{};
  std::vector<Float_t>
      *m_AnalysisElectrons_effSF_Chflip_Tight_Tight_VarRad_NOSYS{};
  std::vector<Int_t> *m_AnalysisElectrons_truthType{};
  std::vector<Int_t> *m_AnalysisElectrons_truthOrigin{};
  std::vector<Int_t> *m_AnalysisElectrons_firstEgMotherTruthType{};
  std::vector<Int_t> *m_AnalysisElectrons_firstEgMotherTruthOrigin{};
  std::vector<Int_t> *m_AnalysisElectrons_firstEgMotherPdgId{};
  std::vector<bool> *m_AnalysisElectrons_el_selected_NOSYS{};
  std::vector<bool> *m_AnalysisElectrons_likelihood_Medium{};
  std::vector<bool> *m_AnalysisElectrons_likelihood_Tight{};
  std::vector<bool> *m_AnalysisElectrons_isIsolated_FCHighPtCaloOnly_NOSYS{};
  std::vector<bool> *m_AnalysisElectrons_isIsolated_FCLoose_NOSYS{};
  std::vector<bool> *m_AnalysisElectrons_isIsolated_Tight_VarRad_NOSYS{};
  std::vector<bool> *m_AnalysisElectrons_isIsolated_Gradient_NOSYS{};
  std::vector<bool> *m_AnalysisElectrons_matched_HLT_e24_lhmedium_L1EM20VH{};
  std::vector<bool> *m_AnalysisElectrons_matched_HLT_e60_lhmedium{};
  std::vector<bool> *m_AnalysisElectrons_matched_HLT_e120_lhloose{};
  std::vector<bool>
      *m_AnalysisElectrons_matched_HLT_e26_lhtight_nod0_ivarloose{};
  std::vector<bool> *m_AnalysisElectrons_matched_HLT_e60_lhmedium_nod0{};
  std::vector<bool> *m_AnalysisElectrons_matched_HLT_e140_lhloose_nod0{};

  // jets
  std::vector<float> *m_AnalysisJets_pt_NOSYS{};
  std::vector<float> *m_AnalysisJets_ftag_select_DL1r_FixedCutBEff_70{};
  std::vector<float> *m_AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_NOSYS{};
  std::vector<float> *m_AnalysisJets_eta{};
  std::vector<float> *m_AnalysisJets_phi{};
  std::vector<float> *m_AnalysisJets_m{};
  std::vector<bool> *m_AnalysisJets_jet_selected_NOSYS{};
  float m_EventInfo_ftag_effSF_DL1r_FixedCutBEff_70_NOSYS{};

  // extra stuff
  std::vector<float> *m_AnalysisElectrons_d0{};
  std::vector<float> *m_AnalysisElectrons_d0sig{};
  std::vector<float> *m_AnalysisElectrons_z0sinTheta{};
  std::vector<float> *m_AnalysisElectrons_topoetcone20{};
  std::vector<float> *m_AnalysisElectrons_ptvarcone20_TightTTVA_pt1000{};
  std::vector<float> *m_AnalysisElectrons_ptvarcone30_TightTTVA_pt1000{};
  std::vector<float> *m_AnalysisElectrons_EOverP{};
  std::vector<float> *m_AnalysisMuons_d0{};
  std::vector<float> *m_AnalysisMuons_d0sig{};
  std::vector<float> *m_AnalysisMuons_z0sinTheta{};
  std::vector<float> *m_AnalysisMuons_topoetcone20{};
  std::vector<float> *m_AnalysisMuons_ptvarcone20_TightTTVA_pt1000{};
  std::vector<float> *m_AnalysisMuons_ptvarcone30_TightTTVA_pt1000{};
  std::vector<float> *m_AnalysisMuons_ptvarcone30_TightTTVA_pt500{};
  std::vector<float> *m_AnalysisMuons_neflowisol20{};

  // systematics
  std::vector<std::vector<std::pair<unsigned int, float>>> m_electrons_sys;
  std::vector<std::vector<std::pair<unsigned int, float>>> m_muons_sys;
  std::vector<std::vector<std::pair<unsigned int, float>>>
      m_loose_electrons_sys;
  std::vector<std::vector<std::pair<unsigned int, float>>> m_loose_muons_sys;

  // electrons
  std::vector<std::vector<Float_t> *> m_sys_el_pt;
  std::vector<std::vector<bool> *> m_sys_el_iso;
  std::vector<std::vector<bool> *> m_sys_el_selected;
  std::vector<std::vector<Float_t> *> m_sys_el_reco_eff;
  std::vector<std::vector<Float_t> *> m_sys_el_id_eff;
  std::vector<std::vector<Float_t> *> m_sys_el_iso_eff;
  std::vector<std::vector<Float_t> *> m_sys_el_trig_eff;
  std::vector<std::vector<Float_t> *> m_sys_el_trig_sf;
  std::vector<std::vector<Float_t> *> m_sys_el_charge_sf;
  std::vector<std::vector<Float_t> *> m_sys_el_trig_noiso_eff;
  std::vector<std::vector<Float_t> *> m_sys_el_trig_noiso_sf;

  // muon
  std::vector<std::vector<Float_t> *> m_sys_mu_pt;
  std::vector<std::vector<bool> *> m_sys_mu_iso;
  std::vector<std::vector<bool> *> m_sys_mu_quality;
  std::vector<std::vector<bool> *> m_sys_mu_selected;
  std::vector<std::vector<Float_t> *> m_sys_mu_quality_eff;
  std::vector<std::vector<Float_t> *> m_sys_mu_ttva_eff;
  std::vector<std::vector<Float_t> *> m_sys_mu_iso_eff;
  std::vector<std::vector<Float_t> *> m_sys_mu_trig_2015_eff_data;
  std::vector<std::vector<Float_t> *> m_sys_mu_trig_2015_eff_mc;
  std::vector<std::vector<Float_t> *> m_sys_mu_trig_2018_eff_data;
  std::vector<std::vector<Float_t> *> m_sys_mu_trig_2018_eff_mc;

  // jets
  std::vector<std::vector<Float_t> *> m_sys_jet_pt;
  std::vector<std::vector<Float_t> *> m_sys_jet_ftag_eff;
  std::vector<std::vector<bool> *> m_sys_jet_selected;

  // event weight branches
  sys_br<Float_t> m_sys_br_GEN{};
  sys_br<Float_t> m_sys_br_PU{};
  sys_br<Float_t> m_sys_br_JVT{};
  sys_br<Float_t> m_sys_br_FTAG{};
  sys_br<Float_t> m_sys_br_PROD_FRAC{};

  // electron sys branches
  sys_br<std::vector<Float_t> *> m_sys_br_el_pt{};
  sys_br<std::vector<Float_t> *> m_sys_br_el_reco_eff{};
  sys_br<std::vector<Float_t> *> m_sys_br_el_id_eff{};
  sys_br<std::vector<Float_t> *> m_sys_br_el_iso_eff{};
  sys_br<std::vector<Float_t> *> m_sys_br_el_trig_eff{};
  sys_br<std::vector<Float_t> *> m_sys_br_el_trig_sf{};
  sys_br<std::vector<Float_t> *> m_sys_br_el_charge_eff{};
  sys_br<std::vector<Float_t> *> m_sys_br_el_calib_reco_eff{};
  sys_br<std::vector<Float_t> *> m_sys_br_el_calib_id_eff{};
  sys_br<std::vector<Float_t> *> m_sys_br_el_calib_iso_eff{};
  sys_br<std::vector<Float_t> *> m_sys_br_el_calib_trig_eff{};
  sys_br<std::vector<Float_t> *> m_sys_br_el_calib_trig_sf{};
  sys_br<std::vector<Float_t> *> m_sys_br_el_calib_charge_eff{};
  sys_br<std::vector<Float_t> *> m_sys_br_el_trig_noiso_eff{};
  sys_br<std::vector<Float_t> *> m_sys_br_el_trig_noiso_sf{};
  sys_br<std::vector<Float_t> *> m_sys_br_el_calib_trig_noiso_eff{};
  sys_br<std::vector<Float_t> *> m_sys_br_el_calib_trig_noiso_sf{};
  sys_br<std::vector<bool> *> m_sys_br_el_selected{};
  sys_br<std::vector<bool> *> m_sys_br_el_isIsolated_Tight_VarRad{};

  // muon sys branches
  sys_br<std::vector<Float_t> *> m_sys_br_mu_pt{};
  sys_br<std::vector<Float_t> *> m_sys_br_mu_quality_eff{};
  sys_br<std::vector<Float_t> *> m_sys_br_mu_ttva_eff{};
  sys_br<std::vector<Float_t> *> m_sys_br_mu_iso_eff{};
  sys_br<std::vector<Float_t> *> m_sys_br_mu_trig_2015_eff_data{};
  sys_br<std::vector<Float_t> *> m_sys_br_mu_trig_2015_eff_mc{};
  sys_br<std::vector<Float_t> *> m_sys_br_mu_trig_2018_eff_data{};
  sys_br<std::vector<Float_t> *> m_sys_br_mu_trig_2018_eff_mc{};
  sys_br<std::vector<Float_t> *> m_sys_br_mu_calib_quality_eff{};
  sys_br<std::vector<Float_t> *> m_sys_br_mu_calib_ttva_eff{};
  sys_br<std::vector<Float_t> *> m_sys_br_mu_calib_iso_eff{};
  sys_br<std::vector<Float_t> *> m_sys_br_mu_calib_trig_2015_eff_data{};
  sys_br<std::vector<Float_t> *> m_sys_br_mu_calib_trig_2015_eff_mc{};
  sys_br<std::vector<Float_t> *> m_sys_br_mu_calib_trig_2018_eff_data{};
  sys_br<std::vector<Float_t> *> m_sys_br_mu_calib_trig_2018_eff_mc{};
  sys_br<std::vector<bool> *> m_sys_br_mu_selected{};
  sys_br<std::vector<bool> *> m_sys_br_mu_isIsolated_PflowTight_VarRad{};
  sys_br<std::vector<bool> *> m_sys_br_mu_isQuality_Medium{};

  // jet sys branches
  sys_br<std::vector<Float_t> *> m_sys_br_jet_pt{};
  sys_br<std::vector<bool> *> m_sys_br_jet_selected{};
  sys_br<std::vector<Float_t> *> m_sys_br_jet_ftag_eff{};
  sys_br<std::vector<Float_t> *> m_sys_br_jet_calib_ftag_eff{};
};

}  // namespace Charm

#endif  // Z_PLUS_CHARM_LOOP_H
