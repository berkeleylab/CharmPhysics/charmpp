#include "TruthInfo.h"

#include "DMesonRec.h"

namespace Charm {
void TruthInfo::reweight_production_fractions(EventLoopBase *elb) {
  // obsoleted in v15 ntuples
  std::cout << "WARNING::reweight_production_fractions() "
            << "should use the PROD_FRAC branch from the ntuples instead!"
            << std::endl;

  // charm production fraction reweight
  for (unsigned int j = 0; j < elb->m_nSyst; j++) {
    std::string sys_string = "";
    if (j > 0) {
      sys_string = elb->m_systematics.at(j - 1);
    }

    float w = 1.0;
    std::string shower = elb->m_mc_shower;
    if (shower == "pythia" || shower == "sherpa2211" || shower == "herwig") {
      for (unsigned int i = 0; i < m_TruthParticles_Selected_pdgId->size();
           i++) {
        int pdgId = fabs(m_TruthParticles_Selected_pdgId->at(i));
        if ((m_TruthParticles_Selected_pt->at(i) * Charm::GeV > 5.0) &&
            (fabs(m_TruthParticles_Selected_eta->at(i)) < 2.2)) {
          w *= elb->m_prod_frac_weight_helper.get_weight(sys_string, pdgId,
                                                         shower);
        }
      }
    }
    elb->multiply_event_weight(w, j);
  }
}

void TruthInfo::reweight_wjets(EventLoopBase *elb) {
  // NLO MG reweighting
  for (unsigned int j = 0; j < elb->m_nSyst; j++) {
    std::string sys_string = "";
    if (j > 0) {
      sys_string = elb->m_systematics.at(j - 1);
    }

    float w = 1.0;
    if (elb->m_mc_gen == "madgraph") {
      for (unsigned int i = 0; i < m_TruthParticles_Selected_pdgId->size();
           i++) {
        int pdgId = fabs(m_TruthParticles_Selected_pdgId->at(i));
        if ((pdgId == 411 || pdgId == 421 || pdgId == 431 || pdgId > 4000) &&
            (m_TruthParticles_Selected_pt->at(i) * Charm::GeV >= 8.0) &&
            (fabs(m_TruthParticles_Selected_eta->at(i)) < 2.2)) {
          w *= elb->m_nlo_mg_sys_helper.get_wjets_weight(
              sys_string, m_TruthParticles_Selected_pt->at(i) * Charm::GeV,
              "Dplus");
        }
      }
    }
    elb->multiply_event_weight(w, j);
  }
}

void TruthInfo::get_charmed_hadrons() {
  // truth hadrons passing fiducial cuts
  m_truth_charmed_hadrons.clear();
  m_truth_charmed_hadrons_from_B.clear();

  // loop over truth particles
  for (unsigned int i = 0; i < m_TruthParticles_Selected_pdgId->size(); i++) {
    // fiducial cuts
    if (m_TruthParticles_Selected_pt->at(i) * Charm::GeV >= 0.0 &&
        fabs(m_TruthParticles_Selected_eta->at(i)) < 100.) {
      if (m_TruthParticles_Selected_fromBdecay->at(i)) {
        m_truth_charmed_hadrons_from_B.push_back(
            fabs(m_TruthParticles_Selected_pdgId->at(i)));
      } else {
        m_truth_charmed_hadrons.push_back(
            fabs(m_TruthParticles_Selected_pdgId->at(i)));
      }
    }
  }
}

std::string TruthInfo::get_decay_mode(int index) {
  // check for decay products
  int check_daughters_int = check_daughters(index);
  if (check_daughters_int == 0) {
    return "";
  }

  // find di-photon resonances
  std::vector<float> diphotons = get_diphoton_masses(index);

  // veto any decays with eta resonances
  if (fabs(diphoton_resonance_mass(diphotons, ETA_MASS) - ETA_MASS) <
      1.0 * Charm::MeV) {
    return "";
  }

  if (fabs(m_TruthParticles_Selected_pdgId->at(index)) == 411) {
    if (fabs(diphoton_resonance_mass(diphotons, PION0_MASS) - PION0_MASS) <
        0.1 * Charm::MeV) {
      return "";
    }
    if (check_daughters_int == 1)
      return "Kpipi";
    else if (check_daughters_int == 2) {
      if (fabs(phi_resonance_mass(index) - PHI_MASS) < 12.0)
        return "Phipi";
      else
        return "";
    }
  } else if (fabs(m_TruthParticles_Selected_pdgId->at(index)) == 431) {
    if (fabs(diphoton_resonance_mass(diphotons, PION0_MASS) - PION0_MASS) <
        0.1 * Charm::MeV) {
      return "";
    }
    if (check_daughters_int == 1)
      return "Kpipi";
    else if (check_daughters_int == 2) {
      if (m_TruthParticles_Selected_decayMode->at(index) == 5)
        return "Phipi";
      else
        return "";
    }
  } else if (fabs(m_TruthParticles_Selected_pdgId->at(index)) == 4122) {
    if (fabs(diphoton_resonance_mass(diphotons, PION0_MASS) - PION0_MASS) <
        0.1 * Charm::MeV) {
      return "";
    }
    if (check_daughters_int == 3) {
      return "pKpi";
    }
  } else if (fabs(m_TruthParticles_Selected_pdgId->at(index)) == 413 &&
             check_daughters_int == 1) {
    // count the number of pi0
    int Npi0 = 0;
    for (auto mass : diphotons) {
      if (fabs(mass - PION0_MASS) < 0.1 * Charm::MeV) {
        Npi0++;
      }
    }
    // either 0 or 1 p0 is acceptable
    if (Npi0 > 1) {
      return "";
    } else if (Npi0 == 1) {
      return "Kpipipi0";
    } else {
      return "Kpipi";
    }
  }
  return "";
}

int TruthInfo::check_daughters(int index) {
  std::vector<int> vecK{};
  std::vector<int> vecPi{};
  std::vector<int> vecProton{};
  std::vector<int> vecOther{};
  for (unsigned int i = 0;
       i < m_TruthParticles_Selected_daughterInfoT__pdgId->at(index).size();
       i++) {
    int pdgId = m_TruthParticles_Selected_daughterInfoT__pdgId->at(index).at(i);
    if (fabs(pdgId) == 211) {
      vecPi.push_back(pdgId);
    } else if (fabs(pdgId) == 321) {
      vecK.push_back(pdgId);
    } else if (fabs(pdgId) == 2212) {
      vecProton.push_back(pdgId);
    }
    // do not reject photons and D0; photons are resonances like pi0
    // and D0 gets saved in the D* decay products
    else if (fabs(pdgId) != 22 && fabs(pdgId) != 421) {
      vecOther.push_back(pdgId);
    }
  }

  // K pi pi
  if ((vecK.size() == 1 && vecPi.size() == 2 && vecOther.size() == 0 &&
       (vecPi[0] * vecPi[1] > 0.0) && (vecPi[0] * vecK[0] < 0.0))) {
    return 1;
  }
  // K K pi
  else if ((vecK.size() == 2 && vecPi.size() == 1 && vecOther.size() == 0) &&
           (vecK[0] * vecK[1] < 0.0)) {
    return 2;
  }
  // p K pi
  if ((vecK.size() == 1 && vecPi.size() == 1 && vecProton.size() == 1 && vecOther.size() == 0 &&
       (vecPi[0] * vecProton[0] > 0.0) && (vecPi[0] * vecK[0] < 0.0))) {
    return 3;
  }

  return 0;
}

std::vector<float> TruthInfo::get_diphoton_masses(int index) {
  std::vector<float> phot_mass = {};
  for (unsigned int i = 0;
       i < m_TruthParticles_Selected_daughterInfoT__pdgId->at(index).size();
       i++) {
    if (m_TruthParticles_Selected_daughterInfoT__pdgId->at(index).at(i) != 22) {
      continue;
    }
    for (unsigned int j = i + 1;
         j < m_TruthParticles_Selected_daughterInfoT__pdgId->at(index).size();
         j++) {
      if (m_TruthParticles_Selected_daughterInfoT__pdgId->at(index).at(j) !=
          22) {
        continue;
      }
      TLorentzVector phot1;
      TLorentzVector phot2;
      phot1.SetPtEtaPhiM(
          m_TruthParticles_Selected_daughterInfoT__pt->at(index).at(i),
          m_TruthParticles_Selected_daughterInfoT__eta->at(index).at(i),
          m_TruthParticles_Selected_daughterInfoT__phi->at(index).at(i), 0);
      phot2.SetPtEtaPhiM(
          m_TruthParticles_Selected_daughterInfoT__pt->at(index).at(j),
          m_TruthParticles_Selected_daughterInfoT__eta->at(index).at(j),
          m_TruthParticles_Selected_daughterInfoT__phi->at(index).at(j), 0);
      TLorentzVector pi0 = phot1 + phot2;
      phot_mass.push_back(pi0.M());
    }
  }
  return phot_mass;
}

float TruthInfo::truth_daughter_mass(int index) {
  if (m_TruthParticles_Selected_daughterInfoT__pdgId->at(index).size() < 3) {
    return 0.0;
  }

  std::vector<TLorentzVector> daughters;
  for (unsigned int i = 0;
       i < m_TruthParticles_Selected_daughterInfoT__pdgId->at(index).size();
       i++) {
    int pdgId = m_TruthParticles_Selected_daughterInfoT__pdgId->at(index).at(i);
    if (fabs(pdgId) == 211) {
      TLorentzVector tmp_vec;
      tmp_vec.SetPtEtaPhiM(
          m_TruthParticles_Selected_daughterInfoT__pt->at(index).at(i),
          m_TruthParticles_Selected_daughterInfoT__eta->at(index).at(i),
          m_TruthParticles_Selected_daughterInfoT__phi->at(index).at(i),
          PION_MASS);
      daughters.push_back(tmp_vec);
    } else if (fabs(pdgId) == 321) {
      TLorentzVector tmp_vec;
      tmp_vec.SetPtEtaPhiM(
          m_TruthParticles_Selected_daughterInfoT__pt->at(index).at(i),
          m_TruthParticles_Selected_daughterInfoT__eta->at(index).at(i),
          m_TruthParticles_Selected_daughterInfoT__phi->at(index).at(i),
          KAON_MASS);
      daughters.push_back(tmp_vec);
    } else if (fabs(pdgId) == 2212) {
      TLorentzVector tmp_vec;
      tmp_vec.SetPtEtaPhiM(
          m_TruthParticles_Selected_daughterInfoT__pt->at(index).at(i),
          m_TruthParticles_Selected_daughterInfoT__eta->at(index).at(i),
          m_TruthParticles_Selected_daughterInfoT__phi->at(index).at(i),
          PROTON_MASS);
      daughters.push_back(tmp_vec);
    }
  }

  // calcualte the sum
  TLorentzVector sum;
  if (daughters.size() > 0) {
    sum = daughters[0];
    for (unsigned int i = 1; i < daughters.size(); i++) {
      sum += daughters[i];
    }
    return sum.M();
  }
  return 0.0;
}

bool TruthInfo::get_truth_wjets(std::string sample_type) {
  // required origin
  unsigned int origin = 12;
  if (sample_type == "ttbar") {
    origin = 10;
  }

  // truth leptons
  std::vector<unsigned int> truth_leptons;
  for (unsigned int i = 0; i < m_TruthLeptons_pdgId->size(); i++) {
    if (fabs(m_TruthLeptons_pdgId->at(i)) == 11 ||
        fabs(m_TruthLeptons_pdgId->at(i)) == 13) {
      if (m_TruthLeptons_classifierParticleOrigin->at(i) == origin) {
        truth_leptons.push_back(i);
      }
    }
  }

  // remove low pT non-prompt electrons that don't get handeled by truth
  // matching need to catch some weird cases where electron / muon are not
  // origin == 12
  if (truth_leptons.size() < 1) {
    // neutrino barcode
    int neutrino_barcode = 1e5;
    int neutrino_pdg = 0;
    for (unsigned int i = 0; i < m_TruthLeptons_pdgId->size(); i++) {
      if ((fabs(m_TruthLeptons_pdgId->at(i)) == 12 ||
           fabs(m_TruthLeptons_pdgId->at(i)) == 14)) {
        if (m_TruthLeptons_barcode->at(i) < neutrino_barcode) {
          neutrino_barcode = m_TruthLeptons_barcode->at(i);
          neutrino_pdg = m_TruthLeptons_pdgId->at(i);
        }
      }
    }
    for (unsigned int i = 0; i < m_TruthLeptons_pdgId->size(); i++) {
      if (fabs(m_TruthLeptons_pdgId->at(i)) == 11 ||
          fabs(m_TruthLeptons_pdgId->at(i)) == 13) {
        if (m_TruthLeptons_classifierParticleOrigin->at(i) != origin) {
          if (abs(m_TruthLeptons_barcode->at(i) - neutrino_barcode) > 10) {
            continue;
          } else if ((neutrino_pdg == 12 &&
                      fabs(m_TruthLeptons_pdgId->at(i)) == 13) ||
                     (neutrino_pdg == 14 &&
                      fabs(m_TruthLeptons_pdgId->at(i)) == 11)) {
            continue;
          } else if (m_TruthLeptons_classifierParticleOrigin->at(i) == 5) {
            continue;
          }
        }
        truth_leptons.push_back(i);
      }
    }
  }

  // need exactly one charged lepton
  if (truth_leptons.size() != 1) {
    if (sample_type == "wjets") {
      std::cout << "WARNING::get_truth_wjets() "
                << "not exactly one charged lepton:" << std::endl;
      for (unsigned int i = 0; i < m_TruthLeptons_pdgId->size(); i++) {
        std::cout << "PDG: " << m_TruthLeptons_pdgId->at(i) << " ORIGIN: "
                  << m_TruthLeptons_classifierParticleOrigin->at(i)
                  << " TYPE: " << m_TruthLeptons_classifierParticleType->at(i)
                  << " BARCODE: " << m_TruthLeptons_barcode->at(i)
                  << " PT: " << m_TruthLeptons_pt->at(i) * Charm::GeV
                  << std::endl;
      }
    }
    return false;
  }

  // neutrinos
  TLorentzVector stable_neutrinos;
  for (unsigned int i = 0; i < m_TruthLeptons_pdgId->size(); i++) {
    if ((fabs(m_TruthLeptons_pdgId->at(i)) == 12 ||
         fabs(m_TruthLeptons_pdgId->at(i)) == 14 ||
         fabs(m_TruthLeptons_pdgId->at(i)) == 16) &&
        (m_TruthLeptons_classifierParticleOrigin->at(i) == origin)) {
      TLorentzVector neutrino;
      neutrino.SetPtEtaPhiE(
          m_TruthLeptons_pt->at(i) * Charm::GeV, m_TruthLeptons_eta->at(i),
          m_TruthLeptons_phi->at(i), m_TruthLeptons_e->at(i) * Charm::GeV);
      stable_neutrinos += neutrino;
    }
  }

  // set neutrino
  m_truth_lep2 = stable_neutrinos;

  // lep1: charged lepton
  // lep2: neutrino
  unsigned int index1 = truth_leptons.at(0);
  if (m_dressed_containers) {
    m_truth_lep1.SetPtEtaPhiE(
        m_TruthLeptons_pt_dressed->at(index1) * Charm::GeV,
        m_TruthLeptons_eta_dressed->at(index1),
        m_TruthLeptons_phi_dressed->at(index1),
        m_TruthLeptons_e_dressed->at(index1) * Charm::GeV);
  } else {
    m_truth_lep1.SetPtEtaPhiE(m_TruthLeptons_pt->at(index1) * Charm::GeV,
                              m_TruthLeptons_eta->at(index1),
                              m_TruthLeptons_phi->at(index1),
                              m_TruthLeptons_e->at(index1) * Charm::GeV);
  }
  m_truth_lep1_charge = -(m_TruthLeptons_pdgId->at(index1) /
                          fabs(m_TruthLeptons_pdgId->at(index1)));
  m_truth_v = m_truth_lep1 + m_truth_lep2;
  m_truth_mt = sqrt(2 * m_truth_lep1.Pt() * m_truth_lep2.Pt() *
                    (1 - cos(m_truth_lep1.DeltaPhi(m_truth_lep2))));

  // define channel
  if (fabs(m_TruthLeptons_pdgId->at(index1)) == 11) {
    m_truth_channel = "el";
  } else if (fabs(m_TruthLeptons_pdgId->at(index1)) == 13) {
    m_truth_channel = "mu";
  } else if (fabs(m_TruthLeptons_pdgId->at(index1)) == 15) {
    m_truth_channel = "tau";
  }

  if (m_TruthLeptons_pdgId->at(index1) > 0) {
    m_truth_channel += "_minus";
  } else {
    m_truth_channel += "_plus";
  }

  return true;
}

bool TruthInfo::get_truth_zjets() {
  // truth leptons
  std::vector<unsigned int> truth_leptons;
  for (unsigned int i = 0; i < m_TruthLeptons_pdgId->size(); i++) {
    if (fabs(m_TruthLeptons_pdgId->at(i)) == 11 ||
        fabs(m_TruthLeptons_pdgId->at(i)) == 13) {
      // classifierParticleOrigin = 13(12) for Z(W)
      // Full list at:
      // https://gitlab.cern.ch/atlas/athena/blob/21.2/PhysicsAnalysis/MCTruthClassifier/MCTruthClassifier/MCTruthClassifierDefs.h
      if (m_TruthLeptons_classifierParticleOrigin->at(i) == 13) {
        truth_leptons.push_back(i);
      }
    }
  }

  // need exactly two charged leptons
  if (truth_leptons.size() != 2) {
    std::cout << "WARNING::get_truth_zjets() "
              << "not exactly two charged leptons:" << std::endl;
    for (unsigned int i = 0; i < m_TruthLeptons_pdgId->size(); i++) {
      std::cout << "PDG: " << m_TruthLeptons_pdgId->at(i)
                << " ORIGIN: " << m_TruthLeptons_classifierParticleOrigin->at(i)
                << " TYPE: " << m_TruthLeptons_classifierParticleType->at(i)
                << " BARCODE: " << m_TruthLeptons_barcode->at(i)
                << " PT: " << m_TruthLeptons_pt->at(i) * Charm::GeV
                << std::endl;
    }
    return false;
  }

  unsigned int index1 = truth_leptons.at(0);
  unsigned int index2 = truth_leptons.at(1);

  if (m_dressed_containers) {
    // order by pT
    if (m_TruthLeptons_pt_dressed->at(index2) >
        m_TruthLeptons_pt_dressed->at(index1)) {
      unsigned int temp = index1;
      index1 = index2;
      index2 = temp;
    }
    m_truth_lep1.SetPtEtaPhiE(
        m_TruthLeptons_pt_dressed->at(index1) * Charm::GeV,
        m_TruthLeptons_eta_dressed->at(index1),
        m_TruthLeptons_phi_dressed->at(index1),
        m_TruthLeptons_e_dressed->at(index1) * Charm::GeV);
    m_truth_lep2.SetPtEtaPhiE(
        m_TruthLeptons_pt_dressed->at(index2) * Charm::GeV,
        m_TruthLeptons_eta_dressed->at(index2),
        m_TruthLeptons_phi_dressed->at(index2),
        m_TruthLeptons_e_dressed->at(index2) * Charm::GeV);
  } else {
    // order by pT
    if (m_TruthLeptons_pt->at(index2) > m_TruthLeptons_pt->at(index1)) {
      unsigned int temp = index1;
      index1 = index2;
      index2 = temp;
    }
    m_truth_lep1.SetPtEtaPhiE(m_TruthLeptons_pt->at(index1) * Charm::GeV,
                              m_TruthLeptons_eta->at(index1),
                              m_TruthLeptons_phi->at(index1),
                              m_TruthLeptons_e->at(index1) * Charm::GeV);
    m_truth_lep2.SetPtEtaPhiE(m_TruthLeptons_pt->at(index2) * Charm::GeV,
                              m_TruthLeptons_eta->at(index2),
                              m_TruthLeptons_phi->at(index2),
                              m_TruthLeptons_e->at(index2) * Charm::GeV);
  }
  m_truth_lep1_charge = -(m_TruthLeptons_pdgId->at(index1) /
                          fabs(m_TruthLeptons_pdgId->at(index1)));
  m_truth_lep2_charge = -(m_TruthLeptons_pdgId->at(index2) /
                          fabs(m_TruthLeptons_pdgId->at(index2)));
  m_truth_lep1_pdgId = m_TruthLeptons_pdgId->at(index1);
  m_truth_lep2_pdgId = m_TruthLeptons_pdgId->at(index2);

  // Two oppositely charged leptons
  if (m_truth_lep1_charge * m_truth_lep2_charge > 0) {
    std::cout
        << "WARNING::get_truth_zjets() Both leptons are the same sign charge"
        << std::endl;
    return false;
  }

  // define channel
  if (fabs(m_TruthLeptons_pdgId->at(index1)) == 11 &&
      fabs(m_TruthLeptons_pdgId->at(index2)) == 11) {
    m_truth_channel = "el";
  } else if (fabs(m_TruthLeptons_pdgId->at(index1)) == 13 &&
             fabs(m_TruthLeptons_pdgId->at(index1)) == 13) {
    m_truth_channel = "mu";
  } else if (fabs(m_TruthLeptons_pdgId->at(index1)) == 15 &&
             fabs(m_TruthLeptons_pdgId->at(index1)) == 15) {
    m_truth_channel = "tau";
  } else {
    std::cout << "WARNING::get_truth_zjets() "
              << "not exactly two same flavor leptons:" << std::endl;
    for (unsigned int i = 0; i < m_TruthLeptons_pdgId->size(); i++) {
      std::cout << "PDG: " << m_TruthLeptons_pdgId->at(i)
                << " ORIGIN: " << m_TruthLeptons_classifierParticleOrigin->at(i)
                << " TYPE: " << m_TruthLeptons_classifierParticleType->at(i)
                << " BARCODE: " << m_TruthLeptons_barcode->at(i)
                << " PT: " << m_TruthLeptons_pt->at(i) * Charm::GeV
                << std::endl;
    }
    return false;
  }

  // Get Z boson Lorentz vector
  m_truth_Z = m_truth_lep1 + m_truth_lep2;

  return true;
}

float TruthInfo::diphoton_resonance_mass(std::vector<float> &diphoton_mass,
                                         float target) {
  float pi0_mass = 0.0;
  for (float mass : diphoton_mass) {
    if (pi0_mass == 0.0) {
      pi0_mass = mass;
    } else if (fabs(mass - target) < fabs(pi0_mass - target)) {
      pi0_mass = mass;
    }
  }
  return pi0_mass;
}

void TruthInfo::get_truth_D_mesons(int pdgID) {
  if (pdgID == 411) {
    m_truth_D_plus_mesons.clear();
  }
  if (pdgID == 413) {
    m_truth_D_star_mesons.clear();
    m_truth_D_star_pi0_mesons.clear();
  }

  if (pdgID == 431) {
    m_truth_D_s_mesons.clear();
  }

  if (!m_TruthParticles_Selected_pt->size()) return;

  for (unsigned int i = 0; i < m_TruthParticles_Selected_pt->size(); i++) {
    if (abs(m_TruthParticles_Selected_pdgId->at(i)) != pdgID) continue;
    if (pdgID == 411) {
      m_truth_D_plus_mesons.push_back(i);
    } else if (pdgID == 413) {
      m_truth_D_star_mesons.push_back(i);
      m_truth_D_star_pi0_mesons.push_back(i);
    } else if (pdgID == 431) {
      m_truth_D_s_mesons.push_back(i);
    }
  }
}

bool TruthInfo::lep_pass_truth_fiducial(bool zplusd, bool met_cut) {
  if (!zplusd) {
    if ((m_truth_lep1.Pt() > 30 && fabs(m_truth_lep1.Eta()) <= 2.5)) {
      if (met_cut) {
        if (m_truth_mt > 60 && m_truth_lep2.Pt() > 30) {
          return true;
        } else {
          return false;
        }
      }
      return true;
    }
  } else {
    if (fabs(m_truth_lep1_pdgId) == fabs(m_truth_lep2_pdgId)) {
      if (m_truth_lep1_charge * m_truth_lep2_charge < 0) {
        if (m_truth_lep1.Pt() >= 27. && fabs(m_truth_lep1.Eta()) <= 2.5) {
          if (m_truth_lep2.Pt() >= 27. && fabs(m_truth_lep2.Eta()) <= 2.5) {
            if (m_truth_Z.M() >= 76. && m_truth_Z.M() <= 106.) {
              return true;
            }
          }
        }
      }
    }
  }
  return false;
}

int TruthInfo::truthMatch(EventLoopBase *elb, int index,
                          std::string decayString, bool leptonTruth,
                          bool zplusd) {
  // get the DMesonRec object
  DMesonRec *DMR = dynamic_cast<DMesonRec *>(elb);

  // truth index
  int truthIndex = getMatchedBarcode(DMR->m_DMesons_truthBarcode->at(index));

  // checks if there is a fully truth matched D
  if (truthIndex > -1) {
    if ((m_TruthParticles_Selected_pdgId->at(truthIndex)) ==
        (DMR->m_DMesons_pdgId->at(index))) {
      // fully matched D
      if (get_decay_mode(truthIndex) == decayString) {
        if (!(pass_truth_fiducial(truthIndex) &&
              (!leptonTruth || lep_pass_truth_fiducial(zplusd)))) {
          return abs(m_TruthParticles_Selected_pdgId->at(truthIndex));
        }
        return 1;
      } else {
        return abs(m_TruthParticles_Selected_pdgId->at(truthIndex));
      }
    } else if (abs(m_TruthParticles_Selected_pdgId->at(truthIndex)) == 411 &&
               abs(DMR->m_DMesons_pdgId->at(index)) == 431 &&
               get_decay_mode(truthIndex) == decayString) {
      if (!(pass_truth_fiducial(truthIndex) &&
            (!leptonTruth || lep_pass_truth_fiducial(zplusd)))) {
        return abs(m_TruthParticles_Selected_pdgId->at(truthIndex));
      }
      return -2;
    } else {
      // fully mismatched D
      return abs(m_TruthParticles_Selected_pdgId->at(truthIndex));
    }
  } else if (truthIndex < 0 && DMR->m_DMesons_truthBarcode->at(index) > 0) {
    // D mismatched to unused D*, D+, D_s decay mode
    return 3;
  } else {
    // partial Matches
    int daugMatchedCode = nonZeroDaughters(DMR, index);
    if (daugMatchedCode > 0) {
      int truthIndexDaug = getMatchedBarcode(daugMatchedCode);
      // D partial mismatched to unused D*, D+, D_s decay mode
      if (truthIndexDaug == -1 && daugMatchedCode > 1) {
        int tmp;
        // Set GeoMatch to 0 to turn off by default
        int checkMatch = 0;  // checkGeoMatch(DMR, index, tmp);
        if (checkMatch == 0) {
          return 6;
        } else {
          return 4;
        }
      } else if (truthIndexDaug == -1) {
        int tmp;
        // Set GeoMatch to 0 to turn off by default
        int checkMatch = 0;  // checkGeoMatch(DMR, index, tmp);
        if (checkMatch == 0) {
          return 8;
        } else {
          return 4;
        }
      } else if (m_TruthParticles_Selected_pdgId->at(truthIndexDaug) ==
                 DMR->m_DMesons_pdgId->at(index)) {
        return 4;
      } else {
        return 5;
      }
    } else if (daugMatchedCode == 0) {
      int tmp;
      // Set GeoMatch to 0 to turn off by default
      int checkMatch = 0;  // checkGeoMatch(DMR, index, tmp);
      if (checkMatch == 0) {
        return 2;
      } else {
        return 4;
      }
    } else {
      return 7;
    }
  }
  return 0;
}

int TruthInfo::getMatchedBarcode(int recoCode) {
  if (recoCode < 1) {
    return -1;
  }
  std::vector<int>::iterator iter =
      std::find(m_TruthParticles_Selected_barcode->begin(),
                m_TruthParticles_Selected_barcode->end(), recoCode);
  if (iter == m_TruthParticles_Selected_barcode->cend()) {
    return -1;
  } else {
    return std::distance(m_TruthParticles_Selected_barcode->begin(), iter);
  }
}

int TruthInfo::checkGeoMatch(DMesonRec *DMR, int index, int &truth_index) {
  TLorentzVector reco_D;
  reco_D.SetPtEtaPhiM(
      DMR->m_DMesons_pt->at(index), DMR->m_DMesons_eta->at(index),
      DMR->m_DMesons_phi->at(index), DMR->m_DMesons_m->at(index));
  truth_index = -1;
  if (!(m_TruthParticles_Selected_pt->size())) {
    return 0;
  }

  for (int i = 0; i < m_TruthParticles_Selected_pt->size(); i++) {
    TLorentzVector truth_D;
    truth_D.SetPtEtaPhiM(m_TruthParticles_Selected_pt->at(i),
                         m_TruthParticles_Selected_eta->at(i),
                         m_TruthParticles_Selected_phi->at(i), 0.0);
    if (reco_D.DeltaR(truth_D) < 0.1) truth_index = i;
  }
  if (truth_index < 0) {
    return 0;
  } else {
    return m_TruthParticles_Selected_pdgId->at(truth_index);
  }
}

int TruthInfo::nonZeroDaughters(DMesonRec *DMR, int index) {
  int repeatCode = -1;
  int repeatCodeCount = 0;
  bool is_0_code = false;

  for (unsigned int i = 0;
       i < DMR->m_DMesons_daughterInfo__truthDBarcode->at(index).size(); i++) {
    int tmpCode = 0;
    int tmpCodeCount = 0;
    if (DMR->m_DMesons_daughterInfo__truthDBarcode->at(index).at(i) > 0) {
      tmpCode = DMR->m_DMesons_daughterInfo__truthDBarcode->at(index).at(i);
      tmpCodeCount = std::count(
          DMR->m_DMesons_daughterInfo__truthDBarcode->at(index).begin(),
          DMR->m_DMesons_daughterInfo__truthDBarcode->at(index).end(), tmpCode);
      if (tmpCodeCount > repeatCodeCount) {
        repeatCode = tmpCode;
        repeatCodeCount = tmpCodeCount;
      }
    } else if (DMR->m_DMesons_daughterInfo__truthDBarcode->at(index).at(i) ==
               0) {
      is_0_code = true;
    }
  }
  if (repeatCode > 0) {
    return repeatCode;
  } else if (repeatCode < 1 && is_0_code == true) {
    return 0;
  } else {
    return -1;
  }
}

float TruthInfo::truth_daughter_minTrackPt(unsigned int index) {
  float pt_min = 1000000.;
  for (unsigned int i = 0;
       i < m_TruthParticles_Selected_daughterInfoT__pdgId->at(index).size();
       i++) {
    if (abs(m_TruthParticles_Selected_daughterInfoT__pdgId->at(index).at(i)) !=
            211 &&
        abs(m_TruthParticles_Selected_daughterInfoT__pdgId->at(index).at(i)) !=
            321)
      continue;
    if (m_TruthParticles_Selected_daughterInfoT__pt->at(index).at(i) < pt_min) {
      pt_min = m_TruthParticles_Selected_daughterInfoT__pt->at(index).at(i);
    }
  }
  return pt_min;
}

float TruthInfo::truth_daughter_maxTrackEta(unsigned int index) {
  float eta_max = 0.;
  for (unsigned int i = 0;
       i < m_TruthParticles_Selected_daughterInfoT__pdgId->at(index).size();
       i++) {
    if (abs(m_TruthParticles_Selected_daughterInfoT__pdgId->at(index).at(i)) !=
            211 &&
        abs(m_TruthParticles_Selected_daughterInfoT__pdgId->at(index).at(i)) !=
            321)
      continue;
    if (fabs(m_TruthParticles_Selected_daughterInfoT__eta->at(index).at(i)) >
        fabs(eta_max)) {
      eta_max = m_TruthParticles_Selected_daughterInfoT__eta->at(index).at(i);
    }
  }
  return eta_max;
}

float TruthInfo::truth_daughter_minTrackDR(unsigned int index) {
  float dRmin = 1000;
  std::vector<float> *daughterInfo__eta =
      &m_TruthParticles_Selected_daughterInfoT__eta->at(index);
  std::vector<float> *daughterInfo__phi =
      &m_TruthParticles_Selected_daughterInfoT__phi->at(index);
  for (unsigned int i = 0; i < daughterInfo__eta->size(); i++) {
    if (abs(m_TruthParticles_Selected_daughterInfoT__pdgId->at(index).at(i)) !=
            321 &&
        abs(m_TruthParticles_Selected_daughterInfoT__pdgId->at(index).at(i)) !=
            211)
      continue;
    for (unsigned int j = i + 1; j < daughterInfo__eta->size(); j++) {
      if (abs(m_TruthParticles_Selected_daughterInfoT__pdgId->at(index).at(
              j)) != 321 &&
          abs(m_TruthParticles_Selected_daughterInfoT__pdgId->at(index).at(
              j)) != 211)
        continue;
      float deta = daughterInfo__eta->at(i) - daughterInfo__eta->at(j);
      float dphi = TVector2::Phi_mpi_pi(daughterInfo__phi->at(i) -
                                        daughterInfo__phi->at(j));
      float dR = TMath::Sqrt(deta * deta + dphi * dphi);
      if (dR < dRmin) dRmin = dR;
    }
  }
  return dRmin;
}

float TruthInfo::truth_daughter_maxTrackDR(unsigned int index) {
  float dRmax = 0.;
  std::vector<float> *daughterInfo__eta =
      &m_TruthParticles_Selected_daughterInfoT__eta->at(index);
  std::vector<float> *daughterInfo__phi =
      &m_TruthParticles_Selected_daughterInfoT__phi->at(index);
  for (unsigned int i = 0; i < daughterInfo__eta->size(); i++) {
    if (abs(m_TruthParticles_Selected_daughterInfoT__pdgId->at(index).at(i)) !=
            321 &&
        abs(m_TruthParticles_Selected_daughterInfoT__pdgId->at(index).at(i)) !=
            211) {
      continue;
    }
    for (unsigned int j = i + 1; j < daughterInfo__eta->size(); j++) {
      if (abs(m_TruthParticles_Selected_daughterInfoT__pdgId->at(index).at(
              j)) != 321 &&
          abs(m_TruthParticles_Selected_daughterInfoT__pdgId->at(index).at(
              j)) != 211) {
        continue;
      }
      float deta = daughterInfo__eta->at(i) - daughterInfo__eta->at(j);
      float dphi = TVector2::Phi_mpi_pi(daughterInfo__phi->at(i) -
                                        daughterInfo__phi->at(j));
      float dR = TMath::Sqrt(deta * deta + dphi * dphi);
      if (dR > dRmax) {
        dRmax = dR;
      }
    }
  }
  return dRmax;
}

float TruthInfo::truth_daughter_minTrackDEta(unsigned int index) {
  float detamin = 1000;
  std::vector<float> *daughterInfo__eta =
      &m_TruthParticles_Selected_daughterInfoT__eta->at(index);
  for (unsigned int i = 0; i < daughterInfo__eta->size(); i++) {
    if (abs(m_TruthParticles_Selected_daughterInfoT__pdgId->at(index).at(i)) !=
            321 &&
        abs(m_TruthParticles_Selected_daughterInfoT__pdgId->at(index).at(i)) !=
            211) {
      continue;
    }
    for (unsigned int j = i + 1; j < daughterInfo__eta->size(); j++) {
      if (abs(m_TruthParticles_Selected_daughterInfoT__pdgId->at(index).at(
              j)) != 321 &&
          abs(m_TruthParticles_Selected_daughterInfoT__pdgId->at(index).at(
              j)) != 211) {
        continue;
      }
      float deta = abs(daughterInfo__eta->at(i) - daughterInfo__eta->at(j));
      if (deta < detamin) {
        detamin = deta;
      }
    }
  }
  return detamin;
}

float TruthInfo::truth_daughter_minTrackDPhi(unsigned int index) {
  float dphimin = 1000;
  std::vector<float> *daughterInfo__phi =
      &m_TruthParticles_Selected_daughterInfoT__phi->at(index);
  for (unsigned int i = 0; i < daughterInfo__phi->size(); i++) {
    if (abs(m_TruthParticles_Selected_daughterInfoT__pdgId->at(index).at(i)) !=
            321 &&
        abs(m_TruthParticles_Selected_daughterInfoT__pdgId->at(index).at(i)) !=
            211) {
      continue;
    }
    for (unsigned int j = i + 1; j < daughterInfo__phi->size(); j++) {
      float dphi = TVector2::Phi_mpi_pi(daughterInfo__phi->at(i) -
                                        daughterInfo__phi->at(j));
      if (dphi < dphimin) {
        dphimin = dphi;
      }
    }
  }
  return dphimin;
}

bool TruthInfo::pass_track_truth(unsigned int index) {
  if (truth_daughter_minTrackPt(index) < 800.) {
    return false;
  }
  if (fabs(truth_daughter_maxTrackEta(index)) > 2.5) {
    return false;
  }
  if (truth_daughter_maxTrackDR(index) > 0.6) {
    return false;
  }
  return true;
}

bool TruthInfo::pass_truth_fiducial(unsigned int index) {
  if (m_TruthParticles_Selected_pt->at(index) * Charm::GeV > 8.0 &&
      m_TruthParticles_Selected_pt->at(index) * Charm::GeV < 150.0 &&
      fabs(m_TruthParticles_Selected_eta->at(index)) < 2.2) {
    return true;
  } else {
    return false;
  }
}

float TruthInfo::phi_resonance_mass(unsigned int index) {
  TLorentzVector phiCand;
  phiCand.SetPtEtaPhiM(0., 0., 0., 0.);
  int KaonCounter = 0;
  for (unsigned int i = 0;
       i < m_TruthParticles_Selected_daughterInfoT__pdgId->at(index).size();
       i++) {
    int pdgId = m_TruthParticles_Selected_daughterInfoT__pdgId->at(index).at(i);
    TLorentzVector tmp;
    if (fabs(pdgId) == 321) {
      tmp.SetPtEtaPhiM(
          m_TruthParticles_Selected_daughterInfoT__pt->at(index).at(i),
          m_TruthParticles_Selected_daughterInfoT__eta->at(index).at(i),
          m_TruthParticles_Selected_daughterInfoT__phi->at(index).at(i),
          KAON_MASS);
      phiCand += tmp;
      KaonCounter++;
    }
  }
  if (KaonCounter == 2) return phiCand.M();

  return 0.0;
}

std::string TruthInfo::get_truth_category(EventLoopBase *elb,
                                          unsigned int Dplus_index,
                                          std::string decayString,
                                          bool leptonTruth, bool zplusd,
                                          bool simplified) {
  // get truth class
  int TruthClass =
      truthMatch(elb, Dplus_index, decayString, leptonTruth, zplusd);
  std::string channel_out{};

  if (TruthClass == -1) {
    // -----------------------------------------------
    // never return -1 at the moment...
    // Non-fiducial match goes into 411MisMatch etc.
    // -----------------------------------------------
    // does not pass pT(D/lep) or eta(D/lep) truth cuts
    if (simplified) {
      return "MatchedNoFid";
      // return "MisMatched";
    } else {
      return "MatchedNoFid";
    }
  } else if (TruthClass == 1) {
    return "Matched";
  } else if (TruthClass == -2) {
    return "411Matched";
  } else if (TruthClass > 400) {
    if (simplified) {
      return "CharmMisMatched";
    } else {
      if (m_full_mis_match_info) {
        if (abs(TruthClass) < 1000) {
          return std::to_string(TruthClass) + "MisMatched";
        } else {
          return "BaryonMisMatched";
        }
      } else if (m_partial_mis_match_info) {
        if (abs(TruthClass) == 411) {
          return "411MisMatched";
        } else if (abs(TruthClass) == 413) {
          return "413MisMatched";
        } else if (abs(TruthClass) == 421) {
          return "421MisMatched";
        } else if (abs(TruthClass) == 431) {
          return "431MisMatched";
        } else if (abs(TruthClass) > 1000) {
          return "BaryonMisMatched";
        } else {
          return "MisMatched";
        }
      } else {
        return "MisMatched";
      }
    }
  } else if (TruthClass == 4) {
    if (m_full_mis_match_info) {
      return "MxMatched";
    } else {
      return "MisMatched";
    }
  } else if (TruthClass == 5) {
    if (m_full_mis_match_info) {
      return "MxMisMatched";
    } else {
      return "MisMatched";
    }
  } else if (TruthClass == 2 || TruthClass == 6 || TruthClass == 8) {
    if (m_full_mis_match_info || m_partial_mis_match_info) {
      return "HardMisMatched";
    } else {
      return "MisMatched";
    }
  } else if (TruthClass == 3) {
    return "HardMisMatched";
  } else if (TruthClass == 10) {
    return "ZeroNoMatchTr";
  } else if (TruthClass == 11) {
    return "OneNoMatchTr";
  } else if (TruthClass == 12) {
    return "TwoNoMatchTr";
  } else if (TruthClass == 13) {
    return "ThreeNoMatchTr";
  } else {
    if (simplified) {
      return "HardMisMatched";
    } else {
      return "Other";
    }
  }
}

bool TruthInfo::fill_truth_D_histograms(EventLoopBase *elb, std::string channel,
                                        unsigned int i) {
  bool Dhisto = true;
  if (m_TruthParticles_Selected_pt->at(i) * Charm::GeV < 8 ||
      fabs(m_TruthParticles_Selected_eta->at(i)) > 2.2) {
    return Dhisto;
  }
  if (m_created_channels.find(channel) == m_created_channels.end()) {
    m_created_channels.insert(channel);
    Dhisto = false;
  }

  elb->add_fill_histogram_fast_in_channel(
      channel, "D_pt0", m_TruthParticles_Selected_pt->at(i) / 1000., 200, 0,
      150., Dhisto);
  elb->add_fill_histogram_fast_in_channel(channel, "D_phi0",
                                          m_TruthParticles_Selected_phi->at(i),
                                          100, -M_PI, M_PI, Dhisto);
  elb->add_fill_histogram_fast_in_channel(channel, "D_eta0",
                                          m_TruthParticles_Selected_eta->at(i),
                                          100, -3.0, 3.0, Dhisto);
  elb->add_fill_histogram_fast_in_channel(
      channel, "D_decayMode", m_TruthParticles_Selected_decayMode->at(i), 6,
      -0.5, 5.5, Dhisto);
  elb->add_fill_histogram_fast_in_channel(channel, "D_Lxy0",
                                          m_TruthParticles_Selected_LxyT->at(i),
                                          100, 0, 30., Dhisto);
  elb->add_fill_histogram_fast_in_channel(
      channel, "D_pt0_x_eta0", m_TruthParticles_Selected_pt->at(i) / 1000.,
      m_TruthParticles_Selected_eta->at(i), 1000, 0., 150., 1000, -2.7, 2.7,
      Dhisto);
  elb->add_fill_histogram_fast_in_channel(
      channel, "D_pt0_x_MinTrackPt0",
      m_TruthParticles_Selected_pt->at(i) / 1000.,
      truth_daughter_minTrackPt(i) / 1000., 200, 0., 150., 700, 0., 70.,
      Dhisto);
  elb->add_fill_histogram_fast_in_channel(
      channel, "D_pt0_x_MaxTrackEta0",
      m_TruthParticles_Selected_pt->at(i) / 1000.,
      truth_daughter_maxTrackEta(i), 200, 0., 150., 100, -3.0, 3.0, Dhisto);
  elb->add_fill_histogram_fast_in_channel(
      channel, "D_pt0_x_MinTrackDR0",
      m_TruthParticles_Selected_pt->at(i) / 1000., truth_daughter_minTrackDR(i),
      200, 0., 150., 100, 0.0, 0.2, Dhisto);
  elb->add_fill_histogram_fast_in_channel(
      channel, "D_pt0_x_MinTrackDEta0",
      m_TruthParticles_Selected_pt->at(i) / 1000.,
      truth_daughter_minTrackDEta(i), 200, 0., 150., 100, 0.0, 0.2, Dhisto);
  elb->add_fill_histogram_fast_in_channel(
      channel, "D_pt0_x_MinTrackDPhi0",
      m_TruthParticles_Selected_pt->at(i) / 1000.,
      truth_daughter_minTrackDPhi(i), 200, 0., 150., 100, 0.0, 0.2, Dhisto);
  elb->add_fill_histogram_fast_in_channel(channel, "D_MaxTrackEta0",
                                          truth_daughter_maxTrackEta(i), 100,
                                          -3.0, 3.0, Dhisto);
  elb->add_fill_histogram_fast_in_channel(channel, "D_MinTrackPt0",
                                          truth_daughter_minTrackPt(i) / 1000.,
                                          400, 0, 40., Dhisto);
  elb->add_fill_histogram_fast_in_channel(channel, "D_MinTrackDR0",
                                          truth_daughter_minTrackDR(i), 100, 0,
                                          0.2, Dhisto);
  elb->add_fill_histogram_fast_in_channel(channel, "D_MinTrackDEta0",
                                          truth_daughter_minTrackDEta(i), 100,
                                          0, 0.2, Dhisto);
  elb->add_fill_histogram_fast_in_channel(channel, "D_MinTrackDPhi0",
                                          truth_daughter_minTrackDPhi(i), 100,
                                          0, 0.2, Dhisto);
  // elb->add_fill_histogram_fast_in_channel(channel, "D_TrackPt1",
  // m_TruthParticles_Selected_daughterInfoT__pt->at(i).at(0) / 1000., 100,
  // 0, 8., Dhisto); elb->add_fill_histogram_fast_in_channel(channel,
  // "D_TrackPt2", m_TruthParticles_Selected_daughterInfoT__pt->at(i).at(1) /
  // 1000., 100, 0, 8., Dhisto);
  // elb->add_fill_histogram_fast_in_channel(channel, "D_TrackPt3",
  // m_TruthParticles_Selected_daughterInfoT__pt->at(i).at(2) / 1000., 100,
  // 0, 8., Dhisto); elb->add_fill_histogram_fast_in_channel(channel,
  // "D_TrackEta1", m_TruthParticles_Selected_daughterInfoT__eta->at(i).at(0),
  // 100, -3.0, 3.0, Dhisto); elb->add_fill_histogram_fast_in_channel(channel,
  // "D_TrackEta2", m_TruthParticles_Selected_daughterInfoT__eta->at(i).at(1),
  // 100, -3.0, 3.0, Dhisto); elb->add_fill_histogram_fast_in_channel(channel,
  // "D_TrackEta3", m_TruthParticles_Selected_daughterInfoT__eta->at(i).at(2),
  // 100, -3.0, 3.0, Dhisto);
  return Dhisto;
}

void TruthInfo::fill_stand_alone_truth(EventLoopBase *elb) {
  std::string channel = "truth";
  bool histo_exists = true;
  if (m_created_channels.find(channel) == m_created_channels.end()) {
    m_created_channels.insert(channel);
    histo_exists = false;
  }

  // charmed hadrons with fiducial cuts
  get_charmed_hadrons();

  elb->add_fill_histogram_fast_in_channel(
      channel, "N_D",
      std::count(m_truth_charmed_hadrons.begin(), m_truth_charmed_hadrons.end(),
                 411),
      20, -0.5, 19.5, histo_exists);
  elb->add_fill_histogram_fast_in_channel(
      channel, "N_D0",
      std::count(m_truth_charmed_hadrons.begin(), m_truth_charmed_hadrons.end(),
                 421),
      20, -0.5, 19.5, histo_exists);
  elb->add_fill_histogram_fast_in_channel(
      channel, "N_DS",
      std::count(m_truth_charmed_hadrons.begin(), m_truth_charmed_hadrons.end(),
                 431),
      20, -0.5, 19.5, histo_exists);
  elb->add_fill_histogram_fast_in_channel(
      channel, "N_LambdaC",
      std::count(m_truth_charmed_hadrons.begin(), m_truth_charmed_hadrons.end(),
                 4122),
      20, -0.5, 19.5, histo_exists);
  elb->add_fill_histogram_fast_in_channel(
      channel, "N_Cascade",
      std::count(m_truth_charmed_hadrons.begin(), m_truth_charmed_hadrons.end(),
                 4232),
      20, -0.5, 19.5, histo_exists);
  elb->add_fill_histogram_fast_in_channel(
      channel, "N_Cascade0",
      std::count(m_truth_charmed_hadrons.begin(), m_truth_charmed_hadrons.end(),
                 4132),
      20, -0.5, 19.5, histo_exists);
  elb->add_fill_histogram_fast_in_channel(
      channel, "N_OmegaC",
      std::count(m_truth_charmed_hadrons.begin(), m_truth_charmed_hadrons.end(),
                 4332),
      20, -0.5, 19.5, histo_exists);
  elb->add_fill_histogram_fast_in_channel(
      channel, "N_D_from_B",
      std::count(m_truth_charmed_hadrons_from_B.begin(),
                 m_truth_charmed_hadrons_from_B.end(), 411),
      20, -0.5, 19.5, histo_exists);
  elb->add_fill_histogram_fast_in_channel(
      channel, "N_D0_from_B",
      std::count(m_truth_charmed_hadrons_from_B.begin(),
                 m_truth_charmed_hadrons_from_B.end(), 421),
      20, -0.5, 19.5, histo_exists);
  elb->add_fill_histogram_fast_in_channel(
      channel, "N_DS_from_B",
      std::count(m_truth_charmed_hadrons_from_B.begin(),
                 m_truth_charmed_hadrons_from_B.end(), 431),
      20, -0.5, 19.5, histo_exists);
  elb->add_fill_histogram_fast_in_channel(
      channel, "N_LambdaC_from_B",
      std::count(m_truth_charmed_hadrons_from_B.begin(),
                 m_truth_charmed_hadrons_from_B.end(), 4122),
      20, -0.5, 19.5, histo_exists);
  elb->add_fill_histogram_fast_in_channel(
      channel, "N_Cascade_from_B",
      std::count(m_truth_charmed_hadrons_from_B.begin(),
                 m_truth_charmed_hadrons_from_B.end(), 4232),
      20, -0.5, 19.5, histo_exists);
  elb->add_fill_histogram_fast_in_channel(
      channel, "N_Cascade0_from_B",
      std::count(m_truth_charmed_hadrons_from_B.begin(),
                 m_truth_charmed_hadrons_from_B.end(), 4132),
      20, -0.5, 19.5, histo_exists);
  elb->add_fill_histogram_fast_in_channel(
      channel, "N_OmegaC_from_B",
      std::count(m_truth_charmed_hadrons_from_B.begin(),
                 m_truth_charmed_hadrons_from_B.end(), 4332),
      20, -0.5, 19.5, histo_exists);
}

void TruthInfo::connect_truth_D_meson_branches(EventLoopBase *elb,
                                               bool do_truth_vjets,
                                               bool do_track_jets) {
  if (m_branches_already_added) {
    std::cout
        << "WARNING:: caling connect_truth_D_meson_branches twice. Skipping.."
        << std::endl;
    return;
  }
  m_branches_already_added = true;

  if (do_truth_vjets) {
    elb->add_presel_br("TruthLeptons_barcode", &m_TruthLeptons_barcode);
    elb->add_presel_br("TruthLeptons_classifierParticleOrigin",
                       &m_TruthLeptons_classifierParticleOrigin);
    elb->add_presel_br("TruthLeptons_classifierParticleType",
                       &m_TruthLeptons_classifierParticleType);
    elb->add_presel_br("TruthLeptons_e", &m_TruthLeptons_e);
    elb->add_presel_br("TruthLeptons_eta", &m_TruthLeptons_eta);
    elb->add_presel_br("TruthLeptons_m", &m_TruthLeptons_m);
    elb->add_presel_br("TruthLeptons_pdgId", &m_TruthLeptons_pdgId);
    elb->add_presel_br("TruthLeptons_phi", &m_TruthLeptons_phi);
    elb->add_presel_br("TruthLeptons_pt", &m_TruthLeptons_pt);
  }
  elb->add_presel_br("TruthParticles_Selected_barcode",
                     &m_TruthParticles_Selected_barcode);
  elb->add_presel_br("TruthParticles_Selected_cosThetaStarT",
                     &m_TruthParticles_Selected_cosThetaStarT);
  elb->add_presel_br("TruthParticles_Selected_daughterInfoT__eta",
                     &m_TruthParticles_Selected_daughterInfoT__eta);
  elb->add_presel_br("TruthParticles_Selected_daughterInfoT__pdgId",
                     &m_TruthParticles_Selected_daughterInfoT__pdgId);
  elb->add_presel_br("TruthParticles_Selected_daughterInfoT__barcode",
                     &m_TruthParticles_Selected_daughterInfoT__barcode);
  elb->add_presel_br("TruthParticles_Selected_daughterInfoT__phi",
                     &m_TruthParticles_Selected_daughterInfoT__phi);
  elb->add_presel_br("TruthParticles_Selected_daughterInfoT__pt",
                     &m_TruthParticles_Selected_daughterInfoT__pt);
  elb->add_presel_br("TruthParticles_Selected_decayMode",
                     &m_TruthParticles_Selected_decayMode);
  elb->add_presel_br("TruthParticles_Selected_eta",
                     &m_TruthParticles_Selected_eta);
  elb->add_presel_br("TruthParticles_Selected_fromBdecay",
                     &m_TruthParticles_Selected_fromBdecay);
  elb->add_presel_br("TruthParticles_Selected_ImpactT",
                     &m_TruthParticles_Selected_ImpactT);
  elb->add_presel_br("TruthParticles_Selected_LxyT",
                     &m_TruthParticles_Selected_LxyT);
  elb->add_presel_br("TruthParticles_Selected_m", &m_TruthParticles_Selected_m);
  elb->add_presel_br("TruthParticles_Selected_pdgId",
                     &m_TruthParticles_Selected_pdgId);
  elb->add_presel_br("TruthParticles_Selected_phi",
                     &m_TruthParticles_Selected_phi);
  elb->add_presel_br("TruthParticles_Selected_pt",
                     &m_TruthParticles_Selected_pt);
  if (do_truth_vjets && m_dressed_containers) {
    elb->add_presel_br("TruthLeptons_e_dressed", &m_TruthLeptons_e_dressed);
    elb->add_presel_br("TruthLeptons_eta_dressed", &m_TruthLeptons_eta_dressed);
    elb->add_presel_br("TruthLeptons_phi_dressed", &m_TruthLeptons_phi_dressed);
    elb->add_presel_br("TruthLeptons_pt_dressed", &m_TruthLeptons_pt_dressed);
  }
  if (do_track_jets) {
    for (const std::string &r : Charm::TRACK_JET_RADIUS) {
      elb->add_presel_br(Form("AntiKt%sTruthChargedJets_eta", r.c_str()),
                         &m_AntiKtTruthChargedJets_eta[r]);
      elb->add_presel_br(Form("AntiKt%sTruthChargedJets_m", r.c_str()),
                         &m_AntiKtTruthChargedJets_m[r]);
      elb->add_presel_br(Form("AntiKt%sTruthChargedJets_phi", r.c_str()),
                         &m_AntiKtTruthChargedJets_phi[r]);
      elb->add_presel_br(Form("AntiKt%sTruthChargedJets_pt", r.c_str()),
                         &m_AntiKtTruthChargedJets_pt[r]);
    }
  }
}

TruthInfo::TruthInfo() {
  m_full_mis_match_info = false;
  m_dressed_containers = true;
  m_partial_mis_match_info = true;
}

}  // end namespace Charm
