#include <math.h>

#include <iostream>

#include "CharmLoopBase.h"
#include "HelperFunctions.h"
#include "TVector2.h"

namespace Charm {

void CharmLoopBase::fill_D_s_histograms(std::string channel) {
  // number of D mesons
  if (m_fill_wjets && !m_fit_variables_only) {
    add_fill_hist_sys("N_Ds", channel, m_D_s_indices.size(), 20, -0.5, 19.5,
                      true);
  }

  // single entry region with at least one D meson
  if (m_do_single_entry_D) {
    auto single_entry_channel = std::vector<std::string>(m_nSyst, channel);
    auto sys_at_least_one_D = std::vector<bool>(m_nSyst, false);
    std::vector<float> multiplicity = std::vector<float>(m_nSyst, 0);
    bool has_D = true;
    for (unsigned int j = 0; j < m_nSyst; j++) {
      // loop over D s mesons
      for (unsigned int i = 0; i < m_D_s_indices.size(); i++) {
        // retreive the D meson
        const auto D_index = m_D_s_indices.at(i);

        // dR cut
        float dRlep = 999;
        if (m_do_lep_presel) {
          dRlep = DMesonRec::D_meson_lepton_dR(D_index, m_sys_lep_eta.at(j),
                                               m_sys_lep_phi.at(j));
        }
        if (dRlep >= 0.3) {
          sys_at_least_one_D.at(j) = true;
          multiplicity.at(j) += 1;
        }
      }
      if (multiplicity.at(j) > 0) {
        single_entry_channel.at(j) += "_Ds";
        has_D = true;
      }
    }
    if (m_do_single_entry_D_only && m_have_one_D)
      return;
    else if (m_do_single_entry_D_only && !m_have_one_D && has_D)
      m_have_one_D = true;

    if (m_do_lep_presel) {
      fill_lepton_histograms(single_entry_channel, sys_at_least_one_D);
    }
    if (m_do_PV) {
      fill_PV_histograms(single_entry_channel, sys_at_least_one_D);
    }
    add_fill_hist_sys("N_Ds", single_entry_channel, multiplicity, 20, -0.5,
                      19.5, true, sys_at_least_one_D);
    if (m_do_single_entry_D_only) {
      return;
    }
  }

  // counters for D multiplicity
  std::vector<std::unordered_map<std::string, unsigned int>>
      multiplicity_counter;
  std::set<std::string> used_channels;

  // loop over D s mesons
  for (unsigned int i = 0; i < m_D_s_indices.size(); i++) {
    // retreive the D meson
    const auto D_index = m_D_s_indices.at(i);

    // dR w.r.t. the lepton depends on systematic
    auto sys_dRlep = std::vector<float>(m_nSyst, 0.);

    // OS or SS region
    auto charged_channel = std::vector<std::string>(m_nSyst, channel);
    auto pass_dR_cut = std::vector<bool>(m_nSyst, false);
    for (unsigned int j = 0; j < m_nSyst; j++) {
      multiplicity_counter.push_back({});
      if (!m_do_lep_presel ||
          (event_pass_sys(j) && m_sys_lep_charge.at(j) != 0)) {
        // dR w.r.t. the lepton
        float dRlep = 999;
        float lep_charge = 0;
        if (m_do_lep_presel) {
          dRlep = DMesonRec::D_meson_lepton_dR(D_index, m_sys_lep_eta.at(j),
                                               m_sys_lep_phi.at(j));
          lep_charge = m_sys_lep_charge.at(j);
        }
        sys_dRlep.at(j) = dRlep;

        // dR cut
        if (dRlep < 0.3) {
          continue;
        }

        pass_dR_cut.at(j) = true;
        charged_channel.at(j) += "_Ds" + get_charge_prefix(lep_charge, D_index);
        multiplicity_counter.at(j)[charged_channel.at(j)]++;
        used_channels.insert(charged_channel.at(j));
      }
    }

    // // Sideband or central region
    // if (m_split_into_SB) {
    //     std::string region_name =
    //     Charm::get_D_s_mass_region(m_DMesons_m->at(D_index)); if
    //     (region_name == "") {
    //         continue;
    //     }
    //     charged_channel += "_" + region_name;
    // }

    // D s meson specific histograms
    // lepton histograms for this channel
    if (m_do_lep_presel) {
      fill_lepton_histograms(charged_channel, pass_dR_cut);
    }
    if (m_do_PV) {
      fill_PV_histograms(charged_channel, pass_dR_cut);
    }

    if (!m_fit_variables_only) {
      add_fill_hist_sys("Dmeson_dRlep", charged_channel, sys_dRlep, 120, 0., 6.,
                        false, pass_dR_cut);
    }
    DMesonRec::fill_D_s_histograms(this, charged_channel, D_index, pass_dR_cut);

    // D meson truth match
    if (m_is_mc && m_truth_matchD) {
      std::string truth_category = TruthInfo::get_truth_category(
          this, D_index, "Phipi", m_fiducial_selection);
      auto truth_channel = charged_channel;
      for (unsigned int j = 0; j < m_nSyst; j++) {
        if (!m_do_lep_presel ||
            (event_pass_sys(j) && m_sys_lep_charge.at(j) != 0 &&
             pass_dR_cut.at(j))) {
          truth_channel.at(j) += "_" + truth_category;
          multiplicity_counter.at(j)[truth_channel.at(j)]++;
          used_channels.insert(truth_channel.at(j));
        }
      }
      if (m_do_lep_presel) {
        fill_lepton_histograms(truth_channel, pass_dR_cut);
      }
      if (!m_fit_variables_only) {
        add_fill_hist_sys("Dmeson_dRlep", truth_channel, sys_dRlep, 120, 0., 6.,
                          false, pass_dR_cut);
      }
      DMesonRec::fill_D_s_histograms(this, truth_channel, D_index, pass_dR_cut);
      // Add truth matched hist if requested
      if (m_truth_D && truth_category == "Matched") {
        int truthIndex =
            TruthInfo::getMatchedBarcode(m_DMesons_truthBarcode->at(D_index));
        std::string truth_channel;
        // accounting for presence of neutrals in truth D+ decay mode (RM =
        // Right Mode)
        if (fabs(truth_daughter_mass(truthIndex) - DS_MASS) < 40) {
          truth_channel = channel + "_recoMatch_truth_Ds_RM";
          TruthInfo::fill_truth_D_histograms(this, truth_channel, truthIndex);
        }
        truth_channel = channel + "_Ds_recoMatch_truth";
        TruthInfo::fill_truth_D_histograms(this, truth_channel, truthIndex);
      }
      // Add truth matched hist if requested
      // if(m_truth_D && truth_category == "Matched"){
      //     int truthIndex =
      //     TruthInfo::getMatchedBarcode(m_DMesons_truthBarcode->at(D_index));
      //     std::string truth_channel;
      //     // accounting for presence of neutrals in truth D+ decay mode (RM =
      //     Right Mode) if(fabs(truth_daughter_mass(truthIndex) - DP_MASS) <
      //     40) truth_channel = channel + "_recoMatch_truth_Ds_RM"; else
      //     truth_channel = channel +
      //     "_Ds_recoMatch_truth";
      //     TruthInfo::fill_truth_D_histograms(this, truth_channel,
      //     truthIndex);
      // }
    }
  }

  // counting D multiplicity
  for (auto channel : used_channels) {
    std::vector<float> multiplicity;
    for (unsigned int j = 0; j < m_nSyst; j++) {
      multiplicity.push_back(multiplicity_counter.at(j)[channel]);
    }
    if (!m_fit_variables_only) {
      add_fill_hist_sys("N_Ds", channel, multiplicity, 20, -0.5, 19.5, true);
    }
  }

  // // pure D meson truth
  if (m_is_mc && m_truth_D) {
    for (unsigned int i = 0; i < m_truth_D_s_mesons.size(); i++) {
      unsigned int index = m_truth_D_s_mesons.at(i);
      std::string truth_channel;
      // accounting for presence of neutrals in truth D+ decay mode (RM = Right
      // Mode)
      if (fabs(truth_daughter_mass(index) - DS_MASS) < 40) {
        truth_channel = channel + "_truth_Ds_RM";
        TruthInfo::fill_truth_D_histograms(this, truth_channel, index);
      }
      truth_channel = channel + "_truth_Ds";
      TruthInfo::fill_truth_D_histograms(this, truth_channel, index);
    }
  }
}

}  // namespace Charm
