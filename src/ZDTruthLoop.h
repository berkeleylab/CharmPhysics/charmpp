#ifndef ZD_TRUTH_LOOP_H
#define ZD_TRUTH_LOOP_H

#include "EventLoopBase.h"
#include "HelperFunctions.h"
#include "TruthInfo.h"

namespace Charm {
class ZDTruthLoop : public EventLoopBase, public TruthInfo {
 public:
  ZDTruthLoop(TString input_file, TString out_path,
              TString tree_name = "TruthTree");

 private:
  virtual void connect_branches() override;

  virtual void initialize() override;

  virtual bool preselection() override;

  virtual int execute() override;

  float dR_min(std::vector<unsigned int> *selected);

  void fill_histograms(std::string channel = "");

  void fill_D_histograms(std::string channel, unsigned int i);

  void fill_Dplus_histograms(std::string channel = "",
                             bool requireDecay = false);

  void fill_Dstar_histograms(std::string channel = "",
                             bool requireDecay = false);

  void fill_Dzero_histograms(std::string channel = "",
                             bool requireDecay = false);

  void fill_eventNumber_histogram();

  void configure();

  // event info
  Float_t m_EventInfo_generatorWeight_NOSYS{};
  Float_t m_EventInfo_PileupWeight_NOSYS{};
  Float_t m_EventInfo_prodFracWeight_NOSYS{};

  // systematics
  sys_br<Float_t> m_sys_br_GEN{};
  sys_br<Float_t> m_sys_br_PROD_FRAC{};

  // prod frac reweight
  bool m_do_prod_fraction_rw{false};
  bool m_do_prod_fraction_rw_old{false};
};

}  // namespace Charm

#endif  // ZD_TRUTH_LOOP_H
