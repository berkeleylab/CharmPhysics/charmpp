#ifndef HELPER_FUNCTIONS_H
#define HELPER_FUNCTIONS_H

#include <algorithm>
#include <array>
#include <initializer_list>
#include <iostream>
#include <unordered_map>

#include "RooRealVar.h"
#include "TBranch.h"
#include "TF1.h"
#include "TGraphAsymmErrors.h"
#include "TH1.h"
#include "TH2.h"
#include "TRandom3.h"
#include "TString.h"

namespace Charm {
template <typename T>
bool is_in(const T &v, std::initializer_list<T> lst) {
  return std::find(std::begin(lst), std::end(lst), v) != std::end(lst);
}

template <typename T>
struct sys_br {
  std::vector<std::string> keys;
  std::vector<TBranch *> branches;
  std::vector<T> values;
  void init(std::vector<std::string> k) {
    for (unsigned int i = 0; i < k.size(); i++) {
      keys.push_back(k.at(i));
      values.push_back({});
    }
  }
};

template <typename T>
int countMatchingElements(const std::vector<T> &vector1,
                          const std::vector<T> &vector2) {
  int count = 0;

  // Sort the second vector for faster search using binary_search
  std::vector<T> sortedVector2 = vector2;
  std::sort(sortedVector2.begin(), sortedVector2.end());

  for (const T &element : vector1) {
    if (std::binary_search(sortedVector2.begin(), sortedVector2.end(),
                           element)) {
      count++;
    }
  }

  return count;
}

int getJetBin(double tj);

double getUnfoldingVariable(double tj, double zt);

int get_ptRel_variable(double jet_pt, double ptRel);

TString get_run_number_string(bool is_mc);

TString get_pileup_string(bool is_mc, std::string mc_period,
                          std::string data_period);

std::string basename(std::string filePath, bool withExtension = true,
                     char seperator = '/');

float get_cross_section(int dsid);

float get_sum_of_weights(int dsid, std::string period,
                         bool normalize_to_nevents = false);

float rescaled_d0(float d0, UInt_t run, bool is_mc);

std::string get_period(UInt_t run);

std::string get_mc_gen(int dsid);

std::string get_mc_shower(int dsid);

std::string get_sample_type(int dsid);

std::string get_D_plus_mass_region(float mass);

std::string get_charge_prefix(float lep_charge, int pdgId);

std::string get_lepton_charge_string(float charge);

bool is_forced_decay(int dsid);

double two_sided_cb_function(double x, const double *par);

struct differential_bins {
  const double m_differential_pt_bins[5] = {8, 12, 20, 40, 80};

  const double *get_differential_bins() { return m_differential_pt_bins; };

  int get_n_differential() {
    return sizeof(m_differential_pt_bins) / sizeof(m_differential_pt_bins[0]) -
           1;
  };

  int get_pt_bin(double pt) {
    if (pt >= m_differential_pt_bins[0]) {
      for (unsigned int i = 1; i <= get_n_differential(); i++) {
        if (pt < m_differential_pt_bins[i]) {
          return i - 1;
        }
      }
      return get_n_differential();
    } else {
      return -1;
    }
  }
};

struct differential_bins_eta {
  const double m_differential_eta_bins[6] = {0.0, 0.5, 1.0, 1.5, 2.0, 2.5};

  const double *get_differential_bins() { return m_differential_eta_bins; };

  int get_n_differential() {
    return sizeof(m_differential_eta_bins) /
               sizeof(m_differential_eta_bins[0]) -
           1;
  };

  int get_eta_bin(double eta) {
    if (eta >= m_differential_eta_bins[0]) {
      for (unsigned int i = 1; i <= get_n_differential(); i++) {
        if (eta < m_differential_eta_bins[i]) {
          return i - 1;
        }
      }
      return get_n_differential();
    } else {
      return -1;
    }
  }
};

struct zd_differential_bins_z_pt {
  // Bin edges: 0, 15, 25, 40, 65, 400
  const double m_zd_differential_z_pt_bins[6] = {0, 15, 25, 40, 65, 400};

  const double *get_differential_bins() { return m_zd_differential_z_pt_bins; };

  int get_n_differential() {
    return sizeof(m_zd_differential_z_pt_bins) /
               sizeof(m_zd_differential_z_pt_bins[0]) -
           1;
  };

  int get_pt_bin(double pt) {
    if (pt >= m_zd_differential_z_pt_bins[0]) {
      for (unsigned int i = 1; i <= get_n_differential(); i++) {
        if (pt < m_zd_differential_z_pt_bins[i]) {
          return i - 1;
        }
      }
      return get_n_differential();
    } else {
      return -1;
    }
  }
};

struct zd_differential_bins_d_pt {
  // Bin edges: 8, 10, 14, 20, 30, 150
  const double m_zd_differential_d_pt_bins[6] = {8, 10, 14, 20, 30, 150};

  const double *get_differential_bins() { return m_zd_differential_d_pt_bins; };

  int get_n_differential() {
    return sizeof(m_zd_differential_d_pt_bins) /
               sizeof(m_zd_differential_d_pt_bins[0]) -
           1;
  };

  int get_pt_bin(double pt) {
    if (pt >= m_zd_differential_d_pt_bins[0]) {
      for (unsigned int i = 1; i <= get_n_differential(); i++) {
        if (pt < m_zd_differential_d_pt_bins[i]) {
          return i - 1;
        }
      }
      return get_n_differential();
    } else {
      return -1;
    }
  }
};

struct matrix_method_helper {
  std::unordered_map<std::string, std::unique_ptr<TH1>> *real_rate_map;
  std::unordered_map<std::string, std::unique_ptr<TH1>> *fake_rate_map;
  std::unordered_map<std::string, std::unique_ptr<TH1>> *real_rate_map_incl;
  std::unordered_map<std::string, std::unique_ptr<TH1>> *fake_rate_map_incl;
  std::unordered_map<std::string, std::unique_ptr<TH1>> *rw_hists_map;
  void set_real_rate(
      std::unordered_map<std::string, std::unique_ptr<TH1>> *map) {
    real_rate_map = map;
  };
  void set_fake_rate(
      std::unordered_map<std::string, std::unique_ptr<TH1>> *map) {
    fake_rate_map = map;
  };
  void set_real_rate_incl(
      std::unordered_map<std::string, std::unique_ptr<TH1>> *map) {
    real_rate_map_incl = map;
  };
  void set_fake_rate_incl(
      std::unordered_map<std::string, std::unique_ptr<TH1>> *map) {
    fake_rate_map_incl = map;
  };
  void set_rw_hists(
      std::unordered_map<std::string, std::unique_ptr<TH1>> *map) {
    rw_hists_map = map;
  };

  // WARNING: names need to start with "MM_[EL/MU]"!
  std::vector<std::string> sys_variations = {
      "MM_EL_RR_STAT__1up", "MM_EL_RR_STAT__1down",
      "MM_EL_RR_MadGraph_Var",  // Real Rates variation using alternate MC
                                // (Sherpa default, MG var)
      //"MM_EL_RR_MadGraph_Var_BAD", // Real Rates variation using alternate MC
      //(Sherpa default, MG var)
      "MM_EL_FR_STAT__1up", "MM_EL_FR_STAT__1down",
      //"MM_EL_FR__Wjets_MadGraph_Up",
      //"MM_EL_FR__Wjets_MadGraph_Dn",
      "MM_EL_FR__Wjets_MadGraph_Var", "MM_EL_FR__QCD_Var_Up",
      "MM_EL_FR__QCD_Var_Dn",
      "MM_EL_FR_MET_Var",  // Fake Rates variation using MET < 30 GeV
      "MM_EL_NON_CLOSURE", "MM_MU_RR_STAT__1up", "MM_MU_RR_STAT__1down",
      "MM_MU_RR_MadGraph_Var",  // Real Rates variation using alternate MC
                                // (Sherpa default, MG var)
      //"MM_MU_RR_MadGraph_Var_BAD", // Real Rates variation using alternate MC
      //(Sherpa default, MG var)
      "MM_MU_FR_STAT__1up", "MM_MU_FR_STAT__1down",
      //"MM_MU_FR__Wjets_MadGraph_Up",
      //"MM_MU_FR__Wjets_MadGraph_Dn",
      "MM_MU_FR__Wjets_MadGraph_Var", "MM_MU_FR__QCD_Var_Up",
      "MM_MU_FR__QCD_Var_Dn", "MM_MU_NON_CLOSURE",
      "MM_MU_FR_MET_Var"  // Fake Rates variation using MET < 30 GeV
                          // "MM_REAL_RATE_SPECIES_DECAY__1up",
                          // "MM_REAL_RATE_SPECIES_DECAY__1down",
  };

  std::vector<std::string> sys_variations_incl = {
      "MM_EL_RR_STAT__1up",
      "MM_EL_RR_STAT__1down",
      "MM_EL_FR_STAT__1up",
      "MM_EL_FR_STAT__1down",
      "MM_EL_RR_ttbar_Var",
      "MM_EL_FR__Wjets_MadGraph_Var",
      "MM_EL_FR_MET_Var",
      "MM_MU_RR_STAT__1up",
      "MM_MU_RR_STAT__1down",
      "MM_MU_FR_STAT__1up",
      "MM_MU_FR_STAT__1down",
      "MM_MU_RR_ttbar_Var",
      "MM_MU_FR__Wjets_MadGraph_Var",
      "MM_MU_FR_MET_Var"};

  // fake rate
  double get_fake_rate(std::string sys, std::string channel, std::string period,
                       std::string Dspecies, int nbjet, float pt, float eta,
                       float met, bool &fake_rate_one) {
    std::string bjet_string = "0tag";
    if (nbjet > 0) {
      bjet_string = "1tag";
    }

    std::string period_string = "";
    if (period != "") {
      period_string += "_";
    }

    // rates and closure are calculated only from MET > 30 region
    std::string met_string = "mtCtrl_metCtrl_";
    std::string met_sys_string = "mtCtrl_metSig_";
    // if the fake rate is = 1.0, we need to set the OVER MJ uncertainty to 100%
    fake_rate_one = false;

    if (sys == "") {
      auto *histo =
          (*fake_rate_map)["Fake_Rate_Sh_2211_Multijet_" + period_string +
                           channel + "_QCD_" + met_string + bjet_string + "_" +
                           Dspecies + "_final"]
              .get();
      return histo->GetBinContent(histo->FindBin(pt, eta));
      // return histo->GetBinContent(histo->FindBin(pt, eta)) > 1.0 ? 1.0 :
      // histo->GetBinContent(histo->FindBin(pt, eta));
    }

    if ((sys.find("MM_EL") == 0 && channel == "el") ||
        (sys.find("MM_MU") == 0 && channel == "mu")) {
      if (sys.substr(6, sys.length() - 1) == "FR__Wjets_MadGraph_Up") {
        auto *histo =
            (*fake_rate_map)["Fake_Rate_MG_Multijet_Up_" + period_string +
                             channel + "_QCD_" + met_string + bjet_string +
                             "_" + Dspecies + "_final"]
                .get();
        return histo->GetBinContent(histo->FindBin(pt, eta));
      } else if (sys.substr(6, sys.length() - 1) == "FR__Wjets_MadGraph_Dn") {
        auto *histo =
            (*fake_rate_map)["Fake_Rate_MG_Multijet_Dn_" + period_string +
                             channel + "_QCD_" + met_string + bjet_string +
                             "_" + Dspecies + "_final"]
                .get();
        return histo->GetBinContent(histo->FindBin(pt, eta));
      } else if (sys.substr(6, sys.length() - 1) == "FR__Wjets_MadGraph_Var") {
        auto *histo =
            (*fake_rate_map)["Fake_Rate_MG_Multijet_" + period_string +
                             channel + "_QCD_" + met_string + bjet_string +
                             "_" + Dspecies + "_final"]
                .get();
        return histo->GetBinContent(histo->FindBin(pt, eta));
      } else if (sys.substr(6, sys.length() - 1) == "FR__QCD_Var_Dn") {
        auto *histo =
            (*fake_rate_map)["Fake_Rate_Sh_2211_Multijet_" + period_string +
                             channel + "_QCD_" + met_string + bjet_string +
                             "_" + Dspecies + "_final_GEN_sys_DN"]
                .get();
        float sys_bin_content = histo->GetBinContent(histo->FindBin(pt, eta));
        if (sys_bin_content < 0) {
          sys_bin_content = 0.0;
        }
        return sys_bin_content;
      } else if (sys.substr(6, sys.length() - 1) == "FR__QCD_Var_Up") {
        auto *histo =
            (*fake_rate_map)["Fake_Rate_Sh_2211_Multijet_" + period_string +
                             channel + "_QCD_" + met_string + bjet_string +
                             "_" + Dspecies + "_final_GEN_sys_UP"]
                .get();
        float sys_bin_content = histo->GetBinContent(histo->FindBin(pt, eta));
        if (sys_bin_content > .999) {
          auto *histo2 =
              (*fake_rate_map)["Fake_Rate_Sh_2211_Multijet_" + period_string +
                               channel + "_QCD_" + met_string + bjet_string +
                               "_" + Dspecies + "_final"]
                  .get();
          sys_bin_content = histo2->GetBinContent(histo->FindBin(pt, eta));
          fake_rate_one = true;
        }
        return sys_bin_content;
      } else if (sys.substr(6, sys.length() - 1) == "FR_STAT__1up") {
        auto *histo =
            (*fake_rate_map)["Fake_Rate_Sh_2211_Multijet_" + period_string +
                             channel + "_QCD_" + met_string + bjet_string +
                             "_" + Dspecies + "_final"]
                .get();
        return histo->GetBinContent(histo->FindBin(pt, eta)) +
               histo->GetBinError(histo->FindBin(pt, eta));
      } else if (sys.substr(6, sys.length() - 1) == "FR_STAT__1down") {
        auto *histo =
            (*fake_rate_map)["Fake_Rate_Sh_2211_Multijet_" + period_string +
                             channel + "_QCD_" + met_string + bjet_string +
                             "_" + Dspecies + "_final"]
                .get();
        float sys_bin_content = histo->GetBinContent(histo->FindBin(pt, eta)) -
                                histo->GetBinError(histo->FindBin(pt, eta));
        if (sys_bin_content < 0) {
          sys_bin_content = 0.0;
        }
        return sys_bin_content;
      } else if (sys.substr(6, sys.length() - 1) == "FR_MET_Var") {
        auto *histo =
            (*fake_rate_map)["Fake_Rate_Sh_2211_Multijet_" + period_string +
                             channel + "_QCD_" + met_sys_string + bjet_string +
                             "_" + Dspecies + "_final"]
                .get();
        float sys_bin_content = histo->GetBinContent(histo->FindBin(pt, eta));
        if (sys_bin_content < 0) {
          sys_bin_content = 0.0;
        } else if (sys_bin_content > 1.0) {
          sys_bin_content = 1.0;
        }
        return sys_bin_content;
      }
    }
    auto *histo =
        (*fake_rate_map)["Fake_Rate_Sh_2211_Multijet_" + period_string +
                         channel + "_QCD_" + met_string + bjet_string + "_" +
                         Dspecies + "_final"]
            .get();
    return histo->GetBinContent(histo->FindBin(pt, eta));
    // return histo->GetBinContent(histo->FindBin(pt, eta)) > 1.0 ? 1.0 :
    // histo->GetBinContent(histo->FindBin(pt, eta)) > 1.0;
  }

  // real rate
  double get_real_rate(std::string sys, std::string channel, std::string period,
                       std::string Dspecies, int nbjet, float pt, float eta,
                       float mt) {
    std::string bjet_string = "0tag";
    if (nbjet > 0) {
      bjet_string = "1tag";
    }

    std::string period_string = "";
    if (period != "") {
      period_string += "_";
    }

    std::string mt_string = "";
    if (mt < 40) {
      mt_string = "_CR";
    } else if (mt > 60) {
      mt_string = "_SR";
    } else {
      mt_string = "_VR";
    }

    Dspecies = "_" + Dspecies;

    if (sys == "") {
      auto *histo = (*real_rate_map)["Fake_Rate_" + period_string + channel +
                                     mt_string + "_final"]
                        .get();
      return histo->GetBinContent(histo->FindBin(pt, eta));
    }

    if ((sys.find("MM_EL") == 0 && channel == "el") ||
        (sys.find("MM_MU") == 0 && channel == "mu")) {
      if (sys.substr(6, sys.length() - 1) == "RR_STAT__1up") {
        auto *histo = (*real_rate_map)["Fake_Rate_" + period_string + channel +
                                       mt_string + "_final"]
                          .get();
        return histo->GetBinContent(histo->FindBin(pt, eta)) +
               histo->GetBinError(histo->FindBin(pt, eta));
      } else if (sys.substr(6, sys.length() - 1) == "RR_STAT__1down") {
        auto *histo = (*real_rate_map)["Fake_Rate_" + period_string + channel +
                                       mt_string + "_final"]
                          .get();
        return histo->GetBinContent(histo->FindBin(pt, eta)) -
               histo->GetBinError(histo->FindBin(pt, eta));
      } else if (sys.substr(6, sys.length() - 1) == "RR_SPECIES_DECAY__1up") {
        auto *histo = (*real_rate_map)["Fake_Rate_" + period_string + channel +
                                       mt_string + "_final"]
                          .get();
        return histo->GetBinContent(histo->FindBin(pt, eta)) +
               histo->GetBinContent(histo->FindBin(pt, eta)) * 0.02;
      } else if (sys.substr(6, sys.length() - 1) == "RR_SPECIES_DECAY__1down") {
        auto *histo = (*real_rate_map)["Fake_Rate_" + period_string + channel +
                                       mt_string + "_final"]
                          .get();
        return histo->GetBinContent(histo->FindBin(pt, eta)) -
               histo->GetBinContent(histo->FindBin(pt, eta)) * 0.02;
      } else if (sys.substr(6, sys.length() - 1) == "RR_MadGraph_Var") {
        auto *histo = (*real_rate_map)["Fake_Rate_" + period_string + channel +
                                       mt_string + "_sys_final"]
                          .get();
        return histo->GetBinContent(histo->FindBin(pt, eta));
      } else if (sys.substr(6, sys.length() - 1) == "RR_MadGraph_Var_BAD") {
        auto *histo = (*real_rate_map)["Fake_Rate_" + period_string + channel +
                                       mt_string + "_sys_final"]
                          .get();
        return histo->GetBinContent(histo->FindBin(pt, eta)) -
               histo->GetBinContent(histo->FindBin(pt, eta)) * 0.02;
      }
    }
    auto *histo = (*real_rate_map)["Fake_Rate_" + period_string + channel +
                                   mt_string + "_final"]
                      .get();
    return histo->GetBinContent(histo->FindBin(pt, eta));
  }

  // rw from met hist in closure DEPRECATED
  double get_closure_rw(std::string sys, std::string channel,
                        std::string period, std::string Dspecies, int nbjet,
                        float met) {
    std::string bjet_string = "0tag";
    if (nbjet > 0) {
      bjet_string = "1tag";
    }

    std::string period_string = "";
    if (period != "") {
      period_string += "_";
    }

    if (sys == "") {
      auto *histo =
          (*rw_hists_map)["RW_ratio_" + period_string + channel + "_QCD_" +
                          bjet_string + "_" + Dspecies + "_met_met_final"]
              .get();
      int hist_index = 0;
      if (histo->FindBin(met) >= histo->GetNbinsX()) {
        hist_index = histo->GetNbinsX();
      } else {
        hist_index = histo->FindBin(met);
      }

      return histo->GetBinContent(hist_index);
    }

    return 1.0;
  }

  // non-closure sys
  double get_non_closure_sys(std::string sys, std::string channel,
                             std::string period, std::string Dspecies,
                             int nbjet, float met) {
    std::string bjet_string = "0tag";
    if (nbjet > 0) {
      bjet_string = "1tag";
    }

    std::string period_string = "";
    if (period != "") {
      period_string += "_";
    }

    if (sys == "") {
      return 1.0;
    }

    if ((sys.find("MM_EL") == 0 && channel == "el") ||
        (sys.find("MM_MU") == 0 && channel == "mu")) {
      if (sys.substr(6, sys.length() - 1) == "NON_CLOSURE") {
        // Reweighting value set from charmplot non closure uncertainties.
        // Integral nonclosure ~5% thus set in all channels for conservative
        // coverage
        return 1.05;
      }
    }
    return 1.0;
  }

  double get_fake_rate_incl(std::string sys, std::string channel,
                            std::string period, float pt, float eta,
                            bool &fake_rate_one) {
    std::string period_string = "";
    if (period != "") {
      period_string += "_";
    }

    std::string met_string = "";
    // rates and closure are calculated only from MET > 30 region
    met_string = "mtCtrl_metCtrl_";
    std::string met_sys_string = "mtCtrl_metSig_";
    // if the fake rate is = 1.0, we need to set the OVER MJ uncertainty to 100%
    fake_rate_one = false;

    if (sys == "") {
      auto *histo =
          (*fake_rate_map_incl)["Fake_Rate_Sh_2211_Multijet_" + period_string +
                                channel + "_QCD_" + met_string + "final"]
              .get();
      return histo->GetBinContent(histo->FindBin(pt, eta));
      // return histo->GetBinContent(histo->FindBin(pt, eta)) > 1.0 ? 1.0 :
      // histo->GetBinContent(histo->FindBin(pt, eta));
    }

    if ((sys.find("MM_EL") == 0 && channel == "el") ||
        (sys.find("MM_MU") == 0 && channel == "mu")) {
      if (sys.substr(6, sys.length() - 1) == "FR_STAT__1up") {
        auto *histo = (*fake_rate_map_incl)["Fake_Rate_Sh_2211_Multijet_" +
                                            period_string + channel + "_QCD_" +
                                            met_string + "final"]
                          .get();
        float return_val = histo->GetBinContent(histo->FindBin(pt, eta)) +
                           histo->GetBinError(histo->FindBin(pt, eta));
        if (return_val > 1.0) {
          return 1.0;
        }
        return return_val;
      } else if (sys.substr(6, sys.length() - 1) == "FR_STAT__1down") {
        auto *histo = (*fake_rate_map_incl)["Fake_Rate_Sh_2211_Multijet_" +
                                            period_string + channel + "_QCD_" +
                                            met_string + "final"]
                          .get();
        float return_val = histo->GetBinContent(histo->FindBin(pt, eta)) -
                           histo->GetBinError(histo->FindBin(pt, eta));
        if (return_val < 0.0) {
          return 0.0;
        }
        return return_val;
      } else if (sys.substr(6, sys.length() - 1) == "FR__Wjets_MadGraph_Var") {
        auto *histo =
            (*fake_rate_map_incl)["Fake_Rate_MG_Multijet_" + period_string +
                                  channel + "_QCD_" + met_string + "final"]
                .get();
        float return_val = histo->GetBinContent(histo->FindBin(pt, eta));
        if (return_val > 1.0) {
          return 1.0;
        } else if (return_val < 0.0) {
          return 0.0;
        }
        return return_val;
      }

      else if (sys.substr(6, sys.length() - 1) == "FR_MET_Var") {
        auto *histo = (*fake_rate_map_incl)["Fake_Rate_Sh_2211_Multijet_" +
                                            period_string + channel + "_QCD_" +
                                            met_sys_string + "final"]
                          .get();
        float return_val = histo->GetBinContent(histo->FindBin(pt, eta));
        if (return_val > 1.0) {
          return 1.0;
        } else if (return_val < 0.0) {
          return 0.0;
        }
        return return_val;
      }
    }
    auto *histo =
        (*fake_rate_map_incl)["Fake_Rate_Sh_2211_Multijet_" + period_string +
                              channel + "_QCD_" + met_string + "final"]
            .get();
    return histo->GetBinContent(histo->FindBin(pt, eta));
    // return histo->GetBinContent(histo->FindBin(pt, eta)) > 1.0 ? 1.0 :
    // histo->GetBinContent(histo->FindBin(pt, eta)) > 1.0;
  }

  double get_real_rate_incl(std::string sys, std::string channel,
                            std::string period, float pt, float eta, float mt) {
    std::string period_string = "";
    if (period != "") {
      period_string += "_";
    }

    std::string mt_string = "";
    if (mt < 40) {
      mt_string = "_CR";
    } else if (mt > 60) {
      mt_string = "_SR";
    } else {
      mt_string = "_VR";
    }

    if (sys == "") {
      auto *histo =
          (*real_rate_map_incl)["Fake_Rate_Sh_2211_Wjets_emu_" + period_string +
                                channel + mt_string + "_final"]
              .get();
      return histo->GetBinContent(histo->FindBin(pt, eta));
    }

    if ((sys.find("MM_EL") == 0 && channel == "el") ||
        (sys.find("MM_MU") == 0 && channel == "mu")) {
      if (sys.substr(6, sys.length() - 1) == "RR_STAT__1up") {
        auto *histo = (*real_rate_map_incl)["Fake_Rate_Sh_2211_Wjets_emu_" +
                                            period_string + channel +
                                            mt_string + "_final"]
                          .get();
        float return_val = histo->GetBinContent(histo->FindBin(pt, eta)) +
                           histo->GetBinError(histo->FindBin(pt, eta));
        if (return_val > 1.0) {
          return 1.0;
        }
        return return_val;
      } else if (sys.substr(6, sys.length() - 1) == "RR_STAT__1down") {
        auto *histo = (*real_rate_map_incl)["Fake_Rate_Sh_2211_Wjets_emu_" +
                                            period_string + channel +
                                            mt_string + "_final"]
                          .get();
        return histo->GetBinContent(histo->FindBin(pt, eta)) -
               histo->GetBinError(histo->FindBin(pt, eta));
      } else if (sys.substr(6, sys.length() - 1) == "RR_ttbar_Var") {
        auto *histo =
            (*real_rate_map_incl)["Fake_Rate_Top_ttbar_" + period_string +
                                  channel + mt_string + "_final"]
                .get();
        return histo->GetBinContent(histo->FindBin(pt, eta));
      }
    }
    auto *histo =
        (*real_rate_map_incl)["Fake_Rate_Sh_2211_Wjets_emu_" + period_string +
                              channel + mt_string + "_final"]
            .get();
    return histo->GetBinContent(histo->FindBin(pt, eta));
  }
};

struct track_sys_helper {
  std::unordered_map<std::string, std::unique_ptr<TObject>> *sys_map;
  void set_map(std::unordered_map<std::string, std::unique_ptr<TObject>> *map) {
    sys_map = map;
  };

  std::vector<std::string> sys_variations = {
      "TRACK_EFF_IBL",
      "TRACK_EFF_Overal",
      "TRACK_EFF_PP0",
      "TRACK_EFF_QGSP",
      "TRACK_EFF_TRK_RES_D0_DEAD",
      "TRACK_EFF_TRK_RES_D0_MEAS",
      "TRACK_EFF_TRK_RES_Z0_DEAD",
      "TRACK_EFF_TRK_RES_Z0_MEAS",
  };

  std::map<int, std::string> pdgId_map = {
      {+413, "999961"},
      {+411, "999964"},
      {-413, "999971"},
      {-411, "999974"},
  };

  // track efficiecny
  double get_track_eff_weight(std::string sys, float pt, float eta, int pdgId) {
    if (std::find(sys_variations.begin(), sys_variations.end(), sys) !=
            sys_variations.end() &&
        sys.rfind("TRACK_EFF", 0) == 0 && (pt >= 5.0) && (fabs(eta) < 2.2)) {
      if (pt > 100.0) {
        pt = 99.9;
      }
      std::size_t pos = 9;
      auto *histo = dynamic_cast<TH2D *>(
          (*sys_map)[pdgId_map.at(pdgId) + sys.substr(pos) + "_rebinned"]
              .get());
      return histo->GetBinContent(histo->FindBin(fabs(eta), pt));
    } else {
      return 1.0;
    }
  }
};

struct mass_smear_helper {
  std::unordered_map<std::string, std::unique_ptr<TObject>> *sys_map;
  void set_map(std::unordered_map<std::string, std::unique_ptr<TObject>> *map) {
    sys_map = map;
  };

  std::vector<std::string> sys_variations = {
      "DMESON_RESO_TOT",       "DMESON_RESO_ADHOC_1",  "DMESON_RESO_ADHOC_2",
      "DMESON_RESO_ADHOC_4",   "DMESON_MASS_TOT_UP",   "DMESON_MASS_TOT_DN",
      "DMESON_MASS_ADHOC_UP",  "DMESON_MASS_ADHOC_DN", "DMESON_MASS_ADHOC_2UP",
      "DMESON_MASS_ADHOC_2DN",
  };

  std::map<int, std::string> pdgId_map = {
      {+411, "Dplus"},
      {-411, "Dplus"},
      {+413, "Dstar"},
      {-413, "Dstar"},
  };

  std::map<int, std::string> pdgId_map2 = {
      {+411, "DPlus"},
      {-411, "DMinus"},
      {+413, "DstarPlus"},
      {-413, "DstarMinus"},
  };

  // random number generator
  TRandom3 m_random;

  // invariant mass smearing
  double get_smeared_mass(std::string sys, unsigned int pt_bin, float eta,
                          int pdgId, float mass) {
    if (std::find(sys_variations.begin(), sys_variations.end(), sys) !=
            sys_variations.end() &&
        (sys.rfind("DMESON_", 0) == 0) && (fabs(eta) < 2.2) && (pt_bin >= 0)) {
      if (sys.rfind("DMESON_RESO_ADHOC", 0) == 0) {
        TH1D *vars = dynamic_cast<TH1D *>(
            (*sys_map)["pars_" + pdgId_map2.at(pdgId) + "_nominal_pt_bin" +
                       std::to_string(pt_bin + 1)]
                .get());

        // set the size of the uncertainty
        double sigma = 1;
        if (sys == "DMESON_RESO_ADHOC_4") {
          sigma = 4;
        } else if (sys == "DMESON_RESO_ADHOC_2") {
          sigma = 2;
        }

        // smear mass
        if (std::abs(pdgId) == 411) {
          // convert to MeV
          return mass +
                 m_random.Gaus(0.0, 1000 * vars->GetBinContent(6) / sigma);
        }
        return mass + m_random.Gaus(0.0, vars->GetBinContent(6) / sigma);
      } else if (sys == "DMESON_MASS_ADHOC_UP") {
        if (std::abs(pdgId) == 411) {
          return mass - 1.0;
        } else if (std::abs(pdgId) == 413) {
          return mass - 0.1;
        }
        return mass;
      } else if (sys == "DMESON_MASS_ADHOC_2UP") {
        if (std::abs(pdgId) == 411) {
          return mass - 2.0;
        } else if (std::abs(pdgId) == 413) {
          return mass - 0.2;
        }
        return mass;
      } else if (sys == "DMESON_MASS_ADHOC_DN") {
        if (std::abs(pdgId) == 411) {
          return mass + 1.0;
        } else if (std::abs(pdgId) == 413) {
          return mass + 0.1;
        }
        return mass;
      } else if (sys == "DMESON_MASS_ADHOC_2DN") {
        if (std::abs(pdgId) == 411) {
          return mass + 2.0;
        } else if (std::abs(pdgId) == 413) {
          return mass + 0.2;
        }
        return mass;
      } else if (sys == "DMESON_RESO_TOT") {
        RooRealVar *var = dynamic_cast<RooRealVar *>(
            (*sys_map)[pdgId_map.at(pdgId) + "_TRACK_EFF_TOT_pt_bin" +
                       std::to_string(pt_bin + 1) + "_sigma_diff"]
                .get());
        return mass + m_random.Gaus(0.0, var->getVal());
      } else if (sys == "DMESON_MASS_TOT_UP") {
        RooRealVar *var = dynamic_cast<RooRealVar *>(
            (*sys_map)[pdgId_map.at(pdgId) + "_TRACK_EFF_TOT_pt_bin" +
                       std::to_string(pt_bin + 1) + "_mean_diff"]
                .get());
        return mass - var->getVal();
      } else if (sys == "DMESON_MASS_TOT_DN") {
        RooRealVar *var = dynamic_cast<RooRealVar *>(
            (*sys_map)[pdgId_map.at(pdgId) + "_TRACK_EFF_TOT_pt_bin" +
                       std::to_string(pt_bin + 1) + "_mean_diff"]
                .get());
        return mass + var->getVal();
      }
    }
    return mass;
  }
};

struct nlo_mg_sys_helper {
  std::unordered_map<std::string, std::unique_ptr<TObject>> *histo_map;
  void set_map(std::unordered_map<std::string, std::unique_ptr<TObject>> *map) {
    histo_map = map;
  };

  std::vector<std::string> sys_variations = {
      "WJETS_THEORY_NOWEIGHT",
      "WJETS_THEORY_ALPHA_S",
      "WJETS_THEORY_PDF",
      "WJETS_THEORY_QCD_SCALE",
  };

  // wjets weight
  double get_wjets_weight(std::string sys, float pt, std::string species) {
    // histogram bounds are [8, 140]
    if (pt > 140) {
      pt = 135;
    }

    // histogram
    auto *histo = dynamic_cast<TH1F *>(
        (*histo_map)["Sherpa2211_WplusD_OS-SS_" + species + "_D_pt_truth_ratio"]
            .get());
    int bin = histo->FindBin(pt);

    // no weight or out of bounds
    if (sys == "WJETS_THEORY_NOWEIGHT" || pt < 8.0) {
      return 1.0;
    }
    // systematic variations
    else if (sys == "WJETS_THEORY_ALPHA_S") {
      auto *gr = dynamic_cast<TGraphAsymmErrors *>(
          (*histo_map)["Sherpa2211_WplusD_OS-SS_" + species +
                       "_D_pt_truth_ratio_sherpa2211_theory_as"]
              .get());
      return (histo->GetBinContent(bin) + gr->GetErrorYhigh(bin - 1));
    } else if (sys == "WJETS_THEORY_PDF") {
      auto *gr = dynamic_cast<TGraphAsymmErrors *>(
          (*histo_map)["Sherpa2211_WplusD_OS-SS_" + species +
                       "_D_pt_truth_ratio_sherpa2211_theory_pdf"]
              .get());
      return (histo->GetBinContent(bin) + gr->GetErrorYhigh(bin - 1));
    } else if (sys == "WJETS_THEORY_QCD_SCALE") {
      auto *gr = dynamic_cast<TGraphAsymmErrors *>(
          (*histo_map)["Sherpa2211_WplusD_OS-SS_" + species +
                       "_D_pt_truth_ratio_sherpa2211_theory_qcd"]
              .get());
      return (histo->GetBinContent(bin) + gr->GetErrorYhigh(bin - 1));
    } else {
      return histo->GetBinContent(bin);
    }
  }
};

struct prod_frac_weight_helper {
  std::vector<std::string> sys_variations = {
      "PROD_FRAC_NOWEIGHT",
      "PROD_FRAC_EIG_1",
      "PROD_FRAC_EIG_2",
      "PROD_FRAC_EIG_3",
  };

  // D+, D0, Ds, Lambda
  std::vector<float> nomi = {0.2404, 0.6086, 0.0802, 0.0623};
  std::vector<float> eig1 = {0.2405, 0.6087, 0.0770, 0.0650};
  std::vector<float> eig2 = {0.2439, 0.6108, 0.0779, 0.0593};
  std::vector<float> eig3 = {0.2461, 0.6013, 0.0808, 0.0632};

  // LO MG
  std::map<std::string, std::vector<float>> prod_fracs = {
      {"pythia", {0.2932, 0.5565, 0.0908, 0.0407}},
      {"sherpa2211", {0.240, 0.625, 0.077, 0.058}},
      {"herwig", {0.263746, 0.59287, 0.082093, 0.0612916}},
  };

  // get the production fraction weight
  float get_weight(std::string sys, int pdgid, std::string gen) {
    std::vector<float> *weights = &nomi;
    if (sys == "PROD_FRAC_NOWEIGHT") {
      return 1.0;
    } else if (sys == "PROD_FRAC_EIG_1") {
      weights = &eig1;
    } else if (sys == "PROD_FRAC_EIG_2") {
      weights = &eig2;
    } else if (sys == "PROD_FRAC_EIG_3") {
      weights = &eig3;
    }

    switch (pdgid) {
      case 411:
        return weights->at(0) / prod_fracs[gen].at(0);
      case 421:
        return weights->at(1) / prod_fracs[gen].at(1);
      case 431:
        return weights->at(2) / prod_fracs[gen].at(2);
      case 4122:
        return weights->at(3) / prod_fracs[gen].at(3);
      case 4232:
        return weights->at(3) / prod_fracs[gen].at(3);
      case 4132:
        return weights->at(3) / prod_fracs[gen].at(3);
      case 4332:
        return weights->at(3) / prod_fracs[gen].at(3);
      default:
        return 1.0;
    }
  }
};

struct spg_forced_decay_helper {
  std::unordered_map<std::string, std::unique_ptr<TObject>> *histo_map;
  void set_map(std::unordered_map<std::string, std::unique_ptr<TObject>> *map) {
    histo_map = map;
  };

  // double sided crystal ball for SPG
  // Double_t alphaHi = par[0]
  // Double_t alphaLo = par[1]
  // Double_t mean = par[2]
  // Double_t nHi = par[3]
  // Double_t nLo = par[4]
  // Double_t sigma = par[5]
  std::unordered_map<unsigned int, std::vector<double>> parameters_cache_spg;
  std::unordered_map<unsigned int, std::vector<double>> parameters_cache_mg;

  // get spg sample weight
  float get_weight(float mass, unsigned int pt_bin, std::string decay_mode) {
    if (pt_bin < 0) {
      return 1.0;
    }

    float weight = 1.0;

    if (decay_mode == "Dplus") {
      // mass rw for signal
      if (mass >= 1.7 && mass < 2.2) {
        std::vector<double> spg_pars;
        std::vector<double> lo_mg_pars;
        if (parameters_cache_spg.find(pt_bin) != parameters_cache_spg.end()) {
          spg_pars = parameters_cache_spg.at(pt_bin);
          lo_mg_pars = parameters_cache_mg.at(pt_bin);
        } else {
          unsigned int bin = pt_bin + 1;
          auto *spg_hist = dynamic_cast<TH1D *>(
              histo_map
                  ->at("pars_SPG_Matched_truth_pt_bin" + std::to_string(bin) +
                       "_OS_Dplus_Matched_truth_pt_bin" + std::to_string(bin) +
                       "_pt_bin" + std::to_string(bin) + "_Dmeson_m")
                  .get());
          auto *mg_hist = dynamic_cast<TH1D *>(
              histo_map
                  ->at("pars_Wjets_emu_Matched_truth_pt_bin" +
                       std::to_string(bin) + "_OS_Dplus_Matched_truth_pt_bin" +
                       std::to_string(bin) + "_pt_bin" + std::to_string(bin) +
                       "_Dmeson_m")
                  .get());
          for (unsigned int i = 1; i < 7; i++) {
            spg_pars.push_back(spg_hist->GetBinContent(i));
            lo_mg_pars.push_back(mg_hist->GetBinContent(i));
          }
          parameters_cache_spg[pt_bin] = spg_pars;
          parameters_cache_mg[pt_bin] = lo_mg_pars;
        }
        weight *= two_sided_cb_function(mass, &(lo_mg_pars.at(0))) /
                  two_sided_cb_function(mass, &(spg_pars.at(0)));
        std::cout << weight << std::endl;
      }
    }

    if (decay_mode == "Dstar") {
      // mass rw for signal
      if (mass >= 140 && mass < 150) {
        std::vector<double> spg_pars;
        std::vector<double> lo_mg_pars;
        if (parameters_cache_spg.find(pt_bin) != parameters_cache_spg.end()) {
          spg_pars = parameters_cache_spg.at(pt_bin);
          lo_mg_pars = parameters_cache_mg.at(pt_bin);
        } else {
          unsigned int bin = pt_bin + 1;
          auto *spg_hist = dynamic_cast<TH1D *>(
              histo_map
                  ->at("pars_SPG_Matched_truth_pt_bin" + std::to_string(bin) +
                       "_OS_Dstar_Matched_truth_pt_bin" + std::to_string(bin) +
                       "_pt_bin" + std::to_string(bin) + "_Dmeson_mdiff")
                  .get());
          auto *mg_hist = dynamic_cast<TH1D *>(
              histo_map
                  ->at("pars_Wjets_emu_Matched_truth_pt_bin" +
                       std::to_string(bin) + "_OS_Dstar_Matched_truth_pt_bin" +
                       std::to_string(bin) + "_pt_bin" + std::to_string(bin) +
                       "_Dmeson_mdiff")
                  .get());
          for (unsigned int i = 1; i < 7; i++) {
            spg_pars.push_back(spg_hist->GetBinContent(i));
            lo_mg_pars.push_back(mg_hist->GetBinContent(i));
          }
          parameters_cache_spg[pt_bin] = spg_pars;
          parameters_cache_mg[pt_bin] = lo_mg_pars;
        }
        weight *= two_sided_cb_function(mass, &(lo_mg_pars.at(0))) /
                  two_sided_cb_function(mass, &(spg_pars.at(0)));
        std::cout << weight << std::endl;
      }
    }

    return weight;
  }
};

struct modeling_uncertainty_helper {
  std::unordered_map<std::string, std::unique_ptr<TObject>> *weights_sys_map;
  void set_map(std::unordered_map<std::string, std::unique_ptr<TObject>> *map) {
    weights_sys_map = map;
  };

  std::vector<std::string> sys_variations = {
      // "FID_EFF_WPLUSD_MG_Wminus",
      // "FID_EFF_WPLUSD_MG_Wplus",
      "FID_EFF_WPLUSD_NLO_MG",   "FID_EFF_WPLUSD_MG_FxFx",
      "FID_EFF_WPLUSD_QCD__1up", "FID_EFF_WPLUSD_QCD__1down",
      "FID_EFF_WPLUSD_PDF__1up", "FID_EFF_WPLUSD_PDF__1down",
      "FID_EFF_WPLUSD_AS__1up",  "FID_EFF_WPLUSD_AS__1down",
  };

  // get the production fraction weight
  float get_weight_pt(std::string sys, float pt, std::string species) {
    // histogram bounds are [8, 140]
    if (pt > 140) {
      pt = 135;
    }

    // histogram
    if (pt < 8) {
      return 1.0;
    } else if (sys == "FID_EFF_WPLUSD_NLO_MG") {
      auto histo =
          dynamic_cast<TH1D *>(weights_sys_map
                                   ->at("MGPy8EG_NLO_WplusD_OS-SS_" + species +
                                        "_Kpipi_fid_eff_pt_ratio")
                                   .get());
      return histo->GetBinContent(histo->FindBin(pt));
    } else if (sys == "FID_EFF_WPLUSD_MG_FxFx") {
      auto histo = dynamic_cast<TH1D *>(
          weights_sys_map
              ->at("MGFxFx_WplusD_OS-SS_" + species + "_Kpipi_fid_eff_pt_ratio")
              .get());
      return histo->GetBinContent(histo->FindBin(pt));
    } else if (sys == "FID_EFF_WPLUSD_QCD__1up") {
      auto histo = dynamic_cast<TH1D *>(
          weights_sys_map
              ->at("Sherpa2211_WplusD_GEN_MUR2_MUF2_PDF303200_PSMUR2_PSMUF2_OS-"
                   "SS_" +
                   species + "_Kpipi_fid_eff_pt_ratio_qcd_err_up")
              .get());
      return histo->GetBinContent(histo->FindBin(pt));
    } else if (sys == "FID_EFF_WPLUSD_QCD__1down") {
      auto histo = dynamic_cast<TH1D *>(
          weights_sys_map
              ->at("Sherpa2211_WplusD_GEN_MUR2_MUF2_PDF303200_PSMUR2_PSMUF2_OS-"
                   "SS_" +
                   species + "_Kpipi_fid_eff_pt_ratio_qcd_err_dn")
              .get());
      return histo->GetBinContent(histo->FindBin(pt));
    } else if (sys == "FID_EFF_WPLUSD_PDF__1up") {
      auto histo = dynamic_cast<TH1D *>(
          weights_sys_map
              ->at("Sherpa2211_WplusD_GEN_MUR1_MUF1_PDF303300_OS-SS_" +
                   species + "_Kpipi_fid_eff_pt_ratio_pdf_err_up")
              .get());
      return histo->GetBinContent(histo->FindBin(pt));
    } else if (sys == "FID_EFF_WPLUSD_PDF__1down") {
      auto histo = dynamic_cast<TH1D *>(
          weights_sys_map
              ->at("Sherpa2211_WplusD_GEN_MUR1_MUF1_PDF303300_OS-SS_" +
                   species + "_Kpipi_fid_eff_pt_ratio_pdf_err_dn")
              .get());
      return histo->GetBinContent(histo->FindBin(pt));
    } else if (sys == "FID_EFF_WPLUSD_AS__1up") {
      auto histo = dynamic_cast<TH1D *>(
          weights_sys_map
              ->at("Sherpa2211_WplusD_GEN_MUR1_MUF1_PDF269000_OS-SS_" +
                   species + "_Kpipi_fid_eff_pt_ratio_as_up")
              .get());
      return histo->GetBinContent(histo->FindBin(pt));
    } else if (sys == "FID_EFF_WPLUSD_AS__1down") {
      auto histo = dynamic_cast<TH1D *>(
          weights_sys_map
              ->at("Sherpa2211_WplusD_GEN_MUR1_MUF1_PDF269000_OS-SS_" +
                   species + "_Kpipi_fid_eff_pt_ratio_as_dn")
              .get());
      return histo->GetBinContent(histo->FindBin(pt));
    } else {
      return 1.0;
    }
  }

  // get the production fraction weight
  float get_weight_eta(std::string sys, float eta, std::string species) {
    if (eta > 2.5 || eta < 0.0) {
      return 1.0;
    }

    if (sys == "FID_EFF_WPLUSD_NLO_MG") {
      auto histo =
          dynamic_cast<TH1D *>(weights_sys_map
                                   ->at("MGPy8EG_NLO_WplusD_OS-SS_" + species +
                                        "_Kpipi_fid_eff_eta_ratio")
                                   .get());
      return histo->GetBinContent(histo->FindBin(eta));
    } else if (sys == "FID_EFF_WPLUSD_MG_FxFx") {
      auto histo =
          dynamic_cast<TH1D *>(weights_sys_map
                                   ->at("MGFxFx_WplusD_OS-SS_" + species +
                                        "_Kpipi_fid_eff_eta_ratio")
                                   .get());
      return histo->GetBinContent(histo->FindBin(eta));
    } else if (sys == "FID_EFF_WPLUSD_QCD__1up") {
      auto histo = dynamic_cast<TH1D *>(
          weights_sys_map
              ->at("Sherpa2211_WplusD_GEN_MUR2_MUF2_PDF303200_PSMUR2_PSMUF2_OS-"
                   "SS_" +
                   species + "_Kpipi_fid_eff_eta_ratio_qcd_err_up")
              .get());
      return histo->GetBinContent(histo->FindBin(eta));
    } else if (sys == "FID_EFF_WPLUSD_QCD__1down") {
      auto histo = dynamic_cast<TH1D *>(
          weights_sys_map
              ->at("Sherpa2211_WplusD_GEN_MUR2_MUF2_PDF303200_PSMUR2_PSMUF2_OS-"
                   "SS_" +
                   species + "_Kpipi_fid_eff_eta_ratio_qcd_err_dn")
              .get());
      return histo->GetBinContent(histo->FindBin(eta));
    } else if (sys == "FID_EFF_WPLUSD_PDF__1up") {
      auto histo = dynamic_cast<TH1D *>(
          weights_sys_map
              ->at("Sherpa2211_WplusD_GEN_MUR1_MUF1_PDF303300_OS-SS_" +
                   species + "_Kpipi_fid_eff_eta_ratio_pdf_err_up")
              .get());
      return histo->GetBinContent(histo->FindBin(eta));
    } else if (sys == "FID_EFF_WPLUSD_PDF__1down") {
      auto histo = dynamic_cast<TH1D *>(
          weights_sys_map
              ->at("Sherpa2211_WplusD_GEN_MUR1_MUF1_PDF303300_OS-SS_" +
                   species + "_Kpipi_fid_eff_eta_ratio_pdf_err_dn")
              .get());
      return histo->GetBinContent(histo->FindBin(eta));
    } else if (sys == "FID_EFF_WPLUSD_AS__1up") {
      auto histo = dynamic_cast<TH1D *>(
          weights_sys_map
              ->at("Sherpa2211_WplusD_GEN_MUR1_MUF1_PDF269000_OS-SS_" +
                   species + "_Kpipi_fid_eff_eta_ratio_as_up")
              .get());
      return histo->GetBinContent(histo->FindBin(eta));
    } else if (sys == "FID_EFF_WPLUSD_AS__1down") {
      auto histo = dynamic_cast<TH1D *>(
          weights_sys_map
              ->at("Sherpa2211_WplusD_GEN_MUR1_MUF1_PDF269000_OS-SS_" +
                   species + "_Kpipi_fid_eff_eta_ratio_as_dn")
              .get());
      return histo->GetBinContent(histo->FindBin(eta));
    } else {
      return 1.0;
    }
  }
};

struct dplus_bkg_br_uncertainty {
  std::unordered_map<std::string, std::unique_ptr<TObject>> *weights_sys_map;
  void set_map(std::unordered_map<std::string, std::unique_ptr<TObject>> *map) {
    weights_sys_map = map;
  };

  std::vector<std::string> sys_variations = {
      "DPLUS_BKG_BR_Dplus",
      "DPLUS_BKG_BR_Dsubs",
      "DPLUS_BKG_BR_Dzero",
  };

  // get the production fraction weight
  float get_weight(std::string sys, float mass, std::string truth_category,
                   std::string charge) {
    // histogram bounds are [1.7, 2.2]
    if (mass > 2.2 || mass < 1.7) {
      return 1.0;
    }

    if (sys == "DPLUS_BKG_BR_Dplus" && truth_category == "411MisMatched") {
      auto histo =
          dynamic_cast<TH1F *>(weights_sys_map
                                   ->at("II_SPG_411MisMatched_OS_0tag_Dplus_"
                                        "411MisMatched_Dmeson_m_ratio")
                                   .get());
      return histo->GetBinContent(histo->FindBin(mass));
    } else if (sys == "DPLUS_BKG_BR_Dsubs" &&
               truth_category == "431MisMatched") {
      auto histo =
          dynamic_cast<TH1F *>(weights_sys_map
                                   ->at("II_SPG_431MisMatched_OS_0tag_Dplus_"
                                        "431MisMatched_Dmeson_m_ratio")
                                   .get());
      return histo->GetBinContent(histo->FindBin(mass));
    } else if (sys == "DPLUS_BKG_BR_Dzero" &&
               truth_category == "421MisMatched") {
      if (charge == "zplusd") {
        auto histo_os =
            dynamic_cast<TH1F *>(weights_sys_map
                                     ->at("II_SPG_421MisMatched_OS_0tag_Dplus_"
                                          "421MisMatched_Dmeson_m_ratio")
                                     .get());
        auto histo_ss =
            dynamic_cast<TH1F *>(weights_sys_map
                                     ->at("II_SPG_421MisMatched_SS_0tag_Dplus_"
                                          "421MisMatched_Dmeson_m_ratio")
                                     .get());
        auto os_bin_cont = histo_os->GetBinContent(histo_os->FindBin(mass));
        auto ss_bin_cont = histo_ss->GetBinContent(histo_ss->FindBin(mass));
        return (os_bin_cont + ss_bin_cont) / 2.;
      } else {
        auto histo = dynamic_cast<TH1F *>(
            weights_sys_map
                ->at("II_SPG_421MisMatched" + charge +
                     "_0tag_Dplus_421MisMatched_Dmeson_m_ratio")
                .get());
        return histo->GetBinContent(histo->FindBin(mass));
      }
    } else {
      return 1.0;
    }
  }
};

class TrackJetContainer {
 public:
  std::vector<float> *pt;
  std::vector<float> *eta;
  std::vector<float> *phi;
  std::vector<float> *m;
  std::vector<std::vector<float>> *daughter_pt;
  std::vector<std::vector<float>> *daughter_eta;
  std::vector<std::vector<float>> *daughter_phi;
  std::vector<std::vector<float>> *daughter_trackId;

  // Constructor
  TrackJetContainer(std::vector<float> *pt, std::vector<float> *eta,
                    std::vector<float> *phi, std::vector<float> *m,
                    std::vector<std::vector<float>> *daughter_pt = nullptr,
                    std::vector<std::vector<float>> *daughter_eta = nullptr,
                    std::vector<std::vector<float>> *daughter_phi = nullptr,
                    std::vector<std::vector<float>> *daughter_trackId = nullptr)
      : pt(pt),
        eta(eta),
        phi(phi),
        m(m),
        daughter_pt(daughter_pt),
        daughter_eta(daughter_eta),
        daughter_phi(daughter_phi),
        daughter_trackId(daughter_trackId) {}

  // Default constructor
  TrackJetContainer()
      : pt(nullptr),
        eta(nullptr),
        phi(nullptr),
        m(nullptr),
        daughter_pt(nullptr),
        daughter_eta(nullptr),
        daughter_phi(nullptr),
        daughter_trackId(nullptr) {}
};

struct SysTrackJet {
  std::vector<int> track_jet_index;
  std::vector<float> track_jet_dR;
  std::vector<float> track_jet_pt;
  std::vector<float> track_jet_eta;
  std::vector<float> track_jet_phi;
  std::vector<float> track_jet_zt;
  std::vector<float> track_jet_zt_unfold;
  std::vector<float> track_jet_zl;
  std::vector<float> track_jet_zl_unfold;
  std::vector<float> track_jet_pt_rel;
  std::vector<float> track_jet_pt_rel_unfold;
  std::vector<int> truth_track_jet_index;
  std::vector<float> truth_track_jet_dR;
  std::vector<float> truth_track_jet_pt;
  std::vector<float> truth_track_jet_eta;
  std::vector<float> truth_track_jet_phi;
  std::vector<float> truth_track_jet_zt;
  std::vector<float> truth_track_jet_zt_unfold;
  std::vector<float> truth_track_jet_zl;
  std::vector<float> truth_track_jet_zl_unfold;
  std::vector<float> truth_track_jet_pt_rel;
  SysTrackJet(long unsigned int n)
      : track_jet_index(n, -1),
        track_jet_dR(n, 999),
        track_jet_pt(n, -1),
        track_jet_eta(n, 999),
        track_jet_phi(n, 999),
        track_jet_zt(n, -1),
        track_jet_zt_unfold(n, -1),
        track_jet_zl(n, -1),
        track_jet_zl_unfold(n, -1),
        track_jet_pt_rel(n, -1),
        track_jet_pt_rel_unfold(n, -1),
        truth_track_jet_index(n, -1),
        truth_track_jet_dR(n, 999),
        truth_track_jet_pt(n, -1),
        truth_track_jet_eta(n, 999),
        truth_track_jet_phi(n, 999),
        truth_track_jet_zt(n, -1),
        truth_track_jet_zt_unfold(n, -1),
        truth_track_jet_zl(n, -1),
        truth_track_jet_zl_unfold(n, -1),
        truth_track_jet_pt_rel(n, -1) {}
};

struct SysJet {
  std::vector<int> jet_index;
  std::vector<float> jet_dR;
  std::vector<float> jet_pt;
  std::vector<float> jet_eta;
  std::vector<float> jet_phi;
  std::vector<float> jet_zt;
  std::vector<int> jet_truth_flavor;
  std::vector<float> jet_ftag_select_70;
  std::vector<float> jet_ftag_effSF;
  SysJet(long unsigned int n)
      : jet_index(n, -1),
        jet_dR(n, 999),
        jet_pt(n, -1),
        jet_eta(n, 999),
        jet_phi(n, 999),
        jet_zt(n, -1),
        jet_truth_flavor(n, -1),
        jet_ftag_select_70(n, -1),
        jet_ftag_effSF(n, -1){}
};


}  // namespace Charm

#endif  // HELPER_FUNCTIONS_H
