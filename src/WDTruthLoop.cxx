#include "WDTruthLoop.h"

namespace Charm {
void WDTruthLoop::initialize() {
  std::cout << "WDTruthLoop::initiaize" << std::endl;

  // configure
  configure();

  // systematics
  if (m_do_systematics) {
    // GEN
    m_sys_br_GEN.init(get_sys("systematics_GEN"));

    // Production fractions
    m_sys_br_PROD_FRAC.init(get_sys("systematics_PROD_FRAC"));
  }
}

float WDTruthLoop::dR_min(std::vector<unsigned int> *selected, bool OS) {
  float dR_min = 999;
  for (unsigned int i = 0; i < selected->size(); i++) {
    for (unsigned int j = i + 1; j < selected->size(); j++) {
      unsigned int index1 = selected->at(i);
      unsigned int index2 = selected->at(j);
      if (OS && (m_TruthParticles_Selected_pdgId->at(index1) *
                     m_TruthParticles_Selected_pdgId->at(index2) >
                 0.0)) {
        continue;
      }
      float deta = m_TruthParticles_Selected_eta->at(index1) -
                   m_TruthParticles_Selected_eta->at(index2);
      float dphi =
          TVector2::Phi_mpi_pi(m_TruthParticles_Selected_phi->at(index1) -
                               m_TruthParticles_Selected_phi->at(index2));
      float dR = TMath::Sqrt(deta * deta + dphi * dphi);
      if (dR < dR_min) {
        dR_min = dR;
      }
    }
  }
  return dR_min;
}

void WDTruthLoop::fill_Dplus_histograms(std::string channel,
                                        bool requireDecay) {
  // number of D, dR
  std::vector<unsigned int> selected = {};

  // truth D selection
  for (unsigned int i = 0; i < m_TruthParticles_Selected_decayMode->size(); i++) {
    // require PDG ID
    if (fabs(m_TruthParticles_Selected_pdgId->at(i)) != 411) {
      continue;
    }
    // truth fiducial pT cut
    if ((m_TruthParticles_Selected_pt->at(i) * Charm::GeV) < 8.0) {
      continue;
    }
    // truth fiducial eta cut
    if (fabs(m_TruthParticles_Selected_eta->at(i)) > 2.2) {
      continue;
    }
    // require the D+ -> K pi pi decay if required
    std::string mode = "";
    if (requireDecay) {
      mode = get_decay_mode(i);
      if (mode == "") {
        continue;
      }
    }
    // number of D, dR
    selected.push_back(i);

    //  channel name
    std::string name = get_charge_prefix(
        m_truth_lep1_charge, m_TruthParticles_Selected_pdgId->at(i));
    if (channel != "") {
      name += "_" + channel;
    }
    if (requireDecay) {
      name += "_" + mode;
    }

    fill_D_histograms("Dplus" + name, i);

    if (m_track_jet_selection) {
      fill_jet_histograms("Dplus" + name, i);
    }

    // fill output tree
    if (m_save_output_tree) {
      fill_truth_branches(i);
      fill_write_tree({"_Dplus" + name});
    }

    if (m_split_flavor == true) {
      if (m_TruthParticles_Selected_fromBdecay->at(i)) {
        name += "_b";
      } else {
        name += "_c";
      }

      fill_D_histograms("Dplus" + name, i);
    }
  }
  // overall plots
  std::string channel_name = !channel.empty() ? "_" + channel : "";
  auto channels = std::vector<std::string>(m_nSyst, channel_name);
  if (!requireDecay) {
    add_fill_hist_sys("N_Dplus", channels, selected.size(), 10, -0.5, 9.5,
                      false);
    add_fill_hist_sys("dR_min_Dplus", channels, dR_min(&selected), 240, 0.0,
                      6.0, false);
    add_fill_hist_sys("dR_min_OS_Dplus", channels, dR_min(&selected, true), 240,
                      0.0, 6.0, false);
  }
}

void WDTruthLoop::fill_Dstar_histograms(std::string channel,
                                        bool requireDecay) {
  // truth D selection
  for (unsigned int i = 0; i < m_TruthParticles_Selected_decayMode->size();
       i++) {
    // require PDG ID
    if (fabs(m_TruthParticles_Selected_pdgId->at(i)) != 413) {
      continue;
    }
    // truth fiducial pT cut
    if ((m_TruthParticles_Selected_pt->at(i) * Charm::GeV) < 8.0) {
      continue;
    }
    // truth fiducial eta cut
    if (fabs(m_TruthParticles_Selected_eta->at(i)) > 2.2) {
      continue;
    }
    // require the D+ -> K pi pi decay if required
    std::string mode = "";
    if (requireDecay) {
      mode = get_decay_mode(i);
      if (mode == "") {
        continue;
      }
    }
    // channel name
    std::string name = get_charge_prefix(
        m_truth_lep1_charge, m_TruthParticles_Selected_pdgId->at(i));
    if (channel != "") {
      name += "_" + channel;
    }
    if (requireDecay) {
      name += "_" + mode;
    }
    fill_D_histograms("Dstar" + name, i);

    if (m_track_jet_selection) {
      fill_jet_histograms("Dstar" + name, i);
    }

    // fill output tree
    if (m_save_output_tree) {
      fill_truth_branches(i);
      fill_write_tree({"_Dstar" + name});
    }

  }
}

void WDTruthLoop::fill_Dzero_histograms(std::string channel,
                                        bool requireDecay) {
  // number of D, dR
  std::vector<unsigned int> selected = {};

  // truth D selection
  for (unsigned int i = 0; i < m_TruthParticles_Selected_decayMode->size();
       i++) {
    // require PDG ID
    if (fabs(m_TruthParticles_Selected_pdgId->at(i)) != 421) {
      continue;
    }
    // truth fiducial pT cut
    if ((m_TruthParticles_Selected_pt->at(i) * Charm::GeV) < 8.0) {
      continue;
    }
    // truth fiducial eta cut
    if (fabs(m_TruthParticles_Selected_eta->at(i)) > 2.2) {
      continue;
    }
    // require the D0 -> K pi pi pi decay if required
    std::string mode = "";
    if (requireDecay) {
      mode = get_decay_mode(i);
      if (mode == "") {
        continue;
      }
    }
    // number of D, dR
    selected.push_back(i);

    // channel name
    std::string name = get_charge_prefix(
        m_truth_lep1_charge, m_TruthParticles_Selected_pdgId->at(i));
    if (channel != "") {
      name += "_" + channel;
    }
    if (requireDecay) {
      name += "_" + mode;
    }
    fill_D_histograms("Dzero" + name, i);
  }
  // overall plots
  std::string channel_name = !channel.empty() ? "_" + channel : "";
  auto channels = std::vector<std::string>(m_nSyst, channel_name);
  if (!requireDecay) {
    add_fill_hist_sys("N_Dzero", channels, selected.size(), 10, -0.5, 9.5,
                      false);
    add_fill_hist_sys("dR_min_Dzero", channels, dR_min(&selected), 240, 0.0,
                      6.0, false);
    add_fill_hist_sys("dR_min_OS_Dzero", channels, dR_min(&selected, true), 240,
                      0.0, 6.0, false);
  }
}

void WDTruthLoop::fill_Ds_histograms(std::string channel, bool requireDecay) {
  // number of D, dR
  std::vector<unsigned int> selected = {};

  // truth D selection
  for (unsigned int i = 0; i < m_TruthParticles_Selected_decayMode->size();
       i++) {
    // require PDG ID
    if (fabs(m_TruthParticles_Selected_pdgId->at(i)) != 431) {
      continue;
    }
    // truth fiducial pT cut
    if ((m_TruthParticles_Selected_pt->at(i) * Charm::GeV) < 8.0) {
      continue;
    }
    // truth fiducial eta cut
    if (fabs(m_TruthParticles_Selected_eta->at(i)) > 2.2) {
      continue;
    }
    // require the Ds -> phi pi -> K K pi decay if required
    std::string mode = "";
    if (requireDecay) {
      mode = get_decay_mode(i);
      if (mode == "") {
        continue;
      }
    }
    // number of D, dR
    selected.push_back(i);

    // channel name
    std::string name = get_charge_prefix(
        m_truth_lep1_charge, m_TruthParticles_Selected_pdgId->at(i));
    if (channel != "") {
      name += "_" + channel;
    }
    if (requireDecay) {
      name += "_" + mode;
    }
    fill_D_histograms("Ds" + name, i);
  }
  // overall plots
  std::string channel_name = !channel.empty() ? "_" + channel : "";
  auto channels = std::vector<std::string>(m_nSyst, channel_name);
  if (!requireDecay) {
    add_fill_hist_sys("N_Ds", channels, selected.size(), 10, -0.5, 9.5, false);
    add_fill_hist_sys("dR_min_Ds", channels, dR_min(&selected), 240, 0.0, 6.0,
                      false);
    add_fill_hist_sys("dR_min_OS_Ds", channels, dR_min(&selected, true), 240,
                      0.0, 6.0, false);
  }
}

void WDTruthLoop::fill_LambdaC_histograms(std::string channel, bool requireDecay) {
  // number of D, dR
  std::vector<unsigned int> selected = {};

  // truth D selection
  for (unsigned int i = 0; i < m_TruthParticles_Selected_decayMode->size();
       i++) {
    // require PDG ID
    if (fabs(m_TruthParticles_Selected_pdgId->at(i)) != 4122) {
      continue;
    }
    // truth fiducial pT cut
    if ((m_TruthParticles_Selected_pt->at(i) * Charm::GeV) < 8.0) {
      continue;
    }
    // truth fiducial eta cut
    if (fabs(m_TruthParticles_Selected_eta->at(i)) > 2.2) {
      continue;
    }
    // require the LambdaC -> p K pi decay if required
    std::string mode = "";
    if (requireDecay) {
      mode = get_decay_mode(i);
      if (mode == "") {
        continue;
      }
    }
    // number of D, dR
    selected.push_back(i);

    // channel name
    std::string name = get_charge_prefix(
        m_truth_lep1_charge, m_TruthParticles_Selected_pdgId->at(i));
    if (channel != "") {
      name += "_" + channel;
    }
    if (requireDecay) {
      name += "_" + mode;
    }
    fill_D_histograms("LambdaC" + name, i);

    // fill output tree
    if (m_save_output_tree) {
      fill_truth_branches(i);
      fill_write_tree({"_LambdaC" + name});
    }
  }
}

bool WDTruthLoop::preselection() {
  // Weighted by the MC16a/d/e lumi fraction
  multiply_mc_event_weight(m_EventInfo_generatorWeight_NOSYS, &m_sys_br_GEN,
                           true, true,
                           (m_mc_shower == "sherpa2211" && m_reweight_sherpa));

  // production fraction weight
  if (m_do_prod_fraction_rw) {
    multiply_nominal_and_sys_weight(m_EventInfo_prodFracWeight_NOSYS,
                                    &m_sys_br_PROD_FRAC);
  }

  // decipher wjets truth
  if (!get_truth_wjets(m_sample_type)) {
    return false;
  }

  // channel name
  auto channels = std::vector<std::string>(m_nSyst, m_truth_channel);
  m_channel_sys = channels;

  return true;
}

int WDTruthLoop::execute() {
  // 1. charged lepton pT > 30 GeV
  if (m_truth_lep1.Pt() < 30) {
    return 1;
  }

  // 2. charged lepton |eta| < 2.5
  if (fabs(m_truth_lep1.Eta()) > 2.5) {
    return 1;
  }

  // reweight
  if (m_do_prod_fraction_rw_old) {
    reweight_production_fractions(this);
  }
  if (m_do_wjets_rw) {
    reweight_wjets(this);
  }

  // inclusive histograms
  fill_histograms();

  // truth D+ selection
  fill_Dplus_histograms();
  fill_Dplus_histograms("", true);

  // truth D* selection
  fill_Dstar_histograms();
  fill_Dstar_histograms("", true);

  // truth D0 selection
  fill_Dzero_histograms();
  fill_Dzero_histograms("", true);

  // truth Ds selection
  fill_Ds_histograms();
  fill_Ds_histograms("", true);

  // truth LambdaC selection
  fill_LambdaC_histograms();
  fill_LambdaC_histograms("", true);

  // fiducial MET and mT cuts
  if (!m_do_fiducial_met) {
    return 1;
  }

  // 3. MET > 30 GeV
  if (m_truth_lep2.Pt() < 30) {
    return 1;
  }

  // 4. mT > 60 GeV
  if (m_truth_mt < 60) {
    return 1;
  }

  // fill fiducial histograms
  fill_histograms("fid");

  // truth D+ selection
  fill_Dplus_histograms("fid");
  fill_Dplus_histograms("fid", true);

  // truth D* selection
  fill_Dstar_histograms("fid");
  fill_Dstar_histograms("fid", true);

  // truth D0 selection
  fill_Dzero_histograms("fid");
  fill_Dzero_histograms("fid", true);

  // truth Ds selection
  fill_Ds_histograms("fid");
  fill_Ds_histograms("fid", true);

  return 1;
}

void WDTruthLoop::fill_D_histograms(std::string channel, unsigned int i) {
  std::vector<float> diphotons = get_diphoton_masses(i);
  std::string channel_name = !channel.empty() ? "_" + channel : "";
  auto channels = std::vector<std::string>(m_nSyst, channel_name);
  add_fill_hist_sys("D_pt", channels,
                    m_TruthParticles_Selected_pt->at(i) * Charm::GeV, 150, 0,
                    150, true);
  add_fill_hist_sys("D_eta", channels, m_TruthParticles_Selected_eta->at(i),
                    100, -3.0, 3.0, false);
  add_fill_hist_sys("D_phi", channels, m_TruthParticles_Selected_phi->at(i),
                    100, -M_PI, M_PI, false);
  add_fill_hist_sys("D_m", channels,
                    m_TruthParticles_Selected_m->at(i) * Charm::GeV, 300, 0.0,
                    3.0, false);
  add_fill_hist_sys("D_m_tracks", channels, truth_daughter_mass(i) * Charm::GeV,
                    300, 0.0, 3.0, false);
  add_fill_hist_sys("D_m_pi0", channels,
                    diphoton_resonance_mass(diphotons, PION0_MASS), 200, 0,
                    1000, false);
  add_fill_hist_sys("D_differential_pt", channels,
                    m_TruthParticles_Selected_pt->at(i) * Charm::GeV,
                    m_differential_bins.get_n_differential(),
                    m_differential_bins.get_differential_bins(), true);
  add_fill_hist_sys("D_differential_lep_eta", channels,
                    std::abs(m_truth_lep1.Eta()),
                    m_differential_bins_eta.get_n_differential(),
                    m_differential_bins_eta.get_differential_bins(), true);

  add_fill_hist_sys("D_Lxy", channels, m_TruthParticles_Selected_LxyT->at(i),
                    300, 0, 30., false);
  // proper decay time tau
  add_fill_hist_sys("D_ctau", channels,
                    m_TruthParticles_Selected_LxyT->at(i) *
                        m_TruthParticles_Selected_m->at(i) /
                        m_TruthParticles_Selected_pt->at(i),
                    200, 0, 2.0, false);
  // d0
  add_fill_hist_sys("D_d0", channels,
                    m_TruthParticles_Selected_ImpactT->at(i) * Charm::mum, 200,
                    -1100, 1100, false);
  fill_histograms(channel);
}

void WDTruthLoop::fill_histograms(std::string channel) {
  std::string channel_name = !channel.empty() ? "_" + channel : "";
  auto channels = std::vector<std::string>(m_nSyst, channel_name);
  add_fill_hist_sys("lep_pt", channels, m_truth_lep1.Pt(), 400, 0, 400, false);
  add_fill_hist_sys("lep_eta", channels, m_truth_lep1.Eta(), 100, -3.0, 3.0, false);
  add_fill_hist_sys("lep_phi", channels, m_truth_lep1.Phi(), 100, -M_PI, M_PI, false);
  add_fill_hist_sys("lep_charge", channels, m_truth_lep1_charge, 2, -2, 2, false);
  add_fill_hist_sys("v_pt", channels, m_truth_v.Pt(), 400, 0, 400, false);
  add_fill_hist_sys("v_eta", channels, m_truth_v.Eta(), 100, -3.0, 3.0, false);
  add_fill_hist_sys("v_phi", channels, m_truth_v.Phi(), 100, -M_PI, M_PI, false);
  add_fill_hist_sys("v_m", channels, m_truth_v.M(), 400, 0, 400, false);
  add_fill_hist_sys("met_met", channels, m_truth_lep2.Pt(), 400, 0, 400, false);
  add_fill_hist_sys("met_mt", channels, m_truth_mt, 400, 0, 400, false);
}

void WDTruthLoop::fill_jet_histograms(std::string channel, unsigned int i) {
  // whatever code this is
  std::string channel_name = !channel.empty() ? "_" + channel : "";
  auto channels = std::vector<std::string>(m_nSyst, channel_name);

  // find matching truth jet
  int jet_10_index = -1;
  float jet_10_dR = 999;
  float jet_10_zt = 0.0;
  float jet_10_zl = 0.0;
  float jet_10_pt = 0.0;
  float jet_10_eta = 999;
  float jet_10_phi = 999;
  int jet_8_index = -1;
  float jet_8_dR = 999;
  float jet_8_zt = 0.0;
  float jet_8_zl = 0.0;
  float jet_8_pt = 0.0;
  float jet_8_eta = 999;
  float jet_8_phi = 999;
  int jet_6_index = -1;
  float jet_6_dR = 999;
  float jet_6_zt = 0.0;
  float jet_6_zl = 0.0;
  float jet_6_pt = 0.0;
  float jet_6_eta = 999;
  float jet_6_phi = 999;

  TVector3 Dmeson_p3;
  Dmeson_p3.SetPtEtaPhi(m_TruthParticles_Selected_pt->at(i),
                        m_TruthParticles_Selected_eta->at(i),
                        m_TruthParticles_Selected_phi->at(i));

  // dR = 1.0
  for (unsigned int jet = 0; jet < m_AntiKtTruthChargedJets_pt["10"]->size();
       jet++) {
    float deta = m_AntiKtTruthChargedJets_eta["10"]->at(jet) -
                 m_TruthParticles_Selected_eta->at(i);
    float dphi =
        TVector2::Phi_mpi_pi(m_AntiKtTruthChargedJets_phi["10"]->at(jet) -
                             m_TruthParticles_Selected_phi->at(i));
    float dR = TMath::Sqrt(deta * deta + dphi * dphi);
    if (dR < jet_10_dR) {
      jet_10_dR = dR;
      jet_10_index = jet;
    }
  }
  if (jet_10_index >= 0) {
    jet_10_pt = m_AntiKtTruthChargedJets_pt["10"]->at(jet_10_index);
    jet_10_eta = m_AntiKtTruthChargedJets_eta["10"]->at(jet_10_index);
    jet_10_phi = m_AntiKtTruthChargedJets_phi["10"]->at(jet_10_index);
    jet_10_zt = m_TruthParticles_Selected_pt->at(i) /
                m_AntiKtTruthChargedJets_pt["10"]->at(jet_10_index);
    TVector3 Dmeson_j3;
    Dmeson_j3.SetPtEtaPhi(m_AntiKtTruthChargedJets_pt["10"]->at(jet_10_index),
                          m_AntiKtTruthChargedJets_eta["10"]->at(jet_10_index),
                          m_AntiKtTruthChargedJets_phi["10"]->at(jet_10_index));
    jet_10_zl = Dmeson_p3.Dot(Dmeson_j3) / Dmeson_j3.Mag2();
  }

  // dR = 0.8
  for (unsigned int jet = 0; jet < m_AntiKtTruthChargedJets_pt["8"]->size();
       jet++) {
    float deta = m_AntiKtTruthChargedJets_eta["8"]->at(jet) -
                 m_TruthParticles_Selected_eta->at(i);
    float dphi =
        TVector2::Phi_mpi_pi(m_AntiKtTruthChargedJets_phi["8"]->at(jet) -
                             m_TruthParticles_Selected_phi->at(i));
    float dR = TMath::Sqrt(deta * deta + dphi * dphi);
    if (dR < jet_8_dR) {
      jet_8_dR = dR;
      jet_8_index = jet;
    }
  }
  if (jet_8_index >= 0) {
    jet_8_pt = m_AntiKtTruthChargedJets_pt["8"]->at(jet_8_index);
    jet_8_eta = m_AntiKtTruthChargedJets_eta["8"]->at(jet_8_index);
    jet_8_phi = m_AntiKtTruthChargedJets_phi["8"]->at(jet_8_index);
    jet_8_zt = m_TruthParticles_Selected_pt->at(i) /
               m_AntiKtTruthChargedJets_pt["8"]->at(jet_8_index);
    TVector3 Dmeson_j3;
    Dmeson_j3.SetPtEtaPhi(m_AntiKtTruthChargedJets_pt["8"]->at(jet_8_index),
                          m_AntiKtTruthChargedJets_eta["8"]->at(jet_8_index),
                          m_AntiKtTruthChargedJets_phi["8"]->at(jet_8_index));
    jet_8_zl = Dmeson_p3.Dot(Dmeson_j3) / Dmeson_j3.Mag2();
  }

  // dR = 0.6
  for (unsigned int jet = 0; jet < m_AntiKtTruthChargedJets_pt["6"]->size();
       jet++) {
    float deta = m_AntiKtTruthChargedJets_eta["6"]->at(jet) -
                 m_TruthParticles_Selected_eta->at(i);
    float dphi =
        TVector2::Phi_mpi_pi(m_AntiKtTruthChargedJets_phi["6"]->at(jet) -
                             m_TruthParticles_Selected_phi->at(i));
    float dR = TMath::Sqrt(deta * deta + dphi * dphi);
    if (dR < jet_6_dR) {
      jet_6_dR = dR;
      jet_6_index = jet;
    }
  }
  if (jet_6_index >= 0) {
    jet_6_pt = m_AntiKtTruthChargedJets_pt["6"]->at(jet_6_index);
    jet_6_eta = m_AntiKtTruthChargedJets_eta["6"]->at(jet_6_index);
    jet_6_phi = m_AntiKtTruthChargedJets_phi["6"]->at(jet_6_index);
    jet_6_zt = m_TruthParticles_Selected_pt->at(i) /
               m_AntiKtTruthChargedJets_pt["6"]->at(jet_6_index);
    TVector3 Dmeson_j3;
    Dmeson_j3.SetPtEtaPhi(m_AntiKtTruthChargedJets_pt["6"]->at(jet_6_index),
                          m_AntiKtTruthChargedJets_eta["6"]->at(jet_6_index),
                          m_AntiKtTruthChargedJets_phi["6"]->at(jet_6_index));
    jet_6_zl = Dmeson_p3.Dot(Dmeson_j3) / Dmeson_j3.Mag2();
  }

  // fill histograms
  add_fill_hist_sys("D_jet_10_pt", channels, jet_10_pt * Charm::GeV, 150, 0, 150, true);
  add_fill_hist_sys("D_jet_10_eta", channels, jet_10_eta, 100, -3.0, 3.0, false);
  add_fill_hist_sys("D_jet_10_phi", channels, jet_10_phi, 100, -M_PI, M_PI, false);
  add_fill_hist_sys("D_jet_10_dR", channels, jet_10_dR, 120, 0., 6., false);
  add_fill_hist_sys("D_jet_10_zt", channels, jet_10_zt, 120, 0., 1.2, false);
  add_fill_hist_sys("D_jet_10_zt", channels, jet_10_zl, 120, 0., 1.2, false);
  add_fill_hist_sys("D_jet_8_pt", channels, jet_8_pt * Charm::GeV, 150, 0, 150, true);
  add_fill_hist_sys("D_jet_8_eta", channels, jet_8_eta, 100, -3.0, 3.0, false);
  add_fill_hist_sys("D_jet_8_phi", channels, jet_8_phi, 100, -M_PI, M_PI, false);
  add_fill_hist_sys("D_jet_8_dR", channels, jet_8_dR, 120, 0., 6., false);
  add_fill_hist_sys("D_jet_8_zt", channels, jet_8_zt, 120, 0., 1.2, false);
  add_fill_hist_sys("D_jet_8_zt", channels, jet_8_zl, 120, 0., 1.2, false);
  add_fill_hist_sys("D_jet_6_pt", channels, jet_6_pt * Charm::GeV, 150, 0, 150, true);
  add_fill_hist_sys("D_jet_6_eta", channels, jet_6_eta, 100, -3.0, 3.0, false);
  add_fill_hist_sys("D_jet_6_phi", channels, jet_6_phi, 100, -M_PI, M_PI, false);
  add_fill_hist_sys("D_jet_6_dR", channels, jet_6_dR, 120, 0., 6., false);
  add_fill_hist_sys("D_jet_6_zt", channels, jet_6_zt, 120, 0., 1.2, false);
  add_fill_hist_sys("D_jet_6_zt", channels, jet_6_zl, 120, 0., 1.2, false);

  // output tree
  if (m_save_output_tree) {
    m_out_truth_Dmeson_track_jet_6_pt = jet_6_pt * Charm::GeV;
    m_out_truth_Dmeson_track_jet_6_eta = jet_6_eta;
    m_out_truth_Dmeson_track_jet_6_phi = jet_6_phi;
    m_out_truth_Dmeson_track_jet_6_zt = jet_6_zt;
    m_out_truth_Dmeson_track_jet_6_zl = jet_6_zl;
    m_out_truth_Dmeson_track_jet_8_pt = jet_8_pt * Charm::GeV;
    m_out_truth_Dmeson_track_jet_8_eta = jet_8_eta;
    m_out_truth_Dmeson_track_jet_8_phi = jet_8_phi;
    m_out_truth_Dmeson_track_jet_8_zt = jet_8_zt;
    m_out_truth_Dmeson_track_jet_8_zl = jet_8_zl;
    m_out_truth_Dmeson_track_jet_10_pt = jet_10_pt * Charm::GeV;
    m_out_truth_Dmeson_track_jet_10_eta = jet_10_eta;
    m_out_truth_Dmeson_track_jet_10_phi = jet_10_phi;
    m_out_truth_Dmeson_track_jet_10_zt = jet_10_zt;
    m_out_truth_Dmeson_track_jet_10_zl = jet_10_zl;
  }
}

void WDTruthLoop::connect_branches() {
  connect_truth_D_meson_branches(this, true, m_track_jet_selection);

  // event info
  add_presel_br("EventInfo_generatorWeight_NOSYS",
                &m_EventInfo_generatorWeight_NOSYS);
  add_presel_br("EventInfo_PileupWeight_NOSYS",
                &m_EventInfo_PileupWeight_NOSYS);
  add_presel_br("EventInfo_prodFracWeight_NOSYS",
                &m_EventInfo_prodFracWeight_NOSYS);
  add_presel_br("EventInfo_eventNumber", &m_EventInfo_eventNumber);

  // sys branches
  if (m_do_systematics) {
    // event weights
    init_sys_br(&m_sys_br_GEN, "EventInfo_generatorWeight_%s");
    init_sys_br(&m_sys_br_PROD_FRAC, "EventInfo_prodFracWeight_%s");
  }

  // output tree
  if (m_save_output_tree) {
    connect_output_branches();
  }
}

void WDTruthLoop::connect_output_branches() {
  get_output_tree()->Branch("truth_Dmeson_m", &m_out_truth_Dmeson_m);
  get_output_tree()->Branch("truth_Dmeson_pt", &m_out_truth_Dmeson_pt);
  get_output_tree()->Branch("truth_Dmeson_eta", &m_out_truth_Dmeson_eta);
  get_output_tree()->Branch("truth_Dmeson_phi", &m_out_truth_Dmeson_phi);
  get_output_tree()->Branch("truth_Dmeson_pdgId", &m_out_truth_Dmeson_pdgId);
  get_output_tree()->Branch("truth_Dmeson_track_jet_6_pt",
                            &m_out_truth_Dmeson_track_jet_6_pt);
  get_output_tree()->Branch("truth_Dmeson_track_jet_6_eta",
                            &m_out_truth_Dmeson_track_jet_6_eta);
  get_output_tree()->Branch("truth_Dmeson_track_jet_6_phi",
                            &m_out_truth_Dmeson_track_jet_6_phi);
  get_output_tree()->Branch("truth_Dmeson_track_jet_6_zt",
                            &m_out_truth_Dmeson_track_jet_6_zt);
  get_output_tree()->Branch("truth_Dmeson_track_jet_6_zl",
                            &m_out_truth_Dmeson_track_jet_6_zl);
  get_output_tree()->Branch("truth_Dmeson_track_jet_8_pt",
                            &m_out_truth_Dmeson_track_jet_8_pt);
  get_output_tree()->Branch("truth_Dmeson_track_jet_8_eta",
                            &m_out_truth_Dmeson_track_jet_8_eta);
  get_output_tree()->Branch("truth_Dmeson_track_jet_8_phi",
                            &m_out_truth_Dmeson_track_jet_8_phi);
  get_output_tree()->Branch("truth_Dmeson_track_jet_8_zt",
                            &m_out_truth_Dmeson_track_jet_8_zt);
  get_output_tree()->Branch("truth_Dmeson_track_jet_8_zl",
                            &m_out_truth_Dmeson_track_jet_8_zl);
  get_output_tree()->Branch("truth_Dmeson_track_jet_10_pt",
                            &m_out_truth_Dmeson_track_jet_10_pt);
  get_output_tree()->Branch("truth_Dmeson_track_jet_10_eta",
                            &m_out_truth_Dmeson_track_jet_10_eta);
  get_output_tree()->Branch("truth_Dmeson_track_jet_10_phi",
                            &m_out_truth_Dmeson_track_jet_10_phi);
  get_output_tree()->Branch("truth_Dmeson_track_jet_10_zt",
                            &m_out_truth_Dmeson_track_jet_10_zt);
  get_output_tree()->Branch("truth_Dmeson_track_jet_10_zl",
                            &m_out_truth_Dmeson_track_jet_10_zl);
}

void WDTruthLoop::fill_truth_branches(int truthIndex) {
  m_out_truth_Dmeson_m = 0;
  m_out_truth_Dmeson_pt = 0;
  m_out_truth_Dmeson_eta = 999;
  m_out_truth_Dmeson_phi = 999;
  m_out_truth_Dmeson_pdgId = 0;
  if (truthIndex >= 0) {
    m_out_truth_Dmeson_m = truth_daughter_mass(truthIndex) * Charm::GeV;
    m_out_truth_Dmeson_pt =
        m_TruthParticles_Selected_pt->at(truthIndex) * Charm::GeV;
    m_out_truth_Dmeson_eta = m_TruthParticles_Selected_eta->at(truthIndex);
    m_out_truth_Dmeson_phi = m_TruthParticles_Selected_phi->at(truthIndex);
    m_out_truth_Dmeson_pdgId = m_TruthParticles_Selected_pdgId->at(truthIndex);
  }
}

void WDTruthLoop::configure() {
  // set configs
  std::cout << get_config() << std::endl;

  // systematics
  m_do_systematics = get_config()["do_systematics"].as<bool>(false);

  // fiducial MET cuts
  m_do_fiducial_met = get_config()["do_fiducial_met"].as<bool>(false);

  // charm production fraction reweight from the ntuple
  m_do_prod_fraction_rw = get_config()["do_prod_fraction_rw"].as<bool>(true);

  // charm production fraction reweight implemented in charmpp
  m_do_prod_fraction_rw_old =
      get_config()["do_prod_fraction_rw_old"].as<bool>(false);

  // reweight wjets samples
  m_do_wjets_rw = get_config()["do_wjets_rw"].as<bool>(true);

  // use track-jets
  m_track_jet_selection = get_config()["track_jet_selection"].as<bool>(true);

  // whether to split c or b flavor
  m_split_flavor = get_config()["eval_truth_flavor"].as<bool>(false);

  // reweight sherpa 2.2.11 samples (09/2023 not recommend except for Diboson)
  m_reweight_sherpa = get_config()["reweight_sherpa"].as<bool>(false);

  // Save output tree
  m_save_output_tree = get_config()["save_output_tree"].as<bool>(false);
}

WDTruthLoop::WDTruthLoop(TString input_file, TString out_path,
                         TString tree_name)
    : EventLoopBase(input_file, out_path, tree_name), TruthInfo() {}

}  // namespace Charm
