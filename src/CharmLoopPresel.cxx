#include <math.h>

#include <iostream>

#include "CharmLoopBase.h"
#include "HelperFunctions.h"
#include "TVector2.h"

namespace Charm {
bool CharmLoopBase::preselection() {
  set_channel("inclusive");

  // skip if not ordered to do lepton preselection
  if (!m_do_lep_presel) {
    std::string prepend = "inclusive";
    m_channel_sys = std::vector<std::string>(m_nSyst, prepend);
    DMesonRec::set_lepton(nullptr);
    m_lep_charge = 0;
    if (m_data_period == "ForcedDecay") {
      // mc event weight, initial sum of weights, x-sec, lumi weight
      multiply_mc_event_weight(m_EventInfo_generatorWeight_NOSYS, &m_sys_br_GEN,
                               true, false, false);
    }
    return true;
  }

  // other weights
  if (m_is_mc && !m_no_weights) {
    // mc event weight, initial sum of weights, x-sec, lumi weight
    multiply_mc_event_weight(
        m_EventInfo_generatorWeight_NOSYS, &m_sys_br_GEN, m_apply_lumi, false,
        (m_mc_shower == "sherpa2211" && m_reweight_sherpa));
    // jvt weight and systematics
    multiply_nominal_and_sys_weight(m_EventInfo_jvt_effSF_NOSYS, &m_sys_br_JVT);
    // pileup weight and systematics
    multiply_nominal_and_sys_weight(m_EventInfo_PileupWeight_NOSYS,
                                    &m_sys_br_PU);
    // ttbar weight (TODO: add systematics)
    // multiply_nominal_and_sys_weight(m_CharmEventInfo_TopWeight, &m_sys_br_TOP);
    m_out_top_weight = m_CharmEventInfo_TopWeight;
    // production fraction weight
    if (m_do_prod_fraction_rw) {
      multiply_nominal_and_sys_weight(m_EventInfo_prodFracWeight_NOSYS,
                                      &m_sys_br_PROD_FRAC);
    }
    // FTAG systematics
    if (m_is_mc && m_jet_selection) {
      // multiply_nominal_and_sys_weight(m_EventInfo_ftag_effSF_DL1r_FixedCutBEff_70_NOSYS,
      // &m_sys_br_FTAG);
    }
  }

  // sys branches for leptons
  read_sys_br();

  // read MET
  m_sys_met = get_val_sys(m_METInfo_MET_NOSYS, &m_sys_br_MET_met, Charm::GeV);
  m_sys_met_phi = get_val_sys(m_METInfo_METPhi_NOSYS, &m_sys_br_MET_phi);

  // require exactly one loose lepton
  get_loose_electrons();
  get_loose_muons();

  // require exactly one tight lepton
  get_tight_electrons();
  get_tight_muons();

  // clear all before running over systematics
  m_sys_anti_tight = std::vector<bool>(m_nSyst, 0.);
  m_sys_met_dphi = std::vector<float>(m_nSyst, 0.);
  m_sys_met_mt = std::vector<float>(m_nSyst, 0.);
  m_sys_lep_pt = std::vector<float>(m_nSyst, 0.);
  m_sys_lep_pt_plusTopoConeET20 = std::vector<float>(m_nSyst, 0.);
  m_sys_lep_eta = std::vector<float>(m_nSyst, 0.);
  m_sys_lep_abs_eta = std::vector<float>(m_nSyst, 0.);
  m_sys_lep_calo_eta = std::vector<float>(m_nSyst, 0.);
  m_sys_lep_phi = std::vector<float>(m_nSyst, 0.);
  m_sys_lep_charge = std::vector<float>(m_nSyst, 0.);
  m_sys_nbjets = std::vector<float>(m_nSyst, 0.);
  m_sys_met_AT = std::vector<float>(m_nSyst, 0.);
  m_sys_met_perp = std::vector<float>(m_nSyst, 0.);
  m_sys_met_para = std::vector<float>(m_nSyst, 0.);

  for (unsigned int i = 0; i < m_nSyst; i++) {
    bool nominal = i == 0;
    if (!m_do_systematics && !nominal) {
      continue;
    }

    unsigned int sys_index = i - 1;
    std::vector<std::pair<unsigned int, float>> electrons, muons;
    electrons = m_electrons_sys.at(i);
    muons = m_muons_sys.at(i);
    auto loose_electrons = m_loose_electrons_sys.at(i);
    auto loose_muons = m_loose_muons_sys.at(i);
    TLorentzVector lep;
    std::string channel_string = "";

    // bad muon veto
    for (unsigned int j = 0; j < loose_muons.size(); j++) {
      if (m_AnalysisMuons_is_bad->at(loose_muons.at(j).first)) {
        continue;
      }
    }

    // exactly one loose lepton
    if ((loose_electrons.size() + loose_muons.size()) != 1) {
      continue;
    }

    // check if anti-tight region
    if ((electrons.size() + muons.size()) != 1) {
      m_sys_anti_tight.at(i) = true;
      if (!(m_fake_rate_measurement || m_fake_factor_method ||
            (!m_is_mc && m_eval_fake_rate))) {
        continue;
      }
    }

    // electron channel
    if (loose_electrons.size() == 1) {
      // electron index
      unsigned int index = loose_electrons.at(0).first;

      // channel
      channel_string = "el";

      // pass electron trigger
      if (!pass_el_trigger()) {
        continue;
      }

      // single electron trigger
      if (!el_is_trigger_matched(index,
                                 m_sys_el_pt.at(i)->at(index) * Charm::GeV)) {
        continue;
      }

      // fill variables
      lep.SetPtEtaPhiM(m_sys_el_pt.at(i)->at(index) * Charm::GeV,
                       m_AnalysisElectrons_eta->at(index),
                       m_AnalysisElectrons_phi->at(index),
                       Charm::ELECTRON_MASS * Charm::GeV);
      if (nominal) {
        m_lep = lep;
      }
      m_sys_lep_pt.at(i) = m_sys_el_pt.at(i)->at(index) * Charm::GeV;
      if (m_do_el_topocone_param) {
        m_sys_lep_pt_plusTopoConeET20.at(i) =
            m_sys_lep_pt.at(i) +
            m_AnalysisElectrons_topoetcone20->at(index) * Charm::GeV;
      }
      m_sys_lep_eta.at(i) = m_AnalysisElectrons_eta->at(index);
      m_sys_lep_abs_eta.at(i) = std::abs(m_AnalysisElectrons_eta->at(index));
      m_sys_lep_calo_eta.at(i) = m_AnalysisElectrons_caloCluster_eta->at(index);
      m_sys_lep_phi.at(i) = m_AnalysisElectrons_phi->at(index);
      m_sys_lep_charge.at(i) = m_AnalysisElectrons_charge->at(index);
      if (nominal) {
        m_d0 = m_AnalysisElectrons_d0->at(index);
        m_d0sig = m_AnalysisElectrons_d0sig->at(index);
        if (m_do_extra_histograms) {
          m_lep_is_isolated =
              m_AnalysisElectrons_isIsolated_Tight_VarRad_NOSYS->at(index);
          m_z0sinTheta = m_AnalysisElectrons_z0sinTheta->at(index);
          m_topoetcone20 =
              m_AnalysisElectrons_topoetcone20->at(index) * Charm::GeV;
          m_ptvarcone20_TightTTVA_pt1000 =
              m_AnalysisElectrons_ptvarcone20_TightTTVA_pt1000->at(index) *
              Charm::GeV;
          m_ptvarcone30_TightTTVA_pt1000 =
              m_AnalysisElectrons_ptvarcone30_TightTTVA_pt1000->at(index) *
              Charm::GeV;
          m_topoetcone20_over_pt = m_topoetcone20 / lep.Pt();
          m_ptvarcone20_TightTTVA_pt1000_over_pt =
              m_ptvarcone20_TightTTVA_pt1000 / lep.Pt();
          m_ptvarcone30_TightTTVA_pt1000_over_pt =
              m_ptvarcone30_TightTTVA_pt1000 / lep.Pt();
          m_newflowisol = -1.0;
          // m_EOverP = m_AnalysisElectrons_EOverP->at(index);
        }
      }

      // efficiency weight
      if (m_is_mc) {
        if (!m_sys_anti_tight.at(i)) {
          multiply_event_weight(m_electrons_sys.at(i).at(0).second, i);
        } else {
          multiply_event_weight(m_loose_electrons_sys.at(i).at(0).second, i);
        }
      }
    } else if (loose_muons.size() == 1) {
      // muon index
      unsigned int index = loose_muons.at(0).first;

      // channel
      channel_string = "mu";

      // pass muon trigger
      if (!pass_mu_trigger()) {
        continue;
      }

      // single muon trigger
      if (!mu_is_trigger_matched(index,
                                 m_sys_mu_pt.at(i)->at(index) * Charm::GeV)) {
        continue;
      }

      // fill variables
      lep.SetPtEtaPhiM(m_sys_mu_pt.at(i)->at(index) * Charm::GeV,
                       m_AnalysisMuons_eta->at(index),
                       m_AnalysisMuons_phi->at(index),
                       Charm::MUON_MASS * Charm::GeV);
      if (nominal) {
        m_lep = lep;
      }
      m_sys_lep_pt.at(i) = m_sys_mu_pt.at(i)->at(index) * Charm::GeV;
      if (m_do_el_topocone_param) {
        m_sys_lep_pt_plusTopoConeET20.at(i) =
            m_sys_lep_pt.at(i) +
            m_AnalysisMuons_topoetcone20->at(index) * Charm::GeV;
      }
      m_sys_lep_eta.at(i) = m_AnalysisMuons_eta->at(index);
      m_sys_lep_abs_eta.at(i) = std::abs(m_AnalysisMuons_eta->at(index));
      m_sys_lep_calo_eta.at(i) = m_AnalysisMuons_eta->at(index);
      m_sys_lep_phi.at(i) = m_AnalysisMuons_phi->at(index);
      m_sys_lep_charge.at(i) = m_AnalysisMuons_charge->at(index);
      if (nominal) {
        m_d0 = m_AnalysisMuons_d0->at(index);
        m_d0sig = m_AnalysisMuons_d0sig->at(index);
        if (m_do_extra_histograms) {
          m_lep_is_isolated =
              m_AnalysisMuons_isIsolated_PflowTight_VarRad_NOSYS->at(index);
          m_z0sinTheta = m_AnalysisMuons_z0sinTheta->at(index);
          m_topoetcone20 = m_AnalysisMuons_topoetcone20->at(index) * Charm::GeV;
          m_ptvarcone30_TightTTVA_pt1000 =
              m_AnalysisMuons_ptvarcone30_TightTTVA_pt1000->at(index) *
              Charm::GeV;
          m_topoetcone20_over_pt = m_topoetcone20 / lep.Pt();
          m_ptvarcone30_TightTTVA_pt1000_over_pt =
              m_ptvarcone30_TightTTVA_pt1000 / lep.Pt();
          m_ptvarcone20_TightTTVA_pt1000_over_pt = -1.0;
          m_ptvarcone30_TightTTVA_pt1000_over_pt = -1.0;
          m_newflowisol =
              (m_AnalysisMuons_ptvarcone30_TightTTVA_pt500->at(index) +
               0.4 * m_AnalysisMuons_neflowisol20->at(index)) *
              Charm::GeV / lep.Pt();
        }
      }
      // efficiency weight
      if (m_is_mc) {
        if (!m_sys_anti_tight.at(i)) {
          multiply_event_weight(m_muons_sys.at(i).at(0).second, i);
        } else {
          multiply_event_weight(m_loose_muons_sys.at(i).at(0).second, i);
        }
      }
    }

    // set channel
    if (channel_string != "") {
      // base name
      m_channel_sys.at(i) = channel_string;

      // period string
      if (m_split_by_period) {
        m_channel_sys.at(i) = get_period_string() + "_" + m_channel_sys.at(i);
      }

      // lepton charge
      if (m_split_by_charge) {
        m_channel_sys.at(i) = m_channel_sys.at(i) + "_" +
                              get_lepton_charge_string(m_sys_lep_charge.at(i));
      }
    } else {
      continue;
    }

    // MET for antiTight is set to a special definition w/o systematic
    // uncertainty (as it originates from data)
    if (m_use_anti_tight_met) {
      if (m_sys_anti_tight.at(i)) {
        m_sys_met_AT.at(i) = m_METInfoAntiTight_NOSYS_MET * Charm::GeV;
        if (loose_muons.size() == 1) {
          TVector2 met_tmp, mu_tmp;
          double tmp_lep_pt = m_sys_lep_pt.at(i);
          met_tmp.SetMagPhi(m_METInfoAntiTight_NOSYS_MET * Charm::GeV,
                            m_METInfoAntiTight_NOSYS_METPhi);
          mu_tmp.SetMagPhi(tmp_lep_pt, m_sys_lep_phi.at(i));
          m_sys_met.at(i) = (met_tmp - mu_tmp).Mod();
          m_sys_met_phi.at(i) =
              TVector2::Phi_mpi_pi(met_tmp.Phi() - mu_tmp.Phi());
        }
      }
    }

    // additional MET variables
    m_sys_met_dphi.at(i) =
        TVector2::Phi_mpi_pi(m_sys_met_phi.at(i) - lep.Phi());
    m_sys_met_mt.at(i) = sqrt(2 * lep.Pt() * (m_sys_met.at(i)) *
                              (1 - cos(m_sys_met_dphi.at(i))));
    m_sys_met_para.at(i) = m_sys_met.at(i) * cos(m_sys_met_dphi.at(i));
    m_sys_met_perp.at(i) = m_sys_met.at(i) * sin(m_sys_met_dphi.at(i));
  }

  // truth selection
  if (m_is_mc) {
    if (m_fiducial_selection || m_save_truth_wjets) {
      get_truth_wjets(m_sample_type);
    }
    if (m_do_pt_v_rw) {
      if (m_mc_gen == "powheg" || m_mc_gen == "sherpa") {
        m_event_weight *= calculate_ptv_weight();
      }
    }
  }

  // accept event
  return true;
}

}  // namespace Charm
