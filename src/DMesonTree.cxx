#include "DMesonRec.h"

namespace Charm {

void DMesonRec::fill_D_plus_tree(unsigned int index, bool extended_branches, bool is_mc) {
  m_out_Dmeson_m = m_DMesons_m->at(index) * Charm::GeV;
  m_out_Dmeson_pt = m_DMesons_pt->at(index) * Charm::GeV;
  m_out_Dmeson_eta = m_DMesons_eta->at(index);
  m_out_Dmeson_phi = m_DMesons_phi->at(index);
  if (extended_branches) {
    m_out_Dmeson_Chi2 = m_DMesons_fitOutput__Chi2->at(index);
    m_out_Dmeson_Impact = m_DMesons_fitOutput__Impact->at(index);
    m_out_Dmeson_ImpactZ0SinTheta =
        m_DMesons_fitOutput__ImpactZ0SinTheta->at(index);
    m_out_Dmeson_ImpactSignificance =
        m_DMesons_fitOutput__ImpactSignificance->at(index);
    m_out_Dmeson_Lxy = m_DMesons_fitOutput__Lxy->at(index);
    m_out_Dmeson_costhetastar = m_DMesons_costhetastar->at(index);
    m_out_Dmeson_maxdR = D_meson_max_track_dR(index);
    m_out_Dmeson_mKpi1 = m_DMesons_mKpi1->at(index) * Charm::GeV;
    m_out_Dmeson_mKpi2 = m_DMesons_mKpi2->at(index) * Charm::GeV;
    m_out_Dmeson_ptcone40 = m_DMesons_ptcone40->at(index) * Charm::GeV;
    for (int i = 0; i < 3; i++) {
      m_out_Dmeson_trk_pt[i] =
          m_DMesons_daughterInfo__pt->at(index).at(i) * Charm::GeV;
      m_out_Dmeson_trk_eta[i] = m_DMesons_daughterInfo__eta->at(index).at(i);
      m_out_Dmeson_trk_phi[i] = m_DMesons_daughterInfo__phi->at(index).at(i);
      m_out_Dmeson_trk_z0SinThetaPV[i] =
          m_DMesons_daughterInfo__z0SinThetaPV->at(index).at(i);
      m_out_Dmeson_trk_id[i] = m_DMesons_daughterInfo__trackId->at(index).at(i);
    }

    // D* branches
    int D0Index = m_DMesons_D0Index->at(index);
    if (D0Index >= 0) {
      m_out_Dmeson_soft_pion_dr = soft_pion_Dstar_separation(index, D0Index);
      m_out_Dmeson_soft_pion_d0 = m_DMesons_SlowPionD0->at(index);
      m_out_Dmeson_mdiff = m_DMesons_DeltaMass->at(index);
      m_out_Dmeson_mdzero = m_DMesons_m->at(D0Index);
    } else {
      m_out_Dmeson_soft_pion_dr = -999;
      m_out_Dmeson_soft_pion_d0 = -999;
      m_out_Dmeson_mdiff = -999;
      m_out_Dmeson_mdzero = -999;
    }
  }
}

void DMesonRec::fill_track_jet_tree(SysTrackJet *sys_track_jet,
                                    std::string track_jet_radius) {
  m_out_Dmeson_track_jet_pt[track_jet_radius] =
      sys_track_jet->track_jet_pt.at(0);
  m_out_Dmeson_track_jet_eta[track_jet_radius] =
      sys_track_jet->track_jet_eta.at(0);
  m_out_Dmeson_track_jet_phi[track_jet_radius] =
      sys_track_jet->track_jet_phi.at(0);
  m_out_Dmeson_track_jet_zt[track_jet_radius] =
      sys_track_jet->track_jet_zt.at(0);
  m_out_Dmeson_track_jet_zl[track_jet_radius] =
      sys_track_jet->track_jet_zl.at(0);
  m_out_truth_Dmeson_track_jet_pt[track_jet_radius] =
      sys_track_jet->truth_track_jet_pt.at(0);
  m_out_truth_Dmeson_track_jet_eta[track_jet_radius] =
      sys_track_jet->truth_track_jet_eta.at(0);
  m_out_truth_Dmeson_track_jet_phi[track_jet_radius] =
      sys_track_jet->truth_track_jet_phi.at(0);
  m_out_truth_Dmeson_track_jet_zt[track_jet_radius] =
      sys_track_jet->truth_track_jet_zt.at(0);
  m_out_truth_Dmeson_track_jet_zl[track_jet_radius] =
      sys_track_jet->truth_track_jet_zl.at(0);
}

void DMesonRec::connect_output_tree_branches(EventLoopBase *elb, bool is_mc,
                                             bool track_jet_selection,
                                             bool extended_branches,
                                             std::string track_jet_radius) {
  elb->get_output_tree()->Branch("Dmeson_m", &m_out_Dmeson_m);
  elb->get_output_tree()->Branch("Dmeson_mdiff", &m_out_Dmeson_mdiff);
  elb->get_output_tree()->Branch("Dmeson_mdzero", &m_out_Dmeson_mdzero);
  elb->get_output_tree()->Branch("Dmeson_pt", &m_out_Dmeson_pt);
  elb->get_output_tree()->Branch("Dmeson_eta", &m_out_Dmeson_eta);
  elb->get_output_tree()->Branch("Dmeson_phi", &m_out_Dmeson_phi);
  if (is_mc) {
    elb->get_output_tree()->Branch("truth_Dmeson_m", &m_out_truth_Dmeson_m);
    elb->get_output_tree()->Branch("truth_Dmeson_pt", &m_out_truth_Dmeson_pt);
    elb->get_output_tree()->Branch("truth_Dmeson_eta", &m_out_truth_Dmeson_eta);
    elb->get_output_tree()->Branch("truth_Dmeson_phi", &m_out_truth_Dmeson_phi);
    elb->get_output_tree()->Branch("truth_Dmeson_pdgId", &m_out_truth_Dmeson_pdgId);
  }
  if (track_jet_selection) {
    elb->get_output_tree()->Branch(
        Form("Dmeson_track_jet_%s_pt", track_jet_radius.c_str()),
        &m_out_Dmeson_track_jet_pt[track_jet_radius]);
    elb->get_output_tree()->Branch(
        Form("Dmeson_track_jet_%s_eta", track_jet_radius.c_str()),
        &m_out_Dmeson_track_jet_eta[track_jet_radius]);
    elb->get_output_tree()->Branch(
        Form("Dmeson_track_jet_%s_phi", track_jet_radius.c_str()),
        &m_out_Dmeson_track_jet_phi[track_jet_radius]);
    elb->get_output_tree()->Branch(
        Form("Dmeson_track_jet_%s_zt", track_jet_radius.c_str()),
        &m_out_Dmeson_track_jet_zt[track_jet_radius]);
    elb->get_output_tree()->Branch(
        Form("Dmeson_track_jet_%s_zl", track_jet_radius.c_str()),
        &m_out_Dmeson_track_jet_zl[track_jet_radius]);
    elb->get_output_tree()->Branch(
        Form("truth_Dmeson_track_jet_%s_pt", track_jet_radius.c_str()),
        &m_out_truth_Dmeson_track_jet_pt[track_jet_radius]);
    elb->get_output_tree()->Branch(
        Form("truth_Dmeson_track_jet_%s_eta", track_jet_radius.c_str()),
        &m_out_truth_Dmeson_track_jet_eta[track_jet_radius]);
    elb->get_output_tree()->Branch(
        Form("truth_Dmeson_track_jet_%s_phi", track_jet_radius.c_str()),
        &m_out_truth_Dmeson_track_jet_phi[track_jet_radius]);
    elb->get_output_tree()->Branch(
        Form("truth_Dmeson_track_jet_%s_zt", track_jet_radius.c_str()),
        &m_out_truth_Dmeson_track_jet_zt[track_jet_radius]);
    elb->get_output_tree()->Branch(
        Form("truth_Dmeson_track_jet_%s_zl", track_jet_radius.c_str()),
        &m_out_truth_Dmeson_track_jet_zl[track_jet_radius]);
  }
  if (extended_branches) {
    // SV variables
    elb->get_output_tree()->Branch("Dmeson_Chi2", &m_out_Dmeson_Chi2);
    elb->get_output_tree()->Branch("Dmeson_Impact", &m_out_Dmeson_Impact);
    elb->get_output_tree()->Branch("Dmeson_z0SinTheta",
                                   &m_out_Dmeson_ImpactZ0SinTheta);
    elb->get_output_tree()->Branch("Dmeson_ImpactSig",
                                   &m_out_Dmeson_ImpactSignificance);
    elb->get_output_tree()->Branch("Dmeson_Lxy", &m_out_Dmeson_Lxy);
    elb->get_output_tree()->Branch("Dmeson_costhetastar",
                                   &m_out_Dmeson_costhetastar);
    elb->get_output_tree()->Branch("Dmeson_maxdR", &m_out_Dmeson_maxdR);
    elb->get_output_tree()->Branch("Dmeson_mKpi1", &m_out_Dmeson_mKpi1);
    elb->get_output_tree()->Branch("Dmeson_mKpi2", &m_out_Dmeson_mKpi2);
    elb->get_output_tree()->Branch("Dmeson_ptcone40", &m_out_Dmeson_ptcone40);

    // individual tracks
    elb->get_output_tree()->Branch("Dmeson_trk1_pt", &m_out_Dmeson_trk_pt[0]);
    elb->get_output_tree()->Branch("Dmeson_trk1_eta", &m_out_Dmeson_trk_eta[0]);
    elb->get_output_tree()->Branch("Dmeson_trk1_phi", &m_out_Dmeson_trk_phi[0]);
    elb->get_output_tree()->Branch("Dmeson_trk1_z0SinThetaPV",
                                   &m_out_Dmeson_trk_z0SinThetaPV[0]);
    elb->get_output_tree()->Branch("Dmeson_trk1_id", &m_out_Dmeson_trk_id[0]);
    elb->get_output_tree()->Branch("Dmeson_trk2_pt", &m_out_Dmeson_trk_pt[1]);
    elb->get_output_tree()->Branch("Dmeson_trk2_eta", &m_out_Dmeson_trk_eta[1]);
    elb->get_output_tree()->Branch("Dmeson_trk2_phi", &m_out_Dmeson_trk_phi[1]);
    elb->get_output_tree()->Branch("Dmeson_trk2_z0SinThetaPV",
                                   &m_out_Dmeson_trk_z0SinThetaPV[1]);
    elb->get_output_tree()->Branch("Dmeson_trk2_id", &m_out_Dmeson_trk_id[1]);
    elb->get_output_tree()->Branch("Dmeson_trk3_pt", &m_out_Dmeson_trk_pt[2]);
    elb->get_output_tree()->Branch("Dmeson_trk3_eta", &m_out_Dmeson_trk_eta[2]);
    elb->get_output_tree()->Branch("Dmeson_trk3_phi", &m_out_Dmeson_trk_phi[2]);
    elb->get_output_tree()->Branch("Dmeson_trk3_z0SinThetaPV",
                                   &m_out_Dmeson_trk_z0SinThetaPV[2]);
    elb->get_output_tree()->Branch("Dmeson_trk3_id", &m_out_Dmeson_trk_id[2]);

    // D* branches
    elb->get_output_tree()->Branch("Dmeson_soft_pion_dr",
                                   &m_out_Dmeson_soft_pion_dr);
    elb->get_output_tree()->Branch("Dmeson_soft_pion_d0",
                                   &m_out_Dmeson_soft_pion_d0);
  }
}

}  // namespace Charm
