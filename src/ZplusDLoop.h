#ifndef Z_PLUS_CHARM_H
#define Z_PLUS_CHARM_H

#include "ZCharmLoopBase.h"

namespace Charm {
class ZplusDLoop : public ZCharmLoopBase {
 public:
  ZplusDLoop(TString input_file, TString out_path,
             TString tree_name = "CharmAnalysis");

 protected:
  virtual void connect_branches() override;

 private:
  virtual int execute() override;
};

}  // namespace Charm

#endif  // Z_PLUS_CHARM_H
