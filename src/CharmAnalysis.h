//////////////////////////////////////////////////////////
// This class has been automatically generated on
// Mon Aug 28 21:26:50 2023 by ROOT version 6.28/04
// from TTree CharmAnalysis/xAOD->NTuple tree
// found on file: Sherpa_WplusD.MC16e.700367.0.root
//////////////////////////////////////////////////////////

#ifndef CharmAnalysis_h
#define CharmAnalysis_h

#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>

class CharmAnalysis {
public :
   TTree          *fChain;   //!pointer to the analyzed TTree or TChain
   Int_t           fCurrent; //!current Tree number in a TChain

// Fixed size dimensions of array or collections stored in the TTree if any.

   // Declaration of leaf types
   ULong64_t       EventInfo_eventNumber;
   UInt_t          EventInfo_RandomRunNumber;
   Float_t         EventInfo_PileupWeight_NOSYS;
   Float_t         EventInfo_correctedScaled_averageInteractionsPerCrossing;
   Float_t         EventInfo_correctedScaled_actualInteractionsPerCrossing;
   Float_t         EventInfo_generatorWeight_NOSYS;
   Float_t         EventInfo_prodFracWeight_NOSYS;
   Bool_t          EventInfo_trigPassed_HLT_e24_lhmedium_L1EM20VH;
   Bool_t          EventInfo_trigPassed_HLT_e60_lhmedium;
   Bool_t          EventInfo_trigPassed_HLT_e120_lhloose;
   Bool_t          EventInfo_trigPassed_HLT_e26_lhtight_nod0_ivarloose;
   Bool_t          EventInfo_trigPassed_HLT_e60_lhmedium_nod0;
   Bool_t          EventInfo_trigPassed_HLT_e140_lhloose_nod0;
   Bool_t          EventInfo_trigPassed_HLT_mu20_iloose_L1MU15;
   Bool_t          EventInfo_trigPassed_HLT_mu26_ivarmedium;
   Bool_t          EventInfo_trigPassed_HLT_mu50;
   Float_t         EventInfo_jvt_effSF_NOSYS;
   Float_t         CharmEventInfo_TopWeight;
   std::vector<float>   *AnalysisElectrons_pt_NOSYS;
   std::vector<char>    *AnalysisElectrons_el_selected_NOSYS;
   std::vector<float>   *AnalysisElectrons_eta;
   std::vector<float>   *AnalysisElectrons_caloCluster_eta;
   std::vector<float>   *AnalysisElectrons_phi;
   std::vector<float>   *AnalysisElectrons_charge;
   std::vector<float>   *AnalysisElectrons_d0sig;
   std::vector<float>   *AnalysisElectrons_d0;
   std::vector<float>   *AnalysisElectrons_z0sinTheta;
   std::vector<float>   *AnalysisElectrons_topoetcone20;
   std::vector<float>   *AnalysisElectrons_ptvarcone20_TightTTVA_pt1000;
   std::vector<float>   *AnalysisElectrons_ptvarcone30_TightTTVA_pt1000;
   std::vector<char>    *AnalysisElectrons_likelihood_Tight;
   std::vector<char>    *AnalysisElectrons_isIsolated_Tight_VarRad_NOSYS;
   std::vector<float>   *AnalysisElectrons_effSF_NOSYS;
   std::vector<float>   *AnalysisElectrons_effSF_ID_Tight_NOSYS;
   std::vector<float>   *AnalysisElectrons_effSF_Chflip_Tight_Tight_VarRad_NOSYS;
   std::vector<float>   *AnalysisElectrons_effSF_Isol_Tight_Tight_VarRad_NOSYS;
   std::vector<float>   *AnalysisElectrons_effSF_Trig_Tight_Tight_VarRad_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_NOSYS;
   std::vector<float>   *AnalysisElectrons_effSF_Trig_Tight_Tight_VarRad_Eff_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_NOSYS;
   std::vector<float>   *AnalysisElectrons_effSF_Chflip_Tight_noIso_NOSYS;
   std::vector<float>   *AnalysisElectrons_effSF_Trig_Tight_noIso_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_NOSYS;
   std::vector<float>   *AnalysisElectrons_effSF_Trig_Tight_noIso_Eff_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_NOSYS;
   std::vector<char>    *AnalysisElectrons_matched_HLT_e24_lhmedium_L1EM20VH;
   std::vector<char>    *AnalysisElectrons_matched_HLT_e60_lhmedium;
   std::vector<char>    *AnalysisElectrons_matched_HLT_e120_lhloose;
   std::vector<char>    *AnalysisElectrons_matched_HLT_e26_lhtight_nod0_ivarloose;
   std::vector<char>    *AnalysisElectrons_matched_HLT_e60_lhmedium_nod0;
   std::vector<char>    *AnalysisElectrons_matched_HLT_e140_lhloose_nod0;
   std::vector<int>     *AnalysisElectrons_truthType;
   std::vector<int>     *AnalysisElectrons_truthOrigin;
   std::vector<int>     *AnalysisElectrons_firstEgMotherPdgId;
   std::vector<int>     *AnalysisElectrons_firstEgMotherTruthType;
   std::vector<int>     *AnalysisElectrons_firstEgMotherTruthOrigin;
   std::vector<float>   *AnalysisMuons_pt_NOSYS;
   std::vector<char>    *AnalysisMuons_mu_selected_NOSYS;
   std::vector<char>    *AnalysisMuons_is_bad;
   std::vector<float>   *AnalysisMuons_eta;
   std::vector<float>   *AnalysisMuons_phi;
   std::vector<float>   *AnalysisMuons_charge;
   std::vector<float>   *AnalysisMuons_d0sig;
   std::vector<float>   *AnalysisMuons_d0;
   std::vector<float>   *AnalysisMuons_z0sinTheta;
   std::vector<float>   *AnalysisMuons_topoetcone20;
   std::vector<float>   *AnalysisMuons_ptvarcone30_TightTTVA_pt1000;
   std::vector<float>   *AnalysisMuons_ptvarcone30_TightTTVA_pt500;
   std::vector<float>   *AnalysisMuons_neflowisol20;
   std::vector<char>    *AnalysisMuons_isQuality_Loose_NOSYS;
   std::vector<char>    *AnalysisMuons_isQuality_Medium_NOSYS;
   std::vector<char>    *AnalysisMuons_isQuality_Tight_NOSYS;
   std::vector<char>    *AnalysisMuons_isIsolated_PflowTight_VarRad_NOSYS;
   std::vector<float>   *AnalysisMuons_muon_effSF_NOSYS;
   std::vector<float>   *AnalysisMuons_muon_effSF_TTVA_NOSYS;
   std::vector<float>   *AnalysisMuons_muon_effSF_Quality_Loose_NOSYS;
   std::vector<float>   *AnalysisMuons_muon_effSF_Quality_Medium_NOSYS;
   std::vector<float>   *AnalysisMuons_muon_effSF_Quality_Tight_NOSYS;
   std::vector<float>   *AnalysisMuons_muon_effSF_Isol_PflowTight_VarRad_NOSYS;
   std::vector<float>   *AnalysisMuons_muon_effMC_Trig_Loose_HLT_mu26_ivarmedium_OR_HLT_mu50_NOSYS;
   std::vector<float>   *AnalysisMuons_muon_effData_Trig_Loose_HLT_mu26_ivarmedium_OR_HLT_mu50_NOSYS;
   std::vector<float>   *AnalysisMuons_muon_effMC_Trig_Loose_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_NOSYS;
   std::vector<float>   *AnalysisMuons_muon_effData_Trig_Loose_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_NOSYS;
   std::vector<float>   *AnalysisMuons_muon_effMC_Trig_Medium_HLT_mu26_ivarmedium_OR_HLT_mu50_NOSYS;
   std::vector<float>   *AnalysisMuons_muon_effData_Trig_Medium_HLT_mu26_ivarmedium_OR_HLT_mu50_NOSYS;
   std::vector<float>   *AnalysisMuons_muon_effMC_Trig_Medium_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_NOSYS;
   std::vector<float>   *AnalysisMuons_muon_effData_Trig_Medium_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_NOSYS;
   std::vector<float>   *AnalysisMuons_muon_effMC_Trig_Tight_HLT_mu26_ivarmedium_OR_HLT_mu50_NOSYS;
   std::vector<float>   *AnalysisMuons_muon_effData_Trig_Tight_HLT_mu26_ivarmedium_OR_HLT_mu50_NOSYS;
   std::vector<float>   *AnalysisMuons_muon_effMC_Trig_Tight_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_NOSYS;
   std::vector<float>   *AnalysisMuons_muon_effData_Trig_Tight_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_NOSYS;
   std::vector<char>    *AnalysisMuons_matched_HLT_mu20_iloose_L1MU15;
   std::vector<char>    *AnalysisMuons_matched_HLT_mu26_ivarmedium;
   std::vector<char>    *AnalysisMuons_matched_HLT_mu50;
   std::vector<int>     *AnalysisMuons_truthType;
   std::vector<int>     *AnalysisMuons_truthOrigin;
   std::vector<float>   *AnalysisJets_pt_NOSYS;
   std::vector<char>    *AnalysisJets_jet_selected_NOSYS;
   std::vector<float>   *AnalysisJets_m;
   std::vector<float>   *AnalysisJets_eta;
   std::vector<float>   *AnalysisJets_phi;
   std::vector<char>    *AnalysisJets_ftag_select_DL1r_FixedCutBEff_70;
   std::vector<float>   *AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_NOSYS;
   std::vector<int>     *AnalysisJets_ftag_quantile_DL1r;
   std::vector<float>   *AnalysisJets_ftag_DL1pu_DL1r;
   std::vector<float>   *AnalysisJets_ftag_DL1pb_DL1r;
   std::vector<float>   *AnalysisJets_ftag_DL1pc_DL1r;
   std::vector<int>     *AnalysisJets_HadronConeExclExtendedTruthLabelID;
   std::vector<int>     *AnalysisJets_HadronConeExclTruthLabelID;
   Float_t         METInfo_MET_NOSYS;
   Float_t         METInfo_METPhi_NOSYS;
   std::vector<float>   *DMesons_costhetastar_NOSYS;
   std::vector<int>     *DMesons_D0Index_NOSYS;
   std::vector<std::vector<float> > *DMesons_daughterInfo__eta_NOSYS;
   std::vector<std::vector<bool> > *DMesons_daughterInfo__passTight_NOSYS;
   std::vector<std::vector<int> > *DMesons_daughterInfo__pdgId_NOSYS;
   std::vector<std::vector<float> > *DMesons_daughterInfo__phi_NOSYS;
   std::vector<std::vector<float> > *DMesons_daughterInfo__pt_NOSYS;
   std::vector<std::vector<int> > *DMesons_daughterInfo__trackId_NOSYS;
   std::vector<std::vector<int> > *DMesons_daughterInfo__truthBarcode_NOSYS;
   std::vector<std::vector<int> > *DMesons_daughterInfo__truthDBarcode_NOSYS;
   std::vector<std::vector<float> > *DMesons_daughterInfo__z0SinTheta_NOSYS;
   std::vector<std::vector<float> > *DMesons_daughterInfo__z0SinThetaPV_NOSYS;
   std::vector<int>     *DMesons_decayType_NOSYS;
   std::vector<float>   *DMesons_DeltaMass_NOSYS;
   std::vector<float>   *DMesons_eta_NOSYS;
   std::vector<int>     *DMesons_fitOutput__Charge_NOSYS;
   std::vector<float>   *DMesons_fitOutput__Chi2_NOSYS;
   std::vector<float>   *DMesons_fitOutput__Impact_NOSYS;
   std::vector<float>   *DMesons_fitOutput__ImpactSignificance_NOSYS;
   std::vector<float>   *DMesons_fitOutput__ImpactZ0SinTheta_NOSYS;
   std::vector<float>   *DMesons_fitOutput__Lxy_NOSYS;
   std::vector<std::vector<float> > *DMesons_fitOutput__VertexPosition_NOSYS;
   std::vector<float>   *DMesons_m_NOSYS;
   std::vector<float>   *DMesons_mKpi1_NOSYS;
   std::vector<float>   *DMesons_mKpi2_NOSYS;
   std::vector<float>   *DMesons_mPhi1_NOSYS;
   std::vector<float>   *DMesons_mPhi2_NOSYS;
   std::vector<int>     *DMesons_pdgId_NOSYS;
   std::vector<float>   *DMesons_phi_NOSYS;
   std::vector<float>   *DMesons_pt_NOSYS;
   std::vector<float>   *DMesons_ptcone40_NOSYS;
   std::vector<float>   *DMesons_SlowPionD0_NOSYS;
   std::vector<float>   *DMesons_SlowPionZ0SinTheta_NOSYS;
   std::vector<int>     *DMesons_truthBarcode_NOSYS;
   std::vector<int>     *TruthParticles_Selected_barcode;
   std::vector<float>   *TruthParticles_Selected_cosThetaStarT;
   std::vector<std::vector<int> > *TruthParticles_Selected_daughterInfoT__barcode;
   std::vector<std::vector<float> > *TruthParticles_Selected_daughterInfoT__eta;
   std::vector<std::vector<int> > *TruthParticles_Selected_daughterInfoT__pdgId;
   std::vector<std::vector<float> > *TruthParticles_Selected_daughterInfoT__phi;
   std::vector<std::vector<float> > *TruthParticles_Selected_daughterInfoT__pt;
   std::vector<int>     *TruthParticles_Selected_decayMode;
   std::vector<float>   *TruthParticles_Selected_eta;
   std::vector<char>    *TruthParticles_Selected_fromBdecay;
   std::vector<float>   *TruthParticles_Selected_ImpactT;
   std::vector<float>   *TruthParticles_Selected_LxyT;
   std::vector<float>   *TruthParticles_Selected_m;
   std::vector<int>     *TruthParticles_Selected_pdgId;
   std::vector<float>   *TruthParticles_Selected_phi;
   std::vector<float>   *TruthParticles_Selected_pt;
   std::vector<int>     *TruthParticles_Selected_status;
   std::vector<std::vector<float> > *TruthParticles_Selected_vertexPosition;
   std::vector<int>     *TruthLeptons_barcode;
   std::vector<unsigned int> *TruthLeptons_classifierParticleOrigin;
   std::vector<unsigned int> *TruthLeptons_classifierParticleType;
   std::vector<float>   *TruthLeptons_e;
   std::vector<float>   *TruthLeptons_eta;
   std::vector<float>   *TruthLeptons_m;
   std::vector<int>     *TruthLeptons_pdgId;
   std::vector<float>   *TruthLeptons_phi;
   std::vector<float>   *TruthLeptons_pt;
   std::vector<int>     *TruthLeptons_status;
   std::vector<float>   *TruthLeptons_e_dressed;
   std::vector<float>   *TruthLeptons_eta_dressed;
   std::vector<float>   *TruthLeptons_phi_dressed;
   std::vector<float>   *TruthLeptons_pt_dressed;
   std::vector<int>     *AntiKt4TruthJets_ConeTruthLabelID;
   std::vector<float>   *AntiKt4TruthJets_eta;
   std::vector<int>     *AntiKt4TruthJets_HadronConeExclTruthLabelID;
   std::vector<float>   *AntiKt4TruthJets_m;
   std::vector<float>   *AntiKt4TruthJets_phi;
   std::vector<float>   *AntiKt4TruthJets_pt;
   std::vector<int>     *AntiKt4TruthChargedJets_ConeTruthLabelID;
   std::vector<float>   *AntiKt4TruthChargedJets_eta;
   std::vector<int>     *AntiKt4TruthChargedJets_HadronConeExclTruthLabelID;
   std::vector<float>   *AntiKt4TruthChargedJets_m;
   std::vector<float>   *AntiKt4TruthChargedJets_phi;
   std::vector<float>   *AntiKt4TruthChargedJets_pt;
   std::vector<int>     *AntiKt6TruthChargedJets_ConeTruthLabelID;
   std::vector<float>   *AntiKt6TruthChargedJets_eta;
   std::vector<int>     *AntiKt6TruthChargedJets_HadronConeExclTruthLabelID;
   std::vector<float>   *AntiKt6TruthChargedJets_m;
   std::vector<float>   *AntiKt6TruthChargedJets_phi;
   std::vector<float>   *AntiKt6TruthChargedJets_pt;
   std::vector<int>     *AntiKt8TruthChargedJets_ConeTruthLabelID;
   std::vector<float>   *AntiKt8TruthChargedJets_eta;
   std::vector<int>     *AntiKt8TruthChargedJets_HadronConeExclTruthLabelID;
   std::vector<float>   *AntiKt8TruthChargedJets_m;
   std::vector<float>   *AntiKt8TruthChargedJets_phi;
   std::vector<float>   *AntiKt8TruthChargedJets_pt;
   std::vector<int>     *AntiKt10TruthChargedJets_ConeTruthLabelID;
   std::vector<float>   *AntiKt10TruthChargedJets_eta;
   std::vector<int>     *AntiKt10TruthChargedJets_HadronConeExclTruthLabelID;
   std::vector<float>   *AntiKt10TruthChargedJets_m;
   std::vector<float>   *AntiKt10TruthChargedJets_phi;
   std::vector<float>   *AntiKt10TruthChargedJets_pt;
   std::vector<float>   *MesonTracks_d0sig_NOSYS;
   std::vector<float>   *MesonTracks_d0sigPV_NOSYS;
   std::vector<float>   *MesonTracks_eta_NOSYS;
   std::vector<char>    *MesonTracks_passTight_NOSYS;
   std::vector<float>   *MesonTracks_phi_NOSYS;
   std::vector<float>   *MesonTracks_pt_NOSYS;
   std::vector<int>     *MesonTracks_trackId_NOSYS;
   std::vector<float>   *MesonTracks_z0sinTheta_NOSYS;
   std::vector<float>   *MesonTracks_truthMatchProbability_NOSYS;
   std::vector<float>   *AntiKt6PV0TrackJets_pt_NOSYS;
   std::vector<float>   *AntiKt6PV0TrackJets_eta_NOSYS;
   std::vector<float>   *AntiKt6PV0TrackJets_phi_NOSYS;
   std::vector<float>   *AntiKt6PV0TrackJets_m_NOSYS;
   std::vector<std::vector<float> > *AntiKt6PV0TrackJets_daughter__pt_NOSYS;
   std::vector<std::vector<float> > *AntiKt6PV0TrackJets_daughter__eta_NOSYS;
   std::vector<std::vector<float> > *AntiKt6PV0TrackJets_daughter__phi_NOSYS;
   std::vector<std::vector<int> > *AntiKt6PV0TrackJets_daughter__trackId_NOSYS;
   std::vector<float>   *AntiKt8PV0TrackJets_pt_NOSYS;
   std::vector<float>   *AntiKt8PV0TrackJets_eta_NOSYS;
   std::vector<float>   *AntiKt8PV0TrackJets_phi_NOSYS;
   std::vector<float>   *AntiKt8PV0TrackJets_m_NOSYS;
   std::vector<std::vector<float> > *AntiKt8PV0TrackJets_daughter__pt_NOSYS;
   std::vector<std::vector<float> > *AntiKt8PV0TrackJets_daughter__eta_NOSYS;
   std::vector<std::vector<float> > *AntiKt8PV0TrackJets_daughter__phi_NOSYS;
   std::vector<std::vector<int> > *AntiKt8PV0TrackJets_daughter__trackId_NOSYS;
   std::vector<float>   *AntiKt10PV0TrackJets_pt_NOSYS;
   std::vector<float>   *AntiKt10PV0TrackJets_eta_NOSYS;
   std::vector<float>   *AntiKt10PV0TrackJets_phi_NOSYS;
   std::vector<float>   *AntiKt10PV0TrackJets_m_NOSYS;
   std::vector<std::vector<float> > *AntiKt10PV0TrackJets_daughter__pt_NOSYS;
   std::vector<std::vector<float> > *AntiKt10PV0TrackJets_daughter__eta_NOSYS;
   std::vector<std::vector<float> > *AntiKt10PV0TrackJets_daughter__phi_NOSYS;
   std::vector<std::vector<int> > *AntiKt10PV0TrackJets_daughter__trackId_NOSYS;
   std::vector<float>   *AntiKtVR30Rmax4Rmin02TrackJets_pt_NOSYS;
   std::vector<float>   *AntiKtVR30Rmax4Rmin02TrackJets_eta_NOSYS;
   std::vector<float>   *AntiKtVR30Rmax4Rmin02TrackJets_phi_NOSYS;
   std::vector<float>   *AntiKtVR30Rmax4Rmin02TrackJets_m_NOSYS;
   std::vector<std::vector<float> > *AntiKtVR30Rmax4Rmin02TrackJets_daughter__pt_NOSYS;
   std::vector<std::vector<float> > *AntiKtVR30Rmax4Rmin02TrackJets_daughter__eta_NOSYS;
   std::vector<std::vector<float> > *AntiKtVR30Rmax4Rmin02TrackJets_daughter__phi_NOSYS;
   std::vector<std::vector<int> > *AntiKtVR30Rmax4Rmin02TrackJets_daughter__trackId_NOSYS;
   std::vector<float>   *AnalysisElectrons_pt_EG_RESOLUTION_ALL__1down;
   std::vector<char>    *AnalysisElectrons_el_selected_EG_RESOLUTION_ALL__1down;
   std::vector<char>    *AnalysisElectrons_isIsolated_Tight_VarRad_EG_RESOLUTION_ALL__1down;
   std::vector<float>   *AnalysisElectrons_effSF_EG_RESOLUTION_ALL__1down;
   std::vector<float>   *AnalysisElectrons_effSF_ID_Tight_EG_RESOLUTION_ALL__1down;
   std::vector<float>   *AnalysisElectrons_effSF_Chflip_Tight_Tight_VarRad_EG_RESOLUTION_ALL__1down;
   std::vector<float>   *AnalysisElectrons_effSF_Isol_Tight_Tight_VarRad_EG_RESOLUTION_ALL__1down;
   std::vector<float>   *AnalysisElectrons_effSF_Trig_Tight_Tight_VarRad_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_EG_RESOLUTION_ALL__1down;
   std::vector<float>   *AnalysisElectrons_effSF_Trig_Tight_Tight_VarRad_Eff_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_EG_RESOLUTION_ALL__1down;
   std::vector<float>   *AnalysisElectrons_effSF_Chflip_Tight_noIso_EG_RESOLUTION_ALL__1down;
   std::vector<float>   *AnalysisElectrons_effSF_Trig_Tight_noIso_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_EG_RESOLUTION_ALL__1down;
   std::vector<float>   *AnalysisElectrons_effSF_Trig_Tight_noIso_Eff_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_EG_RESOLUTION_ALL__1down;
   std::vector<char>    *AnalysisMuons_mu_selected_EG_RESOLUTION_ALL__1down;
   std::vector<char>    *AnalysisJets_jet_selected_EG_RESOLUTION_ALL__1down;
   Float_t         METInfo_MET_EG_RESOLUTION_ALL__1down;
   Float_t         METInfo_METPhi_EG_RESOLUTION_ALL__1down;
   std::vector<float>   *AnalysisElectrons_pt_EG_RESOLUTION_ALL__1up;
   std::vector<char>    *AnalysisElectrons_el_selected_EG_RESOLUTION_ALL__1up;
   std::vector<char>    *AnalysisElectrons_isIsolated_Tight_VarRad_EG_RESOLUTION_ALL__1up;
   std::vector<float>   *AnalysisElectrons_effSF_EG_RESOLUTION_ALL__1up;
   std::vector<float>   *AnalysisElectrons_effSF_ID_Tight_EG_RESOLUTION_ALL__1up;
   std::vector<float>   *AnalysisElectrons_effSF_Chflip_Tight_Tight_VarRad_EG_RESOLUTION_ALL__1up;
   std::vector<float>   *AnalysisElectrons_effSF_Isol_Tight_Tight_VarRad_EG_RESOLUTION_ALL__1up;
   std::vector<float>   *AnalysisElectrons_effSF_Trig_Tight_Tight_VarRad_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_EG_RESOLUTION_ALL__1up;
   std::vector<float>   *AnalysisElectrons_effSF_Trig_Tight_Tight_VarRad_Eff_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_EG_RESOLUTION_ALL__1up;
   std::vector<float>   *AnalysisElectrons_effSF_Chflip_Tight_noIso_EG_RESOLUTION_ALL__1up;
   std::vector<float>   *AnalysisElectrons_effSF_Trig_Tight_noIso_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_EG_RESOLUTION_ALL__1up;
   std::vector<float>   *AnalysisElectrons_effSF_Trig_Tight_noIso_Eff_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_EG_RESOLUTION_ALL__1up;
   std::vector<char>    *AnalysisMuons_mu_selected_EG_RESOLUTION_ALL__1up;
   std::vector<char>    *AnalysisJets_jet_selected_EG_RESOLUTION_ALL__1up;
   Float_t         METInfo_MET_EG_RESOLUTION_ALL__1up;
   Float_t         METInfo_METPhi_EG_RESOLUTION_ALL__1up;
   std::vector<float>   *AnalysisElectrons_pt_EG_SCALE_AF2__1down;
   std::vector<char>    *AnalysisElectrons_el_selected_EG_SCALE_AF2__1down;
   std::vector<char>    *AnalysisElectrons_isIsolated_Tight_VarRad_EG_SCALE_AF2__1down;
   std::vector<float>   *AnalysisElectrons_effSF_EG_SCALE_AF2__1down;
   std::vector<float>   *AnalysisElectrons_effSF_ID_Tight_EG_SCALE_AF2__1down;
   std::vector<float>   *AnalysisElectrons_effSF_Chflip_Tight_Tight_VarRad_EG_SCALE_AF2__1down;
   std::vector<float>   *AnalysisElectrons_effSF_Isol_Tight_Tight_VarRad_EG_SCALE_AF2__1down;
   std::vector<float>   *AnalysisElectrons_effSF_Trig_Tight_Tight_VarRad_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_EG_SCALE_AF2__1down;
   std::vector<float>   *AnalysisElectrons_effSF_Trig_Tight_Tight_VarRad_Eff_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_EG_SCALE_AF2__1down;
   std::vector<float>   *AnalysisElectrons_effSF_Chflip_Tight_noIso_EG_SCALE_AF2__1down;
   std::vector<float>   *AnalysisElectrons_effSF_Trig_Tight_noIso_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_EG_SCALE_AF2__1down;
   std::vector<float>   *AnalysisElectrons_effSF_Trig_Tight_noIso_Eff_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_EG_SCALE_AF2__1down;
   std::vector<char>    *AnalysisMuons_mu_selected_EG_SCALE_AF2__1down;
   std::vector<char>    *AnalysisJets_jet_selected_EG_SCALE_AF2__1down;
   Float_t         METInfo_MET_EG_SCALE_AF2__1down;
   Float_t         METInfo_METPhi_EG_SCALE_AF2__1down;
   std::vector<float>   *AnalysisElectrons_pt_EG_SCALE_AF2__1up;
   std::vector<char>    *AnalysisElectrons_el_selected_EG_SCALE_AF2__1up;
   std::vector<char>    *AnalysisElectrons_isIsolated_Tight_VarRad_EG_SCALE_AF2__1up;
   std::vector<float>   *AnalysisElectrons_effSF_EG_SCALE_AF2__1up;
   std::vector<float>   *AnalysisElectrons_effSF_ID_Tight_EG_SCALE_AF2__1up;
   std::vector<float>   *AnalysisElectrons_effSF_Chflip_Tight_Tight_VarRad_EG_SCALE_AF2__1up;
   std::vector<float>   *AnalysisElectrons_effSF_Isol_Tight_Tight_VarRad_EG_SCALE_AF2__1up;
   std::vector<float>   *AnalysisElectrons_effSF_Trig_Tight_Tight_VarRad_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_EG_SCALE_AF2__1up;
   std::vector<float>   *AnalysisElectrons_effSF_Trig_Tight_Tight_VarRad_Eff_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_EG_SCALE_AF2__1up;
   std::vector<float>   *AnalysisElectrons_effSF_Chflip_Tight_noIso_EG_SCALE_AF2__1up;
   std::vector<float>   *AnalysisElectrons_effSF_Trig_Tight_noIso_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_EG_SCALE_AF2__1up;
   std::vector<float>   *AnalysisElectrons_effSF_Trig_Tight_noIso_Eff_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_EG_SCALE_AF2__1up;
   std::vector<char>    *AnalysisMuons_mu_selected_EG_SCALE_AF2__1up;
   std::vector<char>    *AnalysisJets_jet_selected_EG_SCALE_AF2__1up;
   Float_t         METInfo_MET_EG_SCALE_AF2__1up;
   Float_t         METInfo_METPhi_EG_SCALE_AF2__1up;
   std::vector<float>   *AnalysisElectrons_pt_EG_SCALE_ALL__1down;
   std::vector<char>    *AnalysisElectrons_el_selected_EG_SCALE_ALL__1down;
   std::vector<char>    *AnalysisElectrons_isIsolated_Tight_VarRad_EG_SCALE_ALL__1down;
   std::vector<float>   *AnalysisElectrons_effSF_EG_SCALE_ALL__1down;
   std::vector<float>   *AnalysisElectrons_effSF_ID_Tight_EG_SCALE_ALL__1down;
   std::vector<float>   *AnalysisElectrons_effSF_Chflip_Tight_Tight_VarRad_EG_SCALE_ALL__1down;
   std::vector<float>   *AnalysisElectrons_effSF_Isol_Tight_Tight_VarRad_EG_SCALE_ALL__1down;
   std::vector<float>   *AnalysisElectrons_effSF_Trig_Tight_Tight_VarRad_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_EG_SCALE_ALL__1down;
   std::vector<float>   *AnalysisElectrons_effSF_Trig_Tight_Tight_VarRad_Eff_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_EG_SCALE_ALL__1down;
   std::vector<float>   *AnalysisElectrons_effSF_Chflip_Tight_noIso_EG_SCALE_ALL__1down;
   std::vector<float>   *AnalysisElectrons_effSF_Trig_Tight_noIso_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_EG_SCALE_ALL__1down;
   std::vector<float>   *AnalysisElectrons_effSF_Trig_Tight_noIso_Eff_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_EG_SCALE_ALL__1down;
   std::vector<char>    *AnalysisMuons_mu_selected_EG_SCALE_ALL__1down;
   std::vector<char>    *AnalysisJets_jet_selected_EG_SCALE_ALL__1down;
   Float_t         METInfo_MET_EG_SCALE_ALL__1down;
   Float_t         METInfo_METPhi_EG_SCALE_ALL__1down;
   std::vector<float>   *AnalysisElectrons_pt_EG_SCALE_ALL__1up;
   std::vector<char>    *AnalysisElectrons_el_selected_EG_SCALE_ALL__1up;
   std::vector<char>    *AnalysisElectrons_isIsolated_Tight_VarRad_EG_SCALE_ALL__1up;
   std::vector<float>   *AnalysisElectrons_effSF_EG_SCALE_ALL__1up;
   std::vector<float>   *AnalysisElectrons_effSF_ID_Tight_EG_SCALE_ALL__1up;
   std::vector<float>   *AnalysisElectrons_effSF_Chflip_Tight_Tight_VarRad_EG_SCALE_ALL__1up;
   std::vector<float>   *AnalysisElectrons_effSF_Isol_Tight_Tight_VarRad_EG_SCALE_ALL__1up;
   std::vector<float>   *AnalysisElectrons_effSF_Trig_Tight_Tight_VarRad_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_EG_SCALE_ALL__1up;
   std::vector<float>   *AnalysisElectrons_effSF_Trig_Tight_Tight_VarRad_Eff_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_EG_SCALE_ALL__1up;
   std::vector<float>   *AnalysisElectrons_effSF_Chflip_Tight_noIso_EG_SCALE_ALL__1up;
   std::vector<float>   *AnalysisElectrons_effSF_Trig_Tight_noIso_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_EG_SCALE_ALL__1up;
   std::vector<float>   *AnalysisElectrons_effSF_Trig_Tight_noIso_Eff_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_EG_SCALE_ALL__1up;
   std::vector<char>    *AnalysisMuons_mu_selected_EG_SCALE_ALL__1up;
   std::vector<char>    *AnalysisJets_jet_selected_EG_SCALE_ALL__1up;
   Float_t         METInfo_MET_EG_SCALE_ALL__1up;
   Float_t         METInfo_METPhi_EG_SCALE_ALL__1up;
   std::vector<float>   *AnalysisElectrons_effSF_Chflip_Tight_Tight_VarRad_EL_CHARGEID_STAT__1down;
   std::vector<float>   *AnalysisElectrons_effSF_Chflip_Tight_noIso_EL_CHARGEID_STAT__1down;
   std::vector<float>   *AnalysisElectrons_effSF_Chflip_Tight_Tight_VarRad_EL_CHARGEID_STAT__1up;
   std::vector<float>   *AnalysisElectrons_effSF_Chflip_Tight_noIso_EL_CHARGEID_STAT__1up;
   std::vector<float>   *AnalysisElectrons_effSF_Chflip_Tight_Tight_VarRad_EL_CHARGEID_SYStotal__1down;
   std::vector<float>   *AnalysisElectrons_effSF_Chflip_Tight_noIso_EL_CHARGEID_SYStotal__1down;
   std::vector<float>   *AnalysisElectrons_effSF_Chflip_Tight_Tight_VarRad_EL_CHARGEID_SYStotal__1up;
   std::vector<float>   *AnalysisElectrons_effSF_Chflip_Tight_noIso_EL_CHARGEID_SYStotal__1up;
   std::vector<float>   *AnalysisElectrons_effSF_ID_Tight_EL_EFF_ID_TOTAL_1NPCOR_PLUS_UNCOR__1down;
   std::vector<float>   *AnalysisElectrons_effSF_ID_Tight_EL_EFF_ID_TOTAL_1NPCOR_PLUS_UNCOR__1up;
   std::vector<float>   *AnalysisElectrons_effSF_Isol_Tight_Tight_VarRad_EL_EFF_Iso_TOTAL_1NPCOR_PLUS_UNCOR__1down;
   std::vector<float>   *AnalysisElectrons_effSF_Isol_Tight_Tight_VarRad_EL_EFF_Iso_TOTAL_1NPCOR_PLUS_UNCOR__1up;
   std::vector<float>   *AnalysisElectrons_effSF_EL_EFF_Reco_TOTAL_1NPCOR_PLUS_UNCOR__1down;
   std::vector<float>   *AnalysisElectrons_effSF_EL_EFF_Reco_TOTAL_1NPCOR_PLUS_UNCOR__1up;
   std::vector<float>   *AnalysisElectrons_effSF_Trig_Tight_Tight_VarRad_Eff_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_EL_EFF_TriggerEff_TOTAL_1NPCOR_PLUS_UNCOR__1down;
   std::vector<float>   *AnalysisElectrons_effSF_Trig_Tight_noIso_Eff_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_EL_EFF_TriggerEff_TOTAL_1NPCOR_PLUS_UNCOR__1down;
   std::vector<float>   *AnalysisElectrons_effSF_Trig_Tight_Tight_VarRad_Eff_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_EL_EFF_TriggerEff_TOTAL_1NPCOR_PLUS_UNCOR__1up;
   std::vector<float>   *AnalysisElectrons_effSF_Trig_Tight_noIso_Eff_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_EL_EFF_TriggerEff_TOTAL_1NPCOR_PLUS_UNCOR__1up;
   std::vector<float>   *AnalysisElectrons_effSF_Trig_Tight_Tight_VarRad_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_EL_EFF_Trigger_TOTAL_1NPCOR_PLUS_UNCOR__1down;
   std::vector<float>   *AnalysisElectrons_effSF_Trig_Tight_noIso_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_EL_EFF_Trigger_TOTAL_1NPCOR_PLUS_UNCOR__1down;
   std::vector<float>   *AnalysisElectrons_effSF_Trig_Tight_Tight_VarRad_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_EL_EFF_Trigger_TOTAL_1NPCOR_PLUS_UNCOR__1up;
   std::vector<float>   *AnalysisElectrons_effSF_Trig_Tight_noIso_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_EL_EFF_Trigger_TOTAL_1NPCOR_PLUS_UNCOR__1up;
   std::vector<float>   *AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_FT_EFF_Eigen_B_0__1down;
   std::vector<float>   *AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_FT_EFF_Eigen_B_0__1up;
   std::vector<float>   *AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_FT_EFF_Eigen_B_1__1down;
   std::vector<float>   *AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_FT_EFF_Eigen_B_1__1up;
   std::vector<float>   *AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_FT_EFF_Eigen_B_2__1down;
   std::vector<float>   *AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_FT_EFF_Eigen_B_2__1up;
   std::vector<float>   *AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_FT_EFF_Eigen_B_3__1down;
   std::vector<float>   *AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_FT_EFF_Eigen_B_3__1up;
   std::vector<float>   *AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_FT_EFF_Eigen_B_4__1down;
   std::vector<float>   *AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_FT_EFF_Eigen_B_4__1up;
   std::vector<float>   *AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_FT_EFF_Eigen_B_5__1down;
   std::vector<float>   *AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_FT_EFF_Eigen_B_5__1up;
   std::vector<float>   *AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_FT_EFF_Eigen_B_6__1down;
   std::vector<float>   *AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_FT_EFF_Eigen_B_6__1up;
   std::vector<float>   *AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_FT_EFF_Eigen_B_7__1down;
   std::vector<float>   *AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_FT_EFF_Eigen_B_7__1up;
   std::vector<float>   *AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_FT_EFF_Eigen_B_8__1down;
   std::vector<float>   *AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_FT_EFF_Eigen_B_8__1up;
   std::vector<float>   *AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_FT_EFF_Eigen_C_0__1down;
   std::vector<float>   *AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_FT_EFF_Eigen_C_0__1up;
   std::vector<float>   *AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_FT_EFF_Eigen_C_1__1down;
   std::vector<float>   *AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_FT_EFF_Eigen_C_1__1up;
   std::vector<float>   *AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_FT_EFF_Eigen_C_2__1down;
   std::vector<float>   *AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_FT_EFF_Eigen_C_2__1up;
   std::vector<float>   *AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_FT_EFF_Eigen_C_3__1down;
   std::vector<float>   *AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_FT_EFF_Eigen_C_3__1up;
   std::vector<float>   *AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_FT_EFF_Eigen_Light_0__1down;
   std::vector<float>   *AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_FT_EFF_Eigen_Light_0__1up;
   std::vector<float>   *AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_FT_EFF_Eigen_Light_1__1down;
   std::vector<float>   *AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_FT_EFF_Eigen_Light_1__1up;
   std::vector<float>   *AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_FT_EFF_Eigen_Light_2__1down;
   std::vector<float>   *AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_FT_EFF_Eigen_Light_2__1up;
   std::vector<float>   *AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_FT_EFF_Eigen_Light_3__1down;
   std::vector<float>   *AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_FT_EFF_Eigen_Light_3__1up;
   std::vector<float>   *AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_FT_EFF_Eigen_Light_4__1down;
   std::vector<float>   *AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_FT_EFF_Eigen_Light_4__1up;
   std::vector<float>   *AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_FT_EFF_extrapolation__1down;
   std::vector<float>   *AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_FT_EFF_extrapolation__1up;
   std::vector<float>   *AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_FT_EFF_extrapolation_from_charm__1down;
   std::vector<float>   *AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_FT_EFF_extrapolation_from_charm__1up;
   Float_t         EventInfo_generatorWeight_GEN_MEWeight;
   Float_t         EventInfo_generatorWeight_GEN_ME_ONLY_MUR05_MUF05_PDF303200_PSMUR05_PSMUF05;
   Float_t         EventInfo_generatorWeight_GEN_ME_ONLY_MUR05_MUF1_PDF303200_PSMUR05_PSMUF1;
   Float_t         EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF05_PDF303200_PSMUR1_PSMUF05;
   Float_t         EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF14068;
   Float_t         EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF269000;
   Float_t         EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF270000;
   Float_t         EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF27400;
   Float_t         EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303200;
   Float_t         EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303200_ASSEW;
   Float_t         EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303200_ASSEWLO1;
   Float_t         EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303200_ASSEWLO1LO2;
   Float_t         EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303200_ASSEWLO1LO2LO3;
   Float_t         EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303200_EXPASSEW;
   Float_t         EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303200_EXPASSEWLO1;
   Float_t         EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303200_EXPASSEWLO1LO2;
   Float_t         EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303200_EXPASSEWLO1LO2LO3;
   Float_t         EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303200_MULTIASSEW;
   Float_t         EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303200_MULTIASSEWLO1;
   Float_t         EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303200_MULTIASSEWLO1LO2;
   Float_t         EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303200_MULTIASSEWLO1LO2LO3;
   Float_t         EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303201;
   Float_t         EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303202;
   Float_t         EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303203;
   Float_t         EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303204;
   Float_t         EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303205;
   Float_t         EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303206;
   Float_t         EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303207;
   Float_t         EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303208;
   Float_t         EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303209;
   Float_t         EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303210;
   Float_t         EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303211;
   Float_t         EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303212;
   Float_t         EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303213;
   Float_t         EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303214;
   Float_t         EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303215;
   Float_t         EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303216;
   Float_t         EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303217;
   Float_t         EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303218;
   Float_t         EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303219;
   Float_t         EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303220;
   Float_t         EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303221;
   Float_t         EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303222;
   Float_t         EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303223;
   Float_t         EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303224;
   Float_t         EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303225;
   Float_t         EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303226;
   Float_t         EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303227;
   Float_t         EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303228;
   Float_t         EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303229;
   Float_t         EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303230;
   Float_t         EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303231;
   Float_t         EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303232;
   Float_t         EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303233;
   Float_t         EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303234;
   Float_t         EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303235;
   Float_t         EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303236;
   Float_t         EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303237;
   Float_t         EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303238;
   Float_t         EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303239;
   Float_t         EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303240;
   Float_t         EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303241;
   Float_t         EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303242;
   Float_t         EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303243;
   Float_t         EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303244;
   Float_t         EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303245;
   Float_t         EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303246;
   Float_t         EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303247;
   Float_t         EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303248;
   Float_t         EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303249;
   Float_t         EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303250;
   Float_t         EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303251;
   Float_t         EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303252;
   Float_t         EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303253;
   Float_t         EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303254;
   Float_t         EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303255;
   Float_t         EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303256;
   Float_t         EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303257;
   Float_t         EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303258;
   Float_t         EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303259;
   Float_t         EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303260;
   Float_t         EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303261;
   Float_t         EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303262;
   Float_t         EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303263;
   Float_t         EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303264;
   Float_t         EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303265;
   Float_t         EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303266;
   Float_t         EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303267;
   Float_t         EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303268;
   Float_t         EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303269;
   Float_t         EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303270;
   Float_t         EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303271;
   Float_t         EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303272;
   Float_t         EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303273;
   Float_t         EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303274;
   Float_t         EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303275;
   Float_t         EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303276;
   Float_t         EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303277;
   Float_t         EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303278;
   Float_t         EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303279;
   Float_t         EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303280;
   Float_t         EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303281;
   Float_t         EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303282;
   Float_t         EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303283;
   Float_t         EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303284;
   Float_t         EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303285;
   Float_t         EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303286;
   Float_t         EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303287;
   Float_t         EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303288;
   Float_t         EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303289;
   Float_t         EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303290;
   Float_t         EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303291;
   Float_t         EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303292;
   Float_t         EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303293;
   Float_t         EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303294;
   Float_t         EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303295;
   Float_t         EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303296;
   Float_t         EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303297;
   Float_t         EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303298;
   Float_t         EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303299;
   Float_t         EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303300;
   Float_t         EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF304400;
   Float_t         EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF91400;
   Float_t         EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF91401;
   Float_t         EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF91402;
   Float_t         EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF91403;
   Float_t         EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF91404;
   Float_t         EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF91405;
   Float_t         EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF91406;
   Float_t         EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF91407;
   Float_t         EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF91408;
   Float_t         EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF91409;
   Float_t         EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF91410;
   Float_t         EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF91411;
   Float_t         EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF91412;
   Float_t         EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF91413;
   Float_t         EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF91414;
   Float_t         EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF91415;
   Float_t         EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF91416;
   Float_t         EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF91417;
   Float_t         EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF91418;
   Float_t         EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF91419;
   Float_t         EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF91420;
   Float_t         EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF91421;
   Float_t         EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF91422;
   Float_t         EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF91423;
   Float_t         EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF91424;
   Float_t         EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF91425;
   Float_t         EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF91426;
   Float_t         EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF91427;
   Float_t         EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF91428;
   Float_t         EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF91429;
   Float_t         EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF91430;
   Float_t         EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF91431;
   Float_t         EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF91432;
   Float_t         EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF2_PDF303200_PSMUR1_PSMUF2;
   Float_t         EventInfo_generatorWeight_GEN_ME_ONLY_MUR2_MUF1_PDF303200_PSMUR2_PSMUF1;
   Float_t         EventInfo_generatorWeight_GEN_ME_ONLY_MUR2_MUF2_PDF303200_PSMUR2_PSMUF2;
   Float_t         EventInfo_generatorWeight_GEN_MUR05_MUF05_PDF303200_PSMUR05_PSMUF05;
   Float_t         EventInfo_generatorWeight_GEN_MUR05_MUF1_PDF303200_PSMUR05_PSMUF1;
   Float_t         EventInfo_generatorWeight_GEN_MUR1_MUF05_PDF303200_PSMUR1_PSMUF05;
   Float_t         EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF14068;
   Float_t         EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF269000;
   Float_t         EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF270000;
   Float_t         EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF27400;
   Float_t         EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303200;
   Float_t         EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303200_ASSEW;
   Float_t         EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303200_ASSEWLO1;
   Float_t         EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303200_ASSEWLO1LO2;
   Float_t         EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303200_ASSEWLO1LO2LO3;
   Float_t         EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303200_EXPASSEW;
   Float_t         EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303200_EXPASSEWLO1;
   Float_t         EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303200_EXPASSEWLO1LO2;
   Float_t         EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303200_EXPASSEWLO1LO2LO3;
   Float_t         EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303200_MULTIASSEW;
   Float_t         EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303200_MULTIASSEWLO1;
   Float_t         EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303200_MULTIASSEWLO1LO2;
   Float_t         EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303200_MULTIASSEWLO1LO2LO3;
   Float_t         EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303201;
   Float_t         EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303202;
   Float_t         EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303203;
   Float_t         EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303204;
   Float_t         EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303205;
   Float_t         EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303206;
   Float_t         EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303207;
   Float_t         EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303208;
   Float_t         EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303209;
   Float_t         EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303210;
   Float_t         EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303211;
   Float_t         EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303212;
   Float_t         EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303213;
   Float_t         EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303214;
   Float_t         EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303215;
   Float_t         EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303216;
   Float_t         EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303217;
   Float_t         EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303218;
   Float_t         EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303219;
   Float_t         EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303220;
   Float_t         EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303221;
   Float_t         EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303222;
   Float_t         EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303223;
   Float_t         EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303224;
   Float_t         EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303225;
   Float_t         EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303226;
   Float_t         EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303227;
   Float_t         EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303228;
   Float_t         EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303229;
   Float_t         EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303230;
   Float_t         EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303231;
   Float_t         EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303232;
   Float_t         EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303233;
   Float_t         EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303234;
   Float_t         EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303235;
   Float_t         EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303236;
   Float_t         EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303237;
   Float_t         EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303238;
   Float_t         EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303239;
   Float_t         EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303240;
   Float_t         EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303241;
   Float_t         EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303242;
   Float_t         EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303243;
   Float_t         EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303244;
   Float_t         EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303245;
   Float_t         EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303246;
   Float_t         EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303247;
   Float_t         EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303248;
   Float_t         EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303249;
   Float_t         EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303250;
   Float_t         EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303251;
   Float_t         EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303252;
   Float_t         EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303253;
   Float_t         EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303254;
   Float_t         EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303255;
   Float_t         EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303256;
   Float_t         EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303257;
   Float_t         EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303258;
   Float_t         EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303259;
   Float_t         EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303260;
   Float_t         EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303261;
   Float_t         EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303262;
   Float_t         EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303263;
   Float_t         EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303264;
   Float_t         EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303265;
   Float_t         EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303266;
   Float_t         EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303267;
   Float_t         EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303268;
   Float_t         EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303269;
   Float_t         EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303270;
   Float_t         EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303271;
   Float_t         EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303272;
   Float_t         EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303273;
   Float_t         EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303274;
   Float_t         EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303275;
   Float_t         EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303276;
   Float_t         EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303277;
   Float_t         EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303278;
   Float_t         EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303279;
   Float_t         EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303280;
   Float_t         EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303281;
   Float_t         EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303282;
   Float_t         EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303283;
   Float_t         EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303284;
   Float_t         EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303285;
   Float_t         EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303286;
   Float_t         EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303287;
   Float_t         EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303288;
   Float_t         EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303289;
   Float_t         EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303290;
   Float_t         EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303291;
   Float_t         EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303292;
   Float_t         EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303293;
   Float_t         EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303294;
   Float_t         EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303295;
   Float_t         EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303296;
   Float_t         EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303297;
   Float_t         EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303298;
   Float_t         EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303299;
   Float_t         EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303300;
   Float_t         EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF304400;
   Float_t         EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF91400;
   Float_t         EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF91401;
   Float_t         EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF91402;
   Float_t         EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF91403;
   Float_t         EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF91404;
   Float_t         EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF91405;
   Float_t         EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF91406;
   Float_t         EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF91407;
   Float_t         EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF91408;
   Float_t         EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF91409;
   Float_t         EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF91410;
   Float_t         EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF91411;
   Float_t         EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF91412;
   Float_t         EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF91413;
   Float_t         EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF91414;
   Float_t         EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF91415;
   Float_t         EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF91416;
   Float_t         EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF91417;
   Float_t         EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF91418;
   Float_t         EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF91419;
   Float_t         EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF91420;
   Float_t         EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF91421;
   Float_t         EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF91422;
   Float_t         EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF91423;
   Float_t         EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF91424;
   Float_t         EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF91425;
   Float_t         EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF91426;
   Float_t         EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF91427;
   Float_t         EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF91428;
   Float_t         EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF91429;
   Float_t         EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF91430;
   Float_t         EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF91431;
   Float_t         EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF91432;
   Float_t         EventInfo_generatorWeight_GEN_MUR1_MUF2_PDF303200_PSMUR1_PSMUF2;
   Float_t         EventInfo_generatorWeight_GEN_MUR2_MUF1_PDF303200_PSMUR2_PSMUF1;
   Float_t         EventInfo_generatorWeight_GEN_MUR2_MUF2_PDF303200_PSMUR2_PSMUF2;
   Float_t         EventInfo_generatorWeight_GEN_NTrials;
   Float_t         EventInfo_generatorWeight_GEN_UserHook;
   Float_t         EventInfo_generatorWeight_GEN_WeightNormalisation;
   Float_t         EventInfo_jvt_effSF_JET_EffectiveNP_1__1up;
   std::vector<char>    *AnalysisElectrons_el_selected_JET_EffectiveNP_1__1up;
   std::vector<char>    *AnalysisMuons_mu_selected_JET_EffectiveNP_1__1up;
   std::vector<float>   *AnalysisJets_pt_JET_EffectiveNP_1__1up;
   std::vector<char>    *AnalysisJets_jet_selected_JET_EffectiveNP_1__1up;
   std::vector<float>   *AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_JET_EffectiveNP_1__1up;
   Float_t         METInfo_MET_JET_EffectiveNP_1__1up;
   Float_t         METInfo_METPhi_JET_EffectiveNP_1__1up;
   Float_t         EventInfo_jvt_effSF_JET_EffectiveNP_1__1down;
   std::vector<char>    *AnalysisElectrons_el_selected_JET_EffectiveNP_1__1down;
   std::vector<char>    *AnalysisMuons_mu_selected_JET_EffectiveNP_1__1down;
   std::vector<float>   *AnalysisJets_pt_JET_EffectiveNP_1__1down;
   std::vector<char>    *AnalysisJets_jet_selected_JET_EffectiveNP_1__1down;
   std::vector<float>   *AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_JET_EffectiveNP_1__1down;
   Float_t         METInfo_MET_JET_EffectiveNP_1__1down;
   Float_t         METInfo_METPhi_JET_EffectiveNP_1__1down;
   Float_t         EventInfo_jvt_effSF_JET_EffectiveNP_2__1up;
   std::vector<char>    *AnalysisElectrons_el_selected_JET_EffectiveNP_2__1up;
   std::vector<char>    *AnalysisMuons_mu_selected_JET_EffectiveNP_2__1up;
   std::vector<float>   *AnalysisJets_pt_JET_EffectiveNP_2__1up;
   std::vector<char>    *AnalysisJets_jet_selected_JET_EffectiveNP_2__1up;
   std::vector<float>   *AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_JET_EffectiveNP_2__1up;
   Float_t         METInfo_MET_JET_EffectiveNP_2__1up;
   Float_t         METInfo_METPhi_JET_EffectiveNP_2__1up;
   Float_t         EventInfo_jvt_effSF_JET_EffectiveNP_2__1down;
   std::vector<char>    *AnalysisElectrons_el_selected_JET_EffectiveNP_2__1down;
   std::vector<char>    *AnalysisMuons_mu_selected_JET_EffectiveNP_2__1down;
   std::vector<float>   *AnalysisJets_pt_JET_EffectiveNP_2__1down;
   std::vector<char>    *AnalysisJets_jet_selected_JET_EffectiveNP_2__1down;
   std::vector<float>   *AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_JET_EffectiveNP_2__1down;
   Float_t         METInfo_MET_JET_EffectiveNP_2__1down;
   Float_t         METInfo_METPhi_JET_EffectiveNP_2__1down;
   Float_t         EventInfo_jvt_effSF_JET_EffectiveNP_3__1up;
   std::vector<char>    *AnalysisElectrons_el_selected_JET_EffectiveNP_3__1up;
   std::vector<char>    *AnalysisMuons_mu_selected_JET_EffectiveNP_3__1up;
   std::vector<float>   *AnalysisJets_pt_JET_EffectiveNP_3__1up;
   std::vector<char>    *AnalysisJets_jet_selected_JET_EffectiveNP_3__1up;
   std::vector<float>   *AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_JET_EffectiveNP_3__1up;
   Float_t         METInfo_MET_JET_EffectiveNP_3__1up;
   Float_t         METInfo_METPhi_JET_EffectiveNP_3__1up;
   Float_t         EventInfo_jvt_effSF_JET_EffectiveNP_3__1down;
   std::vector<char>    *AnalysisElectrons_el_selected_JET_EffectiveNP_3__1down;
   std::vector<char>    *AnalysisMuons_mu_selected_JET_EffectiveNP_3__1down;
   std::vector<float>   *AnalysisJets_pt_JET_EffectiveNP_3__1down;
   std::vector<char>    *AnalysisJets_jet_selected_JET_EffectiveNP_3__1down;
   std::vector<float>   *AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_JET_EffectiveNP_3__1down;
   Float_t         METInfo_MET_JET_EffectiveNP_3__1down;
   Float_t         METInfo_METPhi_JET_EffectiveNP_3__1down;
   Float_t         EventInfo_jvt_effSF_JET_EffectiveNP_4__1up;
   std::vector<char>    *AnalysisElectrons_el_selected_JET_EffectiveNP_4__1up;
   std::vector<char>    *AnalysisMuons_mu_selected_JET_EffectiveNP_4__1up;
   std::vector<float>   *AnalysisJets_pt_JET_EffectiveNP_4__1up;
   std::vector<char>    *AnalysisJets_jet_selected_JET_EffectiveNP_4__1up;
   std::vector<float>   *AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_JET_EffectiveNP_4__1up;
   Float_t         METInfo_MET_JET_EffectiveNP_4__1up;
   Float_t         METInfo_METPhi_JET_EffectiveNP_4__1up;
   Float_t         EventInfo_jvt_effSF_JET_EffectiveNP_4__1down;
   std::vector<char>    *AnalysisElectrons_el_selected_JET_EffectiveNP_4__1down;
   std::vector<char>    *AnalysisMuons_mu_selected_JET_EffectiveNP_4__1down;
   std::vector<float>   *AnalysisJets_pt_JET_EffectiveNP_4__1down;
   std::vector<char>    *AnalysisJets_jet_selected_JET_EffectiveNP_4__1down;
   std::vector<float>   *AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_JET_EffectiveNP_4__1down;
   Float_t         METInfo_MET_JET_EffectiveNP_4__1down;
   Float_t         METInfo_METPhi_JET_EffectiveNP_4__1down;
   Float_t         EventInfo_jvt_effSF_JET_EffectiveNP_5__1up;
   std::vector<char>    *AnalysisElectrons_el_selected_JET_EffectiveNP_5__1up;
   std::vector<char>    *AnalysisMuons_mu_selected_JET_EffectiveNP_5__1up;
   std::vector<float>   *AnalysisJets_pt_JET_EffectiveNP_5__1up;
   std::vector<char>    *AnalysisJets_jet_selected_JET_EffectiveNP_5__1up;
   std::vector<float>   *AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_JET_EffectiveNP_5__1up;
   Float_t         METInfo_MET_JET_EffectiveNP_5__1up;
   Float_t         METInfo_METPhi_JET_EffectiveNP_5__1up;
   Float_t         EventInfo_jvt_effSF_JET_EffectiveNP_5__1down;
   std::vector<char>    *AnalysisElectrons_el_selected_JET_EffectiveNP_5__1down;
   std::vector<char>    *AnalysisMuons_mu_selected_JET_EffectiveNP_5__1down;
   std::vector<float>   *AnalysisJets_pt_JET_EffectiveNP_5__1down;
   std::vector<char>    *AnalysisJets_jet_selected_JET_EffectiveNP_5__1down;
   std::vector<float>   *AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_JET_EffectiveNP_5__1down;
   Float_t         METInfo_MET_JET_EffectiveNP_5__1down;
   Float_t         METInfo_METPhi_JET_EffectiveNP_5__1down;
   Float_t         EventInfo_jvt_effSF_JET_EffectiveNP_6__1up;
   std::vector<char>    *AnalysisElectrons_el_selected_JET_EffectiveNP_6__1up;
   std::vector<char>    *AnalysisMuons_mu_selected_JET_EffectiveNP_6__1up;
   std::vector<float>   *AnalysisJets_pt_JET_EffectiveNP_6__1up;
   std::vector<char>    *AnalysisJets_jet_selected_JET_EffectiveNP_6__1up;
   std::vector<float>   *AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_JET_EffectiveNP_6__1up;
   Float_t         METInfo_MET_JET_EffectiveNP_6__1up;
   Float_t         METInfo_METPhi_JET_EffectiveNP_6__1up;
   Float_t         EventInfo_jvt_effSF_JET_EffectiveNP_6__1down;
   std::vector<char>    *AnalysisElectrons_el_selected_JET_EffectiveNP_6__1down;
   std::vector<char>    *AnalysisMuons_mu_selected_JET_EffectiveNP_6__1down;
   std::vector<float>   *AnalysisJets_pt_JET_EffectiveNP_6__1down;
   std::vector<char>    *AnalysisJets_jet_selected_JET_EffectiveNP_6__1down;
   std::vector<float>   *AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_JET_EffectiveNP_6__1down;
   Float_t         METInfo_MET_JET_EffectiveNP_6__1down;
   Float_t         METInfo_METPhi_JET_EffectiveNP_6__1down;
   Float_t         EventInfo_jvt_effSF_JET_EffectiveNP_7__1up;
   std::vector<char>    *AnalysisElectrons_el_selected_JET_EffectiveNP_7__1up;
   std::vector<char>    *AnalysisMuons_mu_selected_JET_EffectiveNP_7__1up;
   std::vector<float>   *AnalysisJets_pt_JET_EffectiveNP_7__1up;
   std::vector<char>    *AnalysisJets_jet_selected_JET_EffectiveNP_7__1up;
   std::vector<float>   *AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_JET_EffectiveNP_7__1up;
   Float_t         METInfo_MET_JET_EffectiveNP_7__1up;
   Float_t         METInfo_METPhi_JET_EffectiveNP_7__1up;
   Float_t         EventInfo_jvt_effSF_JET_EffectiveNP_7__1down;
   std::vector<char>    *AnalysisElectrons_el_selected_JET_EffectiveNP_7__1down;
   std::vector<char>    *AnalysisMuons_mu_selected_JET_EffectiveNP_7__1down;
   std::vector<float>   *AnalysisJets_pt_JET_EffectiveNP_7__1down;
   std::vector<char>    *AnalysisJets_jet_selected_JET_EffectiveNP_7__1down;
   std::vector<float>   *AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_JET_EffectiveNP_7__1down;
   Float_t         METInfo_MET_JET_EffectiveNP_7__1down;
   Float_t         METInfo_METPhi_JET_EffectiveNP_7__1down;
   Float_t         EventInfo_jvt_effSF_JET_EffectiveNP_8restTerm__1up;
   std::vector<char>    *AnalysisElectrons_el_selected_JET_EffectiveNP_8restTerm__1up;
   std::vector<char>    *AnalysisMuons_mu_selected_JET_EffectiveNP_8restTerm__1up;
   std::vector<float>   *AnalysisJets_pt_JET_EffectiveNP_8restTerm__1up;
   std::vector<char>    *AnalysisJets_jet_selected_JET_EffectiveNP_8restTerm__1up;
   std::vector<float>   *AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_JET_EffectiveNP_8restTerm__1up;
   Float_t         METInfo_MET_JET_EffectiveNP_8restTerm__1up;
   Float_t         METInfo_METPhi_JET_EffectiveNP_8restTerm__1up;
   Float_t         EventInfo_jvt_effSF_JET_EffectiveNP_8restTerm__1down;
   std::vector<char>    *AnalysisElectrons_el_selected_JET_EffectiveNP_8restTerm__1down;
   std::vector<char>    *AnalysisMuons_mu_selected_JET_EffectiveNP_8restTerm__1down;
   std::vector<float>   *AnalysisJets_pt_JET_EffectiveNP_8restTerm__1down;
   std::vector<char>    *AnalysisJets_jet_selected_JET_EffectiveNP_8restTerm__1down;
   std::vector<float>   *AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_JET_EffectiveNP_8restTerm__1down;
   Float_t         METInfo_MET_JET_EffectiveNP_8restTerm__1down;
   Float_t         METInfo_METPhi_JET_EffectiveNP_8restTerm__1down;
   Float_t         EventInfo_jvt_effSF_JET_EtaIntercalibration_Modelling__1up;
   std::vector<char>    *AnalysisElectrons_el_selected_JET_EtaIntercalibration_Modelling__1up;
   std::vector<char>    *AnalysisMuons_mu_selected_JET_EtaIntercalibration_Modelling__1up;
   std::vector<float>   *AnalysisJets_pt_JET_EtaIntercalibration_Modelling__1up;
   std::vector<char>    *AnalysisJets_jet_selected_JET_EtaIntercalibration_Modelling__1up;
   std::vector<float>   *AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_JET_EtaIntercalibration_Modelling__1up;
   Float_t         METInfo_MET_JET_EtaIntercalibration_Modelling__1up;
   Float_t         METInfo_METPhi_JET_EtaIntercalibration_Modelling__1up;
   Float_t         EventInfo_jvt_effSF_JET_EtaIntercalibration_Modelling__1down;
   std::vector<char>    *AnalysisElectrons_el_selected_JET_EtaIntercalibration_Modelling__1down;
   std::vector<char>    *AnalysisMuons_mu_selected_JET_EtaIntercalibration_Modelling__1down;
   std::vector<float>   *AnalysisJets_pt_JET_EtaIntercalibration_Modelling__1down;
   std::vector<char>    *AnalysisJets_jet_selected_JET_EtaIntercalibration_Modelling__1down;
   std::vector<float>   *AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_JET_EtaIntercalibration_Modelling__1down;
   Float_t         METInfo_MET_JET_EtaIntercalibration_Modelling__1down;
   Float_t         METInfo_METPhi_JET_EtaIntercalibration_Modelling__1down;
   Float_t         EventInfo_jvt_effSF_JET_EtaIntercalibration_NonClosure_highE__1up;
   std::vector<char>    *AnalysisElectrons_el_selected_JET_EtaIntercalibration_NonClosure_highE__1up;
   std::vector<char>    *AnalysisMuons_mu_selected_JET_EtaIntercalibration_NonClosure_highE__1up;
   std::vector<float>   *AnalysisJets_pt_JET_EtaIntercalibration_NonClosure_highE__1up;
   std::vector<char>    *AnalysisJets_jet_selected_JET_EtaIntercalibration_NonClosure_highE__1up;
   std::vector<float>   *AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_JET_EtaIntercalibration_NonClosure_highE__1up;
   Float_t         METInfo_MET_JET_EtaIntercalibration_NonClosure_highE__1up;
   Float_t         METInfo_METPhi_JET_EtaIntercalibration_NonClosure_highE__1up;
   Float_t         EventInfo_jvt_effSF_JET_EtaIntercalibration_NonClosure_highE__1down;
   std::vector<char>    *AnalysisElectrons_el_selected_JET_EtaIntercalibration_NonClosure_highE__1down;
   std::vector<char>    *AnalysisMuons_mu_selected_JET_EtaIntercalibration_NonClosure_highE__1down;
   std::vector<float>   *AnalysisJets_pt_JET_EtaIntercalibration_NonClosure_highE__1down;
   std::vector<char>    *AnalysisJets_jet_selected_JET_EtaIntercalibration_NonClosure_highE__1down;
   std::vector<float>   *AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_JET_EtaIntercalibration_NonClosure_highE__1down;
   Float_t         METInfo_MET_JET_EtaIntercalibration_NonClosure_highE__1down;
   Float_t         METInfo_METPhi_JET_EtaIntercalibration_NonClosure_highE__1down;
   Float_t         EventInfo_jvt_effSF_JET_EtaIntercalibration_NonClosure_negEta__1up;
   std::vector<char>    *AnalysisElectrons_el_selected_JET_EtaIntercalibration_NonClosure_negEta__1up;
   std::vector<char>    *AnalysisMuons_mu_selected_JET_EtaIntercalibration_NonClosure_negEta__1up;
   std::vector<float>   *AnalysisJets_pt_JET_EtaIntercalibration_NonClosure_negEta__1up;
   std::vector<char>    *AnalysisJets_jet_selected_JET_EtaIntercalibration_NonClosure_negEta__1up;
   std::vector<float>   *AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_JET_EtaIntercalibration_NonClosure_negEta__1up;
   Float_t         METInfo_MET_JET_EtaIntercalibration_NonClosure_negEta__1up;
   Float_t         METInfo_METPhi_JET_EtaIntercalibration_NonClosure_negEta__1up;
   Float_t         EventInfo_jvt_effSF_JET_EtaIntercalibration_NonClosure_negEta__1down;
   std::vector<char>    *AnalysisElectrons_el_selected_JET_EtaIntercalibration_NonClosure_negEta__1down;
   std::vector<char>    *AnalysisMuons_mu_selected_JET_EtaIntercalibration_NonClosure_negEta__1down;
   std::vector<float>   *AnalysisJets_pt_JET_EtaIntercalibration_NonClosure_negEta__1down;
   std::vector<char>    *AnalysisJets_jet_selected_JET_EtaIntercalibration_NonClosure_negEta__1down;
   std::vector<float>   *AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_JET_EtaIntercalibration_NonClosure_negEta__1down;
   Float_t         METInfo_MET_JET_EtaIntercalibration_NonClosure_negEta__1down;
   Float_t         METInfo_METPhi_JET_EtaIntercalibration_NonClosure_negEta__1down;
   Float_t         EventInfo_jvt_effSF_JET_EtaIntercalibration_NonClosure_posEta__1up;
   std::vector<char>    *AnalysisElectrons_el_selected_JET_EtaIntercalibration_NonClosure_posEta__1up;
   std::vector<char>    *AnalysisMuons_mu_selected_JET_EtaIntercalibration_NonClosure_posEta__1up;
   std::vector<float>   *AnalysisJets_pt_JET_EtaIntercalibration_NonClosure_posEta__1up;
   std::vector<char>    *AnalysisJets_jet_selected_JET_EtaIntercalibration_NonClosure_posEta__1up;
   std::vector<float>   *AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_JET_EtaIntercalibration_NonClosure_posEta__1up;
   Float_t         METInfo_MET_JET_EtaIntercalibration_NonClosure_posEta__1up;
   Float_t         METInfo_METPhi_JET_EtaIntercalibration_NonClosure_posEta__1up;
   Float_t         EventInfo_jvt_effSF_JET_EtaIntercalibration_NonClosure_posEta__1down;
   std::vector<char>    *AnalysisElectrons_el_selected_JET_EtaIntercalibration_NonClosure_posEta__1down;
   std::vector<char>    *AnalysisMuons_mu_selected_JET_EtaIntercalibration_NonClosure_posEta__1down;
   std::vector<float>   *AnalysisJets_pt_JET_EtaIntercalibration_NonClosure_posEta__1down;
   std::vector<char>    *AnalysisJets_jet_selected_JET_EtaIntercalibration_NonClosure_posEta__1down;
   std::vector<float>   *AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_JET_EtaIntercalibration_NonClosure_posEta__1down;
   Float_t         METInfo_MET_JET_EtaIntercalibration_NonClosure_posEta__1down;
   Float_t         METInfo_METPhi_JET_EtaIntercalibration_NonClosure_posEta__1down;
   Float_t         EventInfo_jvt_effSF_JET_EtaIntercalibration_TotalStat__1up;
   std::vector<char>    *AnalysisElectrons_el_selected_JET_EtaIntercalibration_TotalStat__1up;
   std::vector<char>    *AnalysisMuons_mu_selected_JET_EtaIntercalibration_TotalStat__1up;
   std::vector<float>   *AnalysisJets_pt_JET_EtaIntercalibration_TotalStat__1up;
   std::vector<char>    *AnalysisJets_jet_selected_JET_EtaIntercalibration_TotalStat__1up;
   std::vector<float>   *AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_JET_EtaIntercalibration_TotalStat__1up;
   Float_t         METInfo_MET_JET_EtaIntercalibration_TotalStat__1up;
   Float_t         METInfo_METPhi_JET_EtaIntercalibration_TotalStat__1up;
   Float_t         EventInfo_jvt_effSF_JET_EtaIntercalibration_TotalStat__1down;
   std::vector<char>    *AnalysisElectrons_el_selected_JET_EtaIntercalibration_TotalStat__1down;
   std::vector<char>    *AnalysisMuons_mu_selected_JET_EtaIntercalibration_TotalStat__1down;
   std::vector<float>   *AnalysisJets_pt_JET_EtaIntercalibration_TotalStat__1down;
   std::vector<char>    *AnalysisJets_jet_selected_JET_EtaIntercalibration_TotalStat__1down;
   std::vector<float>   *AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_JET_EtaIntercalibration_TotalStat__1down;
   Float_t         METInfo_MET_JET_EtaIntercalibration_TotalStat__1down;
   Float_t         METInfo_METPhi_JET_EtaIntercalibration_TotalStat__1down;
   Float_t         EventInfo_jvt_effSF_JET_Flavor_Composition__1up;
   std::vector<char>    *AnalysisElectrons_el_selected_JET_Flavor_Composition__1up;
   std::vector<char>    *AnalysisMuons_mu_selected_JET_Flavor_Composition__1up;
   std::vector<float>   *AnalysisJets_pt_JET_Flavor_Composition__1up;
   std::vector<char>    *AnalysisJets_jet_selected_JET_Flavor_Composition__1up;
   std::vector<float>   *AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_JET_Flavor_Composition__1up;
   Float_t         METInfo_MET_JET_Flavor_Composition__1up;
   Float_t         METInfo_METPhi_JET_Flavor_Composition__1up;
   Float_t         EventInfo_jvt_effSF_JET_Flavor_Composition__1down;
   std::vector<char>    *AnalysisElectrons_el_selected_JET_Flavor_Composition__1down;
   std::vector<char>    *AnalysisMuons_mu_selected_JET_Flavor_Composition__1down;
   std::vector<float>   *AnalysisJets_pt_JET_Flavor_Composition__1down;
   std::vector<char>    *AnalysisJets_jet_selected_JET_Flavor_Composition__1down;
   std::vector<float>   *AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_JET_Flavor_Composition__1down;
   Float_t         METInfo_MET_JET_Flavor_Composition__1down;
   Float_t         METInfo_METPhi_JET_Flavor_Composition__1down;
   Float_t         EventInfo_jvt_effSF_JET_Flavor_Response__1up;
   std::vector<char>    *AnalysisElectrons_el_selected_JET_Flavor_Response__1up;
   std::vector<char>    *AnalysisMuons_mu_selected_JET_Flavor_Response__1up;
   std::vector<float>   *AnalysisJets_pt_JET_Flavor_Response__1up;
   std::vector<char>    *AnalysisJets_jet_selected_JET_Flavor_Response__1up;
   std::vector<float>   *AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_JET_Flavor_Response__1up;
   Float_t         METInfo_MET_JET_Flavor_Response__1up;
   Float_t         METInfo_METPhi_JET_Flavor_Response__1up;
   Float_t         EventInfo_jvt_effSF_JET_Flavor_Response__1down;
   std::vector<char>    *AnalysisElectrons_el_selected_JET_Flavor_Response__1down;
   std::vector<char>    *AnalysisMuons_mu_selected_JET_Flavor_Response__1down;
   std::vector<float>   *AnalysisJets_pt_JET_Flavor_Response__1down;
   std::vector<char>    *AnalysisJets_jet_selected_JET_Flavor_Response__1down;
   std::vector<float>   *AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_JET_Flavor_Response__1down;
   Float_t         METInfo_MET_JET_Flavor_Response__1down;
   Float_t         METInfo_METPhi_JET_Flavor_Response__1down;
   Float_t         EventInfo_jvt_effSF_JET_JER_DataVsMC_MC16__1up;
   std::vector<char>    *AnalysisElectrons_el_selected_JET_JER_DataVsMC_MC16__1up;
   std::vector<char>    *AnalysisMuons_mu_selected_JET_JER_DataVsMC_MC16__1up;
   std::vector<float>   *AnalysisJets_pt_JET_JER_DataVsMC_MC16__1up;
   std::vector<char>    *AnalysisJets_jet_selected_JET_JER_DataVsMC_MC16__1up;
   std::vector<float>   *AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_JET_JER_DataVsMC_MC16__1up;
   Float_t         METInfo_MET_JET_JER_DataVsMC_MC16__1up;
   Float_t         METInfo_METPhi_JET_JER_DataVsMC_MC16__1up;
   Float_t         EventInfo_jvt_effSF_JET_JER_DataVsMC_MC16__1down;
   std::vector<char>    *AnalysisElectrons_el_selected_JET_JER_DataVsMC_MC16__1down;
   std::vector<char>    *AnalysisMuons_mu_selected_JET_JER_DataVsMC_MC16__1down;
   std::vector<float>   *AnalysisJets_pt_JET_JER_DataVsMC_MC16__1down;
   std::vector<char>    *AnalysisJets_jet_selected_JET_JER_DataVsMC_MC16__1down;
   std::vector<float>   *AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_JET_JER_DataVsMC_MC16__1down;
   Float_t         METInfo_MET_JET_JER_DataVsMC_MC16__1down;
   Float_t         METInfo_METPhi_JET_JER_DataVsMC_MC16__1down;
   Float_t         EventInfo_jvt_effSF_JET_JER_EffectiveNP_1__1up;
   std::vector<char>    *AnalysisElectrons_el_selected_JET_JER_EffectiveNP_1__1up;
   std::vector<char>    *AnalysisMuons_mu_selected_JET_JER_EffectiveNP_1__1up;
   std::vector<float>   *AnalysisJets_pt_JET_JER_EffectiveNP_1__1up;
   std::vector<char>    *AnalysisJets_jet_selected_JET_JER_EffectiveNP_1__1up;
   std::vector<float>   *AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_JET_JER_EffectiveNP_1__1up;
   Float_t         METInfo_MET_JET_JER_EffectiveNP_1__1up;
   Float_t         METInfo_METPhi_JET_JER_EffectiveNP_1__1up;
   Float_t         EventInfo_jvt_effSF_JET_JER_EffectiveNP_1__1down;
   std::vector<char>    *AnalysisElectrons_el_selected_JET_JER_EffectiveNP_1__1down;
   std::vector<char>    *AnalysisMuons_mu_selected_JET_JER_EffectiveNP_1__1down;
   std::vector<float>   *AnalysisJets_pt_JET_JER_EffectiveNP_1__1down;
   std::vector<char>    *AnalysisJets_jet_selected_JET_JER_EffectiveNP_1__1down;
   std::vector<float>   *AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_JET_JER_EffectiveNP_1__1down;
   Float_t         METInfo_MET_JET_JER_EffectiveNP_1__1down;
   Float_t         METInfo_METPhi_JET_JER_EffectiveNP_1__1down;
   Float_t         EventInfo_jvt_effSF_JET_JER_EffectiveNP_2__1up;
   std::vector<char>    *AnalysisElectrons_el_selected_JET_JER_EffectiveNP_2__1up;
   std::vector<char>    *AnalysisMuons_mu_selected_JET_JER_EffectiveNP_2__1up;
   std::vector<float>   *AnalysisJets_pt_JET_JER_EffectiveNP_2__1up;
   std::vector<char>    *AnalysisJets_jet_selected_JET_JER_EffectiveNP_2__1up;
   std::vector<float>   *AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_JET_JER_EffectiveNP_2__1up;
   Float_t         METInfo_MET_JET_JER_EffectiveNP_2__1up;
   Float_t         METInfo_METPhi_JET_JER_EffectiveNP_2__1up;
   Float_t         EventInfo_jvt_effSF_JET_JER_EffectiveNP_2__1down;
   std::vector<char>    *AnalysisElectrons_el_selected_JET_JER_EffectiveNP_2__1down;
   std::vector<char>    *AnalysisMuons_mu_selected_JET_JER_EffectiveNP_2__1down;
   std::vector<float>   *AnalysisJets_pt_JET_JER_EffectiveNP_2__1down;
   std::vector<char>    *AnalysisJets_jet_selected_JET_JER_EffectiveNP_2__1down;
   std::vector<float>   *AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_JET_JER_EffectiveNP_2__1down;
   Float_t         METInfo_MET_JET_JER_EffectiveNP_2__1down;
   Float_t         METInfo_METPhi_JET_JER_EffectiveNP_2__1down;
   Float_t         EventInfo_jvt_effSF_JET_JER_EffectiveNP_3__1up;
   std::vector<char>    *AnalysisElectrons_el_selected_JET_JER_EffectiveNP_3__1up;
   std::vector<char>    *AnalysisMuons_mu_selected_JET_JER_EffectiveNP_3__1up;
   std::vector<float>   *AnalysisJets_pt_JET_JER_EffectiveNP_3__1up;
   std::vector<char>    *AnalysisJets_jet_selected_JET_JER_EffectiveNP_3__1up;
   std::vector<float>   *AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_JET_JER_EffectiveNP_3__1up;
   Float_t         METInfo_MET_JET_JER_EffectiveNP_3__1up;
   Float_t         METInfo_METPhi_JET_JER_EffectiveNP_3__1up;
   Float_t         EventInfo_jvt_effSF_JET_JER_EffectiveNP_3__1down;
   std::vector<char>    *AnalysisElectrons_el_selected_JET_JER_EffectiveNP_3__1down;
   std::vector<char>    *AnalysisMuons_mu_selected_JET_JER_EffectiveNP_3__1down;
   std::vector<float>   *AnalysisJets_pt_JET_JER_EffectiveNP_3__1down;
   std::vector<char>    *AnalysisJets_jet_selected_JET_JER_EffectiveNP_3__1down;
   std::vector<float>   *AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_JET_JER_EffectiveNP_3__1down;
   Float_t         METInfo_MET_JET_JER_EffectiveNP_3__1down;
   Float_t         METInfo_METPhi_JET_JER_EffectiveNP_3__1down;
   Float_t         EventInfo_jvt_effSF_JET_JER_EffectiveNP_4__1up;
   std::vector<char>    *AnalysisElectrons_el_selected_JET_JER_EffectiveNP_4__1up;
   std::vector<char>    *AnalysisMuons_mu_selected_JET_JER_EffectiveNP_4__1up;
   std::vector<float>   *AnalysisJets_pt_JET_JER_EffectiveNP_4__1up;
   std::vector<char>    *AnalysisJets_jet_selected_JET_JER_EffectiveNP_4__1up;
   std::vector<float>   *AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_JET_JER_EffectiveNP_4__1up;
   Float_t         METInfo_MET_JET_JER_EffectiveNP_4__1up;
   Float_t         METInfo_METPhi_JET_JER_EffectiveNP_4__1up;
   Float_t         EventInfo_jvt_effSF_JET_JER_EffectiveNP_4__1down;
   std::vector<char>    *AnalysisElectrons_el_selected_JET_JER_EffectiveNP_4__1down;
   std::vector<char>    *AnalysisMuons_mu_selected_JET_JER_EffectiveNP_4__1down;
   std::vector<float>   *AnalysisJets_pt_JET_JER_EffectiveNP_4__1down;
   std::vector<char>    *AnalysisJets_jet_selected_JET_JER_EffectiveNP_4__1down;
   std::vector<float>   *AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_JET_JER_EffectiveNP_4__1down;
   Float_t         METInfo_MET_JET_JER_EffectiveNP_4__1down;
   Float_t         METInfo_METPhi_JET_JER_EffectiveNP_4__1down;
   Float_t         EventInfo_jvt_effSF_JET_JER_EffectiveNP_5__1up;
   std::vector<char>    *AnalysisElectrons_el_selected_JET_JER_EffectiveNP_5__1up;
   std::vector<char>    *AnalysisMuons_mu_selected_JET_JER_EffectiveNP_5__1up;
   std::vector<float>   *AnalysisJets_pt_JET_JER_EffectiveNP_5__1up;
   std::vector<char>    *AnalysisJets_jet_selected_JET_JER_EffectiveNP_5__1up;
   std::vector<float>   *AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_JET_JER_EffectiveNP_5__1up;
   Float_t         METInfo_MET_JET_JER_EffectiveNP_5__1up;
   Float_t         METInfo_METPhi_JET_JER_EffectiveNP_5__1up;
   Float_t         EventInfo_jvt_effSF_JET_JER_EffectiveNP_5__1down;
   std::vector<char>    *AnalysisElectrons_el_selected_JET_JER_EffectiveNP_5__1down;
   std::vector<char>    *AnalysisMuons_mu_selected_JET_JER_EffectiveNP_5__1down;
   std::vector<float>   *AnalysisJets_pt_JET_JER_EffectiveNP_5__1down;
   std::vector<char>    *AnalysisJets_jet_selected_JET_JER_EffectiveNP_5__1down;
   std::vector<float>   *AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_JET_JER_EffectiveNP_5__1down;
   Float_t         METInfo_MET_JET_JER_EffectiveNP_5__1down;
   Float_t         METInfo_METPhi_JET_JER_EffectiveNP_5__1down;
   Float_t         EventInfo_jvt_effSF_JET_JER_EffectiveNP_6__1up;
   std::vector<char>    *AnalysisElectrons_el_selected_JET_JER_EffectiveNP_6__1up;
   std::vector<char>    *AnalysisMuons_mu_selected_JET_JER_EffectiveNP_6__1up;
   std::vector<float>   *AnalysisJets_pt_JET_JER_EffectiveNP_6__1up;
   std::vector<char>    *AnalysisJets_jet_selected_JET_JER_EffectiveNP_6__1up;
   std::vector<float>   *AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_JET_JER_EffectiveNP_6__1up;
   Float_t         METInfo_MET_JET_JER_EffectiveNP_6__1up;
   Float_t         METInfo_METPhi_JET_JER_EffectiveNP_6__1up;
   Float_t         EventInfo_jvt_effSF_JET_JER_EffectiveNP_6__1down;
   std::vector<char>    *AnalysisElectrons_el_selected_JET_JER_EffectiveNP_6__1down;
   std::vector<char>    *AnalysisMuons_mu_selected_JET_JER_EffectiveNP_6__1down;
   std::vector<float>   *AnalysisJets_pt_JET_JER_EffectiveNP_6__1down;
   std::vector<char>    *AnalysisJets_jet_selected_JET_JER_EffectiveNP_6__1down;
   std::vector<float>   *AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_JET_JER_EffectiveNP_6__1down;
   Float_t         METInfo_MET_JET_JER_EffectiveNP_6__1down;
   Float_t         METInfo_METPhi_JET_JER_EffectiveNP_6__1down;
   Float_t         EventInfo_jvt_effSF_JET_JER_EffectiveNP_7restTerm__1up;
   std::vector<char>    *AnalysisElectrons_el_selected_JET_JER_EffectiveNP_7restTerm__1up;
   std::vector<char>    *AnalysisMuons_mu_selected_JET_JER_EffectiveNP_7restTerm__1up;
   std::vector<float>   *AnalysisJets_pt_JET_JER_EffectiveNP_7restTerm__1up;
   std::vector<char>    *AnalysisJets_jet_selected_JET_JER_EffectiveNP_7restTerm__1up;
   std::vector<float>   *AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_JET_JER_EffectiveNP_7restTerm__1up;
   Float_t         METInfo_MET_JET_JER_EffectiveNP_7restTerm__1up;
   Float_t         METInfo_METPhi_JET_JER_EffectiveNP_7restTerm__1up;
   Float_t         EventInfo_jvt_effSF_JET_JER_EffectiveNP_7restTerm__1down;
   std::vector<char>    *AnalysisElectrons_el_selected_JET_JER_EffectiveNP_7restTerm__1down;
   std::vector<char>    *AnalysisMuons_mu_selected_JET_JER_EffectiveNP_7restTerm__1down;
   std::vector<float>   *AnalysisJets_pt_JET_JER_EffectiveNP_7restTerm__1down;
   std::vector<char>    *AnalysisJets_jet_selected_JET_JER_EffectiveNP_7restTerm__1down;
   std::vector<float>   *AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_JET_JER_EffectiveNP_7restTerm__1down;
   Float_t         METInfo_MET_JET_JER_EffectiveNP_7restTerm__1down;
   Float_t         METInfo_METPhi_JET_JER_EffectiveNP_7restTerm__1down;
   Float_t         EventInfo_jvt_effSF_JET_JvtEfficiency__1down;
   Float_t         EventInfo_jvt_effSF_JET_JvtEfficiency__1up;
   Float_t         EventInfo_jvt_effSF_JET_Pileup_OffsetMu__1up;
   std::vector<char>    *AnalysisElectrons_el_selected_JET_Pileup_OffsetMu__1up;
   std::vector<char>    *AnalysisMuons_mu_selected_JET_Pileup_OffsetMu__1up;
   std::vector<float>   *AnalysisJets_pt_JET_Pileup_OffsetMu__1up;
   std::vector<char>    *AnalysisJets_jet_selected_JET_Pileup_OffsetMu__1up;
   std::vector<float>   *AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_JET_Pileup_OffsetMu__1up;
   Float_t         METInfo_MET_JET_Pileup_OffsetMu__1up;
   Float_t         METInfo_METPhi_JET_Pileup_OffsetMu__1up;
   Float_t         EventInfo_jvt_effSF_JET_Pileup_OffsetMu__1down;
   std::vector<char>    *AnalysisElectrons_el_selected_JET_Pileup_OffsetMu__1down;
   std::vector<char>    *AnalysisMuons_mu_selected_JET_Pileup_OffsetMu__1down;
   std::vector<float>   *AnalysisJets_pt_JET_Pileup_OffsetMu__1down;
   std::vector<char>    *AnalysisJets_jet_selected_JET_Pileup_OffsetMu__1down;
   std::vector<float>   *AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_JET_Pileup_OffsetMu__1down;
   Float_t         METInfo_MET_JET_Pileup_OffsetMu__1down;
   Float_t         METInfo_METPhi_JET_Pileup_OffsetMu__1down;
   Float_t         EventInfo_jvt_effSF_JET_Pileup_OffsetNPV__1up;
   std::vector<char>    *AnalysisElectrons_el_selected_JET_Pileup_OffsetNPV__1up;
   std::vector<char>    *AnalysisMuons_mu_selected_JET_Pileup_OffsetNPV__1up;
   std::vector<float>   *AnalysisJets_pt_JET_Pileup_OffsetNPV__1up;
   std::vector<char>    *AnalysisJets_jet_selected_JET_Pileup_OffsetNPV__1up;
   std::vector<float>   *AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_JET_Pileup_OffsetNPV__1up;
   Float_t         METInfo_MET_JET_Pileup_OffsetNPV__1up;
   Float_t         METInfo_METPhi_JET_Pileup_OffsetNPV__1up;
   Float_t         EventInfo_jvt_effSF_JET_Pileup_OffsetNPV__1down;
   std::vector<char>    *AnalysisElectrons_el_selected_JET_Pileup_OffsetNPV__1down;
   std::vector<char>    *AnalysisMuons_mu_selected_JET_Pileup_OffsetNPV__1down;
   std::vector<float>   *AnalysisJets_pt_JET_Pileup_OffsetNPV__1down;
   std::vector<char>    *AnalysisJets_jet_selected_JET_Pileup_OffsetNPV__1down;
   std::vector<float>   *AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_JET_Pileup_OffsetNPV__1down;
   Float_t         METInfo_MET_JET_Pileup_OffsetNPV__1down;
   Float_t         METInfo_METPhi_JET_Pileup_OffsetNPV__1down;
   Float_t         EventInfo_jvt_effSF_JET_Pileup_PtTerm__1up;
   std::vector<char>    *AnalysisElectrons_el_selected_JET_Pileup_PtTerm__1up;
   std::vector<char>    *AnalysisMuons_mu_selected_JET_Pileup_PtTerm__1up;
   std::vector<float>   *AnalysisJets_pt_JET_Pileup_PtTerm__1up;
   std::vector<char>    *AnalysisJets_jet_selected_JET_Pileup_PtTerm__1up;
   std::vector<float>   *AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_JET_Pileup_PtTerm__1up;
   Float_t         METInfo_MET_JET_Pileup_PtTerm__1up;
   Float_t         METInfo_METPhi_JET_Pileup_PtTerm__1up;
   Float_t         EventInfo_jvt_effSF_JET_Pileup_PtTerm__1down;
   std::vector<char>    *AnalysisElectrons_el_selected_JET_Pileup_PtTerm__1down;
   std::vector<char>    *AnalysisMuons_mu_selected_JET_Pileup_PtTerm__1down;
   std::vector<float>   *AnalysisJets_pt_JET_Pileup_PtTerm__1down;
   std::vector<char>    *AnalysisJets_jet_selected_JET_Pileup_PtTerm__1down;
   std::vector<float>   *AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_JET_Pileup_PtTerm__1down;
   Float_t         METInfo_MET_JET_Pileup_PtTerm__1down;
   Float_t         METInfo_METPhi_JET_Pileup_PtTerm__1down;
   Float_t         EventInfo_jvt_effSF_JET_Pileup_RhoTopology__1up;
   std::vector<char>    *AnalysisElectrons_el_selected_JET_Pileup_RhoTopology__1up;
   std::vector<char>    *AnalysisMuons_mu_selected_JET_Pileup_RhoTopology__1up;
   std::vector<float>   *AnalysisJets_pt_JET_Pileup_RhoTopology__1up;
   std::vector<char>    *AnalysisJets_jet_selected_JET_Pileup_RhoTopology__1up;
   std::vector<float>   *AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_JET_Pileup_RhoTopology__1up;
   Float_t         METInfo_MET_JET_Pileup_RhoTopology__1up;
   Float_t         METInfo_METPhi_JET_Pileup_RhoTopology__1up;
   Float_t         EventInfo_jvt_effSF_JET_Pileup_RhoTopology__1down;
   std::vector<char>    *AnalysisElectrons_el_selected_JET_Pileup_RhoTopology__1down;
   std::vector<char>    *AnalysisMuons_mu_selected_JET_Pileup_RhoTopology__1down;
   std::vector<float>   *AnalysisJets_pt_JET_Pileup_RhoTopology__1down;
   std::vector<char>    *AnalysisJets_jet_selected_JET_Pileup_RhoTopology__1down;
   std::vector<float>   *AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_JET_Pileup_RhoTopology__1down;
   Float_t         METInfo_MET_JET_Pileup_RhoTopology__1down;
   Float_t         METInfo_METPhi_JET_Pileup_RhoTopology__1down;
   Float_t         EventInfo_jvt_effSF_JET_PunchThrough_MC16__1up;
   std::vector<char>    *AnalysisElectrons_el_selected_JET_PunchThrough_MC16__1up;
   std::vector<char>    *AnalysisMuons_mu_selected_JET_PunchThrough_MC16__1up;
   std::vector<float>   *AnalysisJets_pt_JET_PunchThrough_MC16__1up;
   std::vector<char>    *AnalysisJets_jet_selected_JET_PunchThrough_MC16__1up;
   std::vector<float>   *AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_JET_PunchThrough_MC16__1up;
   Float_t         METInfo_MET_JET_PunchThrough_MC16__1up;
   Float_t         METInfo_METPhi_JET_PunchThrough_MC16__1up;
   Float_t         EventInfo_jvt_effSF_JET_PunchThrough_MC16__1down;
   std::vector<char>    *AnalysisElectrons_el_selected_JET_PunchThrough_MC16__1down;
   std::vector<char>    *AnalysisMuons_mu_selected_JET_PunchThrough_MC16__1down;
   std::vector<float>   *AnalysisJets_pt_JET_PunchThrough_MC16__1down;
   std::vector<char>    *AnalysisJets_jet_selected_JET_PunchThrough_MC16__1down;
   std::vector<float>   *AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_JET_PunchThrough_MC16__1down;
   Float_t         METInfo_MET_JET_PunchThrough_MC16__1down;
   Float_t         METInfo_METPhi_JET_PunchThrough_MC16__1down;
   Float_t         METInfo_MET_MET_SoftTrk_ResoPara;
   Float_t         METInfo_METPhi_MET_SoftTrk_ResoPara;
   Float_t         METInfo_MET_MET_SoftTrk_ResoPerp;
   Float_t         METInfo_METPhi_MET_SoftTrk_ResoPerp;
   Float_t         METInfo_MET_MET_SoftTrk_Scale__1down;
   Float_t         METInfo_METPhi_MET_SoftTrk_Scale__1down;
   Float_t         METInfo_MET_MET_SoftTrk_Scale__1up;
   Float_t         METInfo_METPhi_MET_SoftTrk_Scale__1up;
   std::vector<char>    *AnalysisElectrons_el_selected_MUON_CB__1down;
   std::vector<float>   *AnalysisMuons_pt_MUON_CB__1down;
   std::vector<char>    *AnalysisMuons_mu_selected_MUON_CB__1down;
   std::vector<char>    *AnalysisMuons_isQuality_Loose_MUON_CB__1down;
   std::vector<char>    *AnalysisMuons_isQuality_Medium_MUON_CB__1down;
   std::vector<char>    *AnalysisMuons_isQuality_Tight_MUON_CB__1down;
   std::vector<char>    *AnalysisMuons_isIsolated_PflowTight_VarRad_MUON_CB__1down;
   std::vector<float>   *AnalysisMuons_muon_effSF_MUON_CB__1down;
   std::vector<float>   *AnalysisMuons_muon_effSF_TTVA_MUON_CB__1down;
   std::vector<float>   *AnalysisMuons_muon_effSF_Quality_Loose_MUON_CB__1down;
   std::vector<float>   *AnalysisMuons_muon_effSF_Quality_Medium_MUON_CB__1down;
   std::vector<float>   *AnalysisMuons_muon_effSF_Quality_Tight_MUON_CB__1down;
   std::vector<float>   *AnalysisMuons_muon_effSF_Isol_PflowTight_VarRad_MUON_CB__1down;
   std::vector<float>   *AnalysisMuons_muon_effMC_Trig_Loose_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_CB__1down;
   std::vector<float>   *AnalysisMuons_muon_effData_Trig_Loose_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_CB__1down;
   std::vector<float>   *AnalysisMuons_muon_effMC_Trig_Loose_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_CB__1down;
   std::vector<float>   *AnalysisMuons_muon_effData_Trig_Loose_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_CB__1down;
   std::vector<float>   *AnalysisMuons_muon_effMC_Trig_Medium_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_CB__1down;
   std::vector<float>   *AnalysisMuons_muon_effData_Trig_Medium_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_CB__1down;
   std::vector<float>   *AnalysisMuons_muon_effMC_Trig_Medium_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_CB__1down;
   std::vector<float>   *AnalysisMuons_muon_effData_Trig_Medium_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_CB__1down;
   std::vector<float>   *AnalysisMuons_muon_effMC_Trig_Tight_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_CB__1down;
   std::vector<float>   *AnalysisMuons_muon_effData_Trig_Tight_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_CB__1down;
   std::vector<float>   *AnalysisMuons_muon_effMC_Trig_Tight_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_CB__1down;
   std::vector<float>   *AnalysisMuons_muon_effData_Trig_Tight_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_CB__1down;
   std::vector<char>    *AnalysisJets_jet_selected_MUON_CB__1down;
   Float_t         METInfo_MET_MUON_CB__1down;
   Float_t         METInfo_METPhi_MUON_CB__1down;
   std::vector<char>    *AnalysisElectrons_el_selected_MUON_CB__1up;
   std::vector<float>   *AnalysisMuons_pt_MUON_CB__1up;
   std::vector<char>    *AnalysisMuons_mu_selected_MUON_CB__1up;
   std::vector<char>    *AnalysisMuons_isQuality_Loose_MUON_CB__1up;
   std::vector<char>    *AnalysisMuons_isQuality_Medium_MUON_CB__1up;
   std::vector<char>    *AnalysisMuons_isQuality_Tight_MUON_CB__1up;
   std::vector<char>    *AnalysisMuons_isIsolated_PflowTight_VarRad_MUON_CB__1up;
   std::vector<float>   *AnalysisMuons_muon_effSF_MUON_CB__1up;
   std::vector<float>   *AnalysisMuons_muon_effSF_TTVA_MUON_CB__1up;
   std::vector<float>   *AnalysisMuons_muon_effSF_Quality_Loose_MUON_CB__1up;
   std::vector<float>   *AnalysisMuons_muon_effSF_Quality_Medium_MUON_CB__1up;
   std::vector<float>   *AnalysisMuons_muon_effSF_Quality_Tight_MUON_CB__1up;
   std::vector<float>   *AnalysisMuons_muon_effSF_Isol_PflowTight_VarRad_MUON_CB__1up;
   std::vector<float>   *AnalysisMuons_muon_effMC_Trig_Loose_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_CB__1up;
   std::vector<float>   *AnalysisMuons_muon_effData_Trig_Loose_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_CB__1up;
   std::vector<float>   *AnalysisMuons_muon_effMC_Trig_Loose_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_CB__1up;
   std::vector<float>   *AnalysisMuons_muon_effData_Trig_Loose_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_CB__1up;
   std::vector<float>   *AnalysisMuons_muon_effMC_Trig_Medium_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_CB__1up;
   std::vector<float>   *AnalysisMuons_muon_effData_Trig_Medium_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_CB__1up;
   std::vector<float>   *AnalysisMuons_muon_effMC_Trig_Medium_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_CB__1up;
   std::vector<float>   *AnalysisMuons_muon_effData_Trig_Medium_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_CB__1up;
   std::vector<float>   *AnalysisMuons_muon_effMC_Trig_Tight_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_CB__1up;
   std::vector<float>   *AnalysisMuons_muon_effData_Trig_Tight_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_CB__1up;
   std::vector<float>   *AnalysisMuons_muon_effMC_Trig_Tight_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_CB__1up;
   std::vector<float>   *AnalysisMuons_muon_effData_Trig_Tight_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_CB__1up;
   std::vector<char>    *AnalysisJets_jet_selected_MUON_CB__1up;
   Float_t         METInfo_MET_MUON_CB__1up;
   Float_t         METInfo_METPhi_MUON_CB__1up;
   std::vector<float>   *AnalysisMuons_muon_effSF_Isol_PflowTight_VarRad_MUON_EFF_ISO_STAT__1down;
   std::vector<float>   *AnalysisMuons_muon_effSF_Isol_PflowTight_VarRad_MUON_EFF_ISO_STAT__1up;
   std::vector<float>   *AnalysisMuons_muon_effSF_Isol_PflowTight_VarRad_MUON_EFF_ISO_SYS__1down;
   std::vector<float>   *AnalysisMuons_muon_effSF_Isol_PflowTight_VarRad_MUON_EFF_ISO_SYS__1up;
   std::vector<float>   *AnalysisMuons_muon_effSF_MUON_EFF_RECO_STAT__1down;
   std::vector<float>   *AnalysisMuons_muon_effSF_Quality_Loose_MUON_EFF_RECO_STAT__1down;
   std::vector<float>   *AnalysisMuons_muon_effSF_Quality_Medium_MUON_EFF_RECO_STAT__1down;
   std::vector<float>   *AnalysisMuons_muon_effSF_Quality_Tight_MUON_EFF_RECO_STAT__1down;
   std::vector<float>   *AnalysisMuons_muon_effSF_MUON_EFF_RECO_STAT__1up;
   std::vector<float>   *AnalysisMuons_muon_effSF_Quality_Loose_MUON_EFF_RECO_STAT__1up;
   std::vector<float>   *AnalysisMuons_muon_effSF_Quality_Medium_MUON_EFF_RECO_STAT__1up;
   std::vector<float>   *AnalysisMuons_muon_effSF_Quality_Tight_MUON_EFF_RECO_STAT__1up;
   std::vector<float>   *AnalysisMuons_muon_effSF_MUON_EFF_RECO_STAT_LOWPT__1down;
   std::vector<float>   *AnalysisMuons_muon_effSF_Quality_Loose_MUON_EFF_RECO_STAT_LOWPT__1down;
   std::vector<float>   *AnalysisMuons_muon_effSF_Quality_Medium_MUON_EFF_RECO_STAT_LOWPT__1down;
   std::vector<float>   *AnalysisMuons_muon_effSF_Quality_Tight_MUON_EFF_RECO_STAT_LOWPT__1down;
   std::vector<float>   *AnalysisMuons_muon_effSF_MUON_EFF_RECO_STAT_LOWPT__1up;
   std::vector<float>   *AnalysisMuons_muon_effSF_Quality_Loose_MUON_EFF_RECO_STAT_LOWPT__1up;
   std::vector<float>   *AnalysisMuons_muon_effSF_Quality_Medium_MUON_EFF_RECO_STAT_LOWPT__1up;
   std::vector<float>   *AnalysisMuons_muon_effSF_Quality_Tight_MUON_EFF_RECO_STAT_LOWPT__1up;
   std::vector<float>   *AnalysisMuons_muon_effSF_MUON_EFF_RECO_SYS__1down;
   std::vector<float>   *AnalysisMuons_muon_effSF_Quality_Loose_MUON_EFF_RECO_SYS__1down;
   std::vector<float>   *AnalysisMuons_muon_effSF_Quality_Medium_MUON_EFF_RECO_SYS__1down;
   std::vector<float>   *AnalysisMuons_muon_effSF_Quality_Tight_MUON_EFF_RECO_SYS__1down;
   std::vector<float>   *AnalysisMuons_muon_effSF_MUON_EFF_RECO_SYS__1up;
   std::vector<float>   *AnalysisMuons_muon_effSF_Quality_Loose_MUON_EFF_RECO_SYS__1up;
   std::vector<float>   *AnalysisMuons_muon_effSF_Quality_Medium_MUON_EFF_RECO_SYS__1up;
   std::vector<float>   *AnalysisMuons_muon_effSF_Quality_Tight_MUON_EFF_RECO_SYS__1up;
   std::vector<float>   *AnalysisMuons_muon_effSF_MUON_EFF_RECO_SYS_LOWPT__1down;
   std::vector<float>   *AnalysisMuons_muon_effSF_Quality_Loose_MUON_EFF_RECO_SYS_LOWPT__1down;
   std::vector<float>   *AnalysisMuons_muon_effSF_Quality_Medium_MUON_EFF_RECO_SYS_LOWPT__1down;
   std::vector<float>   *AnalysisMuons_muon_effSF_Quality_Tight_MUON_EFF_RECO_SYS_LOWPT__1down;
   std::vector<float>   *AnalysisMuons_muon_effSF_MUON_EFF_RECO_SYS_LOWPT__1up;
   std::vector<float>   *AnalysisMuons_muon_effSF_Quality_Loose_MUON_EFF_RECO_SYS_LOWPT__1up;
   std::vector<float>   *AnalysisMuons_muon_effSF_Quality_Medium_MUON_EFF_RECO_SYS_LOWPT__1up;
   std::vector<float>   *AnalysisMuons_muon_effSF_Quality_Tight_MUON_EFF_RECO_SYS_LOWPT__1up;
   std::vector<float>   *AnalysisMuons_muon_effSF_TTVA_MUON_EFF_TTVA_STAT__1down;
   std::vector<float>   *AnalysisMuons_muon_effSF_TTVA_MUON_EFF_TTVA_STAT__1up;
   std::vector<float>   *AnalysisMuons_muon_effSF_TTVA_MUON_EFF_TTVA_SYS__1down;
   std::vector<float>   *AnalysisMuons_muon_effSF_TTVA_MUON_EFF_TTVA_SYS__1up;
   std::vector<float>   *AnalysisMuons_muon_effMC_Trig_Loose_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_EFF_TrigStatUncertainty__1down;
   std::vector<float>   *AnalysisMuons_muon_effData_Trig_Loose_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_EFF_TrigStatUncertainty__1down;
   std::vector<float>   *AnalysisMuons_muon_effMC_Trig_Loose_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_EFF_TrigStatUncertainty__1down;
   std::vector<float>   *AnalysisMuons_muon_effData_Trig_Loose_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_EFF_TrigStatUncertainty__1down;
   std::vector<float>   *AnalysisMuons_muon_effMC_Trig_Medium_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_EFF_TrigStatUncertainty__1down;
   std::vector<float>   *AnalysisMuons_muon_effData_Trig_Medium_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_EFF_TrigStatUncertainty__1down;
   std::vector<float>   *AnalysisMuons_muon_effMC_Trig_Medium_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_EFF_TrigStatUncertainty__1down;
   std::vector<float>   *AnalysisMuons_muon_effData_Trig_Medium_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_EFF_TrigStatUncertainty__1down;
   std::vector<float>   *AnalysisMuons_muon_effMC_Trig_Tight_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_EFF_TrigStatUncertainty__1down;
   std::vector<float>   *AnalysisMuons_muon_effData_Trig_Tight_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_EFF_TrigStatUncertainty__1down;
   std::vector<float>   *AnalysisMuons_muon_effMC_Trig_Tight_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_EFF_TrigStatUncertainty__1down;
   std::vector<float>   *AnalysisMuons_muon_effData_Trig_Tight_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_EFF_TrigStatUncertainty__1down;
   std::vector<float>   *AnalysisMuons_muon_effMC_Trig_Loose_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_EFF_TrigStatUncertainty__1up;
   std::vector<float>   *AnalysisMuons_muon_effData_Trig_Loose_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_EFF_TrigStatUncertainty__1up;
   std::vector<float>   *AnalysisMuons_muon_effMC_Trig_Loose_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_EFF_TrigStatUncertainty__1up;
   std::vector<float>   *AnalysisMuons_muon_effData_Trig_Loose_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_EFF_TrigStatUncertainty__1up;
   std::vector<float>   *AnalysisMuons_muon_effMC_Trig_Medium_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_EFF_TrigStatUncertainty__1up;
   std::vector<float>   *AnalysisMuons_muon_effData_Trig_Medium_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_EFF_TrigStatUncertainty__1up;
   std::vector<float>   *AnalysisMuons_muon_effMC_Trig_Medium_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_EFF_TrigStatUncertainty__1up;
   std::vector<float>   *AnalysisMuons_muon_effData_Trig_Medium_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_EFF_TrigStatUncertainty__1up;
   std::vector<float>   *AnalysisMuons_muon_effMC_Trig_Tight_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_EFF_TrigStatUncertainty__1up;
   std::vector<float>   *AnalysisMuons_muon_effData_Trig_Tight_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_EFF_TrigStatUncertainty__1up;
   std::vector<float>   *AnalysisMuons_muon_effMC_Trig_Tight_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_EFF_TrigStatUncertainty__1up;
   std::vector<float>   *AnalysisMuons_muon_effData_Trig_Tight_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_EFF_TrigStatUncertainty__1up;
   std::vector<float>   *AnalysisMuons_muon_effMC_Trig_Loose_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_EFF_TrigSystUncertainty__1down;
   std::vector<float>   *AnalysisMuons_muon_effData_Trig_Loose_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_EFF_TrigSystUncertainty__1down;
   std::vector<float>   *AnalysisMuons_muon_effMC_Trig_Loose_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_EFF_TrigSystUncertainty__1down;
   std::vector<float>   *AnalysisMuons_muon_effData_Trig_Loose_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_EFF_TrigSystUncertainty__1down;
   std::vector<float>   *AnalysisMuons_muon_effMC_Trig_Medium_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_EFF_TrigSystUncertainty__1down;
   std::vector<float>   *AnalysisMuons_muon_effData_Trig_Medium_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_EFF_TrigSystUncertainty__1down;
   std::vector<float>   *AnalysisMuons_muon_effMC_Trig_Medium_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_EFF_TrigSystUncertainty__1down;
   std::vector<float>   *AnalysisMuons_muon_effData_Trig_Medium_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_EFF_TrigSystUncertainty__1down;
   std::vector<float>   *AnalysisMuons_muon_effMC_Trig_Tight_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_EFF_TrigSystUncertainty__1down;
   std::vector<float>   *AnalysisMuons_muon_effData_Trig_Tight_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_EFF_TrigSystUncertainty__1down;
   std::vector<float>   *AnalysisMuons_muon_effMC_Trig_Tight_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_EFF_TrigSystUncertainty__1down;
   std::vector<float>   *AnalysisMuons_muon_effData_Trig_Tight_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_EFF_TrigSystUncertainty__1down;
   std::vector<float>   *AnalysisMuons_muon_effMC_Trig_Loose_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_EFF_TrigSystUncertainty__1up;
   std::vector<float>   *AnalysisMuons_muon_effData_Trig_Loose_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_EFF_TrigSystUncertainty__1up;
   std::vector<float>   *AnalysisMuons_muon_effMC_Trig_Loose_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_EFF_TrigSystUncertainty__1up;
   std::vector<float>   *AnalysisMuons_muon_effData_Trig_Loose_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_EFF_TrigSystUncertainty__1up;
   std::vector<float>   *AnalysisMuons_muon_effMC_Trig_Medium_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_EFF_TrigSystUncertainty__1up;
   std::vector<float>   *AnalysisMuons_muon_effData_Trig_Medium_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_EFF_TrigSystUncertainty__1up;
   std::vector<float>   *AnalysisMuons_muon_effMC_Trig_Medium_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_EFF_TrigSystUncertainty__1up;
   std::vector<float>   *AnalysisMuons_muon_effData_Trig_Medium_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_EFF_TrigSystUncertainty__1up;
   std::vector<float>   *AnalysisMuons_muon_effMC_Trig_Tight_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_EFF_TrigSystUncertainty__1up;
   std::vector<float>   *AnalysisMuons_muon_effData_Trig_Tight_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_EFF_TrigSystUncertainty__1up;
   std::vector<float>   *AnalysisMuons_muon_effMC_Trig_Tight_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_EFF_TrigSystUncertainty__1up;
   std::vector<float>   *AnalysisMuons_muon_effData_Trig_Tight_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_EFF_TrigSystUncertainty__1up;
   std::vector<char>    *AnalysisElectrons_el_selected_MUON_SAGITTA_DATASTAT__1down;
   std::vector<float>   *AnalysisMuons_pt_MUON_SAGITTA_DATASTAT__1down;
   std::vector<char>    *AnalysisMuons_mu_selected_MUON_SAGITTA_DATASTAT__1down;
   std::vector<char>    *AnalysisMuons_isQuality_Loose_MUON_SAGITTA_DATASTAT__1down;
   std::vector<char>    *AnalysisMuons_isQuality_Medium_MUON_SAGITTA_DATASTAT__1down;
   std::vector<char>    *AnalysisMuons_isQuality_Tight_MUON_SAGITTA_DATASTAT__1down;
   std::vector<char>    *AnalysisMuons_isIsolated_PflowTight_VarRad_MUON_SAGITTA_DATASTAT__1down;
   std::vector<float>   *AnalysisMuons_muon_effSF_MUON_SAGITTA_DATASTAT__1down;
   std::vector<float>   *AnalysisMuons_muon_effSF_TTVA_MUON_SAGITTA_DATASTAT__1down;
   std::vector<float>   *AnalysisMuons_muon_effSF_Quality_Loose_MUON_SAGITTA_DATASTAT__1down;
   std::vector<float>   *AnalysisMuons_muon_effSF_Quality_Medium_MUON_SAGITTA_DATASTAT__1down;
   std::vector<float>   *AnalysisMuons_muon_effSF_Quality_Tight_MUON_SAGITTA_DATASTAT__1down;
   std::vector<float>   *AnalysisMuons_muon_effSF_Isol_PflowTight_VarRad_MUON_SAGITTA_DATASTAT__1down;
   std::vector<float>   *AnalysisMuons_muon_effMC_Trig_Loose_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_SAGITTA_DATASTAT__1down;
   std::vector<float>   *AnalysisMuons_muon_effData_Trig_Loose_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_SAGITTA_DATASTAT__1down;
   std::vector<float>   *AnalysisMuons_muon_effMC_Trig_Loose_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_SAGITTA_DATASTAT__1down;
   std::vector<float>   *AnalysisMuons_muon_effData_Trig_Loose_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_SAGITTA_DATASTAT__1down;
   std::vector<float>   *AnalysisMuons_muon_effMC_Trig_Medium_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_SAGITTA_DATASTAT__1down;
   std::vector<float>   *AnalysisMuons_muon_effData_Trig_Medium_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_SAGITTA_DATASTAT__1down;
   std::vector<float>   *AnalysisMuons_muon_effMC_Trig_Medium_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_SAGITTA_DATASTAT__1down;
   std::vector<float>   *AnalysisMuons_muon_effData_Trig_Medium_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_SAGITTA_DATASTAT__1down;
   std::vector<float>   *AnalysisMuons_muon_effMC_Trig_Tight_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_SAGITTA_DATASTAT__1down;
   std::vector<float>   *AnalysisMuons_muon_effData_Trig_Tight_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_SAGITTA_DATASTAT__1down;
   std::vector<float>   *AnalysisMuons_muon_effMC_Trig_Tight_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_SAGITTA_DATASTAT__1down;
   std::vector<float>   *AnalysisMuons_muon_effData_Trig_Tight_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_SAGITTA_DATASTAT__1down;
   std::vector<char>    *AnalysisJets_jet_selected_MUON_SAGITTA_DATASTAT__1down;
   Float_t         METInfo_MET_MUON_SAGITTA_DATASTAT__1down;
   Float_t         METInfo_METPhi_MUON_SAGITTA_DATASTAT__1down;
   std::vector<char>    *AnalysisElectrons_el_selected_MUON_SAGITTA_DATASTAT__1up;
   std::vector<float>   *AnalysisMuons_pt_MUON_SAGITTA_DATASTAT__1up;
   std::vector<char>    *AnalysisMuons_mu_selected_MUON_SAGITTA_DATASTAT__1up;
   std::vector<char>    *AnalysisMuons_isQuality_Loose_MUON_SAGITTA_DATASTAT__1up;
   std::vector<char>    *AnalysisMuons_isQuality_Medium_MUON_SAGITTA_DATASTAT__1up;
   std::vector<char>    *AnalysisMuons_isQuality_Tight_MUON_SAGITTA_DATASTAT__1up;
   std::vector<char>    *AnalysisMuons_isIsolated_PflowTight_VarRad_MUON_SAGITTA_DATASTAT__1up;
   std::vector<float>   *AnalysisMuons_muon_effSF_MUON_SAGITTA_DATASTAT__1up;
   std::vector<float>   *AnalysisMuons_muon_effSF_TTVA_MUON_SAGITTA_DATASTAT__1up;
   std::vector<float>   *AnalysisMuons_muon_effSF_Quality_Loose_MUON_SAGITTA_DATASTAT__1up;
   std::vector<float>   *AnalysisMuons_muon_effSF_Quality_Medium_MUON_SAGITTA_DATASTAT__1up;
   std::vector<float>   *AnalysisMuons_muon_effSF_Quality_Tight_MUON_SAGITTA_DATASTAT__1up;
   std::vector<float>   *AnalysisMuons_muon_effSF_Isol_PflowTight_VarRad_MUON_SAGITTA_DATASTAT__1up;
   std::vector<float>   *AnalysisMuons_muon_effMC_Trig_Loose_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_SAGITTA_DATASTAT__1up;
   std::vector<float>   *AnalysisMuons_muon_effData_Trig_Loose_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_SAGITTA_DATASTAT__1up;
   std::vector<float>   *AnalysisMuons_muon_effMC_Trig_Loose_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_SAGITTA_DATASTAT__1up;
   std::vector<float>   *AnalysisMuons_muon_effData_Trig_Loose_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_SAGITTA_DATASTAT__1up;
   std::vector<float>   *AnalysisMuons_muon_effMC_Trig_Medium_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_SAGITTA_DATASTAT__1up;
   std::vector<float>   *AnalysisMuons_muon_effData_Trig_Medium_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_SAGITTA_DATASTAT__1up;
   std::vector<float>   *AnalysisMuons_muon_effMC_Trig_Medium_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_SAGITTA_DATASTAT__1up;
   std::vector<float>   *AnalysisMuons_muon_effData_Trig_Medium_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_SAGITTA_DATASTAT__1up;
   std::vector<float>   *AnalysisMuons_muon_effMC_Trig_Tight_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_SAGITTA_DATASTAT__1up;
   std::vector<float>   *AnalysisMuons_muon_effData_Trig_Tight_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_SAGITTA_DATASTAT__1up;
   std::vector<float>   *AnalysisMuons_muon_effMC_Trig_Tight_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_SAGITTA_DATASTAT__1up;
   std::vector<float>   *AnalysisMuons_muon_effData_Trig_Tight_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_SAGITTA_DATASTAT__1up;
   std::vector<char>    *AnalysisJets_jet_selected_MUON_SAGITTA_DATASTAT__1up;
   Float_t         METInfo_MET_MUON_SAGITTA_DATASTAT__1up;
   Float_t         METInfo_METPhi_MUON_SAGITTA_DATASTAT__1up;
   std::vector<char>    *AnalysisElectrons_el_selected_MUON_SAGITTA_RESBIAS__1down;
   std::vector<float>   *AnalysisMuons_pt_MUON_SAGITTA_RESBIAS__1down;
   std::vector<char>    *AnalysisMuons_mu_selected_MUON_SAGITTA_RESBIAS__1down;
   std::vector<char>    *AnalysisMuons_isQuality_Loose_MUON_SAGITTA_RESBIAS__1down;
   std::vector<char>    *AnalysisMuons_isQuality_Medium_MUON_SAGITTA_RESBIAS__1down;
   std::vector<char>    *AnalysisMuons_isQuality_Tight_MUON_SAGITTA_RESBIAS__1down;
   std::vector<char>    *AnalysisMuons_isIsolated_PflowTight_VarRad_MUON_SAGITTA_RESBIAS__1down;
   std::vector<float>   *AnalysisMuons_muon_effSF_MUON_SAGITTA_RESBIAS__1down;
   std::vector<float>   *AnalysisMuons_muon_effSF_TTVA_MUON_SAGITTA_RESBIAS__1down;
   std::vector<float>   *AnalysisMuons_muon_effSF_Quality_Loose_MUON_SAGITTA_RESBIAS__1down;
   std::vector<float>   *AnalysisMuons_muon_effSF_Quality_Medium_MUON_SAGITTA_RESBIAS__1down;
   std::vector<float>   *AnalysisMuons_muon_effSF_Quality_Tight_MUON_SAGITTA_RESBIAS__1down;
   std::vector<float>   *AnalysisMuons_muon_effSF_Isol_PflowTight_VarRad_MUON_SAGITTA_RESBIAS__1down;
   std::vector<float>   *AnalysisMuons_muon_effMC_Trig_Loose_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_SAGITTA_RESBIAS__1down;
   std::vector<float>   *AnalysisMuons_muon_effData_Trig_Loose_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_SAGITTA_RESBIAS__1down;
   std::vector<float>   *AnalysisMuons_muon_effMC_Trig_Loose_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_SAGITTA_RESBIAS__1down;
   std::vector<float>   *AnalysisMuons_muon_effData_Trig_Loose_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_SAGITTA_RESBIAS__1down;
   std::vector<float>   *AnalysisMuons_muon_effMC_Trig_Medium_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_SAGITTA_RESBIAS__1down;
   std::vector<float>   *AnalysisMuons_muon_effData_Trig_Medium_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_SAGITTA_RESBIAS__1down;
   std::vector<float>   *AnalysisMuons_muon_effMC_Trig_Medium_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_SAGITTA_RESBIAS__1down;
   std::vector<float>   *AnalysisMuons_muon_effData_Trig_Medium_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_SAGITTA_RESBIAS__1down;
   std::vector<float>   *AnalysisMuons_muon_effMC_Trig_Tight_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_SAGITTA_RESBIAS__1down;
   std::vector<float>   *AnalysisMuons_muon_effData_Trig_Tight_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_SAGITTA_RESBIAS__1down;
   std::vector<float>   *AnalysisMuons_muon_effMC_Trig_Tight_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_SAGITTA_RESBIAS__1down;
   std::vector<float>   *AnalysisMuons_muon_effData_Trig_Tight_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_SAGITTA_RESBIAS__1down;
   std::vector<char>    *AnalysisJets_jet_selected_MUON_SAGITTA_RESBIAS__1down;
   Float_t         METInfo_MET_MUON_SAGITTA_RESBIAS__1down;
   Float_t         METInfo_METPhi_MUON_SAGITTA_RESBIAS__1down;
   std::vector<char>    *AnalysisElectrons_el_selected_MUON_SAGITTA_RESBIAS__1up;
   std::vector<float>   *AnalysisMuons_pt_MUON_SAGITTA_RESBIAS__1up;
   std::vector<char>    *AnalysisMuons_mu_selected_MUON_SAGITTA_RESBIAS__1up;
   std::vector<char>    *AnalysisMuons_isQuality_Loose_MUON_SAGITTA_RESBIAS__1up;
   std::vector<char>    *AnalysisMuons_isQuality_Medium_MUON_SAGITTA_RESBIAS__1up;
   std::vector<char>    *AnalysisMuons_isQuality_Tight_MUON_SAGITTA_RESBIAS__1up;
   std::vector<char>    *AnalysisMuons_isIsolated_PflowTight_VarRad_MUON_SAGITTA_RESBIAS__1up;
   std::vector<float>   *AnalysisMuons_muon_effSF_MUON_SAGITTA_RESBIAS__1up;
   std::vector<float>   *AnalysisMuons_muon_effSF_TTVA_MUON_SAGITTA_RESBIAS__1up;
   std::vector<float>   *AnalysisMuons_muon_effSF_Quality_Loose_MUON_SAGITTA_RESBIAS__1up;
   std::vector<float>   *AnalysisMuons_muon_effSF_Quality_Medium_MUON_SAGITTA_RESBIAS__1up;
   std::vector<float>   *AnalysisMuons_muon_effSF_Quality_Tight_MUON_SAGITTA_RESBIAS__1up;
   std::vector<float>   *AnalysisMuons_muon_effSF_Isol_PflowTight_VarRad_MUON_SAGITTA_RESBIAS__1up;
   std::vector<float>   *AnalysisMuons_muon_effMC_Trig_Loose_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_SAGITTA_RESBIAS__1up;
   std::vector<float>   *AnalysisMuons_muon_effData_Trig_Loose_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_SAGITTA_RESBIAS__1up;
   std::vector<float>   *AnalysisMuons_muon_effMC_Trig_Loose_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_SAGITTA_RESBIAS__1up;
   std::vector<float>   *AnalysisMuons_muon_effData_Trig_Loose_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_SAGITTA_RESBIAS__1up;
   std::vector<float>   *AnalysisMuons_muon_effMC_Trig_Medium_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_SAGITTA_RESBIAS__1up;
   std::vector<float>   *AnalysisMuons_muon_effData_Trig_Medium_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_SAGITTA_RESBIAS__1up;
   std::vector<float>   *AnalysisMuons_muon_effMC_Trig_Medium_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_SAGITTA_RESBIAS__1up;
   std::vector<float>   *AnalysisMuons_muon_effData_Trig_Medium_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_SAGITTA_RESBIAS__1up;
   std::vector<float>   *AnalysisMuons_muon_effMC_Trig_Tight_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_SAGITTA_RESBIAS__1up;
   std::vector<float>   *AnalysisMuons_muon_effData_Trig_Tight_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_SAGITTA_RESBIAS__1up;
   std::vector<float>   *AnalysisMuons_muon_effMC_Trig_Tight_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_SAGITTA_RESBIAS__1up;
   std::vector<float>   *AnalysisMuons_muon_effData_Trig_Tight_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_SAGITTA_RESBIAS__1up;
   std::vector<char>    *AnalysisJets_jet_selected_MUON_SAGITTA_RESBIAS__1up;
   Float_t         METInfo_MET_MUON_SAGITTA_RESBIAS__1up;
   Float_t         METInfo_METPhi_MUON_SAGITTA_RESBIAS__1up;
   std::vector<char>    *AnalysisElectrons_el_selected_MUON_SCALE__1down;
   std::vector<float>   *AnalysisMuons_pt_MUON_SCALE__1down;
   std::vector<char>    *AnalysisMuons_mu_selected_MUON_SCALE__1down;
   std::vector<char>    *AnalysisMuons_isQuality_Loose_MUON_SCALE__1down;
   std::vector<char>    *AnalysisMuons_isQuality_Medium_MUON_SCALE__1down;
   std::vector<char>    *AnalysisMuons_isQuality_Tight_MUON_SCALE__1down;
   std::vector<char>    *AnalysisMuons_isIsolated_PflowTight_VarRad_MUON_SCALE__1down;
   std::vector<float>   *AnalysisMuons_muon_effSF_MUON_SCALE__1down;
   std::vector<float>   *AnalysisMuons_muon_effSF_TTVA_MUON_SCALE__1down;
   std::vector<float>   *AnalysisMuons_muon_effSF_Quality_Loose_MUON_SCALE__1down;
   std::vector<float>   *AnalysisMuons_muon_effSF_Quality_Medium_MUON_SCALE__1down;
   std::vector<float>   *AnalysisMuons_muon_effSF_Quality_Tight_MUON_SCALE__1down;
   std::vector<float>   *AnalysisMuons_muon_effSF_Isol_PflowTight_VarRad_MUON_SCALE__1down;
   std::vector<float>   *AnalysisMuons_muon_effMC_Trig_Loose_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_SCALE__1down;
   std::vector<float>   *AnalysisMuons_muon_effData_Trig_Loose_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_SCALE__1down;
   std::vector<float>   *AnalysisMuons_muon_effMC_Trig_Loose_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_SCALE__1down;
   std::vector<float>   *AnalysisMuons_muon_effData_Trig_Loose_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_SCALE__1down;
   std::vector<float>   *AnalysisMuons_muon_effMC_Trig_Medium_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_SCALE__1down;
   std::vector<float>   *AnalysisMuons_muon_effData_Trig_Medium_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_SCALE__1down;
   std::vector<float>   *AnalysisMuons_muon_effMC_Trig_Medium_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_SCALE__1down;
   std::vector<float>   *AnalysisMuons_muon_effData_Trig_Medium_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_SCALE__1down;
   std::vector<float>   *AnalysisMuons_muon_effMC_Trig_Tight_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_SCALE__1down;
   std::vector<float>   *AnalysisMuons_muon_effData_Trig_Tight_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_SCALE__1down;
   std::vector<float>   *AnalysisMuons_muon_effMC_Trig_Tight_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_SCALE__1down;
   std::vector<float>   *AnalysisMuons_muon_effData_Trig_Tight_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_SCALE__1down;
   std::vector<char>    *AnalysisJets_jet_selected_MUON_SCALE__1down;
   Float_t         METInfo_MET_MUON_SCALE__1down;
   Float_t         METInfo_METPhi_MUON_SCALE__1down;
   std::vector<char>    *AnalysisElectrons_el_selected_MUON_SCALE__1up;
   std::vector<float>   *AnalysisMuons_pt_MUON_SCALE__1up;
   std::vector<char>    *AnalysisMuons_mu_selected_MUON_SCALE__1up;
   std::vector<char>    *AnalysisMuons_isQuality_Loose_MUON_SCALE__1up;
   std::vector<char>    *AnalysisMuons_isQuality_Medium_MUON_SCALE__1up;
   std::vector<char>    *AnalysisMuons_isQuality_Tight_MUON_SCALE__1up;
   std::vector<char>    *AnalysisMuons_isIsolated_PflowTight_VarRad_MUON_SCALE__1up;
   std::vector<float>   *AnalysisMuons_muon_effSF_MUON_SCALE__1up;
   std::vector<float>   *AnalysisMuons_muon_effSF_TTVA_MUON_SCALE__1up;
   std::vector<float>   *AnalysisMuons_muon_effSF_Quality_Loose_MUON_SCALE__1up;
   std::vector<float>   *AnalysisMuons_muon_effSF_Quality_Medium_MUON_SCALE__1up;
   std::vector<float>   *AnalysisMuons_muon_effSF_Quality_Tight_MUON_SCALE__1up;
   std::vector<float>   *AnalysisMuons_muon_effSF_Isol_PflowTight_VarRad_MUON_SCALE__1up;
   std::vector<float>   *AnalysisMuons_muon_effMC_Trig_Loose_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_SCALE__1up;
   std::vector<float>   *AnalysisMuons_muon_effData_Trig_Loose_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_SCALE__1up;
   std::vector<float>   *AnalysisMuons_muon_effMC_Trig_Loose_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_SCALE__1up;
   std::vector<float>   *AnalysisMuons_muon_effData_Trig_Loose_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_SCALE__1up;
   std::vector<float>   *AnalysisMuons_muon_effMC_Trig_Medium_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_SCALE__1up;
   std::vector<float>   *AnalysisMuons_muon_effData_Trig_Medium_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_SCALE__1up;
   std::vector<float>   *AnalysisMuons_muon_effMC_Trig_Medium_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_SCALE__1up;
   std::vector<float>   *AnalysisMuons_muon_effData_Trig_Medium_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_SCALE__1up;
   std::vector<float>   *AnalysisMuons_muon_effMC_Trig_Tight_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_SCALE__1up;
   std::vector<float>   *AnalysisMuons_muon_effData_Trig_Tight_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_SCALE__1up;
   std::vector<float>   *AnalysisMuons_muon_effMC_Trig_Tight_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_SCALE__1up;
   std::vector<float>   *AnalysisMuons_muon_effData_Trig_Tight_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_SCALE__1up;
   std::vector<char>    *AnalysisJets_jet_selected_MUON_SCALE__1up;
   Float_t         METInfo_MET_MUON_SCALE__1up;
   Float_t         METInfo_METPhi_MUON_SCALE__1up;
   Float_t         EventInfo_prodFracWeight_PROD_FRAC_BOTTOM_EIG_1;
   Float_t         EventInfo_prodFracWeight_PROD_FRAC_BOTTOM_EIG_2;
   Float_t         EventInfo_prodFracWeight_PROD_FRAC_CHARM_EIG_1;
   Float_t         EventInfo_prodFracWeight_PROD_FRAC_CHARM_EIG_2;
   Float_t         EventInfo_prodFracWeight_PROD_FRAC_CHARM_EIG_3;
   Float_t         EventInfo_PileupWeight_PRW_DATASF__1down;
   Float_t         EventInfo_PileupWeight_PRW_DATASF__1up;
   std::vector<float>   *DMesons_costhetastar_TRK_BIAS_D0_WM;
   std::vector<int>     *DMesons_D0Index_TRK_BIAS_D0_WM;
   std::vector<std::vector<float> > *DMesons_daughterInfo__eta_TRK_BIAS_D0_WM;
   std::vector<std::vector<bool> > *DMesons_daughterInfo__passTight_TRK_BIAS_D0_WM;
   std::vector<std::vector<int> > *DMesons_daughterInfo__pdgId_TRK_BIAS_D0_WM;
   std::vector<std::vector<float> > *DMesons_daughterInfo__phi_TRK_BIAS_D0_WM;
   std::vector<std::vector<float> > *DMesons_daughterInfo__pt_TRK_BIAS_D0_WM;
   std::vector<std::vector<int> > *DMesons_daughterInfo__trackId_TRK_BIAS_D0_WM;
   std::vector<std::vector<int> > *DMesons_daughterInfo__truthBarcode_TRK_BIAS_D0_WM;
   std::vector<std::vector<int> > *DMesons_daughterInfo__truthDBarcode_TRK_BIAS_D0_WM;
   std::vector<std::vector<float> > *DMesons_daughterInfo__z0SinTheta_TRK_BIAS_D0_WM;
   std::vector<std::vector<float> > *DMesons_daughterInfo__z0SinThetaPV_TRK_BIAS_D0_WM;
   std::vector<int>     *DMesons_decayType_TRK_BIAS_D0_WM;
   std::vector<float>   *DMesons_DeltaMass_TRK_BIAS_D0_WM;
   std::vector<float>   *DMesons_eta_TRK_BIAS_D0_WM;
   std::vector<int>     *DMesons_fitOutput__Charge_TRK_BIAS_D0_WM;
   std::vector<float>   *DMesons_fitOutput__Chi2_TRK_BIAS_D0_WM;
   std::vector<float>   *DMesons_fitOutput__Impact_TRK_BIAS_D0_WM;
   std::vector<float>   *DMesons_fitOutput__ImpactSignificance_TRK_BIAS_D0_WM;
   std::vector<float>   *DMesons_fitOutput__ImpactZ0SinTheta_TRK_BIAS_D0_WM;
   std::vector<float>   *DMesons_fitOutput__Lxy_TRK_BIAS_D0_WM;
   std::vector<std::vector<float> > *DMesons_fitOutput__VertexPosition_TRK_BIAS_D0_WM;
   std::vector<float>   *DMesons_m_TRK_BIAS_D0_WM;
   std::vector<float>   *DMesons_mKpi1_TRK_BIAS_D0_WM;
   std::vector<float>   *DMesons_mKpi2_TRK_BIAS_D0_WM;
   std::vector<float>   *DMesons_mPhi1_TRK_BIAS_D0_WM;
   std::vector<float>   *DMesons_mPhi2_TRK_BIAS_D0_WM;
   std::vector<int>     *DMesons_pdgId_TRK_BIAS_D0_WM;
   std::vector<float>   *DMesons_phi_TRK_BIAS_D0_WM;
   std::vector<float>   *DMesons_pt_TRK_BIAS_D0_WM;
   std::vector<float>   *DMesons_ptcone40_TRK_BIAS_D0_WM;
   std::vector<float>   *DMesons_SlowPionD0_TRK_BIAS_D0_WM;
   std::vector<float>   *DMesons_SlowPionZ0SinTheta_TRK_BIAS_D0_WM;
   std::vector<int>     *DMesons_truthBarcode_TRK_BIAS_D0_WM;
   std::vector<float>   *MesonTracks_d0sig_TRK_BIAS_D0_WM;
   std::vector<float>   *MesonTracks_d0sigPV_TRK_BIAS_D0_WM;
   std::vector<float>   *MesonTracks_eta_TRK_BIAS_D0_WM;
   std::vector<char>    *MesonTracks_passTight_TRK_BIAS_D0_WM;
   std::vector<float>   *MesonTracks_phi_TRK_BIAS_D0_WM;
   std::vector<float>   *MesonTracks_pt_TRK_BIAS_D0_WM;
   std::vector<int>     *MesonTracks_trackId_TRK_BIAS_D0_WM;
   std::vector<float>   *MesonTracks_z0sinTheta_TRK_BIAS_D0_WM;
   std::vector<float>   *MesonTracks_truthMatchProbability_TRK_BIAS_D0_WM;
   std::vector<float>   *AntiKt6PV0TrackJets_pt_TRK_BIAS_D0_WM;
   std::vector<float>   *AntiKt6PV0TrackJets_eta_TRK_BIAS_D0_WM;
   std::vector<float>   *AntiKt6PV0TrackJets_phi_TRK_BIAS_D0_WM;
   std::vector<float>   *AntiKt6PV0TrackJets_m_TRK_BIAS_D0_WM;
   std::vector<std::vector<float> > *AntiKt6PV0TrackJets_daughter__pt_TRK_BIAS_D0_WM;
   std::vector<std::vector<float> > *AntiKt6PV0TrackJets_daughter__eta_TRK_BIAS_D0_WM;
   std::vector<std::vector<float> > *AntiKt6PV0TrackJets_daughter__phi_TRK_BIAS_D0_WM;
   std::vector<std::vector<int> > *AntiKt6PV0TrackJets_daughter__trackId_TRK_BIAS_D0_WM;
   std::vector<float>   *AntiKt8PV0TrackJets_pt_TRK_BIAS_D0_WM;
   std::vector<float>   *AntiKt8PV0TrackJets_eta_TRK_BIAS_D0_WM;
   std::vector<float>   *AntiKt8PV0TrackJets_phi_TRK_BIAS_D0_WM;
   std::vector<float>   *AntiKt8PV0TrackJets_m_TRK_BIAS_D0_WM;
   std::vector<std::vector<float> > *AntiKt8PV0TrackJets_daughter__pt_TRK_BIAS_D0_WM;
   std::vector<std::vector<float> > *AntiKt8PV0TrackJets_daughter__eta_TRK_BIAS_D0_WM;
   std::vector<std::vector<float> > *AntiKt8PV0TrackJets_daughter__phi_TRK_BIAS_D0_WM;
   std::vector<std::vector<int> > *AntiKt8PV0TrackJets_daughter__trackId_TRK_BIAS_D0_WM;
   std::vector<float>   *AntiKt10PV0TrackJets_pt_TRK_BIAS_D0_WM;
   std::vector<float>   *AntiKt10PV0TrackJets_eta_TRK_BIAS_D0_WM;
   std::vector<float>   *AntiKt10PV0TrackJets_phi_TRK_BIAS_D0_WM;
   std::vector<float>   *AntiKt10PV0TrackJets_m_TRK_BIAS_D0_WM;
   std::vector<std::vector<float> > *AntiKt10PV0TrackJets_daughter__pt_TRK_BIAS_D0_WM;
   std::vector<std::vector<float> > *AntiKt10PV0TrackJets_daughter__eta_TRK_BIAS_D0_WM;
   std::vector<std::vector<float> > *AntiKt10PV0TrackJets_daughter__phi_TRK_BIAS_D0_WM;
   std::vector<std::vector<int> > *AntiKt10PV0TrackJets_daughter__trackId_TRK_BIAS_D0_WM;
   std::vector<float>   *AntiKtVR30Rmax4Rmin02TrackJets_pt_TRK_BIAS_D0_WM;
   std::vector<float>   *AntiKtVR30Rmax4Rmin02TrackJets_eta_TRK_BIAS_D0_WM;
   std::vector<float>   *AntiKtVR30Rmax4Rmin02TrackJets_phi_TRK_BIAS_D0_WM;
   std::vector<float>   *AntiKtVR30Rmax4Rmin02TrackJets_m_TRK_BIAS_D0_WM;
   std::vector<std::vector<float> > *AntiKtVR30Rmax4Rmin02TrackJets_daughter__pt_TRK_BIAS_D0_WM;
   std::vector<std::vector<float> > *AntiKtVR30Rmax4Rmin02TrackJets_daughter__eta_TRK_BIAS_D0_WM;
   std::vector<std::vector<float> > *AntiKtVR30Rmax4Rmin02TrackJets_daughter__phi_TRK_BIAS_D0_WM;
   std::vector<std::vector<int> > *AntiKtVR30Rmax4Rmin02TrackJets_daughter__trackId_TRK_BIAS_D0_WM;
   std::vector<float>   *DMesons_costhetastar_TRK_BIAS_QOVERP_SAGITTA_WM;
   std::vector<int>     *DMesons_D0Index_TRK_BIAS_QOVERP_SAGITTA_WM;
   std::vector<std::vector<float> > *DMesons_daughterInfo__eta_TRK_BIAS_QOVERP_SAGITTA_WM;
   std::vector<std::vector<bool> > *DMesons_daughterInfo__passTight_TRK_BIAS_QOVERP_SAGITTA_WM;
   std::vector<std::vector<int> > *DMesons_daughterInfo__pdgId_TRK_BIAS_QOVERP_SAGITTA_WM;
   std::vector<std::vector<float> > *DMesons_daughterInfo__phi_TRK_BIAS_QOVERP_SAGITTA_WM;
   std::vector<std::vector<float> > *DMesons_daughterInfo__pt_TRK_BIAS_QOVERP_SAGITTA_WM;
   std::vector<std::vector<int> > *DMesons_daughterInfo__trackId_TRK_BIAS_QOVERP_SAGITTA_WM;
   std::vector<std::vector<int> > *DMesons_daughterInfo__truthBarcode_TRK_BIAS_QOVERP_SAGITTA_WM;
   std::vector<std::vector<int> > *DMesons_daughterInfo__truthDBarcode_TRK_BIAS_QOVERP_SAGITTA_WM;
   std::vector<std::vector<float> > *DMesons_daughterInfo__z0SinTheta_TRK_BIAS_QOVERP_SAGITTA_WM;
   std::vector<std::vector<float> > *DMesons_daughterInfo__z0SinThetaPV_TRK_BIAS_QOVERP_SAGITTA_WM;
   std::vector<int>     *DMesons_decayType_TRK_BIAS_QOVERP_SAGITTA_WM;
   std::vector<float>   *DMesons_DeltaMass_TRK_BIAS_QOVERP_SAGITTA_WM;
   std::vector<float>   *DMesons_eta_TRK_BIAS_QOVERP_SAGITTA_WM;
   std::vector<int>     *DMesons_fitOutput__Charge_TRK_BIAS_QOVERP_SAGITTA_WM;
   std::vector<float>   *DMesons_fitOutput__Chi2_TRK_BIAS_QOVERP_SAGITTA_WM;
   std::vector<float>   *DMesons_fitOutput__Impact_TRK_BIAS_QOVERP_SAGITTA_WM;
   std::vector<float>   *DMesons_fitOutput__ImpactSignificance_TRK_BIAS_QOVERP_SAGITTA_WM;
   std::vector<float>   *DMesons_fitOutput__ImpactZ0SinTheta_TRK_BIAS_QOVERP_SAGITTA_WM;
   std::vector<float>   *DMesons_fitOutput__Lxy_TRK_BIAS_QOVERP_SAGITTA_WM;
   std::vector<std::vector<float> > *DMesons_fitOutput__VertexPosition_TRK_BIAS_QOVERP_SAGITTA_WM;
   std::vector<float>   *DMesons_m_TRK_BIAS_QOVERP_SAGITTA_WM;
   std::vector<float>   *DMesons_mKpi1_TRK_BIAS_QOVERP_SAGITTA_WM;
   std::vector<float>   *DMesons_mKpi2_TRK_BIAS_QOVERP_SAGITTA_WM;
   std::vector<float>   *DMesons_mPhi1_TRK_BIAS_QOVERP_SAGITTA_WM;
   std::vector<float>   *DMesons_mPhi2_TRK_BIAS_QOVERP_SAGITTA_WM;
   std::vector<int>     *DMesons_pdgId_TRK_BIAS_QOVERP_SAGITTA_WM;
   std::vector<float>   *DMesons_phi_TRK_BIAS_QOVERP_SAGITTA_WM;
   std::vector<float>   *DMesons_pt_TRK_BIAS_QOVERP_SAGITTA_WM;
   std::vector<float>   *DMesons_ptcone40_TRK_BIAS_QOVERP_SAGITTA_WM;
   std::vector<float>   *DMesons_SlowPionD0_TRK_BIAS_QOVERP_SAGITTA_WM;
   std::vector<float>   *DMesons_SlowPionZ0SinTheta_TRK_BIAS_QOVERP_SAGITTA_WM;
   std::vector<int>     *DMesons_truthBarcode_TRK_BIAS_QOVERP_SAGITTA_WM;
   std::vector<float>   *MesonTracks_d0sig_TRK_BIAS_QOVERP_SAGITTA_WM;
   std::vector<float>   *MesonTracks_d0sigPV_TRK_BIAS_QOVERP_SAGITTA_WM;
   std::vector<float>   *MesonTracks_eta_TRK_BIAS_QOVERP_SAGITTA_WM;
   std::vector<char>    *MesonTracks_passTight_TRK_BIAS_QOVERP_SAGITTA_WM;
   std::vector<float>   *MesonTracks_phi_TRK_BIAS_QOVERP_SAGITTA_WM;
   std::vector<float>   *MesonTracks_pt_TRK_BIAS_QOVERP_SAGITTA_WM;
   std::vector<int>     *MesonTracks_trackId_TRK_BIAS_QOVERP_SAGITTA_WM;
   std::vector<float>   *MesonTracks_z0sinTheta_TRK_BIAS_QOVERP_SAGITTA_WM;
   std::vector<float>   *MesonTracks_truthMatchProbability_TRK_BIAS_QOVERP_SAGITTA_WM;
   std::vector<float>   *AntiKt6PV0TrackJets_pt_TRK_BIAS_QOVERP_SAGITTA_WM;
   std::vector<float>   *AntiKt6PV0TrackJets_eta_TRK_BIAS_QOVERP_SAGITTA_WM;
   std::vector<float>   *AntiKt6PV0TrackJets_phi_TRK_BIAS_QOVERP_SAGITTA_WM;
   std::vector<float>   *AntiKt6PV0TrackJets_m_TRK_BIAS_QOVERP_SAGITTA_WM;
   std::vector<std::vector<float> > *AntiKt6PV0TrackJets_daughter__pt_TRK_BIAS_QOVERP_SAGITTA_WM;
   std::vector<std::vector<float> > *AntiKt6PV0TrackJets_daughter__eta_TRK_BIAS_QOVERP_SAGITTA_WM;
   std::vector<std::vector<float> > *AntiKt6PV0TrackJets_daughter__phi_TRK_BIAS_QOVERP_SAGITTA_WM;
   std::vector<std::vector<int> > *AntiKt6PV0TrackJets_daughter__trackId_TRK_BIAS_QOVERP_SAGITTA_WM;
   std::vector<float>   *AntiKt8PV0TrackJets_pt_TRK_BIAS_QOVERP_SAGITTA_WM;
   std::vector<float>   *AntiKt8PV0TrackJets_eta_TRK_BIAS_QOVERP_SAGITTA_WM;
   std::vector<float>   *AntiKt8PV0TrackJets_phi_TRK_BIAS_QOVERP_SAGITTA_WM;
   std::vector<float>   *AntiKt8PV0TrackJets_m_TRK_BIAS_QOVERP_SAGITTA_WM;
   std::vector<std::vector<float> > *AntiKt8PV0TrackJets_daughter__pt_TRK_BIAS_QOVERP_SAGITTA_WM;
   std::vector<std::vector<float> > *AntiKt8PV0TrackJets_daughter__eta_TRK_BIAS_QOVERP_SAGITTA_WM;
   std::vector<std::vector<float> > *AntiKt8PV0TrackJets_daughter__phi_TRK_BIAS_QOVERP_SAGITTA_WM;
   std::vector<std::vector<int> > *AntiKt8PV0TrackJets_daughter__trackId_TRK_BIAS_QOVERP_SAGITTA_WM;
   std::vector<float>   *AntiKt10PV0TrackJets_pt_TRK_BIAS_QOVERP_SAGITTA_WM;
   std::vector<float>   *AntiKt10PV0TrackJets_eta_TRK_BIAS_QOVERP_SAGITTA_WM;
   std::vector<float>   *AntiKt10PV0TrackJets_phi_TRK_BIAS_QOVERP_SAGITTA_WM;
   std::vector<float>   *AntiKt10PV0TrackJets_m_TRK_BIAS_QOVERP_SAGITTA_WM;
   std::vector<std::vector<float> > *AntiKt10PV0TrackJets_daughter__pt_TRK_BIAS_QOVERP_SAGITTA_WM;
   std::vector<std::vector<float> > *AntiKt10PV0TrackJets_daughter__eta_TRK_BIAS_QOVERP_SAGITTA_WM;
   std::vector<std::vector<float> > *AntiKt10PV0TrackJets_daughter__phi_TRK_BIAS_QOVERP_SAGITTA_WM;
   std::vector<std::vector<int> > *AntiKt10PV0TrackJets_daughter__trackId_TRK_BIAS_QOVERP_SAGITTA_WM;
   std::vector<float>   *AntiKtVR30Rmax4Rmin02TrackJets_pt_TRK_BIAS_QOVERP_SAGITTA_WM;
   std::vector<float>   *AntiKtVR30Rmax4Rmin02TrackJets_eta_TRK_BIAS_QOVERP_SAGITTA_WM;
   std::vector<float>   *AntiKtVR30Rmax4Rmin02TrackJets_phi_TRK_BIAS_QOVERP_SAGITTA_WM;
   std::vector<float>   *AntiKtVR30Rmax4Rmin02TrackJets_m_TRK_BIAS_QOVERP_SAGITTA_WM;
   std::vector<std::vector<float> > *AntiKtVR30Rmax4Rmin02TrackJets_daughter__pt_TRK_BIAS_QOVERP_SAGITTA_WM;
   std::vector<std::vector<float> > *AntiKtVR30Rmax4Rmin02TrackJets_daughter__eta_TRK_BIAS_QOVERP_SAGITTA_WM;
   std::vector<std::vector<float> > *AntiKtVR30Rmax4Rmin02TrackJets_daughter__phi_TRK_BIAS_QOVERP_SAGITTA_WM;
   std::vector<std::vector<int> > *AntiKtVR30Rmax4Rmin02TrackJets_daughter__trackId_TRK_BIAS_QOVERP_SAGITTA_WM;
   std::vector<float>   *DMesons_costhetastar_TRK_BIAS_Z0_WM;
   std::vector<int>     *DMesons_D0Index_TRK_BIAS_Z0_WM;
   std::vector<std::vector<float> > *DMesons_daughterInfo__eta_TRK_BIAS_Z0_WM;
   std::vector<std::vector<bool> > *DMesons_daughterInfo__passTight_TRK_BIAS_Z0_WM;
   std::vector<std::vector<int> > *DMesons_daughterInfo__pdgId_TRK_BIAS_Z0_WM;
   std::vector<std::vector<float> > *DMesons_daughterInfo__phi_TRK_BIAS_Z0_WM;
   std::vector<std::vector<float> > *DMesons_daughterInfo__pt_TRK_BIAS_Z0_WM;
   std::vector<std::vector<int> > *DMesons_daughterInfo__trackId_TRK_BIAS_Z0_WM;
   std::vector<std::vector<int> > *DMesons_daughterInfo__truthBarcode_TRK_BIAS_Z0_WM;
   std::vector<std::vector<int> > *DMesons_daughterInfo__truthDBarcode_TRK_BIAS_Z0_WM;
   std::vector<std::vector<float> > *DMesons_daughterInfo__z0SinTheta_TRK_BIAS_Z0_WM;
   std::vector<std::vector<float> > *DMesons_daughterInfo__z0SinThetaPV_TRK_BIAS_Z0_WM;
   std::vector<int>     *DMesons_decayType_TRK_BIAS_Z0_WM;
   std::vector<float>   *DMesons_DeltaMass_TRK_BIAS_Z0_WM;
   std::vector<float>   *DMesons_eta_TRK_BIAS_Z0_WM;
   std::vector<int>     *DMesons_fitOutput__Charge_TRK_BIAS_Z0_WM;
   std::vector<float>   *DMesons_fitOutput__Chi2_TRK_BIAS_Z0_WM;
   std::vector<float>   *DMesons_fitOutput__Impact_TRK_BIAS_Z0_WM;
   std::vector<float>   *DMesons_fitOutput__ImpactSignificance_TRK_BIAS_Z0_WM;
   std::vector<float>   *DMesons_fitOutput__ImpactZ0SinTheta_TRK_BIAS_Z0_WM;
   std::vector<float>   *DMesons_fitOutput__Lxy_TRK_BIAS_Z0_WM;
   std::vector<std::vector<float> > *DMesons_fitOutput__VertexPosition_TRK_BIAS_Z0_WM;
   std::vector<float>   *DMesons_m_TRK_BIAS_Z0_WM;
   std::vector<float>   *DMesons_mKpi1_TRK_BIAS_Z0_WM;
   std::vector<float>   *DMesons_mKpi2_TRK_BIAS_Z0_WM;
   std::vector<float>   *DMesons_mPhi1_TRK_BIAS_Z0_WM;
   std::vector<float>   *DMesons_mPhi2_TRK_BIAS_Z0_WM;
   std::vector<int>     *DMesons_pdgId_TRK_BIAS_Z0_WM;
   std::vector<float>   *DMesons_phi_TRK_BIAS_Z0_WM;
   std::vector<float>   *DMesons_pt_TRK_BIAS_Z0_WM;
   std::vector<float>   *DMesons_ptcone40_TRK_BIAS_Z0_WM;
   std::vector<float>   *DMesons_SlowPionD0_TRK_BIAS_Z0_WM;
   std::vector<float>   *DMesons_SlowPionZ0SinTheta_TRK_BIAS_Z0_WM;
   std::vector<int>     *DMesons_truthBarcode_TRK_BIAS_Z0_WM;
   std::vector<float>   *MesonTracks_d0sig_TRK_BIAS_Z0_WM;
   std::vector<float>   *MesonTracks_d0sigPV_TRK_BIAS_Z0_WM;
   std::vector<float>   *MesonTracks_eta_TRK_BIAS_Z0_WM;
   std::vector<char>    *MesonTracks_passTight_TRK_BIAS_Z0_WM;
   std::vector<float>   *MesonTracks_phi_TRK_BIAS_Z0_WM;
   std::vector<float>   *MesonTracks_pt_TRK_BIAS_Z0_WM;
   std::vector<int>     *MesonTracks_trackId_TRK_BIAS_Z0_WM;
   std::vector<float>   *MesonTracks_z0sinTheta_TRK_BIAS_Z0_WM;
   std::vector<float>   *MesonTracks_truthMatchProbability_TRK_BIAS_Z0_WM;
   std::vector<float>   *AntiKt6PV0TrackJets_pt_TRK_BIAS_Z0_WM;
   std::vector<float>   *AntiKt6PV0TrackJets_eta_TRK_BIAS_Z0_WM;
   std::vector<float>   *AntiKt6PV0TrackJets_phi_TRK_BIAS_Z0_WM;
   std::vector<float>   *AntiKt6PV0TrackJets_m_TRK_BIAS_Z0_WM;
   std::vector<std::vector<float> > *AntiKt6PV0TrackJets_daughter__pt_TRK_BIAS_Z0_WM;
   std::vector<std::vector<float> > *AntiKt6PV0TrackJets_daughter__eta_TRK_BIAS_Z0_WM;
   std::vector<std::vector<float> > *AntiKt6PV0TrackJets_daughter__phi_TRK_BIAS_Z0_WM;
   std::vector<std::vector<int> > *AntiKt6PV0TrackJets_daughter__trackId_TRK_BIAS_Z0_WM;
   std::vector<float>   *AntiKt8PV0TrackJets_pt_TRK_BIAS_Z0_WM;
   std::vector<float>   *AntiKt8PV0TrackJets_eta_TRK_BIAS_Z0_WM;
   std::vector<float>   *AntiKt8PV0TrackJets_phi_TRK_BIAS_Z0_WM;
   std::vector<float>   *AntiKt8PV0TrackJets_m_TRK_BIAS_Z0_WM;
   std::vector<std::vector<float> > *AntiKt8PV0TrackJets_daughter__pt_TRK_BIAS_Z0_WM;
   std::vector<std::vector<float> > *AntiKt8PV0TrackJets_daughter__eta_TRK_BIAS_Z0_WM;
   std::vector<std::vector<float> > *AntiKt8PV0TrackJets_daughter__phi_TRK_BIAS_Z0_WM;
   std::vector<std::vector<int> > *AntiKt8PV0TrackJets_daughter__trackId_TRK_BIAS_Z0_WM;
   std::vector<float>   *AntiKt10PV0TrackJets_pt_TRK_BIAS_Z0_WM;
   std::vector<float>   *AntiKt10PV0TrackJets_eta_TRK_BIAS_Z0_WM;
   std::vector<float>   *AntiKt10PV0TrackJets_phi_TRK_BIAS_Z0_WM;
   std::vector<float>   *AntiKt10PV0TrackJets_m_TRK_BIAS_Z0_WM;
   std::vector<std::vector<float> > *AntiKt10PV0TrackJets_daughter__pt_TRK_BIAS_Z0_WM;
   std::vector<std::vector<float> > *AntiKt10PV0TrackJets_daughter__eta_TRK_BIAS_Z0_WM;
   std::vector<std::vector<float> > *AntiKt10PV0TrackJets_daughter__phi_TRK_BIAS_Z0_WM;
   std::vector<std::vector<int> > *AntiKt10PV0TrackJets_daughter__trackId_TRK_BIAS_Z0_WM;
   std::vector<float>   *AntiKtVR30Rmax4Rmin02TrackJets_pt_TRK_BIAS_Z0_WM;
   std::vector<float>   *AntiKtVR30Rmax4Rmin02TrackJets_eta_TRK_BIAS_Z0_WM;
   std::vector<float>   *AntiKtVR30Rmax4Rmin02TrackJets_phi_TRK_BIAS_Z0_WM;
   std::vector<float>   *AntiKtVR30Rmax4Rmin02TrackJets_m_TRK_BIAS_Z0_WM;
   std::vector<std::vector<float> > *AntiKtVR30Rmax4Rmin02TrackJets_daughter__pt_TRK_BIAS_Z0_WM;
   std::vector<std::vector<float> > *AntiKtVR30Rmax4Rmin02TrackJets_daughter__eta_TRK_BIAS_Z0_WM;
   std::vector<std::vector<float> > *AntiKtVR30Rmax4Rmin02TrackJets_daughter__phi_TRK_BIAS_Z0_WM;
   std::vector<std::vector<int> > *AntiKtVR30Rmax4Rmin02TrackJets_daughter__trackId_TRK_BIAS_Z0_WM;
   std::vector<float>   *DMesons_costhetastar_TRK_EFF_LOOSE_GLOBAL;
   std::vector<int>     *DMesons_D0Index_TRK_EFF_LOOSE_GLOBAL;
   std::vector<std::vector<float> > *DMesons_daughterInfo__eta_TRK_EFF_LOOSE_GLOBAL;
   std::vector<std::vector<bool> > *DMesons_daughterInfo__passTight_TRK_EFF_LOOSE_GLOBAL;
   std::vector<std::vector<int> > *DMesons_daughterInfo__pdgId_TRK_EFF_LOOSE_GLOBAL;
   std::vector<std::vector<float> > *DMesons_daughterInfo__phi_TRK_EFF_LOOSE_GLOBAL;
   std::vector<std::vector<float> > *DMesons_daughterInfo__pt_TRK_EFF_LOOSE_GLOBAL;
   std::vector<std::vector<int> > *DMesons_daughterInfo__trackId_TRK_EFF_LOOSE_GLOBAL;
   std::vector<std::vector<int> > *DMesons_daughterInfo__truthBarcode_TRK_EFF_LOOSE_GLOBAL;
   std::vector<std::vector<int> > *DMesons_daughterInfo__truthDBarcode_TRK_EFF_LOOSE_GLOBAL;
   std::vector<std::vector<float> > *DMesons_daughterInfo__z0SinTheta_TRK_EFF_LOOSE_GLOBAL;
   std::vector<std::vector<float> > *DMesons_daughterInfo__z0SinThetaPV_TRK_EFF_LOOSE_GLOBAL;
   std::vector<int>     *DMesons_decayType_TRK_EFF_LOOSE_GLOBAL;
   std::vector<float>   *DMesons_DeltaMass_TRK_EFF_LOOSE_GLOBAL;
   std::vector<float>   *DMesons_eta_TRK_EFF_LOOSE_GLOBAL;
   std::vector<int>     *DMesons_fitOutput__Charge_TRK_EFF_LOOSE_GLOBAL;
   std::vector<float>   *DMesons_fitOutput__Chi2_TRK_EFF_LOOSE_GLOBAL;
   std::vector<float>   *DMesons_fitOutput__Impact_TRK_EFF_LOOSE_GLOBAL;
   std::vector<float>   *DMesons_fitOutput__ImpactSignificance_TRK_EFF_LOOSE_GLOBAL;
   std::vector<float>   *DMesons_fitOutput__ImpactZ0SinTheta_TRK_EFF_LOOSE_GLOBAL;
   std::vector<float>   *DMesons_fitOutput__Lxy_TRK_EFF_LOOSE_GLOBAL;
   std::vector<std::vector<float> > *DMesons_fitOutput__VertexPosition_TRK_EFF_LOOSE_GLOBAL;
   std::vector<float>   *DMesons_m_TRK_EFF_LOOSE_GLOBAL;
   std::vector<float>   *DMesons_mKpi1_TRK_EFF_LOOSE_GLOBAL;
   std::vector<float>   *DMesons_mKpi2_TRK_EFF_LOOSE_GLOBAL;
   std::vector<float>   *DMesons_mPhi1_TRK_EFF_LOOSE_GLOBAL;
   std::vector<float>   *DMesons_mPhi2_TRK_EFF_LOOSE_GLOBAL;
   std::vector<int>     *DMesons_pdgId_TRK_EFF_LOOSE_GLOBAL;
   std::vector<float>   *DMesons_phi_TRK_EFF_LOOSE_GLOBAL;
   std::vector<float>   *DMesons_pt_TRK_EFF_LOOSE_GLOBAL;
   std::vector<float>   *DMesons_ptcone40_TRK_EFF_LOOSE_GLOBAL;
   std::vector<float>   *DMesons_SlowPionD0_TRK_EFF_LOOSE_GLOBAL;
   std::vector<float>   *DMesons_SlowPionZ0SinTheta_TRK_EFF_LOOSE_GLOBAL;
   std::vector<int>     *DMesons_truthBarcode_TRK_EFF_LOOSE_GLOBAL;
   std::vector<float>   *MesonTracks_d0sig_TRK_EFF_LOOSE_GLOBAL;
   std::vector<float>   *MesonTracks_d0sigPV_TRK_EFF_LOOSE_GLOBAL;
   std::vector<float>   *MesonTracks_eta_TRK_EFF_LOOSE_GLOBAL;
   std::vector<char>    *MesonTracks_passTight_TRK_EFF_LOOSE_GLOBAL;
   std::vector<float>   *MesonTracks_phi_TRK_EFF_LOOSE_GLOBAL;
   std::vector<float>   *MesonTracks_pt_TRK_EFF_LOOSE_GLOBAL;
   std::vector<int>     *MesonTracks_trackId_TRK_EFF_LOOSE_GLOBAL;
   std::vector<float>   *MesonTracks_z0sinTheta_TRK_EFF_LOOSE_GLOBAL;
   std::vector<float>   *MesonTracks_truthMatchProbability_TRK_EFF_LOOSE_GLOBAL;
   std::vector<float>   *AntiKt6PV0TrackJets_pt_TRK_EFF_LOOSE_GLOBAL;
   std::vector<float>   *AntiKt6PV0TrackJets_eta_TRK_EFF_LOOSE_GLOBAL;
   std::vector<float>   *AntiKt6PV0TrackJets_phi_TRK_EFF_LOOSE_GLOBAL;
   std::vector<float>   *AntiKt6PV0TrackJets_m_TRK_EFF_LOOSE_GLOBAL;
   std::vector<std::vector<float> > *AntiKt6PV0TrackJets_daughter__pt_TRK_EFF_LOOSE_GLOBAL;
   std::vector<std::vector<float> > *AntiKt6PV0TrackJets_daughter__eta_TRK_EFF_LOOSE_GLOBAL;
   std::vector<std::vector<float> > *AntiKt6PV0TrackJets_daughter__phi_TRK_EFF_LOOSE_GLOBAL;
   std::vector<std::vector<int> > *AntiKt6PV0TrackJets_daughter__trackId_TRK_EFF_LOOSE_GLOBAL;
   std::vector<float>   *AntiKt8PV0TrackJets_pt_TRK_EFF_LOOSE_GLOBAL;
   std::vector<float>   *AntiKt8PV0TrackJets_eta_TRK_EFF_LOOSE_GLOBAL;
   std::vector<float>   *AntiKt8PV0TrackJets_phi_TRK_EFF_LOOSE_GLOBAL;
   std::vector<float>   *AntiKt8PV0TrackJets_m_TRK_EFF_LOOSE_GLOBAL;
   std::vector<std::vector<float> > *AntiKt8PV0TrackJets_daughter__pt_TRK_EFF_LOOSE_GLOBAL;
   std::vector<std::vector<float> > *AntiKt8PV0TrackJets_daughter__eta_TRK_EFF_LOOSE_GLOBAL;
   std::vector<std::vector<float> > *AntiKt8PV0TrackJets_daughter__phi_TRK_EFF_LOOSE_GLOBAL;
   std::vector<std::vector<int> > *AntiKt8PV0TrackJets_daughter__trackId_TRK_EFF_LOOSE_GLOBAL;
   std::vector<float>   *AntiKt10PV0TrackJets_pt_TRK_EFF_LOOSE_GLOBAL;
   std::vector<float>   *AntiKt10PV0TrackJets_eta_TRK_EFF_LOOSE_GLOBAL;
   std::vector<float>   *AntiKt10PV0TrackJets_phi_TRK_EFF_LOOSE_GLOBAL;
   std::vector<float>   *AntiKt10PV0TrackJets_m_TRK_EFF_LOOSE_GLOBAL;
   std::vector<std::vector<float> > *AntiKt10PV0TrackJets_daughter__pt_TRK_EFF_LOOSE_GLOBAL;
   std::vector<std::vector<float> > *AntiKt10PV0TrackJets_daughter__eta_TRK_EFF_LOOSE_GLOBAL;
   std::vector<std::vector<float> > *AntiKt10PV0TrackJets_daughter__phi_TRK_EFF_LOOSE_GLOBAL;
   std::vector<std::vector<int> > *AntiKt10PV0TrackJets_daughter__trackId_TRK_EFF_LOOSE_GLOBAL;
   std::vector<float>   *AntiKtVR30Rmax4Rmin02TrackJets_pt_TRK_EFF_LOOSE_GLOBAL;
   std::vector<float>   *AntiKtVR30Rmax4Rmin02TrackJets_eta_TRK_EFF_LOOSE_GLOBAL;
   std::vector<float>   *AntiKtVR30Rmax4Rmin02TrackJets_phi_TRK_EFF_LOOSE_GLOBAL;
   std::vector<float>   *AntiKtVR30Rmax4Rmin02TrackJets_m_TRK_EFF_LOOSE_GLOBAL;
   std::vector<std::vector<float> > *AntiKtVR30Rmax4Rmin02TrackJets_daughter__pt_TRK_EFF_LOOSE_GLOBAL;
   std::vector<std::vector<float> > *AntiKtVR30Rmax4Rmin02TrackJets_daughter__eta_TRK_EFF_LOOSE_GLOBAL;
   std::vector<std::vector<float> > *AntiKtVR30Rmax4Rmin02TrackJets_daughter__phi_TRK_EFF_LOOSE_GLOBAL;
   std::vector<std::vector<int> > *AntiKtVR30Rmax4Rmin02TrackJets_daughter__trackId_TRK_EFF_LOOSE_GLOBAL;
   std::vector<float>   *DMesons_costhetastar_TRK_EFF_LOOSE_IBL;
   std::vector<int>     *DMesons_D0Index_TRK_EFF_LOOSE_IBL;
   std::vector<std::vector<float> > *DMesons_daughterInfo__eta_TRK_EFF_LOOSE_IBL;
   std::vector<std::vector<bool> > *DMesons_daughterInfo__passTight_TRK_EFF_LOOSE_IBL;
   std::vector<std::vector<int> > *DMesons_daughterInfo__pdgId_TRK_EFF_LOOSE_IBL;
   std::vector<std::vector<float> > *DMesons_daughterInfo__phi_TRK_EFF_LOOSE_IBL;
   std::vector<std::vector<float> > *DMesons_daughterInfo__pt_TRK_EFF_LOOSE_IBL;
   std::vector<std::vector<int> > *DMesons_daughterInfo__trackId_TRK_EFF_LOOSE_IBL;
   std::vector<std::vector<int> > *DMesons_daughterInfo__truthBarcode_TRK_EFF_LOOSE_IBL;
   std::vector<std::vector<int> > *DMesons_daughterInfo__truthDBarcode_TRK_EFF_LOOSE_IBL;
   std::vector<std::vector<float> > *DMesons_daughterInfo__z0SinTheta_TRK_EFF_LOOSE_IBL;
   std::vector<std::vector<float> > *DMesons_daughterInfo__z0SinThetaPV_TRK_EFF_LOOSE_IBL;
   std::vector<int>     *DMesons_decayType_TRK_EFF_LOOSE_IBL;
   std::vector<float>   *DMesons_DeltaMass_TRK_EFF_LOOSE_IBL;
   std::vector<float>   *DMesons_eta_TRK_EFF_LOOSE_IBL;
   std::vector<int>     *DMesons_fitOutput__Charge_TRK_EFF_LOOSE_IBL;
   std::vector<float>   *DMesons_fitOutput__Chi2_TRK_EFF_LOOSE_IBL;
   std::vector<float>   *DMesons_fitOutput__Impact_TRK_EFF_LOOSE_IBL;
   std::vector<float>   *DMesons_fitOutput__ImpactSignificance_TRK_EFF_LOOSE_IBL;
   std::vector<float>   *DMesons_fitOutput__ImpactZ0SinTheta_TRK_EFF_LOOSE_IBL;
   std::vector<float>   *DMesons_fitOutput__Lxy_TRK_EFF_LOOSE_IBL;
   std::vector<std::vector<float> > *DMesons_fitOutput__VertexPosition_TRK_EFF_LOOSE_IBL;
   std::vector<float>   *DMesons_m_TRK_EFF_LOOSE_IBL;
   std::vector<float>   *DMesons_mKpi1_TRK_EFF_LOOSE_IBL;
   std::vector<float>   *DMesons_mKpi2_TRK_EFF_LOOSE_IBL;
   std::vector<float>   *DMesons_mPhi1_TRK_EFF_LOOSE_IBL;
   std::vector<float>   *DMesons_mPhi2_TRK_EFF_LOOSE_IBL;
   std::vector<int>     *DMesons_pdgId_TRK_EFF_LOOSE_IBL;
   std::vector<float>   *DMesons_phi_TRK_EFF_LOOSE_IBL;
   std::vector<float>   *DMesons_pt_TRK_EFF_LOOSE_IBL;
   std::vector<float>   *DMesons_ptcone40_TRK_EFF_LOOSE_IBL;
   std::vector<float>   *DMesons_SlowPionD0_TRK_EFF_LOOSE_IBL;
   std::vector<float>   *DMesons_SlowPionZ0SinTheta_TRK_EFF_LOOSE_IBL;
   std::vector<int>     *DMesons_truthBarcode_TRK_EFF_LOOSE_IBL;
   std::vector<float>   *MesonTracks_d0sig_TRK_EFF_LOOSE_IBL;
   std::vector<float>   *MesonTracks_d0sigPV_TRK_EFF_LOOSE_IBL;
   std::vector<float>   *MesonTracks_eta_TRK_EFF_LOOSE_IBL;
   std::vector<char>    *MesonTracks_passTight_TRK_EFF_LOOSE_IBL;
   std::vector<float>   *MesonTracks_phi_TRK_EFF_LOOSE_IBL;
   std::vector<float>   *MesonTracks_pt_TRK_EFF_LOOSE_IBL;
   std::vector<int>     *MesonTracks_trackId_TRK_EFF_LOOSE_IBL;
   std::vector<float>   *MesonTracks_z0sinTheta_TRK_EFF_LOOSE_IBL;
   std::vector<float>   *MesonTracks_truthMatchProbability_TRK_EFF_LOOSE_IBL;
   std::vector<float>   *AntiKt6PV0TrackJets_pt_TRK_EFF_LOOSE_IBL;
   std::vector<float>   *AntiKt6PV0TrackJets_eta_TRK_EFF_LOOSE_IBL;
   std::vector<float>   *AntiKt6PV0TrackJets_phi_TRK_EFF_LOOSE_IBL;
   std::vector<float>   *AntiKt6PV0TrackJets_m_TRK_EFF_LOOSE_IBL;
   std::vector<std::vector<float> > *AntiKt6PV0TrackJets_daughter__pt_TRK_EFF_LOOSE_IBL;
   std::vector<std::vector<float> > *AntiKt6PV0TrackJets_daughter__eta_TRK_EFF_LOOSE_IBL;
   std::vector<std::vector<float> > *AntiKt6PV0TrackJets_daughter__phi_TRK_EFF_LOOSE_IBL;
   std::vector<std::vector<int> > *AntiKt6PV0TrackJets_daughter__trackId_TRK_EFF_LOOSE_IBL;
   std::vector<float>   *AntiKt8PV0TrackJets_pt_TRK_EFF_LOOSE_IBL;
   std::vector<float>   *AntiKt8PV0TrackJets_eta_TRK_EFF_LOOSE_IBL;
   std::vector<float>   *AntiKt8PV0TrackJets_phi_TRK_EFF_LOOSE_IBL;
   std::vector<float>   *AntiKt8PV0TrackJets_m_TRK_EFF_LOOSE_IBL;
   std::vector<std::vector<float> > *AntiKt8PV0TrackJets_daughter__pt_TRK_EFF_LOOSE_IBL;
   std::vector<std::vector<float> > *AntiKt8PV0TrackJets_daughter__eta_TRK_EFF_LOOSE_IBL;
   std::vector<std::vector<float> > *AntiKt8PV0TrackJets_daughter__phi_TRK_EFF_LOOSE_IBL;
   std::vector<std::vector<int> > *AntiKt8PV0TrackJets_daughter__trackId_TRK_EFF_LOOSE_IBL;
   std::vector<float>   *AntiKt10PV0TrackJets_pt_TRK_EFF_LOOSE_IBL;
   std::vector<float>   *AntiKt10PV0TrackJets_eta_TRK_EFF_LOOSE_IBL;
   std::vector<float>   *AntiKt10PV0TrackJets_phi_TRK_EFF_LOOSE_IBL;
   std::vector<float>   *AntiKt10PV0TrackJets_m_TRK_EFF_LOOSE_IBL;
   std::vector<std::vector<float> > *AntiKt10PV0TrackJets_daughter__pt_TRK_EFF_LOOSE_IBL;
   std::vector<std::vector<float> > *AntiKt10PV0TrackJets_daughter__eta_TRK_EFF_LOOSE_IBL;
   std::vector<std::vector<float> > *AntiKt10PV0TrackJets_daughter__phi_TRK_EFF_LOOSE_IBL;
   std::vector<std::vector<int> > *AntiKt10PV0TrackJets_daughter__trackId_TRK_EFF_LOOSE_IBL;
   std::vector<float>   *AntiKtVR30Rmax4Rmin02TrackJets_pt_TRK_EFF_LOOSE_IBL;
   std::vector<float>   *AntiKtVR30Rmax4Rmin02TrackJets_eta_TRK_EFF_LOOSE_IBL;
   std::vector<float>   *AntiKtVR30Rmax4Rmin02TrackJets_phi_TRK_EFF_LOOSE_IBL;
   std::vector<float>   *AntiKtVR30Rmax4Rmin02TrackJets_m_TRK_EFF_LOOSE_IBL;
   std::vector<std::vector<float> > *AntiKtVR30Rmax4Rmin02TrackJets_daughter__pt_TRK_EFF_LOOSE_IBL;
   std::vector<std::vector<float> > *AntiKtVR30Rmax4Rmin02TrackJets_daughter__eta_TRK_EFF_LOOSE_IBL;
   std::vector<std::vector<float> > *AntiKtVR30Rmax4Rmin02TrackJets_daughter__phi_TRK_EFF_LOOSE_IBL;
   std::vector<std::vector<int> > *AntiKtVR30Rmax4Rmin02TrackJets_daughter__trackId_TRK_EFF_LOOSE_IBL;
   std::vector<float>   *DMesons_costhetastar_TRK_EFF_LOOSE_PHYSMODEL;
   std::vector<int>     *DMesons_D0Index_TRK_EFF_LOOSE_PHYSMODEL;
   std::vector<std::vector<float> > *DMesons_daughterInfo__eta_TRK_EFF_LOOSE_PHYSMODEL;
   std::vector<std::vector<bool> > *DMesons_daughterInfo__passTight_TRK_EFF_LOOSE_PHYSMODEL;
   std::vector<std::vector<int> > *DMesons_daughterInfo__pdgId_TRK_EFF_LOOSE_PHYSMODEL;
   std::vector<std::vector<float> > *DMesons_daughterInfo__phi_TRK_EFF_LOOSE_PHYSMODEL;
   std::vector<std::vector<float> > *DMesons_daughterInfo__pt_TRK_EFF_LOOSE_PHYSMODEL;
   std::vector<std::vector<int> > *DMesons_daughterInfo__trackId_TRK_EFF_LOOSE_PHYSMODEL;
   std::vector<std::vector<int> > *DMesons_daughterInfo__truthBarcode_TRK_EFF_LOOSE_PHYSMODEL;
   std::vector<std::vector<int> > *DMesons_daughterInfo__truthDBarcode_TRK_EFF_LOOSE_PHYSMODEL;
   std::vector<std::vector<float> > *DMesons_daughterInfo__z0SinTheta_TRK_EFF_LOOSE_PHYSMODEL;
   std::vector<std::vector<float> > *DMesons_daughterInfo__z0SinThetaPV_TRK_EFF_LOOSE_PHYSMODEL;
   std::vector<int>     *DMesons_decayType_TRK_EFF_LOOSE_PHYSMODEL;
   std::vector<float>   *DMesons_DeltaMass_TRK_EFF_LOOSE_PHYSMODEL;
   std::vector<float>   *DMesons_eta_TRK_EFF_LOOSE_PHYSMODEL;
   std::vector<int>     *DMesons_fitOutput__Charge_TRK_EFF_LOOSE_PHYSMODEL;
   std::vector<float>   *DMesons_fitOutput__Chi2_TRK_EFF_LOOSE_PHYSMODEL;
   std::vector<float>   *DMesons_fitOutput__Impact_TRK_EFF_LOOSE_PHYSMODEL;
   std::vector<float>   *DMesons_fitOutput__ImpactSignificance_TRK_EFF_LOOSE_PHYSMODEL;
   std::vector<float>   *DMesons_fitOutput__ImpactZ0SinTheta_TRK_EFF_LOOSE_PHYSMODEL;
   std::vector<float>   *DMesons_fitOutput__Lxy_TRK_EFF_LOOSE_PHYSMODEL;
   std::vector<std::vector<float> > *DMesons_fitOutput__VertexPosition_TRK_EFF_LOOSE_PHYSMODEL;
   std::vector<float>   *DMesons_m_TRK_EFF_LOOSE_PHYSMODEL;
   std::vector<float>   *DMesons_mKpi1_TRK_EFF_LOOSE_PHYSMODEL;
   std::vector<float>   *DMesons_mKpi2_TRK_EFF_LOOSE_PHYSMODEL;
   std::vector<float>   *DMesons_mPhi1_TRK_EFF_LOOSE_PHYSMODEL;
   std::vector<float>   *DMesons_mPhi2_TRK_EFF_LOOSE_PHYSMODEL;
   std::vector<int>     *DMesons_pdgId_TRK_EFF_LOOSE_PHYSMODEL;
   std::vector<float>   *DMesons_phi_TRK_EFF_LOOSE_PHYSMODEL;
   std::vector<float>   *DMesons_pt_TRK_EFF_LOOSE_PHYSMODEL;
   std::vector<float>   *DMesons_ptcone40_TRK_EFF_LOOSE_PHYSMODEL;
   std::vector<float>   *DMesons_SlowPionD0_TRK_EFF_LOOSE_PHYSMODEL;
   std::vector<float>   *DMesons_SlowPionZ0SinTheta_TRK_EFF_LOOSE_PHYSMODEL;
   std::vector<int>     *DMesons_truthBarcode_TRK_EFF_LOOSE_PHYSMODEL;
   std::vector<float>   *MesonTracks_d0sig_TRK_EFF_LOOSE_PHYSMODEL;
   std::vector<float>   *MesonTracks_d0sigPV_TRK_EFF_LOOSE_PHYSMODEL;
   std::vector<float>   *MesonTracks_eta_TRK_EFF_LOOSE_PHYSMODEL;
   std::vector<char>    *MesonTracks_passTight_TRK_EFF_LOOSE_PHYSMODEL;
   std::vector<float>   *MesonTracks_phi_TRK_EFF_LOOSE_PHYSMODEL;
   std::vector<float>   *MesonTracks_pt_TRK_EFF_LOOSE_PHYSMODEL;
   std::vector<int>     *MesonTracks_trackId_TRK_EFF_LOOSE_PHYSMODEL;
   std::vector<float>   *MesonTracks_z0sinTheta_TRK_EFF_LOOSE_PHYSMODEL;
   std::vector<float>   *MesonTracks_truthMatchProbability_TRK_EFF_LOOSE_PHYSMODEL;
   std::vector<float>   *AntiKt6PV0TrackJets_pt_TRK_EFF_LOOSE_PHYSMODEL;
   std::vector<float>   *AntiKt6PV0TrackJets_eta_TRK_EFF_LOOSE_PHYSMODEL;
   std::vector<float>   *AntiKt6PV0TrackJets_phi_TRK_EFF_LOOSE_PHYSMODEL;
   std::vector<float>   *AntiKt6PV0TrackJets_m_TRK_EFF_LOOSE_PHYSMODEL;
   std::vector<std::vector<float> > *AntiKt6PV0TrackJets_daughter__pt_TRK_EFF_LOOSE_PHYSMODEL;
   std::vector<std::vector<float> > *AntiKt6PV0TrackJets_daughter__eta_TRK_EFF_LOOSE_PHYSMODEL;
   std::vector<std::vector<float> > *AntiKt6PV0TrackJets_daughter__phi_TRK_EFF_LOOSE_PHYSMODEL;
   std::vector<std::vector<int> > *AntiKt6PV0TrackJets_daughter__trackId_TRK_EFF_LOOSE_PHYSMODEL;
   std::vector<float>   *AntiKt8PV0TrackJets_pt_TRK_EFF_LOOSE_PHYSMODEL;
   std::vector<float>   *AntiKt8PV0TrackJets_eta_TRK_EFF_LOOSE_PHYSMODEL;
   std::vector<float>   *AntiKt8PV0TrackJets_phi_TRK_EFF_LOOSE_PHYSMODEL;
   std::vector<float>   *AntiKt8PV0TrackJets_m_TRK_EFF_LOOSE_PHYSMODEL;
   std::vector<std::vector<float> > *AntiKt8PV0TrackJets_daughter__pt_TRK_EFF_LOOSE_PHYSMODEL;
   std::vector<std::vector<float> > *AntiKt8PV0TrackJets_daughter__eta_TRK_EFF_LOOSE_PHYSMODEL;
   std::vector<std::vector<float> > *AntiKt8PV0TrackJets_daughter__phi_TRK_EFF_LOOSE_PHYSMODEL;
   std::vector<std::vector<int> > *AntiKt8PV0TrackJets_daughter__trackId_TRK_EFF_LOOSE_PHYSMODEL;
   std::vector<float>   *AntiKt10PV0TrackJets_pt_TRK_EFF_LOOSE_PHYSMODEL;
   std::vector<float>   *AntiKt10PV0TrackJets_eta_TRK_EFF_LOOSE_PHYSMODEL;
   std::vector<float>   *AntiKt10PV0TrackJets_phi_TRK_EFF_LOOSE_PHYSMODEL;
   std::vector<float>   *AntiKt10PV0TrackJets_m_TRK_EFF_LOOSE_PHYSMODEL;
   std::vector<std::vector<float> > *AntiKt10PV0TrackJets_daughter__pt_TRK_EFF_LOOSE_PHYSMODEL;
   std::vector<std::vector<float> > *AntiKt10PV0TrackJets_daughter__eta_TRK_EFF_LOOSE_PHYSMODEL;
   std::vector<std::vector<float> > *AntiKt10PV0TrackJets_daughter__phi_TRK_EFF_LOOSE_PHYSMODEL;
   std::vector<std::vector<int> > *AntiKt10PV0TrackJets_daughter__trackId_TRK_EFF_LOOSE_PHYSMODEL;
   std::vector<float>   *AntiKtVR30Rmax4Rmin02TrackJets_pt_TRK_EFF_LOOSE_PHYSMODEL;
   std::vector<float>   *AntiKtVR30Rmax4Rmin02TrackJets_eta_TRK_EFF_LOOSE_PHYSMODEL;
   std::vector<float>   *AntiKtVR30Rmax4Rmin02TrackJets_phi_TRK_EFF_LOOSE_PHYSMODEL;
   std::vector<float>   *AntiKtVR30Rmax4Rmin02TrackJets_m_TRK_EFF_LOOSE_PHYSMODEL;
   std::vector<std::vector<float> > *AntiKtVR30Rmax4Rmin02TrackJets_daughter__pt_TRK_EFF_LOOSE_PHYSMODEL;
   std::vector<std::vector<float> > *AntiKtVR30Rmax4Rmin02TrackJets_daughter__eta_TRK_EFF_LOOSE_PHYSMODEL;
   std::vector<std::vector<float> > *AntiKtVR30Rmax4Rmin02TrackJets_daughter__phi_TRK_EFF_LOOSE_PHYSMODEL;
   std::vector<std::vector<int> > *AntiKtVR30Rmax4Rmin02TrackJets_daughter__trackId_TRK_EFF_LOOSE_PHYSMODEL;
   std::vector<float>   *DMesons_costhetastar_TRK_EFF_LOOSE_PP0;
   std::vector<int>     *DMesons_D0Index_TRK_EFF_LOOSE_PP0;
   std::vector<std::vector<float> > *DMesons_daughterInfo__eta_TRK_EFF_LOOSE_PP0;
   std::vector<std::vector<bool> > *DMesons_daughterInfo__passTight_TRK_EFF_LOOSE_PP0;
   std::vector<std::vector<int> > *DMesons_daughterInfo__pdgId_TRK_EFF_LOOSE_PP0;
   std::vector<std::vector<float> > *DMesons_daughterInfo__phi_TRK_EFF_LOOSE_PP0;
   std::vector<std::vector<float> > *DMesons_daughterInfo__pt_TRK_EFF_LOOSE_PP0;
   std::vector<std::vector<int> > *DMesons_daughterInfo__trackId_TRK_EFF_LOOSE_PP0;
   std::vector<std::vector<int> > *DMesons_daughterInfo__truthBarcode_TRK_EFF_LOOSE_PP0;
   std::vector<std::vector<int> > *DMesons_daughterInfo__truthDBarcode_TRK_EFF_LOOSE_PP0;
   std::vector<std::vector<float> > *DMesons_daughterInfo__z0SinTheta_TRK_EFF_LOOSE_PP0;
   std::vector<std::vector<float> > *DMesons_daughterInfo__z0SinThetaPV_TRK_EFF_LOOSE_PP0;
   std::vector<int>     *DMesons_decayType_TRK_EFF_LOOSE_PP0;
   std::vector<float>   *DMesons_DeltaMass_TRK_EFF_LOOSE_PP0;
   std::vector<float>   *DMesons_eta_TRK_EFF_LOOSE_PP0;
   std::vector<int>     *DMesons_fitOutput__Charge_TRK_EFF_LOOSE_PP0;
   std::vector<float>   *DMesons_fitOutput__Chi2_TRK_EFF_LOOSE_PP0;
   std::vector<float>   *DMesons_fitOutput__Impact_TRK_EFF_LOOSE_PP0;
   std::vector<float>   *DMesons_fitOutput__ImpactSignificance_TRK_EFF_LOOSE_PP0;
   std::vector<float>   *DMesons_fitOutput__ImpactZ0SinTheta_TRK_EFF_LOOSE_PP0;
   std::vector<float>   *DMesons_fitOutput__Lxy_TRK_EFF_LOOSE_PP0;
   std::vector<std::vector<float> > *DMesons_fitOutput__VertexPosition_TRK_EFF_LOOSE_PP0;
   std::vector<float>   *DMesons_m_TRK_EFF_LOOSE_PP0;
   std::vector<float>   *DMesons_mKpi1_TRK_EFF_LOOSE_PP0;
   std::vector<float>   *DMesons_mKpi2_TRK_EFF_LOOSE_PP0;
   std::vector<float>   *DMesons_mPhi1_TRK_EFF_LOOSE_PP0;
   std::vector<float>   *DMesons_mPhi2_TRK_EFF_LOOSE_PP0;
   std::vector<int>     *DMesons_pdgId_TRK_EFF_LOOSE_PP0;
   std::vector<float>   *DMesons_phi_TRK_EFF_LOOSE_PP0;
   std::vector<float>   *DMesons_pt_TRK_EFF_LOOSE_PP0;
   std::vector<float>   *DMesons_ptcone40_TRK_EFF_LOOSE_PP0;
   std::vector<float>   *DMesons_SlowPionD0_TRK_EFF_LOOSE_PP0;
   std::vector<float>   *DMesons_SlowPionZ0SinTheta_TRK_EFF_LOOSE_PP0;
   std::vector<int>     *DMesons_truthBarcode_TRK_EFF_LOOSE_PP0;
   std::vector<float>   *MesonTracks_d0sig_TRK_EFF_LOOSE_PP0;
   std::vector<float>   *MesonTracks_d0sigPV_TRK_EFF_LOOSE_PP0;
   std::vector<float>   *MesonTracks_eta_TRK_EFF_LOOSE_PP0;
   std::vector<char>    *MesonTracks_passTight_TRK_EFF_LOOSE_PP0;
   std::vector<float>   *MesonTracks_phi_TRK_EFF_LOOSE_PP0;
   std::vector<float>   *MesonTracks_pt_TRK_EFF_LOOSE_PP0;
   std::vector<int>     *MesonTracks_trackId_TRK_EFF_LOOSE_PP0;
   std::vector<float>   *MesonTracks_z0sinTheta_TRK_EFF_LOOSE_PP0;
   std::vector<float>   *MesonTracks_truthMatchProbability_TRK_EFF_LOOSE_PP0;
   std::vector<float>   *AntiKt6PV0TrackJets_pt_TRK_EFF_LOOSE_PP0;
   std::vector<float>   *AntiKt6PV0TrackJets_eta_TRK_EFF_LOOSE_PP0;
   std::vector<float>   *AntiKt6PV0TrackJets_phi_TRK_EFF_LOOSE_PP0;
   std::vector<float>   *AntiKt6PV0TrackJets_m_TRK_EFF_LOOSE_PP0;
   std::vector<std::vector<float> > *AntiKt6PV0TrackJets_daughter__pt_TRK_EFF_LOOSE_PP0;
   std::vector<std::vector<float> > *AntiKt6PV0TrackJets_daughter__eta_TRK_EFF_LOOSE_PP0;
   std::vector<std::vector<float> > *AntiKt6PV0TrackJets_daughter__phi_TRK_EFF_LOOSE_PP0;
   std::vector<std::vector<int> > *AntiKt6PV0TrackJets_daughter__trackId_TRK_EFF_LOOSE_PP0;
   std::vector<float>   *AntiKt8PV0TrackJets_pt_TRK_EFF_LOOSE_PP0;
   std::vector<float>   *AntiKt8PV0TrackJets_eta_TRK_EFF_LOOSE_PP0;
   std::vector<float>   *AntiKt8PV0TrackJets_phi_TRK_EFF_LOOSE_PP0;
   std::vector<float>   *AntiKt8PV0TrackJets_m_TRK_EFF_LOOSE_PP0;
   std::vector<std::vector<float> > *AntiKt8PV0TrackJets_daughter__pt_TRK_EFF_LOOSE_PP0;
   std::vector<std::vector<float> > *AntiKt8PV0TrackJets_daughter__eta_TRK_EFF_LOOSE_PP0;
   std::vector<std::vector<float> > *AntiKt8PV0TrackJets_daughter__phi_TRK_EFF_LOOSE_PP0;
   std::vector<std::vector<int> > *AntiKt8PV0TrackJets_daughter__trackId_TRK_EFF_LOOSE_PP0;
   std::vector<float>   *AntiKt10PV0TrackJets_pt_TRK_EFF_LOOSE_PP0;
   std::vector<float>   *AntiKt10PV0TrackJets_eta_TRK_EFF_LOOSE_PP0;
   std::vector<float>   *AntiKt10PV0TrackJets_phi_TRK_EFF_LOOSE_PP0;
   std::vector<float>   *AntiKt10PV0TrackJets_m_TRK_EFF_LOOSE_PP0;
   std::vector<std::vector<float> > *AntiKt10PV0TrackJets_daughter__pt_TRK_EFF_LOOSE_PP0;
   std::vector<std::vector<float> > *AntiKt10PV0TrackJets_daughter__eta_TRK_EFF_LOOSE_PP0;
   std::vector<std::vector<float> > *AntiKt10PV0TrackJets_daughter__phi_TRK_EFF_LOOSE_PP0;
   std::vector<std::vector<int> > *AntiKt10PV0TrackJets_daughter__trackId_TRK_EFF_LOOSE_PP0;
   std::vector<float>   *AntiKtVR30Rmax4Rmin02TrackJets_pt_TRK_EFF_LOOSE_PP0;
   std::vector<float>   *AntiKtVR30Rmax4Rmin02TrackJets_eta_TRK_EFF_LOOSE_PP0;
   std::vector<float>   *AntiKtVR30Rmax4Rmin02TrackJets_phi_TRK_EFF_LOOSE_PP0;
   std::vector<float>   *AntiKtVR30Rmax4Rmin02TrackJets_m_TRK_EFF_LOOSE_PP0;
   std::vector<std::vector<float> > *AntiKtVR30Rmax4Rmin02TrackJets_daughter__pt_TRK_EFF_LOOSE_PP0;
   std::vector<std::vector<float> > *AntiKtVR30Rmax4Rmin02TrackJets_daughter__eta_TRK_EFF_LOOSE_PP0;
   std::vector<std::vector<float> > *AntiKtVR30Rmax4Rmin02TrackJets_daughter__phi_TRK_EFF_LOOSE_PP0;
   std::vector<std::vector<int> > *AntiKtVR30Rmax4Rmin02TrackJets_daughter__trackId_TRK_EFF_LOOSE_PP0;
   std::vector<float>   *DMesons_costhetastar_TRK_FAKE_RATE_LOOSE;
   std::vector<int>     *DMesons_D0Index_TRK_FAKE_RATE_LOOSE;
   std::vector<std::vector<float> > *DMesons_daughterInfo__eta_TRK_FAKE_RATE_LOOSE;
   std::vector<std::vector<bool> > *DMesons_daughterInfo__passTight_TRK_FAKE_RATE_LOOSE;
   std::vector<std::vector<int> > *DMesons_daughterInfo__pdgId_TRK_FAKE_RATE_LOOSE;
   std::vector<std::vector<float> > *DMesons_daughterInfo__phi_TRK_FAKE_RATE_LOOSE;
   std::vector<std::vector<float> > *DMesons_daughterInfo__pt_TRK_FAKE_RATE_LOOSE;
   std::vector<std::vector<int> > *DMesons_daughterInfo__trackId_TRK_FAKE_RATE_LOOSE;
   std::vector<std::vector<int> > *DMesons_daughterInfo__truthBarcode_TRK_FAKE_RATE_LOOSE;
   std::vector<std::vector<int> > *DMesons_daughterInfo__truthDBarcode_TRK_FAKE_RATE_LOOSE;
   std::vector<std::vector<float> > *DMesons_daughterInfo__z0SinTheta_TRK_FAKE_RATE_LOOSE;
   std::vector<std::vector<float> > *DMesons_daughterInfo__z0SinThetaPV_TRK_FAKE_RATE_LOOSE;
   std::vector<int>     *DMesons_decayType_TRK_FAKE_RATE_LOOSE;
   std::vector<float>   *DMesons_DeltaMass_TRK_FAKE_RATE_LOOSE;
   std::vector<float>   *DMesons_eta_TRK_FAKE_RATE_LOOSE;
   std::vector<int>     *DMesons_fitOutput__Charge_TRK_FAKE_RATE_LOOSE;
   std::vector<float>   *DMesons_fitOutput__Chi2_TRK_FAKE_RATE_LOOSE;
   std::vector<float>   *DMesons_fitOutput__Impact_TRK_FAKE_RATE_LOOSE;
   std::vector<float>   *DMesons_fitOutput__ImpactSignificance_TRK_FAKE_RATE_LOOSE;
   std::vector<float>   *DMesons_fitOutput__ImpactZ0SinTheta_TRK_FAKE_RATE_LOOSE;
   std::vector<float>   *DMesons_fitOutput__Lxy_TRK_FAKE_RATE_LOOSE;
   std::vector<std::vector<float> > *DMesons_fitOutput__VertexPosition_TRK_FAKE_RATE_LOOSE;
   std::vector<float>   *DMesons_m_TRK_FAKE_RATE_LOOSE;
   std::vector<float>   *DMesons_mKpi1_TRK_FAKE_RATE_LOOSE;
   std::vector<float>   *DMesons_mKpi2_TRK_FAKE_RATE_LOOSE;
   std::vector<float>   *DMesons_mPhi1_TRK_FAKE_RATE_LOOSE;
   std::vector<float>   *DMesons_mPhi2_TRK_FAKE_RATE_LOOSE;
   std::vector<int>     *DMesons_pdgId_TRK_FAKE_RATE_LOOSE;
   std::vector<float>   *DMesons_phi_TRK_FAKE_RATE_LOOSE;
   std::vector<float>   *DMesons_pt_TRK_FAKE_RATE_LOOSE;
   std::vector<float>   *DMesons_ptcone40_TRK_FAKE_RATE_LOOSE;
   std::vector<float>   *DMesons_SlowPionD0_TRK_FAKE_RATE_LOOSE;
   std::vector<float>   *DMesons_SlowPionZ0SinTheta_TRK_FAKE_RATE_LOOSE;
   std::vector<int>     *DMesons_truthBarcode_TRK_FAKE_RATE_LOOSE;
   std::vector<float>   *MesonTracks_d0sig_TRK_FAKE_RATE_LOOSE;
   std::vector<float>   *MesonTracks_d0sigPV_TRK_FAKE_RATE_LOOSE;
   std::vector<float>   *MesonTracks_eta_TRK_FAKE_RATE_LOOSE;
   std::vector<char>    *MesonTracks_passTight_TRK_FAKE_RATE_LOOSE;
   std::vector<float>   *MesonTracks_phi_TRK_FAKE_RATE_LOOSE;
   std::vector<float>   *MesonTracks_pt_TRK_FAKE_RATE_LOOSE;
   std::vector<int>     *MesonTracks_trackId_TRK_FAKE_RATE_LOOSE;
   std::vector<float>   *MesonTracks_z0sinTheta_TRK_FAKE_RATE_LOOSE;
   std::vector<float>   *MesonTracks_truthMatchProbability_TRK_FAKE_RATE_LOOSE;
   std::vector<float>   *AntiKt6PV0TrackJets_pt_TRK_FAKE_RATE_LOOSE;
   std::vector<float>   *AntiKt6PV0TrackJets_eta_TRK_FAKE_RATE_LOOSE;
   std::vector<float>   *AntiKt6PV0TrackJets_phi_TRK_FAKE_RATE_LOOSE;
   std::vector<float>   *AntiKt6PV0TrackJets_m_TRK_FAKE_RATE_LOOSE;
   std::vector<std::vector<float> > *AntiKt6PV0TrackJets_daughter__pt_TRK_FAKE_RATE_LOOSE;
   std::vector<std::vector<float> > *AntiKt6PV0TrackJets_daughter__eta_TRK_FAKE_RATE_LOOSE;
   std::vector<std::vector<float> > *AntiKt6PV0TrackJets_daughter__phi_TRK_FAKE_RATE_LOOSE;
   std::vector<std::vector<int> > *AntiKt6PV0TrackJets_daughter__trackId_TRK_FAKE_RATE_LOOSE;
   std::vector<float>   *AntiKt8PV0TrackJets_pt_TRK_FAKE_RATE_LOOSE;
   std::vector<float>   *AntiKt8PV0TrackJets_eta_TRK_FAKE_RATE_LOOSE;
   std::vector<float>   *AntiKt8PV0TrackJets_phi_TRK_FAKE_RATE_LOOSE;
   std::vector<float>   *AntiKt8PV0TrackJets_m_TRK_FAKE_RATE_LOOSE;
   std::vector<std::vector<float> > *AntiKt8PV0TrackJets_daughter__pt_TRK_FAKE_RATE_LOOSE;
   std::vector<std::vector<float> > *AntiKt8PV0TrackJets_daughter__eta_TRK_FAKE_RATE_LOOSE;
   std::vector<std::vector<float> > *AntiKt8PV0TrackJets_daughter__phi_TRK_FAKE_RATE_LOOSE;
   std::vector<std::vector<int> > *AntiKt8PV0TrackJets_daughter__trackId_TRK_FAKE_RATE_LOOSE;
   std::vector<float>   *AntiKt10PV0TrackJets_pt_TRK_FAKE_RATE_LOOSE;
   std::vector<float>   *AntiKt10PV0TrackJets_eta_TRK_FAKE_RATE_LOOSE;
   std::vector<float>   *AntiKt10PV0TrackJets_phi_TRK_FAKE_RATE_LOOSE;
   std::vector<float>   *AntiKt10PV0TrackJets_m_TRK_FAKE_RATE_LOOSE;
   std::vector<std::vector<float> > *AntiKt10PV0TrackJets_daughter__pt_TRK_FAKE_RATE_LOOSE;
   std::vector<std::vector<float> > *AntiKt10PV0TrackJets_daughter__eta_TRK_FAKE_RATE_LOOSE;
   std::vector<std::vector<float> > *AntiKt10PV0TrackJets_daughter__phi_TRK_FAKE_RATE_LOOSE;
   std::vector<std::vector<int> > *AntiKt10PV0TrackJets_daughter__trackId_TRK_FAKE_RATE_LOOSE;
   std::vector<float>   *AntiKtVR30Rmax4Rmin02TrackJets_pt_TRK_FAKE_RATE_LOOSE;
   std::vector<float>   *AntiKtVR30Rmax4Rmin02TrackJets_eta_TRK_FAKE_RATE_LOOSE;
   std::vector<float>   *AntiKtVR30Rmax4Rmin02TrackJets_phi_TRK_FAKE_RATE_LOOSE;
   std::vector<float>   *AntiKtVR30Rmax4Rmin02TrackJets_m_TRK_FAKE_RATE_LOOSE;
   std::vector<std::vector<float> > *AntiKtVR30Rmax4Rmin02TrackJets_daughter__pt_TRK_FAKE_RATE_LOOSE;
   std::vector<std::vector<float> > *AntiKtVR30Rmax4Rmin02TrackJets_daughter__eta_TRK_FAKE_RATE_LOOSE;
   std::vector<std::vector<float> > *AntiKtVR30Rmax4Rmin02TrackJets_daughter__phi_TRK_FAKE_RATE_LOOSE;
   std::vector<std::vector<int> > *AntiKtVR30Rmax4Rmin02TrackJets_daughter__trackId_TRK_FAKE_RATE_LOOSE;
   std::vector<float>   *DMesons_costhetastar_TRK_RES_D0_DEAD;
   std::vector<int>     *DMesons_D0Index_TRK_RES_D0_DEAD;
   std::vector<std::vector<float> > *DMesons_daughterInfo__eta_TRK_RES_D0_DEAD;
   std::vector<std::vector<bool> > *DMesons_daughterInfo__passTight_TRK_RES_D0_DEAD;
   std::vector<std::vector<int> > *DMesons_daughterInfo__pdgId_TRK_RES_D0_DEAD;
   std::vector<std::vector<float> > *DMesons_daughterInfo__phi_TRK_RES_D0_DEAD;
   std::vector<std::vector<float> > *DMesons_daughterInfo__pt_TRK_RES_D0_DEAD;
   std::vector<std::vector<int> > *DMesons_daughterInfo__trackId_TRK_RES_D0_DEAD;
   std::vector<std::vector<int> > *DMesons_daughterInfo__truthBarcode_TRK_RES_D0_DEAD;
   std::vector<std::vector<int> > *DMesons_daughterInfo__truthDBarcode_TRK_RES_D0_DEAD;
   std::vector<std::vector<float> > *DMesons_daughterInfo__z0SinTheta_TRK_RES_D0_DEAD;
   std::vector<std::vector<float> > *DMesons_daughterInfo__z0SinThetaPV_TRK_RES_D0_DEAD;
   std::vector<int>     *DMesons_decayType_TRK_RES_D0_DEAD;
   std::vector<float>   *DMesons_DeltaMass_TRK_RES_D0_DEAD;
   std::vector<float>   *DMesons_eta_TRK_RES_D0_DEAD;
   std::vector<int>     *DMesons_fitOutput__Charge_TRK_RES_D0_DEAD;
   std::vector<float>   *DMesons_fitOutput__Chi2_TRK_RES_D0_DEAD;
   std::vector<float>   *DMesons_fitOutput__Impact_TRK_RES_D0_DEAD;
   std::vector<float>   *DMesons_fitOutput__ImpactSignificance_TRK_RES_D0_DEAD;
   std::vector<float>   *DMesons_fitOutput__ImpactZ0SinTheta_TRK_RES_D0_DEAD;
   std::vector<float>   *DMesons_fitOutput__Lxy_TRK_RES_D0_DEAD;
   std::vector<std::vector<float> > *DMesons_fitOutput__VertexPosition_TRK_RES_D0_DEAD;
   std::vector<float>   *DMesons_m_TRK_RES_D0_DEAD;
   std::vector<float>   *DMesons_mKpi1_TRK_RES_D0_DEAD;
   std::vector<float>   *DMesons_mKpi2_TRK_RES_D0_DEAD;
   std::vector<float>   *DMesons_mPhi1_TRK_RES_D0_DEAD;
   std::vector<float>   *DMesons_mPhi2_TRK_RES_D0_DEAD;
   std::vector<int>     *DMesons_pdgId_TRK_RES_D0_DEAD;
   std::vector<float>   *DMesons_phi_TRK_RES_D0_DEAD;
   std::vector<float>   *DMesons_pt_TRK_RES_D0_DEAD;
   std::vector<float>   *DMesons_ptcone40_TRK_RES_D0_DEAD;
   std::vector<float>   *DMesons_SlowPionD0_TRK_RES_D0_DEAD;
   std::vector<float>   *DMesons_SlowPionZ0SinTheta_TRK_RES_D0_DEAD;
   std::vector<int>     *DMesons_truthBarcode_TRK_RES_D0_DEAD;
   std::vector<float>   *MesonTracks_d0sig_TRK_RES_D0_DEAD;
   std::vector<float>   *MesonTracks_d0sigPV_TRK_RES_D0_DEAD;
   std::vector<float>   *MesonTracks_eta_TRK_RES_D0_DEAD;
   std::vector<char>    *MesonTracks_passTight_TRK_RES_D0_DEAD;
   std::vector<float>   *MesonTracks_phi_TRK_RES_D0_DEAD;
   std::vector<float>   *MesonTracks_pt_TRK_RES_D0_DEAD;
   std::vector<int>     *MesonTracks_trackId_TRK_RES_D0_DEAD;
   std::vector<float>   *MesonTracks_z0sinTheta_TRK_RES_D0_DEAD;
   std::vector<float>   *MesonTracks_truthMatchProbability_TRK_RES_D0_DEAD;
   std::vector<float>   *AntiKt6PV0TrackJets_pt_TRK_RES_D0_DEAD;
   std::vector<float>   *AntiKt6PV0TrackJets_eta_TRK_RES_D0_DEAD;
   std::vector<float>   *AntiKt6PV0TrackJets_phi_TRK_RES_D0_DEAD;
   std::vector<float>   *AntiKt6PV0TrackJets_m_TRK_RES_D0_DEAD;
   std::vector<std::vector<float> > *AntiKt6PV0TrackJets_daughter__pt_TRK_RES_D0_DEAD;
   std::vector<std::vector<float> > *AntiKt6PV0TrackJets_daughter__eta_TRK_RES_D0_DEAD;
   std::vector<std::vector<float> > *AntiKt6PV0TrackJets_daughter__phi_TRK_RES_D0_DEAD;
   std::vector<std::vector<int> > *AntiKt6PV0TrackJets_daughter__trackId_TRK_RES_D0_DEAD;
   std::vector<float>   *AntiKt8PV0TrackJets_pt_TRK_RES_D0_DEAD;
   std::vector<float>   *AntiKt8PV0TrackJets_eta_TRK_RES_D0_DEAD;
   std::vector<float>   *AntiKt8PV0TrackJets_phi_TRK_RES_D0_DEAD;
   std::vector<float>   *AntiKt8PV0TrackJets_m_TRK_RES_D0_DEAD;
   std::vector<std::vector<float> > *AntiKt8PV0TrackJets_daughter__pt_TRK_RES_D0_DEAD;
   std::vector<std::vector<float> > *AntiKt8PV0TrackJets_daughter__eta_TRK_RES_D0_DEAD;
   std::vector<std::vector<float> > *AntiKt8PV0TrackJets_daughter__phi_TRK_RES_D0_DEAD;
   std::vector<std::vector<int> > *AntiKt8PV0TrackJets_daughter__trackId_TRK_RES_D0_DEAD;
   std::vector<float>   *AntiKt10PV0TrackJets_pt_TRK_RES_D0_DEAD;
   std::vector<float>   *AntiKt10PV0TrackJets_eta_TRK_RES_D0_DEAD;
   std::vector<float>   *AntiKt10PV0TrackJets_phi_TRK_RES_D0_DEAD;
   std::vector<float>   *AntiKt10PV0TrackJets_m_TRK_RES_D0_DEAD;
   std::vector<std::vector<float> > *AntiKt10PV0TrackJets_daughter__pt_TRK_RES_D0_DEAD;
   std::vector<std::vector<float> > *AntiKt10PV0TrackJets_daughter__eta_TRK_RES_D0_DEAD;
   std::vector<std::vector<float> > *AntiKt10PV0TrackJets_daughter__phi_TRK_RES_D0_DEAD;
   std::vector<std::vector<int> > *AntiKt10PV0TrackJets_daughter__trackId_TRK_RES_D0_DEAD;
   std::vector<float>   *AntiKtVR30Rmax4Rmin02TrackJets_pt_TRK_RES_D0_DEAD;
   std::vector<float>   *AntiKtVR30Rmax4Rmin02TrackJets_eta_TRK_RES_D0_DEAD;
   std::vector<float>   *AntiKtVR30Rmax4Rmin02TrackJets_phi_TRK_RES_D0_DEAD;
   std::vector<float>   *AntiKtVR30Rmax4Rmin02TrackJets_m_TRK_RES_D0_DEAD;
   std::vector<std::vector<float> > *AntiKtVR30Rmax4Rmin02TrackJets_daughter__pt_TRK_RES_D0_DEAD;
   std::vector<std::vector<float> > *AntiKtVR30Rmax4Rmin02TrackJets_daughter__eta_TRK_RES_D0_DEAD;
   std::vector<std::vector<float> > *AntiKtVR30Rmax4Rmin02TrackJets_daughter__phi_TRK_RES_D0_DEAD;
   std::vector<std::vector<int> > *AntiKtVR30Rmax4Rmin02TrackJets_daughter__trackId_TRK_RES_D0_DEAD;
   std::vector<float>   *DMesons_costhetastar_TRK_RES_D0_MEAS;
   std::vector<int>     *DMesons_D0Index_TRK_RES_D0_MEAS;
   std::vector<std::vector<float> > *DMesons_daughterInfo__eta_TRK_RES_D0_MEAS;
   std::vector<std::vector<bool> > *DMesons_daughterInfo__passTight_TRK_RES_D0_MEAS;
   std::vector<std::vector<int> > *DMesons_daughterInfo__pdgId_TRK_RES_D0_MEAS;
   std::vector<std::vector<float> > *DMesons_daughterInfo__phi_TRK_RES_D0_MEAS;
   std::vector<std::vector<float> > *DMesons_daughterInfo__pt_TRK_RES_D0_MEAS;
   std::vector<std::vector<int> > *DMesons_daughterInfo__trackId_TRK_RES_D0_MEAS;
   std::vector<std::vector<int> > *DMesons_daughterInfo__truthBarcode_TRK_RES_D0_MEAS;
   std::vector<std::vector<int> > *DMesons_daughterInfo__truthDBarcode_TRK_RES_D0_MEAS;
   std::vector<std::vector<float> > *DMesons_daughterInfo__z0SinTheta_TRK_RES_D0_MEAS;
   std::vector<std::vector<float> > *DMesons_daughterInfo__z0SinThetaPV_TRK_RES_D0_MEAS;
   std::vector<int>     *DMesons_decayType_TRK_RES_D0_MEAS;
   std::vector<float>   *DMesons_DeltaMass_TRK_RES_D0_MEAS;
   std::vector<float>   *DMesons_eta_TRK_RES_D0_MEAS;
   std::vector<int>     *DMesons_fitOutput__Charge_TRK_RES_D0_MEAS;
   std::vector<float>   *DMesons_fitOutput__Chi2_TRK_RES_D0_MEAS;
   std::vector<float>   *DMesons_fitOutput__Impact_TRK_RES_D0_MEAS;
   std::vector<float>   *DMesons_fitOutput__ImpactSignificance_TRK_RES_D0_MEAS;
   std::vector<float>   *DMesons_fitOutput__ImpactZ0SinTheta_TRK_RES_D0_MEAS;
   std::vector<float>   *DMesons_fitOutput__Lxy_TRK_RES_D0_MEAS;
   std::vector<std::vector<float> > *DMesons_fitOutput__VertexPosition_TRK_RES_D0_MEAS;
   std::vector<float>   *DMesons_m_TRK_RES_D0_MEAS;
   std::vector<float>   *DMesons_mKpi1_TRK_RES_D0_MEAS;
   std::vector<float>   *DMesons_mKpi2_TRK_RES_D0_MEAS;
   std::vector<float>   *DMesons_mPhi1_TRK_RES_D0_MEAS;
   std::vector<float>   *DMesons_mPhi2_TRK_RES_D0_MEAS;
   std::vector<int>     *DMesons_pdgId_TRK_RES_D0_MEAS;
   std::vector<float>   *DMesons_phi_TRK_RES_D0_MEAS;
   std::vector<float>   *DMesons_pt_TRK_RES_D0_MEAS;
   std::vector<float>   *DMesons_ptcone40_TRK_RES_D0_MEAS;
   std::vector<float>   *DMesons_SlowPionD0_TRK_RES_D0_MEAS;
   std::vector<float>   *DMesons_SlowPionZ0SinTheta_TRK_RES_D0_MEAS;
   std::vector<int>     *DMesons_truthBarcode_TRK_RES_D0_MEAS;
   std::vector<float>   *MesonTracks_d0sig_TRK_RES_D0_MEAS;
   std::vector<float>   *MesonTracks_d0sigPV_TRK_RES_D0_MEAS;
   std::vector<float>   *MesonTracks_eta_TRK_RES_D0_MEAS;
   std::vector<char>    *MesonTracks_passTight_TRK_RES_D0_MEAS;
   std::vector<float>   *MesonTracks_phi_TRK_RES_D0_MEAS;
   std::vector<float>   *MesonTracks_pt_TRK_RES_D0_MEAS;
   std::vector<int>     *MesonTracks_trackId_TRK_RES_D0_MEAS;
   std::vector<float>   *MesonTracks_z0sinTheta_TRK_RES_D0_MEAS;
   std::vector<float>   *MesonTracks_truthMatchProbability_TRK_RES_D0_MEAS;
   std::vector<float>   *AntiKt6PV0TrackJets_pt_TRK_RES_D0_MEAS;
   std::vector<float>   *AntiKt6PV0TrackJets_eta_TRK_RES_D0_MEAS;
   std::vector<float>   *AntiKt6PV0TrackJets_phi_TRK_RES_D0_MEAS;
   std::vector<float>   *AntiKt6PV0TrackJets_m_TRK_RES_D0_MEAS;
   std::vector<std::vector<float> > *AntiKt6PV0TrackJets_daughter__pt_TRK_RES_D0_MEAS;
   std::vector<std::vector<float> > *AntiKt6PV0TrackJets_daughter__eta_TRK_RES_D0_MEAS;
   std::vector<std::vector<float> > *AntiKt6PV0TrackJets_daughter__phi_TRK_RES_D0_MEAS;
   std::vector<std::vector<int> > *AntiKt6PV0TrackJets_daughter__trackId_TRK_RES_D0_MEAS;
   std::vector<float>   *AntiKt8PV0TrackJets_pt_TRK_RES_D0_MEAS;
   std::vector<float>   *AntiKt8PV0TrackJets_eta_TRK_RES_D0_MEAS;
   std::vector<float>   *AntiKt8PV0TrackJets_phi_TRK_RES_D0_MEAS;
   std::vector<float>   *AntiKt8PV0TrackJets_m_TRK_RES_D0_MEAS;
   std::vector<std::vector<float> > *AntiKt8PV0TrackJets_daughter__pt_TRK_RES_D0_MEAS;
   std::vector<std::vector<float> > *AntiKt8PV0TrackJets_daughter__eta_TRK_RES_D0_MEAS;
   std::vector<std::vector<float> > *AntiKt8PV0TrackJets_daughter__phi_TRK_RES_D0_MEAS;
   std::vector<std::vector<int> > *AntiKt8PV0TrackJets_daughter__trackId_TRK_RES_D0_MEAS;
   std::vector<float>   *AntiKt10PV0TrackJets_pt_TRK_RES_D0_MEAS;
   std::vector<float>   *AntiKt10PV0TrackJets_eta_TRK_RES_D0_MEAS;
   std::vector<float>   *AntiKt10PV0TrackJets_phi_TRK_RES_D0_MEAS;
   std::vector<float>   *AntiKt10PV0TrackJets_m_TRK_RES_D0_MEAS;
   std::vector<std::vector<float> > *AntiKt10PV0TrackJets_daughter__pt_TRK_RES_D0_MEAS;
   std::vector<std::vector<float> > *AntiKt10PV0TrackJets_daughter__eta_TRK_RES_D0_MEAS;
   std::vector<std::vector<float> > *AntiKt10PV0TrackJets_daughter__phi_TRK_RES_D0_MEAS;
   std::vector<std::vector<int> > *AntiKt10PV0TrackJets_daughter__trackId_TRK_RES_D0_MEAS;
   std::vector<float>   *AntiKtVR30Rmax4Rmin02TrackJets_pt_TRK_RES_D0_MEAS;
   std::vector<float>   *AntiKtVR30Rmax4Rmin02TrackJets_eta_TRK_RES_D0_MEAS;
   std::vector<float>   *AntiKtVR30Rmax4Rmin02TrackJets_phi_TRK_RES_D0_MEAS;
   std::vector<float>   *AntiKtVR30Rmax4Rmin02TrackJets_m_TRK_RES_D0_MEAS;
   std::vector<std::vector<float> > *AntiKtVR30Rmax4Rmin02TrackJets_daughter__pt_TRK_RES_D0_MEAS;
   std::vector<std::vector<float> > *AntiKtVR30Rmax4Rmin02TrackJets_daughter__eta_TRK_RES_D0_MEAS;
   std::vector<std::vector<float> > *AntiKtVR30Rmax4Rmin02TrackJets_daughter__phi_TRK_RES_D0_MEAS;
   std::vector<std::vector<int> > *AntiKtVR30Rmax4Rmin02TrackJets_daughter__trackId_TRK_RES_D0_MEAS;
   std::vector<float>   *DMesons_costhetastar_TRK_RES_Z0_DEAD;
   std::vector<int>     *DMesons_D0Index_TRK_RES_Z0_DEAD;
   std::vector<std::vector<float> > *DMesons_daughterInfo__eta_TRK_RES_Z0_DEAD;
   std::vector<std::vector<bool> > *DMesons_daughterInfo__passTight_TRK_RES_Z0_DEAD;
   std::vector<std::vector<int> > *DMesons_daughterInfo__pdgId_TRK_RES_Z0_DEAD;
   std::vector<std::vector<float> > *DMesons_daughterInfo__phi_TRK_RES_Z0_DEAD;
   std::vector<std::vector<float> > *DMesons_daughterInfo__pt_TRK_RES_Z0_DEAD;
   std::vector<std::vector<int> > *DMesons_daughterInfo__trackId_TRK_RES_Z0_DEAD;
   std::vector<std::vector<int> > *DMesons_daughterInfo__truthBarcode_TRK_RES_Z0_DEAD;
   std::vector<std::vector<int> > *DMesons_daughterInfo__truthDBarcode_TRK_RES_Z0_DEAD;
   std::vector<std::vector<float> > *DMesons_daughterInfo__z0SinTheta_TRK_RES_Z0_DEAD;
   std::vector<std::vector<float> > *DMesons_daughterInfo__z0SinThetaPV_TRK_RES_Z0_DEAD;
   std::vector<int>     *DMesons_decayType_TRK_RES_Z0_DEAD;
   std::vector<float>   *DMesons_DeltaMass_TRK_RES_Z0_DEAD;
   std::vector<float>   *DMesons_eta_TRK_RES_Z0_DEAD;
   std::vector<int>     *DMesons_fitOutput__Charge_TRK_RES_Z0_DEAD;
   std::vector<float>   *DMesons_fitOutput__Chi2_TRK_RES_Z0_DEAD;
   std::vector<float>   *DMesons_fitOutput__Impact_TRK_RES_Z0_DEAD;
   std::vector<float>   *DMesons_fitOutput__ImpactSignificance_TRK_RES_Z0_DEAD;
   std::vector<float>   *DMesons_fitOutput__ImpactZ0SinTheta_TRK_RES_Z0_DEAD;
   std::vector<float>   *DMesons_fitOutput__Lxy_TRK_RES_Z0_DEAD;
   std::vector<std::vector<float> > *DMesons_fitOutput__VertexPosition_TRK_RES_Z0_DEAD;
   std::vector<float>   *DMesons_m_TRK_RES_Z0_DEAD;
   std::vector<float>   *DMesons_mKpi1_TRK_RES_Z0_DEAD;
   std::vector<float>   *DMesons_mKpi2_TRK_RES_Z0_DEAD;
   std::vector<float>   *DMesons_mPhi1_TRK_RES_Z0_DEAD;
   std::vector<float>   *DMesons_mPhi2_TRK_RES_Z0_DEAD;
   std::vector<int>     *DMesons_pdgId_TRK_RES_Z0_DEAD;
   std::vector<float>   *DMesons_phi_TRK_RES_Z0_DEAD;
   std::vector<float>   *DMesons_pt_TRK_RES_Z0_DEAD;
   std::vector<float>   *DMesons_ptcone40_TRK_RES_Z0_DEAD;
   std::vector<float>   *DMesons_SlowPionD0_TRK_RES_Z0_DEAD;
   std::vector<float>   *DMesons_SlowPionZ0SinTheta_TRK_RES_Z0_DEAD;
   std::vector<int>     *DMesons_truthBarcode_TRK_RES_Z0_DEAD;
   std::vector<float>   *MesonTracks_d0sig_TRK_RES_Z0_DEAD;
   std::vector<float>   *MesonTracks_d0sigPV_TRK_RES_Z0_DEAD;
   std::vector<float>   *MesonTracks_eta_TRK_RES_Z0_DEAD;
   std::vector<char>    *MesonTracks_passTight_TRK_RES_Z0_DEAD;
   std::vector<float>   *MesonTracks_phi_TRK_RES_Z0_DEAD;
   std::vector<float>   *MesonTracks_pt_TRK_RES_Z0_DEAD;
   std::vector<int>     *MesonTracks_trackId_TRK_RES_Z0_DEAD;
   std::vector<float>   *MesonTracks_z0sinTheta_TRK_RES_Z0_DEAD;
   std::vector<float>   *MesonTracks_truthMatchProbability_TRK_RES_Z0_DEAD;
   std::vector<float>   *AntiKt6PV0TrackJets_pt_TRK_RES_Z0_DEAD;
   std::vector<float>   *AntiKt6PV0TrackJets_eta_TRK_RES_Z0_DEAD;
   std::vector<float>   *AntiKt6PV0TrackJets_phi_TRK_RES_Z0_DEAD;
   std::vector<float>   *AntiKt6PV0TrackJets_m_TRK_RES_Z0_DEAD;
   std::vector<std::vector<float> > *AntiKt6PV0TrackJets_daughter__pt_TRK_RES_Z0_DEAD;
   std::vector<std::vector<float> > *AntiKt6PV0TrackJets_daughter__eta_TRK_RES_Z0_DEAD;
   std::vector<std::vector<float> > *AntiKt6PV0TrackJets_daughter__phi_TRK_RES_Z0_DEAD;
   std::vector<std::vector<int> > *AntiKt6PV0TrackJets_daughter__trackId_TRK_RES_Z0_DEAD;
   std::vector<float>   *AntiKt8PV0TrackJets_pt_TRK_RES_Z0_DEAD;
   std::vector<float>   *AntiKt8PV0TrackJets_eta_TRK_RES_Z0_DEAD;
   std::vector<float>   *AntiKt8PV0TrackJets_phi_TRK_RES_Z0_DEAD;
   std::vector<float>   *AntiKt8PV0TrackJets_m_TRK_RES_Z0_DEAD;
   std::vector<std::vector<float> > *AntiKt8PV0TrackJets_daughter__pt_TRK_RES_Z0_DEAD;
   std::vector<std::vector<float> > *AntiKt8PV0TrackJets_daughter__eta_TRK_RES_Z0_DEAD;
   std::vector<std::vector<float> > *AntiKt8PV0TrackJets_daughter__phi_TRK_RES_Z0_DEAD;
   std::vector<std::vector<int> > *AntiKt8PV0TrackJets_daughter__trackId_TRK_RES_Z0_DEAD;
   std::vector<float>   *AntiKt10PV0TrackJets_pt_TRK_RES_Z0_DEAD;
   std::vector<float>   *AntiKt10PV0TrackJets_eta_TRK_RES_Z0_DEAD;
   std::vector<float>   *AntiKt10PV0TrackJets_phi_TRK_RES_Z0_DEAD;
   std::vector<float>   *AntiKt10PV0TrackJets_m_TRK_RES_Z0_DEAD;
   std::vector<std::vector<float> > *AntiKt10PV0TrackJets_daughter__pt_TRK_RES_Z0_DEAD;
   std::vector<std::vector<float> > *AntiKt10PV0TrackJets_daughter__eta_TRK_RES_Z0_DEAD;
   std::vector<std::vector<float> > *AntiKt10PV0TrackJets_daughter__phi_TRK_RES_Z0_DEAD;
   std::vector<std::vector<int> > *AntiKt10PV0TrackJets_daughter__trackId_TRK_RES_Z0_DEAD;
   std::vector<float>   *AntiKtVR30Rmax4Rmin02TrackJets_pt_TRK_RES_Z0_DEAD;
   std::vector<float>   *AntiKtVR30Rmax4Rmin02TrackJets_eta_TRK_RES_Z0_DEAD;
   std::vector<float>   *AntiKtVR30Rmax4Rmin02TrackJets_phi_TRK_RES_Z0_DEAD;
   std::vector<float>   *AntiKtVR30Rmax4Rmin02TrackJets_m_TRK_RES_Z0_DEAD;
   std::vector<std::vector<float> > *AntiKtVR30Rmax4Rmin02TrackJets_daughter__pt_TRK_RES_Z0_DEAD;
   std::vector<std::vector<float> > *AntiKtVR30Rmax4Rmin02TrackJets_daughter__eta_TRK_RES_Z0_DEAD;
   std::vector<std::vector<float> > *AntiKtVR30Rmax4Rmin02TrackJets_daughter__phi_TRK_RES_Z0_DEAD;
   std::vector<std::vector<int> > *AntiKtVR30Rmax4Rmin02TrackJets_daughter__trackId_TRK_RES_Z0_DEAD;
   std::vector<float>   *DMesons_costhetastar_TRK_RES_Z0_MEAS;
   std::vector<int>     *DMesons_D0Index_TRK_RES_Z0_MEAS;
   std::vector<std::vector<float> > *DMesons_daughterInfo__eta_TRK_RES_Z0_MEAS;
   std::vector<std::vector<bool> > *DMesons_daughterInfo__passTight_TRK_RES_Z0_MEAS;
   std::vector<std::vector<int> > *DMesons_daughterInfo__pdgId_TRK_RES_Z0_MEAS;
   std::vector<std::vector<float> > *DMesons_daughterInfo__phi_TRK_RES_Z0_MEAS;
   std::vector<std::vector<float> > *DMesons_daughterInfo__pt_TRK_RES_Z0_MEAS;
   std::vector<std::vector<int> > *DMesons_daughterInfo__trackId_TRK_RES_Z0_MEAS;
   std::vector<std::vector<int> > *DMesons_daughterInfo__truthBarcode_TRK_RES_Z0_MEAS;
   std::vector<std::vector<int> > *DMesons_daughterInfo__truthDBarcode_TRK_RES_Z0_MEAS;
   std::vector<std::vector<float> > *DMesons_daughterInfo__z0SinTheta_TRK_RES_Z0_MEAS;
   std::vector<std::vector<float> > *DMesons_daughterInfo__z0SinThetaPV_TRK_RES_Z0_MEAS;
   std::vector<int>     *DMesons_decayType_TRK_RES_Z0_MEAS;
   std::vector<float>   *DMesons_DeltaMass_TRK_RES_Z0_MEAS;
   std::vector<float>   *DMesons_eta_TRK_RES_Z0_MEAS;
   std::vector<int>     *DMesons_fitOutput__Charge_TRK_RES_Z0_MEAS;
   std::vector<float>   *DMesons_fitOutput__Chi2_TRK_RES_Z0_MEAS;
   std::vector<float>   *DMesons_fitOutput__Impact_TRK_RES_Z0_MEAS;
   std::vector<float>   *DMesons_fitOutput__ImpactSignificance_TRK_RES_Z0_MEAS;
   std::vector<float>   *DMesons_fitOutput__ImpactZ0SinTheta_TRK_RES_Z0_MEAS;
   std::vector<float>   *DMesons_fitOutput__Lxy_TRK_RES_Z0_MEAS;
   std::vector<std::vector<float> > *DMesons_fitOutput__VertexPosition_TRK_RES_Z0_MEAS;
   std::vector<float>   *DMesons_m_TRK_RES_Z0_MEAS;
   std::vector<float>   *DMesons_mKpi1_TRK_RES_Z0_MEAS;
   std::vector<float>   *DMesons_mKpi2_TRK_RES_Z0_MEAS;
   std::vector<float>   *DMesons_mPhi1_TRK_RES_Z0_MEAS;
   std::vector<float>   *DMesons_mPhi2_TRK_RES_Z0_MEAS;
   std::vector<int>     *DMesons_pdgId_TRK_RES_Z0_MEAS;
   std::vector<float>   *DMesons_phi_TRK_RES_Z0_MEAS;
   std::vector<float>   *DMesons_pt_TRK_RES_Z0_MEAS;
   std::vector<float>   *DMesons_ptcone40_TRK_RES_Z0_MEAS;
   std::vector<float>   *DMesons_SlowPionD0_TRK_RES_Z0_MEAS;
   std::vector<float>   *DMesons_SlowPionZ0SinTheta_TRK_RES_Z0_MEAS;
   std::vector<int>     *DMesons_truthBarcode_TRK_RES_Z0_MEAS;
   std::vector<float>   *MesonTracks_d0sig_TRK_RES_Z0_MEAS;
   std::vector<float>   *MesonTracks_d0sigPV_TRK_RES_Z0_MEAS;
   std::vector<float>   *MesonTracks_eta_TRK_RES_Z0_MEAS;
   std::vector<char>    *MesonTracks_passTight_TRK_RES_Z0_MEAS;
   std::vector<float>   *MesonTracks_phi_TRK_RES_Z0_MEAS;
   std::vector<float>   *MesonTracks_pt_TRK_RES_Z0_MEAS;
   std::vector<int>     *MesonTracks_trackId_TRK_RES_Z0_MEAS;
   std::vector<float>   *MesonTracks_z0sinTheta_TRK_RES_Z0_MEAS;
   std::vector<float>   *MesonTracks_truthMatchProbability_TRK_RES_Z0_MEAS;
   std::vector<float>   *AntiKt6PV0TrackJets_pt_TRK_RES_Z0_MEAS;
   std::vector<float>   *AntiKt6PV0TrackJets_eta_TRK_RES_Z0_MEAS;
   std::vector<float>   *AntiKt6PV0TrackJets_phi_TRK_RES_Z0_MEAS;
   std::vector<float>   *AntiKt6PV0TrackJets_m_TRK_RES_Z0_MEAS;
   std::vector<std::vector<float> > *AntiKt6PV0TrackJets_daughter__pt_TRK_RES_Z0_MEAS;
   std::vector<std::vector<float> > *AntiKt6PV0TrackJets_daughter__eta_TRK_RES_Z0_MEAS;
   std::vector<std::vector<float> > *AntiKt6PV0TrackJets_daughter__phi_TRK_RES_Z0_MEAS;
   std::vector<std::vector<int> > *AntiKt6PV0TrackJets_daughter__trackId_TRK_RES_Z0_MEAS;
   std::vector<float>   *AntiKt8PV0TrackJets_pt_TRK_RES_Z0_MEAS;
   std::vector<float>   *AntiKt8PV0TrackJets_eta_TRK_RES_Z0_MEAS;
   std::vector<float>   *AntiKt8PV0TrackJets_phi_TRK_RES_Z0_MEAS;
   std::vector<float>   *AntiKt8PV0TrackJets_m_TRK_RES_Z0_MEAS;
   std::vector<std::vector<float> > *AntiKt8PV0TrackJets_daughter__pt_TRK_RES_Z0_MEAS;
   std::vector<std::vector<float> > *AntiKt8PV0TrackJets_daughter__eta_TRK_RES_Z0_MEAS;
   std::vector<std::vector<float> > *AntiKt8PV0TrackJets_daughter__phi_TRK_RES_Z0_MEAS;
   std::vector<std::vector<int> > *AntiKt8PV0TrackJets_daughter__trackId_TRK_RES_Z0_MEAS;
   std::vector<float>   *AntiKt10PV0TrackJets_pt_TRK_RES_Z0_MEAS;
   std::vector<float>   *AntiKt10PV0TrackJets_eta_TRK_RES_Z0_MEAS;
   std::vector<float>   *AntiKt10PV0TrackJets_phi_TRK_RES_Z0_MEAS;
   std::vector<float>   *AntiKt10PV0TrackJets_m_TRK_RES_Z0_MEAS;
   std::vector<std::vector<float> > *AntiKt10PV0TrackJets_daughter__pt_TRK_RES_Z0_MEAS;
   std::vector<std::vector<float> > *AntiKt10PV0TrackJets_daughter__eta_TRK_RES_Z0_MEAS;
   std::vector<std::vector<float> > *AntiKt10PV0TrackJets_daughter__phi_TRK_RES_Z0_MEAS;
   std::vector<std::vector<int> > *AntiKt10PV0TrackJets_daughter__trackId_TRK_RES_Z0_MEAS;
   std::vector<float>   *AntiKtVR30Rmax4Rmin02TrackJets_pt_TRK_RES_Z0_MEAS;
   std::vector<float>   *AntiKtVR30Rmax4Rmin02TrackJets_eta_TRK_RES_Z0_MEAS;
   std::vector<float>   *AntiKtVR30Rmax4Rmin02TrackJets_phi_TRK_RES_Z0_MEAS;
   std::vector<float>   *AntiKtVR30Rmax4Rmin02TrackJets_m_TRK_RES_Z0_MEAS;
   std::vector<std::vector<float> > *AntiKtVR30Rmax4Rmin02TrackJets_daughter__pt_TRK_RES_Z0_MEAS;
   std::vector<std::vector<float> > *AntiKtVR30Rmax4Rmin02TrackJets_daughter__eta_TRK_RES_Z0_MEAS;
   std::vector<std::vector<float> > *AntiKtVR30Rmax4Rmin02TrackJets_daughter__phi_TRK_RES_Z0_MEAS;
   std::vector<std::vector<int> > *AntiKtVR30Rmax4Rmin02TrackJets_daughter__trackId_TRK_RES_Z0_MEAS;

   // List of branches
   TBranch        *b_EventInfo_eventNumber;   //!
   TBranch        *b_EventInfo_RandomRunNumber;   //!
   TBranch        *b_EventInfo_PileupWeight_NOSYS;   //!
   TBranch        *b_EventInfo_correctedScaled_averageInteractionsPerCrossing;   //!
   TBranch        *b_EventInfo_correctedScaled_actualInteractionsPerCrossing;   //!
   TBranch        *b_EventInfo_generatorWeight_NOSYS;   //!
   TBranch        *b_EventInfo_prodFracWeight_NOSYS;   //!
   TBranch        *b_EventInfo_trigPassed_HLT_e24_lhmedium_L1EM20VH;   //!
   TBranch        *b_EventInfo_trigPassed_HLT_e60_lhmedium;   //!
   TBranch        *b_EventInfo_trigPassed_HLT_e120_lhloose;   //!
   TBranch        *b_EventInfo_trigPassed_HLT_e26_lhtight_nod0_ivarloose;   //!
   TBranch        *b_EventInfo_trigPassed_HLT_e60_lhmedium_nod0;   //!
   TBranch        *b_EventInfo_trigPassed_HLT_e140_lhloose_nod0;   //!
   TBranch        *b_EventInfo_trigPassed_HLT_mu20_iloose_L1MU15;   //!
   TBranch        *b_EventInfo_trigPassed_HLT_mu26_ivarmedium;   //!
   TBranch        *b_EventInfo_trigPassed_HLT_mu50;   //!
   TBranch        *b_EventInfo_jvt_effSF_NOSYS;   //!
   TBranch        *b_CharmEventInfo_TopWeight;   //!
   TBranch        *b_AnalysisElectrons_pt_NOSYS;   //!
   TBranch        *b_AnalysisElectrons_el_selected_NOSYS;   //!
   TBranch        *b_AnalysisElectrons_eta;   //!
   TBranch        *b_AnalysisElectrons_caloCluster_eta;   //!
   TBranch        *b_AnalysisElectrons_phi;   //!
   TBranch        *b_AnalysisElectrons_charge;   //!
   TBranch        *b_AnalysisElectrons_d0sig;   //!
   TBranch        *b_AnalysisElectrons_d0;   //!
   TBranch        *b_AnalysisElectrons_z0sinTheta;   //!
   TBranch        *b_AnalysisElectrons_topoetcone20;   //!
   TBranch        *b_AnalysisElectrons_ptvarcone20_TightTTVA_pt1000;   //!
   TBranch        *b_AnalysisElectrons_ptvarcone30_TightTTVA_pt1000;   //!
   TBranch        *b_AnalysisElectrons_likelihood_Tight;   //!
   TBranch        *b_AnalysisElectrons_isIsolated_Tight_VarRad_NOSYS;   //!
   TBranch        *b_AnalysisElectrons_effSF_NOSYS;   //!
   TBranch        *b_AnalysisElectrons_effSF_ID_Tight_NOSYS;   //!
   TBranch        *b_AnalysisElectrons_effSF_Chflip_Tight_Tight_VarRad_NOSYS;   //!
   TBranch        *b_AnalysisElectrons_effSF_Isol_Tight_Tight_VarRad_NOSYS;   //!
   TBranch        *b_AnalysisElectrons_effSF_Trig_Tight_Tight_VarRad_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_NOSYS;   //!
   TBranch        *b_AnalysisElectrons_effSF_Trig_Tight_Tight_VarRad_Eff_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_NOSYS;   //!
   TBranch        *b_AnalysisElectrons_effSF_Chflip_Tight_noIso_NOSYS;   //!
   TBranch        *b_AnalysisElectrons_effSF_Trig_Tight_noIso_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_NOSYS;   //!
   TBranch        *b_AnalysisElectrons_effSF_Trig_Tight_noIso_Eff_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_NOSYS;   //!
   TBranch        *b_AnalysisElectrons_matched_HLT_e24_lhmedium_L1EM20VH;   //!
   TBranch        *b_AnalysisElectrons_matched_HLT_e60_lhmedium;   //!
   TBranch        *b_AnalysisElectrons_matched_HLT_e120_lhloose;   //!
   TBranch        *b_AnalysisElectrons_matched_HLT_e26_lhtight_nod0_ivarloose;   //!
   TBranch        *b_AnalysisElectrons_matched_HLT_e60_lhmedium_nod0;   //!
   TBranch        *b_AnalysisElectrons_matched_HLT_e140_lhloose_nod0;   //!
   TBranch        *b_AnalysisElectrons_truthType;   //!
   TBranch        *b_AnalysisElectrons_truthOrigin;   //!
   TBranch        *b_AnalysisElectrons_firstEgMotherPdgId;   //!
   TBranch        *b_AnalysisElectrons_firstEgMotherTruthType;   //!
   TBranch        *b_AnalysisElectrons_firstEgMotherTruthOrigin;   //!
   TBranch        *b_AnalysisMuons_pt_NOSYS;   //!
   TBranch        *b_AnalysisMuons_mu_selected_NOSYS;   //!
   TBranch        *b_AnalysisMuons_is_bad;   //!
   TBranch        *b_AnalysisMuons_eta;   //!
   TBranch        *b_AnalysisMuons_phi;   //!
   TBranch        *b_AnalysisMuons_charge;   //!
   TBranch        *b_AnalysisMuons_d0sig;   //!
   TBranch        *b_AnalysisMuons_d0;   //!
   TBranch        *b_AnalysisMuons_z0sinTheta;   //!
   TBranch        *b_AnalysisMuons_topoetcone20;   //!
   TBranch        *b_AnalysisMuons_ptvarcone30_TightTTVA_pt1000;   //!
   TBranch        *b_AnalysisMuons_ptvarcone30_TightTTVA_pt500;   //!
   TBranch        *b_AnalysisMuons_neflowisol20;   //!
   TBranch        *b_AnalysisMuons_isQuality_Loose_NOSYS;   //!
   TBranch        *b_AnalysisMuons_isQuality_Medium_NOSYS;   //!
   TBranch        *b_AnalysisMuons_isQuality_Tight_NOSYS;   //!
   TBranch        *b_AnalysisMuons_isIsolated_PflowTight_VarRad_NOSYS;   //!
   TBranch        *b_AnalysisMuons_muon_effSF_NOSYS;   //!
   TBranch        *b_AnalysisMuons_muon_effSF_TTVA_NOSYS;   //!
   TBranch        *b_AnalysisMuons_muon_effSF_Quality_Loose_NOSYS;   //!
   TBranch        *b_AnalysisMuons_muon_effSF_Quality_Medium_NOSYS;   //!
   TBranch        *b_AnalysisMuons_muon_effSF_Quality_Tight_NOSYS;   //!
   TBranch        *b_AnalysisMuons_muon_effSF_Isol_PflowTight_VarRad_NOSYS;   //!
   TBranch        *b_AnalysisMuons_muon_effMC_Trig_Loose_HLT_mu26_ivarmedium_OR_HLT_mu50_NOSYS;   //!
   TBranch        *b_AnalysisMuons_muon_effData_Trig_Loose_HLT_mu26_ivarmedium_OR_HLT_mu50_NOSYS;   //!
   TBranch        *b_AnalysisMuons_muon_effMC_Trig_Loose_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_NOSYS;   //!
   TBranch        *b_AnalysisMuons_muon_effData_Trig_Loose_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_NOSYS;   //!
   TBranch        *b_AnalysisMuons_muon_effMC_Trig_Medium_HLT_mu26_ivarmedium_OR_HLT_mu50_NOSYS;   //!
   TBranch        *b_AnalysisMuons_muon_effData_Trig_Medium_HLT_mu26_ivarmedium_OR_HLT_mu50_NOSYS;   //!
   TBranch        *b_AnalysisMuons_muon_effMC_Trig_Medium_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_NOSYS;   //!
   TBranch        *b_AnalysisMuons_muon_effData_Trig_Medium_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_NOSYS;   //!
   TBranch        *b_AnalysisMuons_muon_effMC_Trig_Tight_HLT_mu26_ivarmedium_OR_HLT_mu50_NOSYS;   //!
   TBranch        *b_AnalysisMuons_muon_effData_Trig_Tight_HLT_mu26_ivarmedium_OR_HLT_mu50_NOSYS;   //!
   TBranch        *b_AnalysisMuons_muon_effMC_Trig_Tight_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_NOSYS;   //!
   TBranch        *b_AnalysisMuons_muon_effData_Trig_Tight_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_NOSYS;   //!
   TBranch        *b_AnalysisMuons_matched_HLT_mu20_iloose_L1MU15;   //!
   TBranch        *b_AnalysisMuons_matched_HLT_mu26_ivarmedium;   //!
   TBranch        *b_AnalysisMuons_matched_HLT_mu50;   //!
   TBranch        *b_AnalysisMuons_truthType;   //!
   TBranch        *b_AnalysisMuons_truthOrigin;   //!
   TBranch        *b_AnalysisJets_pt_NOSYS;   //!
   TBranch        *b_AnalysisJets_jet_selected_NOSYS;   //!
   TBranch        *b_AnalysisJets_m;   //!
   TBranch        *b_AnalysisJets_eta;   //!
   TBranch        *b_AnalysisJets_phi;   //!
   TBranch        *b_AnalysisJets_ftag_select_DL1r_FixedCutBEff_70;   //!
   TBranch        *b_AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_NOSYS;   //!
   TBranch        *b_AnalysisJets_ftag_quantile_DL1r;   //!
   TBranch        *b_AnalysisJets_ftag_DL1pu_DL1r;   //!
   TBranch        *b_AnalysisJets_ftag_DL1pb_DL1r;   //!
   TBranch        *b_AnalysisJets_ftag_DL1pc_DL1r;   //!
   TBranch        *b_AnalysisJets_HadronConeExclExtendedTruthLabelID;   //!
   TBranch        *b_AnalysisJets_HadronConeExclTruthLabelID;   //!
   TBranch        *b_METInfo_MET_NOSYS;   //!
   TBranch        *b_METInfo_METPhi_NOSYS;   //!
   TBranch        *b_DMesons_costhetastar_NOSYS;   //!
   TBranch        *b_DMesons_D0Index_NOSYS;   //!
   TBranch        *b_DMesons_daughterInfo__eta_NOSYS;   //!
   TBranch        *b_DMesons_daughterInfo__passTight_NOSYS;   //!
   TBranch        *b_DMesons_daughterInfo__pdgId_NOSYS;   //!
   TBranch        *b_DMesons_daughterInfo__phi_NOSYS;   //!
   TBranch        *b_DMesons_daughterInfo__pt_NOSYS;   //!
   TBranch        *b_DMesons_daughterInfo__trackId_NOSYS;   //!
   TBranch        *b_DMesons_daughterInfo__truthBarcode_NOSYS;   //!
   TBranch        *b_DMesons_daughterInfo__truthDBarcode_NOSYS;   //!
   TBranch        *b_DMesons_daughterInfo__z0SinTheta_NOSYS;   //!
   TBranch        *b_DMesons_daughterInfo__z0SinThetaPV_NOSYS;   //!
   TBranch        *b_DMesons_decayType_NOSYS;   //!
   TBranch        *b_DMesons_DeltaMass_NOSYS;   //!
   TBranch        *b_DMesons_eta_NOSYS;   //!
   TBranch        *b_DMesons_fitOutput__Charge_NOSYS;   //!
   TBranch        *b_DMesons_fitOutput__Chi2_NOSYS;   //!
   TBranch        *b_DMesons_fitOutput__Impact_NOSYS;   //!
   TBranch        *b_DMesons_fitOutput__ImpactSignificance_NOSYS;   //!
   TBranch        *b_DMesons_fitOutput__ImpactZ0SinTheta_NOSYS;   //!
   TBranch        *b_DMesons_fitOutput__Lxy_NOSYS;   //!
   TBranch        *b_DMesons_fitOutput__VertexPosition_NOSYS;   //!
   TBranch        *b_DMesons_m_NOSYS;   //!
   TBranch        *b_DMesons_mKpi1_NOSYS;   //!
   TBranch        *b_DMesons_mKpi2_NOSYS;   //!
   TBranch        *b_DMesons_mPhi1_NOSYS;   //!
   TBranch        *b_DMesons_mPhi2_NOSYS;   //!
   TBranch        *b_DMesons_pdgId_NOSYS;   //!
   TBranch        *b_DMesons_phi_NOSYS;   //!
   TBranch        *b_DMesons_pt_NOSYS;   //!
   TBranch        *b_DMesons_ptcone40_NOSYS;   //!
   TBranch        *b_DMesons_SlowPionD0_NOSYS;   //!
   TBranch        *b_DMesons_SlowPionZ0SinTheta_NOSYS;   //!
   TBranch        *b_DMesons_truthBarcode_NOSYS;   //!
   TBranch        *b_TruthParticles_Selected_barcode;   //!
   TBranch        *b_TruthParticles_Selected_cosThetaStarT;   //!
   TBranch        *b_TruthParticles_Selected_daughterInfoT__barcode;   //!
   TBranch        *b_TruthParticles_Selected_daughterInfoT__eta;   //!
   TBranch        *b_TruthParticles_Selected_daughterInfoT__pdgId;   //!
   TBranch        *b_TruthParticles_Selected_daughterInfoT__phi;   //!
   TBranch        *b_TruthParticles_Selected_daughterInfoT__pt;   //!
   TBranch        *b_TruthParticles_Selected_decayMode;   //!
   TBranch        *b_TruthParticles_Selected_eta;   //!
   TBranch        *b_TruthParticles_Selected_fromBdecay;   //!
   TBranch        *b_TruthParticles_Selected_ImpactT;   //!
   TBranch        *b_TruthParticles_Selected_LxyT;   //!
   TBranch        *b_TruthParticles_Selected_m;   //!
   TBranch        *b_TruthParticles_Selected_pdgId;   //!
   TBranch        *b_TruthParticles_Selected_phi;   //!
   TBranch        *b_TruthParticles_Selected_pt;   //!
   TBranch        *b_TruthParticles_Selected_status;   //!
   TBranch        *b_TruthParticles_Selected_vertexPosition;   //!
   TBranch        *b_TruthLeptons_barcode;   //!
   TBranch        *b_TruthLeptons_classifierParticleOrigin;   //!
   TBranch        *b_TruthLeptons_classifierParticleType;   //!
   TBranch        *b_TruthLeptons_e;   //!
   TBranch        *b_TruthLeptons_eta;   //!
   TBranch        *b_TruthLeptons_m;   //!
   TBranch        *b_TruthLeptons_pdgId;   //!
   TBranch        *b_TruthLeptons_phi;   //!
   TBranch        *b_TruthLeptons_pt;   //!
   TBranch        *b_TruthLeptons_status;   //!
   TBranch        *b_TruthLeptons_e_dressed;   //!
   TBranch        *b_TruthLeptons_eta_dressed;   //!
   TBranch        *b_TruthLeptons_phi_dressed;   //!
   TBranch        *b_TruthLeptons_pt_dressed;   //!
   TBranch        *b_AntiKt4TruthJets_ConeTruthLabelID;   //!
   TBranch        *b_AntiKt4TruthJets_eta;   //!
   TBranch        *b_AntiKt4TruthJets_HadronConeExclTruthLabelID;   //!
   TBranch        *b_AntiKt4TruthJets_m;   //!
   TBranch        *b_AntiKt4TruthJets_phi;   //!
   TBranch        *b_AntiKt4TruthJets_pt;   //!
   TBranch        *b_AntiKt4TruthChargedJets_ConeTruthLabelID;   //!
   TBranch        *b_AntiKt4TruthChargedJets_eta;   //!
   TBranch        *b_AntiKt4TruthChargedJets_HadronConeExclTruthLabelID;   //!
   TBranch        *b_AntiKt4TruthChargedJets_m;   //!
   TBranch        *b_AntiKt4TruthChargedJets_phi;   //!
   TBranch        *b_AntiKt4TruthChargedJets_pt;   //!
   TBranch        *b_AntiKt6TruthChargedJets_ConeTruthLabelID;   //!
   TBranch        *b_AntiKt6TruthChargedJets_eta;   //!
   TBranch        *b_AntiKt6TruthChargedJets_HadronConeExclTruthLabelID;   //!
   TBranch        *b_AntiKt6TruthChargedJets_m;   //!
   TBranch        *b_AntiKt6TruthChargedJets_phi;   //!
   TBranch        *b_AntiKt6TruthChargedJets_pt;   //!
   TBranch        *b_AntiKt8TruthChargedJets_ConeTruthLabelID;   //!
   TBranch        *b_AntiKt8TruthChargedJets_eta;   //!
   TBranch        *b_AntiKt8TruthChargedJets_HadronConeExclTruthLabelID;   //!
   TBranch        *b_AntiKt8TruthChargedJets_m;   //!
   TBranch        *b_AntiKt8TruthChargedJets_phi;   //!
   TBranch        *b_AntiKt8TruthChargedJets_pt;   //!
   TBranch        *b_AntiKt10TruthChargedJets_ConeTruthLabelID;   //!
   TBranch        *b_AntiKt10TruthChargedJets_eta;   //!
   TBranch        *b_AntiKt10TruthChargedJets_HadronConeExclTruthLabelID;   //!
   TBranch        *b_AntiKt10TruthChargedJets_m;   //!
   TBranch        *b_AntiKt10TruthChargedJets_phi;   //!
   TBranch        *b_AntiKt10TruthChargedJets_pt;   //!
   TBranch        *b_MesonTracks_d0sig_NOSYS;   //!
   TBranch        *b_MesonTracks_d0sigPV_NOSYS;   //!
   TBranch        *b_MesonTracks_eta_NOSYS;   //!
   TBranch        *b_MesonTracks_passTight_NOSYS;   //!
   TBranch        *b_MesonTracks_phi_NOSYS;   //!
   TBranch        *b_MesonTracks_pt_NOSYS;   //!
   TBranch        *b_MesonTracks_trackId_NOSYS;   //!
   TBranch        *b_MesonTracks_z0sinTheta_NOSYS;   //!
   TBranch        *b_MesonTracks_truthMatchProbability_NOSYS;   //!
   TBranch        *b_AntiKt6PV0TrackJets_pt_NOSYS;   //!
   TBranch        *b_AntiKt6PV0TrackJets_eta_NOSYS;   //!
   TBranch        *b_AntiKt6PV0TrackJets_phi_NOSYS;   //!
   TBranch        *b_AntiKt6PV0TrackJets_m_NOSYS;   //!
   TBranch        *b_AntiKt6PV0TrackJets_daughter__pt_NOSYS;   //!
   TBranch        *b_AntiKt6PV0TrackJets_daughter__eta_NOSYS;   //!
   TBranch        *b_AntiKt6PV0TrackJets_daughter__phi_NOSYS;   //!
   TBranch        *b_AntiKt6PV0TrackJets_daughter__trackId_NOSYS;   //!
   TBranch        *b_AntiKt8PV0TrackJets_pt_NOSYS;   //!
   TBranch        *b_AntiKt8PV0TrackJets_eta_NOSYS;   //!
   TBranch        *b_AntiKt8PV0TrackJets_phi_NOSYS;   //!
   TBranch        *b_AntiKt8PV0TrackJets_m_NOSYS;   //!
   TBranch        *b_AntiKt8PV0TrackJets_daughter__pt_NOSYS;   //!
   TBranch        *b_AntiKt8PV0TrackJets_daughter__eta_NOSYS;   //!
   TBranch        *b_AntiKt8PV0TrackJets_daughter__phi_NOSYS;   //!
   TBranch        *b_AntiKt8PV0TrackJets_daughter__trackId_NOSYS;   //!
   TBranch        *b_AntiKt10PV0TrackJets_pt_NOSYS;   //!
   TBranch        *b_AntiKt10PV0TrackJets_eta_NOSYS;   //!
   TBranch        *b_AntiKt10PV0TrackJets_phi_NOSYS;   //!
   TBranch        *b_AntiKt10PV0TrackJets_m_NOSYS;   //!
   TBranch        *b_AntiKt10PV0TrackJets_daughter__pt_NOSYS;   //!
   TBranch        *b_AntiKt10PV0TrackJets_daughter__eta_NOSYS;   //!
   TBranch        *b_AntiKt10PV0TrackJets_daughter__phi_NOSYS;   //!
   TBranch        *b_AntiKt10PV0TrackJets_daughter__trackId_NOSYS;   //!
   TBranch        *b_AntiKtVR30Rmax4Rmin02TrackJets_pt_NOSYS;   //!
   TBranch        *b_AntiKtVR30Rmax4Rmin02TrackJets_eta_NOSYS;   //!
   TBranch        *b_AntiKtVR30Rmax4Rmin02TrackJets_phi_NOSYS;   //!
   TBranch        *b_AntiKtVR30Rmax4Rmin02TrackJets_m_NOSYS;   //!
   TBranch        *b_AntiKtVR30Rmax4Rmin02TrackJets_daughter__pt_NOSYS;   //!
   TBranch        *b_AntiKtVR30Rmax4Rmin02TrackJets_daughter__eta_NOSYS;   //!
   TBranch        *b_AntiKtVR30Rmax4Rmin02TrackJets_daughter__phi_NOSYS;   //!
   TBranch        *b_AntiKtVR30Rmax4Rmin02TrackJets_daughter__trackId_NOSYS;   //!
   TBranch        *b_AnalysisElectrons_pt_EG_RESOLUTION_ALL__1down;   //!
   TBranch        *b_AnalysisElectrons_el_selected_EG_RESOLUTION_ALL__1down;   //!
   TBranch        *b_AnalysisElectrons_isIsolated_Tight_VarRad_EG_RESOLUTION_ALL__1down;   //!
   TBranch        *b_AnalysisElectrons_effSF_EG_RESOLUTION_ALL__1down;   //!
   TBranch        *b_AnalysisElectrons_effSF_ID_Tight_EG_RESOLUTION_ALL__1down;   //!
   TBranch        *b_AnalysisElectrons_effSF_Chflip_Tight_Tight_VarRad_EG_RESOLUTION_ALL__1down;   //!
   TBranch        *b_AnalysisElectrons_effSF_Isol_Tight_Tight_VarRad_EG_RESOLUTION_ALL__1down;   //!
   TBranch        *b_AnalysisElectrons_effSF_Trig_Tight_Tight_VarRad_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_EG_RESOLUTION_ALL__1down;   //!
   TBranch        *b_AnalysisElectrons_effSF_Trig_Tight_Tight_VarRad_Eff_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_EG_RESOLUTION_ALL__1down;   //!
   TBranch        *b_AnalysisElectrons_effSF_Chflip_Tight_noIso_EG_RESOLUTION_ALL__1down;   //!
   TBranch        *b_AnalysisElectrons_effSF_Trig_Tight_noIso_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_EG_RESOLUTION_ALL__1down;   //!
   TBranch        *b_AnalysisElectrons_effSF_Trig_Tight_noIso_Eff_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_EG_RESOLUTION_ALL__1down;   //!
   TBranch        *b_AnalysisMuons_mu_selected_EG_RESOLUTION_ALL__1down;   //!
   TBranch        *b_AnalysisJets_jet_selected_EG_RESOLUTION_ALL__1down;   //!
   TBranch        *b_METInfo_MET_EG_RESOLUTION_ALL__1down;   //!
   TBranch        *b_METInfo_METPhi_EG_RESOLUTION_ALL__1down;   //!
   TBranch        *b_AnalysisElectrons_pt_EG_RESOLUTION_ALL__1up;   //!
   TBranch        *b_AnalysisElectrons_el_selected_EG_RESOLUTION_ALL__1up;   //!
   TBranch        *b_AnalysisElectrons_isIsolated_Tight_VarRad_EG_RESOLUTION_ALL__1up;   //!
   TBranch        *b_AnalysisElectrons_effSF_EG_RESOLUTION_ALL__1up;   //!
   TBranch        *b_AnalysisElectrons_effSF_ID_Tight_EG_RESOLUTION_ALL__1up;   //!
   TBranch        *b_AnalysisElectrons_effSF_Chflip_Tight_Tight_VarRad_EG_RESOLUTION_ALL__1up;   //!
   TBranch        *b_AnalysisElectrons_effSF_Isol_Tight_Tight_VarRad_EG_RESOLUTION_ALL__1up;   //!
   TBranch        *b_AnalysisElectrons_effSF_Trig_Tight_Tight_VarRad_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_EG_RESOLUTION_ALL__1up;   //!
   TBranch        *b_AnalysisElectrons_effSF_Trig_Tight_Tight_VarRad_Eff_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_EG_RESOLUTION_ALL__1up;   //!
   TBranch        *b_AnalysisElectrons_effSF_Chflip_Tight_noIso_EG_RESOLUTION_ALL__1up;   //!
   TBranch        *b_AnalysisElectrons_effSF_Trig_Tight_noIso_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_EG_RESOLUTION_ALL__1up;   //!
   TBranch        *b_AnalysisElectrons_effSF_Trig_Tight_noIso_Eff_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_EG_RESOLUTION_ALL__1up;   //!
   TBranch        *b_AnalysisMuons_mu_selected_EG_RESOLUTION_ALL__1up;   //!
   TBranch        *b_AnalysisJets_jet_selected_EG_RESOLUTION_ALL__1up;   //!
   TBranch        *b_METInfo_MET_EG_RESOLUTION_ALL__1up;   //!
   TBranch        *b_METInfo_METPhi_EG_RESOLUTION_ALL__1up;   //!
   TBranch        *b_AnalysisElectrons_pt_EG_SCALE_AF2__1down;   //!
   TBranch        *b_AnalysisElectrons_el_selected_EG_SCALE_AF2__1down;   //!
   TBranch        *b_AnalysisElectrons_isIsolated_Tight_VarRad_EG_SCALE_AF2__1down;   //!
   TBranch        *b_AnalysisElectrons_effSF_EG_SCALE_AF2__1down;   //!
   TBranch        *b_AnalysisElectrons_effSF_ID_Tight_EG_SCALE_AF2__1down;   //!
   TBranch        *b_AnalysisElectrons_effSF_Chflip_Tight_Tight_VarRad_EG_SCALE_AF2__1down;   //!
   TBranch        *b_AnalysisElectrons_effSF_Isol_Tight_Tight_VarRad_EG_SCALE_AF2__1down;   //!
   TBranch        *b_AnalysisElectrons_effSF_Trig_Tight_Tight_VarRad_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_EG_SCALE_AF2__1down;   //!
   TBranch        *b_AnalysisElectrons_effSF_Trig_Tight_Tight_VarRad_Eff_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_EG_SCALE_AF2__1down;   //!
   TBranch        *b_AnalysisElectrons_effSF_Chflip_Tight_noIso_EG_SCALE_AF2__1down;   //!
   TBranch        *b_AnalysisElectrons_effSF_Trig_Tight_noIso_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_EG_SCALE_AF2__1down;   //!
   TBranch        *b_AnalysisElectrons_effSF_Trig_Tight_noIso_Eff_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_EG_SCALE_AF2__1down;   //!
   TBranch        *b_AnalysisMuons_mu_selected_EG_SCALE_AF2__1down;   //!
   TBranch        *b_AnalysisJets_jet_selected_EG_SCALE_AF2__1down;   //!
   TBranch        *b_METInfo_MET_EG_SCALE_AF2__1down;   //!
   TBranch        *b_METInfo_METPhi_EG_SCALE_AF2__1down;   //!
   TBranch        *b_AnalysisElectrons_pt_EG_SCALE_AF2__1up;   //!
   TBranch        *b_AnalysisElectrons_el_selected_EG_SCALE_AF2__1up;   //!
   TBranch        *b_AnalysisElectrons_isIsolated_Tight_VarRad_EG_SCALE_AF2__1up;   //!
   TBranch        *b_AnalysisElectrons_effSF_EG_SCALE_AF2__1up;   //!
   TBranch        *b_AnalysisElectrons_effSF_ID_Tight_EG_SCALE_AF2__1up;   //!
   TBranch        *b_AnalysisElectrons_effSF_Chflip_Tight_Tight_VarRad_EG_SCALE_AF2__1up;   //!
   TBranch        *b_AnalysisElectrons_effSF_Isol_Tight_Tight_VarRad_EG_SCALE_AF2__1up;   //!
   TBranch        *b_AnalysisElectrons_effSF_Trig_Tight_Tight_VarRad_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_EG_SCALE_AF2__1up;   //!
   TBranch        *b_AnalysisElectrons_effSF_Trig_Tight_Tight_VarRad_Eff_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_EG_SCALE_AF2__1up;   //!
   TBranch        *b_AnalysisElectrons_effSF_Chflip_Tight_noIso_EG_SCALE_AF2__1up;   //!
   TBranch        *b_AnalysisElectrons_effSF_Trig_Tight_noIso_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_EG_SCALE_AF2__1up;   //!
   TBranch        *b_AnalysisElectrons_effSF_Trig_Tight_noIso_Eff_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_EG_SCALE_AF2__1up;   //!
   TBranch        *b_AnalysisMuons_mu_selected_EG_SCALE_AF2__1up;   //!
   TBranch        *b_AnalysisJets_jet_selected_EG_SCALE_AF2__1up;   //!
   TBranch        *b_METInfo_MET_EG_SCALE_AF2__1up;   //!
   TBranch        *b_METInfo_METPhi_EG_SCALE_AF2__1up;   //!
   TBranch        *b_AnalysisElectrons_pt_EG_SCALE_ALL__1down;   //!
   TBranch        *b_AnalysisElectrons_el_selected_EG_SCALE_ALL__1down;   //!
   TBranch        *b_AnalysisElectrons_isIsolated_Tight_VarRad_EG_SCALE_ALL__1down;   //!
   TBranch        *b_AnalysisElectrons_effSF_EG_SCALE_ALL__1down;   //!
   TBranch        *b_AnalysisElectrons_effSF_ID_Tight_EG_SCALE_ALL__1down;   //!
   TBranch        *b_AnalysisElectrons_effSF_Chflip_Tight_Tight_VarRad_EG_SCALE_ALL__1down;   //!
   TBranch        *b_AnalysisElectrons_effSF_Isol_Tight_Tight_VarRad_EG_SCALE_ALL__1down;   //!
   TBranch        *b_AnalysisElectrons_effSF_Trig_Tight_Tight_VarRad_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_EG_SCALE_ALL__1down;   //!
   TBranch        *b_AnalysisElectrons_effSF_Trig_Tight_Tight_VarRad_Eff_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_EG_SCALE_ALL__1down;   //!
   TBranch        *b_AnalysisElectrons_effSF_Chflip_Tight_noIso_EG_SCALE_ALL__1down;   //!
   TBranch        *b_AnalysisElectrons_effSF_Trig_Tight_noIso_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_EG_SCALE_ALL__1down;   //!
   TBranch        *b_AnalysisElectrons_effSF_Trig_Tight_noIso_Eff_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_EG_SCALE_ALL__1down;   //!
   TBranch        *b_AnalysisMuons_mu_selected_EG_SCALE_ALL__1down;   //!
   TBranch        *b_AnalysisJets_jet_selected_EG_SCALE_ALL__1down;   //!
   TBranch        *b_METInfo_MET_EG_SCALE_ALL__1down;   //!
   TBranch        *b_METInfo_METPhi_EG_SCALE_ALL__1down;   //!
   TBranch        *b_AnalysisElectrons_pt_EG_SCALE_ALL__1up;   //!
   TBranch        *b_AnalysisElectrons_el_selected_EG_SCALE_ALL__1up;   //!
   TBranch        *b_AnalysisElectrons_isIsolated_Tight_VarRad_EG_SCALE_ALL__1up;   //!
   TBranch        *b_AnalysisElectrons_effSF_EG_SCALE_ALL__1up;   //!
   TBranch        *b_AnalysisElectrons_effSF_ID_Tight_EG_SCALE_ALL__1up;   //!
   TBranch        *b_AnalysisElectrons_effSF_Chflip_Tight_Tight_VarRad_EG_SCALE_ALL__1up;   //!
   TBranch        *b_AnalysisElectrons_effSF_Isol_Tight_Tight_VarRad_EG_SCALE_ALL__1up;   //!
   TBranch        *b_AnalysisElectrons_effSF_Trig_Tight_Tight_VarRad_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_EG_SCALE_ALL__1up;   //!
   TBranch        *b_AnalysisElectrons_effSF_Trig_Tight_Tight_VarRad_Eff_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_EG_SCALE_ALL__1up;   //!
   TBranch        *b_AnalysisElectrons_effSF_Chflip_Tight_noIso_EG_SCALE_ALL__1up;   //!
   TBranch        *b_AnalysisElectrons_effSF_Trig_Tight_noIso_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_EG_SCALE_ALL__1up;   //!
   TBranch        *b_AnalysisElectrons_effSF_Trig_Tight_noIso_Eff_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_EG_SCALE_ALL__1up;   //!
   TBranch        *b_AnalysisMuons_mu_selected_EG_SCALE_ALL__1up;   //!
   TBranch        *b_AnalysisJets_jet_selected_EG_SCALE_ALL__1up;   //!
   TBranch        *b_METInfo_MET_EG_SCALE_ALL__1up;   //!
   TBranch        *b_METInfo_METPhi_EG_SCALE_ALL__1up;   //!
   TBranch        *b_AnalysisElectrons_effSF_Chflip_Tight_Tight_VarRad_EL_CHARGEID_STAT__1down;   //!
   TBranch        *b_AnalysisElectrons_effSF_Chflip_Tight_noIso_EL_CHARGEID_STAT__1down;   //!
   TBranch        *b_AnalysisElectrons_effSF_Chflip_Tight_Tight_VarRad_EL_CHARGEID_STAT__1up;   //!
   TBranch        *b_AnalysisElectrons_effSF_Chflip_Tight_noIso_EL_CHARGEID_STAT__1up;   //!
   TBranch        *b_AnalysisElectrons_effSF_Chflip_Tight_Tight_VarRad_EL_CHARGEID_SYStotal__1down;   //!
   TBranch        *b_AnalysisElectrons_effSF_Chflip_Tight_noIso_EL_CHARGEID_SYStotal__1down;   //!
   TBranch        *b_AnalysisElectrons_effSF_Chflip_Tight_Tight_VarRad_EL_CHARGEID_SYStotal__1up;   //!
   TBranch        *b_AnalysisElectrons_effSF_Chflip_Tight_noIso_EL_CHARGEID_SYStotal__1up;   //!
   TBranch        *b_AnalysisElectrons_effSF_ID_Tight_EL_EFF_ID_TOTAL_1NPCOR_PLUS_UNCOR__1down;   //!
   TBranch        *b_AnalysisElectrons_effSF_ID_Tight_EL_EFF_ID_TOTAL_1NPCOR_PLUS_UNCOR__1up;   //!
   TBranch        *b_AnalysisElectrons_effSF_Isol_Tight_Tight_VarRad_EL_EFF_Iso_TOTAL_1NPCOR_PLUS_UNCOR__1down;   //!
   TBranch        *b_AnalysisElectrons_effSF_Isol_Tight_Tight_VarRad_EL_EFF_Iso_TOTAL_1NPCOR_PLUS_UNCOR__1up;   //!
   TBranch        *b_AnalysisElectrons_effSF_EL_EFF_Reco_TOTAL_1NPCOR_PLUS_UNCOR__1down;   //!
   TBranch        *b_AnalysisElectrons_effSF_EL_EFF_Reco_TOTAL_1NPCOR_PLUS_UNCOR__1up;   //!
   TBranch        *b_AnalysisElectrons_effSF_Trig_Tight_Tight_VarRad_Eff_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_EL_EFF_TriggerEff_TOTAL_1NPCOR_PLUS_UNCOR__1down;   //!
   TBranch        *b_AnalysisElectrons_effSF_Trig_Tight_noIso_Eff_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_EL_EFF_TriggerEff_TOTAL_1NPCOR_PLUS_UNCOR__1down;   //!
   TBranch        *b_AnalysisElectrons_effSF_Trig_Tight_Tight_VarRad_Eff_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_EL_EFF_TriggerEff_TOTAL_1NPCOR_PLUS_UNCOR__1up;   //!
   TBranch        *b_AnalysisElectrons_effSF_Trig_Tight_noIso_Eff_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_EL_EFF_TriggerEff_TOTAL_1NPCOR_PLUS_UNCOR__1up;   //!
   TBranch        *b_AnalysisElectrons_effSF_Trig_Tight_Tight_VarRad_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_EL_EFF_Trigger_TOTAL_1NPCOR_PLUS_UNCOR__1down;   //!
   TBranch        *b_AnalysisElectrons_effSF_Trig_Tight_noIso_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_EL_EFF_Trigger_TOTAL_1NPCOR_PLUS_UNCOR__1down;   //!
   TBranch        *b_AnalysisElectrons_effSF_Trig_Tight_Tight_VarRad_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_EL_EFF_Trigger_TOTAL_1NPCOR_PLUS_UNCOR__1up;   //!
   TBranch        *b_AnalysisElectrons_effSF_Trig_Tight_noIso_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_EL_EFF_Trigger_TOTAL_1NPCOR_PLUS_UNCOR__1up;   //!
   TBranch        *b_AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_FT_EFF_Eigen_B_0__1down;   //!
   TBranch        *b_AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_FT_EFF_Eigen_B_0__1up;   //!
   TBranch        *b_AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_FT_EFF_Eigen_B_1__1down;   //!
   TBranch        *b_AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_FT_EFF_Eigen_B_1__1up;   //!
   TBranch        *b_AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_FT_EFF_Eigen_B_2__1down;   //!
   TBranch        *b_AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_FT_EFF_Eigen_B_2__1up;   //!
   TBranch        *b_AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_FT_EFF_Eigen_B_3__1down;   //!
   TBranch        *b_AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_FT_EFF_Eigen_B_3__1up;   //!
   TBranch        *b_AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_FT_EFF_Eigen_B_4__1down;   //!
   TBranch        *b_AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_FT_EFF_Eigen_B_4__1up;   //!
   TBranch        *b_AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_FT_EFF_Eigen_B_5__1down;   //!
   TBranch        *b_AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_FT_EFF_Eigen_B_5__1up;   //!
   TBranch        *b_AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_FT_EFF_Eigen_B_6__1down;   //!
   TBranch        *b_AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_FT_EFF_Eigen_B_6__1up;   //!
   TBranch        *b_AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_FT_EFF_Eigen_B_7__1down;   //!
   TBranch        *b_AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_FT_EFF_Eigen_B_7__1up;   //!
   TBranch        *b_AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_FT_EFF_Eigen_B_8__1down;   //!
   TBranch        *b_AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_FT_EFF_Eigen_B_8__1up;   //!
   TBranch        *b_AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_FT_EFF_Eigen_C_0__1down;   //!
   TBranch        *b_AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_FT_EFF_Eigen_C_0__1up;   //!
   TBranch        *b_AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_FT_EFF_Eigen_C_1__1down;   //!
   TBranch        *b_AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_FT_EFF_Eigen_C_1__1up;   //!
   TBranch        *b_AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_FT_EFF_Eigen_C_2__1down;   //!
   TBranch        *b_AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_FT_EFF_Eigen_C_2__1up;   //!
   TBranch        *b_AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_FT_EFF_Eigen_C_3__1down;   //!
   TBranch        *b_AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_FT_EFF_Eigen_C_3__1up;   //!
   TBranch        *b_AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_FT_EFF_Eigen_Light_0__1down;   //!
   TBranch        *b_AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_FT_EFF_Eigen_Light_0__1up;   //!
   TBranch        *b_AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_FT_EFF_Eigen_Light_1__1down;   //!
   TBranch        *b_AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_FT_EFF_Eigen_Light_1__1up;   //!
   TBranch        *b_AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_FT_EFF_Eigen_Light_2__1down;   //!
   TBranch        *b_AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_FT_EFF_Eigen_Light_2__1up;   //!
   TBranch        *b_AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_FT_EFF_Eigen_Light_3__1down;   //!
   TBranch        *b_AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_FT_EFF_Eigen_Light_3__1up;   //!
   TBranch        *b_AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_FT_EFF_Eigen_Light_4__1down;   //!
   TBranch        *b_AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_FT_EFF_Eigen_Light_4__1up;   //!
   TBranch        *b_AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_FT_EFF_extrapolation__1down;   //!
   TBranch        *b_AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_FT_EFF_extrapolation__1up;   //!
   TBranch        *b_AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_FT_EFF_extrapolation_from_charm__1down;   //!
   TBranch        *b_AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_FT_EFF_extrapolation_from_charm__1up;   //!
   TBranch        *b_EventInfo_generatorWeight_GEN_MEWeight;   //!
   TBranch        *b_EventInfo_generatorWeight_GEN_ME_ONLY_MUR05_MUF05_PDF303200_PSMUR05_PSMUF05;   //!
   TBranch        *b_EventInfo_generatorWeight_GEN_ME_ONLY_MUR05_MUF1_PDF303200_PSMUR05_PSMUF1;   //!
   TBranch        *b_EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF05_PDF303200_PSMUR1_PSMUF05;   //!
   TBranch        *b_EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF14068;   //!
   TBranch        *b_EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF269000;   //!
   TBranch        *b_EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF270000;   //!
   TBranch        *b_EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF27400;   //!
   TBranch        *b_EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303200;   //!
   TBranch        *b_EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303200_ASSEW;   //!
   TBranch        *b_EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303200_ASSEWLO1;   //!
   TBranch        *b_EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303200_ASSEWLO1LO2;   //!
   TBranch        *b_EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303200_ASSEWLO1LO2LO3;   //!
   TBranch        *b_EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303200_EXPASSEW;   //!
   TBranch        *b_EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303200_EXPASSEWLO1;   //!
   TBranch        *b_EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303200_EXPASSEWLO1LO2;   //!
   TBranch        *b_EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303200_EXPASSEWLO1LO2LO3;   //!
   TBranch        *b_EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303200_MULTIASSEW;   //!
   TBranch        *b_EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303200_MULTIASSEWLO1;   //!
   TBranch        *b_EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303200_MULTIASSEWLO1LO2;   //!
   TBranch        *b_EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303200_MULTIASSEWLO1LO2LO3;   //!
   TBranch        *b_EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303201;   //!
   TBranch        *b_EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303202;   //!
   TBranch        *b_EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303203;   //!
   TBranch        *b_EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303204;   //!
   TBranch        *b_EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303205;   //!
   TBranch        *b_EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303206;   //!
   TBranch        *b_EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303207;   //!
   TBranch        *b_EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303208;   //!
   TBranch        *b_EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303209;   //!
   TBranch        *b_EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303210;   //!
   TBranch        *b_EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303211;   //!
   TBranch        *b_EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303212;   //!
   TBranch        *b_EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303213;   //!
   TBranch        *b_EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303214;   //!
   TBranch        *b_EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303215;   //!
   TBranch        *b_EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303216;   //!
   TBranch        *b_EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303217;   //!
   TBranch        *b_EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303218;   //!
   TBranch        *b_EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303219;   //!
   TBranch        *b_EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303220;   //!
   TBranch        *b_EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303221;   //!
   TBranch        *b_EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303222;   //!
   TBranch        *b_EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303223;   //!
   TBranch        *b_EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303224;   //!
   TBranch        *b_EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303225;   //!
   TBranch        *b_EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303226;   //!
   TBranch        *b_EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303227;   //!
   TBranch        *b_EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303228;   //!
   TBranch        *b_EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303229;   //!
   TBranch        *b_EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303230;   //!
   TBranch        *b_EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303231;   //!
   TBranch        *b_EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303232;   //!
   TBranch        *b_EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303233;   //!
   TBranch        *b_EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303234;   //!
   TBranch        *b_EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303235;   //!
   TBranch        *b_EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303236;   //!
   TBranch        *b_EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303237;   //!
   TBranch        *b_EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303238;   //!
   TBranch        *b_EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303239;   //!
   TBranch        *b_EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303240;   //!
   TBranch        *b_EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303241;   //!
   TBranch        *b_EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303242;   //!
   TBranch        *b_EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303243;   //!
   TBranch        *b_EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303244;   //!
   TBranch        *b_EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303245;   //!
   TBranch        *b_EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303246;   //!
   TBranch        *b_EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303247;   //!
   TBranch        *b_EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303248;   //!
   TBranch        *b_EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303249;   //!
   TBranch        *b_EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303250;   //!
   TBranch        *b_EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303251;   //!
   TBranch        *b_EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303252;   //!
   TBranch        *b_EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303253;   //!
   TBranch        *b_EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303254;   //!
   TBranch        *b_EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303255;   //!
   TBranch        *b_EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303256;   //!
   TBranch        *b_EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303257;   //!
   TBranch        *b_EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303258;   //!
   TBranch        *b_EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303259;   //!
   TBranch        *b_EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303260;   //!
   TBranch        *b_EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303261;   //!
   TBranch        *b_EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303262;   //!
   TBranch        *b_EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303263;   //!
   TBranch        *b_EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303264;   //!
   TBranch        *b_EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303265;   //!
   TBranch        *b_EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303266;   //!
   TBranch        *b_EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303267;   //!
   TBranch        *b_EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303268;   //!
   TBranch        *b_EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303269;   //!
   TBranch        *b_EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303270;   //!
   TBranch        *b_EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303271;   //!
   TBranch        *b_EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303272;   //!
   TBranch        *b_EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303273;   //!
   TBranch        *b_EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303274;   //!
   TBranch        *b_EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303275;   //!
   TBranch        *b_EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303276;   //!
   TBranch        *b_EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303277;   //!
   TBranch        *b_EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303278;   //!
   TBranch        *b_EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303279;   //!
   TBranch        *b_EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303280;   //!
   TBranch        *b_EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303281;   //!
   TBranch        *b_EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303282;   //!
   TBranch        *b_EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303283;   //!
   TBranch        *b_EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303284;   //!
   TBranch        *b_EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303285;   //!
   TBranch        *b_EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303286;   //!
   TBranch        *b_EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303287;   //!
   TBranch        *b_EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303288;   //!
   TBranch        *b_EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303289;   //!
   TBranch        *b_EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303290;   //!
   TBranch        *b_EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303291;   //!
   TBranch        *b_EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303292;   //!
   TBranch        *b_EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303293;   //!
   TBranch        *b_EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303294;   //!
   TBranch        *b_EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303295;   //!
   TBranch        *b_EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303296;   //!
   TBranch        *b_EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303297;   //!
   TBranch        *b_EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303298;   //!
   TBranch        *b_EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303299;   //!
   TBranch        *b_EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303300;   //!
   TBranch        *b_EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF304400;   //!
   TBranch        *b_EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF91400;   //!
   TBranch        *b_EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF91401;   //!
   TBranch        *b_EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF91402;   //!
   TBranch        *b_EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF91403;   //!
   TBranch        *b_EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF91404;   //!
   TBranch        *b_EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF91405;   //!
   TBranch        *b_EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF91406;   //!
   TBranch        *b_EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF91407;   //!
   TBranch        *b_EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF91408;   //!
   TBranch        *b_EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF91409;   //!
   TBranch        *b_EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF91410;   //!
   TBranch        *b_EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF91411;   //!
   TBranch        *b_EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF91412;   //!
   TBranch        *b_EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF91413;   //!
   TBranch        *b_EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF91414;   //!
   TBranch        *b_EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF91415;   //!
   TBranch        *b_EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF91416;   //!
   TBranch        *b_EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF91417;   //!
   TBranch        *b_EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF91418;   //!
   TBranch        *b_EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF91419;   //!
   TBranch        *b_EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF91420;   //!
   TBranch        *b_EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF91421;   //!
   TBranch        *b_EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF91422;   //!
   TBranch        *b_EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF91423;   //!
   TBranch        *b_EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF91424;   //!
   TBranch        *b_EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF91425;   //!
   TBranch        *b_EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF91426;   //!
   TBranch        *b_EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF91427;   //!
   TBranch        *b_EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF91428;   //!
   TBranch        *b_EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF91429;   //!
   TBranch        *b_EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF91430;   //!
   TBranch        *b_EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF91431;   //!
   TBranch        *b_EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF91432;   //!
   TBranch        *b_EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF2_PDF303200_PSMUR1_PSMUF2;   //!
   TBranch        *b_EventInfo_generatorWeight_GEN_ME_ONLY_MUR2_MUF1_PDF303200_PSMUR2_PSMUF1;   //!
   TBranch        *b_EventInfo_generatorWeight_GEN_ME_ONLY_MUR2_MUF2_PDF303200_PSMUR2_PSMUF2;   //!
   TBranch        *b_EventInfo_generatorWeight_GEN_MUR05_MUF05_PDF303200_PSMUR05_PSMUF05;   //!
   TBranch        *b_EventInfo_generatorWeight_GEN_MUR05_MUF1_PDF303200_PSMUR05_PSMUF1;   //!
   TBranch        *b_EventInfo_generatorWeight_GEN_MUR1_MUF05_PDF303200_PSMUR1_PSMUF05;   //!
   TBranch        *b_EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF14068;   //!
   TBranch        *b_EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF269000;   //!
   TBranch        *b_EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF270000;   //!
   TBranch        *b_EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF27400;   //!
   TBranch        *b_EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303200;   //!
   TBranch        *b_EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303200_ASSEW;   //!
   TBranch        *b_EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303200_ASSEWLO1;   //!
   TBranch        *b_EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303200_ASSEWLO1LO2;   //!
   TBranch        *b_EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303200_ASSEWLO1LO2LO3;   //!
   TBranch        *b_EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303200_EXPASSEW;   //!
   TBranch        *b_EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303200_EXPASSEWLO1;   //!
   TBranch        *b_EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303200_EXPASSEWLO1LO2;   //!
   TBranch        *b_EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303200_EXPASSEWLO1LO2LO3;   //!
   TBranch        *b_EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303200_MULTIASSEW;   //!
   TBranch        *b_EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303200_MULTIASSEWLO1;   //!
   TBranch        *b_EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303200_MULTIASSEWLO1LO2;   //!
   TBranch        *b_EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303200_MULTIASSEWLO1LO2LO3;   //!
   TBranch        *b_EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303201;   //!
   TBranch        *b_EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303202;   //!
   TBranch        *b_EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303203;   //!
   TBranch        *b_EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303204;   //!
   TBranch        *b_EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303205;   //!
   TBranch        *b_EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303206;   //!
   TBranch        *b_EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303207;   //!
   TBranch        *b_EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303208;   //!
   TBranch        *b_EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303209;   //!
   TBranch        *b_EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303210;   //!
   TBranch        *b_EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303211;   //!
   TBranch        *b_EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303212;   //!
   TBranch        *b_EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303213;   //!
   TBranch        *b_EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303214;   //!
   TBranch        *b_EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303215;   //!
   TBranch        *b_EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303216;   //!
   TBranch        *b_EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303217;   //!
   TBranch        *b_EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303218;   //!
   TBranch        *b_EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303219;   //!
   TBranch        *b_EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303220;   //!
   TBranch        *b_EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303221;   //!
   TBranch        *b_EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303222;   //!
   TBranch        *b_EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303223;   //!
   TBranch        *b_EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303224;   //!
   TBranch        *b_EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303225;   //!
   TBranch        *b_EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303226;   //!
   TBranch        *b_EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303227;   //!
   TBranch        *b_EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303228;   //!
   TBranch        *b_EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303229;   //!
   TBranch        *b_EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303230;   //!
   TBranch        *b_EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303231;   //!
   TBranch        *b_EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303232;   //!
   TBranch        *b_EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303233;   //!
   TBranch        *b_EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303234;   //!
   TBranch        *b_EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303235;   //!
   TBranch        *b_EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303236;   //!
   TBranch        *b_EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303237;   //!
   TBranch        *b_EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303238;   //!
   TBranch        *b_EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303239;   //!
   TBranch        *b_EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303240;   //!
   TBranch        *b_EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303241;   //!
   TBranch        *b_EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303242;   //!
   TBranch        *b_EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303243;   //!
   TBranch        *b_EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303244;   //!
   TBranch        *b_EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303245;   //!
   TBranch        *b_EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303246;   //!
   TBranch        *b_EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303247;   //!
   TBranch        *b_EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303248;   //!
   TBranch        *b_EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303249;   //!
   TBranch        *b_EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303250;   //!
   TBranch        *b_EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303251;   //!
   TBranch        *b_EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303252;   //!
   TBranch        *b_EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303253;   //!
   TBranch        *b_EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303254;   //!
   TBranch        *b_EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303255;   //!
   TBranch        *b_EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303256;   //!
   TBranch        *b_EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303257;   //!
   TBranch        *b_EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303258;   //!
   TBranch        *b_EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303259;   //!
   TBranch        *b_EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303260;   //!
   TBranch        *b_EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303261;   //!
   TBranch        *b_EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303262;   //!
   TBranch        *b_EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303263;   //!
   TBranch        *b_EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303264;   //!
   TBranch        *b_EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303265;   //!
   TBranch        *b_EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303266;   //!
   TBranch        *b_EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303267;   //!
   TBranch        *b_EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303268;   //!
   TBranch        *b_EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303269;   //!
   TBranch        *b_EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303270;   //!
   TBranch        *b_EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303271;   //!
   TBranch        *b_EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303272;   //!
   TBranch        *b_EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303273;   //!
   TBranch        *b_EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303274;   //!
   TBranch        *b_EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303275;   //!
   TBranch        *b_EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303276;   //!
   TBranch        *b_EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303277;   //!
   TBranch        *b_EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303278;   //!
   TBranch        *b_EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303279;   //!
   TBranch        *b_EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303280;   //!
   TBranch        *b_EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303281;   //!
   TBranch        *b_EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303282;   //!
   TBranch        *b_EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303283;   //!
   TBranch        *b_EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303284;   //!
   TBranch        *b_EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303285;   //!
   TBranch        *b_EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303286;   //!
   TBranch        *b_EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303287;   //!
   TBranch        *b_EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303288;   //!
   TBranch        *b_EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303289;   //!
   TBranch        *b_EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303290;   //!
   TBranch        *b_EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303291;   //!
   TBranch        *b_EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303292;   //!
   TBranch        *b_EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303293;   //!
   TBranch        *b_EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303294;   //!
   TBranch        *b_EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303295;   //!
   TBranch        *b_EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303296;   //!
   TBranch        *b_EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303297;   //!
   TBranch        *b_EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303298;   //!
   TBranch        *b_EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303299;   //!
   TBranch        *b_EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303300;   //!
   TBranch        *b_EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF304400;   //!
   TBranch        *b_EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF91400;   //!
   TBranch        *b_EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF91401;   //!
   TBranch        *b_EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF91402;   //!
   TBranch        *b_EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF91403;   //!
   TBranch        *b_EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF91404;   //!
   TBranch        *b_EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF91405;   //!
   TBranch        *b_EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF91406;   //!
   TBranch        *b_EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF91407;   //!
   TBranch        *b_EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF91408;   //!
   TBranch        *b_EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF91409;   //!
   TBranch        *b_EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF91410;   //!
   TBranch        *b_EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF91411;   //!
   TBranch        *b_EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF91412;   //!
   TBranch        *b_EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF91413;   //!
   TBranch        *b_EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF91414;   //!
   TBranch        *b_EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF91415;   //!
   TBranch        *b_EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF91416;   //!
   TBranch        *b_EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF91417;   //!
   TBranch        *b_EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF91418;   //!
   TBranch        *b_EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF91419;   //!
   TBranch        *b_EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF91420;   //!
   TBranch        *b_EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF91421;   //!
   TBranch        *b_EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF91422;   //!
   TBranch        *b_EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF91423;   //!
   TBranch        *b_EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF91424;   //!
   TBranch        *b_EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF91425;   //!
   TBranch        *b_EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF91426;   //!
   TBranch        *b_EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF91427;   //!
   TBranch        *b_EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF91428;   //!
   TBranch        *b_EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF91429;   //!
   TBranch        *b_EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF91430;   //!
   TBranch        *b_EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF91431;   //!
   TBranch        *b_EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF91432;   //!
   TBranch        *b_EventInfo_generatorWeight_GEN_MUR1_MUF2_PDF303200_PSMUR1_PSMUF2;   //!
   TBranch        *b_EventInfo_generatorWeight_GEN_MUR2_MUF1_PDF303200_PSMUR2_PSMUF1;   //!
   TBranch        *b_EventInfo_generatorWeight_GEN_MUR2_MUF2_PDF303200_PSMUR2_PSMUF2;   //!
   TBranch        *b_EventInfo_generatorWeight_GEN_NTrials;   //!
   TBranch        *b_EventInfo_generatorWeight_GEN_UserHook;   //!
   TBranch        *b_EventInfo_generatorWeight_GEN_WeightNormalisation;   //!
   TBranch        *b_EventInfo_jvt_effSF_JET_EffectiveNP_1__1up;   //!
   TBranch        *b_AnalysisElectrons_el_selected_JET_EffectiveNP_1__1up;   //!
   TBranch        *b_AnalysisMuons_mu_selected_JET_EffectiveNP_1__1up;   //!
   TBranch        *b_AnalysisJets_pt_JET_EffectiveNP_1__1up;   //!
   TBranch        *b_AnalysisJets_jet_selected_JET_EffectiveNP_1__1up;   //!
   TBranch        *b_AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_JET_EffectiveNP_1__1up;   //!
   TBranch        *b_METInfo_MET_JET_EffectiveNP_1__1up;   //!
   TBranch        *b_METInfo_METPhi_JET_EffectiveNP_1__1up;   //!
   TBranch        *b_EventInfo_jvt_effSF_JET_EffectiveNP_1__1down;   //!
   TBranch        *b_AnalysisElectrons_el_selected_JET_EffectiveNP_1__1down;   //!
   TBranch        *b_AnalysisMuons_mu_selected_JET_EffectiveNP_1__1down;   //!
   TBranch        *b_AnalysisJets_pt_JET_EffectiveNP_1__1down;   //!
   TBranch        *b_AnalysisJets_jet_selected_JET_EffectiveNP_1__1down;   //!
   TBranch        *b_AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_JET_EffectiveNP_1__1down;   //!
   TBranch        *b_METInfo_MET_JET_EffectiveNP_1__1down;   //!
   TBranch        *b_METInfo_METPhi_JET_EffectiveNP_1__1down;   //!
   TBranch        *b_EventInfo_jvt_effSF_JET_EffectiveNP_2__1up;   //!
   TBranch        *b_AnalysisElectrons_el_selected_JET_EffectiveNP_2__1up;   //!
   TBranch        *b_AnalysisMuons_mu_selected_JET_EffectiveNP_2__1up;   //!
   TBranch        *b_AnalysisJets_pt_JET_EffectiveNP_2__1up;   //!
   TBranch        *b_AnalysisJets_jet_selected_JET_EffectiveNP_2__1up;   //!
   TBranch        *b_AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_JET_EffectiveNP_2__1up;   //!
   TBranch        *b_METInfo_MET_JET_EffectiveNP_2__1up;   //!
   TBranch        *b_METInfo_METPhi_JET_EffectiveNP_2__1up;   //!
   TBranch        *b_EventInfo_jvt_effSF_JET_EffectiveNP_2__1down;   //!
   TBranch        *b_AnalysisElectrons_el_selected_JET_EffectiveNP_2__1down;   //!
   TBranch        *b_AnalysisMuons_mu_selected_JET_EffectiveNP_2__1down;   //!
   TBranch        *b_AnalysisJets_pt_JET_EffectiveNP_2__1down;   //!
   TBranch        *b_AnalysisJets_jet_selected_JET_EffectiveNP_2__1down;   //!
   TBranch        *b_AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_JET_EffectiveNP_2__1down;   //!
   TBranch        *b_METInfo_MET_JET_EffectiveNP_2__1down;   //!
   TBranch        *b_METInfo_METPhi_JET_EffectiveNP_2__1down;   //!
   TBranch        *b_EventInfo_jvt_effSF_JET_EffectiveNP_3__1up;   //!
   TBranch        *b_AnalysisElectrons_el_selected_JET_EffectiveNP_3__1up;   //!
   TBranch        *b_AnalysisMuons_mu_selected_JET_EffectiveNP_3__1up;   //!
   TBranch        *b_AnalysisJets_pt_JET_EffectiveNP_3__1up;   //!
   TBranch        *b_AnalysisJets_jet_selected_JET_EffectiveNP_3__1up;   //!
   TBranch        *b_AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_JET_EffectiveNP_3__1up;   //!
   TBranch        *b_METInfo_MET_JET_EffectiveNP_3__1up;   //!
   TBranch        *b_METInfo_METPhi_JET_EffectiveNP_3__1up;   //!
   TBranch        *b_EventInfo_jvt_effSF_JET_EffectiveNP_3__1down;   //!
   TBranch        *b_AnalysisElectrons_el_selected_JET_EffectiveNP_3__1down;   //!
   TBranch        *b_AnalysisMuons_mu_selected_JET_EffectiveNP_3__1down;   //!
   TBranch        *b_AnalysisJets_pt_JET_EffectiveNP_3__1down;   //!
   TBranch        *b_AnalysisJets_jet_selected_JET_EffectiveNP_3__1down;   //!
   TBranch        *b_AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_JET_EffectiveNP_3__1down;   //!
   TBranch        *b_METInfo_MET_JET_EffectiveNP_3__1down;   //!
   TBranch        *b_METInfo_METPhi_JET_EffectiveNP_3__1down;   //!
   TBranch        *b_EventInfo_jvt_effSF_JET_EffectiveNP_4__1up;   //!
   TBranch        *b_AnalysisElectrons_el_selected_JET_EffectiveNP_4__1up;   //!
   TBranch        *b_AnalysisMuons_mu_selected_JET_EffectiveNP_4__1up;   //!
   TBranch        *b_AnalysisJets_pt_JET_EffectiveNP_4__1up;   //!
   TBranch        *b_AnalysisJets_jet_selected_JET_EffectiveNP_4__1up;   //!
   TBranch        *b_AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_JET_EffectiveNP_4__1up;   //!
   TBranch        *b_METInfo_MET_JET_EffectiveNP_4__1up;   //!
   TBranch        *b_METInfo_METPhi_JET_EffectiveNP_4__1up;   //!
   TBranch        *b_EventInfo_jvt_effSF_JET_EffectiveNP_4__1down;   //!
   TBranch        *b_AnalysisElectrons_el_selected_JET_EffectiveNP_4__1down;   //!
   TBranch        *b_AnalysisMuons_mu_selected_JET_EffectiveNP_4__1down;   //!
   TBranch        *b_AnalysisJets_pt_JET_EffectiveNP_4__1down;   //!
   TBranch        *b_AnalysisJets_jet_selected_JET_EffectiveNP_4__1down;   //!
   TBranch        *b_AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_JET_EffectiveNP_4__1down;   //!
   TBranch        *b_METInfo_MET_JET_EffectiveNP_4__1down;   //!
   TBranch        *b_METInfo_METPhi_JET_EffectiveNP_4__1down;   //!
   TBranch        *b_EventInfo_jvt_effSF_JET_EffectiveNP_5__1up;   //!
   TBranch        *b_AnalysisElectrons_el_selected_JET_EffectiveNP_5__1up;   //!
   TBranch        *b_AnalysisMuons_mu_selected_JET_EffectiveNP_5__1up;   //!
   TBranch        *b_AnalysisJets_pt_JET_EffectiveNP_5__1up;   //!
   TBranch        *b_AnalysisJets_jet_selected_JET_EffectiveNP_5__1up;   //!
   TBranch        *b_AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_JET_EffectiveNP_5__1up;   //!
   TBranch        *b_METInfo_MET_JET_EffectiveNP_5__1up;   //!
   TBranch        *b_METInfo_METPhi_JET_EffectiveNP_5__1up;   //!
   TBranch        *b_EventInfo_jvt_effSF_JET_EffectiveNP_5__1down;   //!
   TBranch        *b_AnalysisElectrons_el_selected_JET_EffectiveNP_5__1down;   //!
   TBranch        *b_AnalysisMuons_mu_selected_JET_EffectiveNP_5__1down;   //!
   TBranch        *b_AnalysisJets_pt_JET_EffectiveNP_5__1down;   //!
   TBranch        *b_AnalysisJets_jet_selected_JET_EffectiveNP_5__1down;   //!
   TBranch        *b_AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_JET_EffectiveNP_5__1down;   //!
   TBranch        *b_METInfo_MET_JET_EffectiveNP_5__1down;   //!
   TBranch        *b_METInfo_METPhi_JET_EffectiveNP_5__1down;   //!
   TBranch        *b_EventInfo_jvt_effSF_JET_EffectiveNP_6__1up;   //!
   TBranch        *b_AnalysisElectrons_el_selected_JET_EffectiveNP_6__1up;   //!
   TBranch        *b_AnalysisMuons_mu_selected_JET_EffectiveNP_6__1up;   //!
   TBranch        *b_AnalysisJets_pt_JET_EffectiveNP_6__1up;   //!
   TBranch        *b_AnalysisJets_jet_selected_JET_EffectiveNP_6__1up;   //!
   TBranch        *b_AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_JET_EffectiveNP_6__1up;   //!
   TBranch        *b_METInfo_MET_JET_EffectiveNP_6__1up;   //!
   TBranch        *b_METInfo_METPhi_JET_EffectiveNP_6__1up;   //!
   TBranch        *b_EventInfo_jvt_effSF_JET_EffectiveNP_6__1down;   //!
   TBranch        *b_AnalysisElectrons_el_selected_JET_EffectiveNP_6__1down;   //!
   TBranch        *b_AnalysisMuons_mu_selected_JET_EffectiveNP_6__1down;   //!
   TBranch        *b_AnalysisJets_pt_JET_EffectiveNP_6__1down;   //!
   TBranch        *b_AnalysisJets_jet_selected_JET_EffectiveNP_6__1down;   //!
   TBranch        *b_AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_JET_EffectiveNP_6__1down;   //!
   TBranch        *b_METInfo_MET_JET_EffectiveNP_6__1down;   //!
   TBranch        *b_METInfo_METPhi_JET_EffectiveNP_6__1down;   //!
   TBranch        *b_EventInfo_jvt_effSF_JET_EffectiveNP_7__1up;   //!
   TBranch        *b_AnalysisElectrons_el_selected_JET_EffectiveNP_7__1up;   //!
   TBranch        *b_AnalysisMuons_mu_selected_JET_EffectiveNP_7__1up;   //!
   TBranch        *b_AnalysisJets_pt_JET_EffectiveNP_7__1up;   //!
   TBranch        *b_AnalysisJets_jet_selected_JET_EffectiveNP_7__1up;   //!
   TBranch        *b_AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_JET_EffectiveNP_7__1up;   //!
   TBranch        *b_METInfo_MET_JET_EffectiveNP_7__1up;   //!
   TBranch        *b_METInfo_METPhi_JET_EffectiveNP_7__1up;   //!
   TBranch        *b_EventInfo_jvt_effSF_JET_EffectiveNP_7__1down;   //!
   TBranch        *b_AnalysisElectrons_el_selected_JET_EffectiveNP_7__1down;   //!
   TBranch        *b_AnalysisMuons_mu_selected_JET_EffectiveNP_7__1down;   //!
   TBranch        *b_AnalysisJets_pt_JET_EffectiveNP_7__1down;   //!
   TBranch        *b_AnalysisJets_jet_selected_JET_EffectiveNP_7__1down;   //!
   TBranch        *b_AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_JET_EffectiveNP_7__1down;   //!
   TBranch        *b_METInfo_MET_JET_EffectiveNP_7__1down;   //!
   TBranch        *b_METInfo_METPhi_JET_EffectiveNP_7__1down;   //!
   TBranch        *b_EventInfo_jvt_effSF_JET_EffectiveNP_8restTerm__1up;   //!
   TBranch        *b_AnalysisElectrons_el_selected_JET_EffectiveNP_8restTerm__1up;   //!
   TBranch        *b_AnalysisMuons_mu_selected_JET_EffectiveNP_8restTerm__1up;   //!
   TBranch        *b_AnalysisJets_pt_JET_EffectiveNP_8restTerm__1up;   //!
   TBranch        *b_AnalysisJets_jet_selected_JET_EffectiveNP_8restTerm__1up;   //!
   TBranch        *b_AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_JET_EffectiveNP_8restTerm__1up;   //!
   TBranch        *b_METInfo_MET_JET_EffectiveNP_8restTerm__1up;   //!
   TBranch        *b_METInfo_METPhi_JET_EffectiveNP_8restTerm__1up;   //!
   TBranch        *b_EventInfo_jvt_effSF_JET_EffectiveNP_8restTerm__1down;   //!
   TBranch        *b_AnalysisElectrons_el_selected_JET_EffectiveNP_8restTerm__1down;   //!
   TBranch        *b_AnalysisMuons_mu_selected_JET_EffectiveNP_8restTerm__1down;   //!
   TBranch        *b_AnalysisJets_pt_JET_EffectiveNP_8restTerm__1down;   //!
   TBranch        *b_AnalysisJets_jet_selected_JET_EffectiveNP_8restTerm__1down;   //!
   TBranch        *b_AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_JET_EffectiveNP_8restTerm__1down;   //!
   TBranch        *b_METInfo_MET_JET_EffectiveNP_8restTerm__1down;   //!
   TBranch        *b_METInfo_METPhi_JET_EffectiveNP_8restTerm__1down;   //!
   TBranch        *b_EventInfo_jvt_effSF_JET_EtaIntercalibration_Modelling__1up;   //!
   TBranch        *b_AnalysisElectrons_el_selected_JET_EtaIntercalibration_Modelling__1up;   //!
   TBranch        *b_AnalysisMuons_mu_selected_JET_EtaIntercalibration_Modelling__1up;   //!
   TBranch        *b_AnalysisJets_pt_JET_EtaIntercalibration_Modelling__1up;   //!
   TBranch        *b_AnalysisJets_jet_selected_JET_EtaIntercalibration_Modelling__1up;   //!
   TBranch        *b_AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_JET_EtaIntercalibration_Modelling__1up;   //!
   TBranch        *b_METInfo_MET_JET_EtaIntercalibration_Modelling__1up;   //!
   TBranch        *b_METInfo_METPhi_JET_EtaIntercalibration_Modelling__1up;   //!
   TBranch        *b_EventInfo_jvt_effSF_JET_EtaIntercalibration_Modelling__1down;   //!
   TBranch        *b_AnalysisElectrons_el_selected_JET_EtaIntercalibration_Modelling__1down;   //!
   TBranch        *b_AnalysisMuons_mu_selected_JET_EtaIntercalibration_Modelling__1down;   //!
   TBranch        *b_AnalysisJets_pt_JET_EtaIntercalibration_Modelling__1down;   //!
   TBranch        *b_AnalysisJets_jet_selected_JET_EtaIntercalibration_Modelling__1down;   //!
   TBranch        *b_AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_JET_EtaIntercalibration_Modelling__1down;   //!
   TBranch        *b_METInfo_MET_JET_EtaIntercalibration_Modelling__1down;   //!
   TBranch        *b_METInfo_METPhi_JET_EtaIntercalibration_Modelling__1down;   //!
   TBranch        *b_EventInfo_jvt_effSF_JET_EtaIntercalibration_NonClosure_highE__1up;   //!
   TBranch        *b_AnalysisElectrons_el_selected_JET_EtaIntercalibration_NonClosure_highE__1up;   //!
   TBranch        *b_AnalysisMuons_mu_selected_JET_EtaIntercalibration_NonClosure_highE__1up;   //!
   TBranch        *b_AnalysisJets_pt_JET_EtaIntercalibration_NonClosure_highE__1up;   //!
   TBranch        *b_AnalysisJets_jet_selected_JET_EtaIntercalibration_NonClosure_highE__1up;   //!
   TBranch        *b_AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_JET_EtaIntercalibration_NonClosure_highE__1up;   //!
   TBranch        *b_METInfo_MET_JET_EtaIntercalibration_NonClosure_highE__1up;   //!
   TBranch        *b_METInfo_METPhi_JET_EtaIntercalibration_NonClosure_highE__1up;   //!
   TBranch        *b_EventInfo_jvt_effSF_JET_EtaIntercalibration_NonClosure_highE__1down;   //!
   TBranch        *b_AnalysisElectrons_el_selected_JET_EtaIntercalibration_NonClosure_highE__1down;   //!
   TBranch        *b_AnalysisMuons_mu_selected_JET_EtaIntercalibration_NonClosure_highE__1down;   //!
   TBranch        *b_AnalysisJets_pt_JET_EtaIntercalibration_NonClosure_highE__1down;   //!
   TBranch        *b_AnalysisJets_jet_selected_JET_EtaIntercalibration_NonClosure_highE__1down;   //!
   TBranch        *b_AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_JET_EtaIntercalibration_NonClosure_highE__1down;   //!
   TBranch        *b_METInfo_MET_JET_EtaIntercalibration_NonClosure_highE__1down;   //!
   TBranch        *b_METInfo_METPhi_JET_EtaIntercalibration_NonClosure_highE__1down;   //!
   TBranch        *b_EventInfo_jvt_effSF_JET_EtaIntercalibration_NonClosure_negEta__1up;   //!
   TBranch        *b_AnalysisElectrons_el_selected_JET_EtaIntercalibration_NonClosure_negEta__1up;   //!
   TBranch        *b_AnalysisMuons_mu_selected_JET_EtaIntercalibration_NonClosure_negEta__1up;   //!
   TBranch        *b_AnalysisJets_pt_JET_EtaIntercalibration_NonClosure_negEta__1up;   //!
   TBranch        *b_AnalysisJets_jet_selected_JET_EtaIntercalibration_NonClosure_negEta__1up;   //!
   TBranch        *b_AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_JET_EtaIntercalibration_NonClosure_negEta__1up;   //!
   TBranch        *b_METInfo_MET_JET_EtaIntercalibration_NonClosure_negEta__1up;   //!
   TBranch        *b_METInfo_METPhi_JET_EtaIntercalibration_NonClosure_negEta__1up;   //!
   TBranch        *b_EventInfo_jvt_effSF_JET_EtaIntercalibration_NonClosure_negEta__1down;   //!
   TBranch        *b_AnalysisElectrons_el_selected_JET_EtaIntercalibration_NonClosure_negEta__1down;   //!
   TBranch        *b_AnalysisMuons_mu_selected_JET_EtaIntercalibration_NonClosure_negEta__1down;   //!
   TBranch        *b_AnalysisJets_pt_JET_EtaIntercalibration_NonClosure_negEta__1down;   //!
   TBranch        *b_AnalysisJets_jet_selected_JET_EtaIntercalibration_NonClosure_negEta__1down;   //!
   TBranch        *b_AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_JET_EtaIntercalibration_NonClosure_negEta__1down;   //!
   TBranch        *b_METInfo_MET_JET_EtaIntercalibration_NonClosure_negEta__1down;   //!
   TBranch        *b_METInfo_METPhi_JET_EtaIntercalibration_NonClosure_negEta__1down;   //!
   TBranch        *b_EventInfo_jvt_effSF_JET_EtaIntercalibration_NonClosure_posEta__1up;   //!
   TBranch        *b_AnalysisElectrons_el_selected_JET_EtaIntercalibration_NonClosure_posEta__1up;   //!
   TBranch        *b_AnalysisMuons_mu_selected_JET_EtaIntercalibration_NonClosure_posEta__1up;   //!
   TBranch        *b_AnalysisJets_pt_JET_EtaIntercalibration_NonClosure_posEta__1up;   //!
   TBranch        *b_AnalysisJets_jet_selected_JET_EtaIntercalibration_NonClosure_posEta__1up;   //!
   TBranch        *b_AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_JET_EtaIntercalibration_NonClosure_posEta__1up;   //!
   TBranch        *b_METInfo_MET_JET_EtaIntercalibration_NonClosure_posEta__1up;   //!
   TBranch        *b_METInfo_METPhi_JET_EtaIntercalibration_NonClosure_posEta__1up;   //!
   TBranch        *b_EventInfo_jvt_effSF_JET_EtaIntercalibration_NonClosure_posEta__1down;   //!
   TBranch        *b_AnalysisElectrons_el_selected_JET_EtaIntercalibration_NonClosure_posEta__1down;   //!
   TBranch        *b_AnalysisMuons_mu_selected_JET_EtaIntercalibration_NonClosure_posEta__1down;   //!
   TBranch        *b_AnalysisJets_pt_JET_EtaIntercalibration_NonClosure_posEta__1down;   //!
   TBranch        *b_AnalysisJets_jet_selected_JET_EtaIntercalibration_NonClosure_posEta__1down;   //!
   TBranch        *b_AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_JET_EtaIntercalibration_NonClosure_posEta__1down;   //!
   TBranch        *b_METInfo_MET_JET_EtaIntercalibration_NonClosure_posEta__1down;   //!
   TBranch        *b_METInfo_METPhi_JET_EtaIntercalibration_NonClosure_posEta__1down;   //!
   TBranch        *b_EventInfo_jvt_effSF_JET_EtaIntercalibration_TotalStat__1up;   //!
   TBranch        *b_AnalysisElectrons_el_selected_JET_EtaIntercalibration_TotalStat__1up;   //!
   TBranch        *b_AnalysisMuons_mu_selected_JET_EtaIntercalibration_TotalStat__1up;   //!
   TBranch        *b_AnalysisJets_pt_JET_EtaIntercalibration_TotalStat__1up;   //!
   TBranch        *b_AnalysisJets_jet_selected_JET_EtaIntercalibration_TotalStat__1up;   //!
   TBranch        *b_AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_JET_EtaIntercalibration_TotalStat__1up;   //!
   TBranch        *b_METInfo_MET_JET_EtaIntercalibration_TotalStat__1up;   //!
   TBranch        *b_METInfo_METPhi_JET_EtaIntercalibration_TotalStat__1up;   //!
   TBranch        *b_EventInfo_jvt_effSF_JET_EtaIntercalibration_TotalStat__1down;   //!
   TBranch        *b_AnalysisElectrons_el_selected_JET_EtaIntercalibration_TotalStat__1down;   //!
   TBranch        *b_AnalysisMuons_mu_selected_JET_EtaIntercalibration_TotalStat__1down;   //!
   TBranch        *b_AnalysisJets_pt_JET_EtaIntercalibration_TotalStat__1down;   //!
   TBranch        *b_AnalysisJets_jet_selected_JET_EtaIntercalibration_TotalStat__1down;   //!
   TBranch        *b_AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_JET_EtaIntercalibration_TotalStat__1down;   //!
   TBranch        *b_METInfo_MET_JET_EtaIntercalibration_TotalStat__1down;   //!
   TBranch        *b_METInfo_METPhi_JET_EtaIntercalibration_TotalStat__1down;   //!
   TBranch        *b_EventInfo_jvt_effSF_JET_Flavor_Composition__1up;   //!
   TBranch        *b_AnalysisElectrons_el_selected_JET_Flavor_Composition__1up;   //!
   TBranch        *b_AnalysisMuons_mu_selected_JET_Flavor_Composition__1up;   //!
   TBranch        *b_AnalysisJets_pt_JET_Flavor_Composition__1up;   //!
   TBranch        *b_AnalysisJets_jet_selected_JET_Flavor_Composition__1up;   //!
   TBranch        *b_AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_JET_Flavor_Composition__1up;   //!
   TBranch        *b_METInfo_MET_JET_Flavor_Composition__1up;   //!
   TBranch        *b_METInfo_METPhi_JET_Flavor_Composition__1up;   //!
   TBranch        *b_EventInfo_jvt_effSF_JET_Flavor_Composition__1down;   //!
   TBranch        *b_AnalysisElectrons_el_selected_JET_Flavor_Composition__1down;   //!
   TBranch        *b_AnalysisMuons_mu_selected_JET_Flavor_Composition__1down;   //!
   TBranch        *b_AnalysisJets_pt_JET_Flavor_Composition__1down;   //!
   TBranch        *b_AnalysisJets_jet_selected_JET_Flavor_Composition__1down;   //!
   TBranch        *b_AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_JET_Flavor_Composition__1down;   //!
   TBranch        *b_METInfo_MET_JET_Flavor_Composition__1down;   //!
   TBranch        *b_METInfo_METPhi_JET_Flavor_Composition__1down;   //!
   TBranch        *b_EventInfo_jvt_effSF_JET_Flavor_Response__1up;   //!
   TBranch        *b_AnalysisElectrons_el_selected_JET_Flavor_Response__1up;   //!
   TBranch        *b_AnalysisMuons_mu_selected_JET_Flavor_Response__1up;   //!
   TBranch        *b_AnalysisJets_pt_JET_Flavor_Response__1up;   //!
   TBranch        *b_AnalysisJets_jet_selected_JET_Flavor_Response__1up;   //!
   TBranch        *b_AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_JET_Flavor_Response__1up;   //!
   TBranch        *b_METInfo_MET_JET_Flavor_Response__1up;   //!
   TBranch        *b_METInfo_METPhi_JET_Flavor_Response__1up;   //!
   TBranch        *b_EventInfo_jvt_effSF_JET_Flavor_Response__1down;   //!
   TBranch        *b_AnalysisElectrons_el_selected_JET_Flavor_Response__1down;   //!
   TBranch        *b_AnalysisMuons_mu_selected_JET_Flavor_Response__1down;   //!
   TBranch        *b_AnalysisJets_pt_JET_Flavor_Response__1down;   //!
   TBranch        *b_AnalysisJets_jet_selected_JET_Flavor_Response__1down;   //!
   TBranch        *b_AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_JET_Flavor_Response__1down;   //!
   TBranch        *b_METInfo_MET_JET_Flavor_Response__1down;   //!
   TBranch        *b_METInfo_METPhi_JET_Flavor_Response__1down;   //!
   TBranch        *b_EventInfo_jvt_effSF_JET_JER_DataVsMC_MC16__1up;   //!
   TBranch        *b_AnalysisElectrons_el_selected_JET_JER_DataVsMC_MC16__1up;   //!
   TBranch        *b_AnalysisMuons_mu_selected_JET_JER_DataVsMC_MC16__1up;   //!
   TBranch        *b_AnalysisJets_pt_JET_JER_DataVsMC_MC16__1up;   //!
   TBranch        *b_AnalysisJets_jet_selected_JET_JER_DataVsMC_MC16__1up;   //!
   TBranch        *b_AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_JET_JER_DataVsMC_MC16__1up;   //!
   TBranch        *b_METInfo_MET_JET_JER_DataVsMC_MC16__1up;   //!
   TBranch        *b_METInfo_METPhi_JET_JER_DataVsMC_MC16__1up;   //!
   TBranch        *b_EventInfo_jvt_effSF_JET_JER_DataVsMC_MC16__1down;   //!
   TBranch        *b_AnalysisElectrons_el_selected_JET_JER_DataVsMC_MC16__1down;   //!
   TBranch        *b_AnalysisMuons_mu_selected_JET_JER_DataVsMC_MC16__1down;   //!
   TBranch        *b_AnalysisJets_pt_JET_JER_DataVsMC_MC16__1down;   //!
   TBranch        *b_AnalysisJets_jet_selected_JET_JER_DataVsMC_MC16__1down;   //!
   TBranch        *b_AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_JET_JER_DataVsMC_MC16__1down;   //!
   TBranch        *b_METInfo_MET_JET_JER_DataVsMC_MC16__1down;   //!
   TBranch        *b_METInfo_METPhi_JET_JER_DataVsMC_MC16__1down;   //!
   TBranch        *b_EventInfo_jvt_effSF_JET_JER_EffectiveNP_1__1up;   //!
   TBranch        *b_AnalysisElectrons_el_selected_JET_JER_EffectiveNP_1__1up;   //!
   TBranch        *b_AnalysisMuons_mu_selected_JET_JER_EffectiveNP_1__1up;   //!
   TBranch        *b_AnalysisJets_pt_JET_JER_EffectiveNP_1__1up;   //!
   TBranch        *b_AnalysisJets_jet_selected_JET_JER_EffectiveNP_1__1up;   //!
   TBranch        *b_AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_JET_JER_EffectiveNP_1__1up;   //!
   TBranch        *b_METInfo_MET_JET_JER_EffectiveNP_1__1up;   //!
   TBranch        *b_METInfo_METPhi_JET_JER_EffectiveNP_1__1up;   //!
   TBranch        *b_EventInfo_jvt_effSF_JET_JER_EffectiveNP_1__1down;   //!
   TBranch        *b_AnalysisElectrons_el_selected_JET_JER_EffectiveNP_1__1down;   //!
   TBranch        *b_AnalysisMuons_mu_selected_JET_JER_EffectiveNP_1__1down;   //!
   TBranch        *b_AnalysisJets_pt_JET_JER_EffectiveNP_1__1down;   //!
   TBranch        *b_AnalysisJets_jet_selected_JET_JER_EffectiveNP_1__1down;   //!
   TBranch        *b_AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_JET_JER_EffectiveNP_1__1down;   //!
   TBranch        *b_METInfo_MET_JET_JER_EffectiveNP_1__1down;   //!
   TBranch        *b_METInfo_METPhi_JET_JER_EffectiveNP_1__1down;   //!
   TBranch        *b_EventInfo_jvt_effSF_JET_JER_EffectiveNP_2__1up;   //!
   TBranch        *b_AnalysisElectrons_el_selected_JET_JER_EffectiveNP_2__1up;   //!
   TBranch        *b_AnalysisMuons_mu_selected_JET_JER_EffectiveNP_2__1up;   //!
   TBranch        *b_AnalysisJets_pt_JET_JER_EffectiveNP_2__1up;   //!
   TBranch        *b_AnalysisJets_jet_selected_JET_JER_EffectiveNP_2__1up;   //!
   TBranch        *b_AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_JET_JER_EffectiveNP_2__1up;   //!
   TBranch        *b_METInfo_MET_JET_JER_EffectiveNP_2__1up;   //!
   TBranch        *b_METInfo_METPhi_JET_JER_EffectiveNP_2__1up;   //!
   TBranch        *b_EventInfo_jvt_effSF_JET_JER_EffectiveNP_2__1down;   //!
   TBranch        *b_AnalysisElectrons_el_selected_JET_JER_EffectiveNP_2__1down;   //!
   TBranch        *b_AnalysisMuons_mu_selected_JET_JER_EffectiveNP_2__1down;   //!
   TBranch        *b_AnalysisJets_pt_JET_JER_EffectiveNP_2__1down;   //!
   TBranch        *b_AnalysisJets_jet_selected_JET_JER_EffectiveNP_2__1down;   //!
   TBranch        *b_AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_JET_JER_EffectiveNP_2__1down;   //!
   TBranch        *b_METInfo_MET_JET_JER_EffectiveNP_2__1down;   //!
   TBranch        *b_METInfo_METPhi_JET_JER_EffectiveNP_2__1down;   //!
   TBranch        *b_EventInfo_jvt_effSF_JET_JER_EffectiveNP_3__1up;   //!
   TBranch        *b_AnalysisElectrons_el_selected_JET_JER_EffectiveNP_3__1up;   //!
   TBranch        *b_AnalysisMuons_mu_selected_JET_JER_EffectiveNP_3__1up;   //!
   TBranch        *b_AnalysisJets_pt_JET_JER_EffectiveNP_3__1up;   //!
   TBranch        *b_AnalysisJets_jet_selected_JET_JER_EffectiveNP_3__1up;   //!
   TBranch        *b_AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_JET_JER_EffectiveNP_3__1up;   //!
   TBranch        *b_METInfo_MET_JET_JER_EffectiveNP_3__1up;   //!
   TBranch        *b_METInfo_METPhi_JET_JER_EffectiveNP_3__1up;   //!
   TBranch        *b_EventInfo_jvt_effSF_JET_JER_EffectiveNP_3__1down;   //!
   TBranch        *b_AnalysisElectrons_el_selected_JET_JER_EffectiveNP_3__1down;   //!
   TBranch        *b_AnalysisMuons_mu_selected_JET_JER_EffectiveNP_3__1down;   //!
   TBranch        *b_AnalysisJets_pt_JET_JER_EffectiveNP_3__1down;   //!
   TBranch        *b_AnalysisJets_jet_selected_JET_JER_EffectiveNP_3__1down;   //!
   TBranch        *b_AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_JET_JER_EffectiveNP_3__1down;   //!
   TBranch        *b_METInfo_MET_JET_JER_EffectiveNP_3__1down;   //!
   TBranch        *b_METInfo_METPhi_JET_JER_EffectiveNP_3__1down;   //!
   TBranch        *b_EventInfo_jvt_effSF_JET_JER_EffectiveNP_4__1up;   //!
   TBranch        *b_AnalysisElectrons_el_selected_JET_JER_EffectiveNP_4__1up;   //!
   TBranch        *b_AnalysisMuons_mu_selected_JET_JER_EffectiveNP_4__1up;   //!
   TBranch        *b_AnalysisJets_pt_JET_JER_EffectiveNP_4__1up;   //!
   TBranch        *b_AnalysisJets_jet_selected_JET_JER_EffectiveNP_4__1up;   //!
   TBranch        *b_AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_JET_JER_EffectiveNP_4__1up;   //!
   TBranch        *b_METInfo_MET_JET_JER_EffectiveNP_4__1up;   //!
   TBranch        *b_METInfo_METPhi_JET_JER_EffectiveNP_4__1up;   //!
   TBranch        *b_EventInfo_jvt_effSF_JET_JER_EffectiveNP_4__1down;   //!
   TBranch        *b_AnalysisElectrons_el_selected_JET_JER_EffectiveNP_4__1down;   //!
   TBranch        *b_AnalysisMuons_mu_selected_JET_JER_EffectiveNP_4__1down;   //!
   TBranch        *b_AnalysisJets_pt_JET_JER_EffectiveNP_4__1down;   //!
   TBranch        *b_AnalysisJets_jet_selected_JET_JER_EffectiveNP_4__1down;   //!
   TBranch        *b_AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_JET_JER_EffectiveNP_4__1down;   //!
   TBranch        *b_METInfo_MET_JET_JER_EffectiveNP_4__1down;   //!
   TBranch        *b_METInfo_METPhi_JET_JER_EffectiveNP_4__1down;   //!
   TBranch        *b_EventInfo_jvt_effSF_JET_JER_EffectiveNP_5__1up;   //!
   TBranch        *b_AnalysisElectrons_el_selected_JET_JER_EffectiveNP_5__1up;   //!
   TBranch        *b_AnalysisMuons_mu_selected_JET_JER_EffectiveNP_5__1up;   //!
   TBranch        *b_AnalysisJets_pt_JET_JER_EffectiveNP_5__1up;   //!
   TBranch        *b_AnalysisJets_jet_selected_JET_JER_EffectiveNP_5__1up;   //!
   TBranch        *b_AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_JET_JER_EffectiveNP_5__1up;   //!
   TBranch        *b_METInfo_MET_JET_JER_EffectiveNP_5__1up;   //!
   TBranch        *b_METInfo_METPhi_JET_JER_EffectiveNP_5__1up;   //!
   TBranch        *b_EventInfo_jvt_effSF_JET_JER_EffectiveNP_5__1down;   //!
   TBranch        *b_AnalysisElectrons_el_selected_JET_JER_EffectiveNP_5__1down;   //!
   TBranch        *b_AnalysisMuons_mu_selected_JET_JER_EffectiveNP_5__1down;   //!
   TBranch        *b_AnalysisJets_pt_JET_JER_EffectiveNP_5__1down;   //!
   TBranch        *b_AnalysisJets_jet_selected_JET_JER_EffectiveNP_5__1down;   //!
   TBranch        *b_AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_JET_JER_EffectiveNP_5__1down;   //!
   TBranch        *b_METInfo_MET_JET_JER_EffectiveNP_5__1down;   //!
   TBranch        *b_METInfo_METPhi_JET_JER_EffectiveNP_5__1down;   //!
   TBranch        *b_EventInfo_jvt_effSF_JET_JER_EffectiveNP_6__1up;   //!
   TBranch        *b_AnalysisElectrons_el_selected_JET_JER_EffectiveNP_6__1up;   //!
   TBranch        *b_AnalysisMuons_mu_selected_JET_JER_EffectiveNP_6__1up;   //!
   TBranch        *b_AnalysisJets_pt_JET_JER_EffectiveNP_6__1up;   //!
   TBranch        *b_AnalysisJets_jet_selected_JET_JER_EffectiveNP_6__1up;   //!
   TBranch        *b_AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_JET_JER_EffectiveNP_6__1up;   //!
   TBranch        *b_METInfo_MET_JET_JER_EffectiveNP_6__1up;   //!
   TBranch        *b_METInfo_METPhi_JET_JER_EffectiveNP_6__1up;   //!
   TBranch        *b_EventInfo_jvt_effSF_JET_JER_EffectiveNP_6__1down;   //!
   TBranch        *b_AnalysisElectrons_el_selected_JET_JER_EffectiveNP_6__1down;   //!
   TBranch        *b_AnalysisMuons_mu_selected_JET_JER_EffectiveNP_6__1down;   //!
   TBranch        *b_AnalysisJets_pt_JET_JER_EffectiveNP_6__1down;   //!
   TBranch        *b_AnalysisJets_jet_selected_JET_JER_EffectiveNP_6__1down;   //!
   TBranch        *b_AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_JET_JER_EffectiveNP_6__1down;   //!
   TBranch        *b_METInfo_MET_JET_JER_EffectiveNP_6__1down;   //!
   TBranch        *b_METInfo_METPhi_JET_JER_EffectiveNP_6__1down;   //!
   TBranch        *b_EventInfo_jvt_effSF_JET_JER_EffectiveNP_7restTerm__1up;   //!
   TBranch        *b_AnalysisElectrons_el_selected_JET_JER_EffectiveNP_7restTerm__1up;   //!
   TBranch        *b_AnalysisMuons_mu_selected_JET_JER_EffectiveNP_7restTerm__1up;   //!
   TBranch        *b_AnalysisJets_pt_JET_JER_EffectiveNP_7restTerm__1up;   //!
   TBranch        *b_AnalysisJets_jet_selected_JET_JER_EffectiveNP_7restTerm__1up;   //!
   TBranch        *b_AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_JET_JER_EffectiveNP_7restTerm__1up;   //!
   TBranch        *b_METInfo_MET_JET_JER_EffectiveNP_7restTerm__1up;   //!
   TBranch        *b_METInfo_METPhi_JET_JER_EffectiveNP_7restTerm__1up;   //!
   TBranch        *b_EventInfo_jvt_effSF_JET_JER_EffectiveNP_7restTerm__1down;   //!
   TBranch        *b_AnalysisElectrons_el_selected_JET_JER_EffectiveNP_7restTerm__1down;   //!
   TBranch        *b_AnalysisMuons_mu_selected_JET_JER_EffectiveNP_7restTerm__1down;   //!
   TBranch        *b_AnalysisJets_pt_JET_JER_EffectiveNP_7restTerm__1down;   //!
   TBranch        *b_AnalysisJets_jet_selected_JET_JER_EffectiveNP_7restTerm__1down;   //!
   TBranch        *b_AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_JET_JER_EffectiveNP_7restTerm__1down;   //!
   TBranch        *b_METInfo_MET_JET_JER_EffectiveNP_7restTerm__1down;   //!
   TBranch        *b_METInfo_METPhi_JET_JER_EffectiveNP_7restTerm__1down;   //!
   TBranch        *b_EventInfo_jvt_effSF_JET_JvtEfficiency__1down;   //!
   TBranch        *b_EventInfo_jvt_effSF_JET_JvtEfficiency__1up;   //!
   TBranch        *b_EventInfo_jvt_effSF_JET_Pileup_OffsetMu__1up;   //!
   TBranch        *b_AnalysisElectrons_el_selected_JET_Pileup_OffsetMu__1up;   //!
   TBranch        *b_AnalysisMuons_mu_selected_JET_Pileup_OffsetMu__1up;   //!
   TBranch        *b_AnalysisJets_pt_JET_Pileup_OffsetMu__1up;   //!
   TBranch        *b_AnalysisJets_jet_selected_JET_Pileup_OffsetMu__1up;   //!
   TBranch        *b_AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_JET_Pileup_OffsetMu__1up;   //!
   TBranch        *b_METInfo_MET_JET_Pileup_OffsetMu__1up;   //!
   TBranch        *b_METInfo_METPhi_JET_Pileup_OffsetMu__1up;   //!
   TBranch        *b_EventInfo_jvt_effSF_JET_Pileup_OffsetMu__1down;   //!
   TBranch        *b_AnalysisElectrons_el_selected_JET_Pileup_OffsetMu__1down;   //!
   TBranch        *b_AnalysisMuons_mu_selected_JET_Pileup_OffsetMu__1down;   //!
   TBranch        *b_AnalysisJets_pt_JET_Pileup_OffsetMu__1down;   //!
   TBranch        *b_AnalysisJets_jet_selected_JET_Pileup_OffsetMu__1down;   //!
   TBranch        *b_AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_JET_Pileup_OffsetMu__1down;   //!
   TBranch        *b_METInfo_MET_JET_Pileup_OffsetMu__1down;   //!
   TBranch        *b_METInfo_METPhi_JET_Pileup_OffsetMu__1down;   //!
   TBranch        *b_EventInfo_jvt_effSF_JET_Pileup_OffsetNPV__1up;   //!
   TBranch        *b_AnalysisElectrons_el_selected_JET_Pileup_OffsetNPV__1up;   //!
   TBranch        *b_AnalysisMuons_mu_selected_JET_Pileup_OffsetNPV__1up;   //!
   TBranch        *b_AnalysisJets_pt_JET_Pileup_OffsetNPV__1up;   //!
   TBranch        *b_AnalysisJets_jet_selected_JET_Pileup_OffsetNPV__1up;   //!
   TBranch        *b_AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_JET_Pileup_OffsetNPV__1up;   //!
   TBranch        *b_METInfo_MET_JET_Pileup_OffsetNPV__1up;   //!
   TBranch        *b_METInfo_METPhi_JET_Pileup_OffsetNPV__1up;   //!
   TBranch        *b_EventInfo_jvt_effSF_JET_Pileup_OffsetNPV__1down;   //!
   TBranch        *b_AnalysisElectrons_el_selected_JET_Pileup_OffsetNPV__1down;   //!
   TBranch        *b_AnalysisMuons_mu_selected_JET_Pileup_OffsetNPV__1down;   //!
   TBranch        *b_AnalysisJets_pt_JET_Pileup_OffsetNPV__1down;   //!
   TBranch        *b_AnalysisJets_jet_selected_JET_Pileup_OffsetNPV__1down;   //!
   TBranch        *b_AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_JET_Pileup_OffsetNPV__1down;   //!
   TBranch        *b_METInfo_MET_JET_Pileup_OffsetNPV__1down;   //!
   TBranch        *b_METInfo_METPhi_JET_Pileup_OffsetNPV__1down;   //!
   TBranch        *b_EventInfo_jvt_effSF_JET_Pileup_PtTerm__1up;   //!
   TBranch        *b_AnalysisElectrons_el_selected_JET_Pileup_PtTerm__1up;   //!
   TBranch        *b_AnalysisMuons_mu_selected_JET_Pileup_PtTerm__1up;   //!
   TBranch        *b_AnalysisJets_pt_JET_Pileup_PtTerm__1up;   //!
   TBranch        *b_AnalysisJets_jet_selected_JET_Pileup_PtTerm__1up;   //!
   TBranch        *b_AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_JET_Pileup_PtTerm__1up;   //!
   TBranch        *b_METInfo_MET_JET_Pileup_PtTerm__1up;   //!
   TBranch        *b_METInfo_METPhi_JET_Pileup_PtTerm__1up;   //!
   TBranch        *b_EventInfo_jvt_effSF_JET_Pileup_PtTerm__1down;   //!
   TBranch        *b_AnalysisElectrons_el_selected_JET_Pileup_PtTerm__1down;   //!
   TBranch        *b_AnalysisMuons_mu_selected_JET_Pileup_PtTerm__1down;   //!
   TBranch        *b_AnalysisJets_pt_JET_Pileup_PtTerm__1down;   //!
   TBranch        *b_AnalysisJets_jet_selected_JET_Pileup_PtTerm__1down;   //!
   TBranch        *b_AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_JET_Pileup_PtTerm__1down;   //!
   TBranch        *b_METInfo_MET_JET_Pileup_PtTerm__1down;   //!
   TBranch        *b_METInfo_METPhi_JET_Pileup_PtTerm__1down;   //!
   TBranch        *b_EventInfo_jvt_effSF_JET_Pileup_RhoTopology__1up;   //!
   TBranch        *b_AnalysisElectrons_el_selected_JET_Pileup_RhoTopology__1up;   //!
   TBranch        *b_AnalysisMuons_mu_selected_JET_Pileup_RhoTopology__1up;   //!
   TBranch        *b_AnalysisJets_pt_JET_Pileup_RhoTopology__1up;   //!
   TBranch        *b_AnalysisJets_jet_selected_JET_Pileup_RhoTopology__1up;   //!
   TBranch        *b_AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_JET_Pileup_RhoTopology__1up;   //!
   TBranch        *b_METInfo_MET_JET_Pileup_RhoTopology__1up;   //!
   TBranch        *b_METInfo_METPhi_JET_Pileup_RhoTopology__1up;   //!
   TBranch        *b_EventInfo_jvt_effSF_JET_Pileup_RhoTopology__1down;   //!
   TBranch        *b_AnalysisElectrons_el_selected_JET_Pileup_RhoTopology__1down;   //!
   TBranch        *b_AnalysisMuons_mu_selected_JET_Pileup_RhoTopology__1down;   //!
   TBranch        *b_AnalysisJets_pt_JET_Pileup_RhoTopology__1down;   //!
   TBranch        *b_AnalysisJets_jet_selected_JET_Pileup_RhoTopology__1down;   //!
   TBranch        *b_AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_JET_Pileup_RhoTopology__1down;   //!
   TBranch        *b_METInfo_MET_JET_Pileup_RhoTopology__1down;   //!
   TBranch        *b_METInfo_METPhi_JET_Pileup_RhoTopology__1down;   //!
   TBranch        *b_EventInfo_jvt_effSF_JET_PunchThrough_MC16__1up;   //!
   TBranch        *b_AnalysisElectrons_el_selected_JET_PunchThrough_MC16__1up;   //!
   TBranch        *b_AnalysisMuons_mu_selected_JET_PunchThrough_MC16__1up;   //!
   TBranch        *b_AnalysisJets_pt_JET_PunchThrough_MC16__1up;   //!
   TBranch        *b_AnalysisJets_jet_selected_JET_PunchThrough_MC16__1up;   //!
   TBranch        *b_AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_JET_PunchThrough_MC16__1up;   //!
   TBranch        *b_METInfo_MET_JET_PunchThrough_MC16__1up;   //!
   TBranch        *b_METInfo_METPhi_JET_PunchThrough_MC16__1up;   //!
   TBranch        *b_EventInfo_jvt_effSF_JET_PunchThrough_MC16__1down;   //!
   TBranch        *b_AnalysisElectrons_el_selected_JET_PunchThrough_MC16__1down;   //!
   TBranch        *b_AnalysisMuons_mu_selected_JET_PunchThrough_MC16__1down;   //!
   TBranch        *b_AnalysisJets_pt_JET_PunchThrough_MC16__1down;   //!
   TBranch        *b_AnalysisJets_jet_selected_JET_PunchThrough_MC16__1down;   //!
   TBranch        *b_AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_JET_PunchThrough_MC16__1down;   //!
   TBranch        *b_METInfo_MET_JET_PunchThrough_MC16__1down;   //!
   TBranch        *b_METInfo_METPhi_JET_PunchThrough_MC16__1down;   //!
   TBranch        *b_METInfo_MET_MET_SoftTrk_ResoPara;   //!
   TBranch        *b_METInfo_METPhi_MET_SoftTrk_ResoPara;   //!
   TBranch        *b_METInfo_MET_MET_SoftTrk_ResoPerp;   //!
   TBranch        *b_METInfo_METPhi_MET_SoftTrk_ResoPerp;   //!
   TBranch        *b_METInfo_MET_MET_SoftTrk_Scale__1down;   //!
   TBranch        *b_METInfo_METPhi_MET_SoftTrk_Scale__1down;   //!
   TBranch        *b_METInfo_MET_MET_SoftTrk_Scale__1up;   //!
   TBranch        *b_METInfo_METPhi_MET_SoftTrk_Scale__1up;   //!
   TBranch        *b_AnalysisElectrons_el_selected_MUON_CB__1down;   //!
   TBranch        *b_AnalysisMuons_pt_MUON_CB__1down;   //!
   TBranch        *b_AnalysisMuons_mu_selected_MUON_CB__1down;   //!
   TBranch        *b_AnalysisMuons_isQuality_Loose_MUON_CB__1down;   //!
   TBranch        *b_AnalysisMuons_isQuality_Medium_MUON_CB__1down;   //!
   TBranch        *b_AnalysisMuons_isQuality_Tight_MUON_CB__1down;   //!
   TBranch        *b_AnalysisMuons_isIsolated_PflowTight_VarRad_MUON_CB__1down;   //!
   TBranch        *b_AnalysisMuons_muon_effSF_MUON_CB__1down;   //!
   TBranch        *b_AnalysisMuons_muon_effSF_TTVA_MUON_CB__1down;   //!
   TBranch        *b_AnalysisMuons_muon_effSF_Quality_Loose_MUON_CB__1down;   //!
   TBranch        *b_AnalysisMuons_muon_effSF_Quality_Medium_MUON_CB__1down;   //!
   TBranch        *b_AnalysisMuons_muon_effSF_Quality_Tight_MUON_CB__1down;   //!
   TBranch        *b_AnalysisMuons_muon_effSF_Isol_PflowTight_VarRad_MUON_CB__1down;   //!
   TBranch        *b_AnalysisMuons_muon_effMC_Trig_Loose_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_CB__1down;   //!
   TBranch        *b_AnalysisMuons_muon_effData_Trig_Loose_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_CB__1down;   //!
   TBranch        *b_AnalysisMuons_muon_effMC_Trig_Loose_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_CB__1down;   //!
   TBranch        *b_AnalysisMuons_muon_effData_Trig_Loose_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_CB__1down;   //!
   TBranch        *b_AnalysisMuons_muon_effMC_Trig_Medium_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_CB__1down;   //!
   TBranch        *b_AnalysisMuons_muon_effData_Trig_Medium_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_CB__1down;   //!
   TBranch        *b_AnalysisMuons_muon_effMC_Trig_Medium_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_CB__1down;   //!
   TBranch        *b_AnalysisMuons_muon_effData_Trig_Medium_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_CB__1down;   //!
   TBranch        *b_AnalysisMuons_muon_effMC_Trig_Tight_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_CB__1down;   //!
   TBranch        *b_AnalysisMuons_muon_effData_Trig_Tight_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_CB__1down;   //!
   TBranch        *b_AnalysisMuons_muon_effMC_Trig_Tight_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_CB__1down;   //!
   TBranch        *b_AnalysisMuons_muon_effData_Trig_Tight_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_CB__1down;   //!
   TBranch        *b_AnalysisJets_jet_selected_MUON_CB__1down;   //!
   TBranch        *b_METInfo_MET_MUON_CB__1down;   //!
   TBranch        *b_METInfo_METPhi_MUON_CB__1down;   //!
   TBranch        *b_AnalysisElectrons_el_selected_MUON_CB__1up;   //!
   TBranch        *b_AnalysisMuons_pt_MUON_CB__1up;   //!
   TBranch        *b_AnalysisMuons_mu_selected_MUON_CB__1up;   //!
   TBranch        *b_AnalysisMuons_isQuality_Loose_MUON_CB__1up;   //!
   TBranch        *b_AnalysisMuons_isQuality_Medium_MUON_CB__1up;   //!
   TBranch        *b_AnalysisMuons_isQuality_Tight_MUON_CB__1up;   //!
   TBranch        *b_AnalysisMuons_isIsolated_PflowTight_VarRad_MUON_CB__1up;   //!
   TBranch        *b_AnalysisMuons_muon_effSF_MUON_CB__1up;   //!
   TBranch        *b_AnalysisMuons_muon_effSF_TTVA_MUON_CB__1up;   //!
   TBranch        *b_AnalysisMuons_muon_effSF_Quality_Loose_MUON_CB__1up;   //!
   TBranch        *b_AnalysisMuons_muon_effSF_Quality_Medium_MUON_CB__1up;   //!
   TBranch        *b_AnalysisMuons_muon_effSF_Quality_Tight_MUON_CB__1up;   //!
   TBranch        *b_AnalysisMuons_muon_effSF_Isol_PflowTight_VarRad_MUON_CB__1up;   //!
   TBranch        *b_AnalysisMuons_muon_effMC_Trig_Loose_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_CB__1up;   //!
   TBranch        *b_AnalysisMuons_muon_effData_Trig_Loose_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_CB__1up;   //!
   TBranch        *b_AnalysisMuons_muon_effMC_Trig_Loose_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_CB__1up;   //!
   TBranch        *b_AnalysisMuons_muon_effData_Trig_Loose_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_CB__1up;   //!
   TBranch        *b_AnalysisMuons_muon_effMC_Trig_Medium_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_CB__1up;   //!
   TBranch        *b_AnalysisMuons_muon_effData_Trig_Medium_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_CB__1up;   //!
   TBranch        *b_AnalysisMuons_muon_effMC_Trig_Medium_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_CB__1up;   //!
   TBranch        *b_AnalysisMuons_muon_effData_Trig_Medium_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_CB__1up;   //!
   TBranch        *b_AnalysisMuons_muon_effMC_Trig_Tight_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_CB__1up;   //!
   TBranch        *b_AnalysisMuons_muon_effData_Trig_Tight_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_CB__1up;   //!
   TBranch        *b_AnalysisMuons_muon_effMC_Trig_Tight_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_CB__1up;   //!
   TBranch        *b_AnalysisMuons_muon_effData_Trig_Tight_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_CB__1up;   //!
   TBranch        *b_AnalysisJets_jet_selected_MUON_CB__1up;   //!
   TBranch        *b_METInfo_MET_MUON_CB__1up;   //!
   TBranch        *b_METInfo_METPhi_MUON_CB__1up;   //!
   TBranch        *b_AnalysisMuons_muon_effSF_Isol_PflowTight_VarRad_MUON_EFF_ISO_STAT__1down;   //!
   TBranch        *b_AnalysisMuons_muon_effSF_Isol_PflowTight_VarRad_MUON_EFF_ISO_STAT__1up;   //!
   TBranch        *b_AnalysisMuons_muon_effSF_Isol_PflowTight_VarRad_MUON_EFF_ISO_SYS__1down;   //!
   TBranch        *b_AnalysisMuons_muon_effSF_Isol_PflowTight_VarRad_MUON_EFF_ISO_SYS__1up;   //!
   TBranch        *b_AnalysisMuons_muon_effSF_MUON_EFF_RECO_STAT__1down;   //!
   TBranch        *b_AnalysisMuons_muon_effSF_Quality_Loose_MUON_EFF_RECO_STAT__1down;   //!
   TBranch        *b_AnalysisMuons_muon_effSF_Quality_Medium_MUON_EFF_RECO_STAT__1down;   //!
   TBranch        *b_AnalysisMuons_muon_effSF_Quality_Tight_MUON_EFF_RECO_STAT__1down;   //!
   TBranch        *b_AnalysisMuons_muon_effSF_MUON_EFF_RECO_STAT__1up;   //!
   TBranch        *b_AnalysisMuons_muon_effSF_Quality_Loose_MUON_EFF_RECO_STAT__1up;   //!
   TBranch        *b_AnalysisMuons_muon_effSF_Quality_Medium_MUON_EFF_RECO_STAT__1up;   //!
   TBranch        *b_AnalysisMuons_muon_effSF_Quality_Tight_MUON_EFF_RECO_STAT__1up;   //!
   TBranch        *b_AnalysisMuons_muon_effSF_MUON_EFF_RECO_STAT_LOWPT__1down;   //!
   TBranch        *b_AnalysisMuons_muon_effSF_Quality_Loose_MUON_EFF_RECO_STAT_LOWPT__1down;   //!
   TBranch        *b_AnalysisMuons_muon_effSF_Quality_Medium_MUON_EFF_RECO_STAT_LOWPT__1down;   //!
   TBranch        *b_AnalysisMuons_muon_effSF_Quality_Tight_MUON_EFF_RECO_STAT_LOWPT__1down;   //!
   TBranch        *b_AnalysisMuons_muon_effSF_MUON_EFF_RECO_STAT_LOWPT__1up;   //!
   TBranch        *b_AnalysisMuons_muon_effSF_Quality_Loose_MUON_EFF_RECO_STAT_LOWPT__1up;   //!
   TBranch        *b_AnalysisMuons_muon_effSF_Quality_Medium_MUON_EFF_RECO_STAT_LOWPT__1up;   //!
   TBranch        *b_AnalysisMuons_muon_effSF_Quality_Tight_MUON_EFF_RECO_STAT_LOWPT__1up;   //!
   TBranch        *b_AnalysisMuons_muon_effSF_MUON_EFF_RECO_SYS__1down;   //!
   TBranch        *b_AnalysisMuons_muon_effSF_Quality_Loose_MUON_EFF_RECO_SYS__1down;   //!
   TBranch        *b_AnalysisMuons_muon_effSF_Quality_Medium_MUON_EFF_RECO_SYS__1down;   //!
   TBranch        *b_AnalysisMuons_muon_effSF_Quality_Tight_MUON_EFF_RECO_SYS__1down;   //!
   TBranch        *b_AnalysisMuons_muon_effSF_MUON_EFF_RECO_SYS__1up;   //!
   TBranch        *b_AnalysisMuons_muon_effSF_Quality_Loose_MUON_EFF_RECO_SYS__1up;   //!
   TBranch        *b_AnalysisMuons_muon_effSF_Quality_Medium_MUON_EFF_RECO_SYS__1up;   //!
   TBranch        *b_AnalysisMuons_muon_effSF_Quality_Tight_MUON_EFF_RECO_SYS__1up;   //!
   TBranch        *b_AnalysisMuons_muon_effSF_MUON_EFF_RECO_SYS_LOWPT__1down;   //!
   TBranch        *b_AnalysisMuons_muon_effSF_Quality_Loose_MUON_EFF_RECO_SYS_LOWPT__1down;   //!
   TBranch        *b_AnalysisMuons_muon_effSF_Quality_Medium_MUON_EFF_RECO_SYS_LOWPT__1down;   //!
   TBranch        *b_AnalysisMuons_muon_effSF_Quality_Tight_MUON_EFF_RECO_SYS_LOWPT__1down;   //!
   TBranch        *b_AnalysisMuons_muon_effSF_MUON_EFF_RECO_SYS_LOWPT__1up;   //!
   TBranch        *b_AnalysisMuons_muon_effSF_Quality_Loose_MUON_EFF_RECO_SYS_LOWPT__1up;   //!
   TBranch        *b_AnalysisMuons_muon_effSF_Quality_Medium_MUON_EFF_RECO_SYS_LOWPT__1up;   //!
   TBranch        *b_AnalysisMuons_muon_effSF_Quality_Tight_MUON_EFF_RECO_SYS_LOWPT__1up;   //!
   TBranch        *b_AnalysisMuons_muon_effSF_TTVA_MUON_EFF_TTVA_STAT__1down;   //!
   TBranch        *b_AnalysisMuons_muon_effSF_TTVA_MUON_EFF_TTVA_STAT__1up;   //!
   TBranch        *b_AnalysisMuons_muon_effSF_TTVA_MUON_EFF_TTVA_SYS__1down;   //!
   TBranch        *b_AnalysisMuons_muon_effSF_TTVA_MUON_EFF_TTVA_SYS__1up;   //!
   TBranch        *b_AnalysisMuons_muon_effMC_Trig_Loose_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_EFF_TrigStatUncertainty__1down;   //!
   TBranch        *b_AnalysisMuons_muon_effData_Trig_Loose_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_EFF_TrigStatUncertainty__1down;   //!
   TBranch        *b_AnalysisMuons_muon_effMC_Trig_Loose_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_EFF_TrigStatUncertainty__1down;   //!
   TBranch        *b_AnalysisMuons_muon_effData_Trig_Loose_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_EFF_TrigStatUncertainty__1down;   //!
   TBranch        *b_AnalysisMuons_muon_effMC_Trig_Medium_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_EFF_TrigStatUncertainty__1down;   //!
   TBranch        *b_AnalysisMuons_muon_effData_Trig_Medium_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_EFF_TrigStatUncertainty__1down;   //!
   TBranch        *b_AnalysisMuons_muon_effMC_Trig_Medium_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_EFF_TrigStatUncertainty__1down;   //!
   TBranch        *b_AnalysisMuons_muon_effData_Trig_Medium_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_EFF_TrigStatUncertainty__1down;   //!
   TBranch        *b_AnalysisMuons_muon_effMC_Trig_Tight_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_EFF_TrigStatUncertainty__1down;   //!
   TBranch        *b_AnalysisMuons_muon_effData_Trig_Tight_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_EFF_TrigStatUncertainty__1down;   //!
   TBranch        *b_AnalysisMuons_muon_effMC_Trig_Tight_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_EFF_TrigStatUncertainty__1down;   //!
   TBranch        *b_AnalysisMuons_muon_effData_Trig_Tight_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_EFF_TrigStatUncertainty__1down;   //!
   TBranch        *b_AnalysisMuons_muon_effMC_Trig_Loose_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_EFF_TrigStatUncertainty__1up;   //!
   TBranch        *b_AnalysisMuons_muon_effData_Trig_Loose_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_EFF_TrigStatUncertainty__1up;   //!
   TBranch        *b_AnalysisMuons_muon_effMC_Trig_Loose_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_EFF_TrigStatUncertainty__1up;   //!
   TBranch        *b_AnalysisMuons_muon_effData_Trig_Loose_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_EFF_TrigStatUncertainty__1up;   //!
   TBranch        *b_AnalysisMuons_muon_effMC_Trig_Medium_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_EFF_TrigStatUncertainty__1up;   //!
   TBranch        *b_AnalysisMuons_muon_effData_Trig_Medium_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_EFF_TrigStatUncertainty__1up;   //!
   TBranch        *b_AnalysisMuons_muon_effMC_Trig_Medium_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_EFF_TrigStatUncertainty__1up;   //!
   TBranch        *b_AnalysisMuons_muon_effData_Trig_Medium_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_EFF_TrigStatUncertainty__1up;   //!
   TBranch        *b_AnalysisMuons_muon_effMC_Trig_Tight_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_EFF_TrigStatUncertainty__1up;   //!
   TBranch        *b_AnalysisMuons_muon_effData_Trig_Tight_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_EFF_TrigStatUncertainty__1up;   //!
   TBranch        *b_AnalysisMuons_muon_effMC_Trig_Tight_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_EFF_TrigStatUncertainty__1up;   //!
   TBranch        *b_AnalysisMuons_muon_effData_Trig_Tight_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_EFF_TrigStatUncertainty__1up;   //!
   TBranch        *b_AnalysisMuons_muon_effMC_Trig_Loose_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_EFF_TrigSystUncertainty__1down;   //!
   TBranch        *b_AnalysisMuons_muon_effData_Trig_Loose_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_EFF_TrigSystUncertainty__1down;   //!
   TBranch        *b_AnalysisMuons_muon_effMC_Trig_Loose_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_EFF_TrigSystUncertainty__1down;   //!
   TBranch        *b_AnalysisMuons_muon_effData_Trig_Loose_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_EFF_TrigSystUncertainty__1down;   //!
   TBranch        *b_AnalysisMuons_muon_effMC_Trig_Medium_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_EFF_TrigSystUncertainty__1down;   //!
   TBranch        *b_AnalysisMuons_muon_effData_Trig_Medium_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_EFF_TrigSystUncertainty__1down;   //!
   TBranch        *b_AnalysisMuons_muon_effMC_Trig_Medium_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_EFF_TrigSystUncertainty__1down;   //!
   TBranch        *b_AnalysisMuons_muon_effData_Trig_Medium_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_EFF_TrigSystUncertainty__1down;   //!
   TBranch        *b_AnalysisMuons_muon_effMC_Trig_Tight_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_EFF_TrigSystUncertainty__1down;   //!
   TBranch        *b_AnalysisMuons_muon_effData_Trig_Tight_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_EFF_TrigSystUncertainty__1down;   //!
   TBranch        *b_AnalysisMuons_muon_effMC_Trig_Tight_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_EFF_TrigSystUncertainty__1down;   //!
   TBranch        *b_AnalysisMuons_muon_effData_Trig_Tight_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_EFF_TrigSystUncertainty__1down;   //!
   TBranch        *b_AnalysisMuons_muon_effMC_Trig_Loose_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_EFF_TrigSystUncertainty__1up;   //!
   TBranch        *b_AnalysisMuons_muon_effData_Trig_Loose_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_EFF_TrigSystUncertainty__1up;   //!
   TBranch        *b_AnalysisMuons_muon_effMC_Trig_Loose_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_EFF_TrigSystUncertainty__1up;   //!
   TBranch        *b_AnalysisMuons_muon_effData_Trig_Loose_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_EFF_TrigSystUncertainty__1up;   //!
   TBranch        *b_AnalysisMuons_muon_effMC_Trig_Medium_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_EFF_TrigSystUncertainty__1up;   //!
   TBranch        *b_AnalysisMuons_muon_effData_Trig_Medium_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_EFF_TrigSystUncertainty__1up;   //!
   TBranch        *b_AnalysisMuons_muon_effMC_Trig_Medium_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_EFF_TrigSystUncertainty__1up;   //!
   TBranch        *b_AnalysisMuons_muon_effData_Trig_Medium_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_EFF_TrigSystUncertainty__1up;   //!
   TBranch        *b_AnalysisMuons_muon_effMC_Trig_Tight_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_EFF_TrigSystUncertainty__1up;   //!
   TBranch        *b_AnalysisMuons_muon_effData_Trig_Tight_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_EFF_TrigSystUncertainty__1up;   //!
   TBranch        *b_AnalysisMuons_muon_effMC_Trig_Tight_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_EFF_TrigSystUncertainty__1up;   //!
   TBranch        *b_AnalysisMuons_muon_effData_Trig_Tight_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_EFF_TrigSystUncertainty__1up;   //!
   TBranch        *b_AnalysisElectrons_el_selected_MUON_SAGITTA_DATASTAT__1down;   //!
   TBranch        *b_AnalysisMuons_pt_MUON_SAGITTA_DATASTAT__1down;   //!
   TBranch        *b_AnalysisMuons_mu_selected_MUON_SAGITTA_DATASTAT__1down;   //!
   TBranch        *b_AnalysisMuons_isQuality_Loose_MUON_SAGITTA_DATASTAT__1down;   //!
   TBranch        *b_AnalysisMuons_isQuality_Medium_MUON_SAGITTA_DATASTAT__1down;   //!
   TBranch        *b_AnalysisMuons_isQuality_Tight_MUON_SAGITTA_DATASTAT__1down;   //!
   TBranch        *b_AnalysisMuons_isIsolated_PflowTight_VarRad_MUON_SAGITTA_DATASTAT__1down;   //!
   TBranch        *b_AnalysisMuons_muon_effSF_MUON_SAGITTA_DATASTAT__1down;   //!
   TBranch        *b_AnalysisMuons_muon_effSF_TTVA_MUON_SAGITTA_DATASTAT__1down;   //!
   TBranch        *b_AnalysisMuons_muon_effSF_Quality_Loose_MUON_SAGITTA_DATASTAT__1down;   //!
   TBranch        *b_AnalysisMuons_muon_effSF_Quality_Medium_MUON_SAGITTA_DATASTAT__1down;   //!
   TBranch        *b_AnalysisMuons_muon_effSF_Quality_Tight_MUON_SAGITTA_DATASTAT__1down;   //!
   TBranch        *b_AnalysisMuons_muon_effSF_Isol_PflowTight_VarRad_MUON_SAGITTA_DATASTAT__1down;   //!
   TBranch        *b_AnalysisMuons_muon_effMC_Trig_Loose_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_SAGITTA_DATASTAT__1down;   //!
   TBranch        *b_AnalysisMuons_muon_effData_Trig_Loose_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_SAGITTA_DATASTAT__1down;   //!
   TBranch        *b_AnalysisMuons_muon_effMC_Trig_Loose_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_SAGITTA_DATASTAT__1down;   //!
   TBranch        *b_AnalysisMuons_muon_effData_Trig_Loose_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_SAGITTA_DATASTAT__1down;   //!
   TBranch        *b_AnalysisMuons_muon_effMC_Trig_Medium_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_SAGITTA_DATASTAT__1down;   //!
   TBranch        *b_AnalysisMuons_muon_effData_Trig_Medium_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_SAGITTA_DATASTAT__1down;   //!
   TBranch        *b_AnalysisMuons_muon_effMC_Trig_Medium_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_SAGITTA_DATASTAT__1down;   //!
   TBranch        *b_AnalysisMuons_muon_effData_Trig_Medium_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_SAGITTA_DATASTAT__1down;   //!
   TBranch        *b_AnalysisMuons_muon_effMC_Trig_Tight_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_SAGITTA_DATASTAT__1down;   //!
   TBranch        *b_AnalysisMuons_muon_effData_Trig_Tight_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_SAGITTA_DATASTAT__1down;   //!
   TBranch        *b_AnalysisMuons_muon_effMC_Trig_Tight_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_SAGITTA_DATASTAT__1down;   //!
   TBranch        *b_AnalysisMuons_muon_effData_Trig_Tight_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_SAGITTA_DATASTAT__1down;   //!
   TBranch        *b_AnalysisJets_jet_selected_MUON_SAGITTA_DATASTAT__1down;   //!
   TBranch        *b_METInfo_MET_MUON_SAGITTA_DATASTAT__1down;   //!
   TBranch        *b_METInfo_METPhi_MUON_SAGITTA_DATASTAT__1down;   //!
   TBranch        *b_AnalysisElectrons_el_selected_MUON_SAGITTA_DATASTAT__1up;   //!
   TBranch        *b_AnalysisMuons_pt_MUON_SAGITTA_DATASTAT__1up;   //!
   TBranch        *b_AnalysisMuons_mu_selected_MUON_SAGITTA_DATASTAT__1up;   //!
   TBranch        *b_AnalysisMuons_isQuality_Loose_MUON_SAGITTA_DATASTAT__1up;   //!
   TBranch        *b_AnalysisMuons_isQuality_Medium_MUON_SAGITTA_DATASTAT__1up;   //!
   TBranch        *b_AnalysisMuons_isQuality_Tight_MUON_SAGITTA_DATASTAT__1up;   //!
   TBranch        *b_AnalysisMuons_isIsolated_PflowTight_VarRad_MUON_SAGITTA_DATASTAT__1up;   //!
   TBranch        *b_AnalysisMuons_muon_effSF_MUON_SAGITTA_DATASTAT__1up;   //!
   TBranch        *b_AnalysisMuons_muon_effSF_TTVA_MUON_SAGITTA_DATASTAT__1up;   //!
   TBranch        *b_AnalysisMuons_muon_effSF_Quality_Loose_MUON_SAGITTA_DATASTAT__1up;   //!
   TBranch        *b_AnalysisMuons_muon_effSF_Quality_Medium_MUON_SAGITTA_DATASTAT__1up;   //!
   TBranch        *b_AnalysisMuons_muon_effSF_Quality_Tight_MUON_SAGITTA_DATASTAT__1up;   //!
   TBranch        *b_AnalysisMuons_muon_effSF_Isol_PflowTight_VarRad_MUON_SAGITTA_DATASTAT__1up;   //!
   TBranch        *b_AnalysisMuons_muon_effMC_Trig_Loose_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_SAGITTA_DATASTAT__1up;   //!
   TBranch        *b_AnalysisMuons_muon_effData_Trig_Loose_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_SAGITTA_DATASTAT__1up;   //!
   TBranch        *b_AnalysisMuons_muon_effMC_Trig_Loose_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_SAGITTA_DATASTAT__1up;   //!
   TBranch        *b_AnalysisMuons_muon_effData_Trig_Loose_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_SAGITTA_DATASTAT__1up;   //!
   TBranch        *b_AnalysisMuons_muon_effMC_Trig_Medium_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_SAGITTA_DATASTAT__1up;   //!
   TBranch        *b_AnalysisMuons_muon_effData_Trig_Medium_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_SAGITTA_DATASTAT__1up;   //!
   TBranch        *b_AnalysisMuons_muon_effMC_Trig_Medium_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_SAGITTA_DATASTAT__1up;   //!
   TBranch        *b_AnalysisMuons_muon_effData_Trig_Medium_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_SAGITTA_DATASTAT__1up;   //!
   TBranch        *b_AnalysisMuons_muon_effMC_Trig_Tight_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_SAGITTA_DATASTAT__1up;   //!
   TBranch        *b_AnalysisMuons_muon_effData_Trig_Tight_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_SAGITTA_DATASTAT__1up;   //!
   TBranch        *b_AnalysisMuons_muon_effMC_Trig_Tight_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_SAGITTA_DATASTAT__1up;   //!
   TBranch        *b_AnalysisMuons_muon_effData_Trig_Tight_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_SAGITTA_DATASTAT__1up;   //!
   TBranch        *b_AnalysisJets_jet_selected_MUON_SAGITTA_DATASTAT__1up;   //!
   TBranch        *b_METInfo_MET_MUON_SAGITTA_DATASTAT__1up;   //!
   TBranch        *b_METInfo_METPhi_MUON_SAGITTA_DATASTAT__1up;   //!
   TBranch        *b_AnalysisElectrons_el_selected_MUON_SAGITTA_RESBIAS__1down;   //!
   TBranch        *b_AnalysisMuons_pt_MUON_SAGITTA_RESBIAS__1down;   //!
   TBranch        *b_AnalysisMuons_mu_selected_MUON_SAGITTA_RESBIAS__1down;   //!
   TBranch        *b_AnalysisMuons_isQuality_Loose_MUON_SAGITTA_RESBIAS__1down;   //!
   TBranch        *b_AnalysisMuons_isQuality_Medium_MUON_SAGITTA_RESBIAS__1down;   //!
   TBranch        *b_AnalysisMuons_isQuality_Tight_MUON_SAGITTA_RESBIAS__1down;   //!
   TBranch        *b_AnalysisMuons_isIsolated_PflowTight_VarRad_MUON_SAGITTA_RESBIAS__1down;   //!
   TBranch        *b_AnalysisMuons_muon_effSF_MUON_SAGITTA_RESBIAS__1down;   //!
   TBranch        *b_AnalysisMuons_muon_effSF_TTVA_MUON_SAGITTA_RESBIAS__1down;   //!
   TBranch        *b_AnalysisMuons_muon_effSF_Quality_Loose_MUON_SAGITTA_RESBIAS__1down;   //!
   TBranch        *b_AnalysisMuons_muon_effSF_Quality_Medium_MUON_SAGITTA_RESBIAS__1down;   //!
   TBranch        *b_AnalysisMuons_muon_effSF_Quality_Tight_MUON_SAGITTA_RESBIAS__1down;   //!
   TBranch        *b_AnalysisMuons_muon_effSF_Isol_PflowTight_VarRad_MUON_SAGITTA_RESBIAS__1down;   //!
   TBranch        *b_AnalysisMuons_muon_effMC_Trig_Loose_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_SAGITTA_RESBIAS__1down;   //!
   TBranch        *b_AnalysisMuons_muon_effData_Trig_Loose_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_SAGITTA_RESBIAS__1down;   //!
   TBranch        *b_AnalysisMuons_muon_effMC_Trig_Loose_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_SAGITTA_RESBIAS__1down;   //!
   TBranch        *b_AnalysisMuons_muon_effData_Trig_Loose_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_SAGITTA_RESBIAS__1down;   //!
   TBranch        *b_AnalysisMuons_muon_effMC_Trig_Medium_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_SAGITTA_RESBIAS__1down;   //!
   TBranch        *b_AnalysisMuons_muon_effData_Trig_Medium_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_SAGITTA_RESBIAS__1down;   //!
   TBranch        *b_AnalysisMuons_muon_effMC_Trig_Medium_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_SAGITTA_RESBIAS__1down;   //!
   TBranch        *b_AnalysisMuons_muon_effData_Trig_Medium_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_SAGITTA_RESBIAS__1down;   //!
   TBranch        *b_AnalysisMuons_muon_effMC_Trig_Tight_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_SAGITTA_RESBIAS__1down;   //!
   TBranch        *b_AnalysisMuons_muon_effData_Trig_Tight_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_SAGITTA_RESBIAS__1down;   //!
   TBranch        *b_AnalysisMuons_muon_effMC_Trig_Tight_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_SAGITTA_RESBIAS__1down;   //!
   TBranch        *b_AnalysisMuons_muon_effData_Trig_Tight_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_SAGITTA_RESBIAS__1down;   //!
   TBranch        *b_AnalysisJets_jet_selected_MUON_SAGITTA_RESBIAS__1down;   //!
   TBranch        *b_METInfo_MET_MUON_SAGITTA_RESBIAS__1down;   //!
   TBranch        *b_METInfo_METPhi_MUON_SAGITTA_RESBIAS__1down;   //!
   TBranch        *b_AnalysisElectrons_el_selected_MUON_SAGITTA_RESBIAS__1up;   //!
   TBranch        *b_AnalysisMuons_pt_MUON_SAGITTA_RESBIAS__1up;   //!
   TBranch        *b_AnalysisMuons_mu_selected_MUON_SAGITTA_RESBIAS__1up;   //!
   TBranch        *b_AnalysisMuons_isQuality_Loose_MUON_SAGITTA_RESBIAS__1up;   //!
   TBranch        *b_AnalysisMuons_isQuality_Medium_MUON_SAGITTA_RESBIAS__1up;   //!
   TBranch        *b_AnalysisMuons_isQuality_Tight_MUON_SAGITTA_RESBIAS__1up;   //!
   TBranch        *b_AnalysisMuons_isIsolated_PflowTight_VarRad_MUON_SAGITTA_RESBIAS__1up;   //!
   TBranch        *b_AnalysisMuons_muon_effSF_MUON_SAGITTA_RESBIAS__1up;   //!
   TBranch        *b_AnalysisMuons_muon_effSF_TTVA_MUON_SAGITTA_RESBIAS__1up;   //!
   TBranch        *b_AnalysisMuons_muon_effSF_Quality_Loose_MUON_SAGITTA_RESBIAS__1up;   //!
   TBranch        *b_AnalysisMuons_muon_effSF_Quality_Medium_MUON_SAGITTA_RESBIAS__1up;   //!
   TBranch        *b_AnalysisMuons_muon_effSF_Quality_Tight_MUON_SAGITTA_RESBIAS__1up;   //!
   TBranch        *b_AnalysisMuons_muon_effSF_Isol_PflowTight_VarRad_MUON_SAGITTA_RESBIAS__1up;   //!
   TBranch        *b_AnalysisMuons_muon_effMC_Trig_Loose_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_SAGITTA_RESBIAS__1up;   //!
   TBranch        *b_AnalysisMuons_muon_effData_Trig_Loose_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_SAGITTA_RESBIAS__1up;   //!
   TBranch        *b_AnalysisMuons_muon_effMC_Trig_Loose_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_SAGITTA_RESBIAS__1up;   //!
   TBranch        *b_AnalysisMuons_muon_effData_Trig_Loose_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_SAGITTA_RESBIAS__1up;   //!
   TBranch        *b_AnalysisMuons_muon_effMC_Trig_Medium_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_SAGITTA_RESBIAS__1up;   //!
   TBranch        *b_AnalysisMuons_muon_effData_Trig_Medium_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_SAGITTA_RESBIAS__1up;   //!
   TBranch        *b_AnalysisMuons_muon_effMC_Trig_Medium_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_SAGITTA_RESBIAS__1up;   //!
   TBranch        *b_AnalysisMuons_muon_effData_Trig_Medium_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_SAGITTA_RESBIAS__1up;   //!
   TBranch        *b_AnalysisMuons_muon_effMC_Trig_Tight_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_SAGITTA_RESBIAS__1up;   //!
   TBranch        *b_AnalysisMuons_muon_effData_Trig_Tight_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_SAGITTA_RESBIAS__1up;   //!
   TBranch        *b_AnalysisMuons_muon_effMC_Trig_Tight_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_SAGITTA_RESBIAS__1up;   //!
   TBranch        *b_AnalysisMuons_muon_effData_Trig_Tight_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_SAGITTA_RESBIAS__1up;   //!
   TBranch        *b_AnalysisJets_jet_selected_MUON_SAGITTA_RESBIAS__1up;   //!
   TBranch        *b_METInfo_MET_MUON_SAGITTA_RESBIAS__1up;   //!
   TBranch        *b_METInfo_METPhi_MUON_SAGITTA_RESBIAS__1up;   //!
   TBranch        *b_AnalysisElectrons_el_selected_MUON_SCALE__1down;   //!
   TBranch        *b_AnalysisMuons_pt_MUON_SCALE__1down;   //!
   TBranch        *b_AnalysisMuons_mu_selected_MUON_SCALE__1down;   //!
   TBranch        *b_AnalysisMuons_isQuality_Loose_MUON_SCALE__1down;   //!
   TBranch        *b_AnalysisMuons_isQuality_Medium_MUON_SCALE__1down;   //!
   TBranch        *b_AnalysisMuons_isQuality_Tight_MUON_SCALE__1down;   //!
   TBranch        *b_AnalysisMuons_isIsolated_PflowTight_VarRad_MUON_SCALE__1down;   //!
   TBranch        *b_AnalysisMuons_muon_effSF_MUON_SCALE__1down;   //!
   TBranch        *b_AnalysisMuons_muon_effSF_TTVA_MUON_SCALE__1down;   //!
   TBranch        *b_AnalysisMuons_muon_effSF_Quality_Loose_MUON_SCALE__1down;   //!
   TBranch        *b_AnalysisMuons_muon_effSF_Quality_Medium_MUON_SCALE__1down;   //!
   TBranch        *b_AnalysisMuons_muon_effSF_Quality_Tight_MUON_SCALE__1down;   //!
   TBranch        *b_AnalysisMuons_muon_effSF_Isol_PflowTight_VarRad_MUON_SCALE__1down;   //!
   TBranch        *b_AnalysisMuons_muon_effMC_Trig_Loose_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_SCALE__1down;   //!
   TBranch        *b_AnalysisMuons_muon_effData_Trig_Loose_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_SCALE__1down;   //!
   TBranch        *b_AnalysisMuons_muon_effMC_Trig_Loose_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_SCALE__1down;   //!
   TBranch        *b_AnalysisMuons_muon_effData_Trig_Loose_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_SCALE__1down;   //!
   TBranch        *b_AnalysisMuons_muon_effMC_Trig_Medium_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_SCALE__1down;   //!
   TBranch        *b_AnalysisMuons_muon_effData_Trig_Medium_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_SCALE__1down;   //!
   TBranch        *b_AnalysisMuons_muon_effMC_Trig_Medium_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_SCALE__1down;   //!
   TBranch        *b_AnalysisMuons_muon_effData_Trig_Medium_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_SCALE__1down;   //!
   TBranch        *b_AnalysisMuons_muon_effMC_Trig_Tight_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_SCALE__1down;   //!
   TBranch        *b_AnalysisMuons_muon_effData_Trig_Tight_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_SCALE__1down;   //!
   TBranch        *b_AnalysisMuons_muon_effMC_Trig_Tight_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_SCALE__1down;   //!
   TBranch        *b_AnalysisMuons_muon_effData_Trig_Tight_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_SCALE__1down;   //!
   TBranch        *b_AnalysisJets_jet_selected_MUON_SCALE__1down;   //!
   TBranch        *b_METInfo_MET_MUON_SCALE__1down;   //!
   TBranch        *b_METInfo_METPhi_MUON_SCALE__1down;   //!
   TBranch        *b_AnalysisElectrons_el_selected_MUON_SCALE__1up;   //!
   TBranch        *b_AnalysisMuons_pt_MUON_SCALE__1up;   //!
   TBranch        *b_AnalysisMuons_mu_selected_MUON_SCALE__1up;   //!
   TBranch        *b_AnalysisMuons_isQuality_Loose_MUON_SCALE__1up;   //!
   TBranch        *b_AnalysisMuons_isQuality_Medium_MUON_SCALE__1up;   //!
   TBranch        *b_AnalysisMuons_isQuality_Tight_MUON_SCALE__1up;   //!
   TBranch        *b_AnalysisMuons_isIsolated_PflowTight_VarRad_MUON_SCALE__1up;   //!
   TBranch        *b_AnalysisMuons_muon_effSF_MUON_SCALE__1up;   //!
   TBranch        *b_AnalysisMuons_muon_effSF_TTVA_MUON_SCALE__1up;   //!
   TBranch        *b_AnalysisMuons_muon_effSF_Quality_Loose_MUON_SCALE__1up;   //!
   TBranch        *b_AnalysisMuons_muon_effSF_Quality_Medium_MUON_SCALE__1up;   //!
   TBranch        *b_AnalysisMuons_muon_effSF_Quality_Tight_MUON_SCALE__1up;   //!
   TBranch        *b_AnalysisMuons_muon_effSF_Isol_PflowTight_VarRad_MUON_SCALE__1up;   //!
   TBranch        *b_AnalysisMuons_muon_effMC_Trig_Loose_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_SCALE__1up;   //!
   TBranch        *b_AnalysisMuons_muon_effData_Trig_Loose_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_SCALE__1up;   //!
   TBranch        *b_AnalysisMuons_muon_effMC_Trig_Loose_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_SCALE__1up;   //!
   TBranch        *b_AnalysisMuons_muon_effData_Trig_Loose_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_SCALE__1up;   //!
   TBranch        *b_AnalysisMuons_muon_effMC_Trig_Medium_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_SCALE__1up;   //!
   TBranch        *b_AnalysisMuons_muon_effData_Trig_Medium_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_SCALE__1up;   //!
   TBranch        *b_AnalysisMuons_muon_effMC_Trig_Medium_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_SCALE__1up;   //!
   TBranch        *b_AnalysisMuons_muon_effData_Trig_Medium_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_SCALE__1up;   //!
   TBranch        *b_AnalysisMuons_muon_effMC_Trig_Tight_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_SCALE__1up;   //!
   TBranch        *b_AnalysisMuons_muon_effData_Trig_Tight_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_SCALE__1up;   //!
   TBranch        *b_AnalysisMuons_muon_effMC_Trig_Tight_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_SCALE__1up;   //!
   TBranch        *b_AnalysisMuons_muon_effData_Trig_Tight_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_SCALE__1up;   //!
   TBranch        *b_AnalysisJets_jet_selected_MUON_SCALE__1up;   //!
   TBranch        *b_METInfo_MET_MUON_SCALE__1up;   //!
   TBranch        *b_METInfo_METPhi_MUON_SCALE__1up;   //!
   TBranch        *b_EventInfo_prodFracWeight_PROD_FRAC_BOTTOM_EIG_1;   //!
   TBranch        *b_EventInfo_prodFracWeight_PROD_FRAC_BOTTOM_EIG_2;   //!
   TBranch        *b_EventInfo_prodFracWeight_PROD_FRAC_CHARM_EIG_1;   //!
   TBranch        *b_EventInfo_prodFracWeight_PROD_FRAC_CHARM_EIG_2;   //!
   TBranch        *b_EventInfo_prodFracWeight_PROD_FRAC_CHARM_EIG_3;   //!
   TBranch        *b_EventInfo_PileupWeight_PRW_DATASF__1down;   //!
   TBranch        *b_EventInfo_PileupWeight_PRW_DATASF__1up;   //!
   TBranch        *b_DMesons_costhetastar_TRK_BIAS_D0_WM;   //!
   TBranch        *b_DMesons_D0Index_TRK_BIAS_D0_WM;   //!
   TBranch        *b_DMesons_daughterInfo__eta_TRK_BIAS_D0_WM;   //!
   TBranch        *b_DMesons_daughterInfo__passTight_TRK_BIAS_D0_WM;   //!
   TBranch        *b_DMesons_daughterInfo__pdgId_TRK_BIAS_D0_WM;   //!
   TBranch        *b_DMesons_daughterInfo__phi_TRK_BIAS_D0_WM;   //!
   TBranch        *b_DMesons_daughterInfo__pt_TRK_BIAS_D0_WM;   //!
   TBranch        *b_DMesons_daughterInfo__trackId_TRK_BIAS_D0_WM;   //!
   TBranch        *b_DMesons_daughterInfo__truthBarcode_TRK_BIAS_D0_WM;   //!
   TBranch        *b_DMesons_daughterInfo__truthDBarcode_TRK_BIAS_D0_WM;   //!
   TBranch        *b_DMesons_daughterInfo__z0SinTheta_TRK_BIAS_D0_WM;   //!
   TBranch        *b_DMesons_daughterInfo__z0SinThetaPV_TRK_BIAS_D0_WM;   //!
   TBranch        *b_DMesons_decayType_TRK_BIAS_D0_WM;   //!
   TBranch        *b_DMesons_DeltaMass_TRK_BIAS_D0_WM;   //!
   TBranch        *b_DMesons_eta_TRK_BIAS_D0_WM;   //!
   TBranch        *b_DMesons_fitOutput__Charge_TRK_BIAS_D0_WM;   //!
   TBranch        *b_DMesons_fitOutput__Chi2_TRK_BIAS_D0_WM;   //!
   TBranch        *b_DMesons_fitOutput__Impact_TRK_BIAS_D0_WM;   //!
   TBranch        *b_DMesons_fitOutput__ImpactSignificance_TRK_BIAS_D0_WM;   //!
   TBranch        *b_DMesons_fitOutput__ImpactZ0SinTheta_TRK_BIAS_D0_WM;   //!
   TBranch        *b_DMesons_fitOutput__Lxy_TRK_BIAS_D0_WM;   //!
   TBranch        *b_DMesons_fitOutput__VertexPosition_TRK_BIAS_D0_WM;   //!
   TBranch        *b_DMesons_m_TRK_BIAS_D0_WM;   //!
   TBranch        *b_DMesons_mKpi1_TRK_BIAS_D0_WM;   //!
   TBranch        *b_DMesons_mKpi2_TRK_BIAS_D0_WM;   //!
   TBranch        *b_DMesons_mPhi1_TRK_BIAS_D0_WM;   //!
   TBranch        *b_DMesons_mPhi2_TRK_BIAS_D0_WM;   //!
   TBranch        *b_DMesons_pdgId_TRK_BIAS_D0_WM;   //!
   TBranch        *b_DMesons_phi_TRK_BIAS_D0_WM;   //!
   TBranch        *b_DMesons_pt_TRK_BIAS_D0_WM;   //!
   TBranch        *b_DMesons_ptcone40_TRK_BIAS_D0_WM;   //!
   TBranch        *b_DMesons_SlowPionD0_TRK_BIAS_D0_WM;   //!
   TBranch        *b_DMesons_SlowPionZ0SinTheta_TRK_BIAS_D0_WM;   //!
   TBranch        *b_DMesons_truthBarcode_TRK_BIAS_D0_WM;   //!
   TBranch        *b_MesonTracks_d0sig_TRK_BIAS_D0_WM;   //!
   TBranch        *b_MesonTracks_d0sigPV_TRK_BIAS_D0_WM;   //!
   TBranch        *b_MesonTracks_eta_TRK_BIAS_D0_WM;   //!
   TBranch        *b_MesonTracks_passTight_TRK_BIAS_D0_WM;   //!
   TBranch        *b_MesonTracks_phi_TRK_BIAS_D0_WM;   //!
   TBranch        *b_MesonTracks_pt_TRK_BIAS_D0_WM;   //!
   TBranch        *b_MesonTracks_trackId_TRK_BIAS_D0_WM;   //!
   TBranch        *b_MesonTracks_z0sinTheta_TRK_BIAS_D0_WM;   //!
   TBranch        *b_MesonTracks_truthMatchProbability_TRK_BIAS_D0_WM;   //!
   TBranch        *b_AntiKt6PV0TrackJets_pt_TRK_BIAS_D0_WM;   //!
   TBranch        *b_AntiKt6PV0TrackJets_eta_TRK_BIAS_D0_WM;   //!
   TBranch        *b_AntiKt6PV0TrackJets_phi_TRK_BIAS_D0_WM;   //!
   TBranch        *b_AntiKt6PV0TrackJets_m_TRK_BIAS_D0_WM;   //!
   TBranch        *b_AntiKt6PV0TrackJets_daughter__pt_TRK_BIAS_D0_WM;   //!
   TBranch        *b_AntiKt6PV0TrackJets_daughter__eta_TRK_BIAS_D0_WM;   //!
   TBranch        *b_AntiKt6PV0TrackJets_daughter__phi_TRK_BIAS_D0_WM;   //!
   TBranch        *b_AntiKt6PV0TrackJets_daughter__trackId_TRK_BIAS_D0_WM;   //!
   TBranch        *b_AntiKt8PV0TrackJets_pt_TRK_BIAS_D0_WM;   //!
   TBranch        *b_AntiKt8PV0TrackJets_eta_TRK_BIAS_D0_WM;   //!
   TBranch        *b_AntiKt8PV0TrackJets_phi_TRK_BIAS_D0_WM;   //!
   TBranch        *b_AntiKt8PV0TrackJets_m_TRK_BIAS_D0_WM;   //!
   TBranch        *b_AntiKt8PV0TrackJets_daughter__pt_TRK_BIAS_D0_WM;   //!
   TBranch        *b_AntiKt8PV0TrackJets_daughter__eta_TRK_BIAS_D0_WM;   //!
   TBranch        *b_AntiKt8PV0TrackJets_daughter__phi_TRK_BIAS_D0_WM;   //!
   TBranch        *b_AntiKt8PV0TrackJets_daughter__trackId_TRK_BIAS_D0_WM;   //!
   TBranch        *b_AntiKt10PV0TrackJets_pt_TRK_BIAS_D0_WM;   //!
   TBranch        *b_AntiKt10PV0TrackJets_eta_TRK_BIAS_D0_WM;   //!
   TBranch        *b_AntiKt10PV0TrackJets_phi_TRK_BIAS_D0_WM;   //!
   TBranch        *b_AntiKt10PV0TrackJets_m_TRK_BIAS_D0_WM;   //!
   TBranch        *b_AntiKt10PV0TrackJets_daughter__pt_TRK_BIAS_D0_WM;   //!
   TBranch        *b_AntiKt10PV0TrackJets_daughter__eta_TRK_BIAS_D0_WM;   //!
   TBranch        *b_AntiKt10PV0TrackJets_daughter__phi_TRK_BIAS_D0_WM;   //!
   TBranch        *b_AntiKt10PV0TrackJets_daughter__trackId_TRK_BIAS_D0_WM;   //!
   TBranch        *b_AntiKtVR30Rmax4Rmin02TrackJets_pt_TRK_BIAS_D0_WM;   //!
   TBranch        *b_AntiKtVR30Rmax4Rmin02TrackJets_eta_TRK_BIAS_D0_WM;   //!
   TBranch        *b_AntiKtVR30Rmax4Rmin02TrackJets_phi_TRK_BIAS_D0_WM;   //!
   TBranch        *b_AntiKtVR30Rmax4Rmin02TrackJets_m_TRK_BIAS_D0_WM;   //!
   TBranch        *b_AntiKtVR30Rmax4Rmin02TrackJets_daughter__pt_TRK_BIAS_D0_WM;   //!
   TBranch        *b_AntiKtVR30Rmax4Rmin02TrackJets_daughter__eta_TRK_BIAS_D0_WM;   //!
   TBranch        *b_AntiKtVR30Rmax4Rmin02TrackJets_daughter__phi_TRK_BIAS_D0_WM;   //!
   TBranch        *b_AntiKtVR30Rmax4Rmin02TrackJets_daughter__trackId_TRK_BIAS_D0_WM;   //!
   TBranch        *b_DMesons_costhetastar_TRK_BIAS_QOVERP_SAGITTA_WM;   //!
   TBranch        *b_DMesons_D0Index_TRK_BIAS_QOVERP_SAGITTA_WM;   //!
   TBranch        *b_DMesons_daughterInfo__eta_TRK_BIAS_QOVERP_SAGITTA_WM;   //!
   TBranch        *b_DMesons_daughterInfo__passTight_TRK_BIAS_QOVERP_SAGITTA_WM;   //!
   TBranch        *b_DMesons_daughterInfo__pdgId_TRK_BIAS_QOVERP_SAGITTA_WM;   //!
   TBranch        *b_DMesons_daughterInfo__phi_TRK_BIAS_QOVERP_SAGITTA_WM;   //!
   TBranch        *b_DMesons_daughterInfo__pt_TRK_BIAS_QOVERP_SAGITTA_WM;   //!
   TBranch        *b_DMesons_daughterInfo__trackId_TRK_BIAS_QOVERP_SAGITTA_WM;   //!
   TBranch        *b_DMesons_daughterInfo__truthBarcode_TRK_BIAS_QOVERP_SAGITTA_WM;   //!
   TBranch        *b_DMesons_daughterInfo__truthDBarcode_TRK_BIAS_QOVERP_SAGITTA_WM;   //!
   TBranch        *b_DMesons_daughterInfo__z0SinTheta_TRK_BIAS_QOVERP_SAGITTA_WM;   //!
   TBranch        *b_DMesons_daughterInfo__z0SinThetaPV_TRK_BIAS_QOVERP_SAGITTA_WM;   //!
   TBranch        *b_DMesons_decayType_TRK_BIAS_QOVERP_SAGITTA_WM;   //!
   TBranch        *b_DMesons_DeltaMass_TRK_BIAS_QOVERP_SAGITTA_WM;   //!
   TBranch        *b_DMesons_eta_TRK_BIAS_QOVERP_SAGITTA_WM;   //!
   TBranch        *b_DMesons_fitOutput__Charge_TRK_BIAS_QOVERP_SAGITTA_WM;   //!
   TBranch        *b_DMesons_fitOutput__Chi2_TRK_BIAS_QOVERP_SAGITTA_WM;   //!
   TBranch        *b_DMesons_fitOutput__Impact_TRK_BIAS_QOVERP_SAGITTA_WM;   //!
   TBranch        *b_DMesons_fitOutput__ImpactSignificance_TRK_BIAS_QOVERP_SAGITTA_WM;   //!
   TBranch        *b_DMesons_fitOutput__ImpactZ0SinTheta_TRK_BIAS_QOVERP_SAGITTA_WM;   //!
   TBranch        *b_DMesons_fitOutput__Lxy_TRK_BIAS_QOVERP_SAGITTA_WM;   //!
   TBranch        *b_DMesons_fitOutput__VertexPosition_TRK_BIAS_QOVERP_SAGITTA_WM;   //!
   TBranch        *b_DMesons_m_TRK_BIAS_QOVERP_SAGITTA_WM;   //!
   TBranch        *b_DMesons_mKpi1_TRK_BIAS_QOVERP_SAGITTA_WM;   //!
   TBranch        *b_DMesons_mKpi2_TRK_BIAS_QOVERP_SAGITTA_WM;   //!
   TBranch        *b_DMesons_mPhi1_TRK_BIAS_QOVERP_SAGITTA_WM;   //!
   TBranch        *b_DMesons_mPhi2_TRK_BIAS_QOVERP_SAGITTA_WM;   //!
   TBranch        *b_DMesons_pdgId_TRK_BIAS_QOVERP_SAGITTA_WM;   //!
   TBranch        *b_DMesons_phi_TRK_BIAS_QOVERP_SAGITTA_WM;   //!
   TBranch        *b_DMesons_pt_TRK_BIAS_QOVERP_SAGITTA_WM;   //!
   TBranch        *b_DMesons_ptcone40_TRK_BIAS_QOVERP_SAGITTA_WM;   //!
   TBranch        *b_DMesons_SlowPionD0_TRK_BIAS_QOVERP_SAGITTA_WM;   //!
   TBranch        *b_DMesons_SlowPionZ0SinTheta_TRK_BIAS_QOVERP_SAGITTA_WM;   //!
   TBranch        *b_DMesons_truthBarcode_TRK_BIAS_QOVERP_SAGITTA_WM;   //!
   TBranch        *b_MesonTracks_d0sig_TRK_BIAS_QOVERP_SAGITTA_WM;   //!
   TBranch        *b_MesonTracks_d0sigPV_TRK_BIAS_QOVERP_SAGITTA_WM;   //!
   TBranch        *b_MesonTracks_eta_TRK_BIAS_QOVERP_SAGITTA_WM;   //!
   TBranch        *b_MesonTracks_passTight_TRK_BIAS_QOVERP_SAGITTA_WM;   //!
   TBranch        *b_MesonTracks_phi_TRK_BIAS_QOVERP_SAGITTA_WM;   //!
   TBranch        *b_MesonTracks_pt_TRK_BIAS_QOVERP_SAGITTA_WM;   //!
   TBranch        *b_MesonTracks_trackId_TRK_BIAS_QOVERP_SAGITTA_WM;   //!
   TBranch        *b_MesonTracks_z0sinTheta_TRK_BIAS_QOVERP_SAGITTA_WM;   //!
   TBranch        *b_MesonTracks_truthMatchProbability_TRK_BIAS_QOVERP_SAGITTA_WM;   //!
   TBranch        *b_AntiKt6PV0TrackJets_pt_TRK_BIAS_QOVERP_SAGITTA_WM;   //!
   TBranch        *b_AntiKt6PV0TrackJets_eta_TRK_BIAS_QOVERP_SAGITTA_WM;   //!
   TBranch        *b_AntiKt6PV0TrackJets_phi_TRK_BIAS_QOVERP_SAGITTA_WM;   //!
   TBranch        *b_AntiKt6PV0TrackJets_m_TRK_BIAS_QOVERP_SAGITTA_WM;   //!
   TBranch        *b_AntiKt6PV0TrackJets_daughter__pt_TRK_BIAS_QOVERP_SAGITTA_WM;   //!
   TBranch        *b_AntiKt6PV0TrackJets_daughter__eta_TRK_BIAS_QOVERP_SAGITTA_WM;   //!
   TBranch        *b_AntiKt6PV0TrackJets_daughter__phi_TRK_BIAS_QOVERP_SAGITTA_WM;   //!
   TBranch        *b_AntiKt6PV0TrackJets_daughter__trackId_TRK_BIAS_QOVERP_SAGITTA_WM;   //!
   TBranch        *b_AntiKt8PV0TrackJets_pt_TRK_BIAS_QOVERP_SAGITTA_WM;   //!
   TBranch        *b_AntiKt8PV0TrackJets_eta_TRK_BIAS_QOVERP_SAGITTA_WM;   //!
   TBranch        *b_AntiKt8PV0TrackJets_phi_TRK_BIAS_QOVERP_SAGITTA_WM;   //!
   TBranch        *b_AntiKt8PV0TrackJets_m_TRK_BIAS_QOVERP_SAGITTA_WM;   //!
   TBranch        *b_AntiKt8PV0TrackJets_daughter__pt_TRK_BIAS_QOVERP_SAGITTA_WM;   //!
   TBranch        *b_AntiKt8PV0TrackJets_daughter__eta_TRK_BIAS_QOVERP_SAGITTA_WM;   //!
   TBranch        *b_AntiKt8PV0TrackJets_daughter__phi_TRK_BIAS_QOVERP_SAGITTA_WM;   //!
   TBranch        *b_AntiKt8PV0TrackJets_daughter__trackId_TRK_BIAS_QOVERP_SAGITTA_WM;   //!
   TBranch        *b_AntiKt10PV0TrackJets_pt_TRK_BIAS_QOVERP_SAGITTA_WM;   //!
   TBranch        *b_AntiKt10PV0TrackJets_eta_TRK_BIAS_QOVERP_SAGITTA_WM;   //!
   TBranch        *b_AntiKt10PV0TrackJets_phi_TRK_BIAS_QOVERP_SAGITTA_WM;   //!
   TBranch        *b_AntiKt10PV0TrackJets_m_TRK_BIAS_QOVERP_SAGITTA_WM;   //!
   TBranch        *b_AntiKt10PV0TrackJets_daughter__pt_TRK_BIAS_QOVERP_SAGITTA_WM;   //!
   TBranch        *b_AntiKt10PV0TrackJets_daughter__eta_TRK_BIAS_QOVERP_SAGITTA_WM;   //!
   TBranch        *b_AntiKt10PV0TrackJets_daughter__phi_TRK_BIAS_QOVERP_SAGITTA_WM;   //!
   TBranch        *b_AntiKt10PV0TrackJets_daughter__trackId_TRK_BIAS_QOVERP_SAGITTA_WM;   //!
   TBranch        *b_AntiKtVR30Rmax4Rmin02TrackJets_pt_TRK_BIAS_QOVERP_SAGITTA_WM;   //!
   TBranch        *b_AntiKtVR30Rmax4Rmin02TrackJets_eta_TRK_BIAS_QOVERP_SAGITTA_WM;   //!
   TBranch        *b_AntiKtVR30Rmax4Rmin02TrackJets_phi_TRK_BIAS_QOVERP_SAGITTA_WM;   //!
   TBranch        *b_AntiKtVR30Rmax4Rmin02TrackJets_m_TRK_BIAS_QOVERP_SAGITTA_WM;   //!
   TBranch        *b_AntiKtVR30Rmax4Rmin02TrackJets_daughter__pt_TRK_BIAS_QOVERP_SAGITTA_WM;   //!
   TBranch        *b_AntiKtVR30Rmax4Rmin02TrackJets_daughter__eta_TRK_BIAS_QOVERP_SAGITTA_WM;   //!
   TBranch        *b_AntiKtVR30Rmax4Rmin02TrackJets_daughter__phi_TRK_BIAS_QOVERP_SAGITTA_WM;   //!
   TBranch        *b_AntiKtVR30Rmax4Rmin02TrackJets_daughter__trackId_TRK_BIAS_QOVERP_SAGITTA_WM;   //!
   TBranch        *b_DMesons_costhetastar_TRK_BIAS_Z0_WM;   //!
   TBranch        *b_DMesons_D0Index_TRK_BIAS_Z0_WM;   //!
   TBranch        *b_DMesons_daughterInfo__eta_TRK_BIAS_Z0_WM;   //!
   TBranch        *b_DMesons_daughterInfo__passTight_TRK_BIAS_Z0_WM;   //!
   TBranch        *b_DMesons_daughterInfo__pdgId_TRK_BIAS_Z0_WM;   //!
   TBranch        *b_DMesons_daughterInfo__phi_TRK_BIAS_Z0_WM;   //!
   TBranch        *b_DMesons_daughterInfo__pt_TRK_BIAS_Z0_WM;   //!
   TBranch        *b_DMesons_daughterInfo__trackId_TRK_BIAS_Z0_WM;   //!
   TBranch        *b_DMesons_daughterInfo__truthBarcode_TRK_BIAS_Z0_WM;   //!
   TBranch        *b_DMesons_daughterInfo__truthDBarcode_TRK_BIAS_Z0_WM;   //!
   TBranch        *b_DMesons_daughterInfo__z0SinTheta_TRK_BIAS_Z0_WM;   //!
   TBranch        *b_DMesons_daughterInfo__z0SinThetaPV_TRK_BIAS_Z0_WM;   //!
   TBranch        *b_DMesons_decayType_TRK_BIAS_Z0_WM;   //!
   TBranch        *b_DMesons_DeltaMass_TRK_BIAS_Z0_WM;   //!
   TBranch        *b_DMesons_eta_TRK_BIAS_Z0_WM;   //!
   TBranch        *b_DMesons_fitOutput__Charge_TRK_BIAS_Z0_WM;   //!
   TBranch        *b_DMesons_fitOutput__Chi2_TRK_BIAS_Z0_WM;   //!
   TBranch        *b_DMesons_fitOutput__Impact_TRK_BIAS_Z0_WM;   //!
   TBranch        *b_DMesons_fitOutput__ImpactSignificance_TRK_BIAS_Z0_WM;   //!
   TBranch        *b_DMesons_fitOutput__ImpactZ0SinTheta_TRK_BIAS_Z0_WM;   //!
   TBranch        *b_DMesons_fitOutput__Lxy_TRK_BIAS_Z0_WM;   //!
   TBranch        *b_DMesons_fitOutput__VertexPosition_TRK_BIAS_Z0_WM;   //!
   TBranch        *b_DMesons_m_TRK_BIAS_Z0_WM;   //!
   TBranch        *b_DMesons_mKpi1_TRK_BIAS_Z0_WM;   //!
   TBranch        *b_DMesons_mKpi2_TRK_BIAS_Z0_WM;   //!
   TBranch        *b_DMesons_mPhi1_TRK_BIAS_Z0_WM;   //!
   TBranch        *b_DMesons_mPhi2_TRK_BIAS_Z0_WM;   //!
   TBranch        *b_DMesons_pdgId_TRK_BIAS_Z0_WM;   //!
   TBranch        *b_DMesons_phi_TRK_BIAS_Z0_WM;   //!
   TBranch        *b_DMesons_pt_TRK_BIAS_Z0_WM;   //!
   TBranch        *b_DMesons_ptcone40_TRK_BIAS_Z0_WM;   //!
   TBranch        *b_DMesons_SlowPionD0_TRK_BIAS_Z0_WM;   //!
   TBranch        *b_DMesons_SlowPionZ0SinTheta_TRK_BIAS_Z0_WM;   //!
   TBranch        *b_DMesons_truthBarcode_TRK_BIAS_Z0_WM;   //!
   TBranch        *b_MesonTracks_d0sig_TRK_BIAS_Z0_WM;   //!
   TBranch        *b_MesonTracks_d0sigPV_TRK_BIAS_Z0_WM;   //!
   TBranch        *b_MesonTracks_eta_TRK_BIAS_Z0_WM;   //!
   TBranch        *b_MesonTracks_passTight_TRK_BIAS_Z0_WM;   //!
   TBranch        *b_MesonTracks_phi_TRK_BIAS_Z0_WM;   //!
   TBranch        *b_MesonTracks_pt_TRK_BIAS_Z0_WM;   //!
   TBranch        *b_MesonTracks_trackId_TRK_BIAS_Z0_WM;   //!
   TBranch        *b_MesonTracks_z0sinTheta_TRK_BIAS_Z0_WM;   //!
   TBranch        *b_MesonTracks_truthMatchProbability_TRK_BIAS_Z0_WM;   //!
   TBranch        *b_AntiKt6PV0TrackJets_pt_TRK_BIAS_Z0_WM;   //!
   TBranch        *b_AntiKt6PV0TrackJets_eta_TRK_BIAS_Z0_WM;   //!
   TBranch        *b_AntiKt6PV0TrackJets_phi_TRK_BIAS_Z0_WM;   //!
   TBranch        *b_AntiKt6PV0TrackJets_m_TRK_BIAS_Z0_WM;   //!
   TBranch        *b_AntiKt6PV0TrackJets_daughter__pt_TRK_BIAS_Z0_WM;   //!
   TBranch        *b_AntiKt6PV0TrackJets_daughter__eta_TRK_BIAS_Z0_WM;   //!
   TBranch        *b_AntiKt6PV0TrackJets_daughter__phi_TRK_BIAS_Z0_WM;   //!
   TBranch        *b_AntiKt6PV0TrackJets_daughter__trackId_TRK_BIAS_Z0_WM;   //!
   TBranch        *b_AntiKt8PV0TrackJets_pt_TRK_BIAS_Z0_WM;   //!
   TBranch        *b_AntiKt8PV0TrackJets_eta_TRK_BIAS_Z0_WM;   //!
   TBranch        *b_AntiKt8PV0TrackJets_phi_TRK_BIAS_Z0_WM;   //!
   TBranch        *b_AntiKt8PV0TrackJets_m_TRK_BIAS_Z0_WM;   //!
   TBranch        *b_AntiKt8PV0TrackJets_daughter__pt_TRK_BIAS_Z0_WM;   //!
   TBranch        *b_AntiKt8PV0TrackJets_daughter__eta_TRK_BIAS_Z0_WM;   //!
   TBranch        *b_AntiKt8PV0TrackJets_daughter__phi_TRK_BIAS_Z0_WM;   //!
   TBranch        *b_AntiKt8PV0TrackJets_daughter__trackId_TRK_BIAS_Z0_WM;   //!
   TBranch        *b_AntiKt10PV0TrackJets_pt_TRK_BIAS_Z0_WM;   //!
   TBranch        *b_AntiKt10PV0TrackJets_eta_TRK_BIAS_Z0_WM;   //!
   TBranch        *b_AntiKt10PV0TrackJets_phi_TRK_BIAS_Z0_WM;   //!
   TBranch        *b_AntiKt10PV0TrackJets_m_TRK_BIAS_Z0_WM;   //!
   TBranch        *b_AntiKt10PV0TrackJets_daughter__pt_TRK_BIAS_Z0_WM;   //!
   TBranch        *b_AntiKt10PV0TrackJets_daughter__eta_TRK_BIAS_Z0_WM;   //!
   TBranch        *b_AntiKt10PV0TrackJets_daughter__phi_TRK_BIAS_Z0_WM;   //!
   TBranch        *b_AntiKt10PV0TrackJets_daughter__trackId_TRK_BIAS_Z0_WM;   //!
   TBranch        *b_AntiKtVR30Rmax4Rmin02TrackJets_pt_TRK_BIAS_Z0_WM;   //!
   TBranch        *b_AntiKtVR30Rmax4Rmin02TrackJets_eta_TRK_BIAS_Z0_WM;   //!
   TBranch        *b_AntiKtVR30Rmax4Rmin02TrackJets_phi_TRK_BIAS_Z0_WM;   //!
   TBranch        *b_AntiKtVR30Rmax4Rmin02TrackJets_m_TRK_BIAS_Z0_WM;   //!
   TBranch        *b_AntiKtVR30Rmax4Rmin02TrackJets_daughter__pt_TRK_BIAS_Z0_WM;   //!
   TBranch        *b_AntiKtVR30Rmax4Rmin02TrackJets_daughter__eta_TRK_BIAS_Z0_WM;   //!
   TBranch        *b_AntiKtVR30Rmax4Rmin02TrackJets_daughter__phi_TRK_BIAS_Z0_WM;   //!
   TBranch        *b_AntiKtVR30Rmax4Rmin02TrackJets_daughter__trackId_TRK_BIAS_Z0_WM;   //!
   TBranch        *b_DMesons_costhetastar_TRK_EFF_LOOSE_GLOBAL;   //!
   TBranch        *b_DMesons_D0Index_TRK_EFF_LOOSE_GLOBAL;   //!
   TBranch        *b_DMesons_daughterInfo__eta_TRK_EFF_LOOSE_GLOBAL;   //!
   TBranch        *b_DMesons_daughterInfo__passTight_TRK_EFF_LOOSE_GLOBAL;   //!
   TBranch        *b_DMesons_daughterInfo__pdgId_TRK_EFF_LOOSE_GLOBAL;   //!
   TBranch        *b_DMesons_daughterInfo__phi_TRK_EFF_LOOSE_GLOBAL;   //!
   TBranch        *b_DMesons_daughterInfo__pt_TRK_EFF_LOOSE_GLOBAL;   //!
   TBranch        *b_DMesons_daughterInfo__trackId_TRK_EFF_LOOSE_GLOBAL;   //!
   TBranch        *b_DMesons_daughterInfo__truthBarcode_TRK_EFF_LOOSE_GLOBAL;   //!
   TBranch        *b_DMesons_daughterInfo__truthDBarcode_TRK_EFF_LOOSE_GLOBAL;   //!
   TBranch        *b_DMesons_daughterInfo__z0SinTheta_TRK_EFF_LOOSE_GLOBAL;   //!
   TBranch        *b_DMesons_daughterInfo__z0SinThetaPV_TRK_EFF_LOOSE_GLOBAL;   //!
   TBranch        *b_DMesons_decayType_TRK_EFF_LOOSE_GLOBAL;   //!
   TBranch        *b_DMesons_DeltaMass_TRK_EFF_LOOSE_GLOBAL;   //!
   TBranch        *b_DMesons_eta_TRK_EFF_LOOSE_GLOBAL;   //!
   TBranch        *b_DMesons_fitOutput__Charge_TRK_EFF_LOOSE_GLOBAL;   //!
   TBranch        *b_DMesons_fitOutput__Chi2_TRK_EFF_LOOSE_GLOBAL;   //!
   TBranch        *b_DMesons_fitOutput__Impact_TRK_EFF_LOOSE_GLOBAL;   //!
   TBranch        *b_DMesons_fitOutput__ImpactSignificance_TRK_EFF_LOOSE_GLOBAL;   //!
   TBranch        *b_DMesons_fitOutput__ImpactZ0SinTheta_TRK_EFF_LOOSE_GLOBAL;   //!
   TBranch        *b_DMesons_fitOutput__Lxy_TRK_EFF_LOOSE_GLOBAL;   //!
   TBranch        *b_DMesons_fitOutput__VertexPosition_TRK_EFF_LOOSE_GLOBAL;   //!
   TBranch        *b_DMesons_m_TRK_EFF_LOOSE_GLOBAL;   //!
   TBranch        *b_DMesons_mKpi1_TRK_EFF_LOOSE_GLOBAL;   //!
   TBranch        *b_DMesons_mKpi2_TRK_EFF_LOOSE_GLOBAL;   //!
   TBranch        *b_DMesons_mPhi1_TRK_EFF_LOOSE_GLOBAL;   //!
   TBranch        *b_DMesons_mPhi2_TRK_EFF_LOOSE_GLOBAL;   //!
   TBranch        *b_DMesons_pdgId_TRK_EFF_LOOSE_GLOBAL;   //!
   TBranch        *b_DMesons_phi_TRK_EFF_LOOSE_GLOBAL;   //!
   TBranch        *b_DMesons_pt_TRK_EFF_LOOSE_GLOBAL;   //!
   TBranch        *b_DMesons_ptcone40_TRK_EFF_LOOSE_GLOBAL;   //!
   TBranch        *b_DMesons_SlowPionD0_TRK_EFF_LOOSE_GLOBAL;   //!
   TBranch        *b_DMesons_SlowPionZ0SinTheta_TRK_EFF_LOOSE_GLOBAL;   //!
   TBranch        *b_DMesons_truthBarcode_TRK_EFF_LOOSE_GLOBAL;   //!
   TBranch        *b_MesonTracks_d0sig_TRK_EFF_LOOSE_GLOBAL;   //!
   TBranch        *b_MesonTracks_d0sigPV_TRK_EFF_LOOSE_GLOBAL;   //!
   TBranch        *b_MesonTracks_eta_TRK_EFF_LOOSE_GLOBAL;   //!
   TBranch        *b_MesonTracks_passTight_TRK_EFF_LOOSE_GLOBAL;   //!
   TBranch        *b_MesonTracks_phi_TRK_EFF_LOOSE_GLOBAL;   //!
   TBranch        *b_MesonTracks_pt_TRK_EFF_LOOSE_GLOBAL;   //!
   TBranch        *b_MesonTracks_trackId_TRK_EFF_LOOSE_GLOBAL;   //!
   TBranch        *b_MesonTracks_z0sinTheta_TRK_EFF_LOOSE_GLOBAL;   //!
   TBranch        *b_MesonTracks_truthMatchProbability_TRK_EFF_LOOSE_GLOBAL;   //!
   TBranch        *b_AntiKt6PV0TrackJets_pt_TRK_EFF_LOOSE_GLOBAL;   //!
   TBranch        *b_AntiKt6PV0TrackJets_eta_TRK_EFF_LOOSE_GLOBAL;   //!
   TBranch        *b_AntiKt6PV0TrackJets_phi_TRK_EFF_LOOSE_GLOBAL;   //!
   TBranch        *b_AntiKt6PV0TrackJets_m_TRK_EFF_LOOSE_GLOBAL;   //!
   TBranch        *b_AntiKt6PV0TrackJets_daughter__pt_TRK_EFF_LOOSE_GLOBAL;   //!
   TBranch        *b_AntiKt6PV0TrackJets_daughter__eta_TRK_EFF_LOOSE_GLOBAL;   //!
   TBranch        *b_AntiKt6PV0TrackJets_daughter__phi_TRK_EFF_LOOSE_GLOBAL;   //!
   TBranch        *b_AntiKt6PV0TrackJets_daughter__trackId_TRK_EFF_LOOSE_GLOBAL;   //!
   TBranch        *b_AntiKt8PV0TrackJets_pt_TRK_EFF_LOOSE_GLOBAL;   //!
   TBranch        *b_AntiKt8PV0TrackJets_eta_TRK_EFF_LOOSE_GLOBAL;   //!
   TBranch        *b_AntiKt8PV0TrackJets_phi_TRK_EFF_LOOSE_GLOBAL;   //!
   TBranch        *b_AntiKt8PV0TrackJets_m_TRK_EFF_LOOSE_GLOBAL;   //!
   TBranch        *b_AntiKt8PV0TrackJets_daughter__pt_TRK_EFF_LOOSE_GLOBAL;   //!
   TBranch        *b_AntiKt8PV0TrackJets_daughter__eta_TRK_EFF_LOOSE_GLOBAL;   //!
   TBranch        *b_AntiKt8PV0TrackJets_daughter__phi_TRK_EFF_LOOSE_GLOBAL;   //!
   TBranch        *b_AntiKt8PV0TrackJets_daughter__trackId_TRK_EFF_LOOSE_GLOBAL;   //!
   TBranch        *b_AntiKt10PV0TrackJets_pt_TRK_EFF_LOOSE_GLOBAL;   //!
   TBranch        *b_AntiKt10PV0TrackJets_eta_TRK_EFF_LOOSE_GLOBAL;   //!
   TBranch        *b_AntiKt10PV0TrackJets_phi_TRK_EFF_LOOSE_GLOBAL;   //!
   TBranch        *b_AntiKt10PV0TrackJets_m_TRK_EFF_LOOSE_GLOBAL;   //!
   TBranch        *b_AntiKt10PV0TrackJets_daughter__pt_TRK_EFF_LOOSE_GLOBAL;   //!
   TBranch        *b_AntiKt10PV0TrackJets_daughter__eta_TRK_EFF_LOOSE_GLOBAL;   //!
   TBranch        *b_AntiKt10PV0TrackJets_daughter__phi_TRK_EFF_LOOSE_GLOBAL;   //!
   TBranch        *b_AntiKt10PV0TrackJets_daughter__trackId_TRK_EFF_LOOSE_GLOBAL;   //!
   TBranch        *b_AntiKtVR30Rmax4Rmin02TrackJets_pt_TRK_EFF_LOOSE_GLOBAL;   //!
   TBranch        *b_AntiKtVR30Rmax4Rmin02TrackJets_eta_TRK_EFF_LOOSE_GLOBAL;   //!
   TBranch        *b_AntiKtVR30Rmax4Rmin02TrackJets_phi_TRK_EFF_LOOSE_GLOBAL;   //!
   TBranch        *b_AntiKtVR30Rmax4Rmin02TrackJets_m_TRK_EFF_LOOSE_GLOBAL;   //!
   TBranch        *b_AntiKtVR30Rmax4Rmin02TrackJets_daughter__pt_TRK_EFF_LOOSE_GLOBAL;   //!
   TBranch        *b_AntiKtVR30Rmax4Rmin02TrackJets_daughter__eta_TRK_EFF_LOOSE_GLOBAL;   //!
   TBranch        *b_AntiKtVR30Rmax4Rmin02TrackJets_daughter__phi_TRK_EFF_LOOSE_GLOBAL;   //!
   TBranch        *b_AntiKtVR30Rmax4Rmin02TrackJets_daughter__trackId_TRK_EFF_LOOSE_GLOBAL;   //!
   TBranch        *b_DMesons_costhetastar_TRK_EFF_LOOSE_IBL;   //!
   TBranch        *b_DMesons_D0Index_TRK_EFF_LOOSE_IBL;   //!
   TBranch        *b_DMesons_daughterInfo__eta_TRK_EFF_LOOSE_IBL;   //!
   TBranch        *b_DMesons_daughterInfo__passTight_TRK_EFF_LOOSE_IBL;   //!
   TBranch        *b_DMesons_daughterInfo__pdgId_TRK_EFF_LOOSE_IBL;   //!
   TBranch        *b_DMesons_daughterInfo__phi_TRK_EFF_LOOSE_IBL;   //!
   TBranch        *b_DMesons_daughterInfo__pt_TRK_EFF_LOOSE_IBL;   //!
   TBranch        *b_DMesons_daughterInfo__trackId_TRK_EFF_LOOSE_IBL;   //!
   TBranch        *b_DMesons_daughterInfo__truthBarcode_TRK_EFF_LOOSE_IBL;   //!
   TBranch        *b_DMesons_daughterInfo__truthDBarcode_TRK_EFF_LOOSE_IBL;   //!
   TBranch        *b_DMesons_daughterInfo__z0SinTheta_TRK_EFF_LOOSE_IBL;   //!
   TBranch        *b_DMesons_daughterInfo__z0SinThetaPV_TRK_EFF_LOOSE_IBL;   //!
   TBranch        *b_DMesons_decayType_TRK_EFF_LOOSE_IBL;   //!
   TBranch        *b_DMesons_DeltaMass_TRK_EFF_LOOSE_IBL;   //!
   TBranch        *b_DMesons_eta_TRK_EFF_LOOSE_IBL;   //!
   TBranch        *b_DMesons_fitOutput__Charge_TRK_EFF_LOOSE_IBL;   //!
   TBranch        *b_DMesons_fitOutput__Chi2_TRK_EFF_LOOSE_IBL;   //!
   TBranch        *b_DMesons_fitOutput__Impact_TRK_EFF_LOOSE_IBL;   //!
   TBranch        *b_DMesons_fitOutput__ImpactSignificance_TRK_EFF_LOOSE_IBL;   //!
   TBranch        *b_DMesons_fitOutput__ImpactZ0SinTheta_TRK_EFF_LOOSE_IBL;   //!
   TBranch        *b_DMesons_fitOutput__Lxy_TRK_EFF_LOOSE_IBL;   //!
   TBranch        *b_DMesons_fitOutput__VertexPosition_TRK_EFF_LOOSE_IBL;   //!
   TBranch        *b_DMesons_m_TRK_EFF_LOOSE_IBL;   //!
   TBranch        *b_DMesons_mKpi1_TRK_EFF_LOOSE_IBL;   //!
   TBranch        *b_DMesons_mKpi2_TRK_EFF_LOOSE_IBL;   //!
   TBranch        *b_DMesons_mPhi1_TRK_EFF_LOOSE_IBL;   //!
   TBranch        *b_DMesons_mPhi2_TRK_EFF_LOOSE_IBL;   //!
   TBranch        *b_DMesons_pdgId_TRK_EFF_LOOSE_IBL;   //!
   TBranch        *b_DMesons_phi_TRK_EFF_LOOSE_IBL;   //!
   TBranch        *b_DMesons_pt_TRK_EFF_LOOSE_IBL;   //!
   TBranch        *b_DMesons_ptcone40_TRK_EFF_LOOSE_IBL;   //!
   TBranch        *b_DMesons_SlowPionD0_TRK_EFF_LOOSE_IBL;   //!
   TBranch        *b_DMesons_SlowPionZ0SinTheta_TRK_EFF_LOOSE_IBL;   //!
   TBranch        *b_DMesons_truthBarcode_TRK_EFF_LOOSE_IBL;   //!
   TBranch        *b_MesonTracks_d0sig_TRK_EFF_LOOSE_IBL;   //!
   TBranch        *b_MesonTracks_d0sigPV_TRK_EFF_LOOSE_IBL;   //!
   TBranch        *b_MesonTracks_eta_TRK_EFF_LOOSE_IBL;   //!
   TBranch        *b_MesonTracks_passTight_TRK_EFF_LOOSE_IBL;   //!
   TBranch        *b_MesonTracks_phi_TRK_EFF_LOOSE_IBL;   //!
   TBranch        *b_MesonTracks_pt_TRK_EFF_LOOSE_IBL;   //!
   TBranch        *b_MesonTracks_trackId_TRK_EFF_LOOSE_IBL;   //!
   TBranch        *b_MesonTracks_z0sinTheta_TRK_EFF_LOOSE_IBL;   //!
   TBranch        *b_MesonTracks_truthMatchProbability_TRK_EFF_LOOSE_IBL;   //!
   TBranch        *b_AntiKt6PV0TrackJets_pt_TRK_EFF_LOOSE_IBL;   //!
   TBranch        *b_AntiKt6PV0TrackJets_eta_TRK_EFF_LOOSE_IBL;   //!
   TBranch        *b_AntiKt6PV0TrackJets_phi_TRK_EFF_LOOSE_IBL;   //!
   TBranch        *b_AntiKt6PV0TrackJets_m_TRK_EFF_LOOSE_IBL;   //!
   TBranch        *b_AntiKt6PV0TrackJets_daughter__pt_TRK_EFF_LOOSE_IBL;   //!
   TBranch        *b_AntiKt6PV0TrackJets_daughter__eta_TRK_EFF_LOOSE_IBL;   //!
   TBranch        *b_AntiKt6PV0TrackJets_daughter__phi_TRK_EFF_LOOSE_IBL;   //!
   TBranch        *b_AntiKt6PV0TrackJets_daughter__trackId_TRK_EFF_LOOSE_IBL;   //!
   TBranch        *b_AntiKt8PV0TrackJets_pt_TRK_EFF_LOOSE_IBL;   //!
   TBranch        *b_AntiKt8PV0TrackJets_eta_TRK_EFF_LOOSE_IBL;   //!
   TBranch        *b_AntiKt8PV0TrackJets_phi_TRK_EFF_LOOSE_IBL;   //!
   TBranch        *b_AntiKt8PV0TrackJets_m_TRK_EFF_LOOSE_IBL;   //!
   TBranch        *b_AntiKt8PV0TrackJets_daughter__pt_TRK_EFF_LOOSE_IBL;   //!
   TBranch        *b_AntiKt8PV0TrackJets_daughter__eta_TRK_EFF_LOOSE_IBL;   //!
   TBranch        *b_AntiKt8PV0TrackJets_daughter__phi_TRK_EFF_LOOSE_IBL;   //!
   TBranch        *b_AntiKt8PV0TrackJets_daughter__trackId_TRK_EFF_LOOSE_IBL;   //!
   TBranch        *b_AntiKt10PV0TrackJets_pt_TRK_EFF_LOOSE_IBL;   //!
   TBranch        *b_AntiKt10PV0TrackJets_eta_TRK_EFF_LOOSE_IBL;   //!
   TBranch        *b_AntiKt10PV0TrackJets_phi_TRK_EFF_LOOSE_IBL;   //!
   TBranch        *b_AntiKt10PV0TrackJets_m_TRK_EFF_LOOSE_IBL;   //!
   TBranch        *b_AntiKt10PV0TrackJets_daughter__pt_TRK_EFF_LOOSE_IBL;   //!
   TBranch        *b_AntiKt10PV0TrackJets_daughter__eta_TRK_EFF_LOOSE_IBL;   //!
   TBranch        *b_AntiKt10PV0TrackJets_daughter__phi_TRK_EFF_LOOSE_IBL;   //!
   TBranch        *b_AntiKt10PV0TrackJets_daughter__trackId_TRK_EFF_LOOSE_IBL;   //!
   TBranch        *b_AntiKtVR30Rmax4Rmin02TrackJets_pt_TRK_EFF_LOOSE_IBL;   //!
   TBranch        *b_AntiKtVR30Rmax4Rmin02TrackJets_eta_TRK_EFF_LOOSE_IBL;   //!
   TBranch        *b_AntiKtVR30Rmax4Rmin02TrackJets_phi_TRK_EFF_LOOSE_IBL;   //!
   TBranch        *b_AntiKtVR30Rmax4Rmin02TrackJets_m_TRK_EFF_LOOSE_IBL;   //!
   TBranch        *b_AntiKtVR30Rmax4Rmin02TrackJets_daughter__pt_TRK_EFF_LOOSE_IBL;   //!
   TBranch        *b_AntiKtVR30Rmax4Rmin02TrackJets_daughter__eta_TRK_EFF_LOOSE_IBL;   //!
   TBranch        *b_AntiKtVR30Rmax4Rmin02TrackJets_daughter__phi_TRK_EFF_LOOSE_IBL;   //!
   TBranch        *b_AntiKtVR30Rmax4Rmin02TrackJets_daughter__trackId_TRK_EFF_LOOSE_IBL;   //!
   TBranch        *b_DMesons_costhetastar_TRK_EFF_LOOSE_PHYSMODEL;   //!
   TBranch        *b_DMesons_D0Index_TRK_EFF_LOOSE_PHYSMODEL;   //!
   TBranch        *b_DMesons_daughterInfo__eta_TRK_EFF_LOOSE_PHYSMODEL;   //!
   TBranch        *b_DMesons_daughterInfo__passTight_TRK_EFF_LOOSE_PHYSMODEL;   //!
   TBranch        *b_DMesons_daughterInfo__pdgId_TRK_EFF_LOOSE_PHYSMODEL;   //!
   TBranch        *b_DMesons_daughterInfo__phi_TRK_EFF_LOOSE_PHYSMODEL;   //!
   TBranch        *b_DMesons_daughterInfo__pt_TRK_EFF_LOOSE_PHYSMODEL;   //!
   TBranch        *b_DMesons_daughterInfo__trackId_TRK_EFF_LOOSE_PHYSMODEL;   //!
   TBranch        *b_DMesons_daughterInfo__truthBarcode_TRK_EFF_LOOSE_PHYSMODEL;   //!
   TBranch        *b_DMesons_daughterInfo__truthDBarcode_TRK_EFF_LOOSE_PHYSMODEL;   //!
   TBranch        *b_DMesons_daughterInfo__z0SinTheta_TRK_EFF_LOOSE_PHYSMODEL;   //!
   TBranch        *b_DMesons_daughterInfo__z0SinThetaPV_TRK_EFF_LOOSE_PHYSMODEL;   //!
   TBranch        *b_DMesons_decayType_TRK_EFF_LOOSE_PHYSMODEL;   //!
   TBranch        *b_DMesons_DeltaMass_TRK_EFF_LOOSE_PHYSMODEL;   //!
   TBranch        *b_DMesons_eta_TRK_EFF_LOOSE_PHYSMODEL;   //!
   TBranch        *b_DMesons_fitOutput__Charge_TRK_EFF_LOOSE_PHYSMODEL;   //!
   TBranch        *b_DMesons_fitOutput__Chi2_TRK_EFF_LOOSE_PHYSMODEL;   //!
   TBranch        *b_DMesons_fitOutput__Impact_TRK_EFF_LOOSE_PHYSMODEL;   //!
   TBranch        *b_DMesons_fitOutput__ImpactSignificance_TRK_EFF_LOOSE_PHYSMODEL;   //!
   TBranch        *b_DMesons_fitOutput__ImpactZ0SinTheta_TRK_EFF_LOOSE_PHYSMODEL;   //!
   TBranch        *b_DMesons_fitOutput__Lxy_TRK_EFF_LOOSE_PHYSMODEL;   //!
   TBranch        *b_DMesons_fitOutput__VertexPosition_TRK_EFF_LOOSE_PHYSMODEL;   //!
   TBranch        *b_DMesons_m_TRK_EFF_LOOSE_PHYSMODEL;   //!
   TBranch        *b_DMesons_mKpi1_TRK_EFF_LOOSE_PHYSMODEL;   //!
   TBranch        *b_DMesons_mKpi2_TRK_EFF_LOOSE_PHYSMODEL;   //!
   TBranch        *b_DMesons_mPhi1_TRK_EFF_LOOSE_PHYSMODEL;   //!
   TBranch        *b_DMesons_mPhi2_TRK_EFF_LOOSE_PHYSMODEL;   //!
   TBranch        *b_DMesons_pdgId_TRK_EFF_LOOSE_PHYSMODEL;   //!
   TBranch        *b_DMesons_phi_TRK_EFF_LOOSE_PHYSMODEL;   //!
   TBranch        *b_DMesons_pt_TRK_EFF_LOOSE_PHYSMODEL;   //!
   TBranch        *b_DMesons_ptcone40_TRK_EFF_LOOSE_PHYSMODEL;   //!
   TBranch        *b_DMesons_SlowPionD0_TRK_EFF_LOOSE_PHYSMODEL;   //!
   TBranch        *b_DMesons_SlowPionZ0SinTheta_TRK_EFF_LOOSE_PHYSMODEL;   //!
   TBranch        *b_DMesons_truthBarcode_TRK_EFF_LOOSE_PHYSMODEL;   //!
   TBranch        *b_MesonTracks_d0sig_TRK_EFF_LOOSE_PHYSMODEL;   //!
   TBranch        *b_MesonTracks_d0sigPV_TRK_EFF_LOOSE_PHYSMODEL;   //!
   TBranch        *b_MesonTracks_eta_TRK_EFF_LOOSE_PHYSMODEL;   //!
   TBranch        *b_MesonTracks_passTight_TRK_EFF_LOOSE_PHYSMODEL;   //!
   TBranch        *b_MesonTracks_phi_TRK_EFF_LOOSE_PHYSMODEL;   //!
   TBranch        *b_MesonTracks_pt_TRK_EFF_LOOSE_PHYSMODEL;   //!
   TBranch        *b_MesonTracks_trackId_TRK_EFF_LOOSE_PHYSMODEL;   //!
   TBranch        *b_MesonTracks_z0sinTheta_TRK_EFF_LOOSE_PHYSMODEL;   //!
   TBranch        *b_MesonTracks_truthMatchProbability_TRK_EFF_LOOSE_PHYSMODEL;   //!
   TBranch        *b_AntiKt6PV0TrackJets_pt_TRK_EFF_LOOSE_PHYSMODEL;   //!
   TBranch        *b_AntiKt6PV0TrackJets_eta_TRK_EFF_LOOSE_PHYSMODEL;   //!
   TBranch        *b_AntiKt6PV0TrackJets_phi_TRK_EFF_LOOSE_PHYSMODEL;   //!
   TBranch        *b_AntiKt6PV0TrackJets_m_TRK_EFF_LOOSE_PHYSMODEL;   //!
   TBranch        *b_AntiKt6PV0TrackJets_daughter__pt_TRK_EFF_LOOSE_PHYSMODEL;   //!
   TBranch        *b_AntiKt6PV0TrackJets_daughter__eta_TRK_EFF_LOOSE_PHYSMODEL;   //!
   TBranch        *b_AntiKt6PV0TrackJets_daughter__phi_TRK_EFF_LOOSE_PHYSMODEL;   //!
   TBranch        *b_AntiKt6PV0TrackJets_daughter__trackId_TRK_EFF_LOOSE_PHYSMODEL;   //!
   TBranch        *b_AntiKt8PV0TrackJets_pt_TRK_EFF_LOOSE_PHYSMODEL;   //!
   TBranch        *b_AntiKt8PV0TrackJets_eta_TRK_EFF_LOOSE_PHYSMODEL;   //!
   TBranch        *b_AntiKt8PV0TrackJets_phi_TRK_EFF_LOOSE_PHYSMODEL;   //!
   TBranch        *b_AntiKt8PV0TrackJets_m_TRK_EFF_LOOSE_PHYSMODEL;   //!
   TBranch        *b_AntiKt8PV0TrackJets_daughter__pt_TRK_EFF_LOOSE_PHYSMODEL;   //!
   TBranch        *b_AntiKt8PV0TrackJets_daughter__eta_TRK_EFF_LOOSE_PHYSMODEL;   //!
   TBranch        *b_AntiKt8PV0TrackJets_daughter__phi_TRK_EFF_LOOSE_PHYSMODEL;   //!
   TBranch        *b_AntiKt8PV0TrackJets_daughter__trackId_TRK_EFF_LOOSE_PHYSMODEL;   //!
   TBranch        *b_AntiKt10PV0TrackJets_pt_TRK_EFF_LOOSE_PHYSMODEL;   //!
   TBranch        *b_AntiKt10PV0TrackJets_eta_TRK_EFF_LOOSE_PHYSMODEL;   //!
   TBranch        *b_AntiKt10PV0TrackJets_phi_TRK_EFF_LOOSE_PHYSMODEL;   //!
   TBranch        *b_AntiKt10PV0TrackJets_m_TRK_EFF_LOOSE_PHYSMODEL;   //!
   TBranch        *b_AntiKt10PV0TrackJets_daughter__pt_TRK_EFF_LOOSE_PHYSMODEL;   //!
   TBranch        *b_AntiKt10PV0TrackJets_daughter__eta_TRK_EFF_LOOSE_PHYSMODEL;   //!
   TBranch        *b_AntiKt10PV0TrackJets_daughter__phi_TRK_EFF_LOOSE_PHYSMODEL;   //!
   TBranch        *b_AntiKt10PV0TrackJets_daughter__trackId_TRK_EFF_LOOSE_PHYSMODEL;   //!
   TBranch        *b_AntiKtVR30Rmax4Rmin02TrackJets_pt_TRK_EFF_LOOSE_PHYSMODEL;   //!
   TBranch        *b_AntiKtVR30Rmax4Rmin02TrackJets_eta_TRK_EFF_LOOSE_PHYSMODEL;   //!
   TBranch        *b_AntiKtVR30Rmax4Rmin02TrackJets_phi_TRK_EFF_LOOSE_PHYSMODEL;   //!
   TBranch        *b_AntiKtVR30Rmax4Rmin02TrackJets_m_TRK_EFF_LOOSE_PHYSMODEL;   //!
   TBranch        *b_AntiKtVR30Rmax4Rmin02TrackJets_daughter__pt_TRK_EFF_LOOSE_PHYSMODEL;   //!
   TBranch        *b_AntiKtVR30Rmax4Rmin02TrackJets_daughter__eta_TRK_EFF_LOOSE_PHYSMODEL;   //!
   TBranch        *b_AntiKtVR30Rmax4Rmin02TrackJets_daughter__phi_TRK_EFF_LOOSE_PHYSMODEL;   //!
   TBranch        *b_AntiKtVR30Rmax4Rmin02TrackJets_daughter__trackId_TRK_EFF_LOOSE_PHYSMODEL;   //!
   TBranch        *b_DMesons_costhetastar_TRK_EFF_LOOSE_PP0;   //!
   TBranch        *b_DMesons_D0Index_TRK_EFF_LOOSE_PP0;   //!
   TBranch        *b_DMesons_daughterInfo__eta_TRK_EFF_LOOSE_PP0;   //!
   TBranch        *b_DMesons_daughterInfo__passTight_TRK_EFF_LOOSE_PP0;   //!
   TBranch        *b_DMesons_daughterInfo__pdgId_TRK_EFF_LOOSE_PP0;   //!
   TBranch        *b_DMesons_daughterInfo__phi_TRK_EFF_LOOSE_PP0;   //!
   TBranch        *b_DMesons_daughterInfo__pt_TRK_EFF_LOOSE_PP0;   //!
   TBranch        *b_DMesons_daughterInfo__trackId_TRK_EFF_LOOSE_PP0;   //!
   TBranch        *b_DMesons_daughterInfo__truthBarcode_TRK_EFF_LOOSE_PP0;   //!
   TBranch        *b_DMesons_daughterInfo__truthDBarcode_TRK_EFF_LOOSE_PP0;   //!
   TBranch        *b_DMesons_daughterInfo__z0SinTheta_TRK_EFF_LOOSE_PP0;   //!
   TBranch        *b_DMesons_daughterInfo__z0SinThetaPV_TRK_EFF_LOOSE_PP0;   //!
   TBranch        *b_DMesons_decayType_TRK_EFF_LOOSE_PP0;   //!
   TBranch        *b_DMesons_DeltaMass_TRK_EFF_LOOSE_PP0;   //!
   TBranch        *b_DMesons_eta_TRK_EFF_LOOSE_PP0;   //!
   TBranch        *b_DMesons_fitOutput__Charge_TRK_EFF_LOOSE_PP0;   //!
   TBranch        *b_DMesons_fitOutput__Chi2_TRK_EFF_LOOSE_PP0;   //!
   TBranch        *b_DMesons_fitOutput__Impact_TRK_EFF_LOOSE_PP0;   //!
   TBranch        *b_DMesons_fitOutput__ImpactSignificance_TRK_EFF_LOOSE_PP0;   //!
   TBranch        *b_DMesons_fitOutput__ImpactZ0SinTheta_TRK_EFF_LOOSE_PP0;   //!
   TBranch        *b_DMesons_fitOutput__Lxy_TRK_EFF_LOOSE_PP0;   //!
   TBranch        *b_DMesons_fitOutput__VertexPosition_TRK_EFF_LOOSE_PP0;   //!
   TBranch        *b_DMesons_m_TRK_EFF_LOOSE_PP0;   //!
   TBranch        *b_DMesons_mKpi1_TRK_EFF_LOOSE_PP0;   //!
   TBranch        *b_DMesons_mKpi2_TRK_EFF_LOOSE_PP0;   //!
   TBranch        *b_DMesons_mPhi1_TRK_EFF_LOOSE_PP0;   //!
   TBranch        *b_DMesons_mPhi2_TRK_EFF_LOOSE_PP0;   //!
   TBranch        *b_DMesons_pdgId_TRK_EFF_LOOSE_PP0;   //!
   TBranch        *b_DMesons_phi_TRK_EFF_LOOSE_PP0;   //!
   TBranch        *b_DMesons_pt_TRK_EFF_LOOSE_PP0;   //!
   TBranch        *b_DMesons_ptcone40_TRK_EFF_LOOSE_PP0;   //!
   TBranch        *b_DMesons_SlowPionD0_TRK_EFF_LOOSE_PP0;   //!
   TBranch        *b_DMesons_SlowPionZ0SinTheta_TRK_EFF_LOOSE_PP0;   //!
   TBranch        *b_DMesons_truthBarcode_TRK_EFF_LOOSE_PP0;   //!
   TBranch        *b_MesonTracks_d0sig_TRK_EFF_LOOSE_PP0;   //!
   TBranch        *b_MesonTracks_d0sigPV_TRK_EFF_LOOSE_PP0;   //!
   TBranch        *b_MesonTracks_eta_TRK_EFF_LOOSE_PP0;   //!
   TBranch        *b_MesonTracks_passTight_TRK_EFF_LOOSE_PP0;   //!
   TBranch        *b_MesonTracks_phi_TRK_EFF_LOOSE_PP0;   //!
   TBranch        *b_MesonTracks_pt_TRK_EFF_LOOSE_PP0;   //!
   TBranch        *b_MesonTracks_trackId_TRK_EFF_LOOSE_PP0;   //!
   TBranch        *b_MesonTracks_z0sinTheta_TRK_EFF_LOOSE_PP0;   //!
   TBranch        *b_MesonTracks_truthMatchProbability_TRK_EFF_LOOSE_PP0;   //!
   TBranch        *b_AntiKt6PV0TrackJets_pt_TRK_EFF_LOOSE_PP0;   //!
   TBranch        *b_AntiKt6PV0TrackJets_eta_TRK_EFF_LOOSE_PP0;   //!
   TBranch        *b_AntiKt6PV0TrackJets_phi_TRK_EFF_LOOSE_PP0;   //!
   TBranch        *b_AntiKt6PV0TrackJets_m_TRK_EFF_LOOSE_PP0;   //!
   TBranch        *b_AntiKt6PV0TrackJets_daughter__pt_TRK_EFF_LOOSE_PP0;   //!
   TBranch        *b_AntiKt6PV0TrackJets_daughter__eta_TRK_EFF_LOOSE_PP0;   //!
   TBranch        *b_AntiKt6PV0TrackJets_daughter__phi_TRK_EFF_LOOSE_PP0;   //!
   TBranch        *b_AntiKt6PV0TrackJets_daughter__trackId_TRK_EFF_LOOSE_PP0;   //!
   TBranch        *b_AntiKt8PV0TrackJets_pt_TRK_EFF_LOOSE_PP0;   //!
   TBranch        *b_AntiKt8PV0TrackJets_eta_TRK_EFF_LOOSE_PP0;   //!
   TBranch        *b_AntiKt8PV0TrackJets_phi_TRK_EFF_LOOSE_PP0;   //!
   TBranch        *b_AntiKt8PV0TrackJets_m_TRK_EFF_LOOSE_PP0;   //!
   TBranch        *b_AntiKt8PV0TrackJets_daughter__pt_TRK_EFF_LOOSE_PP0;   //!
   TBranch        *b_AntiKt8PV0TrackJets_daughter__eta_TRK_EFF_LOOSE_PP0;   //!
   TBranch        *b_AntiKt8PV0TrackJets_daughter__phi_TRK_EFF_LOOSE_PP0;   //!
   TBranch        *b_AntiKt8PV0TrackJets_daughter__trackId_TRK_EFF_LOOSE_PP0;   //!
   TBranch        *b_AntiKt10PV0TrackJets_pt_TRK_EFF_LOOSE_PP0;   //!
   TBranch        *b_AntiKt10PV0TrackJets_eta_TRK_EFF_LOOSE_PP0;   //!
   TBranch        *b_AntiKt10PV0TrackJets_phi_TRK_EFF_LOOSE_PP0;   //!
   TBranch        *b_AntiKt10PV0TrackJets_m_TRK_EFF_LOOSE_PP0;   //!
   TBranch        *b_AntiKt10PV0TrackJets_daughter__pt_TRK_EFF_LOOSE_PP0;   //!
   TBranch        *b_AntiKt10PV0TrackJets_daughter__eta_TRK_EFF_LOOSE_PP0;   //!
   TBranch        *b_AntiKt10PV0TrackJets_daughter__phi_TRK_EFF_LOOSE_PP0;   //!
   TBranch        *b_AntiKt10PV0TrackJets_daughter__trackId_TRK_EFF_LOOSE_PP0;   //!
   TBranch        *b_AntiKtVR30Rmax4Rmin02TrackJets_pt_TRK_EFF_LOOSE_PP0;   //!
   TBranch        *b_AntiKtVR30Rmax4Rmin02TrackJets_eta_TRK_EFF_LOOSE_PP0;   //!
   TBranch        *b_AntiKtVR30Rmax4Rmin02TrackJets_phi_TRK_EFF_LOOSE_PP0;   //!
   TBranch        *b_AntiKtVR30Rmax4Rmin02TrackJets_m_TRK_EFF_LOOSE_PP0;   //!
   TBranch        *b_AntiKtVR30Rmax4Rmin02TrackJets_daughter__pt_TRK_EFF_LOOSE_PP0;   //!
   TBranch        *b_AntiKtVR30Rmax4Rmin02TrackJets_daughter__eta_TRK_EFF_LOOSE_PP0;   //!
   TBranch        *b_AntiKtVR30Rmax4Rmin02TrackJets_daughter__phi_TRK_EFF_LOOSE_PP0;   //!
   TBranch        *b_AntiKtVR30Rmax4Rmin02TrackJets_daughter__trackId_TRK_EFF_LOOSE_PP0;   //!
   TBranch        *b_DMesons_costhetastar_TRK_FAKE_RATE_LOOSE;   //!
   TBranch        *b_DMesons_D0Index_TRK_FAKE_RATE_LOOSE;   //!
   TBranch        *b_DMesons_daughterInfo__eta_TRK_FAKE_RATE_LOOSE;   //!
   TBranch        *b_DMesons_daughterInfo__passTight_TRK_FAKE_RATE_LOOSE;   //!
   TBranch        *b_DMesons_daughterInfo__pdgId_TRK_FAKE_RATE_LOOSE;   //!
   TBranch        *b_DMesons_daughterInfo__phi_TRK_FAKE_RATE_LOOSE;   //!
   TBranch        *b_DMesons_daughterInfo__pt_TRK_FAKE_RATE_LOOSE;   //!
   TBranch        *b_DMesons_daughterInfo__trackId_TRK_FAKE_RATE_LOOSE;   //!
   TBranch        *b_DMesons_daughterInfo__truthBarcode_TRK_FAKE_RATE_LOOSE;   //!
   TBranch        *b_DMesons_daughterInfo__truthDBarcode_TRK_FAKE_RATE_LOOSE;   //!
   TBranch        *b_DMesons_daughterInfo__z0SinTheta_TRK_FAKE_RATE_LOOSE;   //!
   TBranch        *b_DMesons_daughterInfo__z0SinThetaPV_TRK_FAKE_RATE_LOOSE;   //!
   TBranch        *b_DMesons_decayType_TRK_FAKE_RATE_LOOSE;   //!
   TBranch        *b_DMesons_DeltaMass_TRK_FAKE_RATE_LOOSE;   //!
   TBranch        *b_DMesons_eta_TRK_FAKE_RATE_LOOSE;   //!
   TBranch        *b_DMesons_fitOutput__Charge_TRK_FAKE_RATE_LOOSE;   //!
   TBranch        *b_DMesons_fitOutput__Chi2_TRK_FAKE_RATE_LOOSE;   //!
   TBranch        *b_DMesons_fitOutput__Impact_TRK_FAKE_RATE_LOOSE;   //!
   TBranch        *b_DMesons_fitOutput__ImpactSignificance_TRK_FAKE_RATE_LOOSE;   //!
   TBranch        *b_DMesons_fitOutput__ImpactZ0SinTheta_TRK_FAKE_RATE_LOOSE;   //!
   TBranch        *b_DMesons_fitOutput__Lxy_TRK_FAKE_RATE_LOOSE;   //!
   TBranch        *b_DMesons_fitOutput__VertexPosition_TRK_FAKE_RATE_LOOSE;   //!
   TBranch        *b_DMesons_m_TRK_FAKE_RATE_LOOSE;   //!
   TBranch        *b_DMesons_mKpi1_TRK_FAKE_RATE_LOOSE;   //!
   TBranch        *b_DMesons_mKpi2_TRK_FAKE_RATE_LOOSE;   //!
   TBranch        *b_DMesons_mPhi1_TRK_FAKE_RATE_LOOSE;   //!
   TBranch        *b_DMesons_mPhi2_TRK_FAKE_RATE_LOOSE;   //!
   TBranch        *b_DMesons_pdgId_TRK_FAKE_RATE_LOOSE;   //!
   TBranch        *b_DMesons_phi_TRK_FAKE_RATE_LOOSE;   //!
   TBranch        *b_DMesons_pt_TRK_FAKE_RATE_LOOSE;   //!
   TBranch        *b_DMesons_ptcone40_TRK_FAKE_RATE_LOOSE;   //!
   TBranch        *b_DMesons_SlowPionD0_TRK_FAKE_RATE_LOOSE;   //!
   TBranch        *b_DMesons_SlowPionZ0SinTheta_TRK_FAKE_RATE_LOOSE;   //!
   TBranch        *b_DMesons_truthBarcode_TRK_FAKE_RATE_LOOSE;   //!
   TBranch        *b_MesonTracks_d0sig_TRK_FAKE_RATE_LOOSE;   //!
   TBranch        *b_MesonTracks_d0sigPV_TRK_FAKE_RATE_LOOSE;   //!
   TBranch        *b_MesonTracks_eta_TRK_FAKE_RATE_LOOSE;   //!
   TBranch        *b_MesonTracks_passTight_TRK_FAKE_RATE_LOOSE;   //!
   TBranch        *b_MesonTracks_phi_TRK_FAKE_RATE_LOOSE;   //!
   TBranch        *b_MesonTracks_pt_TRK_FAKE_RATE_LOOSE;   //!
   TBranch        *b_MesonTracks_trackId_TRK_FAKE_RATE_LOOSE;   //!
   TBranch        *b_MesonTracks_z0sinTheta_TRK_FAKE_RATE_LOOSE;   //!
   TBranch        *b_MesonTracks_truthMatchProbability_TRK_FAKE_RATE_LOOSE;   //!
   TBranch        *b_AntiKt6PV0TrackJets_pt_TRK_FAKE_RATE_LOOSE;   //!
   TBranch        *b_AntiKt6PV0TrackJets_eta_TRK_FAKE_RATE_LOOSE;   //!
   TBranch        *b_AntiKt6PV0TrackJets_phi_TRK_FAKE_RATE_LOOSE;   //!
   TBranch        *b_AntiKt6PV0TrackJets_m_TRK_FAKE_RATE_LOOSE;   //!
   TBranch        *b_AntiKt6PV0TrackJets_daughter__pt_TRK_FAKE_RATE_LOOSE;   //!
   TBranch        *b_AntiKt6PV0TrackJets_daughter__eta_TRK_FAKE_RATE_LOOSE;   //!
   TBranch        *b_AntiKt6PV0TrackJets_daughter__phi_TRK_FAKE_RATE_LOOSE;   //!
   TBranch        *b_AntiKt6PV0TrackJets_daughter__trackId_TRK_FAKE_RATE_LOOSE;   //!
   TBranch        *b_AntiKt8PV0TrackJets_pt_TRK_FAKE_RATE_LOOSE;   //!
   TBranch        *b_AntiKt8PV0TrackJets_eta_TRK_FAKE_RATE_LOOSE;   //!
   TBranch        *b_AntiKt8PV0TrackJets_phi_TRK_FAKE_RATE_LOOSE;   //!
   TBranch        *b_AntiKt8PV0TrackJets_m_TRK_FAKE_RATE_LOOSE;   //!
   TBranch        *b_AntiKt8PV0TrackJets_daughter__pt_TRK_FAKE_RATE_LOOSE;   //!
   TBranch        *b_AntiKt8PV0TrackJets_daughter__eta_TRK_FAKE_RATE_LOOSE;   //!
   TBranch        *b_AntiKt8PV0TrackJets_daughter__phi_TRK_FAKE_RATE_LOOSE;   //!
   TBranch        *b_AntiKt8PV0TrackJets_daughter__trackId_TRK_FAKE_RATE_LOOSE;   //!
   TBranch        *b_AntiKt10PV0TrackJets_pt_TRK_FAKE_RATE_LOOSE;   //!
   TBranch        *b_AntiKt10PV0TrackJets_eta_TRK_FAKE_RATE_LOOSE;   //!
   TBranch        *b_AntiKt10PV0TrackJets_phi_TRK_FAKE_RATE_LOOSE;   //!
   TBranch        *b_AntiKt10PV0TrackJets_m_TRK_FAKE_RATE_LOOSE;   //!
   TBranch        *b_AntiKt10PV0TrackJets_daughter__pt_TRK_FAKE_RATE_LOOSE;   //!
   TBranch        *b_AntiKt10PV0TrackJets_daughter__eta_TRK_FAKE_RATE_LOOSE;   //!
   TBranch        *b_AntiKt10PV0TrackJets_daughter__phi_TRK_FAKE_RATE_LOOSE;   //!
   TBranch        *b_AntiKt10PV0TrackJets_daughter__trackId_TRK_FAKE_RATE_LOOSE;   //!
   TBranch        *b_AntiKtVR30Rmax4Rmin02TrackJets_pt_TRK_FAKE_RATE_LOOSE;   //!
   TBranch        *b_AntiKtVR30Rmax4Rmin02TrackJets_eta_TRK_FAKE_RATE_LOOSE;   //!
   TBranch        *b_AntiKtVR30Rmax4Rmin02TrackJets_phi_TRK_FAKE_RATE_LOOSE;   //!
   TBranch        *b_AntiKtVR30Rmax4Rmin02TrackJets_m_TRK_FAKE_RATE_LOOSE;   //!
   TBranch        *b_AntiKtVR30Rmax4Rmin02TrackJets_daughter__pt_TRK_FAKE_RATE_LOOSE;   //!
   TBranch        *b_AntiKtVR30Rmax4Rmin02TrackJets_daughter__eta_TRK_FAKE_RATE_LOOSE;   //!
   TBranch        *b_AntiKtVR30Rmax4Rmin02TrackJets_daughter__phi_TRK_FAKE_RATE_LOOSE;   //!
   TBranch        *b_AntiKtVR30Rmax4Rmin02TrackJets_daughter__trackId_TRK_FAKE_RATE_LOOSE;   //!
   TBranch        *b_DMesons_costhetastar_TRK_RES_D0_DEAD;   //!
   TBranch        *b_DMesons_D0Index_TRK_RES_D0_DEAD;   //!
   TBranch        *b_DMesons_daughterInfo__eta_TRK_RES_D0_DEAD;   //!
   TBranch        *b_DMesons_daughterInfo__passTight_TRK_RES_D0_DEAD;   //!
   TBranch        *b_DMesons_daughterInfo__pdgId_TRK_RES_D0_DEAD;   //!
   TBranch        *b_DMesons_daughterInfo__phi_TRK_RES_D0_DEAD;   //!
   TBranch        *b_DMesons_daughterInfo__pt_TRK_RES_D0_DEAD;   //!
   TBranch        *b_DMesons_daughterInfo__trackId_TRK_RES_D0_DEAD;   //!
   TBranch        *b_DMesons_daughterInfo__truthBarcode_TRK_RES_D0_DEAD;   //!
   TBranch        *b_DMesons_daughterInfo__truthDBarcode_TRK_RES_D0_DEAD;   //!
   TBranch        *b_DMesons_daughterInfo__z0SinTheta_TRK_RES_D0_DEAD;   //!
   TBranch        *b_DMesons_daughterInfo__z0SinThetaPV_TRK_RES_D0_DEAD;   //!
   TBranch        *b_DMesons_decayType_TRK_RES_D0_DEAD;   //!
   TBranch        *b_DMesons_DeltaMass_TRK_RES_D0_DEAD;   //!
   TBranch        *b_DMesons_eta_TRK_RES_D0_DEAD;   //!
   TBranch        *b_DMesons_fitOutput__Charge_TRK_RES_D0_DEAD;   //!
   TBranch        *b_DMesons_fitOutput__Chi2_TRK_RES_D0_DEAD;   //!
   TBranch        *b_DMesons_fitOutput__Impact_TRK_RES_D0_DEAD;   //!
   TBranch        *b_DMesons_fitOutput__ImpactSignificance_TRK_RES_D0_DEAD;   //!
   TBranch        *b_DMesons_fitOutput__ImpactZ0SinTheta_TRK_RES_D0_DEAD;   //!
   TBranch        *b_DMesons_fitOutput__Lxy_TRK_RES_D0_DEAD;   //!
   TBranch        *b_DMesons_fitOutput__VertexPosition_TRK_RES_D0_DEAD;   //!
   TBranch        *b_DMesons_m_TRK_RES_D0_DEAD;   //!
   TBranch        *b_DMesons_mKpi1_TRK_RES_D0_DEAD;   //!
   TBranch        *b_DMesons_mKpi2_TRK_RES_D0_DEAD;   //!
   TBranch        *b_DMesons_mPhi1_TRK_RES_D0_DEAD;   //!
   TBranch        *b_DMesons_mPhi2_TRK_RES_D0_DEAD;   //!
   TBranch        *b_DMesons_pdgId_TRK_RES_D0_DEAD;   //!
   TBranch        *b_DMesons_phi_TRK_RES_D0_DEAD;   //!
   TBranch        *b_DMesons_pt_TRK_RES_D0_DEAD;   //!
   TBranch        *b_DMesons_ptcone40_TRK_RES_D0_DEAD;   //!
   TBranch        *b_DMesons_SlowPionD0_TRK_RES_D0_DEAD;   //!
   TBranch        *b_DMesons_SlowPionZ0SinTheta_TRK_RES_D0_DEAD;   //!
   TBranch        *b_DMesons_truthBarcode_TRK_RES_D0_DEAD;   //!
   TBranch        *b_MesonTracks_d0sig_TRK_RES_D0_DEAD;   //!
   TBranch        *b_MesonTracks_d0sigPV_TRK_RES_D0_DEAD;   //!
   TBranch        *b_MesonTracks_eta_TRK_RES_D0_DEAD;   //!
   TBranch        *b_MesonTracks_passTight_TRK_RES_D0_DEAD;   //!
   TBranch        *b_MesonTracks_phi_TRK_RES_D0_DEAD;   //!
   TBranch        *b_MesonTracks_pt_TRK_RES_D0_DEAD;   //!
   TBranch        *b_MesonTracks_trackId_TRK_RES_D0_DEAD;   //!
   TBranch        *b_MesonTracks_z0sinTheta_TRK_RES_D0_DEAD;   //!
   TBranch        *b_MesonTracks_truthMatchProbability_TRK_RES_D0_DEAD;   //!
   TBranch        *b_AntiKt6PV0TrackJets_pt_TRK_RES_D0_DEAD;   //!
   TBranch        *b_AntiKt6PV0TrackJets_eta_TRK_RES_D0_DEAD;   //!
   TBranch        *b_AntiKt6PV0TrackJets_phi_TRK_RES_D0_DEAD;   //!
   TBranch        *b_AntiKt6PV0TrackJets_m_TRK_RES_D0_DEAD;   //!
   TBranch        *b_AntiKt6PV0TrackJets_daughter__pt_TRK_RES_D0_DEAD;   //!
   TBranch        *b_AntiKt6PV0TrackJets_daughter__eta_TRK_RES_D0_DEAD;   //!
   TBranch        *b_AntiKt6PV0TrackJets_daughter__phi_TRK_RES_D0_DEAD;   //!
   TBranch        *b_AntiKt6PV0TrackJets_daughter__trackId_TRK_RES_D0_DEAD;   //!
   TBranch        *b_AntiKt8PV0TrackJets_pt_TRK_RES_D0_DEAD;   //!
   TBranch        *b_AntiKt8PV0TrackJets_eta_TRK_RES_D0_DEAD;   //!
   TBranch        *b_AntiKt8PV0TrackJets_phi_TRK_RES_D0_DEAD;   //!
   TBranch        *b_AntiKt8PV0TrackJets_m_TRK_RES_D0_DEAD;   //!
   TBranch        *b_AntiKt8PV0TrackJets_daughter__pt_TRK_RES_D0_DEAD;   //!
   TBranch        *b_AntiKt8PV0TrackJets_daughter__eta_TRK_RES_D0_DEAD;   //!
   TBranch        *b_AntiKt8PV0TrackJets_daughter__phi_TRK_RES_D0_DEAD;   //!
   TBranch        *b_AntiKt8PV0TrackJets_daughter__trackId_TRK_RES_D0_DEAD;   //!
   TBranch        *b_AntiKt10PV0TrackJets_pt_TRK_RES_D0_DEAD;   //!
   TBranch        *b_AntiKt10PV0TrackJets_eta_TRK_RES_D0_DEAD;   //!
   TBranch        *b_AntiKt10PV0TrackJets_phi_TRK_RES_D0_DEAD;   //!
   TBranch        *b_AntiKt10PV0TrackJets_m_TRK_RES_D0_DEAD;   //!
   TBranch        *b_AntiKt10PV0TrackJets_daughter__pt_TRK_RES_D0_DEAD;   //!
   TBranch        *b_AntiKt10PV0TrackJets_daughter__eta_TRK_RES_D0_DEAD;   //!
   TBranch        *b_AntiKt10PV0TrackJets_daughter__phi_TRK_RES_D0_DEAD;   //!
   TBranch        *b_AntiKt10PV0TrackJets_daughter__trackId_TRK_RES_D0_DEAD;   //!
   TBranch        *b_AntiKtVR30Rmax4Rmin02TrackJets_pt_TRK_RES_D0_DEAD;   //!
   TBranch        *b_AntiKtVR30Rmax4Rmin02TrackJets_eta_TRK_RES_D0_DEAD;   //!
   TBranch        *b_AntiKtVR30Rmax4Rmin02TrackJets_phi_TRK_RES_D0_DEAD;   //!
   TBranch        *b_AntiKtVR30Rmax4Rmin02TrackJets_m_TRK_RES_D0_DEAD;   //!
   TBranch        *b_AntiKtVR30Rmax4Rmin02TrackJets_daughter__pt_TRK_RES_D0_DEAD;   //!
   TBranch        *b_AntiKtVR30Rmax4Rmin02TrackJets_daughter__eta_TRK_RES_D0_DEAD;   //!
   TBranch        *b_AntiKtVR30Rmax4Rmin02TrackJets_daughter__phi_TRK_RES_D0_DEAD;   //!
   TBranch        *b_AntiKtVR30Rmax4Rmin02TrackJets_daughter__trackId_TRK_RES_D0_DEAD;   //!
   TBranch        *b_DMesons_costhetastar_TRK_RES_D0_MEAS;   //!
   TBranch        *b_DMesons_D0Index_TRK_RES_D0_MEAS;   //!
   TBranch        *b_DMesons_daughterInfo__eta_TRK_RES_D0_MEAS;   //!
   TBranch        *b_DMesons_daughterInfo__passTight_TRK_RES_D0_MEAS;   //!
   TBranch        *b_DMesons_daughterInfo__pdgId_TRK_RES_D0_MEAS;   //!
   TBranch        *b_DMesons_daughterInfo__phi_TRK_RES_D0_MEAS;   //!
   TBranch        *b_DMesons_daughterInfo__pt_TRK_RES_D0_MEAS;   //!
   TBranch        *b_DMesons_daughterInfo__trackId_TRK_RES_D0_MEAS;   //!
   TBranch        *b_DMesons_daughterInfo__truthBarcode_TRK_RES_D0_MEAS;   //!
   TBranch        *b_DMesons_daughterInfo__truthDBarcode_TRK_RES_D0_MEAS;   //!
   TBranch        *b_DMesons_daughterInfo__z0SinTheta_TRK_RES_D0_MEAS;   //!
   TBranch        *b_DMesons_daughterInfo__z0SinThetaPV_TRK_RES_D0_MEAS;   //!
   TBranch        *b_DMesons_decayType_TRK_RES_D0_MEAS;   //!
   TBranch        *b_DMesons_DeltaMass_TRK_RES_D0_MEAS;   //!
   TBranch        *b_DMesons_eta_TRK_RES_D0_MEAS;   //!
   TBranch        *b_DMesons_fitOutput__Charge_TRK_RES_D0_MEAS;   //!
   TBranch        *b_DMesons_fitOutput__Chi2_TRK_RES_D0_MEAS;   //!
   TBranch        *b_DMesons_fitOutput__Impact_TRK_RES_D0_MEAS;   //!
   TBranch        *b_DMesons_fitOutput__ImpactSignificance_TRK_RES_D0_MEAS;   //!
   TBranch        *b_DMesons_fitOutput__ImpactZ0SinTheta_TRK_RES_D0_MEAS;   //!
   TBranch        *b_DMesons_fitOutput__Lxy_TRK_RES_D0_MEAS;   //!
   TBranch        *b_DMesons_fitOutput__VertexPosition_TRK_RES_D0_MEAS;   //!
   TBranch        *b_DMesons_m_TRK_RES_D0_MEAS;   //!
   TBranch        *b_DMesons_mKpi1_TRK_RES_D0_MEAS;   //!
   TBranch        *b_DMesons_mKpi2_TRK_RES_D0_MEAS;   //!
   TBranch        *b_DMesons_mPhi1_TRK_RES_D0_MEAS;   //!
   TBranch        *b_DMesons_mPhi2_TRK_RES_D0_MEAS;   //!
   TBranch        *b_DMesons_pdgId_TRK_RES_D0_MEAS;   //!
   TBranch        *b_DMesons_phi_TRK_RES_D0_MEAS;   //!
   TBranch        *b_DMesons_pt_TRK_RES_D0_MEAS;   //!
   TBranch        *b_DMesons_ptcone40_TRK_RES_D0_MEAS;   //!
   TBranch        *b_DMesons_SlowPionD0_TRK_RES_D0_MEAS;   //!
   TBranch        *b_DMesons_SlowPionZ0SinTheta_TRK_RES_D0_MEAS;   //!
   TBranch        *b_DMesons_truthBarcode_TRK_RES_D0_MEAS;   //!
   TBranch        *b_MesonTracks_d0sig_TRK_RES_D0_MEAS;   //!
   TBranch        *b_MesonTracks_d0sigPV_TRK_RES_D0_MEAS;   //!
   TBranch        *b_MesonTracks_eta_TRK_RES_D0_MEAS;   //!
   TBranch        *b_MesonTracks_passTight_TRK_RES_D0_MEAS;   //!
   TBranch        *b_MesonTracks_phi_TRK_RES_D0_MEAS;   //!
   TBranch        *b_MesonTracks_pt_TRK_RES_D0_MEAS;   //!
   TBranch        *b_MesonTracks_trackId_TRK_RES_D0_MEAS;   //!
   TBranch        *b_MesonTracks_z0sinTheta_TRK_RES_D0_MEAS;   //!
   TBranch        *b_MesonTracks_truthMatchProbability_TRK_RES_D0_MEAS;   //!
   TBranch        *b_AntiKt6PV0TrackJets_pt_TRK_RES_D0_MEAS;   //!
   TBranch        *b_AntiKt6PV0TrackJets_eta_TRK_RES_D0_MEAS;   //!
   TBranch        *b_AntiKt6PV0TrackJets_phi_TRK_RES_D0_MEAS;   //!
   TBranch        *b_AntiKt6PV0TrackJets_m_TRK_RES_D0_MEAS;   //!
   TBranch        *b_AntiKt6PV0TrackJets_daughter__pt_TRK_RES_D0_MEAS;   //!
   TBranch        *b_AntiKt6PV0TrackJets_daughter__eta_TRK_RES_D0_MEAS;   //!
   TBranch        *b_AntiKt6PV0TrackJets_daughter__phi_TRK_RES_D0_MEAS;   //!
   TBranch        *b_AntiKt6PV0TrackJets_daughter__trackId_TRK_RES_D0_MEAS;   //!
   TBranch        *b_AntiKt8PV0TrackJets_pt_TRK_RES_D0_MEAS;   //!
   TBranch        *b_AntiKt8PV0TrackJets_eta_TRK_RES_D0_MEAS;   //!
   TBranch        *b_AntiKt8PV0TrackJets_phi_TRK_RES_D0_MEAS;   //!
   TBranch        *b_AntiKt8PV0TrackJets_m_TRK_RES_D0_MEAS;   //!
   TBranch        *b_AntiKt8PV0TrackJets_daughter__pt_TRK_RES_D0_MEAS;   //!
   TBranch        *b_AntiKt8PV0TrackJets_daughter__eta_TRK_RES_D0_MEAS;   //!
   TBranch        *b_AntiKt8PV0TrackJets_daughter__phi_TRK_RES_D0_MEAS;   //!
   TBranch        *b_AntiKt8PV0TrackJets_daughter__trackId_TRK_RES_D0_MEAS;   //!
   TBranch        *b_AntiKt10PV0TrackJets_pt_TRK_RES_D0_MEAS;   //!
   TBranch        *b_AntiKt10PV0TrackJets_eta_TRK_RES_D0_MEAS;   //!
   TBranch        *b_AntiKt10PV0TrackJets_phi_TRK_RES_D0_MEAS;   //!
   TBranch        *b_AntiKt10PV0TrackJets_m_TRK_RES_D0_MEAS;   //!
   TBranch        *b_AntiKt10PV0TrackJets_daughter__pt_TRK_RES_D0_MEAS;   //!
   TBranch        *b_AntiKt10PV0TrackJets_daughter__eta_TRK_RES_D0_MEAS;   //!
   TBranch        *b_AntiKt10PV0TrackJets_daughter__phi_TRK_RES_D0_MEAS;   //!
   TBranch        *b_AntiKt10PV0TrackJets_daughter__trackId_TRK_RES_D0_MEAS;   //!
   TBranch        *b_AntiKtVR30Rmax4Rmin02TrackJets_pt_TRK_RES_D0_MEAS;   //!
   TBranch        *b_AntiKtVR30Rmax4Rmin02TrackJets_eta_TRK_RES_D0_MEAS;   //!
   TBranch        *b_AntiKtVR30Rmax4Rmin02TrackJets_phi_TRK_RES_D0_MEAS;   //!
   TBranch        *b_AntiKtVR30Rmax4Rmin02TrackJets_m_TRK_RES_D0_MEAS;   //!
   TBranch        *b_AntiKtVR30Rmax4Rmin02TrackJets_daughter__pt_TRK_RES_D0_MEAS;   //!
   TBranch        *b_AntiKtVR30Rmax4Rmin02TrackJets_daughter__eta_TRK_RES_D0_MEAS;   //!
   TBranch        *b_AntiKtVR30Rmax4Rmin02TrackJets_daughter__phi_TRK_RES_D0_MEAS;   //!
   TBranch        *b_AntiKtVR30Rmax4Rmin02TrackJets_daughter__trackId_TRK_RES_D0_MEAS;   //!
   TBranch        *b_DMesons_costhetastar_TRK_RES_Z0_DEAD;   //!
   TBranch        *b_DMesons_D0Index_TRK_RES_Z0_DEAD;   //!
   TBranch        *b_DMesons_daughterInfo__eta_TRK_RES_Z0_DEAD;   //!
   TBranch        *b_DMesons_daughterInfo__passTight_TRK_RES_Z0_DEAD;   //!
   TBranch        *b_DMesons_daughterInfo__pdgId_TRK_RES_Z0_DEAD;   //!
   TBranch        *b_DMesons_daughterInfo__phi_TRK_RES_Z0_DEAD;   //!
   TBranch        *b_DMesons_daughterInfo__pt_TRK_RES_Z0_DEAD;   //!
   TBranch        *b_DMesons_daughterInfo__trackId_TRK_RES_Z0_DEAD;   //!
   TBranch        *b_DMesons_daughterInfo__truthBarcode_TRK_RES_Z0_DEAD;   //!
   TBranch        *b_DMesons_daughterInfo__truthDBarcode_TRK_RES_Z0_DEAD;   //!
   TBranch        *b_DMesons_daughterInfo__z0SinTheta_TRK_RES_Z0_DEAD;   //!
   TBranch        *b_DMesons_daughterInfo__z0SinThetaPV_TRK_RES_Z0_DEAD;   //!
   TBranch        *b_DMesons_decayType_TRK_RES_Z0_DEAD;   //!
   TBranch        *b_DMesons_DeltaMass_TRK_RES_Z0_DEAD;   //!
   TBranch        *b_DMesons_eta_TRK_RES_Z0_DEAD;   //!
   TBranch        *b_DMesons_fitOutput__Charge_TRK_RES_Z0_DEAD;   //!
   TBranch        *b_DMesons_fitOutput__Chi2_TRK_RES_Z0_DEAD;   //!
   TBranch        *b_DMesons_fitOutput__Impact_TRK_RES_Z0_DEAD;   //!
   TBranch        *b_DMesons_fitOutput__ImpactSignificance_TRK_RES_Z0_DEAD;   //!
   TBranch        *b_DMesons_fitOutput__ImpactZ0SinTheta_TRK_RES_Z0_DEAD;   //!
   TBranch        *b_DMesons_fitOutput__Lxy_TRK_RES_Z0_DEAD;   //!
   TBranch        *b_DMesons_fitOutput__VertexPosition_TRK_RES_Z0_DEAD;   //!
   TBranch        *b_DMesons_m_TRK_RES_Z0_DEAD;   //!
   TBranch        *b_DMesons_mKpi1_TRK_RES_Z0_DEAD;   //!
   TBranch        *b_DMesons_mKpi2_TRK_RES_Z0_DEAD;   //!
   TBranch        *b_DMesons_mPhi1_TRK_RES_Z0_DEAD;   //!
   TBranch        *b_DMesons_mPhi2_TRK_RES_Z0_DEAD;   //!
   TBranch        *b_DMesons_pdgId_TRK_RES_Z0_DEAD;   //!
   TBranch        *b_DMesons_phi_TRK_RES_Z0_DEAD;   //!
   TBranch        *b_DMesons_pt_TRK_RES_Z0_DEAD;   //!
   TBranch        *b_DMesons_ptcone40_TRK_RES_Z0_DEAD;   //!
   TBranch        *b_DMesons_SlowPionD0_TRK_RES_Z0_DEAD;   //!
   TBranch        *b_DMesons_SlowPionZ0SinTheta_TRK_RES_Z0_DEAD;   //!
   TBranch        *b_DMesons_truthBarcode_TRK_RES_Z0_DEAD;   //!
   TBranch        *b_MesonTracks_d0sig_TRK_RES_Z0_DEAD;   //!
   TBranch        *b_MesonTracks_d0sigPV_TRK_RES_Z0_DEAD;   //!
   TBranch        *b_MesonTracks_eta_TRK_RES_Z0_DEAD;   //!
   TBranch        *b_MesonTracks_passTight_TRK_RES_Z0_DEAD;   //!
   TBranch        *b_MesonTracks_phi_TRK_RES_Z0_DEAD;   //!
   TBranch        *b_MesonTracks_pt_TRK_RES_Z0_DEAD;   //!
   TBranch        *b_MesonTracks_trackId_TRK_RES_Z0_DEAD;   //!
   TBranch        *b_MesonTracks_z0sinTheta_TRK_RES_Z0_DEAD;   //!
   TBranch        *b_MesonTracks_truthMatchProbability_TRK_RES_Z0_DEAD;   //!
   TBranch        *b_AntiKt6PV0TrackJets_pt_TRK_RES_Z0_DEAD;   //!
   TBranch        *b_AntiKt6PV0TrackJets_eta_TRK_RES_Z0_DEAD;   //!
   TBranch        *b_AntiKt6PV0TrackJets_phi_TRK_RES_Z0_DEAD;   //!
   TBranch        *b_AntiKt6PV0TrackJets_m_TRK_RES_Z0_DEAD;   //!
   TBranch        *b_AntiKt6PV0TrackJets_daughter__pt_TRK_RES_Z0_DEAD;   //!
   TBranch        *b_AntiKt6PV0TrackJets_daughter__eta_TRK_RES_Z0_DEAD;   //!
   TBranch        *b_AntiKt6PV0TrackJets_daughter__phi_TRK_RES_Z0_DEAD;   //!
   TBranch        *b_AntiKt6PV0TrackJets_daughter__trackId_TRK_RES_Z0_DEAD;   //!
   TBranch        *b_AntiKt8PV0TrackJets_pt_TRK_RES_Z0_DEAD;   //!
   TBranch        *b_AntiKt8PV0TrackJets_eta_TRK_RES_Z0_DEAD;   //!
   TBranch        *b_AntiKt8PV0TrackJets_phi_TRK_RES_Z0_DEAD;   //!
   TBranch        *b_AntiKt8PV0TrackJets_m_TRK_RES_Z0_DEAD;   //!
   TBranch        *b_AntiKt8PV0TrackJets_daughter__pt_TRK_RES_Z0_DEAD;   //!
   TBranch        *b_AntiKt8PV0TrackJets_daughter__eta_TRK_RES_Z0_DEAD;   //!
   TBranch        *b_AntiKt8PV0TrackJets_daughter__phi_TRK_RES_Z0_DEAD;   //!
   TBranch        *b_AntiKt8PV0TrackJets_daughter__trackId_TRK_RES_Z0_DEAD;   //!
   TBranch        *b_AntiKt10PV0TrackJets_pt_TRK_RES_Z0_DEAD;   //!
   TBranch        *b_AntiKt10PV0TrackJets_eta_TRK_RES_Z0_DEAD;   //!
   TBranch        *b_AntiKt10PV0TrackJets_phi_TRK_RES_Z0_DEAD;   //!
   TBranch        *b_AntiKt10PV0TrackJets_m_TRK_RES_Z0_DEAD;   //!
   TBranch        *b_AntiKt10PV0TrackJets_daughter__pt_TRK_RES_Z0_DEAD;   //!
   TBranch        *b_AntiKt10PV0TrackJets_daughter__eta_TRK_RES_Z0_DEAD;   //!
   TBranch        *b_AntiKt10PV0TrackJets_daughter__phi_TRK_RES_Z0_DEAD;   //!
   TBranch        *b_AntiKt10PV0TrackJets_daughter__trackId_TRK_RES_Z0_DEAD;   //!
   TBranch        *b_AntiKtVR30Rmax4Rmin02TrackJets_pt_TRK_RES_Z0_DEAD;   //!
   TBranch        *b_AntiKtVR30Rmax4Rmin02TrackJets_eta_TRK_RES_Z0_DEAD;   //!
   TBranch        *b_AntiKtVR30Rmax4Rmin02TrackJets_phi_TRK_RES_Z0_DEAD;   //!
   TBranch        *b_AntiKtVR30Rmax4Rmin02TrackJets_m_TRK_RES_Z0_DEAD;   //!
   TBranch        *b_AntiKtVR30Rmax4Rmin02TrackJets_daughter__pt_TRK_RES_Z0_DEAD;   //!
   TBranch        *b_AntiKtVR30Rmax4Rmin02TrackJets_daughter__eta_TRK_RES_Z0_DEAD;   //!
   TBranch        *b_AntiKtVR30Rmax4Rmin02TrackJets_daughter__phi_TRK_RES_Z0_DEAD;   //!
   TBranch        *b_AntiKtVR30Rmax4Rmin02TrackJets_daughter__trackId_TRK_RES_Z0_DEAD;   //!
   TBranch        *b_DMesons_costhetastar_TRK_RES_Z0_MEAS;   //!
   TBranch        *b_DMesons_D0Index_TRK_RES_Z0_MEAS;   //!
   TBranch        *b_DMesons_daughterInfo__eta_TRK_RES_Z0_MEAS;   //!
   TBranch        *b_DMesons_daughterInfo__passTight_TRK_RES_Z0_MEAS;   //!
   TBranch        *b_DMesons_daughterInfo__pdgId_TRK_RES_Z0_MEAS;   //!
   TBranch        *b_DMesons_daughterInfo__phi_TRK_RES_Z0_MEAS;   //!
   TBranch        *b_DMesons_daughterInfo__pt_TRK_RES_Z0_MEAS;   //!
   TBranch        *b_DMesons_daughterInfo__trackId_TRK_RES_Z0_MEAS;   //!
   TBranch        *b_DMesons_daughterInfo__truthBarcode_TRK_RES_Z0_MEAS;   //!
   TBranch        *b_DMesons_daughterInfo__truthDBarcode_TRK_RES_Z0_MEAS;   //!
   TBranch        *b_DMesons_daughterInfo__z0SinTheta_TRK_RES_Z0_MEAS;   //!
   TBranch        *b_DMesons_daughterInfo__z0SinThetaPV_TRK_RES_Z0_MEAS;   //!
   TBranch        *b_DMesons_decayType_TRK_RES_Z0_MEAS;   //!
   TBranch        *b_DMesons_DeltaMass_TRK_RES_Z0_MEAS;   //!
   TBranch        *b_DMesons_eta_TRK_RES_Z0_MEAS;   //!
   TBranch        *b_DMesons_fitOutput__Charge_TRK_RES_Z0_MEAS;   //!
   TBranch        *b_DMesons_fitOutput__Chi2_TRK_RES_Z0_MEAS;   //!
   TBranch        *b_DMesons_fitOutput__Impact_TRK_RES_Z0_MEAS;   //!
   TBranch        *b_DMesons_fitOutput__ImpactSignificance_TRK_RES_Z0_MEAS;   //!
   TBranch        *b_DMesons_fitOutput__ImpactZ0SinTheta_TRK_RES_Z0_MEAS;   //!
   TBranch        *b_DMesons_fitOutput__Lxy_TRK_RES_Z0_MEAS;   //!
   TBranch        *b_DMesons_fitOutput__VertexPosition_TRK_RES_Z0_MEAS;   //!
   TBranch        *b_DMesons_m_TRK_RES_Z0_MEAS;   //!
   TBranch        *b_DMesons_mKpi1_TRK_RES_Z0_MEAS;   //!
   TBranch        *b_DMesons_mKpi2_TRK_RES_Z0_MEAS;   //!
   TBranch        *b_DMesons_mPhi1_TRK_RES_Z0_MEAS;   //!
   TBranch        *b_DMesons_mPhi2_TRK_RES_Z0_MEAS;   //!
   TBranch        *b_DMesons_pdgId_TRK_RES_Z0_MEAS;   //!
   TBranch        *b_DMesons_phi_TRK_RES_Z0_MEAS;   //!
   TBranch        *b_DMesons_pt_TRK_RES_Z0_MEAS;   //!
   TBranch        *b_DMesons_ptcone40_TRK_RES_Z0_MEAS;   //!
   TBranch        *b_DMesons_SlowPionD0_TRK_RES_Z0_MEAS;   //!
   TBranch        *b_DMesons_SlowPionZ0SinTheta_TRK_RES_Z0_MEAS;   //!
   TBranch        *b_DMesons_truthBarcode_TRK_RES_Z0_MEAS;   //!
   TBranch        *b_MesonTracks_d0sig_TRK_RES_Z0_MEAS;   //!
   TBranch        *b_MesonTracks_d0sigPV_TRK_RES_Z0_MEAS;   //!
   TBranch        *b_MesonTracks_eta_TRK_RES_Z0_MEAS;   //!
   TBranch        *b_MesonTracks_passTight_TRK_RES_Z0_MEAS;   //!
   TBranch        *b_MesonTracks_phi_TRK_RES_Z0_MEAS;   //!
   TBranch        *b_MesonTracks_pt_TRK_RES_Z0_MEAS;   //!
   TBranch        *b_MesonTracks_trackId_TRK_RES_Z0_MEAS;   //!
   TBranch        *b_MesonTracks_z0sinTheta_TRK_RES_Z0_MEAS;   //!
   TBranch        *b_MesonTracks_truthMatchProbability_TRK_RES_Z0_MEAS;   //!
   TBranch        *b_AntiKt6PV0TrackJets_pt_TRK_RES_Z0_MEAS;   //!
   TBranch        *b_AntiKt6PV0TrackJets_eta_TRK_RES_Z0_MEAS;   //!
   TBranch        *b_AntiKt6PV0TrackJets_phi_TRK_RES_Z0_MEAS;   //!
   TBranch        *b_AntiKt6PV0TrackJets_m_TRK_RES_Z0_MEAS;   //!
   TBranch        *b_AntiKt6PV0TrackJets_daughter__pt_TRK_RES_Z0_MEAS;   //!
   TBranch        *b_AntiKt6PV0TrackJets_daughter__eta_TRK_RES_Z0_MEAS;   //!
   TBranch        *b_AntiKt6PV0TrackJets_daughter__phi_TRK_RES_Z0_MEAS;   //!
   TBranch        *b_AntiKt6PV0TrackJets_daughter__trackId_TRK_RES_Z0_MEAS;   //!
   TBranch        *b_AntiKt8PV0TrackJets_pt_TRK_RES_Z0_MEAS;   //!
   TBranch        *b_AntiKt8PV0TrackJets_eta_TRK_RES_Z0_MEAS;   //!
   TBranch        *b_AntiKt8PV0TrackJets_phi_TRK_RES_Z0_MEAS;   //!
   TBranch        *b_AntiKt8PV0TrackJets_m_TRK_RES_Z0_MEAS;   //!
   TBranch        *b_AntiKt8PV0TrackJets_daughter__pt_TRK_RES_Z0_MEAS;   //!
   TBranch        *b_AntiKt8PV0TrackJets_daughter__eta_TRK_RES_Z0_MEAS;   //!
   TBranch        *b_AntiKt8PV0TrackJets_daughter__phi_TRK_RES_Z0_MEAS;   //!
   TBranch        *b_AntiKt8PV0TrackJets_daughter__trackId_TRK_RES_Z0_MEAS;   //!
   TBranch        *b_AntiKt10PV0TrackJets_pt_TRK_RES_Z0_MEAS;   //!
   TBranch        *b_AntiKt10PV0TrackJets_eta_TRK_RES_Z0_MEAS;   //!
   TBranch        *b_AntiKt10PV0TrackJets_phi_TRK_RES_Z0_MEAS;   //!
   TBranch        *b_AntiKt10PV0TrackJets_m_TRK_RES_Z0_MEAS;   //!
   TBranch        *b_AntiKt10PV0TrackJets_daughter__pt_TRK_RES_Z0_MEAS;   //!
   TBranch        *b_AntiKt10PV0TrackJets_daughter__eta_TRK_RES_Z0_MEAS;   //!
   TBranch        *b_AntiKt10PV0TrackJets_daughter__phi_TRK_RES_Z0_MEAS;   //!
   TBranch        *b_AntiKt10PV0TrackJets_daughter__trackId_TRK_RES_Z0_MEAS;   //!
   TBranch        *b_AntiKtVR30Rmax4Rmin02TrackJets_pt_TRK_RES_Z0_MEAS;   //!
   TBranch        *b_AntiKtVR30Rmax4Rmin02TrackJets_eta_TRK_RES_Z0_MEAS;   //!
   TBranch        *b_AntiKtVR30Rmax4Rmin02TrackJets_phi_TRK_RES_Z0_MEAS;   //!
   TBranch        *b_AntiKtVR30Rmax4Rmin02TrackJets_m_TRK_RES_Z0_MEAS;   //!
   TBranch        *b_AntiKtVR30Rmax4Rmin02TrackJets_daughter__pt_TRK_RES_Z0_MEAS;   //!
   TBranch        *b_AntiKtVR30Rmax4Rmin02TrackJets_daughter__eta_TRK_RES_Z0_MEAS;   //!
   TBranch        *b_AntiKtVR30Rmax4Rmin02TrackJets_daughter__phi_TRK_RES_Z0_MEAS;   //!
   TBranch        *b_AntiKtVR30Rmax4Rmin02TrackJets_daughter__trackId_TRK_RES_Z0_MEAS;   //!

   CharmAnalysis(TTree *tree=0);
   virtual ~CharmAnalysis();
   virtual Int_t    Cut(Long64_t entry);
   virtual Int_t    GetEntry(Long64_t entry);
   virtual Long64_t LoadTree(Long64_t entry);
   virtual void     Init(TTree *tree);
   virtual void     Loop();
   virtual Bool_t   Notify();
   virtual void     Show(Long64_t entry = -1);
};

#endif

#ifdef CharmAnalysis_cxx
CharmAnalysis::CharmAnalysis(TTree *tree) : fChain(0) 
{
// if parameter tree is not specified (or zero), connect the file
// used to generate this class and read the Tree.
   if (tree == 0) {
      TFile *f = (TFile*)gROOT->GetListOfFiles()->FindObject("Sherpa_WplusD.MC16e.700367.0.root");
      if (!f || !f->IsOpen()) {
         f = new TFile("Sherpa_WplusD.MC16e.700367.0.root");
      }
      f->GetObject("CharmAnalysis",tree);

   }
   Init(tree);
}

CharmAnalysis::~CharmAnalysis()
{
   if (!fChain) return;
   delete fChain->GetCurrentFile();
}

Int_t CharmAnalysis::GetEntry(Long64_t entry)
{
// Read contents of entry.
   if (!fChain) return 0;
   return fChain->GetEntry(entry);
}
Long64_t CharmAnalysis::LoadTree(Long64_t entry)
{
// Set the environment to read one entry
   if (!fChain) return -5;
   Long64_t centry = fChain->LoadTree(entry);
   if (centry < 0) return centry;
   if (fChain->GetTreeNumber() != fCurrent) {
      fCurrent = fChain->GetTreeNumber();
      Notify();
   }
   return centry;
}

void CharmAnalysis::Init(TTree *tree)
{
   // The Init() function is called when the selector needs to initialize
   // a new tree or chain. Typically here the branch addresses and branch
   // pointers of the tree will be set.
   // It is normally not necessary to make changes to the generated
   // code, but the routine can be extended by the user if needed.
   // Init() will be called many times when running on PROOF
   // (once per file to be processed).

   // Set object pointer
   AnalysisElectrons_pt_NOSYS = 0;
   AnalysisElectrons_el_selected_NOSYS = 0;
   AnalysisElectrons_eta = 0;
   AnalysisElectrons_caloCluster_eta = 0;
   AnalysisElectrons_phi = 0;
   AnalysisElectrons_charge = 0;
   AnalysisElectrons_d0sig = 0;
   AnalysisElectrons_d0 = 0;
   AnalysisElectrons_z0sinTheta = 0;
   AnalysisElectrons_topoetcone20 = 0;
   AnalysisElectrons_ptvarcone20_TightTTVA_pt1000 = 0;
   AnalysisElectrons_ptvarcone30_TightTTVA_pt1000 = 0;
   AnalysisElectrons_likelihood_Tight = 0;
   AnalysisElectrons_isIsolated_Tight_VarRad_NOSYS = 0;
   AnalysisElectrons_effSF_NOSYS = 0;
   AnalysisElectrons_effSF_ID_Tight_NOSYS = 0;
   AnalysisElectrons_effSF_Chflip_Tight_Tight_VarRad_NOSYS = 0;
   AnalysisElectrons_effSF_Isol_Tight_Tight_VarRad_NOSYS = 0;
   AnalysisElectrons_effSF_Trig_Tight_Tight_VarRad_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_NOSYS = 0;
   AnalysisElectrons_effSF_Trig_Tight_Tight_VarRad_Eff_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_NOSYS = 0;
   AnalysisElectrons_effSF_Chflip_Tight_noIso_NOSYS = 0;
   AnalysisElectrons_effSF_Trig_Tight_noIso_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_NOSYS = 0;
   AnalysisElectrons_effSF_Trig_Tight_noIso_Eff_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_NOSYS = 0;
   AnalysisElectrons_matched_HLT_e24_lhmedium_L1EM20VH = 0;
   AnalysisElectrons_matched_HLT_e60_lhmedium = 0;
   AnalysisElectrons_matched_HLT_e120_lhloose = 0;
   AnalysisElectrons_matched_HLT_e26_lhtight_nod0_ivarloose = 0;
   AnalysisElectrons_matched_HLT_e60_lhmedium_nod0 = 0;
   AnalysisElectrons_matched_HLT_e140_lhloose_nod0 = 0;
   AnalysisElectrons_truthType = 0;
   AnalysisElectrons_truthOrigin = 0;
   AnalysisElectrons_firstEgMotherPdgId = 0;
   AnalysisElectrons_firstEgMotherTruthType = 0;
   AnalysisElectrons_firstEgMotherTruthOrigin = 0;
   AnalysisMuons_pt_NOSYS = 0;
   AnalysisMuons_mu_selected_NOSYS = 0;
   AnalysisMuons_is_bad = 0;
   AnalysisMuons_eta = 0;
   AnalysisMuons_phi = 0;
   AnalysisMuons_charge = 0;
   AnalysisMuons_d0sig = 0;
   AnalysisMuons_d0 = 0;
   AnalysisMuons_z0sinTheta = 0;
   AnalysisMuons_topoetcone20 = 0;
   AnalysisMuons_ptvarcone30_TightTTVA_pt1000 = 0;
   AnalysisMuons_ptvarcone30_TightTTVA_pt500 = 0;
   AnalysisMuons_neflowisol20 = 0;
   AnalysisMuons_isQuality_Loose_NOSYS = 0;
   AnalysisMuons_isQuality_Medium_NOSYS = 0;
   AnalysisMuons_isQuality_Tight_NOSYS = 0;
   AnalysisMuons_isIsolated_PflowTight_VarRad_NOSYS = 0;
   AnalysisMuons_muon_effSF_NOSYS = 0;
   AnalysisMuons_muon_effSF_TTVA_NOSYS = 0;
   AnalysisMuons_muon_effSF_Quality_Loose_NOSYS = 0;
   AnalysisMuons_muon_effSF_Quality_Medium_NOSYS = 0;
   AnalysisMuons_muon_effSF_Quality_Tight_NOSYS = 0;
   AnalysisMuons_muon_effSF_Isol_PflowTight_VarRad_NOSYS = 0;
   AnalysisMuons_muon_effMC_Trig_Loose_HLT_mu26_ivarmedium_OR_HLT_mu50_NOSYS = 0;
   AnalysisMuons_muon_effData_Trig_Loose_HLT_mu26_ivarmedium_OR_HLT_mu50_NOSYS = 0;
   AnalysisMuons_muon_effMC_Trig_Loose_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_NOSYS = 0;
   AnalysisMuons_muon_effData_Trig_Loose_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_NOSYS = 0;
   AnalysisMuons_muon_effMC_Trig_Medium_HLT_mu26_ivarmedium_OR_HLT_mu50_NOSYS = 0;
   AnalysisMuons_muon_effData_Trig_Medium_HLT_mu26_ivarmedium_OR_HLT_mu50_NOSYS = 0;
   AnalysisMuons_muon_effMC_Trig_Medium_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_NOSYS = 0;
   AnalysisMuons_muon_effData_Trig_Medium_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_NOSYS = 0;
   AnalysisMuons_muon_effMC_Trig_Tight_HLT_mu26_ivarmedium_OR_HLT_mu50_NOSYS = 0;
   AnalysisMuons_muon_effData_Trig_Tight_HLT_mu26_ivarmedium_OR_HLT_mu50_NOSYS = 0;
   AnalysisMuons_muon_effMC_Trig_Tight_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_NOSYS = 0;
   AnalysisMuons_muon_effData_Trig_Tight_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_NOSYS = 0;
   AnalysisMuons_matched_HLT_mu20_iloose_L1MU15 = 0;
   AnalysisMuons_matched_HLT_mu26_ivarmedium = 0;
   AnalysisMuons_matched_HLT_mu50 = 0;
   AnalysisMuons_truthType = 0;
   AnalysisMuons_truthOrigin = 0;
   AnalysisJets_pt_NOSYS = 0;
   AnalysisJets_jet_selected_NOSYS = 0;
   AnalysisJets_m = 0;
   AnalysisJets_eta = 0;
   AnalysisJets_phi = 0;
   AnalysisJets_ftag_select_DL1r_FixedCutBEff_70 = 0;
   AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_NOSYS = 0;
   AnalysisJets_ftag_quantile_DL1r = 0;
   AnalysisJets_ftag_DL1pu_DL1r = 0;
   AnalysisJets_ftag_DL1pb_DL1r = 0;
   AnalysisJets_ftag_DL1pc_DL1r = 0;
   AnalysisJets_HadronConeExclExtendedTruthLabelID = 0;
   AnalysisJets_HadronConeExclTruthLabelID = 0;
   DMesons_costhetastar_NOSYS = 0;
   DMesons_D0Index_NOSYS = 0;
   DMesons_daughterInfo__eta_NOSYS = 0;
   DMesons_daughterInfo__passTight_NOSYS = 0;
   DMesons_daughterInfo__pdgId_NOSYS = 0;
   DMesons_daughterInfo__phi_NOSYS = 0;
   DMesons_daughterInfo__pt_NOSYS = 0;
   DMesons_daughterInfo__trackId_NOSYS = 0;
   DMesons_daughterInfo__truthBarcode_NOSYS = 0;
   DMesons_daughterInfo__truthDBarcode_NOSYS = 0;
   DMesons_daughterInfo__z0SinTheta_NOSYS = 0;
   DMesons_daughterInfo__z0SinThetaPV_NOSYS = 0;
   DMesons_decayType_NOSYS = 0;
   DMesons_DeltaMass_NOSYS = 0;
   DMesons_eta_NOSYS = 0;
   DMesons_fitOutput__Charge_NOSYS = 0;
   DMesons_fitOutput__Chi2_NOSYS = 0;
   DMesons_fitOutput__Impact_NOSYS = 0;
   DMesons_fitOutput__ImpactSignificance_NOSYS = 0;
   DMesons_fitOutput__ImpactZ0SinTheta_NOSYS = 0;
   DMesons_fitOutput__Lxy_NOSYS = 0;
   DMesons_fitOutput__VertexPosition_NOSYS = 0;
   DMesons_m_NOSYS = 0;
   DMesons_mKpi1_NOSYS = 0;
   DMesons_mKpi2_NOSYS = 0;
   DMesons_mPhi1_NOSYS = 0;
   DMesons_mPhi2_NOSYS = 0;
   DMesons_pdgId_NOSYS = 0;
   DMesons_phi_NOSYS = 0;
   DMesons_pt_NOSYS = 0;
   DMesons_ptcone40_NOSYS = 0;
   DMesons_SlowPionD0_NOSYS = 0;
   DMesons_SlowPionZ0SinTheta_NOSYS = 0;
   DMesons_truthBarcode_NOSYS = 0;
   TruthParticles_Selected_barcode = 0;
   TruthParticles_Selected_cosThetaStarT = 0;
   TruthParticles_Selected_daughterInfoT__barcode = 0;
   TruthParticles_Selected_daughterInfoT__eta = 0;
   TruthParticles_Selected_daughterInfoT__pdgId = 0;
   TruthParticles_Selected_daughterInfoT__phi = 0;
   TruthParticles_Selected_daughterInfoT__pt = 0;
   TruthParticles_Selected_decayMode = 0;
   TruthParticles_Selected_eta = 0;
   TruthParticles_Selected_fromBdecay = 0;
   TruthParticles_Selected_ImpactT = 0;
   TruthParticles_Selected_LxyT = 0;
   TruthParticles_Selected_m = 0;
   TruthParticles_Selected_pdgId = 0;
   TruthParticles_Selected_phi = 0;
   TruthParticles_Selected_pt = 0;
   TruthParticles_Selected_status = 0;
   TruthParticles_Selected_vertexPosition = 0;
   TruthLeptons_barcode = 0;
   TruthLeptons_classifierParticleOrigin = 0;
   TruthLeptons_classifierParticleType = 0;
   TruthLeptons_e = 0;
   TruthLeptons_eta = 0;
   TruthLeptons_m = 0;
   TruthLeptons_pdgId = 0;
   TruthLeptons_phi = 0;
   TruthLeptons_pt = 0;
   TruthLeptons_status = 0;
   TruthLeptons_e_dressed = 0;
   TruthLeptons_eta_dressed = 0;
   TruthLeptons_phi_dressed = 0;
   TruthLeptons_pt_dressed = 0;
   AntiKt4TruthJets_ConeTruthLabelID = 0;
   AntiKt4TruthJets_eta = 0;
   AntiKt4TruthJets_HadronConeExclTruthLabelID = 0;
   AntiKt4TruthJets_m = 0;
   AntiKt4TruthJets_phi = 0;
   AntiKt4TruthJets_pt = 0;
   AntiKt4TruthChargedJets_ConeTruthLabelID = 0;
   AntiKt4TruthChargedJets_eta = 0;
   AntiKt4TruthChargedJets_HadronConeExclTruthLabelID = 0;
   AntiKt4TruthChargedJets_m = 0;
   AntiKt4TruthChargedJets_phi = 0;
   AntiKt4TruthChargedJets_pt = 0;
   AntiKt6TruthChargedJets_ConeTruthLabelID = 0;
   AntiKt6TruthChargedJets_eta = 0;
   AntiKt6TruthChargedJets_HadronConeExclTruthLabelID = 0;
   AntiKt6TruthChargedJets_m = 0;
   AntiKt6TruthChargedJets_phi = 0;
   AntiKt6TruthChargedJets_pt = 0;
   AntiKt8TruthChargedJets_ConeTruthLabelID = 0;
   AntiKt8TruthChargedJets_eta = 0;
   AntiKt8TruthChargedJets_HadronConeExclTruthLabelID = 0;
   AntiKt8TruthChargedJets_m = 0;
   AntiKt8TruthChargedJets_phi = 0;
   AntiKt8TruthChargedJets_pt = 0;
   AntiKt10TruthChargedJets_ConeTruthLabelID = 0;
   AntiKt10TruthChargedJets_eta = 0;
   AntiKt10TruthChargedJets_HadronConeExclTruthLabelID = 0;
   AntiKt10TruthChargedJets_m = 0;
   AntiKt10TruthChargedJets_phi = 0;
   AntiKt10TruthChargedJets_pt = 0;
   MesonTracks_d0sig_NOSYS = 0;
   MesonTracks_d0sigPV_NOSYS = 0;
   MesonTracks_eta_NOSYS = 0;
   MesonTracks_passTight_NOSYS = 0;
   MesonTracks_phi_NOSYS = 0;
   MesonTracks_pt_NOSYS = 0;
   MesonTracks_trackId_NOSYS = 0;
   MesonTracks_z0sinTheta_NOSYS = 0;
   MesonTracks_truthMatchProbability_NOSYS = 0;
   AntiKt6PV0TrackJets_pt_NOSYS = 0;
   AntiKt6PV0TrackJets_eta_NOSYS = 0;
   AntiKt6PV0TrackJets_phi_NOSYS = 0;
   AntiKt6PV0TrackJets_m_NOSYS = 0;
   AntiKt6PV0TrackJets_daughter__pt_NOSYS = 0;
   AntiKt6PV0TrackJets_daughter__eta_NOSYS = 0;
   AntiKt6PV0TrackJets_daughter__phi_NOSYS = 0;
   AntiKt6PV0TrackJets_daughter__trackId_NOSYS = 0;
   AntiKt8PV0TrackJets_pt_NOSYS = 0;
   AntiKt8PV0TrackJets_eta_NOSYS = 0;
   AntiKt8PV0TrackJets_phi_NOSYS = 0;
   AntiKt8PV0TrackJets_m_NOSYS = 0;
   AntiKt8PV0TrackJets_daughter__pt_NOSYS = 0;
   AntiKt8PV0TrackJets_daughter__eta_NOSYS = 0;
   AntiKt8PV0TrackJets_daughter__phi_NOSYS = 0;
   AntiKt8PV0TrackJets_daughter__trackId_NOSYS = 0;
   AntiKt10PV0TrackJets_pt_NOSYS = 0;
   AntiKt10PV0TrackJets_eta_NOSYS = 0;
   AntiKt10PV0TrackJets_phi_NOSYS = 0;
   AntiKt10PV0TrackJets_m_NOSYS = 0;
   AntiKt10PV0TrackJets_daughter__pt_NOSYS = 0;
   AntiKt10PV0TrackJets_daughter__eta_NOSYS = 0;
   AntiKt10PV0TrackJets_daughter__phi_NOSYS = 0;
   AntiKt10PV0TrackJets_daughter__trackId_NOSYS = 0;
   AntiKtVR30Rmax4Rmin02TrackJets_pt_NOSYS = 0;
   AntiKtVR30Rmax4Rmin02TrackJets_eta_NOSYS = 0;
   AntiKtVR30Rmax4Rmin02TrackJets_phi_NOSYS = 0;
   AntiKtVR30Rmax4Rmin02TrackJets_m_NOSYS = 0;
   AntiKtVR30Rmax4Rmin02TrackJets_daughter__pt_NOSYS = 0;
   AntiKtVR30Rmax4Rmin02TrackJets_daughter__eta_NOSYS = 0;
   AntiKtVR30Rmax4Rmin02TrackJets_daughter__phi_NOSYS = 0;
   AntiKtVR30Rmax4Rmin02TrackJets_daughter__trackId_NOSYS = 0;
   AnalysisElectrons_pt_EG_RESOLUTION_ALL__1down = 0;
   AnalysisElectrons_el_selected_EG_RESOLUTION_ALL__1down = 0;
   AnalysisElectrons_isIsolated_Tight_VarRad_EG_RESOLUTION_ALL__1down = 0;
   AnalysisElectrons_effSF_EG_RESOLUTION_ALL__1down = 0;
   AnalysisElectrons_effSF_ID_Tight_EG_RESOLUTION_ALL__1down = 0;
   AnalysisElectrons_effSF_Chflip_Tight_Tight_VarRad_EG_RESOLUTION_ALL__1down = 0;
   AnalysisElectrons_effSF_Isol_Tight_Tight_VarRad_EG_RESOLUTION_ALL__1down = 0;
   AnalysisElectrons_effSF_Trig_Tight_Tight_VarRad_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_EG_RESOLUTION_ALL__1down = 0;
   AnalysisElectrons_effSF_Trig_Tight_Tight_VarRad_Eff_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_EG_RESOLUTION_ALL__1down = 0;
   AnalysisElectrons_effSF_Chflip_Tight_noIso_EG_RESOLUTION_ALL__1down = 0;
   AnalysisElectrons_effSF_Trig_Tight_noIso_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_EG_RESOLUTION_ALL__1down = 0;
   AnalysisElectrons_effSF_Trig_Tight_noIso_Eff_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_EG_RESOLUTION_ALL__1down = 0;
   AnalysisMuons_mu_selected_EG_RESOLUTION_ALL__1down = 0;
   AnalysisJets_jet_selected_EG_RESOLUTION_ALL__1down = 0;
   AnalysisElectrons_pt_EG_RESOLUTION_ALL__1up = 0;
   AnalysisElectrons_el_selected_EG_RESOLUTION_ALL__1up = 0;
   AnalysisElectrons_isIsolated_Tight_VarRad_EG_RESOLUTION_ALL__1up = 0;
   AnalysisElectrons_effSF_EG_RESOLUTION_ALL__1up = 0;
   AnalysisElectrons_effSF_ID_Tight_EG_RESOLUTION_ALL__1up = 0;
   AnalysisElectrons_effSF_Chflip_Tight_Tight_VarRad_EG_RESOLUTION_ALL__1up = 0;
   AnalysisElectrons_effSF_Isol_Tight_Tight_VarRad_EG_RESOLUTION_ALL__1up = 0;
   AnalysisElectrons_effSF_Trig_Tight_Tight_VarRad_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_EG_RESOLUTION_ALL__1up = 0;
   AnalysisElectrons_effSF_Trig_Tight_Tight_VarRad_Eff_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_EG_RESOLUTION_ALL__1up = 0;
   AnalysisElectrons_effSF_Chflip_Tight_noIso_EG_RESOLUTION_ALL__1up = 0;
   AnalysisElectrons_effSF_Trig_Tight_noIso_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_EG_RESOLUTION_ALL__1up = 0;
   AnalysisElectrons_effSF_Trig_Tight_noIso_Eff_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_EG_RESOLUTION_ALL__1up = 0;
   AnalysisMuons_mu_selected_EG_RESOLUTION_ALL__1up = 0;
   AnalysisJets_jet_selected_EG_RESOLUTION_ALL__1up = 0;
   AnalysisElectrons_pt_EG_SCALE_AF2__1down = 0;
   AnalysisElectrons_el_selected_EG_SCALE_AF2__1down = 0;
   AnalysisElectrons_isIsolated_Tight_VarRad_EG_SCALE_AF2__1down = 0;
   AnalysisElectrons_effSF_EG_SCALE_AF2__1down = 0;
   AnalysisElectrons_effSF_ID_Tight_EG_SCALE_AF2__1down = 0;
   AnalysisElectrons_effSF_Chflip_Tight_Tight_VarRad_EG_SCALE_AF2__1down = 0;
   AnalysisElectrons_effSF_Isol_Tight_Tight_VarRad_EG_SCALE_AF2__1down = 0;
   AnalysisElectrons_effSF_Trig_Tight_Tight_VarRad_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_EG_SCALE_AF2__1down = 0;
   AnalysisElectrons_effSF_Trig_Tight_Tight_VarRad_Eff_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_EG_SCALE_AF2__1down = 0;
   AnalysisElectrons_effSF_Chflip_Tight_noIso_EG_SCALE_AF2__1down = 0;
   AnalysisElectrons_effSF_Trig_Tight_noIso_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_EG_SCALE_AF2__1down = 0;
   AnalysisElectrons_effSF_Trig_Tight_noIso_Eff_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_EG_SCALE_AF2__1down = 0;
   AnalysisMuons_mu_selected_EG_SCALE_AF2__1down = 0;
   AnalysisJets_jet_selected_EG_SCALE_AF2__1down = 0;
   AnalysisElectrons_pt_EG_SCALE_AF2__1up = 0;
   AnalysisElectrons_el_selected_EG_SCALE_AF2__1up = 0;
   AnalysisElectrons_isIsolated_Tight_VarRad_EG_SCALE_AF2__1up = 0;
   AnalysisElectrons_effSF_EG_SCALE_AF2__1up = 0;
   AnalysisElectrons_effSF_ID_Tight_EG_SCALE_AF2__1up = 0;
   AnalysisElectrons_effSF_Chflip_Tight_Tight_VarRad_EG_SCALE_AF2__1up = 0;
   AnalysisElectrons_effSF_Isol_Tight_Tight_VarRad_EG_SCALE_AF2__1up = 0;
   AnalysisElectrons_effSF_Trig_Tight_Tight_VarRad_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_EG_SCALE_AF2__1up = 0;
   AnalysisElectrons_effSF_Trig_Tight_Tight_VarRad_Eff_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_EG_SCALE_AF2__1up = 0;
   AnalysisElectrons_effSF_Chflip_Tight_noIso_EG_SCALE_AF2__1up = 0;
   AnalysisElectrons_effSF_Trig_Tight_noIso_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_EG_SCALE_AF2__1up = 0;
   AnalysisElectrons_effSF_Trig_Tight_noIso_Eff_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_EG_SCALE_AF2__1up = 0;
   AnalysisMuons_mu_selected_EG_SCALE_AF2__1up = 0;
   AnalysisJets_jet_selected_EG_SCALE_AF2__1up = 0;
   AnalysisElectrons_pt_EG_SCALE_ALL__1down = 0;
   AnalysisElectrons_el_selected_EG_SCALE_ALL__1down = 0;
   AnalysisElectrons_isIsolated_Tight_VarRad_EG_SCALE_ALL__1down = 0;
   AnalysisElectrons_effSF_EG_SCALE_ALL__1down = 0;
   AnalysisElectrons_effSF_ID_Tight_EG_SCALE_ALL__1down = 0;
   AnalysisElectrons_effSF_Chflip_Tight_Tight_VarRad_EG_SCALE_ALL__1down = 0;
   AnalysisElectrons_effSF_Isol_Tight_Tight_VarRad_EG_SCALE_ALL__1down = 0;
   AnalysisElectrons_effSF_Trig_Tight_Tight_VarRad_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_EG_SCALE_ALL__1down = 0;
   AnalysisElectrons_effSF_Trig_Tight_Tight_VarRad_Eff_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_EG_SCALE_ALL__1down = 0;
   AnalysisElectrons_effSF_Chflip_Tight_noIso_EG_SCALE_ALL__1down = 0;
   AnalysisElectrons_effSF_Trig_Tight_noIso_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_EG_SCALE_ALL__1down = 0;
   AnalysisElectrons_effSF_Trig_Tight_noIso_Eff_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_EG_SCALE_ALL__1down = 0;
   AnalysisMuons_mu_selected_EG_SCALE_ALL__1down = 0;
   AnalysisJets_jet_selected_EG_SCALE_ALL__1down = 0;
   AnalysisElectrons_pt_EG_SCALE_ALL__1up = 0;
   AnalysisElectrons_el_selected_EG_SCALE_ALL__1up = 0;
   AnalysisElectrons_isIsolated_Tight_VarRad_EG_SCALE_ALL__1up = 0;
   AnalysisElectrons_effSF_EG_SCALE_ALL__1up = 0;
   AnalysisElectrons_effSF_ID_Tight_EG_SCALE_ALL__1up = 0;
   AnalysisElectrons_effSF_Chflip_Tight_Tight_VarRad_EG_SCALE_ALL__1up = 0;
   AnalysisElectrons_effSF_Isol_Tight_Tight_VarRad_EG_SCALE_ALL__1up = 0;
   AnalysisElectrons_effSF_Trig_Tight_Tight_VarRad_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_EG_SCALE_ALL__1up = 0;
   AnalysisElectrons_effSF_Trig_Tight_Tight_VarRad_Eff_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_EG_SCALE_ALL__1up = 0;
   AnalysisElectrons_effSF_Chflip_Tight_noIso_EG_SCALE_ALL__1up = 0;
   AnalysisElectrons_effSF_Trig_Tight_noIso_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_EG_SCALE_ALL__1up = 0;
   AnalysisElectrons_effSF_Trig_Tight_noIso_Eff_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_EG_SCALE_ALL__1up = 0;
   AnalysisMuons_mu_selected_EG_SCALE_ALL__1up = 0;
   AnalysisJets_jet_selected_EG_SCALE_ALL__1up = 0;
   AnalysisElectrons_effSF_Chflip_Tight_Tight_VarRad_EL_CHARGEID_STAT__1down = 0;
   AnalysisElectrons_effSF_Chflip_Tight_noIso_EL_CHARGEID_STAT__1down = 0;
   AnalysisElectrons_effSF_Chflip_Tight_Tight_VarRad_EL_CHARGEID_STAT__1up = 0;
   AnalysisElectrons_effSF_Chflip_Tight_noIso_EL_CHARGEID_STAT__1up = 0;
   AnalysisElectrons_effSF_Chflip_Tight_Tight_VarRad_EL_CHARGEID_SYStotal__1down = 0;
   AnalysisElectrons_effSF_Chflip_Tight_noIso_EL_CHARGEID_SYStotal__1down = 0;
   AnalysisElectrons_effSF_Chflip_Tight_Tight_VarRad_EL_CHARGEID_SYStotal__1up = 0;
   AnalysisElectrons_effSF_Chflip_Tight_noIso_EL_CHARGEID_SYStotal__1up = 0;
   AnalysisElectrons_effSF_ID_Tight_EL_EFF_ID_TOTAL_1NPCOR_PLUS_UNCOR__1down = 0;
   AnalysisElectrons_effSF_ID_Tight_EL_EFF_ID_TOTAL_1NPCOR_PLUS_UNCOR__1up = 0;
   AnalysisElectrons_effSF_Isol_Tight_Tight_VarRad_EL_EFF_Iso_TOTAL_1NPCOR_PLUS_UNCOR__1down = 0;
   AnalysisElectrons_effSF_Isol_Tight_Tight_VarRad_EL_EFF_Iso_TOTAL_1NPCOR_PLUS_UNCOR__1up = 0;
   AnalysisElectrons_effSF_EL_EFF_Reco_TOTAL_1NPCOR_PLUS_UNCOR__1down = 0;
   AnalysisElectrons_effSF_EL_EFF_Reco_TOTAL_1NPCOR_PLUS_UNCOR__1up = 0;
   AnalysisElectrons_effSF_Trig_Tight_Tight_VarRad_Eff_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_EL_EFF_TriggerEff_TOTAL_1NPCOR_PLUS_UNCOR__1down = 0;
   AnalysisElectrons_effSF_Trig_Tight_noIso_Eff_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_EL_EFF_TriggerEff_TOTAL_1NPCOR_PLUS_UNCOR__1down = 0;
   AnalysisElectrons_effSF_Trig_Tight_Tight_VarRad_Eff_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_EL_EFF_TriggerEff_TOTAL_1NPCOR_PLUS_UNCOR__1up = 0;
   AnalysisElectrons_effSF_Trig_Tight_noIso_Eff_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_EL_EFF_TriggerEff_TOTAL_1NPCOR_PLUS_UNCOR__1up = 0;
   AnalysisElectrons_effSF_Trig_Tight_Tight_VarRad_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_EL_EFF_Trigger_TOTAL_1NPCOR_PLUS_UNCOR__1down = 0;
   AnalysisElectrons_effSF_Trig_Tight_noIso_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_EL_EFF_Trigger_TOTAL_1NPCOR_PLUS_UNCOR__1down = 0;
   AnalysisElectrons_effSF_Trig_Tight_Tight_VarRad_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_EL_EFF_Trigger_TOTAL_1NPCOR_PLUS_UNCOR__1up = 0;
   AnalysisElectrons_effSF_Trig_Tight_noIso_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_EL_EFF_Trigger_TOTAL_1NPCOR_PLUS_UNCOR__1up = 0;
   AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_FT_EFF_Eigen_B_0__1down = 0;
   AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_FT_EFF_Eigen_B_0__1up = 0;
   AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_FT_EFF_Eigen_B_1__1down = 0;
   AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_FT_EFF_Eigen_B_1__1up = 0;
   AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_FT_EFF_Eigen_B_2__1down = 0;
   AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_FT_EFF_Eigen_B_2__1up = 0;
   AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_FT_EFF_Eigen_B_3__1down = 0;
   AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_FT_EFF_Eigen_B_3__1up = 0;
   AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_FT_EFF_Eigen_B_4__1down = 0;
   AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_FT_EFF_Eigen_B_4__1up = 0;
   AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_FT_EFF_Eigen_B_5__1down = 0;
   AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_FT_EFF_Eigen_B_5__1up = 0;
   AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_FT_EFF_Eigen_B_6__1down = 0;
   AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_FT_EFF_Eigen_B_6__1up = 0;
   AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_FT_EFF_Eigen_B_7__1down = 0;
   AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_FT_EFF_Eigen_B_7__1up = 0;
   AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_FT_EFF_Eigen_B_8__1down = 0;
   AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_FT_EFF_Eigen_B_8__1up = 0;
   AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_FT_EFF_Eigen_C_0__1down = 0;
   AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_FT_EFF_Eigen_C_0__1up = 0;
   AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_FT_EFF_Eigen_C_1__1down = 0;
   AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_FT_EFF_Eigen_C_1__1up = 0;
   AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_FT_EFF_Eigen_C_2__1down = 0;
   AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_FT_EFF_Eigen_C_2__1up = 0;
   AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_FT_EFF_Eigen_C_3__1down = 0;
   AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_FT_EFF_Eigen_C_3__1up = 0;
   AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_FT_EFF_Eigen_Light_0__1down = 0;
   AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_FT_EFF_Eigen_Light_0__1up = 0;
   AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_FT_EFF_Eigen_Light_1__1down = 0;
   AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_FT_EFF_Eigen_Light_1__1up = 0;
   AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_FT_EFF_Eigen_Light_2__1down = 0;
   AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_FT_EFF_Eigen_Light_2__1up = 0;
   AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_FT_EFF_Eigen_Light_3__1down = 0;
   AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_FT_EFF_Eigen_Light_3__1up = 0;
   AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_FT_EFF_Eigen_Light_4__1down = 0;
   AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_FT_EFF_Eigen_Light_4__1up = 0;
   AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_FT_EFF_extrapolation__1down = 0;
   AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_FT_EFF_extrapolation__1up = 0;
   AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_FT_EFF_extrapolation_from_charm__1down = 0;
   AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_FT_EFF_extrapolation_from_charm__1up = 0;
   AnalysisElectrons_el_selected_JET_EffectiveNP_1__1up = 0;
   AnalysisMuons_mu_selected_JET_EffectiveNP_1__1up = 0;
   AnalysisJets_pt_JET_EffectiveNP_1__1up = 0;
   AnalysisJets_jet_selected_JET_EffectiveNP_1__1up = 0;
   AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_JET_EffectiveNP_1__1up = 0;
   AnalysisElectrons_el_selected_JET_EffectiveNP_1__1down = 0;
   AnalysisMuons_mu_selected_JET_EffectiveNP_1__1down = 0;
   AnalysisJets_pt_JET_EffectiveNP_1__1down = 0;
   AnalysisJets_jet_selected_JET_EffectiveNP_1__1down = 0;
   AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_JET_EffectiveNP_1__1down = 0;
   AnalysisElectrons_el_selected_JET_EffectiveNP_2__1up = 0;
   AnalysisMuons_mu_selected_JET_EffectiveNP_2__1up = 0;
   AnalysisJets_pt_JET_EffectiveNP_2__1up = 0;
   AnalysisJets_jet_selected_JET_EffectiveNP_2__1up = 0;
   AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_JET_EffectiveNP_2__1up = 0;
   AnalysisElectrons_el_selected_JET_EffectiveNP_2__1down = 0;
   AnalysisMuons_mu_selected_JET_EffectiveNP_2__1down = 0;
   AnalysisJets_pt_JET_EffectiveNP_2__1down = 0;
   AnalysisJets_jet_selected_JET_EffectiveNP_2__1down = 0;
   AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_JET_EffectiveNP_2__1down = 0;
   AnalysisElectrons_el_selected_JET_EffectiveNP_3__1up = 0;
   AnalysisMuons_mu_selected_JET_EffectiveNP_3__1up = 0;
   AnalysisJets_pt_JET_EffectiveNP_3__1up = 0;
   AnalysisJets_jet_selected_JET_EffectiveNP_3__1up = 0;
   AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_JET_EffectiveNP_3__1up = 0;
   AnalysisElectrons_el_selected_JET_EffectiveNP_3__1down = 0;
   AnalysisMuons_mu_selected_JET_EffectiveNP_3__1down = 0;
   AnalysisJets_pt_JET_EffectiveNP_3__1down = 0;
   AnalysisJets_jet_selected_JET_EffectiveNP_3__1down = 0;
   AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_JET_EffectiveNP_3__1down = 0;
   AnalysisElectrons_el_selected_JET_EffectiveNP_4__1up = 0;
   AnalysisMuons_mu_selected_JET_EffectiveNP_4__1up = 0;
   AnalysisJets_pt_JET_EffectiveNP_4__1up = 0;
   AnalysisJets_jet_selected_JET_EffectiveNP_4__1up = 0;
   AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_JET_EffectiveNP_4__1up = 0;
   AnalysisElectrons_el_selected_JET_EffectiveNP_4__1down = 0;
   AnalysisMuons_mu_selected_JET_EffectiveNP_4__1down = 0;
   AnalysisJets_pt_JET_EffectiveNP_4__1down = 0;
   AnalysisJets_jet_selected_JET_EffectiveNP_4__1down = 0;
   AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_JET_EffectiveNP_4__1down = 0;
   AnalysisElectrons_el_selected_JET_EffectiveNP_5__1up = 0;
   AnalysisMuons_mu_selected_JET_EffectiveNP_5__1up = 0;
   AnalysisJets_pt_JET_EffectiveNP_5__1up = 0;
   AnalysisJets_jet_selected_JET_EffectiveNP_5__1up = 0;
   AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_JET_EffectiveNP_5__1up = 0;
   AnalysisElectrons_el_selected_JET_EffectiveNP_5__1down = 0;
   AnalysisMuons_mu_selected_JET_EffectiveNP_5__1down = 0;
   AnalysisJets_pt_JET_EffectiveNP_5__1down = 0;
   AnalysisJets_jet_selected_JET_EffectiveNP_5__1down = 0;
   AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_JET_EffectiveNP_5__1down = 0;
   AnalysisElectrons_el_selected_JET_EffectiveNP_6__1up = 0;
   AnalysisMuons_mu_selected_JET_EffectiveNP_6__1up = 0;
   AnalysisJets_pt_JET_EffectiveNP_6__1up = 0;
   AnalysisJets_jet_selected_JET_EffectiveNP_6__1up = 0;
   AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_JET_EffectiveNP_6__1up = 0;
   AnalysisElectrons_el_selected_JET_EffectiveNP_6__1down = 0;
   AnalysisMuons_mu_selected_JET_EffectiveNP_6__1down = 0;
   AnalysisJets_pt_JET_EffectiveNP_6__1down = 0;
   AnalysisJets_jet_selected_JET_EffectiveNP_6__1down = 0;
   AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_JET_EffectiveNP_6__1down = 0;
   AnalysisElectrons_el_selected_JET_EffectiveNP_7__1up = 0;
   AnalysisMuons_mu_selected_JET_EffectiveNP_7__1up = 0;
   AnalysisJets_pt_JET_EffectiveNP_7__1up = 0;
   AnalysisJets_jet_selected_JET_EffectiveNP_7__1up = 0;
   AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_JET_EffectiveNP_7__1up = 0;
   AnalysisElectrons_el_selected_JET_EffectiveNP_7__1down = 0;
   AnalysisMuons_mu_selected_JET_EffectiveNP_7__1down = 0;
   AnalysisJets_pt_JET_EffectiveNP_7__1down = 0;
   AnalysisJets_jet_selected_JET_EffectiveNP_7__1down = 0;
   AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_JET_EffectiveNP_7__1down = 0;
   AnalysisElectrons_el_selected_JET_EffectiveNP_8restTerm__1up = 0;
   AnalysisMuons_mu_selected_JET_EffectiveNP_8restTerm__1up = 0;
   AnalysisJets_pt_JET_EffectiveNP_8restTerm__1up = 0;
   AnalysisJets_jet_selected_JET_EffectiveNP_8restTerm__1up = 0;
   AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_JET_EffectiveNP_8restTerm__1up = 0;
   AnalysisElectrons_el_selected_JET_EffectiveNP_8restTerm__1down = 0;
   AnalysisMuons_mu_selected_JET_EffectiveNP_8restTerm__1down = 0;
   AnalysisJets_pt_JET_EffectiveNP_8restTerm__1down = 0;
   AnalysisJets_jet_selected_JET_EffectiveNP_8restTerm__1down = 0;
   AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_JET_EffectiveNP_8restTerm__1down = 0;
   AnalysisElectrons_el_selected_JET_EtaIntercalibration_Modelling__1up = 0;
   AnalysisMuons_mu_selected_JET_EtaIntercalibration_Modelling__1up = 0;
   AnalysisJets_pt_JET_EtaIntercalibration_Modelling__1up = 0;
   AnalysisJets_jet_selected_JET_EtaIntercalibration_Modelling__1up = 0;
   AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_JET_EtaIntercalibration_Modelling__1up = 0;
   AnalysisElectrons_el_selected_JET_EtaIntercalibration_Modelling__1down = 0;
   AnalysisMuons_mu_selected_JET_EtaIntercalibration_Modelling__1down = 0;
   AnalysisJets_pt_JET_EtaIntercalibration_Modelling__1down = 0;
   AnalysisJets_jet_selected_JET_EtaIntercalibration_Modelling__1down = 0;
   AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_JET_EtaIntercalibration_Modelling__1down = 0;
   AnalysisElectrons_el_selected_JET_EtaIntercalibration_NonClosure_highE__1up = 0;
   AnalysisMuons_mu_selected_JET_EtaIntercalibration_NonClosure_highE__1up = 0;
   AnalysisJets_pt_JET_EtaIntercalibration_NonClosure_highE__1up = 0;
   AnalysisJets_jet_selected_JET_EtaIntercalibration_NonClosure_highE__1up = 0;
   AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_JET_EtaIntercalibration_NonClosure_highE__1up = 0;
   AnalysisElectrons_el_selected_JET_EtaIntercalibration_NonClosure_highE__1down = 0;
   AnalysisMuons_mu_selected_JET_EtaIntercalibration_NonClosure_highE__1down = 0;
   AnalysisJets_pt_JET_EtaIntercalibration_NonClosure_highE__1down = 0;
   AnalysisJets_jet_selected_JET_EtaIntercalibration_NonClosure_highE__1down = 0;
   AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_JET_EtaIntercalibration_NonClosure_highE__1down = 0;
   AnalysisElectrons_el_selected_JET_EtaIntercalibration_NonClosure_negEta__1up = 0;
   AnalysisMuons_mu_selected_JET_EtaIntercalibration_NonClosure_negEta__1up = 0;
   AnalysisJets_pt_JET_EtaIntercalibration_NonClosure_negEta__1up = 0;
   AnalysisJets_jet_selected_JET_EtaIntercalibration_NonClosure_negEta__1up = 0;
   AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_JET_EtaIntercalibration_NonClosure_negEta__1up = 0;
   AnalysisElectrons_el_selected_JET_EtaIntercalibration_NonClosure_negEta__1down = 0;
   AnalysisMuons_mu_selected_JET_EtaIntercalibration_NonClosure_negEta__1down = 0;
   AnalysisJets_pt_JET_EtaIntercalibration_NonClosure_negEta__1down = 0;
   AnalysisJets_jet_selected_JET_EtaIntercalibration_NonClosure_negEta__1down = 0;
   AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_JET_EtaIntercalibration_NonClosure_negEta__1down = 0;
   AnalysisElectrons_el_selected_JET_EtaIntercalibration_NonClosure_posEta__1up = 0;
   AnalysisMuons_mu_selected_JET_EtaIntercalibration_NonClosure_posEta__1up = 0;
   AnalysisJets_pt_JET_EtaIntercalibration_NonClosure_posEta__1up = 0;
   AnalysisJets_jet_selected_JET_EtaIntercalibration_NonClosure_posEta__1up = 0;
   AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_JET_EtaIntercalibration_NonClosure_posEta__1up = 0;
   AnalysisElectrons_el_selected_JET_EtaIntercalibration_NonClosure_posEta__1down = 0;
   AnalysisMuons_mu_selected_JET_EtaIntercalibration_NonClosure_posEta__1down = 0;
   AnalysisJets_pt_JET_EtaIntercalibration_NonClosure_posEta__1down = 0;
   AnalysisJets_jet_selected_JET_EtaIntercalibration_NonClosure_posEta__1down = 0;
   AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_JET_EtaIntercalibration_NonClosure_posEta__1down = 0;
   AnalysisElectrons_el_selected_JET_EtaIntercalibration_TotalStat__1up = 0;
   AnalysisMuons_mu_selected_JET_EtaIntercalibration_TotalStat__1up = 0;
   AnalysisJets_pt_JET_EtaIntercalibration_TotalStat__1up = 0;
   AnalysisJets_jet_selected_JET_EtaIntercalibration_TotalStat__1up = 0;
   AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_JET_EtaIntercalibration_TotalStat__1up = 0;
   AnalysisElectrons_el_selected_JET_EtaIntercalibration_TotalStat__1down = 0;
   AnalysisMuons_mu_selected_JET_EtaIntercalibration_TotalStat__1down = 0;
   AnalysisJets_pt_JET_EtaIntercalibration_TotalStat__1down = 0;
   AnalysisJets_jet_selected_JET_EtaIntercalibration_TotalStat__1down = 0;
   AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_JET_EtaIntercalibration_TotalStat__1down = 0;
   AnalysisElectrons_el_selected_JET_Flavor_Composition__1up = 0;
   AnalysisMuons_mu_selected_JET_Flavor_Composition__1up = 0;
   AnalysisJets_pt_JET_Flavor_Composition__1up = 0;
   AnalysisJets_jet_selected_JET_Flavor_Composition__1up = 0;
   AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_JET_Flavor_Composition__1up = 0;
   AnalysisElectrons_el_selected_JET_Flavor_Composition__1down = 0;
   AnalysisMuons_mu_selected_JET_Flavor_Composition__1down = 0;
   AnalysisJets_pt_JET_Flavor_Composition__1down = 0;
   AnalysisJets_jet_selected_JET_Flavor_Composition__1down = 0;
   AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_JET_Flavor_Composition__1down = 0;
   AnalysisElectrons_el_selected_JET_Flavor_Response__1up = 0;
   AnalysisMuons_mu_selected_JET_Flavor_Response__1up = 0;
   AnalysisJets_pt_JET_Flavor_Response__1up = 0;
   AnalysisJets_jet_selected_JET_Flavor_Response__1up = 0;
   AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_JET_Flavor_Response__1up = 0;
   AnalysisElectrons_el_selected_JET_Flavor_Response__1down = 0;
   AnalysisMuons_mu_selected_JET_Flavor_Response__1down = 0;
   AnalysisJets_pt_JET_Flavor_Response__1down = 0;
   AnalysisJets_jet_selected_JET_Flavor_Response__1down = 0;
   AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_JET_Flavor_Response__1down = 0;
   AnalysisElectrons_el_selected_JET_JER_DataVsMC_MC16__1up = 0;
   AnalysisMuons_mu_selected_JET_JER_DataVsMC_MC16__1up = 0;
   AnalysisJets_pt_JET_JER_DataVsMC_MC16__1up = 0;
   AnalysisJets_jet_selected_JET_JER_DataVsMC_MC16__1up = 0;
   AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_JET_JER_DataVsMC_MC16__1up = 0;
   AnalysisElectrons_el_selected_JET_JER_DataVsMC_MC16__1down = 0;
   AnalysisMuons_mu_selected_JET_JER_DataVsMC_MC16__1down = 0;
   AnalysisJets_pt_JET_JER_DataVsMC_MC16__1down = 0;
   AnalysisJets_jet_selected_JET_JER_DataVsMC_MC16__1down = 0;
   AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_JET_JER_DataVsMC_MC16__1down = 0;
   AnalysisElectrons_el_selected_JET_JER_EffectiveNP_1__1up = 0;
   AnalysisMuons_mu_selected_JET_JER_EffectiveNP_1__1up = 0;
   AnalysisJets_pt_JET_JER_EffectiveNP_1__1up = 0;
   AnalysisJets_jet_selected_JET_JER_EffectiveNP_1__1up = 0;
   AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_JET_JER_EffectiveNP_1__1up = 0;
   AnalysisElectrons_el_selected_JET_JER_EffectiveNP_1__1down = 0;
   AnalysisMuons_mu_selected_JET_JER_EffectiveNP_1__1down = 0;
   AnalysisJets_pt_JET_JER_EffectiveNP_1__1down = 0;
   AnalysisJets_jet_selected_JET_JER_EffectiveNP_1__1down = 0;
   AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_JET_JER_EffectiveNP_1__1down = 0;
   AnalysisElectrons_el_selected_JET_JER_EffectiveNP_2__1up = 0;
   AnalysisMuons_mu_selected_JET_JER_EffectiveNP_2__1up = 0;
   AnalysisJets_pt_JET_JER_EffectiveNP_2__1up = 0;
   AnalysisJets_jet_selected_JET_JER_EffectiveNP_2__1up = 0;
   AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_JET_JER_EffectiveNP_2__1up = 0;
   AnalysisElectrons_el_selected_JET_JER_EffectiveNP_2__1down = 0;
   AnalysisMuons_mu_selected_JET_JER_EffectiveNP_2__1down = 0;
   AnalysisJets_pt_JET_JER_EffectiveNP_2__1down = 0;
   AnalysisJets_jet_selected_JET_JER_EffectiveNP_2__1down = 0;
   AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_JET_JER_EffectiveNP_2__1down = 0;
   AnalysisElectrons_el_selected_JET_JER_EffectiveNP_3__1up = 0;
   AnalysisMuons_mu_selected_JET_JER_EffectiveNP_3__1up = 0;
   AnalysisJets_pt_JET_JER_EffectiveNP_3__1up = 0;
   AnalysisJets_jet_selected_JET_JER_EffectiveNP_3__1up = 0;
   AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_JET_JER_EffectiveNP_3__1up = 0;
   AnalysisElectrons_el_selected_JET_JER_EffectiveNP_3__1down = 0;
   AnalysisMuons_mu_selected_JET_JER_EffectiveNP_3__1down = 0;
   AnalysisJets_pt_JET_JER_EffectiveNP_3__1down = 0;
   AnalysisJets_jet_selected_JET_JER_EffectiveNP_3__1down = 0;
   AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_JET_JER_EffectiveNP_3__1down = 0;
   AnalysisElectrons_el_selected_JET_JER_EffectiveNP_4__1up = 0;
   AnalysisMuons_mu_selected_JET_JER_EffectiveNP_4__1up = 0;
   AnalysisJets_pt_JET_JER_EffectiveNP_4__1up = 0;
   AnalysisJets_jet_selected_JET_JER_EffectiveNP_4__1up = 0;
   AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_JET_JER_EffectiveNP_4__1up = 0;
   AnalysisElectrons_el_selected_JET_JER_EffectiveNP_4__1down = 0;
   AnalysisMuons_mu_selected_JET_JER_EffectiveNP_4__1down = 0;
   AnalysisJets_pt_JET_JER_EffectiveNP_4__1down = 0;
   AnalysisJets_jet_selected_JET_JER_EffectiveNP_4__1down = 0;
   AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_JET_JER_EffectiveNP_4__1down = 0;
   AnalysisElectrons_el_selected_JET_JER_EffectiveNP_5__1up = 0;
   AnalysisMuons_mu_selected_JET_JER_EffectiveNP_5__1up = 0;
   AnalysisJets_pt_JET_JER_EffectiveNP_5__1up = 0;
   AnalysisJets_jet_selected_JET_JER_EffectiveNP_5__1up = 0;
   AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_JET_JER_EffectiveNP_5__1up = 0;
   AnalysisElectrons_el_selected_JET_JER_EffectiveNP_5__1down = 0;
   AnalysisMuons_mu_selected_JET_JER_EffectiveNP_5__1down = 0;
   AnalysisJets_pt_JET_JER_EffectiveNP_5__1down = 0;
   AnalysisJets_jet_selected_JET_JER_EffectiveNP_5__1down = 0;
   AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_JET_JER_EffectiveNP_5__1down = 0;
   AnalysisElectrons_el_selected_JET_JER_EffectiveNP_6__1up = 0;
   AnalysisMuons_mu_selected_JET_JER_EffectiveNP_6__1up = 0;
   AnalysisJets_pt_JET_JER_EffectiveNP_6__1up = 0;
   AnalysisJets_jet_selected_JET_JER_EffectiveNP_6__1up = 0;
   AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_JET_JER_EffectiveNP_6__1up = 0;
   AnalysisElectrons_el_selected_JET_JER_EffectiveNP_6__1down = 0;
   AnalysisMuons_mu_selected_JET_JER_EffectiveNP_6__1down = 0;
   AnalysisJets_pt_JET_JER_EffectiveNP_6__1down = 0;
   AnalysisJets_jet_selected_JET_JER_EffectiveNP_6__1down = 0;
   AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_JET_JER_EffectiveNP_6__1down = 0;
   AnalysisElectrons_el_selected_JET_JER_EffectiveNP_7restTerm__1up = 0;
   AnalysisMuons_mu_selected_JET_JER_EffectiveNP_7restTerm__1up = 0;
   AnalysisJets_pt_JET_JER_EffectiveNP_7restTerm__1up = 0;
   AnalysisJets_jet_selected_JET_JER_EffectiveNP_7restTerm__1up = 0;
   AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_JET_JER_EffectiveNP_7restTerm__1up = 0;
   AnalysisElectrons_el_selected_JET_JER_EffectiveNP_7restTerm__1down = 0;
   AnalysisMuons_mu_selected_JET_JER_EffectiveNP_7restTerm__1down = 0;
   AnalysisJets_pt_JET_JER_EffectiveNP_7restTerm__1down = 0;
   AnalysisJets_jet_selected_JET_JER_EffectiveNP_7restTerm__1down = 0;
   AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_JET_JER_EffectiveNP_7restTerm__1down = 0;
   AnalysisElectrons_el_selected_JET_Pileup_OffsetMu__1up = 0;
   AnalysisMuons_mu_selected_JET_Pileup_OffsetMu__1up = 0;
   AnalysisJets_pt_JET_Pileup_OffsetMu__1up = 0;
   AnalysisJets_jet_selected_JET_Pileup_OffsetMu__1up = 0;
   AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_JET_Pileup_OffsetMu__1up = 0;
   AnalysisElectrons_el_selected_JET_Pileup_OffsetMu__1down = 0;
   AnalysisMuons_mu_selected_JET_Pileup_OffsetMu__1down = 0;
   AnalysisJets_pt_JET_Pileup_OffsetMu__1down = 0;
   AnalysisJets_jet_selected_JET_Pileup_OffsetMu__1down = 0;
   AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_JET_Pileup_OffsetMu__1down = 0;
   AnalysisElectrons_el_selected_JET_Pileup_OffsetNPV__1up = 0;
   AnalysisMuons_mu_selected_JET_Pileup_OffsetNPV__1up = 0;
   AnalysisJets_pt_JET_Pileup_OffsetNPV__1up = 0;
   AnalysisJets_jet_selected_JET_Pileup_OffsetNPV__1up = 0;
   AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_JET_Pileup_OffsetNPV__1up = 0;
   AnalysisElectrons_el_selected_JET_Pileup_OffsetNPV__1down = 0;
   AnalysisMuons_mu_selected_JET_Pileup_OffsetNPV__1down = 0;
   AnalysisJets_pt_JET_Pileup_OffsetNPV__1down = 0;
   AnalysisJets_jet_selected_JET_Pileup_OffsetNPV__1down = 0;
   AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_JET_Pileup_OffsetNPV__1down = 0;
   AnalysisElectrons_el_selected_JET_Pileup_PtTerm__1up = 0;
   AnalysisMuons_mu_selected_JET_Pileup_PtTerm__1up = 0;
   AnalysisJets_pt_JET_Pileup_PtTerm__1up = 0;
   AnalysisJets_jet_selected_JET_Pileup_PtTerm__1up = 0;
   AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_JET_Pileup_PtTerm__1up = 0;
   AnalysisElectrons_el_selected_JET_Pileup_PtTerm__1down = 0;
   AnalysisMuons_mu_selected_JET_Pileup_PtTerm__1down = 0;
   AnalysisJets_pt_JET_Pileup_PtTerm__1down = 0;
   AnalysisJets_jet_selected_JET_Pileup_PtTerm__1down = 0;
   AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_JET_Pileup_PtTerm__1down = 0;
   AnalysisElectrons_el_selected_JET_Pileup_RhoTopology__1up = 0;
   AnalysisMuons_mu_selected_JET_Pileup_RhoTopology__1up = 0;
   AnalysisJets_pt_JET_Pileup_RhoTopology__1up = 0;
   AnalysisJets_jet_selected_JET_Pileup_RhoTopology__1up = 0;
   AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_JET_Pileup_RhoTopology__1up = 0;
   AnalysisElectrons_el_selected_JET_Pileup_RhoTopology__1down = 0;
   AnalysisMuons_mu_selected_JET_Pileup_RhoTopology__1down = 0;
   AnalysisJets_pt_JET_Pileup_RhoTopology__1down = 0;
   AnalysisJets_jet_selected_JET_Pileup_RhoTopology__1down = 0;
   AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_JET_Pileup_RhoTopology__1down = 0;
   AnalysisElectrons_el_selected_JET_PunchThrough_MC16__1up = 0;
   AnalysisMuons_mu_selected_JET_PunchThrough_MC16__1up = 0;
   AnalysisJets_pt_JET_PunchThrough_MC16__1up = 0;
   AnalysisJets_jet_selected_JET_PunchThrough_MC16__1up = 0;
   AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_JET_PunchThrough_MC16__1up = 0;
   AnalysisElectrons_el_selected_JET_PunchThrough_MC16__1down = 0;
   AnalysisMuons_mu_selected_JET_PunchThrough_MC16__1down = 0;
   AnalysisJets_pt_JET_PunchThrough_MC16__1down = 0;
   AnalysisJets_jet_selected_JET_PunchThrough_MC16__1down = 0;
   AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_JET_PunchThrough_MC16__1down = 0;
   AnalysisElectrons_el_selected_MUON_CB__1down = 0;
   AnalysisMuons_pt_MUON_CB__1down = 0;
   AnalysisMuons_mu_selected_MUON_CB__1down = 0;
   AnalysisMuons_isQuality_Loose_MUON_CB__1down = 0;
   AnalysisMuons_isQuality_Medium_MUON_CB__1down = 0;
   AnalysisMuons_isQuality_Tight_MUON_CB__1down = 0;
   AnalysisMuons_isIsolated_PflowTight_VarRad_MUON_CB__1down = 0;
   AnalysisMuons_muon_effSF_MUON_CB__1down = 0;
   AnalysisMuons_muon_effSF_TTVA_MUON_CB__1down = 0;
   AnalysisMuons_muon_effSF_Quality_Loose_MUON_CB__1down = 0;
   AnalysisMuons_muon_effSF_Quality_Medium_MUON_CB__1down = 0;
   AnalysisMuons_muon_effSF_Quality_Tight_MUON_CB__1down = 0;
   AnalysisMuons_muon_effSF_Isol_PflowTight_VarRad_MUON_CB__1down = 0;
   AnalysisMuons_muon_effMC_Trig_Loose_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_CB__1down = 0;
   AnalysisMuons_muon_effData_Trig_Loose_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_CB__1down = 0;
   AnalysisMuons_muon_effMC_Trig_Loose_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_CB__1down = 0;
   AnalysisMuons_muon_effData_Trig_Loose_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_CB__1down = 0;
   AnalysisMuons_muon_effMC_Trig_Medium_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_CB__1down = 0;
   AnalysisMuons_muon_effData_Trig_Medium_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_CB__1down = 0;
   AnalysisMuons_muon_effMC_Trig_Medium_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_CB__1down = 0;
   AnalysisMuons_muon_effData_Trig_Medium_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_CB__1down = 0;
   AnalysisMuons_muon_effMC_Trig_Tight_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_CB__1down = 0;
   AnalysisMuons_muon_effData_Trig_Tight_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_CB__1down = 0;
   AnalysisMuons_muon_effMC_Trig_Tight_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_CB__1down = 0;
   AnalysisMuons_muon_effData_Trig_Tight_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_CB__1down = 0;
   AnalysisJets_jet_selected_MUON_CB__1down = 0;
   AnalysisElectrons_el_selected_MUON_CB__1up = 0;
   AnalysisMuons_pt_MUON_CB__1up = 0;
   AnalysisMuons_mu_selected_MUON_CB__1up = 0;
   AnalysisMuons_isQuality_Loose_MUON_CB__1up = 0;
   AnalysisMuons_isQuality_Medium_MUON_CB__1up = 0;
   AnalysisMuons_isQuality_Tight_MUON_CB__1up = 0;
   AnalysisMuons_isIsolated_PflowTight_VarRad_MUON_CB__1up = 0;
   AnalysisMuons_muon_effSF_MUON_CB__1up = 0;
   AnalysisMuons_muon_effSF_TTVA_MUON_CB__1up = 0;
   AnalysisMuons_muon_effSF_Quality_Loose_MUON_CB__1up = 0;
   AnalysisMuons_muon_effSF_Quality_Medium_MUON_CB__1up = 0;
   AnalysisMuons_muon_effSF_Quality_Tight_MUON_CB__1up = 0;
   AnalysisMuons_muon_effSF_Isol_PflowTight_VarRad_MUON_CB__1up = 0;
   AnalysisMuons_muon_effMC_Trig_Loose_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_CB__1up = 0;
   AnalysisMuons_muon_effData_Trig_Loose_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_CB__1up = 0;
   AnalysisMuons_muon_effMC_Trig_Loose_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_CB__1up = 0;
   AnalysisMuons_muon_effData_Trig_Loose_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_CB__1up = 0;
   AnalysisMuons_muon_effMC_Trig_Medium_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_CB__1up = 0;
   AnalysisMuons_muon_effData_Trig_Medium_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_CB__1up = 0;
   AnalysisMuons_muon_effMC_Trig_Medium_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_CB__1up = 0;
   AnalysisMuons_muon_effData_Trig_Medium_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_CB__1up = 0;
   AnalysisMuons_muon_effMC_Trig_Tight_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_CB__1up = 0;
   AnalysisMuons_muon_effData_Trig_Tight_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_CB__1up = 0;
   AnalysisMuons_muon_effMC_Trig_Tight_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_CB__1up = 0;
   AnalysisMuons_muon_effData_Trig_Tight_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_CB__1up = 0;
   AnalysisJets_jet_selected_MUON_CB__1up = 0;
   AnalysisMuons_muon_effSF_Isol_PflowTight_VarRad_MUON_EFF_ISO_STAT__1down = 0;
   AnalysisMuons_muon_effSF_Isol_PflowTight_VarRad_MUON_EFF_ISO_STAT__1up = 0;
   AnalysisMuons_muon_effSF_Isol_PflowTight_VarRad_MUON_EFF_ISO_SYS__1down = 0;
   AnalysisMuons_muon_effSF_Isol_PflowTight_VarRad_MUON_EFF_ISO_SYS__1up = 0;
   AnalysisMuons_muon_effSF_MUON_EFF_RECO_STAT__1down = 0;
   AnalysisMuons_muon_effSF_Quality_Loose_MUON_EFF_RECO_STAT__1down = 0;
   AnalysisMuons_muon_effSF_Quality_Medium_MUON_EFF_RECO_STAT__1down = 0;
   AnalysisMuons_muon_effSF_Quality_Tight_MUON_EFF_RECO_STAT__1down = 0;
   AnalysisMuons_muon_effSF_MUON_EFF_RECO_STAT__1up = 0;
   AnalysisMuons_muon_effSF_Quality_Loose_MUON_EFF_RECO_STAT__1up = 0;
   AnalysisMuons_muon_effSF_Quality_Medium_MUON_EFF_RECO_STAT__1up = 0;
   AnalysisMuons_muon_effSF_Quality_Tight_MUON_EFF_RECO_STAT__1up = 0;
   AnalysisMuons_muon_effSF_MUON_EFF_RECO_STAT_LOWPT__1down = 0;
   AnalysisMuons_muon_effSF_Quality_Loose_MUON_EFF_RECO_STAT_LOWPT__1down = 0;
   AnalysisMuons_muon_effSF_Quality_Medium_MUON_EFF_RECO_STAT_LOWPT__1down = 0;
   AnalysisMuons_muon_effSF_Quality_Tight_MUON_EFF_RECO_STAT_LOWPT__1down = 0;
   AnalysisMuons_muon_effSF_MUON_EFF_RECO_STAT_LOWPT__1up = 0;
   AnalysisMuons_muon_effSF_Quality_Loose_MUON_EFF_RECO_STAT_LOWPT__1up = 0;
   AnalysisMuons_muon_effSF_Quality_Medium_MUON_EFF_RECO_STAT_LOWPT__1up = 0;
   AnalysisMuons_muon_effSF_Quality_Tight_MUON_EFF_RECO_STAT_LOWPT__1up = 0;
   AnalysisMuons_muon_effSF_MUON_EFF_RECO_SYS__1down = 0;
   AnalysisMuons_muon_effSF_Quality_Loose_MUON_EFF_RECO_SYS__1down = 0;
   AnalysisMuons_muon_effSF_Quality_Medium_MUON_EFF_RECO_SYS__1down = 0;
   AnalysisMuons_muon_effSF_Quality_Tight_MUON_EFF_RECO_SYS__1down = 0;
   AnalysisMuons_muon_effSF_MUON_EFF_RECO_SYS__1up = 0;
   AnalysisMuons_muon_effSF_Quality_Loose_MUON_EFF_RECO_SYS__1up = 0;
   AnalysisMuons_muon_effSF_Quality_Medium_MUON_EFF_RECO_SYS__1up = 0;
   AnalysisMuons_muon_effSF_Quality_Tight_MUON_EFF_RECO_SYS__1up = 0;
   AnalysisMuons_muon_effSF_MUON_EFF_RECO_SYS_LOWPT__1down = 0;
   AnalysisMuons_muon_effSF_Quality_Loose_MUON_EFF_RECO_SYS_LOWPT__1down = 0;
   AnalysisMuons_muon_effSF_Quality_Medium_MUON_EFF_RECO_SYS_LOWPT__1down = 0;
   AnalysisMuons_muon_effSF_Quality_Tight_MUON_EFF_RECO_SYS_LOWPT__1down = 0;
   AnalysisMuons_muon_effSF_MUON_EFF_RECO_SYS_LOWPT__1up = 0;
   AnalysisMuons_muon_effSF_Quality_Loose_MUON_EFF_RECO_SYS_LOWPT__1up = 0;
   AnalysisMuons_muon_effSF_Quality_Medium_MUON_EFF_RECO_SYS_LOWPT__1up = 0;
   AnalysisMuons_muon_effSF_Quality_Tight_MUON_EFF_RECO_SYS_LOWPT__1up = 0;
   AnalysisMuons_muon_effSF_TTVA_MUON_EFF_TTVA_STAT__1down = 0;
   AnalysisMuons_muon_effSF_TTVA_MUON_EFF_TTVA_STAT__1up = 0;
   AnalysisMuons_muon_effSF_TTVA_MUON_EFF_TTVA_SYS__1down = 0;
   AnalysisMuons_muon_effSF_TTVA_MUON_EFF_TTVA_SYS__1up = 0;
   AnalysisMuons_muon_effMC_Trig_Loose_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_EFF_TrigStatUncertainty__1down = 0;
   AnalysisMuons_muon_effData_Trig_Loose_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_EFF_TrigStatUncertainty__1down = 0;
   AnalysisMuons_muon_effMC_Trig_Loose_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_EFF_TrigStatUncertainty__1down = 0;
   AnalysisMuons_muon_effData_Trig_Loose_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_EFF_TrigStatUncertainty__1down = 0;
   AnalysisMuons_muon_effMC_Trig_Medium_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_EFF_TrigStatUncertainty__1down = 0;
   AnalysisMuons_muon_effData_Trig_Medium_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_EFF_TrigStatUncertainty__1down = 0;
   AnalysisMuons_muon_effMC_Trig_Medium_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_EFF_TrigStatUncertainty__1down = 0;
   AnalysisMuons_muon_effData_Trig_Medium_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_EFF_TrigStatUncertainty__1down = 0;
   AnalysisMuons_muon_effMC_Trig_Tight_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_EFF_TrigStatUncertainty__1down = 0;
   AnalysisMuons_muon_effData_Trig_Tight_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_EFF_TrigStatUncertainty__1down = 0;
   AnalysisMuons_muon_effMC_Trig_Tight_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_EFF_TrigStatUncertainty__1down = 0;
   AnalysisMuons_muon_effData_Trig_Tight_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_EFF_TrigStatUncertainty__1down = 0;
   AnalysisMuons_muon_effMC_Trig_Loose_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_EFF_TrigStatUncertainty__1up = 0;
   AnalysisMuons_muon_effData_Trig_Loose_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_EFF_TrigStatUncertainty__1up = 0;
   AnalysisMuons_muon_effMC_Trig_Loose_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_EFF_TrigStatUncertainty__1up = 0;
   AnalysisMuons_muon_effData_Trig_Loose_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_EFF_TrigStatUncertainty__1up = 0;
   AnalysisMuons_muon_effMC_Trig_Medium_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_EFF_TrigStatUncertainty__1up = 0;
   AnalysisMuons_muon_effData_Trig_Medium_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_EFF_TrigStatUncertainty__1up = 0;
   AnalysisMuons_muon_effMC_Trig_Medium_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_EFF_TrigStatUncertainty__1up = 0;
   AnalysisMuons_muon_effData_Trig_Medium_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_EFF_TrigStatUncertainty__1up = 0;
   AnalysisMuons_muon_effMC_Trig_Tight_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_EFF_TrigStatUncertainty__1up = 0;
   AnalysisMuons_muon_effData_Trig_Tight_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_EFF_TrigStatUncertainty__1up = 0;
   AnalysisMuons_muon_effMC_Trig_Tight_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_EFF_TrigStatUncertainty__1up = 0;
   AnalysisMuons_muon_effData_Trig_Tight_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_EFF_TrigStatUncertainty__1up = 0;
   AnalysisMuons_muon_effMC_Trig_Loose_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_EFF_TrigSystUncertainty__1down = 0;
   AnalysisMuons_muon_effData_Trig_Loose_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_EFF_TrigSystUncertainty__1down = 0;
   AnalysisMuons_muon_effMC_Trig_Loose_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_EFF_TrigSystUncertainty__1down = 0;
   AnalysisMuons_muon_effData_Trig_Loose_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_EFF_TrigSystUncertainty__1down = 0;
   AnalysisMuons_muon_effMC_Trig_Medium_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_EFF_TrigSystUncertainty__1down = 0;
   AnalysisMuons_muon_effData_Trig_Medium_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_EFF_TrigSystUncertainty__1down = 0;
   AnalysisMuons_muon_effMC_Trig_Medium_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_EFF_TrigSystUncertainty__1down = 0;
   AnalysisMuons_muon_effData_Trig_Medium_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_EFF_TrigSystUncertainty__1down = 0;
   AnalysisMuons_muon_effMC_Trig_Tight_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_EFF_TrigSystUncertainty__1down = 0;
   AnalysisMuons_muon_effData_Trig_Tight_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_EFF_TrigSystUncertainty__1down = 0;
   AnalysisMuons_muon_effMC_Trig_Tight_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_EFF_TrigSystUncertainty__1down = 0;
   AnalysisMuons_muon_effData_Trig_Tight_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_EFF_TrigSystUncertainty__1down = 0;
   AnalysisMuons_muon_effMC_Trig_Loose_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_EFF_TrigSystUncertainty__1up = 0;
   AnalysisMuons_muon_effData_Trig_Loose_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_EFF_TrigSystUncertainty__1up = 0;
   AnalysisMuons_muon_effMC_Trig_Loose_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_EFF_TrigSystUncertainty__1up = 0;
   AnalysisMuons_muon_effData_Trig_Loose_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_EFF_TrigSystUncertainty__1up = 0;
   AnalysisMuons_muon_effMC_Trig_Medium_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_EFF_TrigSystUncertainty__1up = 0;
   AnalysisMuons_muon_effData_Trig_Medium_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_EFF_TrigSystUncertainty__1up = 0;
   AnalysisMuons_muon_effMC_Trig_Medium_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_EFF_TrigSystUncertainty__1up = 0;
   AnalysisMuons_muon_effData_Trig_Medium_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_EFF_TrigSystUncertainty__1up = 0;
   AnalysisMuons_muon_effMC_Trig_Tight_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_EFF_TrigSystUncertainty__1up = 0;
   AnalysisMuons_muon_effData_Trig_Tight_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_EFF_TrigSystUncertainty__1up = 0;
   AnalysisMuons_muon_effMC_Trig_Tight_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_EFF_TrigSystUncertainty__1up = 0;
   AnalysisMuons_muon_effData_Trig_Tight_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_EFF_TrigSystUncertainty__1up = 0;
   AnalysisElectrons_el_selected_MUON_SAGITTA_DATASTAT__1down = 0;
   AnalysisMuons_pt_MUON_SAGITTA_DATASTAT__1down = 0;
   AnalysisMuons_mu_selected_MUON_SAGITTA_DATASTAT__1down = 0;
   AnalysisMuons_isQuality_Loose_MUON_SAGITTA_DATASTAT__1down = 0;
   AnalysisMuons_isQuality_Medium_MUON_SAGITTA_DATASTAT__1down = 0;
   AnalysisMuons_isQuality_Tight_MUON_SAGITTA_DATASTAT__1down = 0;
   AnalysisMuons_isIsolated_PflowTight_VarRad_MUON_SAGITTA_DATASTAT__1down = 0;
   AnalysisMuons_muon_effSF_MUON_SAGITTA_DATASTAT__1down = 0;
   AnalysisMuons_muon_effSF_TTVA_MUON_SAGITTA_DATASTAT__1down = 0;
   AnalysisMuons_muon_effSF_Quality_Loose_MUON_SAGITTA_DATASTAT__1down = 0;
   AnalysisMuons_muon_effSF_Quality_Medium_MUON_SAGITTA_DATASTAT__1down = 0;
   AnalysisMuons_muon_effSF_Quality_Tight_MUON_SAGITTA_DATASTAT__1down = 0;
   AnalysisMuons_muon_effSF_Isol_PflowTight_VarRad_MUON_SAGITTA_DATASTAT__1down = 0;
   AnalysisMuons_muon_effMC_Trig_Loose_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_SAGITTA_DATASTAT__1down = 0;
   AnalysisMuons_muon_effData_Trig_Loose_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_SAGITTA_DATASTAT__1down = 0;
   AnalysisMuons_muon_effMC_Trig_Loose_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_SAGITTA_DATASTAT__1down = 0;
   AnalysisMuons_muon_effData_Trig_Loose_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_SAGITTA_DATASTAT__1down = 0;
   AnalysisMuons_muon_effMC_Trig_Medium_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_SAGITTA_DATASTAT__1down = 0;
   AnalysisMuons_muon_effData_Trig_Medium_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_SAGITTA_DATASTAT__1down = 0;
   AnalysisMuons_muon_effMC_Trig_Medium_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_SAGITTA_DATASTAT__1down = 0;
   AnalysisMuons_muon_effData_Trig_Medium_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_SAGITTA_DATASTAT__1down = 0;
   AnalysisMuons_muon_effMC_Trig_Tight_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_SAGITTA_DATASTAT__1down = 0;
   AnalysisMuons_muon_effData_Trig_Tight_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_SAGITTA_DATASTAT__1down = 0;
   AnalysisMuons_muon_effMC_Trig_Tight_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_SAGITTA_DATASTAT__1down = 0;
   AnalysisMuons_muon_effData_Trig_Tight_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_SAGITTA_DATASTAT__1down = 0;
   AnalysisJets_jet_selected_MUON_SAGITTA_DATASTAT__1down = 0;
   AnalysisElectrons_el_selected_MUON_SAGITTA_DATASTAT__1up = 0;
   AnalysisMuons_pt_MUON_SAGITTA_DATASTAT__1up = 0;
   AnalysisMuons_mu_selected_MUON_SAGITTA_DATASTAT__1up = 0;
   AnalysisMuons_isQuality_Loose_MUON_SAGITTA_DATASTAT__1up = 0;
   AnalysisMuons_isQuality_Medium_MUON_SAGITTA_DATASTAT__1up = 0;
   AnalysisMuons_isQuality_Tight_MUON_SAGITTA_DATASTAT__1up = 0;
   AnalysisMuons_isIsolated_PflowTight_VarRad_MUON_SAGITTA_DATASTAT__1up = 0;
   AnalysisMuons_muon_effSF_MUON_SAGITTA_DATASTAT__1up = 0;
   AnalysisMuons_muon_effSF_TTVA_MUON_SAGITTA_DATASTAT__1up = 0;
   AnalysisMuons_muon_effSF_Quality_Loose_MUON_SAGITTA_DATASTAT__1up = 0;
   AnalysisMuons_muon_effSF_Quality_Medium_MUON_SAGITTA_DATASTAT__1up = 0;
   AnalysisMuons_muon_effSF_Quality_Tight_MUON_SAGITTA_DATASTAT__1up = 0;
   AnalysisMuons_muon_effSF_Isol_PflowTight_VarRad_MUON_SAGITTA_DATASTAT__1up = 0;
   AnalysisMuons_muon_effMC_Trig_Loose_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_SAGITTA_DATASTAT__1up = 0;
   AnalysisMuons_muon_effData_Trig_Loose_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_SAGITTA_DATASTAT__1up = 0;
   AnalysisMuons_muon_effMC_Trig_Loose_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_SAGITTA_DATASTAT__1up = 0;
   AnalysisMuons_muon_effData_Trig_Loose_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_SAGITTA_DATASTAT__1up = 0;
   AnalysisMuons_muon_effMC_Trig_Medium_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_SAGITTA_DATASTAT__1up = 0;
   AnalysisMuons_muon_effData_Trig_Medium_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_SAGITTA_DATASTAT__1up = 0;
   AnalysisMuons_muon_effMC_Trig_Medium_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_SAGITTA_DATASTAT__1up = 0;
   AnalysisMuons_muon_effData_Trig_Medium_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_SAGITTA_DATASTAT__1up = 0;
   AnalysisMuons_muon_effMC_Trig_Tight_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_SAGITTA_DATASTAT__1up = 0;
   AnalysisMuons_muon_effData_Trig_Tight_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_SAGITTA_DATASTAT__1up = 0;
   AnalysisMuons_muon_effMC_Trig_Tight_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_SAGITTA_DATASTAT__1up = 0;
   AnalysisMuons_muon_effData_Trig_Tight_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_SAGITTA_DATASTAT__1up = 0;
   AnalysisJets_jet_selected_MUON_SAGITTA_DATASTAT__1up = 0;
   AnalysisElectrons_el_selected_MUON_SAGITTA_RESBIAS__1down = 0;
   AnalysisMuons_pt_MUON_SAGITTA_RESBIAS__1down = 0;
   AnalysisMuons_mu_selected_MUON_SAGITTA_RESBIAS__1down = 0;
   AnalysisMuons_isQuality_Loose_MUON_SAGITTA_RESBIAS__1down = 0;
   AnalysisMuons_isQuality_Medium_MUON_SAGITTA_RESBIAS__1down = 0;
   AnalysisMuons_isQuality_Tight_MUON_SAGITTA_RESBIAS__1down = 0;
   AnalysisMuons_isIsolated_PflowTight_VarRad_MUON_SAGITTA_RESBIAS__1down = 0;
   AnalysisMuons_muon_effSF_MUON_SAGITTA_RESBIAS__1down = 0;
   AnalysisMuons_muon_effSF_TTVA_MUON_SAGITTA_RESBIAS__1down = 0;
   AnalysisMuons_muon_effSF_Quality_Loose_MUON_SAGITTA_RESBIAS__1down = 0;
   AnalysisMuons_muon_effSF_Quality_Medium_MUON_SAGITTA_RESBIAS__1down = 0;
   AnalysisMuons_muon_effSF_Quality_Tight_MUON_SAGITTA_RESBIAS__1down = 0;
   AnalysisMuons_muon_effSF_Isol_PflowTight_VarRad_MUON_SAGITTA_RESBIAS__1down = 0;
   AnalysisMuons_muon_effMC_Trig_Loose_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_SAGITTA_RESBIAS__1down = 0;
   AnalysisMuons_muon_effData_Trig_Loose_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_SAGITTA_RESBIAS__1down = 0;
   AnalysisMuons_muon_effMC_Trig_Loose_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_SAGITTA_RESBIAS__1down = 0;
   AnalysisMuons_muon_effData_Trig_Loose_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_SAGITTA_RESBIAS__1down = 0;
   AnalysisMuons_muon_effMC_Trig_Medium_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_SAGITTA_RESBIAS__1down = 0;
   AnalysisMuons_muon_effData_Trig_Medium_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_SAGITTA_RESBIAS__1down = 0;
   AnalysisMuons_muon_effMC_Trig_Medium_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_SAGITTA_RESBIAS__1down = 0;
   AnalysisMuons_muon_effData_Trig_Medium_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_SAGITTA_RESBIAS__1down = 0;
   AnalysisMuons_muon_effMC_Trig_Tight_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_SAGITTA_RESBIAS__1down = 0;
   AnalysisMuons_muon_effData_Trig_Tight_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_SAGITTA_RESBIAS__1down = 0;
   AnalysisMuons_muon_effMC_Trig_Tight_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_SAGITTA_RESBIAS__1down = 0;
   AnalysisMuons_muon_effData_Trig_Tight_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_SAGITTA_RESBIAS__1down = 0;
   AnalysisJets_jet_selected_MUON_SAGITTA_RESBIAS__1down = 0;
   AnalysisElectrons_el_selected_MUON_SAGITTA_RESBIAS__1up = 0;
   AnalysisMuons_pt_MUON_SAGITTA_RESBIAS__1up = 0;
   AnalysisMuons_mu_selected_MUON_SAGITTA_RESBIAS__1up = 0;
   AnalysisMuons_isQuality_Loose_MUON_SAGITTA_RESBIAS__1up = 0;
   AnalysisMuons_isQuality_Medium_MUON_SAGITTA_RESBIAS__1up = 0;
   AnalysisMuons_isQuality_Tight_MUON_SAGITTA_RESBIAS__1up = 0;
   AnalysisMuons_isIsolated_PflowTight_VarRad_MUON_SAGITTA_RESBIAS__1up = 0;
   AnalysisMuons_muon_effSF_MUON_SAGITTA_RESBIAS__1up = 0;
   AnalysisMuons_muon_effSF_TTVA_MUON_SAGITTA_RESBIAS__1up = 0;
   AnalysisMuons_muon_effSF_Quality_Loose_MUON_SAGITTA_RESBIAS__1up = 0;
   AnalysisMuons_muon_effSF_Quality_Medium_MUON_SAGITTA_RESBIAS__1up = 0;
   AnalysisMuons_muon_effSF_Quality_Tight_MUON_SAGITTA_RESBIAS__1up = 0;
   AnalysisMuons_muon_effSF_Isol_PflowTight_VarRad_MUON_SAGITTA_RESBIAS__1up = 0;
   AnalysisMuons_muon_effMC_Trig_Loose_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_SAGITTA_RESBIAS__1up = 0;
   AnalysisMuons_muon_effData_Trig_Loose_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_SAGITTA_RESBIAS__1up = 0;
   AnalysisMuons_muon_effMC_Trig_Loose_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_SAGITTA_RESBIAS__1up = 0;
   AnalysisMuons_muon_effData_Trig_Loose_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_SAGITTA_RESBIAS__1up = 0;
   AnalysisMuons_muon_effMC_Trig_Medium_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_SAGITTA_RESBIAS__1up = 0;
   AnalysisMuons_muon_effData_Trig_Medium_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_SAGITTA_RESBIAS__1up = 0;
   AnalysisMuons_muon_effMC_Trig_Medium_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_SAGITTA_RESBIAS__1up = 0;
   AnalysisMuons_muon_effData_Trig_Medium_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_SAGITTA_RESBIAS__1up = 0;
   AnalysisMuons_muon_effMC_Trig_Tight_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_SAGITTA_RESBIAS__1up = 0;
   AnalysisMuons_muon_effData_Trig_Tight_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_SAGITTA_RESBIAS__1up = 0;
   AnalysisMuons_muon_effMC_Trig_Tight_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_SAGITTA_RESBIAS__1up = 0;
   AnalysisMuons_muon_effData_Trig_Tight_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_SAGITTA_RESBIAS__1up = 0;
   AnalysisJets_jet_selected_MUON_SAGITTA_RESBIAS__1up = 0;
   AnalysisElectrons_el_selected_MUON_SCALE__1down = 0;
   AnalysisMuons_pt_MUON_SCALE__1down = 0;
   AnalysisMuons_mu_selected_MUON_SCALE__1down = 0;
   AnalysisMuons_isQuality_Loose_MUON_SCALE__1down = 0;
   AnalysisMuons_isQuality_Medium_MUON_SCALE__1down = 0;
   AnalysisMuons_isQuality_Tight_MUON_SCALE__1down = 0;
   AnalysisMuons_isIsolated_PflowTight_VarRad_MUON_SCALE__1down = 0;
   AnalysisMuons_muon_effSF_MUON_SCALE__1down = 0;
   AnalysisMuons_muon_effSF_TTVA_MUON_SCALE__1down = 0;
   AnalysisMuons_muon_effSF_Quality_Loose_MUON_SCALE__1down = 0;
   AnalysisMuons_muon_effSF_Quality_Medium_MUON_SCALE__1down = 0;
   AnalysisMuons_muon_effSF_Quality_Tight_MUON_SCALE__1down = 0;
   AnalysisMuons_muon_effSF_Isol_PflowTight_VarRad_MUON_SCALE__1down = 0;
   AnalysisMuons_muon_effMC_Trig_Loose_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_SCALE__1down = 0;
   AnalysisMuons_muon_effData_Trig_Loose_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_SCALE__1down = 0;
   AnalysisMuons_muon_effMC_Trig_Loose_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_SCALE__1down = 0;
   AnalysisMuons_muon_effData_Trig_Loose_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_SCALE__1down = 0;
   AnalysisMuons_muon_effMC_Trig_Medium_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_SCALE__1down = 0;
   AnalysisMuons_muon_effData_Trig_Medium_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_SCALE__1down = 0;
   AnalysisMuons_muon_effMC_Trig_Medium_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_SCALE__1down = 0;
   AnalysisMuons_muon_effData_Trig_Medium_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_SCALE__1down = 0;
   AnalysisMuons_muon_effMC_Trig_Tight_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_SCALE__1down = 0;
   AnalysisMuons_muon_effData_Trig_Tight_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_SCALE__1down = 0;
   AnalysisMuons_muon_effMC_Trig_Tight_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_SCALE__1down = 0;
   AnalysisMuons_muon_effData_Trig_Tight_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_SCALE__1down = 0;
   AnalysisJets_jet_selected_MUON_SCALE__1down = 0;
   AnalysisElectrons_el_selected_MUON_SCALE__1up = 0;
   AnalysisMuons_pt_MUON_SCALE__1up = 0;
   AnalysisMuons_mu_selected_MUON_SCALE__1up = 0;
   AnalysisMuons_isQuality_Loose_MUON_SCALE__1up = 0;
   AnalysisMuons_isQuality_Medium_MUON_SCALE__1up = 0;
   AnalysisMuons_isQuality_Tight_MUON_SCALE__1up = 0;
   AnalysisMuons_isIsolated_PflowTight_VarRad_MUON_SCALE__1up = 0;
   AnalysisMuons_muon_effSF_MUON_SCALE__1up = 0;
   AnalysisMuons_muon_effSF_TTVA_MUON_SCALE__1up = 0;
   AnalysisMuons_muon_effSF_Quality_Loose_MUON_SCALE__1up = 0;
   AnalysisMuons_muon_effSF_Quality_Medium_MUON_SCALE__1up = 0;
   AnalysisMuons_muon_effSF_Quality_Tight_MUON_SCALE__1up = 0;
   AnalysisMuons_muon_effSF_Isol_PflowTight_VarRad_MUON_SCALE__1up = 0;
   AnalysisMuons_muon_effMC_Trig_Loose_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_SCALE__1up = 0;
   AnalysisMuons_muon_effData_Trig_Loose_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_SCALE__1up = 0;
   AnalysisMuons_muon_effMC_Trig_Loose_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_SCALE__1up = 0;
   AnalysisMuons_muon_effData_Trig_Loose_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_SCALE__1up = 0;
   AnalysisMuons_muon_effMC_Trig_Medium_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_SCALE__1up = 0;
   AnalysisMuons_muon_effData_Trig_Medium_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_SCALE__1up = 0;
   AnalysisMuons_muon_effMC_Trig_Medium_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_SCALE__1up = 0;
   AnalysisMuons_muon_effData_Trig_Medium_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_SCALE__1up = 0;
   AnalysisMuons_muon_effMC_Trig_Tight_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_SCALE__1up = 0;
   AnalysisMuons_muon_effData_Trig_Tight_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_SCALE__1up = 0;
   AnalysisMuons_muon_effMC_Trig_Tight_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_SCALE__1up = 0;
   AnalysisMuons_muon_effData_Trig_Tight_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_SCALE__1up = 0;
   AnalysisJets_jet_selected_MUON_SCALE__1up = 0;
   DMesons_costhetastar_TRK_BIAS_D0_WM = 0;
   DMesons_D0Index_TRK_BIAS_D0_WM = 0;
   DMesons_daughterInfo__eta_TRK_BIAS_D0_WM = 0;
   DMesons_daughterInfo__passTight_TRK_BIAS_D0_WM = 0;
   DMesons_daughterInfo__pdgId_TRK_BIAS_D0_WM = 0;
   DMesons_daughterInfo__phi_TRK_BIAS_D0_WM = 0;
   DMesons_daughterInfo__pt_TRK_BIAS_D0_WM = 0;
   DMesons_daughterInfo__trackId_TRK_BIAS_D0_WM = 0;
   DMesons_daughterInfo__truthBarcode_TRK_BIAS_D0_WM = 0;
   DMesons_daughterInfo__truthDBarcode_TRK_BIAS_D0_WM = 0;
   DMesons_daughterInfo__z0SinTheta_TRK_BIAS_D0_WM = 0;
   DMesons_daughterInfo__z0SinThetaPV_TRK_BIAS_D0_WM = 0;
   DMesons_decayType_TRK_BIAS_D0_WM = 0;
   DMesons_DeltaMass_TRK_BIAS_D0_WM = 0;
   DMesons_eta_TRK_BIAS_D0_WM = 0;
   DMesons_fitOutput__Charge_TRK_BIAS_D0_WM = 0;
   DMesons_fitOutput__Chi2_TRK_BIAS_D0_WM = 0;
   DMesons_fitOutput__Impact_TRK_BIAS_D0_WM = 0;
   DMesons_fitOutput__ImpactSignificance_TRK_BIAS_D0_WM = 0;
   DMesons_fitOutput__ImpactZ0SinTheta_TRK_BIAS_D0_WM = 0;
   DMesons_fitOutput__Lxy_TRK_BIAS_D0_WM = 0;
   DMesons_fitOutput__VertexPosition_TRK_BIAS_D0_WM = 0;
   DMesons_m_TRK_BIAS_D0_WM = 0;
   DMesons_mKpi1_TRK_BIAS_D0_WM = 0;
   DMesons_mKpi2_TRK_BIAS_D0_WM = 0;
   DMesons_mPhi1_TRK_BIAS_D0_WM = 0;
   DMesons_mPhi2_TRK_BIAS_D0_WM = 0;
   DMesons_pdgId_TRK_BIAS_D0_WM = 0;
   DMesons_phi_TRK_BIAS_D0_WM = 0;
   DMesons_pt_TRK_BIAS_D0_WM = 0;
   DMesons_ptcone40_TRK_BIAS_D0_WM = 0;
   DMesons_SlowPionD0_TRK_BIAS_D0_WM = 0;
   DMesons_SlowPionZ0SinTheta_TRK_BIAS_D0_WM = 0;
   DMesons_truthBarcode_TRK_BIAS_D0_WM = 0;
   MesonTracks_d0sig_TRK_BIAS_D0_WM = 0;
   MesonTracks_d0sigPV_TRK_BIAS_D0_WM = 0;
   MesonTracks_eta_TRK_BIAS_D0_WM = 0;
   MesonTracks_passTight_TRK_BIAS_D0_WM = 0;
   MesonTracks_phi_TRK_BIAS_D0_WM = 0;
   MesonTracks_pt_TRK_BIAS_D0_WM = 0;
   MesonTracks_trackId_TRK_BIAS_D0_WM = 0;
   MesonTracks_z0sinTheta_TRK_BIAS_D0_WM = 0;
   MesonTracks_truthMatchProbability_TRK_BIAS_D0_WM = 0;
   AntiKt6PV0TrackJets_pt_TRK_BIAS_D0_WM = 0;
   AntiKt6PV0TrackJets_eta_TRK_BIAS_D0_WM = 0;
   AntiKt6PV0TrackJets_phi_TRK_BIAS_D0_WM = 0;
   AntiKt6PV0TrackJets_m_TRK_BIAS_D0_WM = 0;
   AntiKt6PV0TrackJets_daughter__pt_TRK_BIAS_D0_WM = 0;
   AntiKt6PV0TrackJets_daughter__eta_TRK_BIAS_D0_WM = 0;
   AntiKt6PV0TrackJets_daughter__phi_TRK_BIAS_D0_WM = 0;
   AntiKt6PV0TrackJets_daughter__trackId_TRK_BIAS_D0_WM = 0;
   AntiKt8PV0TrackJets_pt_TRK_BIAS_D0_WM = 0;
   AntiKt8PV0TrackJets_eta_TRK_BIAS_D0_WM = 0;
   AntiKt8PV0TrackJets_phi_TRK_BIAS_D0_WM = 0;
   AntiKt8PV0TrackJets_m_TRK_BIAS_D0_WM = 0;
   AntiKt8PV0TrackJets_daughter__pt_TRK_BIAS_D0_WM = 0;
   AntiKt8PV0TrackJets_daughter__eta_TRK_BIAS_D0_WM = 0;
   AntiKt8PV0TrackJets_daughter__phi_TRK_BIAS_D0_WM = 0;
   AntiKt8PV0TrackJets_daughter__trackId_TRK_BIAS_D0_WM = 0;
   AntiKt10PV0TrackJets_pt_TRK_BIAS_D0_WM = 0;
   AntiKt10PV0TrackJets_eta_TRK_BIAS_D0_WM = 0;
   AntiKt10PV0TrackJets_phi_TRK_BIAS_D0_WM = 0;
   AntiKt10PV0TrackJets_m_TRK_BIAS_D0_WM = 0;
   AntiKt10PV0TrackJets_daughter__pt_TRK_BIAS_D0_WM = 0;
   AntiKt10PV0TrackJets_daughter__eta_TRK_BIAS_D0_WM = 0;
   AntiKt10PV0TrackJets_daughter__phi_TRK_BIAS_D0_WM = 0;
   AntiKt10PV0TrackJets_daughter__trackId_TRK_BIAS_D0_WM = 0;
   AntiKtVR30Rmax4Rmin02TrackJets_pt_TRK_BIAS_D0_WM = 0;
   AntiKtVR30Rmax4Rmin02TrackJets_eta_TRK_BIAS_D0_WM = 0;
   AntiKtVR30Rmax4Rmin02TrackJets_phi_TRK_BIAS_D0_WM = 0;
   AntiKtVR30Rmax4Rmin02TrackJets_m_TRK_BIAS_D0_WM = 0;
   AntiKtVR30Rmax4Rmin02TrackJets_daughter__pt_TRK_BIAS_D0_WM = 0;
   AntiKtVR30Rmax4Rmin02TrackJets_daughter__eta_TRK_BIAS_D0_WM = 0;
   AntiKtVR30Rmax4Rmin02TrackJets_daughter__phi_TRK_BIAS_D0_WM = 0;
   AntiKtVR30Rmax4Rmin02TrackJets_daughter__trackId_TRK_BIAS_D0_WM = 0;
   DMesons_costhetastar_TRK_BIAS_QOVERP_SAGITTA_WM = 0;
   DMesons_D0Index_TRK_BIAS_QOVERP_SAGITTA_WM = 0;
   DMesons_daughterInfo__eta_TRK_BIAS_QOVERP_SAGITTA_WM = 0;
   DMesons_daughterInfo__passTight_TRK_BIAS_QOVERP_SAGITTA_WM = 0;
   DMesons_daughterInfo__pdgId_TRK_BIAS_QOVERP_SAGITTA_WM = 0;
   DMesons_daughterInfo__phi_TRK_BIAS_QOVERP_SAGITTA_WM = 0;
   DMesons_daughterInfo__pt_TRK_BIAS_QOVERP_SAGITTA_WM = 0;
   DMesons_daughterInfo__trackId_TRK_BIAS_QOVERP_SAGITTA_WM = 0;
   DMesons_daughterInfo__truthBarcode_TRK_BIAS_QOVERP_SAGITTA_WM = 0;
   DMesons_daughterInfo__truthDBarcode_TRK_BIAS_QOVERP_SAGITTA_WM = 0;
   DMesons_daughterInfo__z0SinTheta_TRK_BIAS_QOVERP_SAGITTA_WM = 0;
   DMesons_daughterInfo__z0SinThetaPV_TRK_BIAS_QOVERP_SAGITTA_WM = 0;
   DMesons_decayType_TRK_BIAS_QOVERP_SAGITTA_WM = 0;
   DMesons_DeltaMass_TRK_BIAS_QOVERP_SAGITTA_WM = 0;
   DMesons_eta_TRK_BIAS_QOVERP_SAGITTA_WM = 0;
   DMesons_fitOutput__Charge_TRK_BIAS_QOVERP_SAGITTA_WM = 0;
   DMesons_fitOutput__Chi2_TRK_BIAS_QOVERP_SAGITTA_WM = 0;
   DMesons_fitOutput__Impact_TRK_BIAS_QOVERP_SAGITTA_WM = 0;
   DMesons_fitOutput__ImpactSignificance_TRK_BIAS_QOVERP_SAGITTA_WM = 0;
   DMesons_fitOutput__ImpactZ0SinTheta_TRK_BIAS_QOVERP_SAGITTA_WM = 0;
   DMesons_fitOutput__Lxy_TRK_BIAS_QOVERP_SAGITTA_WM = 0;
   DMesons_fitOutput__VertexPosition_TRK_BIAS_QOVERP_SAGITTA_WM = 0;
   DMesons_m_TRK_BIAS_QOVERP_SAGITTA_WM = 0;
   DMesons_mKpi1_TRK_BIAS_QOVERP_SAGITTA_WM = 0;
   DMesons_mKpi2_TRK_BIAS_QOVERP_SAGITTA_WM = 0;
   DMesons_mPhi1_TRK_BIAS_QOVERP_SAGITTA_WM = 0;
   DMesons_mPhi2_TRK_BIAS_QOVERP_SAGITTA_WM = 0;
   DMesons_pdgId_TRK_BIAS_QOVERP_SAGITTA_WM = 0;
   DMesons_phi_TRK_BIAS_QOVERP_SAGITTA_WM = 0;
   DMesons_pt_TRK_BIAS_QOVERP_SAGITTA_WM = 0;
   DMesons_ptcone40_TRK_BIAS_QOVERP_SAGITTA_WM = 0;
   DMesons_SlowPionD0_TRK_BIAS_QOVERP_SAGITTA_WM = 0;
   DMesons_SlowPionZ0SinTheta_TRK_BIAS_QOVERP_SAGITTA_WM = 0;
   DMesons_truthBarcode_TRK_BIAS_QOVERP_SAGITTA_WM = 0;
   MesonTracks_d0sig_TRK_BIAS_QOVERP_SAGITTA_WM = 0;
   MesonTracks_d0sigPV_TRK_BIAS_QOVERP_SAGITTA_WM = 0;
   MesonTracks_eta_TRK_BIAS_QOVERP_SAGITTA_WM = 0;
   MesonTracks_passTight_TRK_BIAS_QOVERP_SAGITTA_WM = 0;
   MesonTracks_phi_TRK_BIAS_QOVERP_SAGITTA_WM = 0;
   MesonTracks_pt_TRK_BIAS_QOVERP_SAGITTA_WM = 0;
   MesonTracks_trackId_TRK_BIAS_QOVERP_SAGITTA_WM = 0;
   MesonTracks_z0sinTheta_TRK_BIAS_QOVERP_SAGITTA_WM = 0;
   MesonTracks_truthMatchProbability_TRK_BIAS_QOVERP_SAGITTA_WM = 0;
   AntiKt6PV0TrackJets_pt_TRK_BIAS_QOVERP_SAGITTA_WM = 0;
   AntiKt6PV0TrackJets_eta_TRK_BIAS_QOVERP_SAGITTA_WM = 0;
   AntiKt6PV0TrackJets_phi_TRK_BIAS_QOVERP_SAGITTA_WM = 0;
   AntiKt6PV0TrackJets_m_TRK_BIAS_QOVERP_SAGITTA_WM = 0;
   AntiKt6PV0TrackJets_daughter__pt_TRK_BIAS_QOVERP_SAGITTA_WM = 0;
   AntiKt6PV0TrackJets_daughter__eta_TRK_BIAS_QOVERP_SAGITTA_WM = 0;
   AntiKt6PV0TrackJets_daughter__phi_TRK_BIAS_QOVERP_SAGITTA_WM = 0;
   AntiKt6PV0TrackJets_daughter__trackId_TRK_BIAS_QOVERP_SAGITTA_WM = 0;
   AntiKt8PV0TrackJets_pt_TRK_BIAS_QOVERP_SAGITTA_WM = 0;
   AntiKt8PV0TrackJets_eta_TRK_BIAS_QOVERP_SAGITTA_WM = 0;
   AntiKt8PV0TrackJets_phi_TRK_BIAS_QOVERP_SAGITTA_WM = 0;
   AntiKt8PV0TrackJets_m_TRK_BIAS_QOVERP_SAGITTA_WM = 0;
   AntiKt8PV0TrackJets_daughter__pt_TRK_BIAS_QOVERP_SAGITTA_WM = 0;
   AntiKt8PV0TrackJets_daughter__eta_TRK_BIAS_QOVERP_SAGITTA_WM = 0;
   AntiKt8PV0TrackJets_daughter__phi_TRK_BIAS_QOVERP_SAGITTA_WM = 0;
   AntiKt8PV0TrackJets_daughter__trackId_TRK_BIAS_QOVERP_SAGITTA_WM = 0;
   AntiKt10PV0TrackJets_pt_TRK_BIAS_QOVERP_SAGITTA_WM = 0;
   AntiKt10PV0TrackJets_eta_TRK_BIAS_QOVERP_SAGITTA_WM = 0;
   AntiKt10PV0TrackJets_phi_TRK_BIAS_QOVERP_SAGITTA_WM = 0;
   AntiKt10PV0TrackJets_m_TRK_BIAS_QOVERP_SAGITTA_WM = 0;
   AntiKt10PV0TrackJets_daughter__pt_TRK_BIAS_QOVERP_SAGITTA_WM = 0;
   AntiKt10PV0TrackJets_daughter__eta_TRK_BIAS_QOVERP_SAGITTA_WM = 0;
   AntiKt10PV0TrackJets_daughter__phi_TRK_BIAS_QOVERP_SAGITTA_WM = 0;
   AntiKt10PV0TrackJets_daughter__trackId_TRK_BIAS_QOVERP_SAGITTA_WM = 0;
   AntiKtVR30Rmax4Rmin02TrackJets_pt_TRK_BIAS_QOVERP_SAGITTA_WM = 0;
   AntiKtVR30Rmax4Rmin02TrackJets_eta_TRK_BIAS_QOVERP_SAGITTA_WM = 0;
   AntiKtVR30Rmax4Rmin02TrackJets_phi_TRK_BIAS_QOVERP_SAGITTA_WM = 0;
   AntiKtVR30Rmax4Rmin02TrackJets_m_TRK_BIAS_QOVERP_SAGITTA_WM = 0;
   AntiKtVR30Rmax4Rmin02TrackJets_daughter__pt_TRK_BIAS_QOVERP_SAGITTA_WM = 0;
   AntiKtVR30Rmax4Rmin02TrackJets_daughter__eta_TRK_BIAS_QOVERP_SAGITTA_WM = 0;
   AntiKtVR30Rmax4Rmin02TrackJets_daughter__phi_TRK_BIAS_QOVERP_SAGITTA_WM = 0;
   AntiKtVR30Rmax4Rmin02TrackJets_daughter__trackId_TRK_BIAS_QOVERP_SAGITTA_WM = 0;
   DMesons_costhetastar_TRK_BIAS_Z0_WM = 0;
   DMesons_D0Index_TRK_BIAS_Z0_WM = 0;
   DMesons_daughterInfo__eta_TRK_BIAS_Z0_WM = 0;
   DMesons_daughterInfo__passTight_TRK_BIAS_Z0_WM = 0;
   DMesons_daughterInfo__pdgId_TRK_BIAS_Z0_WM = 0;
   DMesons_daughterInfo__phi_TRK_BIAS_Z0_WM = 0;
   DMesons_daughterInfo__pt_TRK_BIAS_Z0_WM = 0;
   DMesons_daughterInfo__trackId_TRK_BIAS_Z0_WM = 0;
   DMesons_daughterInfo__truthBarcode_TRK_BIAS_Z0_WM = 0;
   DMesons_daughterInfo__truthDBarcode_TRK_BIAS_Z0_WM = 0;
   DMesons_daughterInfo__z0SinTheta_TRK_BIAS_Z0_WM = 0;
   DMesons_daughterInfo__z0SinThetaPV_TRK_BIAS_Z0_WM = 0;
   DMesons_decayType_TRK_BIAS_Z0_WM = 0;
   DMesons_DeltaMass_TRK_BIAS_Z0_WM = 0;
   DMesons_eta_TRK_BIAS_Z0_WM = 0;
   DMesons_fitOutput__Charge_TRK_BIAS_Z0_WM = 0;
   DMesons_fitOutput__Chi2_TRK_BIAS_Z0_WM = 0;
   DMesons_fitOutput__Impact_TRK_BIAS_Z0_WM = 0;
   DMesons_fitOutput__ImpactSignificance_TRK_BIAS_Z0_WM = 0;
   DMesons_fitOutput__ImpactZ0SinTheta_TRK_BIAS_Z0_WM = 0;
   DMesons_fitOutput__Lxy_TRK_BIAS_Z0_WM = 0;
   DMesons_fitOutput__VertexPosition_TRK_BIAS_Z0_WM = 0;
   DMesons_m_TRK_BIAS_Z0_WM = 0;
   DMesons_mKpi1_TRK_BIAS_Z0_WM = 0;
   DMesons_mKpi2_TRK_BIAS_Z0_WM = 0;
   DMesons_mPhi1_TRK_BIAS_Z0_WM = 0;
   DMesons_mPhi2_TRK_BIAS_Z0_WM = 0;
   DMesons_pdgId_TRK_BIAS_Z0_WM = 0;
   DMesons_phi_TRK_BIAS_Z0_WM = 0;
   DMesons_pt_TRK_BIAS_Z0_WM = 0;
   DMesons_ptcone40_TRK_BIAS_Z0_WM = 0;
   DMesons_SlowPionD0_TRK_BIAS_Z0_WM = 0;
   DMesons_SlowPionZ0SinTheta_TRK_BIAS_Z0_WM = 0;
   DMesons_truthBarcode_TRK_BIAS_Z0_WM = 0;
   MesonTracks_d0sig_TRK_BIAS_Z0_WM = 0;
   MesonTracks_d0sigPV_TRK_BIAS_Z0_WM = 0;
   MesonTracks_eta_TRK_BIAS_Z0_WM = 0;
   MesonTracks_passTight_TRK_BIAS_Z0_WM = 0;
   MesonTracks_phi_TRK_BIAS_Z0_WM = 0;
   MesonTracks_pt_TRK_BIAS_Z0_WM = 0;
   MesonTracks_trackId_TRK_BIAS_Z0_WM = 0;
   MesonTracks_z0sinTheta_TRK_BIAS_Z0_WM = 0;
   MesonTracks_truthMatchProbability_TRK_BIAS_Z0_WM = 0;
   AntiKt6PV0TrackJets_pt_TRK_BIAS_Z0_WM = 0;
   AntiKt6PV0TrackJets_eta_TRK_BIAS_Z0_WM = 0;
   AntiKt6PV0TrackJets_phi_TRK_BIAS_Z0_WM = 0;
   AntiKt6PV0TrackJets_m_TRK_BIAS_Z0_WM = 0;
   AntiKt6PV0TrackJets_daughter__pt_TRK_BIAS_Z0_WM = 0;
   AntiKt6PV0TrackJets_daughter__eta_TRK_BIAS_Z0_WM = 0;
   AntiKt6PV0TrackJets_daughter__phi_TRK_BIAS_Z0_WM = 0;
   AntiKt6PV0TrackJets_daughter__trackId_TRK_BIAS_Z0_WM = 0;
   AntiKt8PV0TrackJets_pt_TRK_BIAS_Z0_WM = 0;
   AntiKt8PV0TrackJets_eta_TRK_BIAS_Z0_WM = 0;
   AntiKt8PV0TrackJets_phi_TRK_BIAS_Z0_WM = 0;
   AntiKt8PV0TrackJets_m_TRK_BIAS_Z0_WM = 0;
   AntiKt8PV0TrackJets_daughter__pt_TRK_BIAS_Z0_WM = 0;
   AntiKt8PV0TrackJets_daughter__eta_TRK_BIAS_Z0_WM = 0;
   AntiKt8PV0TrackJets_daughter__phi_TRK_BIAS_Z0_WM = 0;
   AntiKt8PV0TrackJets_daughter__trackId_TRK_BIAS_Z0_WM = 0;
   AntiKt10PV0TrackJets_pt_TRK_BIAS_Z0_WM = 0;
   AntiKt10PV0TrackJets_eta_TRK_BIAS_Z0_WM = 0;
   AntiKt10PV0TrackJets_phi_TRK_BIAS_Z0_WM = 0;
   AntiKt10PV0TrackJets_m_TRK_BIAS_Z0_WM = 0;
   AntiKt10PV0TrackJets_daughter__pt_TRK_BIAS_Z0_WM = 0;
   AntiKt10PV0TrackJets_daughter__eta_TRK_BIAS_Z0_WM = 0;
   AntiKt10PV0TrackJets_daughter__phi_TRK_BIAS_Z0_WM = 0;
   AntiKt10PV0TrackJets_daughter__trackId_TRK_BIAS_Z0_WM = 0;
   AntiKtVR30Rmax4Rmin02TrackJets_pt_TRK_BIAS_Z0_WM = 0;
   AntiKtVR30Rmax4Rmin02TrackJets_eta_TRK_BIAS_Z0_WM = 0;
   AntiKtVR30Rmax4Rmin02TrackJets_phi_TRK_BIAS_Z0_WM = 0;
   AntiKtVR30Rmax4Rmin02TrackJets_m_TRK_BIAS_Z0_WM = 0;
   AntiKtVR30Rmax4Rmin02TrackJets_daughter__pt_TRK_BIAS_Z0_WM = 0;
   AntiKtVR30Rmax4Rmin02TrackJets_daughter__eta_TRK_BIAS_Z0_WM = 0;
   AntiKtVR30Rmax4Rmin02TrackJets_daughter__phi_TRK_BIAS_Z0_WM = 0;
   AntiKtVR30Rmax4Rmin02TrackJets_daughter__trackId_TRK_BIAS_Z0_WM = 0;
   DMesons_costhetastar_TRK_EFF_LOOSE_GLOBAL = 0;
   DMesons_D0Index_TRK_EFF_LOOSE_GLOBAL = 0;
   DMesons_daughterInfo__eta_TRK_EFF_LOOSE_GLOBAL = 0;
   DMesons_daughterInfo__passTight_TRK_EFF_LOOSE_GLOBAL = 0;
   DMesons_daughterInfo__pdgId_TRK_EFF_LOOSE_GLOBAL = 0;
   DMesons_daughterInfo__phi_TRK_EFF_LOOSE_GLOBAL = 0;
   DMesons_daughterInfo__pt_TRK_EFF_LOOSE_GLOBAL = 0;
   DMesons_daughterInfo__trackId_TRK_EFF_LOOSE_GLOBAL = 0;
   DMesons_daughterInfo__truthBarcode_TRK_EFF_LOOSE_GLOBAL = 0;
   DMesons_daughterInfo__truthDBarcode_TRK_EFF_LOOSE_GLOBAL = 0;
   DMesons_daughterInfo__z0SinTheta_TRK_EFF_LOOSE_GLOBAL = 0;
   DMesons_daughterInfo__z0SinThetaPV_TRK_EFF_LOOSE_GLOBAL = 0;
   DMesons_decayType_TRK_EFF_LOOSE_GLOBAL = 0;
   DMesons_DeltaMass_TRK_EFF_LOOSE_GLOBAL = 0;
   DMesons_eta_TRK_EFF_LOOSE_GLOBAL = 0;
   DMesons_fitOutput__Charge_TRK_EFF_LOOSE_GLOBAL = 0;
   DMesons_fitOutput__Chi2_TRK_EFF_LOOSE_GLOBAL = 0;
   DMesons_fitOutput__Impact_TRK_EFF_LOOSE_GLOBAL = 0;
   DMesons_fitOutput__ImpactSignificance_TRK_EFF_LOOSE_GLOBAL = 0;
   DMesons_fitOutput__ImpactZ0SinTheta_TRK_EFF_LOOSE_GLOBAL = 0;
   DMesons_fitOutput__Lxy_TRK_EFF_LOOSE_GLOBAL = 0;
   DMesons_fitOutput__VertexPosition_TRK_EFF_LOOSE_GLOBAL = 0;
   DMesons_m_TRK_EFF_LOOSE_GLOBAL = 0;
   DMesons_mKpi1_TRK_EFF_LOOSE_GLOBAL = 0;
   DMesons_mKpi2_TRK_EFF_LOOSE_GLOBAL = 0;
   DMesons_mPhi1_TRK_EFF_LOOSE_GLOBAL = 0;
   DMesons_mPhi2_TRK_EFF_LOOSE_GLOBAL = 0;
   DMesons_pdgId_TRK_EFF_LOOSE_GLOBAL = 0;
   DMesons_phi_TRK_EFF_LOOSE_GLOBAL = 0;
   DMesons_pt_TRK_EFF_LOOSE_GLOBAL = 0;
   DMesons_ptcone40_TRK_EFF_LOOSE_GLOBAL = 0;
   DMesons_SlowPionD0_TRK_EFF_LOOSE_GLOBAL = 0;
   DMesons_SlowPionZ0SinTheta_TRK_EFF_LOOSE_GLOBAL = 0;
   DMesons_truthBarcode_TRK_EFF_LOOSE_GLOBAL = 0;
   MesonTracks_d0sig_TRK_EFF_LOOSE_GLOBAL = 0;
   MesonTracks_d0sigPV_TRK_EFF_LOOSE_GLOBAL = 0;
   MesonTracks_eta_TRK_EFF_LOOSE_GLOBAL = 0;
   MesonTracks_passTight_TRK_EFF_LOOSE_GLOBAL = 0;
   MesonTracks_phi_TRK_EFF_LOOSE_GLOBAL = 0;
   MesonTracks_pt_TRK_EFF_LOOSE_GLOBAL = 0;
   MesonTracks_trackId_TRK_EFF_LOOSE_GLOBAL = 0;
   MesonTracks_z0sinTheta_TRK_EFF_LOOSE_GLOBAL = 0;
   MesonTracks_truthMatchProbability_TRK_EFF_LOOSE_GLOBAL = 0;
   AntiKt6PV0TrackJets_pt_TRK_EFF_LOOSE_GLOBAL = 0;
   AntiKt6PV0TrackJets_eta_TRK_EFF_LOOSE_GLOBAL = 0;
   AntiKt6PV0TrackJets_phi_TRK_EFF_LOOSE_GLOBAL = 0;
   AntiKt6PV0TrackJets_m_TRK_EFF_LOOSE_GLOBAL = 0;
   AntiKt6PV0TrackJets_daughter__pt_TRK_EFF_LOOSE_GLOBAL = 0;
   AntiKt6PV0TrackJets_daughter__eta_TRK_EFF_LOOSE_GLOBAL = 0;
   AntiKt6PV0TrackJets_daughter__phi_TRK_EFF_LOOSE_GLOBAL = 0;
   AntiKt6PV0TrackJets_daughter__trackId_TRK_EFF_LOOSE_GLOBAL = 0;
   AntiKt8PV0TrackJets_pt_TRK_EFF_LOOSE_GLOBAL = 0;
   AntiKt8PV0TrackJets_eta_TRK_EFF_LOOSE_GLOBAL = 0;
   AntiKt8PV0TrackJets_phi_TRK_EFF_LOOSE_GLOBAL = 0;
   AntiKt8PV0TrackJets_m_TRK_EFF_LOOSE_GLOBAL = 0;
   AntiKt8PV0TrackJets_daughter__pt_TRK_EFF_LOOSE_GLOBAL = 0;
   AntiKt8PV0TrackJets_daughter__eta_TRK_EFF_LOOSE_GLOBAL = 0;
   AntiKt8PV0TrackJets_daughter__phi_TRK_EFF_LOOSE_GLOBAL = 0;
   AntiKt8PV0TrackJets_daughter__trackId_TRK_EFF_LOOSE_GLOBAL = 0;
   AntiKt10PV0TrackJets_pt_TRK_EFF_LOOSE_GLOBAL = 0;
   AntiKt10PV0TrackJets_eta_TRK_EFF_LOOSE_GLOBAL = 0;
   AntiKt10PV0TrackJets_phi_TRK_EFF_LOOSE_GLOBAL = 0;
   AntiKt10PV0TrackJets_m_TRK_EFF_LOOSE_GLOBAL = 0;
   AntiKt10PV0TrackJets_daughter__pt_TRK_EFF_LOOSE_GLOBAL = 0;
   AntiKt10PV0TrackJets_daughter__eta_TRK_EFF_LOOSE_GLOBAL = 0;
   AntiKt10PV0TrackJets_daughter__phi_TRK_EFF_LOOSE_GLOBAL = 0;
   AntiKt10PV0TrackJets_daughter__trackId_TRK_EFF_LOOSE_GLOBAL = 0;
   AntiKtVR30Rmax4Rmin02TrackJets_pt_TRK_EFF_LOOSE_GLOBAL = 0;
   AntiKtVR30Rmax4Rmin02TrackJets_eta_TRK_EFF_LOOSE_GLOBAL = 0;
   AntiKtVR30Rmax4Rmin02TrackJets_phi_TRK_EFF_LOOSE_GLOBAL = 0;
   AntiKtVR30Rmax4Rmin02TrackJets_m_TRK_EFF_LOOSE_GLOBAL = 0;
   AntiKtVR30Rmax4Rmin02TrackJets_daughter__pt_TRK_EFF_LOOSE_GLOBAL = 0;
   AntiKtVR30Rmax4Rmin02TrackJets_daughter__eta_TRK_EFF_LOOSE_GLOBAL = 0;
   AntiKtVR30Rmax4Rmin02TrackJets_daughter__phi_TRK_EFF_LOOSE_GLOBAL = 0;
   AntiKtVR30Rmax4Rmin02TrackJets_daughter__trackId_TRK_EFF_LOOSE_GLOBAL = 0;
   DMesons_costhetastar_TRK_EFF_LOOSE_IBL = 0;
   DMesons_D0Index_TRK_EFF_LOOSE_IBL = 0;
   DMesons_daughterInfo__eta_TRK_EFF_LOOSE_IBL = 0;
   DMesons_daughterInfo__passTight_TRK_EFF_LOOSE_IBL = 0;
   DMesons_daughterInfo__pdgId_TRK_EFF_LOOSE_IBL = 0;
   DMesons_daughterInfo__phi_TRK_EFF_LOOSE_IBL = 0;
   DMesons_daughterInfo__pt_TRK_EFF_LOOSE_IBL = 0;
   DMesons_daughterInfo__trackId_TRK_EFF_LOOSE_IBL = 0;
   DMesons_daughterInfo__truthBarcode_TRK_EFF_LOOSE_IBL = 0;
   DMesons_daughterInfo__truthDBarcode_TRK_EFF_LOOSE_IBL = 0;
   DMesons_daughterInfo__z0SinTheta_TRK_EFF_LOOSE_IBL = 0;
   DMesons_daughterInfo__z0SinThetaPV_TRK_EFF_LOOSE_IBL = 0;
   DMesons_decayType_TRK_EFF_LOOSE_IBL = 0;
   DMesons_DeltaMass_TRK_EFF_LOOSE_IBL = 0;
   DMesons_eta_TRK_EFF_LOOSE_IBL = 0;
   DMesons_fitOutput__Charge_TRK_EFF_LOOSE_IBL = 0;
   DMesons_fitOutput__Chi2_TRK_EFF_LOOSE_IBL = 0;
   DMesons_fitOutput__Impact_TRK_EFF_LOOSE_IBL = 0;
   DMesons_fitOutput__ImpactSignificance_TRK_EFF_LOOSE_IBL = 0;
   DMesons_fitOutput__ImpactZ0SinTheta_TRK_EFF_LOOSE_IBL = 0;
   DMesons_fitOutput__Lxy_TRK_EFF_LOOSE_IBL = 0;
   DMesons_fitOutput__VertexPosition_TRK_EFF_LOOSE_IBL = 0;
   DMesons_m_TRK_EFF_LOOSE_IBL = 0;
   DMesons_mKpi1_TRK_EFF_LOOSE_IBL = 0;
   DMesons_mKpi2_TRK_EFF_LOOSE_IBL = 0;
   DMesons_mPhi1_TRK_EFF_LOOSE_IBL = 0;
   DMesons_mPhi2_TRK_EFF_LOOSE_IBL = 0;
   DMesons_pdgId_TRK_EFF_LOOSE_IBL = 0;
   DMesons_phi_TRK_EFF_LOOSE_IBL = 0;
   DMesons_pt_TRK_EFF_LOOSE_IBL = 0;
   DMesons_ptcone40_TRK_EFF_LOOSE_IBL = 0;
   DMesons_SlowPionD0_TRK_EFF_LOOSE_IBL = 0;
   DMesons_SlowPionZ0SinTheta_TRK_EFF_LOOSE_IBL = 0;
   DMesons_truthBarcode_TRK_EFF_LOOSE_IBL = 0;
   MesonTracks_d0sig_TRK_EFF_LOOSE_IBL = 0;
   MesonTracks_d0sigPV_TRK_EFF_LOOSE_IBL = 0;
   MesonTracks_eta_TRK_EFF_LOOSE_IBL = 0;
   MesonTracks_passTight_TRK_EFF_LOOSE_IBL = 0;
   MesonTracks_phi_TRK_EFF_LOOSE_IBL = 0;
   MesonTracks_pt_TRK_EFF_LOOSE_IBL = 0;
   MesonTracks_trackId_TRK_EFF_LOOSE_IBL = 0;
   MesonTracks_z0sinTheta_TRK_EFF_LOOSE_IBL = 0;
   MesonTracks_truthMatchProbability_TRK_EFF_LOOSE_IBL = 0;
   AntiKt6PV0TrackJets_pt_TRK_EFF_LOOSE_IBL = 0;
   AntiKt6PV0TrackJets_eta_TRK_EFF_LOOSE_IBL = 0;
   AntiKt6PV0TrackJets_phi_TRK_EFF_LOOSE_IBL = 0;
   AntiKt6PV0TrackJets_m_TRK_EFF_LOOSE_IBL = 0;
   AntiKt6PV0TrackJets_daughter__pt_TRK_EFF_LOOSE_IBL = 0;
   AntiKt6PV0TrackJets_daughter__eta_TRK_EFF_LOOSE_IBL = 0;
   AntiKt6PV0TrackJets_daughter__phi_TRK_EFF_LOOSE_IBL = 0;
   AntiKt6PV0TrackJets_daughter__trackId_TRK_EFF_LOOSE_IBL = 0;
   AntiKt8PV0TrackJets_pt_TRK_EFF_LOOSE_IBL = 0;
   AntiKt8PV0TrackJets_eta_TRK_EFF_LOOSE_IBL = 0;
   AntiKt8PV0TrackJets_phi_TRK_EFF_LOOSE_IBL = 0;
   AntiKt8PV0TrackJets_m_TRK_EFF_LOOSE_IBL = 0;
   AntiKt8PV0TrackJets_daughter__pt_TRK_EFF_LOOSE_IBL = 0;
   AntiKt8PV0TrackJets_daughter__eta_TRK_EFF_LOOSE_IBL = 0;
   AntiKt8PV0TrackJets_daughter__phi_TRK_EFF_LOOSE_IBL = 0;
   AntiKt8PV0TrackJets_daughter__trackId_TRK_EFF_LOOSE_IBL = 0;
   AntiKt10PV0TrackJets_pt_TRK_EFF_LOOSE_IBL = 0;
   AntiKt10PV0TrackJets_eta_TRK_EFF_LOOSE_IBL = 0;
   AntiKt10PV0TrackJets_phi_TRK_EFF_LOOSE_IBL = 0;
   AntiKt10PV0TrackJets_m_TRK_EFF_LOOSE_IBL = 0;
   AntiKt10PV0TrackJets_daughter__pt_TRK_EFF_LOOSE_IBL = 0;
   AntiKt10PV0TrackJets_daughter__eta_TRK_EFF_LOOSE_IBL = 0;
   AntiKt10PV0TrackJets_daughter__phi_TRK_EFF_LOOSE_IBL = 0;
   AntiKt10PV0TrackJets_daughter__trackId_TRK_EFF_LOOSE_IBL = 0;
   AntiKtVR30Rmax4Rmin02TrackJets_pt_TRK_EFF_LOOSE_IBL = 0;
   AntiKtVR30Rmax4Rmin02TrackJets_eta_TRK_EFF_LOOSE_IBL = 0;
   AntiKtVR30Rmax4Rmin02TrackJets_phi_TRK_EFF_LOOSE_IBL = 0;
   AntiKtVR30Rmax4Rmin02TrackJets_m_TRK_EFF_LOOSE_IBL = 0;
   AntiKtVR30Rmax4Rmin02TrackJets_daughter__pt_TRK_EFF_LOOSE_IBL = 0;
   AntiKtVR30Rmax4Rmin02TrackJets_daughter__eta_TRK_EFF_LOOSE_IBL = 0;
   AntiKtVR30Rmax4Rmin02TrackJets_daughter__phi_TRK_EFF_LOOSE_IBL = 0;
   AntiKtVR30Rmax4Rmin02TrackJets_daughter__trackId_TRK_EFF_LOOSE_IBL = 0;
   DMesons_costhetastar_TRK_EFF_LOOSE_PHYSMODEL = 0;
   DMesons_D0Index_TRK_EFF_LOOSE_PHYSMODEL = 0;
   DMesons_daughterInfo__eta_TRK_EFF_LOOSE_PHYSMODEL = 0;
   DMesons_daughterInfo__passTight_TRK_EFF_LOOSE_PHYSMODEL = 0;
   DMesons_daughterInfo__pdgId_TRK_EFF_LOOSE_PHYSMODEL = 0;
   DMesons_daughterInfo__phi_TRK_EFF_LOOSE_PHYSMODEL = 0;
   DMesons_daughterInfo__pt_TRK_EFF_LOOSE_PHYSMODEL = 0;
   DMesons_daughterInfo__trackId_TRK_EFF_LOOSE_PHYSMODEL = 0;
   DMesons_daughterInfo__truthBarcode_TRK_EFF_LOOSE_PHYSMODEL = 0;
   DMesons_daughterInfo__truthDBarcode_TRK_EFF_LOOSE_PHYSMODEL = 0;
   DMesons_daughterInfo__z0SinTheta_TRK_EFF_LOOSE_PHYSMODEL = 0;
   DMesons_daughterInfo__z0SinThetaPV_TRK_EFF_LOOSE_PHYSMODEL = 0;
   DMesons_decayType_TRK_EFF_LOOSE_PHYSMODEL = 0;
   DMesons_DeltaMass_TRK_EFF_LOOSE_PHYSMODEL = 0;
   DMesons_eta_TRK_EFF_LOOSE_PHYSMODEL = 0;
   DMesons_fitOutput__Charge_TRK_EFF_LOOSE_PHYSMODEL = 0;
   DMesons_fitOutput__Chi2_TRK_EFF_LOOSE_PHYSMODEL = 0;
   DMesons_fitOutput__Impact_TRK_EFF_LOOSE_PHYSMODEL = 0;
   DMesons_fitOutput__ImpactSignificance_TRK_EFF_LOOSE_PHYSMODEL = 0;
   DMesons_fitOutput__ImpactZ0SinTheta_TRK_EFF_LOOSE_PHYSMODEL = 0;
   DMesons_fitOutput__Lxy_TRK_EFF_LOOSE_PHYSMODEL = 0;
   DMesons_fitOutput__VertexPosition_TRK_EFF_LOOSE_PHYSMODEL = 0;
   DMesons_m_TRK_EFF_LOOSE_PHYSMODEL = 0;
   DMesons_mKpi1_TRK_EFF_LOOSE_PHYSMODEL = 0;
   DMesons_mKpi2_TRK_EFF_LOOSE_PHYSMODEL = 0;
   DMesons_mPhi1_TRK_EFF_LOOSE_PHYSMODEL = 0;
   DMesons_mPhi2_TRK_EFF_LOOSE_PHYSMODEL = 0;
   DMesons_pdgId_TRK_EFF_LOOSE_PHYSMODEL = 0;
   DMesons_phi_TRK_EFF_LOOSE_PHYSMODEL = 0;
   DMesons_pt_TRK_EFF_LOOSE_PHYSMODEL = 0;
   DMesons_ptcone40_TRK_EFF_LOOSE_PHYSMODEL = 0;
   DMesons_SlowPionD0_TRK_EFF_LOOSE_PHYSMODEL = 0;
   DMesons_SlowPionZ0SinTheta_TRK_EFF_LOOSE_PHYSMODEL = 0;
   DMesons_truthBarcode_TRK_EFF_LOOSE_PHYSMODEL = 0;
   MesonTracks_d0sig_TRK_EFF_LOOSE_PHYSMODEL = 0;
   MesonTracks_d0sigPV_TRK_EFF_LOOSE_PHYSMODEL = 0;
   MesonTracks_eta_TRK_EFF_LOOSE_PHYSMODEL = 0;
   MesonTracks_passTight_TRK_EFF_LOOSE_PHYSMODEL = 0;
   MesonTracks_phi_TRK_EFF_LOOSE_PHYSMODEL = 0;
   MesonTracks_pt_TRK_EFF_LOOSE_PHYSMODEL = 0;
   MesonTracks_trackId_TRK_EFF_LOOSE_PHYSMODEL = 0;
   MesonTracks_z0sinTheta_TRK_EFF_LOOSE_PHYSMODEL = 0;
   MesonTracks_truthMatchProbability_TRK_EFF_LOOSE_PHYSMODEL = 0;
   AntiKt6PV0TrackJets_pt_TRK_EFF_LOOSE_PHYSMODEL = 0;
   AntiKt6PV0TrackJets_eta_TRK_EFF_LOOSE_PHYSMODEL = 0;
   AntiKt6PV0TrackJets_phi_TRK_EFF_LOOSE_PHYSMODEL = 0;
   AntiKt6PV0TrackJets_m_TRK_EFF_LOOSE_PHYSMODEL = 0;
   AntiKt6PV0TrackJets_daughter__pt_TRK_EFF_LOOSE_PHYSMODEL = 0;
   AntiKt6PV0TrackJets_daughter__eta_TRK_EFF_LOOSE_PHYSMODEL = 0;
   AntiKt6PV0TrackJets_daughter__phi_TRK_EFF_LOOSE_PHYSMODEL = 0;
   AntiKt6PV0TrackJets_daughter__trackId_TRK_EFF_LOOSE_PHYSMODEL = 0;
   AntiKt8PV0TrackJets_pt_TRK_EFF_LOOSE_PHYSMODEL = 0;
   AntiKt8PV0TrackJets_eta_TRK_EFF_LOOSE_PHYSMODEL = 0;
   AntiKt8PV0TrackJets_phi_TRK_EFF_LOOSE_PHYSMODEL = 0;
   AntiKt8PV0TrackJets_m_TRK_EFF_LOOSE_PHYSMODEL = 0;
   AntiKt8PV0TrackJets_daughter__pt_TRK_EFF_LOOSE_PHYSMODEL = 0;
   AntiKt8PV0TrackJets_daughter__eta_TRK_EFF_LOOSE_PHYSMODEL = 0;
   AntiKt8PV0TrackJets_daughter__phi_TRK_EFF_LOOSE_PHYSMODEL = 0;
   AntiKt8PV0TrackJets_daughter__trackId_TRK_EFF_LOOSE_PHYSMODEL = 0;
   AntiKt10PV0TrackJets_pt_TRK_EFF_LOOSE_PHYSMODEL = 0;
   AntiKt10PV0TrackJets_eta_TRK_EFF_LOOSE_PHYSMODEL = 0;
   AntiKt10PV0TrackJets_phi_TRK_EFF_LOOSE_PHYSMODEL = 0;
   AntiKt10PV0TrackJets_m_TRK_EFF_LOOSE_PHYSMODEL = 0;
   AntiKt10PV0TrackJets_daughter__pt_TRK_EFF_LOOSE_PHYSMODEL = 0;
   AntiKt10PV0TrackJets_daughter__eta_TRK_EFF_LOOSE_PHYSMODEL = 0;
   AntiKt10PV0TrackJets_daughter__phi_TRK_EFF_LOOSE_PHYSMODEL = 0;
   AntiKt10PV0TrackJets_daughter__trackId_TRK_EFF_LOOSE_PHYSMODEL = 0;
   AntiKtVR30Rmax4Rmin02TrackJets_pt_TRK_EFF_LOOSE_PHYSMODEL = 0;
   AntiKtVR30Rmax4Rmin02TrackJets_eta_TRK_EFF_LOOSE_PHYSMODEL = 0;
   AntiKtVR30Rmax4Rmin02TrackJets_phi_TRK_EFF_LOOSE_PHYSMODEL = 0;
   AntiKtVR30Rmax4Rmin02TrackJets_m_TRK_EFF_LOOSE_PHYSMODEL = 0;
   AntiKtVR30Rmax4Rmin02TrackJets_daughter__pt_TRK_EFF_LOOSE_PHYSMODEL = 0;
   AntiKtVR30Rmax4Rmin02TrackJets_daughter__eta_TRK_EFF_LOOSE_PHYSMODEL = 0;
   AntiKtVR30Rmax4Rmin02TrackJets_daughter__phi_TRK_EFF_LOOSE_PHYSMODEL = 0;
   AntiKtVR30Rmax4Rmin02TrackJets_daughter__trackId_TRK_EFF_LOOSE_PHYSMODEL = 0;
   DMesons_costhetastar_TRK_EFF_LOOSE_PP0 = 0;
   DMesons_D0Index_TRK_EFF_LOOSE_PP0 = 0;
   DMesons_daughterInfo__eta_TRK_EFF_LOOSE_PP0 = 0;
   DMesons_daughterInfo__passTight_TRK_EFF_LOOSE_PP0 = 0;
   DMesons_daughterInfo__pdgId_TRK_EFF_LOOSE_PP0 = 0;
   DMesons_daughterInfo__phi_TRK_EFF_LOOSE_PP0 = 0;
   DMesons_daughterInfo__pt_TRK_EFF_LOOSE_PP0 = 0;
   DMesons_daughterInfo__trackId_TRK_EFF_LOOSE_PP0 = 0;
   DMesons_daughterInfo__truthBarcode_TRK_EFF_LOOSE_PP0 = 0;
   DMesons_daughterInfo__truthDBarcode_TRK_EFF_LOOSE_PP0 = 0;
   DMesons_daughterInfo__z0SinTheta_TRK_EFF_LOOSE_PP0 = 0;
   DMesons_daughterInfo__z0SinThetaPV_TRK_EFF_LOOSE_PP0 = 0;
   DMesons_decayType_TRK_EFF_LOOSE_PP0 = 0;
   DMesons_DeltaMass_TRK_EFF_LOOSE_PP0 = 0;
   DMesons_eta_TRK_EFF_LOOSE_PP0 = 0;
   DMesons_fitOutput__Charge_TRK_EFF_LOOSE_PP0 = 0;
   DMesons_fitOutput__Chi2_TRK_EFF_LOOSE_PP0 = 0;
   DMesons_fitOutput__Impact_TRK_EFF_LOOSE_PP0 = 0;
   DMesons_fitOutput__ImpactSignificance_TRK_EFF_LOOSE_PP0 = 0;
   DMesons_fitOutput__ImpactZ0SinTheta_TRK_EFF_LOOSE_PP0 = 0;
   DMesons_fitOutput__Lxy_TRK_EFF_LOOSE_PP0 = 0;
   DMesons_fitOutput__VertexPosition_TRK_EFF_LOOSE_PP0 = 0;
   DMesons_m_TRK_EFF_LOOSE_PP0 = 0;
   DMesons_mKpi1_TRK_EFF_LOOSE_PP0 = 0;
   DMesons_mKpi2_TRK_EFF_LOOSE_PP0 = 0;
   DMesons_mPhi1_TRK_EFF_LOOSE_PP0 = 0;
   DMesons_mPhi2_TRK_EFF_LOOSE_PP0 = 0;
   DMesons_pdgId_TRK_EFF_LOOSE_PP0 = 0;
   DMesons_phi_TRK_EFF_LOOSE_PP0 = 0;
   DMesons_pt_TRK_EFF_LOOSE_PP0 = 0;
   DMesons_ptcone40_TRK_EFF_LOOSE_PP0 = 0;
   DMesons_SlowPionD0_TRK_EFF_LOOSE_PP0 = 0;
   DMesons_SlowPionZ0SinTheta_TRK_EFF_LOOSE_PP0 = 0;
   DMesons_truthBarcode_TRK_EFF_LOOSE_PP0 = 0;
   MesonTracks_d0sig_TRK_EFF_LOOSE_PP0 = 0;
   MesonTracks_d0sigPV_TRK_EFF_LOOSE_PP0 = 0;
   MesonTracks_eta_TRK_EFF_LOOSE_PP0 = 0;
   MesonTracks_passTight_TRK_EFF_LOOSE_PP0 = 0;
   MesonTracks_phi_TRK_EFF_LOOSE_PP0 = 0;
   MesonTracks_pt_TRK_EFF_LOOSE_PP0 = 0;
   MesonTracks_trackId_TRK_EFF_LOOSE_PP0 = 0;
   MesonTracks_z0sinTheta_TRK_EFF_LOOSE_PP0 = 0;
   MesonTracks_truthMatchProbability_TRK_EFF_LOOSE_PP0 = 0;
   AntiKt6PV0TrackJets_pt_TRK_EFF_LOOSE_PP0 = 0;
   AntiKt6PV0TrackJets_eta_TRK_EFF_LOOSE_PP0 = 0;
   AntiKt6PV0TrackJets_phi_TRK_EFF_LOOSE_PP0 = 0;
   AntiKt6PV0TrackJets_m_TRK_EFF_LOOSE_PP0 = 0;
   AntiKt6PV0TrackJets_daughter__pt_TRK_EFF_LOOSE_PP0 = 0;
   AntiKt6PV0TrackJets_daughter__eta_TRK_EFF_LOOSE_PP0 = 0;
   AntiKt6PV0TrackJets_daughter__phi_TRK_EFF_LOOSE_PP0 = 0;
   AntiKt6PV0TrackJets_daughter__trackId_TRK_EFF_LOOSE_PP0 = 0;
   AntiKt8PV0TrackJets_pt_TRK_EFF_LOOSE_PP0 = 0;
   AntiKt8PV0TrackJets_eta_TRK_EFF_LOOSE_PP0 = 0;
   AntiKt8PV0TrackJets_phi_TRK_EFF_LOOSE_PP0 = 0;
   AntiKt8PV0TrackJets_m_TRK_EFF_LOOSE_PP0 = 0;
   AntiKt8PV0TrackJets_daughter__pt_TRK_EFF_LOOSE_PP0 = 0;
   AntiKt8PV0TrackJets_daughter__eta_TRK_EFF_LOOSE_PP0 = 0;
   AntiKt8PV0TrackJets_daughter__phi_TRK_EFF_LOOSE_PP0 = 0;
   AntiKt8PV0TrackJets_daughter__trackId_TRK_EFF_LOOSE_PP0 = 0;
   AntiKt10PV0TrackJets_pt_TRK_EFF_LOOSE_PP0 = 0;
   AntiKt10PV0TrackJets_eta_TRK_EFF_LOOSE_PP0 = 0;
   AntiKt10PV0TrackJets_phi_TRK_EFF_LOOSE_PP0 = 0;
   AntiKt10PV0TrackJets_m_TRK_EFF_LOOSE_PP0 = 0;
   AntiKt10PV0TrackJets_daughter__pt_TRK_EFF_LOOSE_PP0 = 0;
   AntiKt10PV0TrackJets_daughter__eta_TRK_EFF_LOOSE_PP0 = 0;
   AntiKt10PV0TrackJets_daughter__phi_TRK_EFF_LOOSE_PP0 = 0;
   AntiKt10PV0TrackJets_daughter__trackId_TRK_EFF_LOOSE_PP0 = 0;
   AntiKtVR30Rmax4Rmin02TrackJets_pt_TRK_EFF_LOOSE_PP0 = 0;
   AntiKtVR30Rmax4Rmin02TrackJets_eta_TRK_EFF_LOOSE_PP0 = 0;
   AntiKtVR30Rmax4Rmin02TrackJets_phi_TRK_EFF_LOOSE_PP0 = 0;
   AntiKtVR30Rmax4Rmin02TrackJets_m_TRK_EFF_LOOSE_PP0 = 0;
   AntiKtVR30Rmax4Rmin02TrackJets_daughter__pt_TRK_EFF_LOOSE_PP0 = 0;
   AntiKtVR30Rmax4Rmin02TrackJets_daughter__eta_TRK_EFF_LOOSE_PP0 = 0;
   AntiKtVR30Rmax4Rmin02TrackJets_daughter__phi_TRK_EFF_LOOSE_PP0 = 0;
   AntiKtVR30Rmax4Rmin02TrackJets_daughter__trackId_TRK_EFF_LOOSE_PP0 = 0;
   DMesons_costhetastar_TRK_FAKE_RATE_LOOSE = 0;
   DMesons_D0Index_TRK_FAKE_RATE_LOOSE = 0;
   DMesons_daughterInfo__eta_TRK_FAKE_RATE_LOOSE = 0;
   DMesons_daughterInfo__passTight_TRK_FAKE_RATE_LOOSE = 0;
   DMesons_daughterInfo__pdgId_TRK_FAKE_RATE_LOOSE = 0;
   DMesons_daughterInfo__phi_TRK_FAKE_RATE_LOOSE = 0;
   DMesons_daughterInfo__pt_TRK_FAKE_RATE_LOOSE = 0;
   DMesons_daughterInfo__trackId_TRK_FAKE_RATE_LOOSE = 0;
   DMesons_daughterInfo__truthBarcode_TRK_FAKE_RATE_LOOSE = 0;
   DMesons_daughterInfo__truthDBarcode_TRK_FAKE_RATE_LOOSE = 0;
   DMesons_daughterInfo__z0SinTheta_TRK_FAKE_RATE_LOOSE = 0;
   DMesons_daughterInfo__z0SinThetaPV_TRK_FAKE_RATE_LOOSE = 0;
   DMesons_decayType_TRK_FAKE_RATE_LOOSE = 0;
   DMesons_DeltaMass_TRK_FAKE_RATE_LOOSE = 0;
   DMesons_eta_TRK_FAKE_RATE_LOOSE = 0;
   DMesons_fitOutput__Charge_TRK_FAKE_RATE_LOOSE = 0;
   DMesons_fitOutput__Chi2_TRK_FAKE_RATE_LOOSE = 0;
   DMesons_fitOutput__Impact_TRK_FAKE_RATE_LOOSE = 0;
   DMesons_fitOutput__ImpactSignificance_TRK_FAKE_RATE_LOOSE = 0;
   DMesons_fitOutput__ImpactZ0SinTheta_TRK_FAKE_RATE_LOOSE = 0;
   DMesons_fitOutput__Lxy_TRK_FAKE_RATE_LOOSE = 0;
   DMesons_fitOutput__VertexPosition_TRK_FAKE_RATE_LOOSE = 0;
   DMesons_m_TRK_FAKE_RATE_LOOSE = 0;
   DMesons_mKpi1_TRK_FAKE_RATE_LOOSE = 0;
   DMesons_mKpi2_TRK_FAKE_RATE_LOOSE = 0;
   DMesons_mPhi1_TRK_FAKE_RATE_LOOSE = 0;
   DMesons_mPhi2_TRK_FAKE_RATE_LOOSE = 0;
   DMesons_pdgId_TRK_FAKE_RATE_LOOSE = 0;
   DMesons_phi_TRK_FAKE_RATE_LOOSE = 0;
   DMesons_pt_TRK_FAKE_RATE_LOOSE = 0;
   DMesons_ptcone40_TRK_FAKE_RATE_LOOSE = 0;
   DMesons_SlowPionD0_TRK_FAKE_RATE_LOOSE = 0;
   DMesons_SlowPionZ0SinTheta_TRK_FAKE_RATE_LOOSE = 0;
   DMesons_truthBarcode_TRK_FAKE_RATE_LOOSE = 0;
   MesonTracks_d0sig_TRK_FAKE_RATE_LOOSE = 0;
   MesonTracks_d0sigPV_TRK_FAKE_RATE_LOOSE = 0;
   MesonTracks_eta_TRK_FAKE_RATE_LOOSE = 0;
   MesonTracks_passTight_TRK_FAKE_RATE_LOOSE = 0;
   MesonTracks_phi_TRK_FAKE_RATE_LOOSE = 0;
   MesonTracks_pt_TRK_FAKE_RATE_LOOSE = 0;
   MesonTracks_trackId_TRK_FAKE_RATE_LOOSE = 0;
   MesonTracks_z0sinTheta_TRK_FAKE_RATE_LOOSE = 0;
   MesonTracks_truthMatchProbability_TRK_FAKE_RATE_LOOSE = 0;
   AntiKt6PV0TrackJets_pt_TRK_FAKE_RATE_LOOSE = 0;
   AntiKt6PV0TrackJets_eta_TRK_FAKE_RATE_LOOSE = 0;
   AntiKt6PV0TrackJets_phi_TRK_FAKE_RATE_LOOSE = 0;
   AntiKt6PV0TrackJets_m_TRK_FAKE_RATE_LOOSE = 0;
   AntiKt6PV0TrackJets_daughter__pt_TRK_FAKE_RATE_LOOSE = 0;
   AntiKt6PV0TrackJets_daughter__eta_TRK_FAKE_RATE_LOOSE = 0;
   AntiKt6PV0TrackJets_daughter__phi_TRK_FAKE_RATE_LOOSE = 0;
   AntiKt6PV0TrackJets_daughter__trackId_TRK_FAKE_RATE_LOOSE = 0;
   AntiKt8PV0TrackJets_pt_TRK_FAKE_RATE_LOOSE = 0;
   AntiKt8PV0TrackJets_eta_TRK_FAKE_RATE_LOOSE = 0;
   AntiKt8PV0TrackJets_phi_TRK_FAKE_RATE_LOOSE = 0;
   AntiKt8PV0TrackJets_m_TRK_FAKE_RATE_LOOSE = 0;
   AntiKt8PV0TrackJets_daughter__pt_TRK_FAKE_RATE_LOOSE = 0;
   AntiKt8PV0TrackJets_daughter__eta_TRK_FAKE_RATE_LOOSE = 0;
   AntiKt8PV0TrackJets_daughter__phi_TRK_FAKE_RATE_LOOSE = 0;
   AntiKt8PV0TrackJets_daughter__trackId_TRK_FAKE_RATE_LOOSE = 0;
   AntiKt10PV0TrackJets_pt_TRK_FAKE_RATE_LOOSE = 0;
   AntiKt10PV0TrackJets_eta_TRK_FAKE_RATE_LOOSE = 0;
   AntiKt10PV0TrackJets_phi_TRK_FAKE_RATE_LOOSE = 0;
   AntiKt10PV0TrackJets_m_TRK_FAKE_RATE_LOOSE = 0;
   AntiKt10PV0TrackJets_daughter__pt_TRK_FAKE_RATE_LOOSE = 0;
   AntiKt10PV0TrackJets_daughter__eta_TRK_FAKE_RATE_LOOSE = 0;
   AntiKt10PV0TrackJets_daughter__phi_TRK_FAKE_RATE_LOOSE = 0;
   AntiKt10PV0TrackJets_daughter__trackId_TRK_FAKE_RATE_LOOSE = 0;
   AntiKtVR30Rmax4Rmin02TrackJets_pt_TRK_FAKE_RATE_LOOSE = 0;
   AntiKtVR30Rmax4Rmin02TrackJets_eta_TRK_FAKE_RATE_LOOSE = 0;
   AntiKtVR30Rmax4Rmin02TrackJets_phi_TRK_FAKE_RATE_LOOSE = 0;
   AntiKtVR30Rmax4Rmin02TrackJets_m_TRK_FAKE_RATE_LOOSE = 0;
   AntiKtVR30Rmax4Rmin02TrackJets_daughter__pt_TRK_FAKE_RATE_LOOSE = 0;
   AntiKtVR30Rmax4Rmin02TrackJets_daughter__eta_TRK_FAKE_RATE_LOOSE = 0;
   AntiKtVR30Rmax4Rmin02TrackJets_daughter__phi_TRK_FAKE_RATE_LOOSE = 0;
   AntiKtVR30Rmax4Rmin02TrackJets_daughter__trackId_TRK_FAKE_RATE_LOOSE = 0;
   DMesons_costhetastar_TRK_RES_D0_DEAD = 0;
   DMesons_D0Index_TRK_RES_D0_DEAD = 0;
   DMesons_daughterInfo__eta_TRK_RES_D0_DEAD = 0;
   DMesons_daughterInfo__passTight_TRK_RES_D0_DEAD = 0;
   DMesons_daughterInfo__pdgId_TRK_RES_D0_DEAD = 0;
   DMesons_daughterInfo__phi_TRK_RES_D0_DEAD = 0;
   DMesons_daughterInfo__pt_TRK_RES_D0_DEAD = 0;
   DMesons_daughterInfo__trackId_TRK_RES_D0_DEAD = 0;
   DMesons_daughterInfo__truthBarcode_TRK_RES_D0_DEAD = 0;
   DMesons_daughterInfo__truthDBarcode_TRK_RES_D0_DEAD = 0;
   DMesons_daughterInfo__z0SinTheta_TRK_RES_D0_DEAD = 0;
   DMesons_daughterInfo__z0SinThetaPV_TRK_RES_D0_DEAD = 0;
   DMesons_decayType_TRK_RES_D0_DEAD = 0;
   DMesons_DeltaMass_TRK_RES_D0_DEAD = 0;
   DMesons_eta_TRK_RES_D0_DEAD = 0;
   DMesons_fitOutput__Charge_TRK_RES_D0_DEAD = 0;
   DMesons_fitOutput__Chi2_TRK_RES_D0_DEAD = 0;
   DMesons_fitOutput__Impact_TRK_RES_D0_DEAD = 0;
   DMesons_fitOutput__ImpactSignificance_TRK_RES_D0_DEAD = 0;
   DMesons_fitOutput__ImpactZ0SinTheta_TRK_RES_D0_DEAD = 0;
   DMesons_fitOutput__Lxy_TRK_RES_D0_DEAD = 0;
   DMesons_fitOutput__VertexPosition_TRK_RES_D0_DEAD = 0;
   DMesons_m_TRK_RES_D0_DEAD = 0;
   DMesons_mKpi1_TRK_RES_D0_DEAD = 0;
   DMesons_mKpi2_TRK_RES_D0_DEAD = 0;
   DMesons_mPhi1_TRK_RES_D0_DEAD = 0;
   DMesons_mPhi2_TRK_RES_D0_DEAD = 0;
   DMesons_pdgId_TRK_RES_D0_DEAD = 0;
   DMesons_phi_TRK_RES_D0_DEAD = 0;
   DMesons_pt_TRK_RES_D0_DEAD = 0;
   DMesons_ptcone40_TRK_RES_D0_DEAD = 0;
   DMesons_SlowPionD0_TRK_RES_D0_DEAD = 0;
   DMesons_SlowPionZ0SinTheta_TRK_RES_D0_DEAD = 0;
   DMesons_truthBarcode_TRK_RES_D0_DEAD = 0;
   MesonTracks_d0sig_TRK_RES_D0_DEAD = 0;
   MesonTracks_d0sigPV_TRK_RES_D0_DEAD = 0;
   MesonTracks_eta_TRK_RES_D0_DEAD = 0;
   MesonTracks_passTight_TRK_RES_D0_DEAD = 0;
   MesonTracks_phi_TRK_RES_D0_DEAD = 0;
   MesonTracks_pt_TRK_RES_D0_DEAD = 0;
   MesonTracks_trackId_TRK_RES_D0_DEAD = 0;
   MesonTracks_z0sinTheta_TRK_RES_D0_DEAD = 0;
   MesonTracks_truthMatchProbability_TRK_RES_D0_DEAD = 0;
   AntiKt6PV0TrackJets_pt_TRK_RES_D0_DEAD = 0;
   AntiKt6PV0TrackJets_eta_TRK_RES_D0_DEAD = 0;
   AntiKt6PV0TrackJets_phi_TRK_RES_D0_DEAD = 0;
   AntiKt6PV0TrackJets_m_TRK_RES_D0_DEAD = 0;
   AntiKt6PV0TrackJets_daughter__pt_TRK_RES_D0_DEAD = 0;
   AntiKt6PV0TrackJets_daughter__eta_TRK_RES_D0_DEAD = 0;
   AntiKt6PV0TrackJets_daughter__phi_TRK_RES_D0_DEAD = 0;
   AntiKt6PV0TrackJets_daughter__trackId_TRK_RES_D0_DEAD = 0;
   AntiKt8PV0TrackJets_pt_TRK_RES_D0_DEAD = 0;
   AntiKt8PV0TrackJets_eta_TRK_RES_D0_DEAD = 0;
   AntiKt8PV0TrackJets_phi_TRK_RES_D0_DEAD = 0;
   AntiKt8PV0TrackJets_m_TRK_RES_D0_DEAD = 0;
   AntiKt8PV0TrackJets_daughter__pt_TRK_RES_D0_DEAD = 0;
   AntiKt8PV0TrackJets_daughter__eta_TRK_RES_D0_DEAD = 0;
   AntiKt8PV0TrackJets_daughter__phi_TRK_RES_D0_DEAD = 0;
   AntiKt8PV0TrackJets_daughter__trackId_TRK_RES_D0_DEAD = 0;
   AntiKt10PV0TrackJets_pt_TRK_RES_D0_DEAD = 0;
   AntiKt10PV0TrackJets_eta_TRK_RES_D0_DEAD = 0;
   AntiKt10PV0TrackJets_phi_TRK_RES_D0_DEAD = 0;
   AntiKt10PV0TrackJets_m_TRK_RES_D0_DEAD = 0;
   AntiKt10PV0TrackJets_daughter__pt_TRK_RES_D0_DEAD = 0;
   AntiKt10PV0TrackJets_daughter__eta_TRK_RES_D0_DEAD = 0;
   AntiKt10PV0TrackJets_daughter__phi_TRK_RES_D0_DEAD = 0;
   AntiKt10PV0TrackJets_daughter__trackId_TRK_RES_D0_DEAD = 0;
   AntiKtVR30Rmax4Rmin02TrackJets_pt_TRK_RES_D0_DEAD = 0;
   AntiKtVR30Rmax4Rmin02TrackJets_eta_TRK_RES_D0_DEAD = 0;
   AntiKtVR30Rmax4Rmin02TrackJets_phi_TRK_RES_D0_DEAD = 0;
   AntiKtVR30Rmax4Rmin02TrackJets_m_TRK_RES_D0_DEAD = 0;
   AntiKtVR30Rmax4Rmin02TrackJets_daughter__pt_TRK_RES_D0_DEAD = 0;
   AntiKtVR30Rmax4Rmin02TrackJets_daughter__eta_TRK_RES_D0_DEAD = 0;
   AntiKtVR30Rmax4Rmin02TrackJets_daughter__phi_TRK_RES_D0_DEAD = 0;
   AntiKtVR30Rmax4Rmin02TrackJets_daughter__trackId_TRK_RES_D0_DEAD = 0;
   DMesons_costhetastar_TRK_RES_D0_MEAS = 0;
   DMesons_D0Index_TRK_RES_D0_MEAS = 0;
   DMesons_daughterInfo__eta_TRK_RES_D0_MEAS = 0;
   DMesons_daughterInfo__passTight_TRK_RES_D0_MEAS = 0;
   DMesons_daughterInfo__pdgId_TRK_RES_D0_MEAS = 0;
   DMesons_daughterInfo__phi_TRK_RES_D0_MEAS = 0;
   DMesons_daughterInfo__pt_TRK_RES_D0_MEAS = 0;
   DMesons_daughterInfo__trackId_TRK_RES_D0_MEAS = 0;
   DMesons_daughterInfo__truthBarcode_TRK_RES_D0_MEAS = 0;
   DMesons_daughterInfo__truthDBarcode_TRK_RES_D0_MEAS = 0;
   DMesons_daughterInfo__z0SinTheta_TRK_RES_D0_MEAS = 0;
   DMesons_daughterInfo__z0SinThetaPV_TRK_RES_D0_MEAS = 0;
   DMesons_decayType_TRK_RES_D0_MEAS = 0;
   DMesons_DeltaMass_TRK_RES_D0_MEAS = 0;
   DMesons_eta_TRK_RES_D0_MEAS = 0;
   DMesons_fitOutput__Charge_TRK_RES_D0_MEAS = 0;
   DMesons_fitOutput__Chi2_TRK_RES_D0_MEAS = 0;
   DMesons_fitOutput__Impact_TRK_RES_D0_MEAS = 0;
   DMesons_fitOutput__ImpactSignificance_TRK_RES_D0_MEAS = 0;
   DMesons_fitOutput__ImpactZ0SinTheta_TRK_RES_D0_MEAS = 0;
   DMesons_fitOutput__Lxy_TRK_RES_D0_MEAS = 0;
   DMesons_fitOutput__VertexPosition_TRK_RES_D0_MEAS = 0;
   DMesons_m_TRK_RES_D0_MEAS = 0;
   DMesons_mKpi1_TRK_RES_D0_MEAS = 0;
   DMesons_mKpi2_TRK_RES_D0_MEAS = 0;
   DMesons_mPhi1_TRK_RES_D0_MEAS = 0;
   DMesons_mPhi2_TRK_RES_D0_MEAS = 0;
   DMesons_pdgId_TRK_RES_D0_MEAS = 0;
   DMesons_phi_TRK_RES_D0_MEAS = 0;
   DMesons_pt_TRK_RES_D0_MEAS = 0;
   DMesons_ptcone40_TRK_RES_D0_MEAS = 0;
   DMesons_SlowPionD0_TRK_RES_D0_MEAS = 0;
   DMesons_SlowPionZ0SinTheta_TRK_RES_D0_MEAS = 0;
   DMesons_truthBarcode_TRK_RES_D0_MEAS = 0;
   MesonTracks_d0sig_TRK_RES_D0_MEAS = 0;
   MesonTracks_d0sigPV_TRK_RES_D0_MEAS = 0;
   MesonTracks_eta_TRK_RES_D0_MEAS = 0;
   MesonTracks_passTight_TRK_RES_D0_MEAS = 0;
   MesonTracks_phi_TRK_RES_D0_MEAS = 0;
   MesonTracks_pt_TRK_RES_D0_MEAS = 0;
   MesonTracks_trackId_TRK_RES_D0_MEAS = 0;
   MesonTracks_z0sinTheta_TRK_RES_D0_MEAS = 0;
   MesonTracks_truthMatchProbability_TRK_RES_D0_MEAS = 0;
   AntiKt6PV0TrackJets_pt_TRK_RES_D0_MEAS = 0;
   AntiKt6PV0TrackJets_eta_TRK_RES_D0_MEAS = 0;
   AntiKt6PV0TrackJets_phi_TRK_RES_D0_MEAS = 0;
   AntiKt6PV0TrackJets_m_TRK_RES_D0_MEAS = 0;
   AntiKt6PV0TrackJets_daughter__pt_TRK_RES_D0_MEAS = 0;
   AntiKt6PV0TrackJets_daughter__eta_TRK_RES_D0_MEAS = 0;
   AntiKt6PV0TrackJets_daughter__phi_TRK_RES_D0_MEAS = 0;
   AntiKt6PV0TrackJets_daughter__trackId_TRK_RES_D0_MEAS = 0;
   AntiKt8PV0TrackJets_pt_TRK_RES_D0_MEAS = 0;
   AntiKt8PV0TrackJets_eta_TRK_RES_D0_MEAS = 0;
   AntiKt8PV0TrackJets_phi_TRK_RES_D0_MEAS = 0;
   AntiKt8PV0TrackJets_m_TRK_RES_D0_MEAS = 0;
   AntiKt8PV0TrackJets_daughter__pt_TRK_RES_D0_MEAS = 0;
   AntiKt8PV0TrackJets_daughter__eta_TRK_RES_D0_MEAS = 0;
   AntiKt8PV0TrackJets_daughter__phi_TRK_RES_D0_MEAS = 0;
   AntiKt8PV0TrackJets_daughter__trackId_TRK_RES_D0_MEAS = 0;
   AntiKt10PV0TrackJets_pt_TRK_RES_D0_MEAS = 0;
   AntiKt10PV0TrackJets_eta_TRK_RES_D0_MEAS = 0;
   AntiKt10PV0TrackJets_phi_TRK_RES_D0_MEAS = 0;
   AntiKt10PV0TrackJets_m_TRK_RES_D0_MEAS = 0;
   AntiKt10PV0TrackJets_daughter__pt_TRK_RES_D0_MEAS = 0;
   AntiKt10PV0TrackJets_daughter__eta_TRK_RES_D0_MEAS = 0;
   AntiKt10PV0TrackJets_daughter__phi_TRK_RES_D0_MEAS = 0;
   AntiKt10PV0TrackJets_daughter__trackId_TRK_RES_D0_MEAS = 0;
   AntiKtVR30Rmax4Rmin02TrackJets_pt_TRK_RES_D0_MEAS = 0;
   AntiKtVR30Rmax4Rmin02TrackJets_eta_TRK_RES_D0_MEAS = 0;
   AntiKtVR30Rmax4Rmin02TrackJets_phi_TRK_RES_D0_MEAS = 0;
   AntiKtVR30Rmax4Rmin02TrackJets_m_TRK_RES_D0_MEAS = 0;
   AntiKtVR30Rmax4Rmin02TrackJets_daughter__pt_TRK_RES_D0_MEAS = 0;
   AntiKtVR30Rmax4Rmin02TrackJets_daughter__eta_TRK_RES_D0_MEAS = 0;
   AntiKtVR30Rmax4Rmin02TrackJets_daughter__phi_TRK_RES_D0_MEAS = 0;
   AntiKtVR30Rmax4Rmin02TrackJets_daughter__trackId_TRK_RES_D0_MEAS = 0;
   DMesons_costhetastar_TRK_RES_Z0_DEAD = 0;
   DMesons_D0Index_TRK_RES_Z0_DEAD = 0;
   DMesons_daughterInfo__eta_TRK_RES_Z0_DEAD = 0;
   DMesons_daughterInfo__passTight_TRK_RES_Z0_DEAD = 0;
   DMesons_daughterInfo__pdgId_TRK_RES_Z0_DEAD = 0;
   DMesons_daughterInfo__phi_TRK_RES_Z0_DEAD = 0;
   DMesons_daughterInfo__pt_TRK_RES_Z0_DEAD = 0;
   DMesons_daughterInfo__trackId_TRK_RES_Z0_DEAD = 0;
   DMesons_daughterInfo__truthBarcode_TRK_RES_Z0_DEAD = 0;
   DMesons_daughterInfo__truthDBarcode_TRK_RES_Z0_DEAD = 0;
   DMesons_daughterInfo__z0SinTheta_TRK_RES_Z0_DEAD = 0;
   DMesons_daughterInfo__z0SinThetaPV_TRK_RES_Z0_DEAD = 0;
   DMesons_decayType_TRK_RES_Z0_DEAD = 0;
   DMesons_DeltaMass_TRK_RES_Z0_DEAD = 0;
   DMesons_eta_TRK_RES_Z0_DEAD = 0;
   DMesons_fitOutput__Charge_TRK_RES_Z0_DEAD = 0;
   DMesons_fitOutput__Chi2_TRK_RES_Z0_DEAD = 0;
   DMesons_fitOutput__Impact_TRK_RES_Z0_DEAD = 0;
   DMesons_fitOutput__ImpactSignificance_TRK_RES_Z0_DEAD = 0;
   DMesons_fitOutput__ImpactZ0SinTheta_TRK_RES_Z0_DEAD = 0;
   DMesons_fitOutput__Lxy_TRK_RES_Z0_DEAD = 0;
   DMesons_fitOutput__VertexPosition_TRK_RES_Z0_DEAD = 0;
   DMesons_m_TRK_RES_Z0_DEAD = 0;
   DMesons_mKpi1_TRK_RES_Z0_DEAD = 0;
   DMesons_mKpi2_TRK_RES_Z0_DEAD = 0;
   DMesons_mPhi1_TRK_RES_Z0_DEAD = 0;
   DMesons_mPhi2_TRK_RES_Z0_DEAD = 0;
   DMesons_pdgId_TRK_RES_Z0_DEAD = 0;
   DMesons_phi_TRK_RES_Z0_DEAD = 0;
   DMesons_pt_TRK_RES_Z0_DEAD = 0;
   DMesons_ptcone40_TRK_RES_Z0_DEAD = 0;
   DMesons_SlowPionD0_TRK_RES_Z0_DEAD = 0;
   DMesons_SlowPionZ0SinTheta_TRK_RES_Z0_DEAD = 0;
   DMesons_truthBarcode_TRK_RES_Z0_DEAD = 0;
   MesonTracks_d0sig_TRK_RES_Z0_DEAD = 0;
   MesonTracks_d0sigPV_TRK_RES_Z0_DEAD = 0;
   MesonTracks_eta_TRK_RES_Z0_DEAD = 0;
   MesonTracks_passTight_TRK_RES_Z0_DEAD = 0;
   MesonTracks_phi_TRK_RES_Z0_DEAD = 0;
   MesonTracks_pt_TRK_RES_Z0_DEAD = 0;
   MesonTracks_trackId_TRK_RES_Z0_DEAD = 0;
   MesonTracks_z0sinTheta_TRK_RES_Z0_DEAD = 0;
   MesonTracks_truthMatchProbability_TRK_RES_Z0_DEAD = 0;
   AntiKt6PV0TrackJets_pt_TRK_RES_Z0_DEAD = 0;
   AntiKt6PV0TrackJets_eta_TRK_RES_Z0_DEAD = 0;
   AntiKt6PV0TrackJets_phi_TRK_RES_Z0_DEAD = 0;
   AntiKt6PV0TrackJets_m_TRK_RES_Z0_DEAD = 0;
   AntiKt6PV0TrackJets_daughter__pt_TRK_RES_Z0_DEAD = 0;
   AntiKt6PV0TrackJets_daughter__eta_TRK_RES_Z0_DEAD = 0;
   AntiKt6PV0TrackJets_daughter__phi_TRK_RES_Z0_DEAD = 0;
   AntiKt6PV0TrackJets_daughter__trackId_TRK_RES_Z0_DEAD = 0;
   AntiKt8PV0TrackJets_pt_TRK_RES_Z0_DEAD = 0;
   AntiKt8PV0TrackJets_eta_TRK_RES_Z0_DEAD = 0;
   AntiKt8PV0TrackJets_phi_TRK_RES_Z0_DEAD = 0;
   AntiKt8PV0TrackJets_m_TRK_RES_Z0_DEAD = 0;
   AntiKt8PV0TrackJets_daughter__pt_TRK_RES_Z0_DEAD = 0;
   AntiKt8PV0TrackJets_daughter__eta_TRK_RES_Z0_DEAD = 0;
   AntiKt8PV0TrackJets_daughter__phi_TRK_RES_Z0_DEAD = 0;
   AntiKt8PV0TrackJets_daughter__trackId_TRK_RES_Z0_DEAD = 0;
   AntiKt10PV0TrackJets_pt_TRK_RES_Z0_DEAD = 0;
   AntiKt10PV0TrackJets_eta_TRK_RES_Z0_DEAD = 0;
   AntiKt10PV0TrackJets_phi_TRK_RES_Z0_DEAD = 0;
   AntiKt10PV0TrackJets_m_TRK_RES_Z0_DEAD = 0;
   AntiKt10PV0TrackJets_daughter__pt_TRK_RES_Z0_DEAD = 0;
   AntiKt10PV0TrackJets_daughter__eta_TRK_RES_Z0_DEAD = 0;
   AntiKt10PV0TrackJets_daughter__phi_TRK_RES_Z0_DEAD = 0;
   AntiKt10PV0TrackJets_daughter__trackId_TRK_RES_Z0_DEAD = 0;
   AntiKtVR30Rmax4Rmin02TrackJets_pt_TRK_RES_Z0_DEAD = 0;
   AntiKtVR30Rmax4Rmin02TrackJets_eta_TRK_RES_Z0_DEAD = 0;
   AntiKtVR30Rmax4Rmin02TrackJets_phi_TRK_RES_Z0_DEAD = 0;
   AntiKtVR30Rmax4Rmin02TrackJets_m_TRK_RES_Z0_DEAD = 0;
   AntiKtVR30Rmax4Rmin02TrackJets_daughter__pt_TRK_RES_Z0_DEAD = 0;
   AntiKtVR30Rmax4Rmin02TrackJets_daughter__eta_TRK_RES_Z0_DEAD = 0;
   AntiKtVR30Rmax4Rmin02TrackJets_daughter__phi_TRK_RES_Z0_DEAD = 0;
   AntiKtVR30Rmax4Rmin02TrackJets_daughter__trackId_TRK_RES_Z0_DEAD = 0;
   DMesons_costhetastar_TRK_RES_Z0_MEAS = 0;
   DMesons_D0Index_TRK_RES_Z0_MEAS = 0;
   DMesons_daughterInfo__eta_TRK_RES_Z0_MEAS = 0;
   DMesons_daughterInfo__passTight_TRK_RES_Z0_MEAS = 0;
   DMesons_daughterInfo__pdgId_TRK_RES_Z0_MEAS = 0;
   DMesons_daughterInfo__phi_TRK_RES_Z0_MEAS = 0;
   DMesons_daughterInfo__pt_TRK_RES_Z0_MEAS = 0;
   DMesons_daughterInfo__trackId_TRK_RES_Z0_MEAS = 0;
   DMesons_daughterInfo__truthBarcode_TRK_RES_Z0_MEAS = 0;
   DMesons_daughterInfo__truthDBarcode_TRK_RES_Z0_MEAS = 0;
   DMesons_daughterInfo__z0SinTheta_TRK_RES_Z0_MEAS = 0;
   DMesons_daughterInfo__z0SinThetaPV_TRK_RES_Z0_MEAS = 0;
   DMesons_decayType_TRK_RES_Z0_MEAS = 0;
   DMesons_DeltaMass_TRK_RES_Z0_MEAS = 0;
   DMesons_eta_TRK_RES_Z0_MEAS = 0;
   DMesons_fitOutput__Charge_TRK_RES_Z0_MEAS = 0;
   DMesons_fitOutput__Chi2_TRK_RES_Z0_MEAS = 0;
   DMesons_fitOutput__Impact_TRK_RES_Z0_MEAS = 0;
   DMesons_fitOutput__ImpactSignificance_TRK_RES_Z0_MEAS = 0;
   DMesons_fitOutput__ImpactZ0SinTheta_TRK_RES_Z0_MEAS = 0;
   DMesons_fitOutput__Lxy_TRK_RES_Z0_MEAS = 0;
   DMesons_fitOutput__VertexPosition_TRK_RES_Z0_MEAS = 0;
   DMesons_m_TRK_RES_Z0_MEAS = 0;
   DMesons_mKpi1_TRK_RES_Z0_MEAS = 0;
   DMesons_mKpi2_TRK_RES_Z0_MEAS = 0;
   DMesons_mPhi1_TRK_RES_Z0_MEAS = 0;
   DMesons_mPhi2_TRK_RES_Z0_MEAS = 0;
   DMesons_pdgId_TRK_RES_Z0_MEAS = 0;
   DMesons_phi_TRK_RES_Z0_MEAS = 0;
   DMesons_pt_TRK_RES_Z0_MEAS = 0;
   DMesons_ptcone40_TRK_RES_Z0_MEAS = 0;
   DMesons_SlowPionD0_TRK_RES_Z0_MEAS = 0;
   DMesons_SlowPionZ0SinTheta_TRK_RES_Z0_MEAS = 0;
   DMesons_truthBarcode_TRK_RES_Z0_MEAS = 0;
   MesonTracks_d0sig_TRK_RES_Z0_MEAS = 0;
   MesonTracks_d0sigPV_TRK_RES_Z0_MEAS = 0;
   MesonTracks_eta_TRK_RES_Z0_MEAS = 0;
   MesonTracks_passTight_TRK_RES_Z0_MEAS = 0;
   MesonTracks_phi_TRK_RES_Z0_MEAS = 0;
   MesonTracks_pt_TRK_RES_Z0_MEAS = 0;
   MesonTracks_trackId_TRK_RES_Z0_MEAS = 0;
   MesonTracks_z0sinTheta_TRK_RES_Z0_MEAS = 0;
   MesonTracks_truthMatchProbability_TRK_RES_Z0_MEAS = 0;
   AntiKt6PV0TrackJets_pt_TRK_RES_Z0_MEAS = 0;
   AntiKt6PV0TrackJets_eta_TRK_RES_Z0_MEAS = 0;
   AntiKt6PV0TrackJets_phi_TRK_RES_Z0_MEAS = 0;
   AntiKt6PV0TrackJets_m_TRK_RES_Z0_MEAS = 0;
   AntiKt6PV0TrackJets_daughter__pt_TRK_RES_Z0_MEAS = 0;
   AntiKt6PV0TrackJets_daughter__eta_TRK_RES_Z0_MEAS = 0;
   AntiKt6PV0TrackJets_daughter__phi_TRK_RES_Z0_MEAS = 0;
   AntiKt6PV0TrackJets_daughter__trackId_TRK_RES_Z0_MEAS = 0;
   AntiKt8PV0TrackJets_pt_TRK_RES_Z0_MEAS = 0;
   AntiKt8PV0TrackJets_eta_TRK_RES_Z0_MEAS = 0;
   AntiKt8PV0TrackJets_phi_TRK_RES_Z0_MEAS = 0;
   AntiKt8PV0TrackJets_m_TRK_RES_Z0_MEAS = 0;
   AntiKt8PV0TrackJets_daughter__pt_TRK_RES_Z0_MEAS = 0;
   AntiKt8PV0TrackJets_daughter__eta_TRK_RES_Z0_MEAS = 0;
   AntiKt8PV0TrackJets_daughter__phi_TRK_RES_Z0_MEAS = 0;
   AntiKt8PV0TrackJets_daughter__trackId_TRK_RES_Z0_MEAS = 0;
   AntiKt10PV0TrackJets_pt_TRK_RES_Z0_MEAS = 0;
   AntiKt10PV0TrackJets_eta_TRK_RES_Z0_MEAS = 0;
   AntiKt10PV0TrackJets_phi_TRK_RES_Z0_MEAS = 0;
   AntiKt10PV0TrackJets_m_TRK_RES_Z0_MEAS = 0;
   AntiKt10PV0TrackJets_daughter__pt_TRK_RES_Z0_MEAS = 0;
   AntiKt10PV0TrackJets_daughter__eta_TRK_RES_Z0_MEAS = 0;
   AntiKt10PV0TrackJets_daughter__phi_TRK_RES_Z0_MEAS = 0;
   AntiKt10PV0TrackJets_daughter__trackId_TRK_RES_Z0_MEAS = 0;
   AntiKtVR30Rmax4Rmin02TrackJets_pt_TRK_RES_Z0_MEAS = 0;
   AntiKtVR30Rmax4Rmin02TrackJets_eta_TRK_RES_Z0_MEAS = 0;
   AntiKtVR30Rmax4Rmin02TrackJets_phi_TRK_RES_Z0_MEAS = 0;
   AntiKtVR30Rmax4Rmin02TrackJets_m_TRK_RES_Z0_MEAS = 0;
   AntiKtVR30Rmax4Rmin02TrackJets_daughter__pt_TRK_RES_Z0_MEAS = 0;
   AntiKtVR30Rmax4Rmin02TrackJets_daughter__eta_TRK_RES_Z0_MEAS = 0;
   AntiKtVR30Rmax4Rmin02TrackJets_daughter__phi_TRK_RES_Z0_MEAS = 0;
   AntiKtVR30Rmax4Rmin02TrackJets_daughter__trackId_TRK_RES_Z0_MEAS = 0;
   // Set branch addresses and branch pointers
   if (!tree) return;
   fChain = tree;
   fCurrent = -1;
   fChain->SetMakeClass(1);

   fChain->SetBranchAddress("EventInfo_eventNumber", &EventInfo_eventNumber, &b_EventInfo_eventNumber);
   fChain->SetBranchAddress("EventInfo_RandomRunNumber", &EventInfo_RandomRunNumber, &b_EventInfo_RandomRunNumber);
   fChain->SetBranchAddress("EventInfo_PileupWeight_NOSYS", &EventInfo_PileupWeight_NOSYS, &b_EventInfo_PileupWeight_NOSYS);
   fChain->SetBranchAddress("EventInfo_correctedScaled_averageInteractionsPerCrossing", &EventInfo_correctedScaled_averageInteractionsPerCrossing, &b_EventInfo_correctedScaled_averageInteractionsPerCrossing);
   fChain->SetBranchAddress("EventInfo_correctedScaled_actualInteractionsPerCrossing", &EventInfo_correctedScaled_actualInteractionsPerCrossing, &b_EventInfo_correctedScaled_actualInteractionsPerCrossing);
   fChain->SetBranchAddress("EventInfo_generatorWeight_NOSYS", &EventInfo_generatorWeight_NOSYS, &b_EventInfo_generatorWeight_NOSYS);
   fChain->SetBranchAddress("EventInfo_prodFracWeight_NOSYS", &EventInfo_prodFracWeight_NOSYS, &b_EventInfo_prodFracWeight_NOSYS);
   fChain->SetBranchAddress("EventInfo_trigPassed_HLT_e24_lhmedium_L1EM20VH", &EventInfo_trigPassed_HLT_e24_lhmedium_L1EM20VH, &b_EventInfo_trigPassed_HLT_e24_lhmedium_L1EM20VH);
   fChain->SetBranchAddress("EventInfo_trigPassed_HLT_e60_lhmedium", &EventInfo_trigPassed_HLT_e60_lhmedium, &b_EventInfo_trigPassed_HLT_e60_lhmedium);
   fChain->SetBranchAddress("EventInfo_trigPassed_HLT_e120_lhloose", &EventInfo_trigPassed_HLT_e120_lhloose, &b_EventInfo_trigPassed_HLT_e120_lhloose);
   fChain->SetBranchAddress("EventInfo_trigPassed_HLT_e26_lhtight_nod0_ivarloose", &EventInfo_trigPassed_HLT_e26_lhtight_nod0_ivarloose, &b_EventInfo_trigPassed_HLT_e26_lhtight_nod0_ivarloose);
   fChain->SetBranchAddress("EventInfo_trigPassed_HLT_e60_lhmedium_nod0", &EventInfo_trigPassed_HLT_e60_lhmedium_nod0, &b_EventInfo_trigPassed_HLT_e60_lhmedium_nod0);
   fChain->SetBranchAddress("EventInfo_trigPassed_HLT_e140_lhloose_nod0", &EventInfo_trigPassed_HLT_e140_lhloose_nod0, &b_EventInfo_trigPassed_HLT_e140_lhloose_nod0);
   fChain->SetBranchAddress("EventInfo_trigPassed_HLT_mu20_iloose_L1MU15", &EventInfo_trigPassed_HLT_mu20_iloose_L1MU15, &b_EventInfo_trigPassed_HLT_mu20_iloose_L1MU15);
   fChain->SetBranchAddress("EventInfo_trigPassed_HLT_mu26_ivarmedium", &EventInfo_trigPassed_HLT_mu26_ivarmedium, &b_EventInfo_trigPassed_HLT_mu26_ivarmedium);
   fChain->SetBranchAddress("EventInfo_trigPassed_HLT_mu50", &EventInfo_trigPassed_HLT_mu50, &b_EventInfo_trigPassed_HLT_mu50);
   fChain->SetBranchAddress("EventInfo_jvt_effSF_NOSYS", &EventInfo_jvt_effSF_NOSYS, &b_EventInfo_jvt_effSF_NOSYS);
   fChain->SetBranchAddress("CharmEventInfo_TopWeight", &CharmEventInfo_TopWeight, &b_CharmEventInfo_TopWeight);
   fChain->SetBranchAddress("AnalysisElectrons_pt_NOSYS", &AnalysisElectrons_pt_NOSYS, &b_AnalysisElectrons_pt_NOSYS);
   fChain->SetBranchAddress("AnalysisElectrons_el_selected_NOSYS", &AnalysisElectrons_el_selected_NOSYS, &b_AnalysisElectrons_el_selected_NOSYS);
   fChain->SetBranchAddress("AnalysisElectrons_eta", &AnalysisElectrons_eta, &b_AnalysisElectrons_eta);
   fChain->SetBranchAddress("AnalysisElectrons_caloCluster_eta", &AnalysisElectrons_caloCluster_eta, &b_AnalysisElectrons_caloCluster_eta);
   fChain->SetBranchAddress("AnalysisElectrons_phi", &AnalysisElectrons_phi, &b_AnalysisElectrons_phi);
   fChain->SetBranchAddress("AnalysisElectrons_charge", &AnalysisElectrons_charge, &b_AnalysisElectrons_charge);
   fChain->SetBranchAddress("AnalysisElectrons_d0sig", &AnalysisElectrons_d0sig, &b_AnalysisElectrons_d0sig);
   fChain->SetBranchAddress("AnalysisElectrons_d0", &AnalysisElectrons_d0, &b_AnalysisElectrons_d0);
   fChain->SetBranchAddress("AnalysisElectrons_z0sinTheta", &AnalysisElectrons_z0sinTheta, &b_AnalysisElectrons_z0sinTheta);
   fChain->SetBranchAddress("AnalysisElectrons_topoetcone20", &AnalysisElectrons_topoetcone20, &b_AnalysisElectrons_topoetcone20);
   fChain->SetBranchAddress("AnalysisElectrons_ptvarcone20_TightTTVA_pt1000", &AnalysisElectrons_ptvarcone20_TightTTVA_pt1000, &b_AnalysisElectrons_ptvarcone20_TightTTVA_pt1000);
   fChain->SetBranchAddress("AnalysisElectrons_ptvarcone30_TightTTVA_pt1000", &AnalysisElectrons_ptvarcone30_TightTTVA_pt1000, &b_AnalysisElectrons_ptvarcone30_TightTTVA_pt1000);
   fChain->SetBranchAddress("AnalysisElectrons_likelihood_Tight", &AnalysisElectrons_likelihood_Tight, &b_AnalysisElectrons_likelihood_Tight);
   fChain->SetBranchAddress("AnalysisElectrons_isIsolated_Tight_VarRad_NOSYS", &AnalysisElectrons_isIsolated_Tight_VarRad_NOSYS, &b_AnalysisElectrons_isIsolated_Tight_VarRad_NOSYS);
   fChain->SetBranchAddress("AnalysisElectrons_effSF_NOSYS", &AnalysisElectrons_effSF_NOSYS, &b_AnalysisElectrons_effSF_NOSYS);
   fChain->SetBranchAddress("AnalysisElectrons_effSF_ID_Tight_NOSYS", &AnalysisElectrons_effSF_ID_Tight_NOSYS, &b_AnalysisElectrons_effSF_ID_Tight_NOSYS);
   fChain->SetBranchAddress("AnalysisElectrons_effSF_Chflip_Tight_Tight_VarRad_NOSYS", &AnalysisElectrons_effSF_Chflip_Tight_Tight_VarRad_NOSYS, &b_AnalysisElectrons_effSF_Chflip_Tight_Tight_VarRad_NOSYS);
   fChain->SetBranchAddress("AnalysisElectrons_effSF_Isol_Tight_Tight_VarRad_NOSYS", &AnalysisElectrons_effSF_Isol_Tight_Tight_VarRad_NOSYS, &b_AnalysisElectrons_effSF_Isol_Tight_Tight_VarRad_NOSYS);
   fChain->SetBranchAddress("AnalysisElectrons_effSF_Trig_Tight_Tight_VarRad_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_NOSYS", &AnalysisElectrons_effSF_Trig_Tight_Tight_VarRad_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_NOSYS, &b_AnalysisElectrons_effSF_Trig_Tight_Tight_VarRad_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_NOSYS);
   fChain->SetBranchAddress("AnalysisElectrons_effSF_Trig_Tight_Tight_VarRad_Eff_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_NOSYS", &AnalysisElectrons_effSF_Trig_Tight_Tight_VarRad_Eff_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_NOSYS, &b_AnalysisElectrons_effSF_Trig_Tight_Tight_VarRad_Eff_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_NOSYS);
   fChain->SetBranchAddress("AnalysisElectrons_effSF_Chflip_Tight_noIso_NOSYS", &AnalysisElectrons_effSF_Chflip_Tight_noIso_NOSYS, &b_AnalysisElectrons_effSF_Chflip_Tight_noIso_NOSYS);
   fChain->SetBranchAddress("AnalysisElectrons_effSF_Trig_Tight_noIso_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_NOSYS", &AnalysisElectrons_effSF_Trig_Tight_noIso_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_NOSYS, &b_AnalysisElectrons_effSF_Trig_Tight_noIso_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_NOSYS);
   fChain->SetBranchAddress("AnalysisElectrons_effSF_Trig_Tight_noIso_Eff_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_NOSYS", &AnalysisElectrons_effSF_Trig_Tight_noIso_Eff_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_NOSYS, &b_AnalysisElectrons_effSF_Trig_Tight_noIso_Eff_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_NOSYS);
   fChain->SetBranchAddress("AnalysisElectrons_matched_HLT_e24_lhmedium_L1EM20VH", &AnalysisElectrons_matched_HLT_e24_lhmedium_L1EM20VH, &b_AnalysisElectrons_matched_HLT_e24_lhmedium_L1EM20VH);
   fChain->SetBranchAddress("AnalysisElectrons_matched_HLT_e60_lhmedium", &AnalysisElectrons_matched_HLT_e60_lhmedium, &b_AnalysisElectrons_matched_HLT_e60_lhmedium);
   fChain->SetBranchAddress("AnalysisElectrons_matched_HLT_e120_lhloose", &AnalysisElectrons_matched_HLT_e120_lhloose, &b_AnalysisElectrons_matched_HLT_e120_lhloose);
   fChain->SetBranchAddress("AnalysisElectrons_matched_HLT_e26_lhtight_nod0_ivarloose", &AnalysisElectrons_matched_HLT_e26_lhtight_nod0_ivarloose, &b_AnalysisElectrons_matched_HLT_e26_lhtight_nod0_ivarloose);
   fChain->SetBranchAddress("AnalysisElectrons_matched_HLT_e60_lhmedium_nod0", &AnalysisElectrons_matched_HLT_e60_lhmedium_nod0, &b_AnalysisElectrons_matched_HLT_e60_lhmedium_nod0);
   fChain->SetBranchAddress("AnalysisElectrons_matched_HLT_e140_lhloose_nod0", &AnalysisElectrons_matched_HLT_e140_lhloose_nod0, &b_AnalysisElectrons_matched_HLT_e140_lhloose_nod0);
   fChain->SetBranchAddress("AnalysisElectrons_truthType", &AnalysisElectrons_truthType, &b_AnalysisElectrons_truthType);
   fChain->SetBranchAddress("AnalysisElectrons_truthOrigin", &AnalysisElectrons_truthOrigin, &b_AnalysisElectrons_truthOrigin);
   fChain->SetBranchAddress("AnalysisElectrons_firstEgMotherPdgId", &AnalysisElectrons_firstEgMotherPdgId, &b_AnalysisElectrons_firstEgMotherPdgId);
   fChain->SetBranchAddress("AnalysisElectrons_firstEgMotherTruthType", &AnalysisElectrons_firstEgMotherTruthType, &b_AnalysisElectrons_firstEgMotherTruthType);
   fChain->SetBranchAddress("AnalysisElectrons_firstEgMotherTruthOrigin", &AnalysisElectrons_firstEgMotherTruthOrigin, &b_AnalysisElectrons_firstEgMotherTruthOrigin);
   fChain->SetBranchAddress("AnalysisMuons_pt_NOSYS", &AnalysisMuons_pt_NOSYS, &b_AnalysisMuons_pt_NOSYS);
   fChain->SetBranchAddress("AnalysisMuons_mu_selected_NOSYS", &AnalysisMuons_mu_selected_NOSYS, &b_AnalysisMuons_mu_selected_NOSYS);
   fChain->SetBranchAddress("AnalysisMuons_is_bad", &AnalysisMuons_is_bad, &b_AnalysisMuons_is_bad);
   fChain->SetBranchAddress("AnalysisMuons_eta", &AnalysisMuons_eta, &b_AnalysisMuons_eta);
   fChain->SetBranchAddress("AnalysisMuons_phi", &AnalysisMuons_phi, &b_AnalysisMuons_phi);
   fChain->SetBranchAddress("AnalysisMuons_charge", &AnalysisMuons_charge, &b_AnalysisMuons_charge);
   fChain->SetBranchAddress("AnalysisMuons_d0sig", &AnalysisMuons_d0sig, &b_AnalysisMuons_d0sig);
   fChain->SetBranchAddress("AnalysisMuons_d0", &AnalysisMuons_d0, &b_AnalysisMuons_d0);
   fChain->SetBranchAddress("AnalysisMuons_z0sinTheta", &AnalysisMuons_z0sinTheta, &b_AnalysisMuons_z0sinTheta);
   fChain->SetBranchAddress("AnalysisMuons_topoetcone20", &AnalysisMuons_topoetcone20, &b_AnalysisMuons_topoetcone20);
   fChain->SetBranchAddress("AnalysisMuons_ptvarcone30_TightTTVA_pt1000", &AnalysisMuons_ptvarcone30_TightTTVA_pt1000, &b_AnalysisMuons_ptvarcone30_TightTTVA_pt1000);
   fChain->SetBranchAddress("AnalysisMuons_ptvarcone30_TightTTVA_pt500", &AnalysisMuons_ptvarcone30_TightTTVA_pt500, &b_AnalysisMuons_ptvarcone30_TightTTVA_pt500);
   fChain->SetBranchAddress("AnalysisMuons_neflowisol20", &AnalysisMuons_neflowisol20, &b_AnalysisMuons_neflowisol20);
   fChain->SetBranchAddress("AnalysisMuons_isQuality_Loose_NOSYS", &AnalysisMuons_isQuality_Loose_NOSYS, &b_AnalysisMuons_isQuality_Loose_NOSYS);
   fChain->SetBranchAddress("AnalysisMuons_isQuality_Medium_NOSYS", &AnalysisMuons_isQuality_Medium_NOSYS, &b_AnalysisMuons_isQuality_Medium_NOSYS);
   fChain->SetBranchAddress("AnalysisMuons_isQuality_Tight_NOSYS", &AnalysisMuons_isQuality_Tight_NOSYS, &b_AnalysisMuons_isQuality_Tight_NOSYS);
   fChain->SetBranchAddress("AnalysisMuons_isIsolated_PflowTight_VarRad_NOSYS", &AnalysisMuons_isIsolated_PflowTight_VarRad_NOSYS, &b_AnalysisMuons_isIsolated_PflowTight_VarRad_NOSYS);
   fChain->SetBranchAddress("AnalysisMuons_muon_effSF_NOSYS", &AnalysisMuons_muon_effSF_NOSYS, &b_AnalysisMuons_muon_effSF_NOSYS);
   fChain->SetBranchAddress("AnalysisMuons_muon_effSF_TTVA_NOSYS", &AnalysisMuons_muon_effSF_TTVA_NOSYS, &b_AnalysisMuons_muon_effSF_TTVA_NOSYS);
   fChain->SetBranchAddress("AnalysisMuons_muon_effSF_Quality_Loose_NOSYS", &AnalysisMuons_muon_effSF_Quality_Loose_NOSYS, &b_AnalysisMuons_muon_effSF_Quality_Loose_NOSYS);
   fChain->SetBranchAddress("AnalysisMuons_muon_effSF_Quality_Medium_NOSYS", &AnalysisMuons_muon_effSF_Quality_Medium_NOSYS, &b_AnalysisMuons_muon_effSF_Quality_Medium_NOSYS);
   fChain->SetBranchAddress("AnalysisMuons_muon_effSF_Quality_Tight_NOSYS", &AnalysisMuons_muon_effSF_Quality_Tight_NOSYS, &b_AnalysisMuons_muon_effSF_Quality_Tight_NOSYS);
   fChain->SetBranchAddress("AnalysisMuons_muon_effSF_Isol_PflowTight_VarRad_NOSYS", &AnalysisMuons_muon_effSF_Isol_PflowTight_VarRad_NOSYS, &b_AnalysisMuons_muon_effSF_Isol_PflowTight_VarRad_NOSYS);
   fChain->SetBranchAddress("AnalysisMuons_muon_effMC_Trig_Loose_HLT_mu26_ivarmedium_OR_HLT_mu50_NOSYS", &AnalysisMuons_muon_effMC_Trig_Loose_HLT_mu26_ivarmedium_OR_HLT_mu50_NOSYS, &b_AnalysisMuons_muon_effMC_Trig_Loose_HLT_mu26_ivarmedium_OR_HLT_mu50_NOSYS);
   fChain->SetBranchAddress("AnalysisMuons_muon_effData_Trig_Loose_HLT_mu26_ivarmedium_OR_HLT_mu50_NOSYS", &AnalysisMuons_muon_effData_Trig_Loose_HLT_mu26_ivarmedium_OR_HLT_mu50_NOSYS, &b_AnalysisMuons_muon_effData_Trig_Loose_HLT_mu26_ivarmedium_OR_HLT_mu50_NOSYS);
   fChain->SetBranchAddress("AnalysisMuons_muon_effMC_Trig_Loose_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_NOSYS", &AnalysisMuons_muon_effMC_Trig_Loose_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_NOSYS, &b_AnalysisMuons_muon_effMC_Trig_Loose_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_NOSYS);
   fChain->SetBranchAddress("AnalysisMuons_muon_effData_Trig_Loose_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_NOSYS", &AnalysisMuons_muon_effData_Trig_Loose_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_NOSYS, &b_AnalysisMuons_muon_effData_Trig_Loose_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_NOSYS);
   fChain->SetBranchAddress("AnalysisMuons_muon_effMC_Trig_Medium_HLT_mu26_ivarmedium_OR_HLT_mu50_NOSYS", &AnalysisMuons_muon_effMC_Trig_Medium_HLT_mu26_ivarmedium_OR_HLT_mu50_NOSYS, &b_AnalysisMuons_muon_effMC_Trig_Medium_HLT_mu26_ivarmedium_OR_HLT_mu50_NOSYS);
   fChain->SetBranchAddress("AnalysisMuons_muon_effData_Trig_Medium_HLT_mu26_ivarmedium_OR_HLT_mu50_NOSYS", &AnalysisMuons_muon_effData_Trig_Medium_HLT_mu26_ivarmedium_OR_HLT_mu50_NOSYS, &b_AnalysisMuons_muon_effData_Trig_Medium_HLT_mu26_ivarmedium_OR_HLT_mu50_NOSYS);
   fChain->SetBranchAddress("AnalysisMuons_muon_effMC_Trig_Medium_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_NOSYS", &AnalysisMuons_muon_effMC_Trig_Medium_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_NOSYS, &b_AnalysisMuons_muon_effMC_Trig_Medium_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_NOSYS);
   fChain->SetBranchAddress("AnalysisMuons_muon_effData_Trig_Medium_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_NOSYS", &AnalysisMuons_muon_effData_Trig_Medium_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_NOSYS, &b_AnalysisMuons_muon_effData_Trig_Medium_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_NOSYS);
   fChain->SetBranchAddress("AnalysisMuons_muon_effMC_Trig_Tight_HLT_mu26_ivarmedium_OR_HLT_mu50_NOSYS", &AnalysisMuons_muon_effMC_Trig_Tight_HLT_mu26_ivarmedium_OR_HLT_mu50_NOSYS, &b_AnalysisMuons_muon_effMC_Trig_Tight_HLT_mu26_ivarmedium_OR_HLT_mu50_NOSYS);
   fChain->SetBranchAddress("AnalysisMuons_muon_effData_Trig_Tight_HLT_mu26_ivarmedium_OR_HLT_mu50_NOSYS", &AnalysisMuons_muon_effData_Trig_Tight_HLT_mu26_ivarmedium_OR_HLT_mu50_NOSYS, &b_AnalysisMuons_muon_effData_Trig_Tight_HLT_mu26_ivarmedium_OR_HLT_mu50_NOSYS);
   fChain->SetBranchAddress("AnalysisMuons_muon_effMC_Trig_Tight_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_NOSYS", &AnalysisMuons_muon_effMC_Trig_Tight_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_NOSYS, &b_AnalysisMuons_muon_effMC_Trig_Tight_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_NOSYS);
   fChain->SetBranchAddress("AnalysisMuons_muon_effData_Trig_Tight_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_NOSYS", &AnalysisMuons_muon_effData_Trig_Tight_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_NOSYS, &b_AnalysisMuons_muon_effData_Trig_Tight_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_NOSYS);
   fChain->SetBranchAddress("AnalysisMuons_matched_HLT_mu20_iloose_L1MU15", &AnalysisMuons_matched_HLT_mu20_iloose_L1MU15, &b_AnalysisMuons_matched_HLT_mu20_iloose_L1MU15);
   fChain->SetBranchAddress("AnalysisMuons_matched_HLT_mu26_ivarmedium", &AnalysisMuons_matched_HLT_mu26_ivarmedium, &b_AnalysisMuons_matched_HLT_mu26_ivarmedium);
   fChain->SetBranchAddress("AnalysisMuons_matched_HLT_mu50", &AnalysisMuons_matched_HLT_mu50, &b_AnalysisMuons_matched_HLT_mu50);
   fChain->SetBranchAddress("AnalysisMuons_truthType", &AnalysisMuons_truthType, &b_AnalysisMuons_truthType);
   fChain->SetBranchAddress("AnalysisMuons_truthOrigin", &AnalysisMuons_truthOrigin, &b_AnalysisMuons_truthOrigin);
   fChain->SetBranchAddress("AnalysisJets_pt_NOSYS", &AnalysisJets_pt_NOSYS, &b_AnalysisJets_pt_NOSYS);
   fChain->SetBranchAddress("AnalysisJets_jet_selected_NOSYS", &AnalysisJets_jet_selected_NOSYS, &b_AnalysisJets_jet_selected_NOSYS);
   fChain->SetBranchAddress("AnalysisJets_m", &AnalysisJets_m, &b_AnalysisJets_m);
   fChain->SetBranchAddress("AnalysisJets_eta", &AnalysisJets_eta, &b_AnalysisJets_eta);
   fChain->SetBranchAddress("AnalysisJets_phi", &AnalysisJets_phi, &b_AnalysisJets_phi);
   fChain->SetBranchAddress("AnalysisJets_ftag_select_DL1r_FixedCutBEff_70", &AnalysisJets_ftag_select_DL1r_FixedCutBEff_70, &b_AnalysisJets_ftag_select_DL1r_FixedCutBEff_70);
   fChain->SetBranchAddress("AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_NOSYS", &AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_NOSYS, &b_AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_NOSYS);
   fChain->SetBranchAddress("AnalysisJets_ftag_quantile_DL1r", &AnalysisJets_ftag_quantile_DL1r, &b_AnalysisJets_ftag_quantile_DL1r);
   fChain->SetBranchAddress("AnalysisJets_ftag_DL1pu_DL1r", &AnalysisJets_ftag_DL1pu_DL1r, &b_AnalysisJets_ftag_DL1pu_DL1r);
   fChain->SetBranchAddress("AnalysisJets_ftag_DL1pb_DL1r", &AnalysisJets_ftag_DL1pb_DL1r, &b_AnalysisJets_ftag_DL1pb_DL1r);
   fChain->SetBranchAddress("AnalysisJets_ftag_DL1pc_DL1r", &AnalysisJets_ftag_DL1pc_DL1r, &b_AnalysisJets_ftag_DL1pc_DL1r);
   fChain->SetBranchAddress("AnalysisJets_HadronConeExclExtendedTruthLabelID", &AnalysisJets_HadronConeExclExtendedTruthLabelID, &b_AnalysisJets_HadronConeExclExtendedTruthLabelID);
   fChain->SetBranchAddress("AnalysisJets_HadronConeExclTruthLabelID", &AnalysisJets_HadronConeExclTruthLabelID, &b_AnalysisJets_HadronConeExclTruthLabelID);
   fChain->SetBranchAddress("METInfo_MET_NOSYS", &METInfo_MET_NOSYS, &b_METInfo_MET_NOSYS);
   fChain->SetBranchAddress("METInfo_METPhi_NOSYS", &METInfo_METPhi_NOSYS, &b_METInfo_METPhi_NOSYS);
   fChain->SetBranchAddress("DMesons_costhetastar_NOSYS", &DMesons_costhetastar_NOSYS, &b_DMesons_costhetastar_NOSYS);
   fChain->SetBranchAddress("DMesons_D0Index_NOSYS", &DMesons_D0Index_NOSYS, &b_DMesons_D0Index_NOSYS);
   fChain->SetBranchAddress("DMesons_daughterInfo__eta_NOSYS", &DMesons_daughterInfo__eta_NOSYS, &b_DMesons_daughterInfo__eta_NOSYS);
   fChain->SetBranchAddress("DMesons_daughterInfo__passTight_NOSYS", &DMesons_daughterInfo__passTight_NOSYS, &b_DMesons_daughterInfo__passTight_NOSYS);
   fChain->SetBranchAddress("DMesons_daughterInfo__pdgId_NOSYS", &DMesons_daughterInfo__pdgId_NOSYS, &b_DMesons_daughterInfo__pdgId_NOSYS);
   fChain->SetBranchAddress("DMesons_daughterInfo__phi_NOSYS", &DMesons_daughterInfo__phi_NOSYS, &b_DMesons_daughterInfo__phi_NOSYS);
   fChain->SetBranchAddress("DMesons_daughterInfo__pt_NOSYS", &DMesons_daughterInfo__pt_NOSYS, &b_DMesons_daughterInfo__pt_NOSYS);
   fChain->SetBranchAddress("DMesons_daughterInfo__trackId_NOSYS", &DMesons_daughterInfo__trackId_NOSYS, &b_DMesons_daughterInfo__trackId_NOSYS);
   fChain->SetBranchAddress("DMesons_daughterInfo__truthBarcode_NOSYS", &DMesons_daughterInfo__truthBarcode_NOSYS, &b_DMesons_daughterInfo__truthBarcode_NOSYS);
   fChain->SetBranchAddress("DMesons_daughterInfo__truthDBarcode_NOSYS", &DMesons_daughterInfo__truthDBarcode_NOSYS, &b_DMesons_daughterInfo__truthDBarcode_NOSYS);
   fChain->SetBranchAddress("DMesons_daughterInfo__z0SinTheta_NOSYS", &DMesons_daughterInfo__z0SinTheta_NOSYS, &b_DMesons_daughterInfo__z0SinTheta_NOSYS);
   fChain->SetBranchAddress("DMesons_daughterInfo__z0SinThetaPV_NOSYS", &DMesons_daughterInfo__z0SinThetaPV_NOSYS, &b_DMesons_daughterInfo__z0SinThetaPV_NOSYS);
   fChain->SetBranchAddress("DMesons_decayType_NOSYS", &DMesons_decayType_NOSYS, &b_DMesons_decayType_NOSYS);
   fChain->SetBranchAddress("DMesons_DeltaMass_NOSYS", &DMesons_DeltaMass_NOSYS, &b_DMesons_DeltaMass_NOSYS);
   fChain->SetBranchAddress("DMesons_eta_NOSYS", &DMesons_eta_NOSYS, &b_DMesons_eta_NOSYS);
   fChain->SetBranchAddress("DMesons_fitOutput__Charge_NOSYS", &DMesons_fitOutput__Charge_NOSYS, &b_DMesons_fitOutput__Charge_NOSYS);
   fChain->SetBranchAddress("DMesons_fitOutput__Chi2_NOSYS", &DMesons_fitOutput__Chi2_NOSYS, &b_DMesons_fitOutput__Chi2_NOSYS);
   fChain->SetBranchAddress("DMesons_fitOutput__Impact_NOSYS", &DMesons_fitOutput__Impact_NOSYS, &b_DMesons_fitOutput__Impact_NOSYS);
   fChain->SetBranchAddress("DMesons_fitOutput__ImpactSignificance_NOSYS", &DMesons_fitOutput__ImpactSignificance_NOSYS, &b_DMesons_fitOutput__ImpactSignificance_NOSYS);
   fChain->SetBranchAddress("DMesons_fitOutput__ImpactZ0SinTheta_NOSYS", &DMesons_fitOutput__ImpactZ0SinTheta_NOSYS, &b_DMesons_fitOutput__ImpactZ0SinTheta_NOSYS);
   fChain->SetBranchAddress("DMesons_fitOutput__Lxy_NOSYS", &DMesons_fitOutput__Lxy_NOSYS, &b_DMesons_fitOutput__Lxy_NOSYS);
   fChain->SetBranchAddress("DMesons_fitOutput__VertexPosition_NOSYS", &DMesons_fitOutput__VertexPosition_NOSYS, &b_DMesons_fitOutput__VertexPosition_NOSYS);
   fChain->SetBranchAddress("DMesons_m_NOSYS", &DMesons_m_NOSYS, &b_DMesons_m_NOSYS);
   fChain->SetBranchAddress("DMesons_mKpi1_NOSYS", &DMesons_mKpi1_NOSYS, &b_DMesons_mKpi1_NOSYS);
   fChain->SetBranchAddress("DMesons_mKpi2_NOSYS", &DMesons_mKpi2_NOSYS, &b_DMesons_mKpi2_NOSYS);
   fChain->SetBranchAddress("DMesons_mPhi1_NOSYS", &DMesons_mPhi1_NOSYS, &b_DMesons_mPhi1_NOSYS);
   fChain->SetBranchAddress("DMesons_mPhi2_NOSYS", &DMesons_mPhi2_NOSYS, &b_DMesons_mPhi2_NOSYS);
   fChain->SetBranchAddress("DMesons_pdgId_NOSYS", &DMesons_pdgId_NOSYS, &b_DMesons_pdgId_NOSYS);
   fChain->SetBranchAddress("DMesons_phi_NOSYS", &DMesons_phi_NOSYS, &b_DMesons_phi_NOSYS);
   fChain->SetBranchAddress("DMesons_pt_NOSYS", &DMesons_pt_NOSYS, &b_DMesons_pt_NOSYS);
   fChain->SetBranchAddress("DMesons_ptcone40_NOSYS", &DMesons_ptcone40_NOSYS, &b_DMesons_ptcone40_NOSYS);
   fChain->SetBranchAddress("DMesons_SlowPionD0_NOSYS", &DMesons_SlowPionD0_NOSYS, &b_DMesons_SlowPionD0_NOSYS);
   fChain->SetBranchAddress("DMesons_SlowPionZ0SinTheta_NOSYS", &DMesons_SlowPionZ0SinTheta_NOSYS, &b_DMesons_SlowPionZ0SinTheta_NOSYS);
   fChain->SetBranchAddress("DMesons_truthBarcode_NOSYS", &DMesons_truthBarcode_NOSYS, &b_DMesons_truthBarcode_NOSYS);
   fChain->SetBranchAddress("TruthParticles_Selected_barcode", &TruthParticles_Selected_barcode, &b_TruthParticles_Selected_barcode);
   fChain->SetBranchAddress("TruthParticles_Selected_cosThetaStarT", &TruthParticles_Selected_cosThetaStarT, &b_TruthParticles_Selected_cosThetaStarT);
   fChain->SetBranchAddress("TruthParticles_Selected_daughterInfoT__barcode", &TruthParticles_Selected_daughterInfoT__barcode, &b_TruthParticles_Selected_daughterInfoT__barcode);
   fChain->SetBranchAddress("TruthParticles_Selected_daughterInfoT__eta", &TruthParticles_Selected_daughterInfoT__eta, &b_TruthParticles_Selected_daughterInfoT__eta);
   fChain->SetBranchAddress("TruthParticles_Selected_daughterInfoT__pdgId", &TruthParticles_Selected_daughterInfoT__pdgId, &b_TruthParticles_Selected_daughterInfoT__pdgId);
   fChain->SetBranchAddress("TruthParticles_Selected_daughterInfoT__phi", &TruthParticles_Selected_daughterInfoT__phi, &b_TruthParticles_Selected_daughterInfoT__phi);
   fChain->SetBranchAddress("TruthParticles_Selected_daughterInfoT__pt", &TruthParticles_Selected_daughterInfoT__pt, &b_TruthParticles_Selected_daughterInfoT__pt);
   fChain->SetBranchAddress("TruthParticles_Selected_decayMode", &TruthParticles_Selected_decayMode, &b_TruthParticles_Selected_decayMode);
   fChain->SetBranchAddress("TruthParticles_Selected_eta", &TruthParticles_Selected_eta, &b_TruthParticles_Selected_eta);
   fChain->SetBranchAddress("TruthParticles_Selected_fromBdecay", &TruthParticles_Selected_fromBdecay, &b_TruthParticles_Selected_fromBdecay);
   fChain->SetBranchAddress("TruthParticles_Selected_ImpactT", &TruthParticles_Selected_ImpactT, &b_TruthParticles_Selected_ImpactT);
   fChain->SetBranchAddress("TruthParticles_Selected_LxyT", &TruthParticles_Selected_LxyT, &b_TruthParticles_Selected_LxyT);
   fChain->SetBranchAddress("TruthParticles_Selected_m", &TruthParticles_Selected_m, &b_TruthParticles_Selected_m);
   fChain->SetBranchAddress("TruthParticles_Selected_pdgId", &TruthParticles_Selected_pdgId, &b_TruthParticles_Selected_pdgId);
   fChain->SetBranchAddress("TruthParticles_Selected_phi", &TruthParticles_Selected_phi, &b_TruthParticles_Selected_phi);
   fChain->SetBranchAddress("TruthParticles_Selected_pt", &TruthParticles_Selected_pt, &b_TruthParticles_Selected_pt);
   fChain->SetBranchAddress("TruthParticles_Selected_status", &TruthParticles_Selected_status, &b_TruthParticles_Selected_status);
   fChain->SetBranchAddress("TruthParticles_Selected_vertexPosition", &TruthParticles_Selected_vertexPosition, &b_TruthParticles_Selected_vertexPosition);
   fChain->SetBranchAddress("TruthLeptons_barcode", &TruthLeptons_barcode, &b_TruthLeptons_barcode);
   fChain->SetBranchAddress("TruthLeptons_classifierParticleOrigin", &TruthLeptons_classifierParticleOrigin, &b_TruthLeptons_classifierParticleOrigin);
   fChain->SetBranchAddress("TruthLeptons_classifierParticleType", &TruthLeptons_classifierParticleType, &b_TruthLeptons_classifierParticleType);
   fChain->SetBranchAddress("TruthLeptons_e", &TruthLeptons_e, &b_TruthLeptons_e);
   fChain->SetBranchAddress("TruthLeptons_eta", &TruthLeptons_eta, &b_TruthLeptons_eta);
   fChain->SetBranchAddress("TruthLeptons_m", &TruthLeptons_m, &b_TruthLeptons_m);
   fChain->SetBranchAddress("TruthLeptons_pdgId", &TruthLeptons_pdgId, &b_TruthLeptons_pdgId);
   fChain->SetBranchAddress("TruthLeptons_phi", &TruthLeptons_phi, &b_TruthLeptons_phi);
   fChain->SetBranchAddress("TruthLeptons_pt", &TruthLeptons_pt, &b_TruthLeptons_pt);
   fChain->SetBranchAddress("TruthLeptons_status", &TruthLeptons_status, &b_TruthLeptons_status);
   fChain->SetBranchAddress("TruthLeptons_e_dressed", &TruthLeptons_e_dressed, &b_TruthLeptons_e_dressed);
   fChain->SetBranchAddress("TruthLeptons_eta_dressed", &TruthLeptons_eta_dressed, &b_TruthLeptons_eta_dressed);
   fChain->SetBranchAddress("TruthLeptons_phi_dressed", &TruthLeptons_phi_dressed, &b_TruthLeptons_phi_dressed);
   fChain->SetBranchAddress("TruthLeptons_pt_dressed", &TruthLeptons_pt_dressed, &b_TruthLeptons_pt_dressed);
   fChain->SetBranchAddress("AntiKt4TruthJets_ConeTruthLabelID", &AntiKt4TruthJets_ConeTruthLabelID, &b_AntiKt4TruthJets_ConeTruthLabelID);
   fChain->SetBranchAddress("AntiKt4TruthJets_eta", &AntiKt4TruthJets_eta, &b_AntiKt4TruthJets_eta);
   fChain->SetBranchAddress("AntiKt4TruthJets_HadronConeExclTruthLabelID", &AntiKt4TruthJets_HadronConeExclTruthLabelID, &b_AntiKt4TruthJets_HadronConeExclTruthLabelID);
   fChain->SetBranchAddress("AntiKt4TruthJets_m", &AntiKt4TruthJets_m, &b_AntiKt4TruthJets_m);
   fChain->SetBranchAddress("AntiKt4TruthJets_phi", &AntiKt4TruthJets_phi, &b_AntiKt4TruthJets_phi);
   fChain->SetBranchAddress("AntiKt4TruthJets_pt", &AntiKt4TruthJets_pt, &b_AntiKt4TruthJets_pt);
   fChain->SetBranchAddress("AntiKt4TruthChargedJets_ConeTruthLabelID", &AntiKt4TruthChargedJets_ConeTruthLabelID, &b_AntiKt4TruthChargedJets_ConeTruthLabelID);
   fChain->SetBranchAddress("AntiKt4TruthChargedJets_eta", &AntiKt4TruthChargedJets_eta, &b_AntiKt4TruthChargedJets_eta);
   fChain->SetBranchAddress("AntiKt4TruthChargedJets_HadronConeExclTruthLabelID", &AntiKt4TruthChargedJets_HadronConeExclTruthLabelID, &b_AntiKt4TruthChargedJets_HadronConeExclTruthLabelID);
   fChain->SetBranchAddress("AntiKt4TruthChargedJets_m", &AntiKt4TruthChargedJets_m, &b_AntiKt4TruthChargedJets_m);
   fChain->SetBranchAddress("AntiKt4TruthChargedJets_phi", &AntiKt4TruthChargedJets_phi, &b_AntiKt4TruthChargedJets_phi);
   fChain->SetBranchAddress("AntiKt4TruthChargedJets_pt", &AntiKt4TruthChargedJets_pt, &b_AntiKt4TruthChargedJets_pt);
   fChain->SetBranchAddress("AntiKt6TruthChargedJets_ConeTruthLabelID", &AntiKt6TruthChargedJets_ConeTruthLabelID, &b_AntiKt6TruthChargedJets_ConeTruthLabelID);
   fChain->SetBranchAddress("AntiKt6TruthChargedJets_eta", &AntiKt6TruthChargedJets_eta, &b_AntiKt6TruthChargedJets_eta);
   fChain->SetBranchAddress("AntiKt6TruthChargedJets_HadronConeExclTruthLabelID", &AntiKt6TruthChargedJets_HadronConeExclTruthLabelID, &b_AntiKt6TruthChargedJets_HadronConeExclTruthLabelID);
   fChain->SetBranchAddress("AntiKt6TruthChargedJets_m", &AntiKt6TruthChargedJets_m, &b_AntiKt6TruthChargedJets_m);
   fChain->SetBranchAddress("AntiKt6TruthChargedJets_phi", &AntiKt6TruthChargedJets_phi, &b_AntiKt6TruthChargedJets_phi);
   fChain->SetBranchAddress("AntiKt6TruthChargedJets_pt", &AntiKt6TruthChargedJets_pt, &b_AntiKt6TruthChargedJets_pt);
   fChain->SetBranchAddress("AntiKt8TruthChargedJets_ConeTruthLabelID", &AntiKt8TruthChargedJets_ConeTruthLabelID, &b_AntiKt8TruthChargedJets_ConeTruthLabelID);
   fChain->SetBranchAddress("AntiKt8TruthChargedJets_eta", &AntiKt8TruthChargedJets_eta, &b_AntiKt8TruthChargedJets_eta);
   fChain->SetBranchAddress("AntiKt8TruthChargedJets_HadronConeExclTruthLabelID", &AntiKt8TruthChargedJets_HadronConeExclTruthLabelID, &b_AntiKt8TruthChargedJets_HadronConeExclTruthLabelID);
   fChain->SetBranchAddress("AntiKt8TruthChargedJets_m", &AntiKt8TruthChargedJets_m, &b_AntiKt8TruthChargedJets_m);
   fChain->SetBranchAddress("AntiKt8TruthChargedJets_phi", &AntiKt8TruthChargedJets_phi, &b_AntiKt8TruthChargedJets_phi);
   fChain->SetBranchAddress("AntiKt8TruthChargedJets_pt", &AntiKt8TruthChargedJets_pt, &b_AntiKt8TruthChargedJets_pt);
   fChain->SetBranchAddress("AntiKt10TruthChargedJets_ConeTruthLabelID", &AntiKt10TruthChargedJets_ConeTruthLabelID, &b_AntiKt10TruthChargedJets_ConeTruthLabelID);
   fChain->SetBranchAddress("AntiKt10TruthChargedJets_eta", &AntiKt10TruthChargedJets_eta, &b_AntiKt10TruthChargedJets_eta);
   fChain->SetBranchAddress("AntiKt10TruthChargedJets_HadronConeExclTruthLabelID", &AntiKt10TruthChargedJets_HadronConeExclTruthLabelID, &b_AntiKt10TruthChargedJets_HadronConeExclTruthLabelID);
   fChain->SetBranchAddress("AntiKt10TruthChargedJets_m", &AntiKt10TruthChargedJets_m, &b_AntiKt10TruthChargedJets_m);
   fChain->SetBranchAddress("AntiKt10TruthChargedJets_phi", &AntiKt10TruthChargedJets_phi, &b_AntiKt10TruthChargedJets_phi);
   fChain->SetBranchAddress("AntiKt10TruthChargedJets_pt", &AntiKt10TruthChargedJets_pt, &b_AntiKt10TruthChargedJets_pt);
   fChain->SetBranchAddress("MesonTracks_d0sig_NOSYS", &MesonTracks_d0sig_NOSYS, &b_MesonTracks_d0sig_NOSYS);
   fChain->SetBranchAddress("MesonTracks_d0sigPV_NOSYS", &MesonTracks_d0sigPV_NOSYS, &b_MesonTracks_d0sigPV_NOSYS);
   fChain->SetBranchAddress("MesonTracks_eta_NOSYS", &MesonTracks_eta_NOSYS, &b_MesonTracks_eta_NOSYS);
   fChain->SetBranchAddress("MesonTracks_passTight_NOSYS", &MesonTracks_passTight_NOSYS, &b_MesonTracks_passTight_NOSYS);
   fChain->SetBranchAddress("MesonTracks_phi_NOSYS", &MesonTracks_phi_NOSYS, &b_MesonTracks_phi_NOSYS);
   fChain->SetBranchAddress("MesonTracks_pt_NOSYS", &MesonTracks_pt_NOSYS, &b_MesonTracks_pt_NOSYS);
   fChain->SetBranchAddress("MesonTracks_trackId_NOSYS", &MesonTracks_trackId_NOSYS, &b_MesonTracks_trackId_NOSYS);
   fChain->SetBranchAddress("MesonTracks_z0sinTheta_NOSYS", &MesonTracks_z0sinTheta_NOSYS, &b_MesonTracks_z0sinTheta_NOSYS);
   fChain->SetBranchAddress("MesonTracks_truthMatchProbability_NOSYS", &MesonTracks_truthMatchProbability_NOSYS, &b_MesonTracks_truthMatchProbability_NOSYS);
   fChain->SetBranchAddress("AntiKt6PV0TrackJets_pt_NOSYS", &AntiKt6PV0TrackJets_pt_NOSYS, &b_AntiKt6PV0TrackJets_pt_NOSYS);
   fChain->SetBranchAddress("AntiKt6PV0TrackJets_eta_NOSYS", &AntiKt6PV0TrackJets_eta_NOSYS, &b_AntiKt6PV0TrackJets_eta_NOSYS);
   fChain->SetBranchAddress("AntiKt6PV0TrackJets_phi_NOSYS", &AntiKt6PV0TrackJets_phi_NOSYS, &b_AntiKt6PV0TrackJets_phi_NOSYS);
   fChain->SetBranchAddress("AntiKt6PV0TrackJets_m_NOSYS", &AntiKt6PV0TrackJets_m_NOSYS, &b_AntiKt6PV0TrackJets_m_NOSYS);
   fChain->SetBranchAddress("AntiKt6PV0TrackJets_daughter__pt_NOSYS", &AntiKt6PV0TrackJets_daughter__pt_NOSYS, &b_AntiKt6PV0TrackJets_daughter__pt_NOSYS);
   fChain->SetBranchAddress("AntiKt6PV0TrackJets_daughter__eta_NOSYS", &AntiKt6PV0TrackJets_daughter__eta_NOSYS, &b_AntiKt6PV0TrackJets_daughter__eta_NOSYS);
   fChain->SetBranchAddress("AntiKt6PV0TrackJets_daughter__phi_NOSYS", &AntiKt6PV0TrackJets_daughter__phi_NOSYS, &b_AntiKt6PV0TrackJets_daughter__phi_NOSYS);
   fChain->SetBranchAddress("AntiKt6PV0TrackJets_daughter__trackId_NOSYS", &AntiKt6PV0TrackJets_daughter__trackId_NOSYS, &b_AntiKt6PV0TrackJets_daughter__trackId_NOSYS);
   fChain->SetBranchAddress("AntiKt8PV0TrackJets_pt_NOSYS", &AntiKt8PV0TrackJets_pt_NOSYS, &b_AntiKt8PV0TrackJets_pt_NOSYS);
   fChain->SetBranchAddress("AntiKt8PV0TrackJets_eta_NOSYS", &AntiKt8PV0TrackJets_eta_NOSYS, &b_AntiKt8PV0TrackJets_eta_NOSYS);
   fChain->SetBranchAddress("AntiKt8PV0TrackJets_phi_NOSYS", &AntiKt8PV0TrackJets_phi_NOSYS, &b_AntiKt8PV0TrackJets_phi_NOSYS);
   fChain->SetBranchAddress("AntiKt8PV0TrackJets_m_NOSYS", &AntiKt8PV0TrackJets_m_NOSYS, &b_AntiKt8PV0TrackJets_m_NOSYS);
   fChain->SetBranchAddress("AntiKt8PV0TrackJets_daughter__pt_NOSYS", &AntiKt8PV0TrackJets_daughter__pt_NOSYS, &b_AntiKt8PV0TrackJets_daughter__pt_NOSYS);
   fChain->SetBranchAddress("AntiKt8PV0TrackJets_daughter__eta_NOSYS", &AntiKt8PV0TrackJets_daughter__eta_NOSYS, &b_AntiKt8PV0TrackJets_daughter__eta_NOSYS);
   fChain->SetBranchAddress("AntiKt8PV0TrackJets_daughter__phi_NOSYS", &AntiKt8PV0TrackJets_daughter__phi_NOSYS, &b_AntiKt8PV0TrackJets_daughter__phi_NOSYS);
   fChain->SetBranchAddress("AntiKt8PV0TrackJets_daughter__trackId_NOSYS", &AntiKt8PV0TrackJets_daughter__trackId_NOSYS, &b_AntiKt8PV0TrackJets_daughter__trackId_NOSYS);
   fChain->SetBranchAddress("AntiKt10PV0TrackJets_pt_NOSYS", &AntiKt10PV0TrackJets_pt_NOSYS, &b_AntiKt10PV0TrackJets_pt_NOSYS);
   fChain->SetBranchAddress("AntiKt10PV0TrackJets_eta_NOSYS", &AntiKt10PV0TrackJets_eta_NOSYS, &b_AntiKt10PV0TrackJets_eta_NOSYS);
   fChain->SetBranchAddress("AntiKt10PV0TrackJets_phi_NOSYS", &AntiKt10PV0TrackJets_phi_NOSYS, &b_AntiKt10PV0TrackJets_phi_NOSYS);
   fChain->SetBranchAddress("AntiKt10PV0TrackJets_m_NOSYS", &AntiKt10PV0TrackJets_m_NOSYS, &b_AntiKt10PV0TrackJets_m_NOSYS);
   fChain->SetBranchAddress("AntiKt10PV0TrackJets_daughter__pt_NOSYS", &AntiKt10PV0TrackJets_daughter__pt_NOSYS, &b_AntiKt10PV0TrackJets_daughter__pt_NOSYS);
   fChain->SetBranchAddress("AntiKt10PV0TrackJets_daughter__eta_NOSYS", &AntiKt10PV0TrackJets_daughter__eta_NOSYS, &b_AntiKt10PV0TrackJets_daughter__eta_NOSYS);
   fChain->SetBranchAddress("AntiKt10PV0TrackJets_daughter__phi_NOSYS", &AntiKt10PV0TrackJets_daughter__phi_NOSYS, &b_AntiKt10PV0TrackJets_daughter__phi_NOSYS);
   fChain->SetBranchAddress("AntiKt10PV0TrackJets_daughter__trackId_NOSYS", &AntiKt10PV0TrackJets_daughter__trackId_NOSYS, &b_AntiKt10PV0TrackJets_daughter__trackId_NOSYS);
   fChain->SetBranchAddress("AntiKtVR30Rmax4Rmin02TrackJets_pt_NOSYS", &AntiKtVR30Rmax4Rmin02TrackJets_pt_NOSYS, &b_AntiKtVR30Rmax4Rmin02TrackJets_pt_NOSYS);
   fChain->SetBranchAddress("AntiKtVR30Rmax4Rmin02TrackJets_eta_NOSYS", &AntiKtVR30Rmax4Rmin02TrackJets_eta_NOSYS, &b_AntiKtVR30Rmax4Rmin02TrackJets_eta_NOSYS);
   fChain->SetBranchAddress("AntiKtVR30Rmax4Rmin02TrackJets_phi_NOSYS", &AntiKtVR30Rmax4Rmin02TrackJets_phi_NOSYS, &b_AntiKtVR30Rmax4Rmin02TrackJets_phi_NOSYS);
   fChain->SetBranchAddress("AntiKtVR30Rmax4Rmin02TrackJets_m_NOSYS", &AntiKtVR30Rmax4Rmin02TrackJets_m_NOSYS, &b_AntiKtVR30Rmax4Rmin02TrackJets_m_NOSYS);
   fChain->SetBranchAddress("AntiKtVR30Rmax4Rmin02TrackJets_daughter__pt_NOSYS", &AntiKtVR30Rmax4Rmin02TrackJets_daughter__pt_NOSYS, &b_AntiKtVR30Rmax4Rmin02TrackJets_daughter__pt_NOSYS);
   fChain->SetBranchAddress("AntiKtVR30Rmax4Rmin02TrackJets_daughter__eta_NOSYS", &AntiKtVR30Rmax4Rmin02TrackJets_daughter__eta_NOSYS, &b_AntiKtVR30Rmax4Rmin02TrackJets_daughter__eta_NOSYS);
   fChain->SetBranchAddress("AntiKtVR30Rmax4Rmin02TrackJets_daughter__phi_NOSYS", &AntiKtVR30Rmax4Rmin02TrackJets_daughter__phi_NOSYS, &b_AntiKtVR30Rmax4Rmin02TrackJets_daughter__phi_NOSYS);
   fChain->SetBranchAddress("AntiKtVR30Rmax4Rmin02TrackJets_daughter__trackId_NOSYS", &AntiKtVR30Rmax4Rmin02TrackJets_daughter__trackId_NOSYS, &b_AntiKtVR30Rmax4Rmin02TrackJets_daughter__trackId_NOSYS);
   fChain->SetBranchAddress("AnalysisElectrons_pt_EG_RESOLUTION_ALL__1down", &AnalysisElectrons_pt_EG_RESOLUTION_ALL__1down, &b_AnalysisElectrons_pt_EG_RESOLUTION_ALL__1down);
   fChain->SetBranchAddress("AnalysisElectrons_el_selected_EG_RESOLUTION_ALL__1down", &AnalysisElectrons_el_selected_EG_RESOLUTION_ALL__1down, &b_AnalysisElectrons_el_selected_EG_RESOLUTION_ALL__1down);
   fChain->SetBranchAddress("AnalysisElectrons_isIsolated_Tight_VarRad_EG_RESOLUTION_ALL__1down", &AnalysisElectrons_isIsolated_Tight_VarRad_EG_RESOLUTION_ALL__1down, &b_AnalysisElectrons_isIsolated_Tight_VarRad_EG_RESOLUTION_ALL__1down);
   fChain->SetBranchAddress("AnalysisElectrons_effSF_EG_RESOLUTION_ALL__1down", &AnalysisElectrons_effSF_EG_RESOLUTION_ALL__1down, &b_AnalysisElectrons_effSF_EG_RESOLUTION_ALL__1down);
   fChain->SetBranchAddress("AnalysisElectrons_effSF_ID_Tight_EG_RESOLUTION_ALL__1down", &AnalysisElectrons_effSF_ID_Tight_EG_RESOLUTION_ALL__1down, &b_AnalysisElectrons_effSF_ID_Tight_EG_RESOLUTION_ALL__1down);
   fChain->SetBranchAddress("AnalysisElectrons_effSF_Chflip_Tight_Tight_VarRad_EG_RESOLUTION_ALL__1down", &AnalysisElectrons_effSF_Chflip_Tight_Tight_VarRad_EG_RESOLUTION_ALL__1down, &b_AnalysisElectrons_effSF_Chflip_Tight_Tight_VarRad_EG_RESOLUTION_ALL__1down);
   fChain->SetBranchAddress("AnalysisElectrons_effSF_Isol_Tight_Tight_VarRad_EG_RESOLUTION_ALL__1down", &AnalysisElectrons_effSF_Isol_Tight_Tight_VarRad_EG_RESOLUTION_ALL__1down, &b_AnalysisElectrons_effSF_Isol_Tight_Tight_VarRad_EG_RESOLUTION_ALL__1down);
   fChain->SetBranchAddress("AnalysisElectrons_effSF_Trig_Tight_Tight_VarRad_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_EG_RESOLUTION_ALL__1down", &AnalysisElectrons_effSF_Trig_Tight_Tight_VarRad_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_EG_RESOLUTION_ALL__1down, &b_AnalysisElectrons_effSF_Trig_Tight_Tight_VarRad_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_EG_RESOLUTION_ALL__1down);
   fChain->SetBranchAddress("AnalysisElectrons_effSF_Trig_Tight_Tight_VarRad_Eff_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_EG_RESOLUTION_ALL__1down", &AnalysisElectrons_effSF_Trig_Tight_Tight_VarRad_Eff_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_EG_RESOLUTION_ALL__1down, &b_AnalysisElectrons_effSF_Trig_Tight_Tight_VarRad_Eff_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_EG_RESOLUTION_ALL__1down);
   fChain->SetBranchAddress("AnalysisElectrons_effSF_Chflip_Tight_noIso_EG_RESOLUTION_ALL__1down", &AnalysisElectrons_effSF_Chflip_Tight_noIso_EG_RESOLUTION_ALL__1down, &b_AnalysisElectrons_effSF_Chflip_Tight_noIso_EG_RESOLUTION_ALL__1down);
   fChain->SetBranchAddress("AnalysisElectrons_effSF_Trig_Tight_noIso_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_EG_RESOLUTION_ALL__1down", &AnalysisElectrons_effSF_Trig_Tight_noIso_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_EG_RESOLUTION_ALL__1down, &b_AnalysisElectrons_effSF_Trig_Tight_noIso_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_EG_RESOLUTION_ALL__1down);
   fChain->SetBranchAddress("AnalysisElectrons_effSF_Trig_Tight_noIso_Eff_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_EG_RESOLUTION_ALL__1down", &AnalysisElectrons_effSF_Trig_Tight_noIso_Eff_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_EG_RESOLUTION_ALL__1down, &b_AnalysisElectrons_effSF_Trig_Tight_noIso_Eff_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_EG_RESOLUTION_ALL__1down);
   fChain->SetBranchAddress("AnalysisMuons_mu_selected_EG_RESOLUTION_ALL__1down", &AnalysisMuons_mu_selected_EG_RESOLUTION_ALL__1down, &b_AnalysisMuons_mu_selected_EG_RESOLUTION_ALL__1down);
   fChain->SetBranchAddress("AnalysisJets_jet_selected_EG_RESOLUTION_ALL__1down", &AnalysisJets_jet_selected_EG_RESOLUTION_ALL__1down, &b_AnalysisJets_jet_selected_EG_RESOLUTION_ALL__1down);
   fChain->SetBranchAddress("METInfo_MET_EG_RESOLUTION_ALL__1down", &METInfo_MET_EG_RESOLUTION_ALL__1down, &b_METInfo_MET_EG_RESOLUTION_ALL__1down);
   fChain->SetBranchAddress("METInfo_METPhi_EG_RESOLUTION_ALL__1down", &METInfo_METPhi_EG_RESOLUTION_ALL__1down, &b_METInfo_METPhi_EG_RESOLUTION_ALL__1down);
   fChain->SetBranchAddress("AnalysisElectrons_pt_EG_RESOLUTION_ALL__1up", &AnalysisElectrons_pt_EG_RESOLUTION_ALL__1up, &b_AnalysisElectrons_pt_EG_RESOLUTION_ALL__1up);
   fChain->SetBranchAddress("AnalysisElectrons_el_selected_EG_RESOLUTION_ALL__1up", &AnalysisElectrons_el_selected_EG_RESOLUTION_ALL__1up, &b_AnalysisElectrons_el_selected_EG_RESOLUTION_ALL__1up);
   fChain->SetBranchAddress("AnalysisElectrons_isIsolated_Tight_VarRad_EG_RESOLUTION_ALL__1up", &AnalysisElectrons_isIsolated_Tight_VarRad_EG_RESOLUTION_ALL__1up, &b_AnalysisElectrons_isIsolated_Tight_VarRad_EG_RESOLUTION_ALL__1up);
   fChain->SetBranchAddress("AnalysisElectrons_effSF_EG_RESOLUTION_ALL__1up", &AnalysisElectrons_effSF_EG_RESOLUTION_ALL__1up, &b_AnalysisElectrons_effSF_EG_RESOLUTION_ALL__1up);
   fChain->SetBranchAddress("AnalysisElectrons_effSF_ID_Tight_EG_RESOLUTION_ALL__1up", &AnalysisElectrons_effSF_ID_Tight_EG_RESOLUTION_ALL__1up, &b_AnalysisElectrons_effSF_ID_Tight_EG_RESOLUTION_ALL__1up);
   fChain->SetBranchAddress("AnalysisElectrons_effSF_Chflip_Tight_Tight_VarRad_EG_RESOLUTION_ALL__1up", &AnalysisElectrons_effSF_Chflip_Tight_Tight_VarRad_EG_RESOLUTION_ALL__1up, &b_AnalysisElectrons_effSF_Chflip_Tight_Tight_VarRad_EG_RESOLUTION_ALL__1up);
   fChain->SetBranchAddress("AnalysisElectrons_effSF_Isol_Tight_Tight_VarRad_EG_RESOLUTION_ALL__1up", &AnalysisElectrons_effSF_Isol_Tight_Tight_VarRad_EG_RESOLUTION_ALL__1up, &b_AnalysisElectrons_effSF_Isol_Tight_Tight_VarRad_EG_RESOLUTION_ALL__1up);
   fChain->SetBranchAddress("AnalysisElectrons_effSF_Trig_Tight_Tight_VarRad_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_EG_RESOLUTION_ALL__1up", &AnalysisElectrons_effSF_Trig_Tight_Tight_VarRad_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_EG_RESOLUTION_ALL__1up, &b_AnalysisElectrons_effSF_Trig_Tight_Tight_VarRad_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_EG_RESOLUTION_ALL__1up);
   fChain->SetBranchAddress("AnalysisElectrons_effSF_Trig_Tight_Tight_VarRad_Eff_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_EG_RESOLUTION_ALL__1up", &AnalysisElectrons_effSF_Trig_Tight_Tight_VarRad_Eff_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_EG_RESOLUTION_ALL__1up, &b_AnalysisElectrons_effSF_Trig_Tight_Tight_VarRad_Eff_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_EG_RESOLUTION_ALL__1up);
   fChain->SetBranchAddress("AnalysisElectrons_effSF_Chflip_Tight_noIso_EG_RESOLUTION_ALL__1up", &AnalysisElectrons_effSF_Chflip_Tight_noIso_EG_RESOLUTION_ALL__1up, &b_AnalysisElectrons_effSF_Chflip_Tight_noIso_EG_RESOLUTION_ALL__1up);
   fChain->SetBranchAddress("AnalysisElectrons_effSF_Trig_Tight_noIso_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_EG_RESOLUTION_ALL__1up", &AnalysisElectrons_effSF_Trig_Tight_noIso_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_EG_RESOLUTION_ALL__1up, &b_AnalysisElectrons_effSF_Trig_Tight_noIso_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_EG_RESOLUTION_ALL__1up);
   fChain->SetBranchAddress("AnalysisElectrons_effSF_Trig_Tight_noIso_Eff_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_EG_RESOLUTION_ALL__1up", &AnalysisElectrons_effSF_Trig_Tight_noIso_Eff_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_EG_RESOLUTION_ALL__1up, &b_AnalysisElectrons_effSF_Trig_Tight_noIso_Eff_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_EG_RESOLUTION_ALL__1up);
   fChain->SetBranchAddress("AnalysisMuons_mu_selected_EG_RESOLUTION_ALL__1up", &AnalysisMuons_mu_selected_EG_RESOLUTION_ALL__1up, &b_AnalysisMuons_mu_selected_EG_RESOLUTION_ALL__1up);
   fChain->SetBranchAddress("AnalysisJets_jet_selected_EG_RESOLUTION_ALL__1up", &AnalysisJets_jet_selected_EG_RESOLUTION_ALL__1up, &b_AnalysisJets_jet_selected_EG_RESOLUTION_ALL__1up);
   fChain->SetBranchAddress("METInfo_MET_EG_RESOLUTION_ALL__1up", &METInfo_MET_EG_RESOLUTION_ALL__1up, &b_METInfo_MET_EG_RESOLUTION_ALL__1up);
   fChain->SetBranchAddress("METInfo_METPhi_EG_RESOLUTION_ALL__1up", &METInfo_METPhi_EG_RESOLUTION_ALL__1up, &b_METInfo_METPhi_EG_RESOLUTION_ALL__1up);
   fChain->SetBranchAddress("AnalysisElectrons_pt_EG_SCALE_AF2__1down", &AnalysisElectrons_pt_EG_SCALE_AF2__1down, &b_AnalysisElectrons_pt_EG_SCALE_AF2__1down);
   fChain->SetBranchAddress("AnalysisElectrons_el_selected_EG_SCALE_AF2__1down", &AnalysisElectrons_el_selected_EG_SCALE_AF2__1down, &b_AnalysisElectrons_el_selected_EG_SCALE_AF2__1down);
   fChain->SetBranchAddress("AnalysisElectrons_isIsolated_Tight_VarRad_EG_SCALE_AF2__1down", &AnalysisElectrons_isIsolated_Tight_VarRad_EG_SCALE_AF2__1down, &b_AnalysisElectrons_isIsolated_Tight_VarRad_EG_SCALE_AF2__1down);
   fChain->SetBranchAddress("AnalysisElectrons_effSF_EG_SCALE_AF2__1down", &AnalysisElectrons_effSF_EG_SCALE_AF2__1down, &b_AnalysisElectrons_effSF_EG_SCALE_AF2__1down);
   fChain->SetBranchAddress("AnalysisElectrons_effSF_ID_Tight_EG_SCALE_AF2__1down", &AnalysisElectrons_effSF_ID_Tight_EG_SCALE_AF2__1down, &b_AnalysisElectrons_effSF_ID_Tight_EG_SCALE_AF2__1down);
   fChain->SetBranchAddress("AnalysisElectrons_effSF_Chflip_Tight_Tight_VarRad_EG_SCALE_AF2__1down", &AnalysisElectrons_effSF_Chflip_Tight_Tight_VarRad_EG_SCALE_AF2__1down, &b_AnalysisElectrons_effSF_Chflip_Tight_Tight_VarRad_EG_SCALE_AF2__1down);
   fChain->SetBranchAddress("AnalysisElectrons_effSF_Isol_Tight_Tight_VarRad_EG_SCALE_AF2__1down", &AnalysisElectrons_effSF_Isol_Tight_Tight_VarRad_EG_SCALE_AF2__1down, &b_AnalysisElectrons_effSF_Isol_Tight_Tight_VarRad_EG_SCALE_AF2__1down);
   fChain->SetBranchAddress("AnalysisElectrons_effSF_Trig_Tight_Tight_VarRad_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_EG_SCALE_AF2__1down", &AnalysisElectrons_effSF_Trig_Tight_Tight_VarRad_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_EG_SCALE_AF2__1down, &b_AnalysisElectrons_effSF_Trig_Tight_Tight_VarRad_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_EG_SCALE_AF2__1down);
   fChain->SetBranchAddress("AnalysisElectrons_effSF_Trig_Tight_Tight_VarRad_Eff_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_EG_SCALE_AF2__1down", &AnalysisElectrons_effSF_Trig_Tight_Tight_VarRad_Eff_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_EG_SCALE_AF2__1down, &b_AnalysisElectrons_effSF_Trig_Tight_Tight_VarRad_Eff_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_EG_SCALE_AF2__1down);
   fChain->SetBranchAddress("AnalysisElectrons_effSF_Chflip_Tight_noIso_EG_SCALE_AF2__1down", &AnalysisElectrons_effSF_Chflip_Tight_noIso_EG_SCALE_AF2__1down, &b_AnalysisElectrons_effSF_Chflip_Tight_noIso_EG_SCALE_AF2__1down);
   fChain->SetBranchAddress("AnalysisElectrons_effSF_Trig_Tight_noIso_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_EG_SCALE_AF2__1down", &AnalysisElectrons_effSF_Trig_Tight_noIso_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_EG_SCALE_AF2__1down, &b_AnalysisElectrons_effSF_Trig_Tight_noIso_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_EG_SCALE_AF2__1down);
   fChain->SetBranchAddress("AnalysisElectrons_effSF_Trig_Tight_noIso_Eff_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_EG_SCALE_AF2__1down", &AnalysisElectrons_effSF_Trig_Tight_noIso_Eff_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_EG_SCALE_AF2__1down, &b_AnalysisElectrons_effSF_Trig_Tight_noIso_Eff_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_EG_SCALE_AF2__1down);
   fChain->SetBranchAddress("AnalysisMuons_mu_selected_EG_SCALE_AF2__1down", &AnalysisMuons_mu_selected_EG_SCALE_AF2__1down, &b_AnalysisMuons_mu_selected_EG_SCALE_AF2__1down);
   fChain->SetBranchAddress("AnalysisJets_jet_selected_EG_SCALE_AF2__1down", &AnalysisJets_jet_selected_EG_SCALE_AF2__1down, &b_AnalysisJets_jet_selected_EG_SCALE_AF2__1down);
   fChain->SetBranchAddress("METInfo_MET_EG_SCALE_AF2__1down", &METInfo_MET_EG_SCALE_AF2__1down, &b_METInfo_MET_EG_SCALE_AF2__1down);
   fChain->SetBranchAddress("METInfo_METPhi_EG_SCALE_AF2__1down", &METInfo_METPhi_EG_SCALE_AF2__1down, &b_METInfo_METPhi_EG_SCALE_AF2__1down);
   fChain->SetBranchAddress("AnalysisElectrons_pt_EG_SCALE_AF2__1up", &AnalysisElectrons_pt_EG_SCALE_AF2__1up, &b_AnalysisElectrons_pt_EG_SCALE_AF2__1up);
   fChain->SetBranchAddress("AnalysisElectrons_el_selected_EG_SCALE_AF2__1up", &AnalysisElectrons_el_selected_EG_SCALE_AF2__1up, &b_AnalysisElectrons_el_selected_EG_SCALE_AF2__1up);
   fChain->SetBranchAddress("AnalysisElectrons_isIsolated_Tight_VarRad_EG_SCALE_AF2__1up", &AnalysisElectrons_isIsolated_Tight_VarRad_EG_SCALE_AF2__1up, &b_AnalysisElectrons_isIsolated_Tight_VarRad_EG_SCALE_AF2__1up);
   fChain->SetBranchAddress("AnalysisElectrons_effSF_EG_SCALE_AF2__1up", &AnalysisElectrons_effSF_EG_SCALE_AF2__1up, &b_AnalysisElectrons_effSF_EG_SCALE_AF2__1up);
   fChain->SetBranchAddress("AnalysisElectrons_effSF_ID_Tight_EG_SCALE_AF2__1up", &AnalysisElectrons_effSF_ID_Tight_EG_SCALE_AF2__1up, &b_AnalysisElectrons_effSF_ID_Tight_EG_SCALE_AF2__1up);
   fChain->SetBranchAddress("AnalysisElectrons_effSF_Chflip_Tight_Tight_VarRad_EG_SCALE_AF2__1up", &AnalysisElectrons_effSF_Chflip_Tight_Tight_VarRad_EG_SCALE_AF2__1up, &b_AnalysisElectrons_effSF_Chflip_Tight_Tight_VarRad_EG_SCALE_AF2__1up);
   fChain->SetBranchAddress("AnalysisElectrons_effSF_Isol_Tight_Tight_VarRad_EG_SCALE_AF2__1up", &AnalysisElectrons_effSF_Isol_Tight_Tight_VarRad_EG_SCALE_AF2__1up, &b_AnalysisElectrons_effSF_Isol_Tight_Tight_VarRad_EG_SCALE_AF2__1up);
   fChain->SetBranchAddress("AnalysisElectrons_effSF_Trig_Tight_Tight_VarRad_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_EG_SCALE_AF2__1up", &AnalysisElectrons_effSF_Trig_Tight_Tight_VarRad_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_EG_SCALE_AF2__1up, &b_AnalysisElectrons_effSF_Trig_Tight_Tight_VarRad_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_EG_SCALE_AF2__1up);
   fChain->SetBranchAddress("AnalysisElectrons_effSF_Trig_Tight_Tight_VarRad_Eff_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_EG_SCALE_AF2__1up", &AnalysisElectrons_effSF_Trig_Tight_Tight_VarRad_Eff_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_EG_SCALE_AF2__1up, &b_AnalysisElectrons_effSF_Trig_Tight_Tight_VarRad_Eff_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_EG_SCALE_AF2__1up);
   fChain->SetBranchAddress("AnalysisElectrons_effSF_Chflip_Tight_noIso_EG_SCALE_AF2__1up", &AnalysisElectrons_effSF_Chflip_Tight_noIso_EG_SCALE_AF2__1up, &b_AnalysisElectrons_effSF_Chflip_Tight_noIso_EG_SCALE_AF2__1up);
   fChain->SetBranchAddress("AnalysisElectrons_effSF_Trig_Tight_noIso_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_EG_SCALE_AF2__1up", &AnalysisElectrons_effSF_Trig_Tight_noIso_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_EG_SCALE_AF2__1up, &b_AnalysisElectrons_effSF_Trig_Tight_noIso_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_EG_SCALE_AF2__1up);
   fChain->SetBranchAddress("AnalysisElectrons_effSF_Trig_Tight_noIso_Eff_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_EG_SCALE_AF2__1up", &AnalysisElectrons_effSF_Trig_Tight_noIso_Eff_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_EG_SCALE_AF2__1up, &b_AnalysisElectrons_effSF_Trig_Tight_noIso_Eff_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_EG_SCALE_AF2__1up);
   fChain->SetBranchAddress("AnalysisMuons_mu_selected_EG_SCALE_AF2__1up", &AnalysisMuons_mu_selected_EG_SCALE_AF2__1up, &b_AnalysisMuons_mu_selected_EG_SCALE_AF2__1up);
   fChain->SetBranchAddress("AnalysisJets_jet_selected_EG_SCALE_AF2__1up", &AnalysisJets_jet_selected_EG_SCALE_AF2__1up, &b_AnalysisJets_jet_selected_EG_SCALE_AF2__1up);
   fChain->SetBranchAddress("METInfo_MET_EG_SCALE_AF2__1up", &METInfo_MET_EG_SCALE_AF2__1up, &b_METInfo_MET_EG_SCALE_AF2__1up);
   fChain->SetBranchAddress("METInfo_METPhi_EG_SCALE_AF2__1up", &METInfo_METPhi_EG_SCALE_AF2__1up, &b_METInfo_METPhi_EG_SCALE_AF2__1up);
   fChain->SetBranchAddress("AnalysisElectrons_pt_EG_SCALE_ALL__1down", &AnalysisElectrons_pt_EG_SCALE_ALL__1down, &b_AnalysisElectrons_pt_EG_SCALE_ALL__1down);
   fChain->SetBranchAddress("AnalysisElectrons_el_selected_EG_SCALE_ALL__1down", &AnalysisElectrons_el_selected_EG_SCALE_ALL__1down, &b_AnalysisElectrons_el_selected_EG_SCALE_ALL__1down);
   fChain->SetBranchAddress("AnalysisElectrons_isIsolated_Tight_VarRad_EG_SCALE_ALL__1down", &AnalysisElectrons_isIsolated_Tight_VarRad_EG_SCALE_ALL__1down, &b_AnalysisElectrons_isIsolated_Tight_VarRad_EG_SCALE_ALL__1down);
   fChain->SetBranchAddress("AnalysisElectrons_effSF_EG_SCALE_ALL__1down", &AnalysisElectrons_effSF_EG_SCALE_ALL__1down, &b_AnalysisElectrons_effSF_EG_SCALE_ALL__1down);
   fChain->SetBranchAddress("AnalysisElectrons_effSF_ID_Tight_EG_SCALE_ALL__1down", &AnalysisElectrons_effSF_ID_Tight_EG_SCALE_ALL__1down, &b_AnalysisElectrons_effSF_ID_Tight_EG_SCALE_ALL__1down);
   fChain->SetBranchAddress("AnalysisElectrons_effSF_Chflip_Tight_Tight_VarRad_EG_SCALE_ALL__1down", &AnalysisElectrons_effSF_Chflip_Tight_Tight_VarRad_EG_SCALE_ALL__1down, &b_AnalysisElectrons_effSF_Chflip_Tight_Tight_VarRad_EG_SCALE_ALL__1down);
   fChain->SetBranchAddress("AnalysisElectrons_effSF_Isol_Tight_Tight_VarRad_EG_SCALE_ALL__1down", &AnalysisElectrons_effSF_Isol_Tight_Tight_VarRad_EG_SCALE_ALL__1down, &b_AnalysisElectrons_effSF_Isol_Tight_Tight_VarRad_EG_SCALE_ALL__1down);
   fChain->SetBranchAddress("AnalysisElectrons_effSF_Trig_Tight_Tight_VarRad_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_EG_SCALE_ALL__1down", &AnalysisElectrons_effSF_Trig_Tight_Tight_VarRad_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_EG_SCALE_ALL__1down, &b_AnalysisElectrons_effSF_Trig_Tight_Tight_VarRad_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_EG_SCALE_ALL__1down);
   fChain->SetBranchAddress("AnalysisElectrons_effSF_Trig_Tight_Tight_VarRad_Eff_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_EG_SCALE_ALL__1down", &AnalysisElectrons_effSF_Trig_Tight_Tight_VarRad_Eff_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_EG_SCALE_ALL__1down, &b_AnalysisElectrons_effSF_Trig_Tight_Tight_VarRad_Eff_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_EG_SCALE_ALL__1down);
   fChain->SetBranchAddress("AnalysisElectrons_effSF_Chflip_Tight_noIso_EG_SCALE_ALL__1down", &AnalysisElectrons_effSF_Chflip_Tight_noIso_EG_SCALE_ALL__1down, &b_AnalysisElectrons_effSF_Chflip_Tight_noIso_EG_SCALE_ALL__1down);
   fChain->SetBranchAddress("AnalysisElectrons_effSF_Trig_Tight_noIso_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_EG_SCALE_ALL__1down", &AnalysisElectrons_effSF_Trig_Tight_noIso_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_EG_SCALE_ALL__1down, &b_AnalysisElectrons_effSF_Trig_Tight_noIso_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_EG_SCALE_ALL__1down);
   fChain->SetBranchAddress("AnalysisElectrons_effSF_Trig_Tight_noIso_Eff_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_EG_SCALE_ALL__1down", &AnalysisElectrons_effSF_Trig_Tight_noIso_Eff_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_EG_SCALE_ALL__1down, &b_AnalysisElectrons_effSF_Trig_Tight_noIso_Eff_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_EG_SCALE_ALL__1down);
   fChain->SetBranchAddress("AnalysisMuons_mu_selected_EG_SCALE_ALL__1down", &AnalysisMuons_mu_selected_EG_SCALE_ALL__1down, &b_AnalysisMuons_mu_selected_EG_SCALE_ALL__1down);
   fChain->SetBranchAddress("AnalysisJets_jet_selected_EG_SCALE_ALL__1down", &AnalysisJets_jet_selected_EG_SCALE_ALL__1down, &b_AnalysisJets_jet_selected_EG_SCALE_ALL__1down);
   fChain->SetBranchAddress("METInfo_MET_EG_SCALE_ALL__1down", &METInfo_MET_EG_SCALE_ALL__1down, &b_METInfo_MET_EG_SCALE_ALL__1down);
   fChain->SetBranchAddress("METInfo_METPhi_EG_SCALE_ALL__1down", &METInfo_METPhi_EG_SCALE_ALL__1down, &b_METInfo_METPhi_EG_SCALE_ALL__1down);
   fChain->SetBranchAddress("AnalysisElectrons_pt_EG_SCALE_ALL__1up", &AnalysisElectrons_pt_EG_SCALE_ALL__1up, &b_AnalysisElectrons_pt_EG_SCALE_ALL__1up);
   fChain->SetBranchAddress("AnalysisElectrons_el_selected_EG_SCALE_ALL__1up", &AnalysisElectrons_el_selected_EG_SCALE_ALL__1up, &b_AnalysisElectrons_el_selected_EG_SCALE_ALL__1up);
   fChain->SetBranchAddress("AnalysisElectrons_isIsolated_Tight_VarRad_EG_SCALE_ALL__1up", &AnalysisElectrons_isIsolated_Tight_VarRad_EG_SCALE_ALL__1up, &b_AnalysisElectrons_isIsolated_Tight_VarRad_EG_SCALE_ALL__1up);
   fChain->SetBranchAddress("AnalysisElectrons_effSF_EG_SCALE_ALL__1up", &AnalysisElectrons_effSF_EG_SCALE_ALL__1up, &b_AnalysisElectrons_effSF_EG_SCALE_ALL__1up);
   fChain->SetBranchAddress("AnalysisElectrons_effSF_ID_Tight_EG_SCALE_ALL__1up", &AnalysisElectrons_effSF_ID_Tight_EG_SCALE_ALL__1up, &b_AnalysisElectrons_effSF_ID_Tight_EG_SCALE_ALL__1up);
   fChain->SetBranchAddress("AnalysisElectrons_effSF_Chflip_Tight_Tight_VarRad_EG_SCALE_ALL__1up", &AnalysisElectrons_effSF_Chflip_Tight_Tight_VarRad_EG_SCALE_ALL__1up, &b_AnalysisElectrons_effSF_Chflip_Tight_Tight_VarRad_EG_SCALE_ALL__1up);
   fChain->SetBranchAddress("AnalysisElectrons_effSF_Isol_Tight_Tight_VarRad_EG_SCALE_ALL__1up", &AnalysisElectrons_effSF_Isol_Tight_Tight_VarRad_EG_SCALE_ALL__1up, &b_AnalysisElectrons_effSF_Isol_Tight_Tight_VarRad_EG_SCALE_ALL__1up);
   fChain->SetBranchAddress("AnalysisElectrons_effSF_Trig_Tight_Tight_VarRad_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_EG_SCALE_ALL__1up", &AnalysisElectrons_effSF_Trig_Tight_Tight_VarRad_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_EG_SCALE_ALL__1up, &b_AnalysisElectrons_effSF_Trig_Tight_Tight_VarRad_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_EG_SCALE_ALL__1up);
   fChain->SetBranchAddress("AnalysisElectrons_effSF_Trig_Tight_Tight_VarRad_Eff_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_EG_SCALE_ALL__1up", &AnalysisElectrons_effSF_Trig_Tight_Tight_VarRad_Eff_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_EG_SCALE_ALL__1up, &b_AnalysisElectrons_effSF_Trig_Tight_Tight_VarRad_Eff_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_EG_SCALE_ALL__1up);
   fChain->SetBranchAddress("AnalysisElectrons_effSF_Chflip_Tight_noIso_EG_SCALE_ALL__1up", &AnalysisElectrons_effSF_Chflip_Tight_noIso_EG_SCALE_ALL__1up, &b_AnalysisElectrons_effSF_Chflip_Tight_noIso_EG_SCALE_ALL__1up);
   fChain->SetBranchAddress("AnalysisElectrons_effSF_Trig_Tight_noIso_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_EG_SCALE_ALL__1up", &AnalysisElectrons_effSF_Trig_Tight_noIso_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_EG_SCALE_ALL__1up, &b_AnalysisElectrons_effSF_Trig_Tight_noIso_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_EG_SCALE_ALL__1up);
   fChain->SetBranchAddress("AnalysisElectrons_effSF_Trig_Tight_noIso_Eff_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_EG_SCALE_ALL__1up", &AnalysisElectrons_effSF_Trig_Tight_noIso_Eff_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_EG_SCALE_ALL__1up, &b_AnalysisElectrons_effSF_Trig_Tight_noIso_Eff_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_EG_SCALE_ALL__1up);
   fChain->SetBranchAddress("AnalysisMuons_mu_selected_EG_SCALE_ALL__1up", &AnalysisMuons_mu_selected_EG_SCALE_ALL__1up, &b_AnalysisMuons_mu_selected_EG_SCALE_ALL__1up);
   fChain->SetBranchAddress("AnalysisJets_jet_selected_EG_SCALE_ALL__1up", &AnalysisJets_jet_selected_EG_SCALE_ALL__1up, &b_AnalysisJets_jet_selected_EG_SCALE_ALL__1up);
   fChain->SetBranchAddress("METInfo_MET_EG_SCALE_ALL__1up", &METInfo_MET_EG_SCALE_ALL__1up, &b_METInfo_MET_EG_SCALE_ALL__1up);
   fChain->SetBranchAddress("METInfo_METPhi_EG_SCALE_ALL__1up", &METInfo_METPhi_EG_SCALE_ALL__1up, &b_METInfo_METPhi_EG_SCALE_ALL__1up);
   fChain->SetBranchAddress("AnalysisElectrons_effSF_Chflip_Tight_Tight_VarRad_EL_CHARGEID_STAT__1down", &AnalysisElectrons_effSF_Chflip_Tight_Tight_VarRad_EL_CHARGEID_STAT__1down, &b_AnalysisElectrons_effSF_Chflip_Tight_Tight_VarRad_EL_CHARGEID_STAT__1down);
   fChain->SetBranchAddress("AnalysisElectrons_effSF_Chflip_Tight_noIso_EL_CHARGEID_STAT__1down", &AnalysisElectrons_effSF_Chflip_Tight_noIso_EL_CHARGEID_STAT__1down, &b_AnalysisElectrons_effSF_Chflip_Tight_noIso_EL_CHARGEID_STAT__1down);
   fChain->SetBranchAddress("AnalysisElectrons_effSF_Chflip_Tight_Tight_VarRad_EL_CHARGEID_STAT__1up", &AnalysisElectrons_effSF_Chflip_Tight_Tight_VarRad_EL_CHARGEID_STAT__1up, &b_AnalysisElectrons_effSF_Chflip_Tight_Tight_VarRad_EL_CHARGEID_STAT__1up);
   fChain->SetBranchAddress("AnalysisElectrons_effSF_Chflip_Tight_noIso_EL_CHARGEID_STAT__1up", &AnalysisElectrons_effSF_Chflip_Tight_noIso_EL_CHARGEID_STAT__1up, &b_AnalysisElectrons_effSF_Chflip_Tight_noIso_EL_CHARGEID_STAT__1up);
   fChain->SetBranchAddress("AnalysisElectrons_effSF_Chflip_Tight_Tight_VarRad_EL_CHARGEID_SYStotal__1down", &AnalysisElectrons_effSF_Chflip_Tight_Tight_VarRad_EL_CHARGEID_SYStotal__1down, &b_AnalysisElectrons_effSF_Chflip_Tight_Tight_VarRad_EL_CHARGEID_SYStotal__1down);
   fChain->SetBranchAddress("AnalysisElectrons_effSF_Chflip_Tight_noIso_EL_CHARGEID_SYStotal__1down", &AnalysisElectrons_effSF_Chflip_Tight_noIso_EL_CHARGEID_SYStotal__1down, &b_AnalysisElectrons_effSF_Chflip_Tight_noIso_EL_CHARGEID_SYStotal__1down);
   fChain->SetBranchAddress("AnalysisElectrons_effSF_Chflip_Tight_Tight_VarRad_EL_CHARGEID_SYStotal__1up", &AnalysisElectrons_effSF_Chflip_Tight_Tight_VarRad_EL_CHARGEID_SYStotal__1up, &b_AnalysisElectrons_effSF_Chflip_Tight_Tight_VarRad_EL_CHARGEID_SYStotal__1up);
   fChain->SetBranchAddress("AnalysisElectrons_effSF_Chflip_Tight_noIso_EL_CHARGEID_SYStotal__1up", &AnalysisElectrons_effSF_Chflip_Tight_noIso_EL_CHARGEID_SYStotal__1up, &b_AnalysisElectrons_effSF_Chflip_Tight_noIso_EL_CHARGEID_SYStotal__1up);
   fChain->SetBranchAddress("AnalysisElectrons_effSF_ID_Tight_EL_EFF_ID_TOTAL_1NPCOR_PLUS_UNCOR__1down", &AnalysisElectrons_effSF_ID_Tight_EL_EFF_ID_TOTAL_1NPCOR_PLUS_UNCOR__1down, &b_AnalysisElectrons_effSF_ID_Tight_EL_EFF_ID_TOTAL_1NPCOR_PLUS_UNCOR__1down);
   fChain->SetBranchAddress("AnalysisElectrons_effSF_ID_Tight_EL_EFF_ID_TOTAL_1NPCOR_PLUS_UNCOR__1up", &AnalysisElectrons_effSF_ID_Tight_EL_EFF_ID_TOTAL_1NPCOR_PLUS_UNCOR__1up, &b_AnalysisElectrons_effSF_ID_Tight_EL_EFF_ID_TOTAL_1NPCOR_PLUS_UNCOR__1up);
   fChain->SetBranchAddress("AnalysisElectrons_effSF_Isol_Tight_Tight_VarRad_EL_EFF_Iso_TOTAL_1NPCOR_PLUS_UNCOR__1down", &AnalysisElectrons_effSF_Isol_Tight_Tight_VarRad_EL_EFF_Iso_TOTAL_1NPCOR_PLUS_UNCOR__1down, &b_AnalysisElectrons_effSF_Isol_Tight_Tight_VarRad_EL_EFF_Iso_TOTAL_1NPCOR_PLUS_UNCOR__1down);
   fChain->SetBranchAddress("AnalysisElectrons_effSF_Isol_Tight_Tight_VarRad_EL_EFF_Iso_TOTAL_1NPCOR_PLUS_UNCOR__1up", &AnalysisElectrons_effSF_Isol_Tight_Tight_VarRad_EL_EFF_Iso_TOTAL_1NPCOR_PLUS_UNCOR__1up, &b_AnalysisElectrons_effSF_Isol_Tight_Tight_VarRad_EL_EFF_Iso_TOTAL_1NPCOR_PLUS_UNCOR__1up);
   fChain->SetBranchAddress("AnalysisElectrons_effSF_EL_EFF_Reco_TOTAL_1NPCOR_PLUS_UNCOR__1down", &AnalysisElectrons_effSF_EL_EFF_Reco_TOTAL_1NPCOR_PLUS_UNCOR__1down, &b_AnalysisElectrons_effSF_EL_EFF_Reco_TOTAL_1NPCOR_PLUS_UNCOR__1down);
   fChain->SetBranchAddress("AnalysisElectrons_effSF_EL_EFF_Reco_TOTAL_1NPCOR_PLUS_UNCOR__1up", &AnalysisElectrons_effSF_EL_EFF_Reco_TOTAL_1NPCOR_PLUS_UNCOR__1up, &b_AnalysisElectrons_effSF_EL_EFF_Reco_TOTAL_1NPCOR_PLUS_UNCOR__1up);
   fChain->SetBranchAddress("AnalysisElectrons_effSF_Trig_Tight_Tight_VarRad_Eff_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_EL_EFF_TriggerEff_TOTAL_1NPCOR_PLUS_UNCOR__1down", &AnalysisElectrons_effSF_Trig_Tight_Tight_VarRad_Eff_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_EL_EFF_TriggerEff_TOTAL_1NPCOR_PLUS_UNCOR__1down, &b_AnalysisElectrons_effSF_Trig_Tight_Tight_VarRad_Eff_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_EL_EFF_TriggerEff_TOTAL_1NPCOR_PLUS_UNCOR__1down);
   fChain->SetBranchAddress("AnalysisElectrons_effSF_Trig_Tight_noIso_Eff_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_EL_EFF_TriggerEff_TOTAL_1NPCOR_PLUS_UNCOR__1down", &AnalysisElectrons_effSF_Trig_Tight_noIso_Eff_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_EL_EFF_TriggerEff_TOTAL_1NPCOR_PLUS_UNCOR__1down, &b_AnalysisElectrons_effSF_Trig_Tight_noIso_Eff_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_EL_EFF_TriggerEff_TOTAL_1NPCOR_PLUS_UNCOR__1down);
   fChain->SetBranchAddress("AnalysisElectrons_effSF_Trig_Tight_Tight_VarRad_Eff_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_EL_EFF_TriggerEff_TOTAL_1NPCOR_PLUS_UNCOR__1up", &AnalysisElectrons_effSF_Trig_Tight_Tight_VarRad_Eff_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_EL_EFF_TriggerEff_TOTAL_1NPCOR_PLUS_UNCOR__1up, &b_AnalysisElectrons_effSF_Trig_Tight_Tight_VarRad_Eff_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_EL_EFF_TriggerEff_TOTAL_1NPCOR_PLUS_UNCOR__1up);
   fChain->SetBranchAddress("AnalysisElectrons_effSF_Trig_Tight_noIso_Eff_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_EL_EFF_TriggerEff_TOTAL_1NPCOR_PLUS_UNCOR__1up", &AnalysisElectrons_effSF_Trig_Tight_noIso_Eff_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_EL_EFF_TriggerEff_TOTAL_1NPCOR_PLUS_UNCOR__1up, &b_AnalysisElectrons_effSF_Trig_Tight_noIso_Eff_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_EL_EFF_TriggerEff_TOTAL_1NPCOR_PLUS_UNCOR__1up);
   fChain->SetBranchAddress("AnalysisElectrons_effSF_Trig_Tight_Tight_VarRad_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_EL_EFF_Trigger_TOTAL_1NPCOR_PLUS_UNCOR__1down", &AnalysisElectrons_effSF_Trig_Tight_Tight_VarRad_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_EL_EFF_Trigger_TOTAL_1NPCOR_PLUS_UNCOR__1down, &b_AnalysisElectrons_effSF_Trig_Tight_Tight_VarRad_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_EL_EFF_Trigger_TOTAL_1NPCOR_PLUS_UNCOR__1down);
   fChain->SetBranchAddress("AnalysisElectrons_effSF_Trig_Tight_noIso_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_EL_EFF_Trigger_TOTAL_1NPCOR_PLUS_UNCOR__1down", &AnalysisElectrons_effSF_Trig_Tight_noIso_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_EL_EFF_Trigger_TOTAL_1NPCOR_PLUS_UNCOR__1down, &b_AnalysisElectrons_effSF_Trig_Tight_noIso_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_EL_EFF_Trigger_TOTAL_1NPCOR_PLUS_UNCOR__1down);
   fChain->SetBranchAddress("AnalysisElectrons_effSF_Trig_Tight_Tight_VarRad_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_EL_EFF_Trigger_TOTAL_1NPCOR_PLUS_UNCOR__1up", &AnalysisElectrons_effSF_Trig_Tight_Tight_VarRad_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_EL_EFF_Trigger_TOTAL_1NPCOR_PLUS_UNCOR__1up, &b_AnalysisElectrons_effSF_Trig_Tight_Tight_VarRad_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_EL_EFF_Trigger_TOTAL_1NPCOR_PLUS_UNCOR__1up);
   fChain->SetBranchAddress("AnalysisElectrons_effSF_Trig_Tight_noIso_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_EL_EFF_Trigger_TOTAL_1NPCOR_PLUS_UNCOR__1up", &AnalysisElectrons_effSF_Trig_Tight_noIso_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_EL_EFF_Trigger_TOTAL_1NPCOR_PLUS_UNCOR__1up, &b_AnalysisElectrons_effSF_Trig_Tight_noIso_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_EL_EFF_Trigger_TOTAL_1NPCOR_PLUS_UNCOR__1up);
   fChain->SetBranchAddress("AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_FT_EFF_Eigen_B_0__1down", &AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_FT_EFF_Eigen_B_0__1down, &b_AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_FT_EFF_Eigen_B_0__1down);
   fChain->SetBranchAddress("AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_FT_EFF_Eigen_B_0__1up", &AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_FT_EFF_Eigen_B_0__1up, &b_AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_FT_EFF_Eigen_B_0__1up);
   fChain->SetBranchAddress("AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_FT_EFF_Eigen_B_1__1down", &AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_FT_EFF_Eigen_B_1__1down, &b_AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_FT_EFF_Eigen_B_1__1down);
   fChain->SetBranchAddress("AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_FT_EFF_Eigen_B_1__1up", &AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_FT_EFF_Eigen_B_1__1up, &b_AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_FT_EFF_Eigen_B_1__1up);
   fChain->SetBranchAddress("AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_FT_EFF_Eigen_B_2__1down", &AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_FT_EFF_Eigen_B_2__1down, &b_AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_FT_EFF_Eigen_B_2__1down);
   fChain->SetBranchAddress("AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_FT_EFF_Eigen_B_2__1up", &AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_FT_EFF_Eigen_B_2__1up, &b_AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_FT_EFF_Eigen_B_2__1up);
   fChain->SetBranchAddress("AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_FT_EFF_Eigen_B_3__1down", &AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_FT_EFF_Eigen_B_3__1down, &b_AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_FT_EFF_Eigen_B_3__1down);
   fChain->SetBranchAddress("AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_FT_EFF_Eigen_B_3__1up", &AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_FT_EFF_Eigen_B_3__1up, &b_AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_FT_EFF_Eigen_B_3__1up);
   fChain->SetBranchAddress("AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_FT_EFF_Eigen_B_4__1down", &AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_FT_EFF_Eigen_B_4__1down, &b_AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_FT_EFF_Eigen_B_4__1down);
   fChain->SetBranchAddress("AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_FT_EFF_Eigen_B_4__1up", &AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_FT_EFF_Eigen_B_4__1up, &b_AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_FT_EFF_Eigen_B_4__1up);
   fChain->SetBranchAddress("AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_FT_EFF_Eigen_B_5__1down", &AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_FT_EFF_Eigen_B_5__1down, &b_AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_FT_EFF_Eigen_B_5__1down);
   fChain->SetBranchAddress("AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_FT_EFF_Eigen_B_5__1up", &AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_FT_EFF_Eigen_B_5__1up, &b_AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_FT_EFF_Eigen_B_5__1up);
   fChain->SetBranchAddress("AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_FT_EFF_Eigen_B_6__1down", &AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_FT_EFF_Eigen_B_6__1down, &b_AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_FT_EFF_Eigen_B_6__1down);
   fChain->SetBranchAddress("AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_FT_EFF_Eigen_B_6__1up", &AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_FT_EFF_Eigen_B_6__1up, &b_AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_FT_EFF_Eigen_B_6__1up);
   fChain->SetBranchAddress("AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_FT_EFF_Eigen_B_7__1down", &AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_FT_EFF_Eigen_B_7__1down, &b_AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_FT_EFF_Eigen_B_7__1down);
   fChain->SetBranchAddress("AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_FT_EFF_Eigen_B_7__1up", &AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_FT_EFF_Eigen_B_7__1up, &b_AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_FT_EFF_Eigen_B_7__1up);
   fChain->SetBranchAddress("AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_FT_EFF_Eigen_B_8__1down", &AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_FT_EFF_Eigen_B_8__1down, &b_AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_FT_EFF_Eigen_B_8__1down);
   fChain->SetBranchAddress("AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_FT_EFF_Eigen_B_8__1up", &AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_FT_EFF_Eigen_B_8__1up, &b_AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_FT_EFF_Eigen_B_8__1up);
   fChain->SetBranchAddress("AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_FT_EFF_Eigen_C_0__1down", &AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_FT_EFF_Eigen_C_0__1down, &b_AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_FT_EFF_Eigen_C_0__1down);
   fChain->SetBranchAddress("AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_FT_EFF_Eigen_C_0__1up", &AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_FT_EFF_Eigen_C_0__1up, &b_AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_FT_EFF_Eigen_C_0__1up);
   fChain->SetBranchAddress("AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_FT_EFF_Eigen_C_1__1down", &AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_FT_EFF_Eigen_C_1__1down, &b_AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_FT_EFF_Eigen_C_1__1down);
   fChain->SetBranchAddress("AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_FT_EFF_Eigen_C_1__1up", &AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_FT_EFF_Eigen_C_1__1up, &b_AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_FT_EFF_Eigen_C_1__1up);
   fChain->SetBranchAddress("AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_FT_EFF_Eigen_C_2__1down", &AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_FT_EFF_Eigen_C_2__1down, &b_AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_FT_EFF_Eigen_C_2__1down);
   fChain->SetBranchAddress("AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_FT_EFF_Eigen_C_2__1up", &AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_FT_EFF_Eigen_C_2__1up, &b_AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_FT_EFF_Eigen_C_2__1up);
   fChain->SetBranchAddress("AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_FT_EFF_Eigen_C_3__1down", &AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_FT_EFF_Eigen_C_3__1down, &b_AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_FT_EFF_Eigen_C_3__1down);
   fChain->SetBranchAddress("AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_FT_EFF_Eigen_C_3__1up", &AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_FT_EFF_Eigen_C_3__1up, &b_AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_FT_EFF_Eigen_C_3__1up);
   fChain->SetBranchAddress("AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_FT_EFF_Eigen_Light_0__1down", &AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_FT_EFF_Eigen_Light_0__1down, &b_AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_FT_EFF_Eigen_Light_0__1down);
   fChain->SetBranchAddress("AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_FT_EFF_Eigen_Light_0__1up", &AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_FT_EFF_Eigen_Light_0__1up, &b_AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_FT_EFF_Eigen_Light_0__1up);
   fChain->SetBranchAddress("AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_FT_EFF_Eigen_Light_1__1down", &AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_FT_EFF_Eigen_Light_1__1down, &b_AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_FT_EFF_Eigen_Light_1__1down);
   fChain->SetBranchAddress("AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_FT_EFF_Eigen_Light_1__1up", &AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_FT_EFF_Eigen_Light_1__1up, &b_AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_FT_EFF_Eigen_Light_1__1up);
   fChain->SetBranchAddress("AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_FT_EFF_Eigen_Light_2__1down", &AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_FT_EFF_Eigen_Light_2__1down, &b_AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_FT_EFF_Eigen_Light_2__1down);
   fChain->SetBranchAddress("AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_FT_EFF_Eigen_Light_2__1up", &AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_FT_EFF_Eigen_Light_2__1up, &b_AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_FT_EFF_Eigen_Light_2__1up);
   fChain->SetBranchAddress("AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_FT_EFF_Eigen_Light_3__1down", &AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_FT_EFF_Eigen_Light_3__1down, &b_AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_FT_EFF_Eigen_Light_3__1down);
   fChain->SetBranchAddress("AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_FT_EFF_Eigen_Light_3__1up", &AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_FT_EFF_Eigen_Light_3__1up, &b_AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_FT_EFF_Eigen_Light_3__1up);
   fChain->SetBranchAddress("AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_FT_EFF_Eigen_Light_4__1down", &AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_FT_EFF_Eigen_Light_4__1down, &b_AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_FT_EFF_Eigen_Light_4__1down);
   fChain->SetBranchAddress("AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_FT_EFF_Eigen_Light_4__1up", &AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_FT_EFF_Eigen_Light_4__1up, &b_AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_FT_EFF_Eigen_Light_4__1up);
   fChain->SetBranchAddress("AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_FT_EFF_extrapolation__1down", &AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_FT_EFF_extrapolation__1down, &b_AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_FT_EFF_extrapolation__1down);
   fChain->SetBranchAddress("AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_FT_EFF_extrapolation__1up", &AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_FT_EFF_extrapolation__1up, &b_AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_FT_EFF_extrapolation__1up);
   fChain->SetBranchAddress("AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_FT_EFF_extrapolation_from_charm__1down", &AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_FT_EFF_extrapolation_from_charm__1down, &b_AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_FT_EFF_extrapolation_from_charm__1down);
   fChain->SetBranchAddress("AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_FT_EFF_extrapolation_from_charm__1up", &AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_FT_EFF_extrapolation_from_charm__1up, &b_AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_FT_EFF_extrapolation_from_charm__1up);
   fChain->SetBranchAddress("EventInfo_generatorWeight_GEN_MEWeight", &EventInfo_generatorWeight_GEN_MEWeight, &b_EventInfo_generatorWeight_GEN_MEWeight);
   fChain->SetBranchAddress("EventInfo_generatorWeight_GEN_ME_ONLY_MUR05_MUF05_PDF303200_PSMUR05_PSMUF05", &EventInfo_generatorWeight_GEN_ME_ONLY_MUR05_MUF05_PDF303200_PSMUR05_PSMUF05, &b_EventInfo_generatorWeight_GEN_ME_ONLY_MUR05_MUF05_PDF303200_PSMUR05_PSMUF05);
   fChain->SetBranchAddress("EventInfo_generatorWeight_GEN_ME_ONLY_MUR05_MUF1_PDF303200_PSMUR05_PSMUF1", &EventInfo_generatorWeight_GEN_ME_ONLY_MUR05_MUF1_PDF303200_PSMUR05_PSMUF1, &b_EventInfo_generatorWeight_GEN_ME_ONLY_MUR05_MUF1_PDF303200_PSMUR05_PSMUF1);
   fChain->SetBranchAddress("EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF05_PDF303200_PSMUR1_PSMUF05", &EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF05_PDF303200_PSMUR1_PSMUF05, &b_EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF05_PDF303200_PSMUR1_PSMUF05);
   fChain->SetBranchAddress("EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF14068", &EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF14068, &b_EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF14068);
   fChain->SetBranchAddress("EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF269000", &EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF269000, &b_EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF269000);
   fChain->SetBranchAddress("EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF270000", &EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF270000, &b_EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF270000);
   fChain->SetBranchAddress("EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF27400", &EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF27400, &b_EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF27400);
   fChain->SetBranchAddress("EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303200", &EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303200, &b_EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303200);
   fChain->SetBranchAddress("EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303200_ASSEW", &EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303200_ASSEW, &b_EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303200_ASSEW);
   fChain->SetBranchAddress("EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303200_ASSEWLO1", &EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303200_ASSEWLO1, &b_EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303200_ASSEWLO1);
   fChain->SetBranchAddress("EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303200_ASSEWLO1LO2", &EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303200_ASSEWLO1LO2, &b_EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303200_ASSEWLO1LO2);
   fChain->SetBranchAddress("EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303200_ASSEWLO1LO2LO3", &EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303200_ASSEWLO1LO2LO3, &b_EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303200_ASSEWLO1LO2LO3);
   fChain->SetBranchAddress("EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303200_EXPASSEW", &EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303200_EXPASSEW, &b_EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303200_EXPASSEW);
   fChain->SetBranchAddress("EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303200_EXPASSEWLO1", &EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303200_EXPASSEWLO1, &b_EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303200_EXPASSEWLO1);
   fChain->SetBranchAddress("EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303200_EXPASSEWLO1LO2", &EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303200_EXPASSEWLO1LO2, &b_EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303200_EXPASSEWLO1LO2);
   fChain->SetBranchAddress("EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303200_EXPASSEWLO1LO2LO3", &EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303200_EXPASSEWLO1LO2LO3, &b_EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303200_EXPASSEWLO1LO2LO3);
   fChain->SetBranchAddress("EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303200_MULTIASSEW", &EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303200_MULTIASSEW, &b_EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303200_MULTIASSEW);
   fChain->SetBranchAddress("EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303200_MULTIASSEWLO1", &EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303200_MULTIASSEWLO1, &b_EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303200_MULTIASSEWLO1);
   fChain->SetBranchAddress("EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303200_MULTIASSEWLO1LO2", &EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303200_MULTIASSEWLO1LO2, &b_EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303200_MULTIASSEWLO1LO2);
   fChain->SetBranchAddress("EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303200_MULTIASSEWLO1LO2LO3", &EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303200_MULTIASSEWLO1LO2LO3, &b_EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303200_MULTIASSEWLO1LO2LO3);
   fChain->SetBranchAddress("EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303201", &EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303201, &b_EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303201);
   fChain->SetBranchAddress("EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303202", &EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303202, &b_EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303202);
   fChain->SetBranchAddress("EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303203", &EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303203, &b_EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303203);
   fChain->SetBranchAddress("EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303204", &EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303204, &b_EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303204);
   fChain->SetBranchAddress("EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303205", &EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303205, &b_EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303205);
   fChain->SetBranchAddress("EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303206", &EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303206, &b_EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303206);
   fChain->SetBranchAddress("EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303207", &EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303207, &b_EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303207);
   fChain->SetBranchAddress("EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303208", &EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303208, &b_EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303208);
   fChain->SetBranchAddress("EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303209", &EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303209, &b_EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303209);
   fChain->SetBranchAddress("EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303210", &EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303210, &b_EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303210);
   fChain->SetBranchAddress("EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303211", &EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303211, &b_EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303211);
   fChain->SetBranchAddress("EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303212", &EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303212, &b_EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303212);
   fChain->SetBranchAddress("EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303213", &EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303213, &b_EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303213);
   fChain->SetBranchAddress("EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303214", &EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303214, &b_EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303214);
   fChain->SetBranchAddress("EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303215", &EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303215, &b_EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303215);
   fChain->SetBranchAddress("EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303216", &EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303216, &b_EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303216);
   fChain->SetBranchAddress("EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303217", &EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303217, &b_EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303217);
   fChain->SetBranchAddress("EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303218", &EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303218, &b_EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303218);
   fChain->SetBranchAddress("EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303219", &EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303219, &b_EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303219);
   fChain->SetBranchAddress("EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303220", &EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303220, &b_EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303220);
   fChain->SetBranchAddress("EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303221", &EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303221, &b_EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303221);
   fChain->SetBranchAddress("EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303222", &EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303222, &b_EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303222);
   fChain->SetBranchAddress("EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303223", &EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303223, &b_EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303223);
   fChain->SetBranchAddress("EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303224", &EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303224, &b_EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303224);
   fChain->SetBranchAddress("EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303225", &EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303225, &b_EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303225);
   fChain->SetBranchAddress("EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303226", &EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303226, &b_EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303226);
   fChain->SetBranchAddress("EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303227", &EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303227, &b_EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303227);
   fChain->SetBranchAddress("EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303228", &EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303228, &b_EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303228);
   fChain->SetBranchAddress("EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303229", &EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303229, &b_EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303229);
   fChain->SetBranchAddress("EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303230", &EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303230, &b_EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303230);
   fChain->SetBranchAddress("EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303231", &EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303231, &b_EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303231);
   fChain->SetBranchAddress("EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303232", &EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303232, &b_EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303232);
   fChain->SetBranchAddress("EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303233", &EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303233, &b_EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303233);
   fChain->SetBranchAddress("EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303234", &EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303234, &b_EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303234);
   fChain->SetBranchAddress("EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303235", &EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303235, &b_EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303235);
   fChain->SetBranchAddress("EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303236", &EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303236, &b_EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303236);
   fChain->SetBranchAddress("EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303237", &EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303237, &b_EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303237);
   fChain->SetBranchAddress("EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303238", &EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303238, &b_EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303238);
   fChain->SetBranchAddress("EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303239", &EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303239, &b_EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303239);
   fChain->SetBranchAddress("EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303240", &EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303240, &b_EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303240);
   fChain->SetBranchAddress("EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303241", &EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303241, &b_EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303241);
   fChain->SetBranchAddress("EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303242", &EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303242, &b_EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303242);
   fChain->SetBranchAddress("EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303243", &EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303243, &b_EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303243);
   fChain->SetBranchAddress("EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303244", &EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303244, &b_EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303244);
   fChain->SetBranchAddress("EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303245", &EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303245, &b_EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303245);
   fChain->SetBranchAddress("EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303246", &EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303246, &b_EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303246);
   fChain->SetBranchAddress("EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303247", &EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303247, &b_EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303247);
   fChain->SetBranchAddress("EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303248", &EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303248, &b_EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303248);
   fChain->SetBranchAddress("EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303249", &EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303249, &b_EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303249);
   fChain->SetBranchAddress("EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303250", &EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303250, &b_EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303250);
   fChain->SetBranchAddress("EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303251", &EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303251, &b_EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303251);
   fChain->SetBranchAddress("EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303252", &EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303252, &b_EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303252);
   fChain->SetBranchAddress("EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303253", &EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303253, &b_EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303253);
   fChain->SetBranchAddress("EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303254", &EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303254, &b_EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303254);
   fChain->SetBranchAddress("EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303255", &EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303255, &b_EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303255);
   fChain->SetBranchAddress("EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303256", &EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303256, &b_EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303256);
   fChain->SetBranchAddress("EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303257", &EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303257, &b_EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303257);
   fChain->SetBranchAddress("EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303258", &EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303258, &b_EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303258);
   fChain->SetBranchAddress("EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303259", &EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303259, &b_EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303259);
   fChain->SetBranchAddress("EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303260", &EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303260, &b_EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303260);
   fChain->SetBranchAddress("EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303261", &EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303261, &b_EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303261);
   fChain->SetBranchAddress("EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303262", &EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303262, &b_EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303262);
   fChain->SetBranchAddress("EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303263", &EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303263, &b_EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303263);
   fChain->SetBranchAddress("EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303264", &EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303264, &b_EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303264);
   fChain->SetBranchAddress("EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303265", &EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303265, &b_EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303265);
   fChain->SetBranchAddress("EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303266", &EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303266, &b_EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303266);
   fChain->SetBranchAddress("EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303267", &EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303267, &b_EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303267);
   fChain->SetBranchAddress("EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303268", &EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303268, &b_EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303268);
   fChain->SetBranchAddress("EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303269", &EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303269, &b_EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303269);
   fChain->SetBranchAddress("EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303270", &EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303270, &b_EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303270);
   fChain->SetBranchAddress("EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303271", &EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303271, &b_EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303271);
   fChain->SetBranchAddress("EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303272", &EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303272, &b_EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303272);
   fChain->SetBranchAddress("EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303273", &EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303273, &b_EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303273);
   fChain->SetBranchAddress("EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303274", &EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303274, &b_EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303274);
   fChain->SetBranchAddress("EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303275", &EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303275, &b_EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303275);
   fChain->SetBranchAddress("EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303276", &EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303276, &b_EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303276);
   fChain->SetBranchAddress("EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303277", &EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303277, &b_EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303277);
   fChain->SetBranchAddress("EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303278", &EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303278, &b_EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303278);
   fChain->SetBranchAddress("EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303279", &EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303279, &b_EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303279);
   fChain->SetBranchAddress("EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303280", &EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303280, &b_EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303280);
   fChain->SetBranchAddress("EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303281", &EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303281, &b_EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303281);
   fChain->SetBranchAddress("EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303282", &EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303282, &b_EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303282);
   fChain->SetBranchAddress("EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303283", &EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303283, &b_EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303283);
   fChain->SetBranchAddress("EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303284", &EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303284, &b_EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303284);
   fChain->SetBranchAddress("EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303285", &EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303285, &b_EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303285);
   fChain->SetBranchAddress("EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303286", &EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303286, &b_EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303286);
   fChain->SetBranchAddress("EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303287", &EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303287, &b_EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303287);
   fChain->SetBranchAddress("EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303288", &EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303288, &b_EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303288);
   fChain->SetBranchAddress("EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303289", &EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303289, &b_EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303289);
   fChain->SetBranchAddress("EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303290", &EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303290, &b_EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303290);
   fChain->SetBranchAddress("EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303291", &EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303291, &b_EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303291);
   fChain->SetBranchAddress("EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303292", &EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303292, &b_EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303292);
   fChain->SetBranchAddress("EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303293", &EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303293, &b_EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303293);
   fChain->SetBranchAddress("EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303294", &EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303294, &b_EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303294);
   fChain->SetBranchAddress("EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303295", &EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303295, &b_EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303295);
   fChain->SetBranchAddress("EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303296", &EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303296, &b_EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303296);
   fChain->SetBranchAddress("EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303297", &EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303297, &b_EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303297);
   fChain->SetBranchAddress("EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303298", &EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303298, &b_EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303298);
   fChain->SetBranchAddress("EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303299", &EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303299, &b_EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303299);
   fChain->SetBranchAddress("EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303300", &EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303300, &b_EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF303300);
   fChain->SetBranchAddress("EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF304400", &EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF304400, &b_EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF304400);
   fChain->SetBranchAddress("EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF91400", &EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF91400, &b_EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF91400);
   fChain->SetBranchAddress("EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF91401", &EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF91401, &b_EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF91401);
   fChain->SetBranchAddress("EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF91402", &EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF91402, &b_EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF91402);
   fChain->SetBranchAddress("EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF91403", &EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF91403, &b_EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF91403);
   fChain->SetBranchAddress("EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF91404", &EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF91404, &b_EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF91404);
   fChain->SetBranchAddress("EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF91405", &EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF91405, &b_EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF91405);
   fChain->SetBranchAddress("EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF91406", &EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF91406, &b_EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF91406);
   fChain->SetBranchAddress("EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF91407", &EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF91407, &b_EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF91407);
   fChain->SetBranchAddress("EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF91408", &EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF91408, &b_EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF91408);
   fChain->SetBranchAddress("EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF91409", &EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF91409, &b_EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF91409);
   fChain->SetBranchAddress("EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF91410", &EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF91410, &b_EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF91410);
   fChain->SetBranchAddress("EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF91411", &EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF91411, &b_EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF91411);
   fChain->SetBranchAddress("EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF91412", &EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF91412, &b_EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF91412);
   fChain->SetBranchAddress("EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF91413", &EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF91413, &b_EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF91413);
   fChain->SetBranchAddress("EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF91414", &EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF91414, &b_EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF91414);
   fChain->SetBranchAddress("EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF91415", &EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF91415, &b_EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF91415);
   fChain->SetBranchAddress("EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF91416", &EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF91416, &b_EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF91416);
   fChain->SetBranchAddress("EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF91417", &EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF91417, &b_EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF91417);
   fChain->SetBranchAddress("EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF91418", &EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF91418, &b_EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF91418);
   fChain->SetBranchAddress("EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF91419", &EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF91419, &b_EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF91419);
   fChain->SetBranchAddress("EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF91420", &EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF91420, &b_EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF91420);
   fChain->SetBranchAddress("EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF91421", &EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF91421, &b_EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF91421);
   fChain->SetBranchAddress("EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF91422", &EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF91422, &b_EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF91422);
   fChain->SetBranchAddress("EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF91423", &EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF91423, &b_EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF91423);
   fChain->SetBranchAddress("EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF91424", &EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF91424, &b_EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF91424);
   fChain->SetBranchAddress("EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF91425", &EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF91425, &b_EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF91425);
   fChain->SetBranchAddress("EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF91426", &EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF91426, &b_EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF91426);
   fChain->SetBranchAddress("EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF91427", &EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF91427, &b_EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF91427);
   fChain->SetBranchAddress("EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF91428", &EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF91428, &b_EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF91428);
   fChain->SetBranchAddress("EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF91429", &EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF91429, &b_EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF91429);
   fChain->SetBranchAddress("EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF91430", &EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF91430, &b_EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF91430);
   fChain->SetBranchAddress("EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF91431", &EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF91431, &b_EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF91431);
   fChain->SetBranchAddress("EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF91432", &EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF91432, &b_EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF1_PDF91432);
   fChain->SetBranchAddress("EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF2_PDF303200_PSMUR1_PSMUF2", &EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF2_PDF303200_PSMUR1_PSMUF2, &b_EventInfo_generatorWeight_GEN_ME_ONLY_MUR1_MUF2_PDF303200_PSMUR1_PSMUF2);
   fChain->SetBranchAddress("EventInfo_generatorWeight_GEN_ME_ONLY_MUR2_MUF1_PDF303200_PSMUR2_PSMUF1", &EventInfo_generatorWeight_GEN_ME_ONLY_MUR2_MUF1_PDF303200_PSMUR2_PSMUF1, &b_EventInfo_generatorWeight_GEN_ME_ONLY_MUR2_MUF1_PDF303200_PSMUR2_PSMUF1);
   fChain->SetBranchAddress("EventInfo_generatorWeight_GEN_ME_ONLY_MUR2_MUF2_PDF303200_PSMUR2_PSMUF2", &EventInfo_generatorWeight_GEN_ME_ONLY_MUR2_MUF2_PDF303200_PSMUR2_PSMUF2, &b_EventInfo_generatorWeight_GEN_ME_ONLY_MUR2_MUF2_PDF303200_PSMUR2_PSMUF2);
   fChain->SetBranchAddress("EventInfo_generatorWeight_GEN_MUR05_MUF05_PDF303200_PSMUR05_PSMUF05", &EventInfo_generatorWeight_GEN_MUR05_MUF05_PDF303200_PSMUR05_PSMUF05, &b_EventInfo_generatorWeight_GEN_MUR05_MUF05_PDF303200_PSMUR05_PSMUF05);
   fChain->SetBranchAddress("EventInfo_generatorWeight_GEN_MUR05_MUF1_PDF303200_PSMUR05_PSMUF1", &EventInfo_generatorWeight_GEN_MUR05_MUF1_PDF303200_PSMUR05_PSMUF1, &b_EventInfo_generatorWeight_GEN_MUR05_MUF1_PDF303200_PSMUR05_PSMUF1);
   fChain->SetBranchAddress("EventInfo_generatorWeight_GEN_MUR1_MUF05_PDF303200_PSMUR1_PSMUF05", &EventInfo_generatorWeight_GEN_MUR1_MUF05_PDF303200_PSMUR1_PSMUF05, &b_EventInfo_generatorWeight_GEN_MUR1_MUF05_PDF303200_PSMUR1_PSMUF05);
   fChain->SetBranchAddress("EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF14068", &EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF14068, &b_EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF14068);
   fChain->SetBranchAddress("EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF269000", &EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF269000, &b_EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF269000);
   fChain->SetBranchAddress("EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF270000", &EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF270000, &b_EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF270000);
   fChain->SetBranchAddress("EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF27400", &EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF27400, &b_EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF27400);
   fChain->SetBranchAddress("EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303200", &EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303200, &b_EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303200);
   fChain->SetBranchAddress("EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303200_ASSEW", &EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303200_ASSEW, &b_EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303200_ASSEW);
   fChain->SetBranchAddress("EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303200_ASSEWLO1", &EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303200_ASSEWLO1, &b_EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303200_ASSEWLO1);
   fChain->SetBranchAddress("EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303200_ASSEWLO1LO2", &EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303200_ASSEWLO1LO2, &b_EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303200_ASSEWLO1LO2);
   fChain->SetBranchAddress("EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303200_ASSEWLO1LO2LO3", &EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303200_ASSEWLO1LO2LO3, &b_EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303200_ASSEWLO1LO2LO3);
   fChain->SetBranchAddress("EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303200_EXPASSEW", &EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303200_EXPASSEW, &b_EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303200_EXPASSEW);
   fChain->SetBranchAddress("EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303200_EXPASSEWLO1", &EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303200_EXPASSEWLO1, &b_EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303200_EXPASSEWLO1);
   fChain->SetBranchAddress("EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303200_EXPASSEWLO1LO2", &EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303200_EXPASSEWLO1LO2, &b_EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303200_EXPASSEWLO1LO2);
   fChain->SetBranchAddress("EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303200_EXPASSEWLO1LO2LO3", &EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303200_EXPASSEWLO1LO2LO3, &b_EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303200_EXPASSEWLO1LO2LO3);
   fChain->SetBranchAddress("EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303200_MULTIASSEW", &EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303200_MULTIASSEW, &b_EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303200_MULTIASSEW);
   fChain->SetBranchAddress("EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303200_MULTIASSEWLO1", &EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303200_MULTIASSEWLO1, &b_EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303200_MULTIASSEWLO1);
   fChain->SetBranchAddress("EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303200_MULTIASSEWLO1LO2", &EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303200_MULTIASSEWLO1LO2, &b_EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303200_MULTIASSEWLO1LO2);
   fChain->SetBranchAddress("EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303200_MULTIASSEWLO1LO2LO3", &EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303200_MULTIASSEWLO1LO2LO3, &b_EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303200_MULTIASSEWLO1LO2LO3);
   fChain->SetBranchAddress("EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303201", &EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303201, &b_EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303201);
   fChain->SetBranchAddress("EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303202", &EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303202, &b_EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303202);
   fChain->SetBranchAddress("EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303203", &EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303203, &b_EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303203);
   fChain->SetBranchAddress("EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303204", &EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303204, &b_EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303204);
   fChain->SetBranchAddress("EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303205", &EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303205, &b_EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303205);
   fChain->SetBranchAddress("EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303206", &EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303206, &b_EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303206);
   fChain->SetBranchAddress("EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303207", &EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303207, &b_EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303207);
   fChain->SetBranchAddress("EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303208", &EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303208, &b_EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303208);
   fChain->SetBranchAddress("EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303209", &EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303209, &b_EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303209);
   fChain->SetBranchAddress("EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303210", &EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303210, &b_EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303210);
   fChain->SetBranchAddress("EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303211", &EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303211, &b_EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303211);
   fChain->SetBranchAddress("EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303212", &EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303212, &b_EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303212);
   fChain->SetBranchAddress("EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303213", &EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303213, &b_EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303213);
   fChain->SetBranchAddress("EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303214", &EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303214, &b_EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303214);
   fChain->SetBranchAddress("EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303215", &EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303215, &b_EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303215);
   fChain->SetBranchAddress("EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303216", &EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303216, &b_EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303216);
   fChain->SetBranchAddress("EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303217", &EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303217, &b_EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303217);
   fChain->SetBranchAddress("EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303218", &EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303218, &b_EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303218);
   fChain->SetBranchAddress("EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303219", &EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303219, &b_EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303219);
   fChain->SetBranchAddress("EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303220", &EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303220, &b_EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303220);
   fChain->SetBranchAddress("EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303221", &EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303221, &b_EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303221);
   fChain->SetBranchAddress("EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303222", &EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303222, &b_EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303222);
   fChain->SetBranchAddress("EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303223", &EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303223, &b_EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303223);
   fChain->SetBranchAddress("EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303224", &EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303224, &b_EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303224);
   fChain->SetBranchAddress("EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303225", &EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303225, &b_EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303225);
   fChain->SetBranchAddress("EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303226", &EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303226, &b_EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303226);
   fChain->SetBranchAddress("EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303227", &EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303227, &b_EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303227);
   fChain->SetBranchAddress("EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303228", &EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303228, &b_EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303228);
   fChain->SetBranchAddress("EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303229", &EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303229, &b_EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303229);
   fChain->SetBranchAddress("EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303230", &EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303230, &b_EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303230);
   fChain->SetBranchAddress("EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303231", &EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303231, &b_EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303231);
   fChain->SetBranchAddress("EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303232", &EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303232, &b_EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303232);
   fChain->SetBranchAddress("EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303233", &EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303233, &b_EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303233);
   fChain->SetBranchAddress("EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303234", &EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303234, &b_EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303234);
   fChain->SetBranchAddress("EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303235", &EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303235, &b_EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303235);
   fChain->SetBranchAddress("EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303236", &EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303236, &b_EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303236);
   fChain->SetBranchAddress("EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303237", &EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303237, &b_EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303237);
   fChain->SetBranchAddress("EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303238", &EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303238, &b_EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303238);
   fChain->SetBranchAddress("EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303239", &EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303239, &b_EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303239);
   fChain->SetBranchAddress("EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303240", &EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303240, &b_EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303240);
   fChain->SetBranchAddress("EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303241", &EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303241, &b_EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303241);
   fChain->SetBranchAddress("EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303242", &EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303242, &b_EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303242);
   fChain->SetBranchAddress("EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303243", &EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303243, &b_EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303243);
   fChain->SetBranchAddress("EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303244", &EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303244, &b_EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303244);
   fChain->SetBranchAddress("EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303245", &EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303245, &b_EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303245);
   fChain->SetBranchAddress("EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303246", &EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303246, &b_EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303246);
   fChain->SetBranchAddress("EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303247", &EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303247, &b_EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303247);
   fChain->SetBranchAddress("EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303248", &EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303248, &b_EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303248);
   fChain->SetBranchAddress("EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303249", &EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303249, &b_EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303249);
   fChain->SetBranchAddress("EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303250", &EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303250, &b_EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303250);
   fChain->SetBranchAddress("EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303251", &EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303251, &b_EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303251);
   fChain->SetBranchAddress("EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303252", &EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303252, &b_EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303252);
   fChain->SetBranchAddress("EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303253", &EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303253, &b_EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303253);
   fChain->SetBranchAddress("EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303254", &EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303254, &b_EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303254);
   fChain->SetBranchAddress("EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303255", &EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303255, &b_EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303255);
   fChain->SetBranchAddress("EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303256", &EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303256, &b_EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303256);
   fChain->SetBranchAddress("EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303257", &EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303257, &b_EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303257);
   fChain->SetBranchAddress("EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303258", &EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303258, &b_EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303258);
   fChain->SetBranchAddress("EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303259", &EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303259, &b_EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303259);
   fChain->SetBranchAddress("EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303260", &EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303260, &b_EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303260);
   fChain->SetBranchAddress("EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303261", &EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303261, &b_EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303261);
   fChain->SetBranchAddress("EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303262", &EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303262, &b_EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303262);
   fChain->SetBranchAddress("EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303263", &EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303263, &b_EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303263);
   fChain->SetBranchAddress("EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303264", &EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303264, &b_EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303264);
   fChain->SetBranchAddress("EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303265", &EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303265, &b_EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303265);
   fChain->SetBranchAddress("EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303266", &EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303266, &b_EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303266);
   fChain->SetBranchAddress("EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303267", &EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303267, &b_EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303267);
   fChain->SetBranchAddress("EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303268", &EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303268, &b_EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303268);
   fChain->SetBranchAddress("EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303269", &EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303269, &b_EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303269);
   fChain->SetBranchAddress("EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303270", &EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303270, &b_EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303270);
   fChain->SetBranchAddress("EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303271", &EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303271, &b_EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303271);
   fChain->SetBranchAddress("EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303272", &EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303272, &b_EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303272);
   fChain->SetBranchAddress("EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303273", &EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303273, &b_EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303273);
   fChain->SetBranchAddress("EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303274", &EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303274, &b_EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303274);
   fChain->SetBranchAddress("EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303275", &EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303275, &b_EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303275);
   fChain->SetBranchAddress("EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303276", &EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303276, &b_EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303276);
   fChain->SetBranchAddress("EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303277", &EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303277, &b_EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303277);
   fChain->SetBranchAddress("EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303278", &EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303278, &b_EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303278);
   fChain->SetBranchAddress("EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303279", &EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303279, &b_EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303279);
   fChain->SetBranchAddress("EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303280", &EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303280, &b_EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303280);
   fChain->SetBranchAddress("EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303281", &EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303281, &b_EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303281);
   fChain->SetBranchAddress("EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303282", &EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303282, &b_EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303282);
   fChain->SetBranchAddress("EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303283", &EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303283, &b_EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303283);
   fChain->SetBranchAddress("EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303284", &EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303284, &b_EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303284);
   fChain->SetBranchAddress("EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303285", &EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303285, &b_EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303285);
   fChain->SetBranchAddress("EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303286", &EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303286, &b_EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303286);
   fChain->SetBranchAddress("EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303287", &EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303287, &b_EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303287);
   fChain->SetBranchAddress("EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303288", &EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303288, &b_EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303288);
   fChain->SetBranchAddress("EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303289", &EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303289, &b_EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303289);
   fChain->SetBranchAddress("EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303290", &EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303290, &b_EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303290);
   fChain->SetBranchAddress("EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303291", &EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303291, &b_EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303291);
   fChain->SetBranchAddress("EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303292", &EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303292, &b_EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303292);
   fChain->SetBranchAddress("EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303293", &EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303293, &b_EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303293);
   fChain->SetBranchAddress("EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303294", &EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303294, &b_EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303294);
   fChain->SetBranchAddress("EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303295", &EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303295, &b_EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303295);
   fChain->SetBranchAddress("EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303296", &EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303296, &b_EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303296);
   fChain->SetBranchAddress("EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303297", &EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303297, &b_EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303297);
   fChain->SetBranchAddress("EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303298", &EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303298, &b_EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303298);
   fChain->SetBranchAddress("EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303299", &EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303299, &b_EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303299);
   fChain->SetBranchAddress("EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303300", &EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303300, &b_EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF303300);
   fChain->SetBranchAddress("EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF304400", &EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF304400, &b_EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF304400);
   fChain->SetBranchAddress("EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF91400", &EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF91400, &b_EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF91400);
   fChain->SetBranchAddress("EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF91401", &EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF91401, &b_EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF91401);
   fChain->SetBranchAddress("EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF91402", &EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF91402, &b_EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF91402);
   fChain->SetBranchAddress("EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF91403", &EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF91403, &b_EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF91403);
   fChain->SetBranchAddress("EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF91404", &EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF91404, &b_EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF91404);
   fChain->SetBranchAddress("EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF91405", &EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF91405, &b_EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF91405);
   fChain->SetBranchAddress("EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF91406", &EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF91406, &b_EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF91406);
   fChain->SetBranchAddress("EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF91407", &EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF91407, &b_EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF91407);
   fChain->SetBranchAddress("EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF91408", &EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF91408, &b_EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF91408);
   fChain->SetBranchAddress("EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF91409", &EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF91409, &b_EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF91409);
   fChain->SetBranchAddress("EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF91410", &EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF91410, &b_EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF91410);
   fChain->SetBranchAddress("EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF91411", &EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF91411, &b_EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF91411);
   fChain->SetBranchAddress("EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF91412", &EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF91412, &b_EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF91412);
   fChain->SetBranchAddress("EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF91413", &EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF91413, &b_EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF91413);
   fChain->SetBranchAddress("EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF91414", &EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF91414, &b_EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF91414);
   fChain->SetBranchAddress("EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF91415", &EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF91415, &b_EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF91415);
   fChain->SetBranchAddress("EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF91416", &EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF91416, &b_EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF91416);
   fChain->SetBranchAddress("EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF91417", &EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF91417, &b_EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF91417);
   fChain->SetBranchAddress("EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF91418", &EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF91418, &b_EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF91418);
   fChain->SetBranchAddress("EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF91419", &EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF91419, &b_EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF91419);
   fChain->SetBranchAddress("EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF91420", &EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF91420, &b_EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF91420);
   fChain->SetBranchAddress("EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF91421", &EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF91421, &b_EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF91421);
   fChain->SetBranchAddress("EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF91422", &EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF91422, &b_EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF91422);
   fChain->SetBranchAddress("EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF91423", &EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF91423, &b_EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF91423);
   fChain->SetBranchAddress("EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF91424", &EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF91424, &b_EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF91424);
   fChain->SetBranchAddress("EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF91425", &EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF91425, &b_EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF91425);
   fChain->SetBranchAddress("EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF91426", &EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF91426, &b_EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF91426);
   fChain->SetBranchAddress("EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF91427", &EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF91427, &b_EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF91427);
   fChain->SetBranchAddress("EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF91428", &EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF91428, &b_EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF91428);
   fChain->SetBranchAddress("EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF91429", &EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF91429, &b_EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF91429);
   fChain->SetBranchAddress("EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF91430", &EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF91430, &b_EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF91430);
   fChain->SetBranchAddress("EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF91431", &EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF91431, &b_EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF91431);
   fChain->SetBranchAddress("EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF91432", &EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF91432, &b_EventInfo_generatorWeight_GEN_MUR1_MUF1_PDF91432);
   fChain->SetBranchAddress("EventInfo_generatorWeight_GEN_MUR1_MUF2_PDF303200_PSMUR1_PSMUF2", &EventInfo_generatorWeight_GEN_MUR1_MUF2_PDF303200_PSMUR1_PSMUF2, &b_EventInfo_generatorWeight_GEN_MUR1_MUF2_PDF303200_PSMUR1_PSMUF2);
   fChain->SetBranchAddress("EventInfo_generatorWeight_GEN_MUR2_MUF1_PDF303200_PSMUR2_PSMUF1", &EventInfo_generatorWeight_GEN_MUR2_MUF1_PDF303200_PSMUR2_PSMUF1, &b_EventInfo_generatorWeight_GEN_MUR2_MUF1_PDF303200_PSMUR2_PSMUF1);
   fChain->SetBranchAddress("EventInfo_generatorWeight_GEN_MUR2_MUF2_PDF303200_PSMUR2_PSMUF2", &EventInfo_generatorWeight_GEN_MUR2_MUF2_PDF303200_PSMUR2_PSMUF2, &b_EventInfo_generatorWeight_GEN_MUR2_MUF2_PDF303200_PSMUR2_PSMUF2);
   fChain->SetBranchAddress("EventInfo_generatorWeight_GEN_NTrials", &EventInfo_generatorWeight_GEN_NTrials, &b_EventInfo_generatorWeight_GEN_NTrials);
   fChain->SetBranchAddress("EventInfo_generatorWeight_GEN_UserHook", &EventInfo_generatorWeight_GEN_UserHook, &b_EventInfo_generatorWeight_GEN_UserHook);
   fChain->SetBranchAddress("EventInfo_generatorWeight_GEN_WeightNormalisation", &EventInfo_generatorWeight_GEN_WeightNormalisation, &b_EventInfo_generatorWeight_GEN_WeightNormalisation);
   fChain->SetBranchAddress("EventInfo_jvt_effSF_JET_EffectiveNP_1__1up", &EventInfo_jvt_effSF_JET_EffectiveNP_1__1up, &b_EventInfo_jvt_effSF_JET_EffectiveNP_1__1up);
   fChain->SetBranchAddress("AnalysisElectrons_el_selected_JET_EffectiveNP_1__1up", &AnalysisElectrons_el_selected_JET_EffectiveNP_1__1up, &b_AnalysisElectrons_el_selected_JET_EffectiveNP_1__1up);
   fChain->SetBranchAddress("AnalysisMuons_mu_selected_JET_EffectiveNP_1__1up", &AnalysisMuons_mu_selected_JET_EffectiveNP_1__1up, &b_AnalysisMuons_mu_selected_JET_EffectiveNP_1__1up);
   fChain->SetBranchAddress("AnalysisJets_pt_JET_EffectiveNP_1__1up", &AnalysisJets_pt_JET_EffectiveNP_1__1up, &b_AnalysisJets_pt_JET_EffectiveNP_1__1up);
   fChain->SetBranchAddress("AnalysisJets_jet_selected_JET_EffectiveNP_1__1up", &AnalysisJets_jet_selected_JET_EffectiveNP_1__1up, &b_AnalysisJets_jet_selected_JET_EffectiveNP_1__1up);
   fChain->SetBranchAddress("AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_JET_EffectiveNP_1__1up", &AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_JET_EffectiveNP_1__1up, &b_AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_JET_EffectiveNP_1__1up);
   fChain->SetBranchAddress("METInfo_MET_JET_EffectiveNP_1__1up", &METInfo_MET_JET_EffectiveNP_1__1up, &b_METInfo_MET_JET_EffectiveNP_1__1up);
   fChain->SetBranchAddress("METInfo_METPhi_JET_EffectiveNP_1__1up", &METInfo_METPhi_JET_EffectiveNP_1__1up, &b_METInfo_METPhi_JET_EffectiveNP_1__1up);
   fChain->SetBranchAddress("EventInfo_jvt_effSF_JET_EffectiveNP_1__1down", &EventInfo_jvt_effSF_JET_EffectiveNP_1__1down, &b_EventInfo_jvt_effSF_JET_EffectiveNP_1__1down);
   fChain->SetBranchAddress("AnalysisElectrons_el_selected_JET_EffectiveNP_1__1down", &AnalysisElectrons_el_selected_JET_EffectiveNP_1__1down, &b_AnalysisElectrons_el_selected_JET_EffectiveNP_1__1down);
   fChain->SetBranchAddress("AnalysisMuons_mu_selected_JET_EffectiveNP_1__1down", &AnalysisMuons_mu_selected_JET_EffectiveNP_1__1down, &b_AnalysisMuons_mu_selected_JET_EffectiveNP_1__1down);
   fChain->SetBranchAddress("AnalysisJets_pt_JET_EffectiveNP_1__1down", &AnalysisJets_pt_JET_EffectiveNP_1__1down, &b_AnalysisJets_pt_JET_EffectiveNP_1__1down);
   fChain->SetBranchAddress("AnalysisJets_jet_selected_JET_EffectiveNP_1__1down", &AnalysisJets_jet_selected_JET_EffectiveNP_1__1down, &b_AnalysisJets_jet_selected_JET_EffectiveNP_1__1down);
   fChain->SetBranchAddress("AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_JET_EffectiveNP_1__1down", &AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_JET_EffectiveNP_1__1down, &b_AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_JET_EffectiveNP_1__1down);
   fChain->SetBranchAddress("METInfo_MET_JET_EffectiveNP_1__1down", &METInfo_MET_JET_EffectiveNP_1__1down, &b_METInfo_MET_JET_EffectiveNP_1__1down);
   fChain->SetBranchAddress("METInfo_METPhi_JET_EffectiveNP_1__1down", &METInfo_METPhi_JET_EffectiveNP_1__1down, &b_METInfo_METPhi_JET_EffectiveNP_1__1down);
   fChain->SetBranchAddress("EventInfo_jvt_effSF_JET_EffectiveNP_2__1up", &EventInfo_jvt_effSF_JET_EffectiveNP_2__1up, &b_EventInfo_jvt_effSF_JET_EffectiveNP_2__1up);
   fChain->SetBranchAddress("AnalysisElectrons_el_selected_JET_EffectiveNP_2__1up", &AnalysisElectrons_el_selected_JET_EffectiveNP_2__1up, &b_AnalysisElectrons_el_selected_JET_EffectiveNP_2__1up);
   fChain->SetBranchAddress("AnalysisMuons_mu_selected_JET_EffectiveNP_2__1up", &AnalysisMuons_mu_selected_JET_EffectiveNP_2__1up, &b_AnalysisMuons_mu_selected_JET_EffectiveNP_2__1up);
   fChain->SetBranchAddress("AnalysisJets_pt_JET_EffectiveNP_2__1up", &AnalysisJets_pt_JET_EffectiveNP_2__1up, &b_AnalysisJets_pt_JET_EffectiveNP_2__1up);
   fChain->SetBranchAddress("AnalysisJets_jet_selected_JET_EffectiveNP_2__1up", &AnalysisJets_jet_selected_JET_EffectiveNP_2__1up, &b_AnalysisJets_jet_selected_JET_EffectiveNP_2__1up);
   fChain->SetBranchAddress("AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_JET_EffectiveNP_2__1up", &AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_JET_EffectiveNP_2__1up, &b_AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_JET_EffectiveNP_2__1up);
   fChain->SetBranchAddress("METInfo_MET_JET_EffectiveNP_2__1up", &METInfo_MET_JET_EffectiveNP_2__1up, &b_METInfo_MET_JET_EffectiveNP_2__1up);
   fChain->SetBranchAddress("METInfo_METPhi_JET_EffectiveNP_2__1up", &METInfo_METPhi_JET_EffectiveNP_2__1up, &b_METInfo_METPhi_JET_EffectiveNP_2__1up);
   fChain->SetBranchAddress("EventInfo_jvt_effSF_JET_EffectiveNP_2__1down", &EventInfo_jvt_effSF_JET_EffectiveNP_2__1down, &b_EventInfo_jvt_effSF_JET_EffectiveNP_2__1down);
   fChain->SetBranchAddress("AnalysisElectrons_el_selected_JET_EffectiveNP_2__1down", &AnalysisElectrons_el_selected_JET_EffectiveNP_2__1down, &b_AnalysisElectrons_el_selected_JET_EffectiveNP_2__1down);
   fChain->SetBranchAddress("AnalysisMuons_mu_selected_JET_EffectiveNP_2__1down", &AnalysisMuons_mu_selected_JET_EffectiveNP_2__1down, &b_AnalysisMuons_mu_selected_JET_EffectiveNP_2__1down);
   fChain->SetBranchAddress("AnalysisJets_pt_JET_EffectiveNP_2__1down", &AnalysisJets_pt_JET_EffectiveNP_2__1down, &b_AnalysisJets_pt_JET_EffectiveNP_2__1down);
   fChain->SetBranchAddress("AnalysisJets_jet_selected_JET_EffectiveNP_2__1down", &AnalysisJets_jet_selected_JET_EffectiveNP_2__1down, &b_AnalysisJets_jet_selected_JET_EffectiveNP_2__1down);
   fChain->SetBranchAddress("AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_JET_EffectiveNP_2__1down", &AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_JET_EffectiveNP_2__1down, &b_AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_JET_EffectiveNP_2__1down);
   fChain->SetBranchAddress("METInfo_MET_JET_EffectiveNP_2__1down", &METInfo_MET_JET_EffectiveNP_2__1down, &b_METInfo_MET_JET_EffectiveNP_2__1down);
   fChain->SetBranchAddress("METInfo_METPhi_JET_EffectiveNP_2__1down", &METInfo_METPhi_JET_EffectiveNP_2__1down, &b_METInfo_METPhi_JET_EffectiveNP_2__1down);
   fChain->SetBranchAddress("EventInfo_jvt_effSF_JET_EffectiveNP_3__1up", &EventInfo_jvt_effSF_JET_EffectiveNP_3__1up, &b_EventInfo_jvt_effSF_JET_EffectiveNP_3__1up);
   fChain->SetBranchAddress("AnalysisElectrons_el_selected_JET_EffectiveNP_3__1up", &AnalysisElectrons_el_selected_JET_EffectiveNP_3__1up, &b_AnalysisElectrons_el_selected_JET_EffectiveNP_3__1up);
   fChain->SetBranchAddress("AnalysisMuons_mu_selected_JET_EffectiveNP_3__1up", &AnalysisMuons_mu_selected_JET_EffectiveNP_3__1up, &b_AnalysisMuons_mu_selected_JET_EffectiveNP_3__1up);
   fChain->SetBranchAddress("AnalysisJets_pt_JET_EffectiveNP_3__1up", &AnalysisJets_pt_JET_EffectiveNP_3__1up, &b_AnalysisJets_pt_JET_EffectiveNP_3__1up);
   fChain->SetBranchAddress("AnalysisJets_jet_selected_JET_EffectiveNP_3__1up", &AnalysisJets_jet_selected_JET_EffectiveNP_3__1up, &b_AnalysisJets_jet_selected_JET_EffectiveNP_3__1up);
   fChain->SetBranchAddress("AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_JET_EffectiveNP_3__1up", &AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_JET_EffectiveNP_3__1up, &b_AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_JET_EffectiveNP_3__1up);
   fChain->SetBranchAddress("METInfo_MET_JET_EffectiveNP_3__1up", &METInfo_MET_JET_EffectiveNP_3__1up, &b_METInfo_MET_JET_EffectiveNP_3__1up);
   fChain->SetBranchAddress("METInfo_METPhi_JET_EffectiveNP_3__1up", &METInfo_METPhi_JET_EffectiveNP_3__1up, &b_METInfo_METPhi_JET_EffectiveNP_3__1up);
   fChain->SetBranchAddress("EventInfo_jvt_effSF_JET_EffectiveNP_3__1down", &EventInfo_jvt_effSF_JET_EffectiveNP_3__1down, &b_EventInfo_jvt_effSF_JET_EffectiveNP_3__1down);
   fChain->SetBranchAddress("AnalysisElectrons_el_selected_JET_EffectiveNP_3__1down", &AnalysisElectrons_el_selected_JET_EffectiveNP_3__1down, &b_AnalysisElectrons_el_selected_JET_EffectiveNP_3__1down);
   fChain->SetBranchAddress("AnalysisMuons_mu_selected_JET_EffectiveNP_3__1down", &AnalysisMuons_mu_selected_JET_EffectiveNP_3__1down, &b_AnalysisMuons_mu_selected_JET_EffectiveNP_3__1down);
   fChain->SetBranchAddress("AnalysisJets_pt_JET_EffectiveNP_3__1down", &AnalysisJets_pt_JET_EffectiveNP_3__1down, &b_AnalysisJets_pt_JET_EffectiveNP_3__1down);
   fChain->SetBranchAddress("AnalysisJets_jet_selected_JET_EffectiveNP_3__1down", &AnalysisJets_jet_selected_JET_EffectiveNP_3__1down, &b_AnalysisJets_jet_selected_JET_EffectiveNP_3__1down);
   fChain->SetBranchAddress("AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_JET_EffectiveNP_3__1down", &AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_JET_EffectiveNP_3__1down, &b_AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_JET_EffectiveNP_3__1down);
   fChain->SetBranchAddress("METInfo_MET_JET_EffectiveNP_3__1down", &METInfo_MET_JET_EffectiveNP_3__1down, &b_METInfo_MET_JET_EffectiveNP_3__1down);
   fChain->SetBranchAddress("METInfo_METPhi_JET_EffectiveNP_3__1down", &METInfo_METPhi_JET_EffectiveNP_3__1down, &b_METInfo_METPhi_JET_EffectiveNP_3__1down);
   fChain->SetBranchAddress("EventInfo_jvt_effSF_JET_EffectiveNP_4__1up", &EventInfo_jvt_effSF_JET_EffectiveNP_4__1up, &b_EventInfo_jvt_effSF_JET_EffectiveNP_4__1up);
   fChain->SetBranchAddress("AnalysisElectrons_el_selected_JET_EffectiveNP_4__1up", &AnalysisElectrons_el_selected_JET_EffectiveNP_4__1up, &b_AnalysisElectrons_el_selected_JET_EffectiveNP_4__1up);
   fChain->SetBranchAddress("AnalysisMuons_mu_selected_JET_EffectiveNP_4__1up", &AnalysisMuons_mu_selected_JET_EffectiveNP_4__1up, &b_AnalysisMuons_mu_selected_JET_EffectiveNP_4__1up);
   fChain->SetBranchAddress("AnalysisJets_pt_JET_EffectiveNP_4__1up", &AnalysisJets_pt_JET_EffectiveNP_4__1up, &b_AnalysisJets_pt_JET_EffectiveNP_4__1up);
   fChain->SetBranchAddress("AnalysisJets_jet_selected_JET_EffectiveNP_4__1up", &AnalysisJets_jet_selected_JET_EffectiveNP_4__1up, &b_AnalysisJets_jet_selected_JET_EffectiveNP_4__1up);
   fChain->SetBranchAddress("AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_JET_EffectiveNP_4__1up", &AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_JET_EffectiveNP_4__1up, &b_AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_JET_EffectiveNP_4__1up);
   fChain->SetBranchAddress("METInfo_MET_JET_EffectiveNP_4__1up", &METInfo_MET_JET_EffectiveNP_4__1up, &b_METInfo_MET_JET_EffectiveNP_4__1up);
   fChain->SetBranchAddress("METInfo_METPhi_JET_EffectiveNP_4__1up", &METInfo_METPhi_JET_EffectiveNP_4__1up, &b_METInfo_METPhi_JET_EffectiveNP_4__1up);
   fChain->SetBranchAddress("EventInfo_jvt_effSF_JET_EffectiveNP_4__1down", &EventInfo_jvt_effSF_JET_EffectiveNP_4__1down, &b_EventInfo_jvt_effSF_JET_EffectiveNP_4__1down);
   fChain->SetBranchAddress("AnalysisElectrons_el_selected_JET_EffectiveNP_4__1down", &AnalysisElectrons_el_selected_JET_EffectiveNP_4__1down, &b_AnalysisElectrons_el_selected_JET_EffectiveNP_4__1down);
   fChain->SetBranchAddress("AnalysisMuons_mu_selected_JET_EffectiveNP_4__1down", &AnalysisMuons_mu_selected_JET_EffectiveNP_4__1down, &b_AnalysisMuons_mu_selected_JET_EffectiveNP_4__1down);
   fChain->SetBranchAddress("AnalysisJets_pt_JET_EffectiveNP_4__1down", &AnalysisJets_pt_JET_EffectiveNP_4__1down, &b_AnalysisJets_pt_JET_EffectiveNP_4__1down);
   fChain->SetBranchAddress("AnalysisJets_jet_selected_JET_EffectiveNP_4__1down", &AnalysisJets_jet_selected_JET_EffectiveNP_4__1down, &b_AnalysisJets_jet_selected_JET_EffectiveNP_4__1down);
   fChain->SetBranchAddress("AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_JET_EffectiveNP_4__1down", &AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_JET_EffectiveNP_4__1down, &b_AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_JET_EffectiveNP_4__1down);
   fChain->SetBranchAddress("METInfo_MET_JET_EffectiveNP_4__1down", &METInfo_MET_JET_EffectiveNP_4__1down, &b_METInfo_MET_JET_EffectiveNP_4__1down);
   fChain->SetBranchAddress("METInfo_METPhi_JET_EffectiveNP_4__1down", &METInfo_METPhi_JET_EffectiveNP_4__1down, &b_METInfo_METPhi_JET_EffectiveNP_4__1down);
   fChain->SetBranchAddress("EventInfo_jvt_effSF_JET_EffectiveNP_5__1up", &EventInfo_jvt_effSF_JET_EffectiveNP_5__1up, &b_EventInfo_jvt_effSF_JET_EffectiveNP_5__1up);
   fChain->SetBranchAddress("AnalysisElectrons_el_selected_JET_EffectiveNP_5__1up", &AnalysisElectrons_el_selected_JET_EffectiveNP_5__1up, &b_AnalysisElectrons_el_selected_JET_EffectiveNP_5__1up);
   fChain->SetBranchAddress("AnalysisMuons_mu_selected_JET_EffectiveNP_5__1up", &AnalysisMuons_mu_selected_JET_EffectiveNP_5__1up, &b_AnalysisMuons_mu_selected_JET_EffectiveNP_5__1up);
   fChain->SetBranchAddress("AnalysisJets_pt_JET_EffectiveNP_5__1up", &AnalysisJets_pt_JET_EffectiveNP_5__1up, &b_AnalysisJets_pt_JET_EffectiveNP_5__1up);
   fChain->SetBranchAddress("AnalysisJets_jet_selected_JET_EffectiveNP_5__1up", &AnalysisJets_jet_selected_JET_EffectiveNP_5__1up, &b_AnalysisJets_jet_selected_JET_EffectiveNP_5__1up);
   fChain->SetBranchAddress("AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_JET_EffectiveNP_5__1up", &AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_JET_EffectiveNP_5__1up, &b_AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_JET_EffectiveNP_5__1up);
   fChain->SetBranchAddress("METInfo_MET_JET_EffectiveNP_5__1up", &METInfo_MET_JET_EffectiveNP_5__1up, &b_METInfo_MET_JET_EffectiveNP_5__1up);
   fChain->SetBranchAddress("METInfo_METPhi_JET_EffectiveNP_5__1up", &METInfo_METPhi_JET_EffectiveNP_5__1up, &b_METInfo_METPhi_JET_EffectiveNP_5__1up);
   fChain->SetBranchAddress("EventInfo_jvt_effSF_JET_EffectiveNP_5__1down", &EventInfo_jvt_effSF_JET_EffectiveNP_5__1down, &b_EventInfo_jvt_effSF_JET_EffectiveNP_5__1down);
   fChain->SetBranchAddress("AnalysisElectrons_el_selected_JET_EffectiveNP_5__1down", &AnalysisElectrons_el_selected_JET_EffectiveNP_5__1down, &b_AnalysisElectrons_el_selected_JET_EffectiveNP_5__1down);
   fChain->SetBranchAddress("AnalysisMuons_mu_selected_JET_EffectiveNP_5__1down", &AnalysisMuons_mu_selected_JET_EffectiveNP_5__1down, &b_AnalysisMuons_mu_selected_JET_EffectiveNP_5__1down);
   fChain->SetBranchAddress("AnalysisJets_pt_JET_EffectiveNP_5__1down", &AnalysisJets_pt_JET_EffectiveNP_5__1down, &b_AnalysisJets_pt_JET_EffectiveNP_5__1down);
   fChain->SetBranchAddress("AnalysisJets_jet_selected_JET_EffectiveNP_5__1down", &AnalysisJets_jet_selected_JET_EffectiveNP_5__1down, &b_AnalysisJets_jet_selected_JET_EffectiveNP_5__1down);
   fChain->SetBranchAddress("AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_JET_EffectiveNP_5__1down", &AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_JET_EffectiveNP_5__1down, &b_AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_JET_EffectiveNP_5__1down);
   fChain->SetBranchAddress("METInfo_MET_JET_EffectiveNP_5__1down", &METInfo_MET_JET_EffectiveNP_5__1down, &b_METInfo_MET_JET_EffectiveNP_5__1down);
   fChain->SetBranchAddress("METInfo_METPhi_JET_EffectiveNP_5__1down", &METInfo_METPhi_JET_EffectiveNP_5__1down, &b_METInfo_METPhi_JET_EffectiveNP_5__1down);
   fChain->SetBranchAddress("EventInfo_jvt_effSF_JET_EffectiveNP_6__1up", &EventInfo_jvt_effSF_JET_EffectiveNP_6__1up, &b_EventInfo_jvt_effSF_JET_EffectiveNP_6__1up);
   fChain->SetBranchAddress("AnalysisElectrons_el_selected_JET_EffectiveNP_6__1up", &AnalysisElectrons_el_selected_JET_EffectiveNP_6__1up, &b_AnalysisElectrons_el_selected_JET_EffectiveNP_6__1up);
   fChain->SetBranchAddress("AnalysisMuons_mu_selected_JET_EffectiveNP_6__1up", &AnalysisMuons_mu_selected_JET_EffectiveNP_6__1up, &b_AnalysisMuons_mu_selected_JET_EffectiveNP_6__1up);
   fChain->SetBranchAddress("AnalysisJets_pt_JET_EffectiveNP_6__1up", &AnalysisJets_pt_JET_EffectiveNP_6__1up, &b_AnalysisJets_pt_JET_EffectiveNP_6__1up);
   fChain->SetBranchAddress("AnalysisJets_jet_selected_JET_EffectiveNP_6__1up", &AnalysisJets_jet_selected_JET_EffectiveNP_6__1up, &b_AnalysisJets_jet_selected_JET_EffectiveNP_6__1up);
   fChain->SetBranchAddress("AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_JET_EffectiveNP_6__1up", &AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_JET_EffectiveNP_6__1up, &b_AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_JET_EffectiveNP_6__1up);
   fChain->SetBranchAddress("METInfo_MET_JET_EffectiveNP_6__1up", &METInfo_MET_JET_EffectiveNP_6__1up, &b_METInfo_MET_JET_EffectiveNP_6__1up);
   fChain->SetBranchAddress("METInfo_METPhi_JET_EffectiveNP_6__1up", &METInfo_METPhi_JET_EffectiveNP_6__1up, &b_METInfo_METPhi_JET_EffectiveNP_6__1up);
   fChain->SetBranchAddress("EventInfo_jvt_effSF_JET_EffectiveNP_6__1down", &EventInfo_jvt_effSF_JET_EffectiveNP_6__1down, &b_EventInfo_jvt_effSF_JET_EffectiveNP_6__1down);
   fChain->SetBranchAddress("AnalysisElectrons_el_selected_JET_EffectiveNP_6__1down", &AnalysisElectrons_el_selected_JET_EffectiveNP_6__1down, &b_AnalysisElectrons_el_selected_JET_EffectiveNP_6__1down);
   fChain->SetBranchAddress("AnalysisMuons_mu_selected_JET_EffectiveNP_6__1down", &AnalysisMuons_mu_selected_JET_EffectiveNP_6__1down, &b_AnalysisMuons_mu_selected_JET_EffectiveNP_6__1down);
   fChain->SetBranchAddress("AnalysisJets_pt_JET_EffectiveNP_6__1down", &AnalysisJets_pt_JET_EffectiveNP_6__1down, &b_AnalysisJets_pt_JET_EffectiveNP_6__1down);
   fChain->SetBranchAddress("AnalysisJets_jet_selected_JET_EffectiveNP_6__1down", &AnalysisJets_jet_selected_JET_EffectiveNP_6__1down, &b_AnalysisJets_jet_selected_JET_EffectiveNP_6__1down);
   fChain->SetBranchAddress("AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_JET_EffectiveNP_6__1down", &AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_JET_EffectiveNP_6__1down, &b_AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_JET_EffectiveNP_6__1down);
   fChain->SetBranchAddress("METInfo_MET_JET_EffectiveNP_6__1down", &METInfo_MET_JET_EffectiveNP_6__1down, &b_METInfo_MET_JET_EffectiveNP_6__1down);
   fChain->SetBranchAddress("METInfo_METPhi_JET_EffectiveNP_6__1down", &METInfo_METPhi_JET_EffectiveNP_6__1down, &b_METInfo_METPhi_JET_EffectiveNP_6__1down);
   fChain->SetBranchAddress("EventInfo_jvt_effSF_JET_EffectiveNP_7__1up", &EventInfo_jvt_effSF_JET_EffectiveNP_7__1up, &b_EventInfo_jvt_effSF_JET_EffectiveNP_7__1up);
   fChain->SetBranchAddress("AnalysisElectrons_el_selected_JET_EffectiveNP_7__1up", &AnalysisElectrons_el_selected_JET_EffectiveNP_7__1up, &b_AnalysisElectrons_el_selected_JET_EffectiveNP_7__1up);
   fChain->SetBranchAddress("AnalysisMuons_mu_selected_JET_EffectiveNP_7__1up", &AnalysisMuons_mu_selected_JET_EffectiveNP_7__1up, &b_AnalysisMuons_mu_selected_JET_EffectiveNP_7__1up);
   fChain->SetBranchAddress("AnalysisJets_pt_JET_EffectiveNP_7__1up", &AnalysisJets_pt_JET_EffectiveNP_7__1up, &b_AnalysisJets_pt_JET_EffectiveNP_7__1up);
   fChain->SetBranchAddress("AnalysisJets_jet_selected_JET_EffectiveNP_7__1up", &AnalysisJets_jet_selected_JET_EffectiveNP_7__1up, &b_AnalysisJets_jet_selected_JET_EffectiveNP_7__1up);
   fChain->SetBranchAddress("AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_JET_EffectiveNP_7__1up", &AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_JET_EffectiveNP_7__1up, &b_AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_JET_EffectiveNP_7__1up);
   fChain->SetBranchAddress("METInfo_MET_JET_EffectiveNP_7__1up", &METInfo_MET_JET_EffectiveNP_7__1up, &b_METInfo_MET_JET_EffectiveNP_7__1up);
   fChain->SetBranchAddress("METInfo_METPhi_JET_EffectiveNP_7__1up", &METInfo_METPhi_JET_EffectiveNP_7__1up, &b_METInfo_METPhi_JET_EffectiveNP_7__1up);
   fChain->SetBranchAddress("EventInfo_jvt_effSF_JET_EffectiveNP_7__1down", &EventInfo_jvt_effSF_JET_EffectiveNP_7__1down, &b_EventInfo_jvt_effSF_JET_EffectiveNP_7__1down);
   fChain->SetBranchAddress("AnalysisElectrons_el_selected_JET_EffectiveNP_7__1down", &AnalysisElectrons_el_selected_JET_EffectiveNP_7__1down, &b_AnalysisElectrons_el_selected_JET_EffectiveNP_7__1down);
   fChain->SetBranchAddress("AnalysisMuons_mu_selected_JET_EffectiveNP_7__1down", &AnalysisMuons_mu_selected_JET_EffectiveNP_7__1down, &b_AnalysisMuons_mu_selected_JET_EffectiveNP_7__1down);
   fChain->SetBranchAddress("AnalysisJets_pt_JET_EffectiveNP_7__1down", &AnalysisJets_pt_JET_EffectiveNP_7__1down, &b_AnalysisJets_pt_JET_EffectiveNP_7__1down);
   fChain->SetBranchAddress("AnalysisJets_jet_selected_JET_EffectiveNP_7__1down", &AnalysisJets_jet_selected_JET_EffectiveNP_7__1down, &b_AnalysisJets_jet_selected_JET_EffectiveNP_7__1down);
   fChain->SetBranchAddress("AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_JET_EffectiveNP_7__1down", &AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_JET_EffectiveNP_7__1down, &b_AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_JET_EffectiveNP_7__1down);
   fChain->SetBranchAddress("METInfo_MET_JET_EffectiveNP_7__1down", &METInfo_MET_JET_EffectiveNP_7__1down, &b_METInfo_MET_JET_EffectiveNP_7__1down);
   fChain->SetBranchAddress("METInfo_METPhi_JET_EffectiveNP_7__1down", &METInfo_METPhi_JET_EffectiveNP_7__1down, &b_METInfo_METPhi_JET_EffectiveNP_7__1down);
   fChain->SetBranchAddress("EventInfo_jvt_effSF_JET_EffectiveNP_8restTerm__1up", &EventInfo_jvt_effSF_JET_EffectiveNP_8restTerm__1up, &b_EventInfo_jvt_effSF_JET_EffectiveNP_8restTerm__1up);
   fChain->SetBranchAddress("AnalysisElectrons_el_selected_JET_EffectiveNP_8restTerm__1up", &AnalysisElectrons_el_selected_JET_EffectiveNP_8restTerm__1up, &b_AnalysisElectrons_el_selected_JET_EffectiveNP_8restTerm__1up);
   fChain->SetBranchAddress("AnalysisMuons_mu_selected_JET_EffectiveNP_8restTerm__1up", &AnalysisMuons_mu_selected_JET_EffectiveNP_8restTerm__1up, &b_AnalysisMuons_mu_selected_JET_EffectiveNP_8restTerm__1up);
   fChain->SetBranchAddress("AnalysisJets_pt_JET_EffectiveNP_8restTerm__1up", &AnalysisJets_pt_JET_EffectiveNP_8restTerm__1up, &b_AnalysisJets_pt_JET_EffectiveNP_8restTerm__1up);
   fChain->SetBranchAddress("AnalysisJets_jet_selected_JET_EffectiveNP_8restTerm__1up", &AnalysisJets_jet_selected_JET_EffectiveNP_8restTerm__1up, &b_AnalysisJets_jet_selected_JET_EffectiveNP_8restTerm__1up);
   fChain->SetBranchAddress("AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_JET_EffectiveNP_8restTerm__1up", &AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_JET_EffectiveNP_8restTerm__1up, &b_AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_JET_EffectiveNP_8restTerm__1up);
   fChain->SetBranchAddress("METInfo_MET_JET_EffectiveNP_8restTerm__1up", &METInfo_MET_JET_EffectiveNP_8restTerm__1up, &b_METInfo_MET_JET_EffectiveNP_8restTerm__1up);
   fChain->SetBranchAddress("METInfo_METPhi_JET_EffectiveNP_8restTerm__1up", &METInfo_METPhi_JET_EffectiveNP_8restTerm__1up, &b_METInfo_METPhi_JET_EffectiveNP_8restTerm__1up);
   fChain->SetBranchAddress("EventInfo_jvt_effSF_JET_EffectiveNP_8restTerm__1down", &EventInfo_jvt_effSF_JET_EffectiveNP_8restTerm__1down, &b_EventInfo_jvt_effSF_JET_EffectiveNP_8restTerm__1down);
   fChain->SetBranchAddress("AnalysisElectrons_el_selected_JET_EffectiveNP_8restTerm__1down", &AnalysisElectrons_el_selected_JET_EffectiveNP_8restTerm__1down, &b_AnalysisElectrons_el_selected_JET_EffectiveNP_8restTerm__1down);
   fChain->SetBranchAddress("AnalysisMuons_mu_selected_JET_EffectiveNP_8restTerm__1down", &AnalysisMuons_mu_selected_JET_EffectiveNP_8restTerm__1down, &b_AnalysisMuons_mu_selected_JET_EffectiveNP_8restTerm__1down);
   fChain->SetBranchAddress("AnalysisJets_pt_JET_EffectiveNP_8restTerm__1down", &AnalysisJets_pt_JET_EffectiveNP_8restTerm__1down, &b_AnalysisJets_pt_JET_EffectiveNP_8restTerm__1down);
   fChain->SetBranchAddress("AnalysisJets_jet_selected_JET_EffectiveNP_8restTerm__1down", &AnalysisJets_jet_selected_JET_EffectiveNP_8restTerm__1down, &b_AnalysisJets_jet_selected_JET_EffectiveNP_8restTerm__1down);
   fChain->SetBranchAddress("AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_JET_EffectiveNP_8restTerm__1down", &AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_JET_EffectiveNP_8restTerm__1down, &b_AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_JET_EffectiveNP_8restTerm__1down);
   fChain->SetBranchAddress("METInfo_MET_JET_EffectiveNP_8restTerm__1down", &METInfo_MET_JET_EffectiveNP_8restTerm__1down, &b_METInfo_MET_JET_EffectiveNP_8restTerm__1down);
   fChain->SetBranchAddress("METInfo_METPhi_JET_EffectiveNP_8restTerm__1down", &METInfo_METPhi_JET_EffectiveNP_8restTerm__1down, &b_METInfo_METPhi_JET_EffectiveNP_8restTerm__1down);
   fChain->SetBranchAddress("EventInfo_jvt_effSF_JET_EtaIntercalibration_Modelling__1up", &EventInfo_jvt_effSF_JET_EtaIntercalibration_Modelling__1up, &b_EventInfo_jvt_effSF_JET_EtaIntercalibration_Modelling__1up);
   fChain->SetBranchAddress("AnalysisElectrons_el_selected_JET_EtaIntercalibration_Modelling__1up", &AnalysisElectrons_el_selected_JET_EtaIntercalibration_Modelling__1up, &b_AnalysisElectrons_el_selected_JET_EtaIntercalibration_Modelling__1up);
   fChain->SetBranchAddress("AnalysisMuons_mu_selected_JET_EtaIntercalibration_Modelling__1up", &AnalysisMuons_mu_selected_JET_EtaIntercalibration_Modelling__1up, &b_AnalysisMuons_mu_selected_JET_EtaIntercalibration_Modelling__1up);
   fChain->SetBranchAddress("AnalysisJets_pt_JET_EtaIntercalibration_Modelling__1up", &AnalysisJets_pt_JET_EtaIntercalibration_Modelling__1up, &b_AnalysisJets_pt_JET_EtaIntercalibration_Modelling__1up);
   fChain->SetBranchAddress("AnalysisJets_jet_selected_JET_EtaIntercalibration_Modelling__1up", &AnalysisJets_jet_selected_JET_EtaIntercalibration_Modelling__1up, &b_AnalysisJets_jet_selected_JET_EtaIntercalibration_Modelling__1up);
   fChain->SetBranchAddress("AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_JET_EtaIntercalibration_Modelling__1up", &AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_JET_EtaIntercalibration_Modelling__1up, &b_AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_JET_EtaIntercalibration_Modelling__1up);
   fChain->SetBranchAddress("METInfo_MET_JET_EtaIntercalibration_Modelling__1up", &METInfo_MET_JET_EtaIntercalibration_Modelling__1up, &b_METInfo_MET_JET_EtaIntercalibration_Modelling__1up);
   fChain->SetBranchAddress("METInfo_METPhi_JET_EtaIntercalibration_Modelling__1up", &METInfo_METPhi_JET_EtaIntercalibration_Modelling__1up, &b_METInfo_METPhi_JET_EtaIntercalibration_Modelling__1up);
   fChain->SetBranchAddress("EventInfo_jvt_effSF_JET_EtaIntercalibration_Modelling__1down", &EventInfo_jvt_effSF_JET_EtaIntercalibration_Modelling__1down, &b_EventInfo_jvt_effSF_JET_EtaIntercalibration_Modelling__1down);
   fChain->SetBranchAddress("AnalysisElectrons_el_selected_JET_EtaIntercalibration_Modelling__1down", &AnalysisElectrons_el_selected_JET_EtaIntercalibration_Modelling__1down, &b_AnalysisElectrons_el_selected_JET_EtaIntercalibration_Modelling__1down);
   fChain->SetBranchAddress("AnalysisMuons_mu_selected_JET_EtaIntercalibration_Modelling__1down", &AnalysisMuons_mu_selected_JET_EtaIntercalibration_Modelling__1down, &b_AnalysisMuons_mu_selected_JET_EtaIntercalibration_Modelling__1down);
   fChain->SetBranchAddress("AnalysisJets_pt_JET_EtaIntercalibration_Modelling__1down", &AnalysisJets_pt_JET_EtaIntercalibration_Modelling__1down, &b_AnalysisJets_pt_JET_EtaIntercalibration_Modelling__1down);
   fChain->SetBranchAddress("AnalysisJets_jet_selected_JET_EtaIntercalibration_Modelling__1down", &AnalysisJets_jet_selected_JET_EtaIntercalibration_Modelling__1down, &b_AnalysisJets_jet_selected_JET_EtaIntercalibration_Modelling__1down);
   fChain->SetBranchAddress("AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_JET_EtaIntercalibration_Modelling__1down", &AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_JET_EtaIntercalibration_Modelling__1down, &b_AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_JET_EtaIntercalibration_Modelling__1down);
   fChain->SetBranchAddress("METInfo_MET_JET_EtaIntercalibration_Modelling__1down", &METInfo_MET_JET_EtaIntercalibration_Modelling__1down, &b_METInfo_MET_JET_EtaIntercalibration_Modelling__1down);
   fChain->SetBranchAddress("METInfo_METPhi_JET_EtaIntercalibration_Modelling__1down", &METInfo_METPhi_JET_EtaIntercalibration_Modelling__1down, &b_METInfo_METPhi_JET_EtaIntercalibration_Modelling__1down);
   fChain->SetBranchAddress("EventInfo_jvt_effSF_JET_EtaIntercalibration_NonClosure_highE__1up", &EventInfo_jvt_effSF_JET_EtaIntercalibration_NonClosure_highE__1up, &b_EventInfo_jvt_effSF_JET_EtaIntercalibration_NonClosure_highE__1up);
   fChain->SetBranchAddress("AnalysisElectrons_el_selected_JET_EtaIntercalibration_NonClosure_highE__1up", &AnalysisElectrons_el_selected_JET_EtaIntercalibration_NonClosure_highE__1up, &b_AnalysisElectrons_el_selected_JET_EtaIntercalibration_NonClosure_highE__1up);
   fChain->SetBranchAddress("AnalysisMuons_mu_selected_JET_EtaIntercalibration_NonClosure_highE__1up", &AnalysisMuons_mu_selected_JET_EtaIntercalibration_NonClosure_highE__1up, &b_AnalysisMuons_mu_selected_JET_EtaIntercalibration_NonClosure_highE__1up);
   fChain->SetBranchAddress("AnalysisJets_pt_JET_EtaIntercalibration_NonClosure_highE__1up", &AnalysisJets_pt_JET_EtaIntercalibration_NonClosure_highE__1up, &b_AnalysisJets_pt_JET_EtaIntercalibration_NonClosure_highE__1up);
   fChain->SetBranchAddress("AnalysisJets_jet_selected_JET_EtaIntercalibration_NonClosure_highE__1up", &AnalysisJets_jet_selected_JET_EtaIntercalibration_NonClosure_highE__1up, &b_AnalysisJets_jet_selected_JET_EtaIntercalibration_NonClosure_highE__1up);
   fChain->SetBranchAddress("AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_JET_EtaIntercalibration_NonClosure_highE__1up", &AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_JET_EtaIntercalibration_NonClosure_highE__1up, &b_AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_JET_EtaIntercalibration_NonClosure_highE__1up);
   fChain->SetBranchAddress("METInfo_MET_JET_EtaIntercalibration_NonClosure_highE__1up", &METInfo_MET_JET_EtaIntercalibration_NonClosure_highE__1up, &b_METInfo_MET_JET_EtaIntercalibration_NonClosure_highE__1up);
   fChain->SetBranchAddress("METInfo_METPhi_JET_EtaIntercalibration_NonClosure_highE__1up", &METInfo_METPhi_JET_EtaIntercalibration_NonClosure_highE__1up, &b_METInfo_METPhi_JET_EtaIntercalibration_NonClosure_highE__1up);
   fChain->SetBranchAddress("EventInfo_jvt_effSF_JET_EtaIntercalibration_NonClosure_highE__1down", &EventInfo_jvt_effSF_JET_EtaIntercalibration_NonClosure_highE__1down, &b_EventInfo_jvt_effSF_JET_EtaIntercalibration_NonClosure_highE__1down);
   fChain->SetBranchAddress("AnalysisElectrons_el_selected_JET_EtaIntercalibration_NonClosure_highE__1down", &AnalysisElectrons_el_selected_JET_EtaIntercalibration_NonClosure_highE__1down, &b_AnalysisElectrons_el_selected_JET_EtaIntercalibration_NonClosure_highE__1down);
   fChain->SetBranchAddress("AnalysisMuons_mu_selected_JET_EtaIntercalibration_NonClosure_highE__1down", &AnalysisMuons_mu_selected_JET_EtaIntercalibration_NonClosure_highE__1down, &b_AnalysisMuons_mu_selected_JET_EtaIntercalibration_NonClosure_highE__1down);
   fChain->SetBranchAddress("AnalysisJets_pt_JET_EtaIntercalibration_NonClosure_highE__1down", &AnalysisJets_pt_JET_EtaIntercalibration_NonClosure_highE__1down, &b_AnalysisJets_pt_JET_EtaIntercalibration_NonClosure_highE__1down);
   fChain->SetBranchAddress("AnalysisJets_jet_selected_JET_EtaIntercalibration_NonClosure_highE__1down", &AnalysisJets_jet_selected_JET_EtaIntercalibration_NonClosure_highE__1down, &b_AnalysisJets_jet_selected_JET_EtaIntercalibration_NonClosure_highE__1down);
   fChain->SetBranchAddress("AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_JET_EtaIntercalibration_NonClosure_highE__1down", &AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_JET_EtaIntercalibration_NonClosure_highE__1down, &b_AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_JET_EtaIntercalibration_NonClosure_highE__1down);
   fChain->SetBranchAddress("METInfo_MET_JET_EtaIntercalibration_NonClosure_highE__1down", &METInfo_MET_JET_EtaIntercalibration_NonClosure_highE__1down, &b_METInfo_MET_JET_EtaIntercalibration_NonClosure_highE__1down);
   fChain->SetBranchAddress("METInfo_METPhi_JET_EtaIntercalibration_NonClosure_highE__1down", &METInfo_METPhi_JET_EtaIntercalibration_NonClosure_highE__1down, &b_METInfo_METPhi_JET_EtaIntercalibration_NonClosure_highE__1down);
   fChain->SetBranchAddress("EventInfo_jvt_effSF_JET_EtaIntercalibration_NonClosure_negEta__1up", &EventInfo_jvt_effSF_JET_EtaIntercalibration_NonClosure_negEta__1up, &b_EventInfo_jvt_effSF_JET_EtaIntercalibration_NonClosure_negEta__1up);
   fChain->SetBranchAddress("AnalysisElectrons_el_selected_JET_EtaIntercalibration_NonClosure_negEta__1up", &AnalysisElectrons_el_selected_JET_EtaIntercalibration_NonClosure_negEta__1up, &b_AnalysisElectrons_el_selected_JET_EtaIntercalibration_NonClosure_negEta__1up);
   fChain->SetBranchAddress("AnalysisMuons_mu_selected_JET_EtaIntercalibration_NonClosure_negEta__1up", &AnalysisMuons_mu_selected_JET_EtaIntercalibration_NonClosure_negEta__1up, &b_AnalysisMuons_mu_selected_JET_EtaIntercalibration_NonClosure_negEta__1up);
   fChain->SetBranchAddress("AnalysisJets_pt_JET_EtaIntercalibration_NonClosure_negEta__1up", &AnalysisJets_pt_JET_EtaIntercalibration_NonClosure_negEta__1up, &b_AnalysisJets_pt_JET_EtaIntercalibration_NonClosure_negEta__1up);
   fChain->SetBranchAddress("AnalysisJets_jet_selected_JET_EtaIntercalibration_NonClosure_negEta__1up", &AnalysisJets_jet_selected_JET_EtaIntercalibration_NonClosure_negEta__1up, &b_AnalysisJets_jet_selected_JET_EtaIntercalibration_NonClosure_negEta__1up);
   fChain->SetBranchAddress("AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_JET_EtaIntercalibration_NonClosure_negEta__1up", &AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_JET_EtaIntercalibration_NonClosure_negEta__1up, &b_AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_JET_EtaIntercalibration_NonClosure_negEta__1up);
   fChain->SetBranchAddress("METInfo_MET_JET_EtaIntercalibration_NonClosure_negEta__1up", &METInfo_MET_JET_EtaIntercalibration_NonClosure_negEta__1up, &b_METInfo_MET_JET_EtaIntercalibration_NonClosure_negEta__1up);
   fChain->SetBranchAddress("METInfo_METPhi_JET_EtaIntercalibration_NonClosure_negEta__1up", &METInfo_METPhi_JET_EtaIntercalibration_NonClosure_negEta__1up, &b_METInfo_METPhi_JET_EtaIntercalibration_NonClosure_negEta__1up);
   fChain->SetBranchAddress("EventInfo_jvt_effSF_JET_EtaIntercalibration_NonClosure_negEta__1down", &EventInfo_jvt_effSF_JET_EtaIntercalibration_NonClosure_negEta__1down, &b_EventInfo_jvt_effSF_JET_EtaIntercalibration_NonClosure_negEta__1down);
   fChain->SetBranchAddress("AnalysisElectrons_el_selected_JET_EtaIntercalibration_NonClosure_negEta__1down", &AnalysisElectrons_el_selected_JET_EtaIntercalibration_NonClosure_negEta__1down, &b_AnalysisElectrons_el_selected_JET_EtaIntercalibration_NonClosure_negEta__1down);
   fChain->SetBranchAddress("AnalysisMuons_mu_selected_JET_EtaIntercalibration_NonClosure_negEta__1down", &AnalysisMuons_mu_selected_JET_EtaIntercalibration_NonClosure_negEta__1down, &b_AnalysisMuons_mu_selected_JET_EtaIntercalibration_NonClosure_negEta__1down);
   fChain->SetBranchAddress("AnalysisJets_pt_JET_EtaIntercalibration_NonClosure_negEta__1down", &AnalysisJets_pt_JET_EtaIntercalibration_NonClosure_negEta__1down, &b_AnalysisJets_pt_JET_EtaIntercalibration_NonClosure_negEta__1down);
   fChain->SetBranchAddress("AnalysisJets_jet_selected_JET_EtaIntercalibration_NonClosure_negEta__1down", &AnalysisJets_jet_selected_JET_EtaIntercalibration_NonClosure_negEta__1down, &b_AnalysisJets_jet_selected_JET_EtaIntercalibration_NonClosure_negEta__1down);
   fChain->SetBranchAddress("AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_JET_EtaIntercalibration_NonClosure_negEta__1down", &AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_JET_EtaIntercalibration_NonClosure_negEta__1down, &b_AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_JET_EtaIntercalibration_NonClosure_negEta__1down);
   fChain->SetBranchAddress("METInfo_MET_JET_EtaIntercalibration_NonClosure_negEta__1down", &METInfo_MET_JET_EtaIntercalibration_NonClosure_negEta__1down, &b_METInfo_MET_JET_EtaIntercalibration_NonClosure_negEta__1down);
   fChain->SetBranchAddress("METInfo_METPhi_JET_EtaIntercalibration_NonClosure_negEta__1down", &METInfo_METPhi_JET_EtaIntercalibration_NonClosure_negEta__1down, &b_METInfo_METPhi_JET_EtaIntercalibration_NonClosure_negEta__1down);
   fChain->SetBranchAddress("EventInfo_jvt_effSF_JET_EtaIntercalibration_NonClosure_posEta__1up", &EventInfo_jvt_effSF_JET_EtaIntercalibration_NonClosure_posEta__1up, &b_EventInfo_jvt_effSF_JET_EtaIntercalibration_NonClosure_posEta__1up);
   fChain->SetBranchAddress("AnalysisElectrons_el_selected_JET_EtaIntercalibration_NonClosure_posEta__1up", &AnalysisElectrons_el_selected_JET_EtaIntercalibration_NonClosure_posEta__1up, &b_AnalysisElectrons_el_selected_JET_EtaIntercalibration_NonClosure_posEta__1up);
   fChain->SetBranchAddress("AnalysisMuons_mu_selected_JET_EtaIntercalibration_NonClosure_posEta__1up", &AnalysisMuons_mu_selected_JET_EtaIntercalibration_NonClosure_posEta__1up, &b_AnalysisMuons_mu_selected_JET_EtaIntercalibration_NonClosure_posEta__1up);
   fChain->SetBranchAddress("AnalysisJets_pt_JET_EtaIntercalibration_NonClosure_posEta__1up", &AnalysisJets_pt_JET_EtaIntercalibration_NonClosure_posEta__1up, &b_AnalysisJets_pt_JET_EtaIntercalibration_NonClosure_posEta__1up);
   fChain->SetBranchAddress("AnalysisJets_jet_selected_JET_EtaIntercalibration_NonClosure_posEta__1up", &AnalysisJets_jet_selected_JET_EtaIntercalibration_NonClosure_posEta__1up, &b_AnalysisJets_jet_selected_JET_EtaIntercalibration_NonClosure_posEta__1up);
   fChain->SetBranchAddress("AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_JET_EtaIntercalibration_NonClosure_posEta__1up", &AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_JET_EtaIntercalibration_NonClosure_posEta__1up, &b_AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_JET_EtaIntercalibration_NonClosure_posEta__1up);
   fChain->SetBranchAddress("METInfo_MET_JET_EtaIntercalibration_NonClosure_posEta__1up", &METInfo_MET_JET_EtaIntercalibration_NonClosure_posEta__1up, &b_METInfo_MET_JET_EtaIntercalibration_NonClosure_posEta__1up);
   fChain->SetBranchAddress("METInfo_METPhi_JET_EtaIntercalibration_NonClosure_posEta__1up", &METInfo_METPhi_JET_EtaIntercalibration_NonClosure_posEta__1up, &b_METInfo_METPhi_JET_EtaIntercalibration_NonClosure_posEta__1up);
   fChain->SetBranchAddress("EventInfo_jvt_effSF_JET_EtaIntercalibration_NonClosure_posEta__1down", &EventInfo_jvt_effSF_JET_EtaIntercalibration_NonClosure_posEta__1down, &b_EventInfo_jvt_effSF_JET_EtaIntercalibration_NonClosure_posEta__1down);
   fChain->SetBranchAddress("AnalysisElectrons_el_selected_JET_EtaIntercalibration_NonClosure_posEta__1down", &AnalysisElectrons_el_selected_JET_EtaIntercalibration_NonClosure_posEta__1down, &b_AnalysisElectrons_el_selected_JET_EtaIntercalibration_NonClosure_posEta__1down);
   fChain->SetBranchAddress("AnalysisMuons_mu_selected_JET_EtaIntercalibration_NonClosure_posEta__1down", &AnalysisMuons_mu_selected_JET_EtaIntercalibration_NonClosure_posEta__1down, &b_AnalysisMuons_mu_selected_JET_EtaIntercalibration_NonClosure_posEta__1down);
   fChain->SetBranchAddress("AnalysisJets_pt_JET_EtaIntercalibration_NonClosure_posEta__1down", &AnalysisJets_pt_JET_EtaIntercalibration_NonClosure_posEta__1down, &b_AnalysisJets_pt_JET_EtaIntercalibration_NonClosure_posEta__1down);
   fChain->SetBranchAddress("AnalysisJets_jet_selected_JET_EtaIntercalibration_NonClosure_posEta__1down", &AnalysisJets_jet_selected_JET_EtaIntercalibration_NonClosure_posEta__1down, &b_AnalysisJets_jet_selected_JET_EtaIntercalibration_NonClosure_posEta__1down);
   fChain->SetBranchAddress("AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_JET_EtaIntercalibration_NonClosure_posEta__1down", &AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_JET_EtaIntercalibration_NonClosure_posEta__1down, &b_AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_JET_EtaIntercalibration_NonClosure_posEta__1down);
   fChain->SetBranchAddress("METInfo_MET_JET_EtaIntercalibration_NonClosure_posEta__1down", &METInfo_MET_JET_EtaIntercalibration_NonClosure_posEta__1down, &b_METInfo_MET_JET_EtaIntercalibration_NonClosure_posEta__1down);
   fChain->SetBranchAddress("METInfo_METPhi_JET_EtaIntercalibration_NonClosure_posEta__1down", &METInfo_METPhi_JET_EtaIntercalibration_NonClosure_posEta__1down, &b_METInfo_METPhi_JET_EtaIntercalibration_NonClosure_posEta__1down);
   fChain->SetBranchAddress("EventInfo_jvt_effSF_JET_EtaIntercalibration_TotalStat__1up", &EventInfo_jvt_effSF_JET_EtaIntercalibration_TotalStat__1up, &b_EventInfo_jvt_effSF_JET_EtaIntercalibration_TotalStat__1up);
   fChain->SetBranchAddress("AnalysisElectrons_el_selected_JET_EtaIntercalibration_TotalStat__1up", &AnalysisElectrons_el_selected_JET_EtaIntercalibration_TotalStat__1up, &b_AnalysisElectrons_el_selected_JET_EtaIntercalibration_TotalStat__1up);
   fChain->SetBranchAddress("AnalysisMuons_mu_selected_JET_EtaIntercalibration_TotalStat__1up", &AnalysisMuons_mu_selected_JET_EtaIntercalibration_TotalStat__1up, &b_AnalysisMuons_mu_selected_JET_EtaIntercalibration_TotalStat__1up);
   fChain->SetBranchAddress("AnalysisJets_pt_JET_EtaIntercalibration_TotalStat__1up", &AnalysisJets_pt_JET_EtaIntercalibration_TotalStat__1up, &b_AnalysisJets_pt_JET_EtaIntercalibration_TotalStat__1up);
   fChain->SetBranchAddress("AnalysisJets_jet_selected_JET_EtaIntercalibration_TotalStat__1up", &AnalysisJets_jet_selected_JET_EtaIntercalibration_TotalStat__1up, &b_AnalysisJets_jet_selected_JET_EtaIntercalibration_TotalStat__1up);
   fChain->SetBranchAddress("AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_JET_EtaIntercalibration_TotalStat__1up", &AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_JET_EtaIntercalibration_TotalStat__1up, &b_AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_JET_EtaIntercalibration_TotalStat__1up);
   fChain->SetBranchAddress("METInfo_MET_JET_EtaIntercalibration_TotalStat__1up", &METInfo_MET_JET_EtaIntercalibration_TotalStat__1up, &b_METInfo_MET_JET_EtaIntercalibration_TotalStat__1up);
   fChain->SetBranchAddress("METInfo_METPhi_JET_EtaIntercalibration_TotalStat__1up", &METInfo_METPhi_JET_EtaIntercalibration_TotalStat__1up, &b_METInfo_METPhi_JET_EtaIntercalibration_TotalStat__1up);
   fChain->SetBranchAddress("EventInfo_jvt_effSF_JET_EtaIntercalibration_TotalStat__1down", &EventInfo_jvt_effSF_JET_EtaIntercalibration_TotalStat__1down, &b_EventInfo_jvt_effSF_JET_EtaIntercalibration_TotalStat__1down);
   fChain->SetBranchAddress("AnalysisElectrons_el_selected_JET_EtaIntercalibration_TotalStat__1down", &AnalysisElectrons_el_selected_JET_EtaIntercalibration_TotalStat__1down, &b_AnalysisElectrons_el_selected_JET_EtaIntercalibration_TotalStat__1down);
   fChain->SetBranchAddress("AnalysisMuons_mu_selected_JET_EtaIntercalibration_TotalStat__1down", &AnalysisMuons_mu_selected_JET_EtaIntercalibration_TotalStat__1down, &b_AnalysisMuons_mu_selected_JET_EtaIntercalibration_TotalStat__1down);
   fChain->SetBranchAddress("AnalysisJets_pt_JET_EtaIntercalibration_TotalStat__1down", &AnalysisJets_pt_JET_EtaIntercalibration_TotalStat__1down, &b_AnalysisJets_pt_JET_EtaIntercalibration_TotalStat__1down);
   fChain->SetBranchAddress("AnalysisJets_jet_selected_JET_EtaIntercalibration_TotalStat__1down", &AnalysisJets_jet_selected_JET_EtaIntercalibration_TotalStat__1down, &b_AnalysisJets_jet_selected_JET_EtaIntercalibration_TotalStat__1down);
   fChain->SetBranchAddress("AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_JET_EtaIntercalibration_TotalStat__1down", &AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_JET_EtaIntercalibration_TotalStat__1down, &b_AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_JET_EtaIntercalibration_TotalStat__1down);
   fChain->SetBranchAddress("METInfo_MET_JET_EtaIntercalibration_TotalStat__1down", &METInfo_MET_JET_EtaIntercalibration_TotalStat__1down, &b_METInfo_MET_JET_EtaIntercalibration_TotalStat__1down);
   fChain->SetBranchAddress("METInfo_METPhi_JET_EtaIntercalibration_TotalStat__1down", &METInfo_METPhi_JET_EtaIntercalibration_TotalStat__1down, &b_METInfo_METPhi_JET_EtaIntercalibration_TotalStat__1down);
   fChain->SetBranchAddress("EventInfo_jvt_effSF_JET_Flavor_Composition__1up", &EventInfo_jvt_effSF_JET_Flavor_Composition__1up, &b_EventInfo_jvt_effSF_JET_Flavor_Composition__1up);
   fChain->SetBranchAddress("AnalysisElectrons_el_selected_JET_Flavor_Composition__1up", &AnalysisElectrons_el_selected_JET_Flavor_Composition__1up, &b_AnalysisElectrons_el_selected_JET_Flavor_Composition__1up);
   fChain->SetBranchAddress("AnalysisMuons_mu_selected_JET_Flavor_Composition__1up", &AnalysisMuons_mu_selected_JET_Flavor_Composition__1up, &b_AnalysisMuons_mu_selected_JET_Flavor_Composition__1up);
   fChain->SetBranchAddress("AnalysisJets_pt_JET_Flavor_Composition__1up", &AnalysisJets_pt_JET_Flavor_Composition__1up, &b_AnalysisJets_pt_JET_Flavor_Composition__1up);
   fChain->SetBranchAddress("AnalysisJets_jet_selected_JET_Flavor_Composition__1up", &AnalysisJets_jet_selected_JET_Flavor_Composition__1up, &b_AnalysisJets_jet_selected_JET_Flavor_Composition__1up);
   fChain->SetBranchAddress("AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_JET_Flavor_Composition__1up", &AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_JET_Flavor_Composition__1up, &b_AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_JET_Flavor_Composition__1up);
   fChain->SetBranchAddress("METInfo_MET_JET_Flavor_Composition__1up", &METInfo_MET_JET_Flavor_Composition__1up, &b_METInfo_MET_JET_Flavor_Composition__1up);
   fChain->SetBranchAddress("METInfo_METPhi_JET_Flavor_Composition__1up", &METInfo_METPhi_JET_Flavor_Composition__1up, &b_METInfo_METPhi_JET_Flavor_Composition__1up);
   fChain->SetBranchAddress("EventInfo_jvt_effSF_JET_Flavor_Composition__1down", &EventInfo_jvt_effSF_JET_Flavor_Composition__1down, &b_EventInfo_jvt_effSF_JET_Flavor_Composition__1down);
   fChain->SetBranchAddress("AnalysisElectrons_el_selected_JET_Flavor_Composition__1down", &AnalysisElectrons_el_selected_JET_Flavor_Composition__1down, &b_AnalysisElectrons_el_selected_JET_Flavor_Composition__1down);
   fChain->SetBranchAddress("AnalysisMuons_mu_selected_JET_Flavor_Composition__1down", &AnalysisMuons_mu_selected_JET_Flavor_Composition__1down, &b_AnalysisMuons_mu_selected_JET_Flavor_Composition__1down);
   fChain->SetBranchAddress("AnalysisJets_pt_JET_Flavor_Composition__1down", &AnalysisJets_pt_JET_Flavor_Composition__1down, &b_AnalysisJets_pt_JET_Flavor_Composition__1down);
   fChain->SetBranchAddress("AnalysisJets_jet_selected_JET_Flavor_Composition__1down", &AnalysisJets_jet_selected_JET_Flavor_Composition__1down, &b_AnalysisJets_jet_selected_JET_Flavor_Composition__1down);
   fChain->SetBranchAddress("AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_JET_Flavor_Composition__1down", &AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_JET_Flavor_Composition__1down, &b_AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_JET_Flavor_Composition__1down);
   fChain->SetBranchAddress("METInfo_MET_JET_Flavor_Composition__1down", &METInfo_MET_JET_Flavor_Composition__1down, &b_METInfo_MET_JET_Flavor_Composition__1down);
   fChain->SetBranchAddress("METInfo_METPhi_JET_Flavor_Composition__1down", &METInfo_METPhi_JET_Flavor_Composition__1down, &b_METInfo_METPhi_JET_Flavor_Composition__1down);
   fChain->SetBranchAddress("EventInfo_jvt_effSF_JET_Flavor_Response__1up", &EventInfo_jvt_effSF_JET_Flavor_Response__1up, &b_EventInfo_jvt_effSF_JET_Flavor_Response__1up);
   fChain->SetBranchAddress("AnalysisElectrons_el_selected_JET_Flavor_Response__1up", &AnalysisElectrons_el_selected_JET_Flavor_Response__1up, &b_AnalysisElectrons_el_selected_JET_Flavor_Response__1up);
   fChain->SetBranchAddress("AnalysisMuons_mu_selected_JET_Flavor_Response__1up", &AnalysisMuons_mu_selected_JET_Flavor_Response__1up, &b_AnalysisMuons_mu_selected_JET_Flavor_Response__1up);
   fChain->SetBranchAddress("AnalysisJets_pt_JET_Flavor_Response__1up", &AnalysisJets_pt_JET_Flavor_Response__1up, &b_AnalysisJets_pt_JET_Flavor_Response__1up);
   fChain->SetBranchAddress("AnalysisJets_jet_selected_JET_Flavor_Response__1up", &AnalysisJets_jet_selected_JET_Flavor_Response__1up, &b_AnalysisJets_jet_selected_JET_Flavor_Response__1up);
   fChain->SetBranchAddress("AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_JET_Flavor_Response__1up", &AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_JET_Flavor_Response__1up, &b_AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_JET_Flavor_Response__1up);
   fChain->SetBranchAddress("METInfo_MET_JET_Flavor_Response__1up", &METInfo_MET_JET_Flavor_Response__1up, &b_METInfo_MET_JET_Flavor_Response__1up);
   fChain->SetBranchAddress("METInfo_METPhi_JET_Flavor_Response__1up", &METInfo_METPhi_JET_Flavor_Response__1up, &b_METInfo_METPhi_JET_Flavor_Response__1up);
   fChain->SetBranchAddress("EventInfo_jvt_effSF_JET_Flavor_Response__1down", &EventInfo_jvt_effSF_JET_Flavor_Response__1down, &b_EventInfo_jvt_effSF_JET_Flavor_Response__1down);
   fChain->SetBranchAddress("AnalysisElectrons_el_selected_JET_Flavor_Response__1down", &AnalysisElectrons_el_selected_JET_Flavor_Response__1down, &b_AnalysisElectrons_el_selected_JET_Flavor_Response__1down);
   fChain->SetBranchAddress("AnalysisMuons_mu_selected_JET_Flavor_Response__1down", &AnalysisMuons_mu_selected_JET_Flavor_Response__1down, &b_AnalysisMuons_mu_selected_JET_Flavor_Response__1down);
   fChain->SetBranchAddress("AnalysisJets_pt_JET_Flavor_Response__1down", &AnalysisJets_pt_JET_Flavor_Response__1down, &b_AnalysisJets_pt_JET_Flavor_Response__1down);
   fChain->SetBranchAddress("AnalysisJets_jet_selected_JET_Flavor_Response__1down", &AnalysisJets_jet_selected_JET_Flavor_Response__1down, &b_AnalysisJets_jet_selected_JET_Flavor_Response__1down);
   fChain->SetBranchAddress("AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_JET_Flavor_Response__1down", &AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_JET_Flavor_Response__1down, &b_AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_JET_Flavor_Response__1down);
   fChain->SetBranchAddress("METInfo_MET_JET_Flavor_Response__1down", &METInfo_MET_JET_Flavor_Response__1down, &b_METInfo_MET_JET_Flavor_Response__1down);
   fChain->SetBranchAddress("METInfo_METPhi_JET_Flavor_Response__1down", &METInfo_METPhi_JET_Flavor_Response__1down, &b_METInfo_METPhi_JET_Flavor_Response__1down);
   fChain->SetBranchAddress("EventInfo_jvt_effSF_JET_JER_DataVsMC_MC16__1up", &EventInfo_jvt_effSF_JET_JER_DataVsMC_MC16__1up, &b_EventInfo_jvt_effSF_JET_JER_DataVsMC_MC16__1up);
   fChain->SetBranchAddress("AnalysisElectrons_el_selected_JET_JER_DataVsMC_MC16__1up", &AnalysisElectrons_el_selected_JET_JER_DataVsMC_MC16__1up, &b_AnalysisElectrons_el_selected_JET_JER_DataVsMC_MC16__1up);
   fChain->SetBranchAddress("AnalysisMuons_mu_selected_JET_JER_DataVsMC_MC16__1up", &AnalysisMuons_mu_selected_JET_JER_DataVsMC_MC16__1up, &b_AnalysisMuons_mu_selected_JET_JER_DataVsMC_MC16__1up);
   fChain->SetBranchAddress("AnalysisJets_pt_JET_JER_DataVsMC_MC16__1up", &AnalysisJets_pt_JET_JER_DataVsMC_MC16__1up, &b_AnalysisJets_pt_JET_JER_DataVsMC_MC16__1up);
   fChain->SetBranchAddress("AnalysisJets_jet_selected_JET_JER_DataVsMC_MC16__1up", &AnalysisJets_jet_selected_JET_JER_DataVsMC_MC16__1up, &b_AnalysisJets_jet_selected_JET_JER_DataVsMC_MC16__1up);
   fChain->SetBranchAddress("AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_JET_JER_DataVsMC_MC16__1up", &AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_JET_JER_DataVsMC_MC16__1up, &b_AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_JET_JER_DataVsMC_MC16__1up);
   fChain->SetBranchAddress("METInfo_MET_JET_JER_DataVsMC_MC16__1up", &METInfo_MET_JET_JER_DataVsMC_MC16__1up, &b_METInfo_MET_JET_JER_DataVsMC_MC16__1up);
   fChain->SetBranchAddress("METInfo_METPhi_JET_JER_DataVsMC_MC16__1up", &METInfo_METPhi_JET_JER_DataVsMC_MC16__1up, &b_METInfo_METPhi_JET_JER_DataVsMC_MC16__1up);
   fChain->SetBranchAddress("EventInfo_jvt_effSF_JET_JER_DataVsMC_MC16__1down", &EventInfo_jvt_effSF_JET_JER_DataVsMC_MC16__1down, &b_EventInfo_jvt_effSF_JET_JER_DataVsMC_MC16__1down);
   fChain->SetBranchAddress("AnalysisElectrons_el_selected_JET_JER_DataVsMC_MC16__1down", &AnalysisElectrons_el_selected_JET_JER_DataVsMC_MC16__1down, &b_AnalysisElectrons_el_selected_JET_JER_DataVsMC_MC16__1down);
   fChain->SetBranchAddress("AnalysisMuons_mu_selected_JET_JER_DataVsMC_MC16__1down", &AnalysisMuons_mu_selected_JET_JER_DataVsMC_MC16__1down, &b_AnalysisMuons_mu_selected_JET_JER_DataVsMC_MC16__1down);
   fChain->SetBranchAddress("AnalysisJets_pt_JET_JER_DataVsMC_MC16__1down", &AnalysisJets_pt_JET_JER_DataVsMC_MC16__1down, &b_AnalysisJets_pt_JET_JER_DataVsMC_MC16__1down);
   fChain->SetBranchAddress("AnalysisJets_jet_selected_JET_JER_DataVsMC_MC16__1down", &AnalysisJets_jet_selected_JET_JER_DataVsMC_MC16__1down, &b_AnalysisJets_jet_selected_JET_JER_DataVsMC_MC16__1down);
   fChain->SetBranchAddress("AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_JET_JER_DataVsMC_MC16__1down", &AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_JET_JER_DataVsMC_MC16__1down, &b_AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_JET_JER_DataVsMC_MC16__1down);
   fChain->SetBranchAddress("METInfo_MET_JET_JER_DataVsMC_MC16__1down", &METInfo_MET_JET_JER_DataVsMC_MC16__1down, &b_METInfo_MET_JET_JER_DataVsMC_MC16__1down);
   fChain->SetBranchAddress("METInfo_METPhi_JET_JER_DataVsMC_MC16__1down", &METInfo_METPhi_JET_JER_DataVsMC_MC16__1down, &b_METInfo_METPhi_JET_JER_DataVsMC_MC16__1down);
   fChain->SetBranchAddress("EventInfo_jvt_effSF_JET_JER_EffectiveNP_1__1up", &EventInfo_jvt_effSF_JET_JER_EffectiveNP_1__1up, &b_EventInfo_jvt_effSF_JET_JER_EffectiveNP_1__1up);
   fChain->SetBranchAddress("AnalysisElectrons_el_selected_JET_JER_EffectiveNP_1__1up", &AnalysisElectrons_el_selected_JET_JER_EffectiveNP_1__1up, &b_AnalysisElectrons_el_selected_JET_JER_EffectiveNP_1__1up);
   fChain->SetBranchAddress("AnalysisMuons_mu_selected_JET_JER_EffectiveNP_1__1up", &AnalysisMuons_mu_selected_JET_JER_EffectiveNP_1__1up, &b_AnalysisMuons_mu_selected_JET_JER_EffectiveNP_1__1up);
   fChain->SetBranchAddress("AnalysisJets_pt_JET_JER_EffectiveNP_1__1up", &AnalysisJets_pt_JET_JER_EffectiveNP_1__1up, &b_AnalysisJets_pt_JET_JER_EffectiveNP_1__1up);
   fChain->SetBranchAddress("AnalysisJets_jet_selected_JET_JER_EffectiveNP_1__1up", &AnalysisJets_jet_selected_JET_JER_EffectiveNP_1__1up, &b_AnalysisJets_jet_selected_JET_JER_EffectiveNP_1__1up);
   fChain->SetBranchAddress("AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_JET_JER_EffectiveNP_1__1up", &AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_JET_JER_EffectiveNP_1__1up, &b_AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_JET_JER_EffectiveNP_1__1up);
   fChain->SetBranchAddress("METInfo_MET_JET_JER_EffectiveNP_1__1up", &METInfo_MET_JET_JER_EffectiveNP_1__1up, &b_METInfo_MET_JET_JER_EffectiveNP_1__1up);
   fChain->SetBranchAddress("METInfo_METPhi_JET_JER_EffectiveNP_1__1up", &METInfo_METPhi_JET_JER_EffectiveNP_1__1up, &b_METInfo_METPhi_JET_JER_EffectiveNP_1__1up);
   fChain->SetBranchAddress("EventInfo_jvt_effSF_JET_JER_EffectiveNP_1__1down", &EventInfo_jvt_effSF_JET_JER_EffectiveNP_1__1down, &b_EventInfo_jvt_effSF_JET_JER_EffectiveNP_1__1down);
   fChain->SetBranchAddress("AnalysisElectrons_el_selected_JET_JER_EffectiveNP_1__1down", &AnalysisElectrons_el_selected_JET_JER_EffectiveNP_1__1down, &b_AnalysisElectrons_el_selected_JET_JER_EffectiveNP_1__1down);
   fChain->SetBranchAddress("AnalysisMuons_mu_selected_JET_JER_EffectiveNP_1__1down", &AnalysisMuons_mu_selected_JET_JER_EffectiveNP_1__1down, &b_AnalysisMuons_mu_selected_JET_JER_EffectiveNP_1__1down);
   fChain->SetBranchAddress("AnalysisJets_pt_JET_JER_EffectiveNP_1__1down", &AnalysisJets_pt_JET_JER_EffectiveNP_1__1down, &b_AnalysisJets_pt_JET_JER_EffectiveNP_1__1down);
   fChain->SetBranchAddress("AnalysisJets_jet_selected_JET_JER_EffectiveNP_1__1down", &AnalysisJets_jet_selected_JET_JER_EffectiveNP_1__1down, &b_AnalysisJets_jet_selected_JET_JER_EffectiveNP_1__1down);
   fChain->SetBranchAddress("AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_JET_JER_EffectiveNP_1__1down", &AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_JET_JER_EffectiveNP_1__1down, &b_AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_JET_JER_EffectiveNP_1__1down);
   fChain->SetBranchAddress("METInfo_MET_JET_JER_EffectiveNP_1__1down", &METInfo_MET_JET_JER_EffectiveNP_1__1down, &b_METInfo_MET_JET_JER_EffectiveNP_1__1down);
   fChain->SetBranchAddress("METInfo_METPhi_JET_JER_EffectiveNP_1__1down", &METInfo_METPhi_JET_JER_EffectiveNP_1__1down, &b_METInfo_METPhi_JET_JER_EffectiveNP_1__1down);
   fChain->SetBranchAddress("EventInfo_jvt_effSF_JET_JER_EffectiveNP_2__1up", &EventInfo_jvt_effSF_JET_JER_EffectiveNP_2__1up, &b_EventInfo_jvt_effSF_JET_JER_EffectiveNP_2__1up);
   fChain->SetBranchAddress("AnalysisElectrons_el_selected_JET_JER_EffectiveNP_2__1up", &AnalysisElectrons_el_selected_JET_JER_EffectiveNP_2__1up, &b_AnalysisElectrons_el_selected_JET_JER_EffectiveNP_2__1up);
   fChain->SetBranchAddress("AnalysisMuons_mu_selected_JET_JER_EffectiveNP_2__1up", &AnalysisMuons_mu_selected_JET_JER_EffectiveNP_2__1up, &b_AnalysisMuons_mu_selected_JET_JER_EffectiveNP_2__1up);
   fChain->SetBranchAddress("AnalysisJets_pt_JET_JER_EffectiveNP_2__1up", &AnalysisJets_pt_JET_JER_EffectiveNP_2__1up, &b_AnalysisJets_pt_JET_JER_EffectiveNP_2__1up);
   fChain->SetBranchAddress("AnalysisJets_jet_selected_JET_JER_EffectiveNP_2__1up", &AnalysisJets_jet_selected_JET_JER_EffectiveNP_2__1up, &b_AnalysisJets_jet_selected_JET_JER_EffectiveNP_2__1up);
   fChain->SetBranchAddress("AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_JET_JER_EffectiveNP_2__1up", &AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_JET_JER_EffectiveNP_2__1up, &b_AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_JET_JER_EffectiveNP_2__1up);
   fChain->SetBranchAddress("METInfo_MET_JET_JER_EffectiveNP_2__1up", &METInfo_MET_JET_JER_EffectiveNP_2__1up, &b_METInfo_MET_JET_JER_EffectiveNP_2__1up);
   fChain->SetBranchAddress("METInfo_METPhi_JET_JER_EffectiveNP_2__1up", &METInfo_METPhi_JET_JER_EffectiveNP_2__1up, &b_METInfo_METPhi_JET_JER_EffectiveNP_2__1up);
   fChain->SetBranchAddress("EventInfo_jvt_effSF_JET_JER_EffectiveNP_2__1down", &EventInfo_jvt_effSF_JET_JER_EffectiveNP_2__1down, &b_EventInfo_jvt_effSF_JET_JER_EffectiveNP_2__1down);
   fChain->SetBranchAddress("AnalysisElectrons_el_selected_JET_JER_EffectiveNP_2__1down", &AnalysisElectrons_el_selected_JET_JER_EffectiveNP_2__1down, &b_AnalysisElectrons_el_selected_JET_JER_EffectiveNP_2__1down);
   fChain->SetBranchAddress("AnalysisMuons_mu_selected_JET_JER_EffectiveNP_2__1down", &AnalysisMuons_mu_selected_JET_JER_EffectiveNP_2__1down, &b_AnalysisMuons_mu_selected_JET_JER_EffectiveNP_2__1down);
   fChain->SetBranchAddress("AnalysisJets_pt_JET_JER_EffectiveNP_2__1down", &AnalysisJets_pt_JET_JER_EffectiveNP_2__1down, &b_AnalysisJets_pt_JET_JER_EffectiveNP_2__1down);
   fChain->SetBranchAddress("AnalysisJets_jet_selected_JET_JER_EffectiveNP_2__1down", &AnalysisJets_jet_selected_JET_JER_EffectiveNP_2__1down, &b_AnalysisJets_jet_selected_JET_JER_EffectiveNP_2__1down);
   fChain->SetBranchAddress("AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_JET_JER_EffectiveNP_2__1down", &AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_JET_JER_EffectiveNP_2__1down, &b_AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_JET_JER_EffectiveNP_2__1down);
   fChain->SetBranchAddress("METInfo_MET_JET_JER_EffectiveNP_2__1down", &METInfo_MET_JET_JER_EffectiveNP_2__1down, &b_METInfo_MET_JET_JER_EffectiveNP_2__1down);
   fChain->SetBranchAddress("METInfo_METPhi_JET_JER_EffectiveNP_2__1down", &METInfo_METPhi_JET_JER_EffectiveNP_2__1down, &b_METInfo_METPhi_JET_JER_EffectiveNP_2__1down);
   fChain->SetBranchAddress("EventInfo_jvt_effSF_JET_JER_EffectiveNP_3__1up", &EventInfo_jvt_effSF_JET_JER_EffectiveNP_3__1up, &b_EventInfo_jvt_effSF_JET_JER_EffectiveNP_3__1up);
   fChain->SetBranchAddress("AnalysisElectrons_el_selected_JET_JER_EffectiveNP_3__1up", &AnalysisElectrons_el_selected_JET_JER_EffectiveNP_3__1up, &b_AnalysisElectrons_el_selected_JET_JER_EffectiveNP_3__1up);
   fChain->SetBranchAddress("AnalysisMuons_mu_selected_JET_JER_EffectiveNP_3__1up", &AnalysisMuons_mu_selected_JET_JER_EffectiveNP_3__1up, &b_AnalysisMuons_mu_selected_JET_JER_EffectiveNP_3__1up);
   fChain->SetBranchAddress("AnalysisJets_pt_JET_JER_EffectiveNP_3__1up", &AnalysisJets_pt_JET_JER_EffectiveNP_3__1up, &b_AnalysisJets_pt_JET_JER_EffectiveNP_3__1up);
   fChain->SetBranchAddress("AnalysisJets_jet_selected_JET_JER_EffectiveNP_3__1up", &AnalysisJets_jet_selected_JET_JER_EffectiveNP_3__1up, &b_AnalysisJets_jet_selected_JET_JER_EffectiveNP_3__1up);
   fChain->SetBranchAddress("AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_JET_JER_EffectiveNP_3__1up", &AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_JET_JER_EffectiveNP_3__1up, &b_AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_JET_JER_EffectiveNP_3__1up);
   fChain->SetBranchAddress("METInfo_MET_JET_JER_EffectiveNP_3__1up", &METInfo_MET_JET_JER_EffectiveNP_3__1up, &b_METInfo_MET_JET_JER_EffectiveNP_3__1up);
   fChain->SetBranchAddress("METInfo_METPhi_JET_JER_EffectiveNP_3__1up", &METInfo_METPhi_JET_JER_EffectiveNP_3__1up, &b_METInfo_METPhi_JET_JER_EffectiveNP_3__1up);
   fChain->SetBranchAddress("EventInfo_jvt_effSF_JET_JER_EffectiveNP_3__1down", &EventInfo_jvt_effSF_JET_JER_EffectiveNP_3__1down, &b_EventInfo_jvt_effSF_JET_JER_EffectiveNP_3__1down);
   fChain->SetBranchAddress("AnalysisElectrons_el_selected_JET_JER_EffectiveNP_3__1down", &AnalysisElectrons_el_selected_JET_JER_EffectiveNP_3__1down, &b_AnalysisElectrons_el_selected_JET_JER_EffectiveNP_3__1down);
   fChain->SetBranchAddress("AnalysisMuons_mu_selected_JET_JER_EffectiveNP_3__1down", &AnalysisMuons_mu_selected_JET_JER_EffectiveNP_3__1down, &b_AnalysisMuons_mu_selected_JET_JER_EffectiveNP_3__1down);
   fChain->SetBranchAddress("AnalysisJets_pt_JET_JER_EffectiveNP_3__1down", &AnalysisJets_pt_JET_JER_EffectiveNP_3__1down, &b_AnalysisJets_pt_JET_JER_EffectiveNP_3__1down);
   fChain->SetBranchAddress("AnalysisJets_jet_selected_JET_JER_EffectiveNP_3__1down", &AnalysisJets_jet_selected_JET_JER_EffectiveNP_3__1down, &b_AnalysisJets_jet_selected_JET_JER_EffectiveNP_3__1down);
   fChain->SetBranchAddress("AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_JET_JER_EffectiveNP_3__1down", &AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_JET_JER_EffectiveNP_3__1down, &b_AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_JET_JER_EffectiveNP_3__1down);
   fChain->SetBranchAddress("METInfo_MET_JET_JER_EffectiveNP_3__1down", &METInfo_MET_JET_JER_EffectiveNP_3__1down, &b_METInfo_MET_JET_JER_EffectiveNP_3__1down);
   fChain->SetBranchAddress("METInfo_METPhi_JET_JER_EffectiveNP_3__1down", &METInfo_METPhi_JET_JER_EffectiveNP_3__1down, &b_METInfo_METPhi_JET_JER_EffectiveNP_3__1down);
   fChain->SetBranchAddress("EventInfo_jvt_effSF_JET_JER_EffectiveNP_4__1up", &EventInfo_jvt_effSF_JET_JER_EffectiveNP_4__1up, &b_EventInfo_jvt_effSF_JET_JER_EffectiveNP_4__1up);
   fChain->SetBranchAddress("AnalysisElectrons_el_selected_JET_JER_EffectiveNP_4__1up", &AnalysisElectrons_el_selected_JET_JER_EffectiveNP_4__1up, &b_AnalysisElectrons_el_selected_JET_JER_EffectiveNP_4__1up);
   fChain->SetBranchAddress("AnalysisMuons_mu_selected_JET_JER_EffectiveNP_4__1up", &AnalysisMuons_mu_selected_JET_JER_EffectiveNP_4__1up, &b_AnalysisMuons_mu_selected_JET_JER_EffectiveNP_4__1up);
   fChain->SetBranchAddress("AnalysisJets_pt_JET_JER_EffectiveNP_4__1up", &AnalysisJets_pt_JET_JER_EffectiveNP_4__1up, &b_AnalysisJets_pt_JET_JER_EffectiveNP_4__1up);
   fChain->SetBranchAddress("AnalysisJets_jet_selected_JET_JER_EffectiveNP_4__1up", &AnalysisJets_jet_selected_JET_JER_EffectiveNP_4__1up, &b_AnalysisJets_jet_selected_JET_JER_EffectiveNP_4__1up);
   fChain->SetBranchAddress("AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_JET_JER_EffectiveNP_4__1up", &AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_JET_JER_EffectiveNP_4__1up, &b_AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_JET_JER_EffectiveNP_4__1up);
   fChain->SetBranchAddress("METInfo_MET_JET_JER_EffectiveNP_4__1up", &METInfo_MET_JET_JER_EffectiveNP_4__1up, &b_METInfo_MET_JET_JER_EffectiveNP_4__1up);
   fChain->SetBranchAddress("METInfo_METPhi_JET_JER_EffectiveNP_4__1up", &METInfo_METPhi_JET_JER_EffectiveNP_4__1up, &b_METInfo_METPhi_JET_JER_EffectiveNP_4__1up);
   fChain->SetBranchAddress("EventInfo_jvt_effSF_JET_JER_EffectiveNP_4__1down", &EventInfo_jvt_effSF_JET_JER_EffectiveNP_4__1down, &b_EventInfo_jvt_effSF_JET_JER_EffectiveNP_4__1down);
   fChain->SetBranchAddress("AnalysisElectrons_el_selected_JET_JER_EffectiveNP_4__1down", &AnalysisElectrons_el_selected_JET_JER_EffectiveNP_4__1down, &b_AnalysisElectrons_el_selected_JET_JER_EffectiveNP_4__1down);
   fChain->SetBranchAddress("AnalysisMuons_mu_selected_JET_JER_EffectiveNP_4__1down", &AnalysisMuons_mu_selected_JET_JER_EffectiveNP_4__1down, &b_AnalysisMuons_mu_selected_JET_JER_EffectiveNP_4__1down);
   fChain->SetBranchAddress("AnalysisJets_pt_JET_JER_EffectiveNP_4__1down", &AnalysisJets_pt_JET_JER_EffectiveNP_4__1down, &b_AnalysisJets_pt_JET_JER_EffectiveNP_4__1down);
   fChain->SetBranchAddress("AnalysisJets_jet_selected_JET_JER_EffectiveNP_4__1down", &AnalysisJets_jet_selected_JET_JER_EffectiveNP_4__1down, &b_AnalysisJets_jet_selected_JET_JER_EffectiveNP_4__1down);
   fChain->SetBranchAddress("AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_JET_JER_EffectiveNP_4__1down", &AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_JET_JER_EffectiveNP_4__1down, &b_AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_JET_JER_EffectiveNP_4__1down);
   fChain->SetBranchAddress("METInfo_MET_JET_JER_EffectiveNP_4__1down", &METInfo_MET_JET_JER_EffectiveNP_4__1down, &b_METInfo_MET_JET_JER_EffectiveNP_4__1down);
   fChain->SetBranchAddress("METInfo_METPhi_JET_JER_EffectiveNP_4__1down", &METInfo_METPhi_JET_JER_EffectiveNP_4__1down, &b_METInfo_METPhi_JET_JER_EffectiveNP_4__1down);
   fChain->SetBranchAddress("EventInfo_jvt_effSF_JET_JER_EffectiveNP_5__1up", &EventInfo_jvt_effSF_JET_JER_EffectiveNP_5__1up, &b_EventInfo_jvt_effSF_JET_JER_EffectiveNP_5__1up);
   fChain->SetBranchAddress("AnalysisElectrons_el_selected_JET_JER_EffectiveNP_5__1up", &AnalysisElectrons_el_selected_JET_JER_EffectiveNP_5__1up, &b_AnalysisElectrons_el_selected_JET_JER_EffectiveNP_5__1up);
   fChain->SetBranchAddress("AnalysisMuons_mu_selected_JET_JER_EffectiveNP_5__1up", &AnalysisMuons_mu_selected_JET_JER_EffectiveNP_5__1up, &b_AnalysisMuons_mu_selected_JET_JER_EffectiveNP_5__1up);
   fChain->SetBranchAddress("AnalysisJets_pt_JET_JER_EffectiveNP_5__1up", &AnalysisJets_pt_JET_JER_EffectiveNP_5__1up, &b_AnalysisJets_pt_JET_JER_EffectiveNP_5__1up);
   fChain->SetBranchAddress("AnalysisJets_jet_selected_JET_JER_EffectiveNP_5__1up", &AnalysisJets_jet_selected_JET_JER_EffectiveNP_5__1up, &b_AnalysisJets_jet_selected_JET_JER_EffectiveNP_5__1up);
   fChain->SetBranchAddress("AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_JET_JER_EffectiveNP_5__1up", &AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_JET_JER_EffectiveNP_5__1up, &b_AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_JET_JER_EffectiveNP_5__1up);
   fChain->SetBranchAddress("METInfo_MET_JET_JER_EffectiveNP_5__1up", &METInfo_MET_JET_JER_EffectiveNP_5__1up, &b_METInfo_MET_JET_JER_EffectiveNP_5__1up);
   fChain->SetBranchAddress("METInfo_METPhi_JET_JER_EffectiveNP_5__1up", &METInfo_METPhi_JET_JER_EffectiveNP_5__1up, &b_METInfo_METPhi_JET_JER_EffectiveNP_5__1up);
   fChain->SetBranchAddress("EventInfo_jvt_effSF_JET_JER_EffectiveNP_5__1down", &EventInfo_jvt_effSF_JET_JER_EffectiveNP_5__1down, &b_EventInfo_jvt_effSF_JET_JER_EffectiveNP_5__1down);
   fChain->SetBranchAddress("AnalysisElectrons_el_selected_JET_JER_EffectiveNP_5__1down", &AnalysisElectrons_el_selected_JET_JER_EffectiveNP_5__1down, &b_AnalysisElectrons_el_selected_JET_JER_EffectiveNP_5__1down);
   fChain->SetBranchAddress("AnalysisMuons_mu_selected_JET_JER_EffectiveNP_5__1down", &AnalysisMuons_mu_selected_JET_JER_EffectiveNP_5__1down, &b_AnalysisMuons_mu_selected_JET_JER_EffectiveNP_5__1down);
   fChain->SetBranchAddress("AnalysisJets_pt_JET_JER_EffectiveNP_5__1down", &AnalysisJets_pt_JET_JER_EffectiveNP_5__1down, &b_AnalysisJets_pt_JET_JER_EffectiveNP_5__1down);
   fChain->SetBranchAddress("AnalysisJets_jet_selected_JET_JER_EffectiveNP_5__1down", &AnalysisJets_jet_selected_JET_JER_EffectiveNP_5__1down, &b_AnalysisJets_jet_selected_JET_JER_EffectiveNP_5__1down);
   fChain->SetBranchAddress("AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_JET_JER_EffectiveNP_5__1down", &AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_JET_JER_EffectiveNP_5__1down, &b_AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_JET_JER_EffectiveNP_5__1down);
   fChain->SetBranchAddress("METInfo_MET_JET_JER_EffectiveNP_5__1down", &METInfo_MET_JET_JER_EffectiveNP_5__1down, &b_METInfo_MET_JET_JER_EffectiveNP_5__1down);
   fChain->SetBranchAddress("METInfo_METPhi_JET_JER_EffectiveNP_5__1down", &METInfo_METPhi_JET_JER_EffectiveNP_5__1down, &b_METInfo_METPhi_JET_JER_EffectiveNP_5__1down);
   fChain->SetBranchAddress("EventInfo_jvt_effSF_JET_JER_EffectiveNP_6__1up", &EventInfo_jvt_effSF_JET_JER_EffectiveNP_6__1up, &b_EventInfo_jvt_effSF_JET_JER_EffectiveNP_6__1up);
   fChain->SetBranchAddress("AnalysisElectrons_el_selected_JET_JER_EffectiveNP_6__1up", &AnalysisElectrons_el_selected_JET_JER_EffectiveNP_6__1up, &b_AnalysisElectrons_el_selected_JET_JER_EffectiveNP_6__1up);
   fChain->SetBranchAddress("AnalysisMuons_mu_selected_JET_JER_EffectiveNP_6__1up", &AnalysisMuons_mu_selected_JET_JER_EffectiveNP_6__1up, &b_AnalysisMuons_mu_selected_JET_JER_EffectiveNP_6__1up);
   fChain->SetBranchAddress("AnalysisJets_pt_JET_JER_EffectiveNP_6__1up", &AnalysisJets_pt_JET_JER_EffectiveNP_6__1up, &b_AnalysisJets_pt_JET_JER_EffectiveNP_6__1up);
   fChain->SetBranchAddress("AnalysisJets_jet_selected_JET_JER_EffectiveNP_6__1up", &AnalysisJets_jet_selected_JET_JER_EffectiveNP_6__1up, &b_AnalysisJets_jet_selected_JET_JER_EffectiveNP_6__1up);
   fChain->SetBranchAddress("AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_JET_JER_EffectiveNP_6__1up", &AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_JET_JER_EffectiveNP_6__1up, &b_AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_JET_JER_EffectiveNP_6__1up);
   fChain->SetBranchAddress("METInfo_MET_JET_JER_EffectiveNP_6__1up", &METInfo_MET_JET_JER_EffectiveNP_6__1up, &b_METInfo_MET_JET_JER_EffectiveNP_6__1up);
   fChain->SetBranchAddress("METInfo_METPhi_JET_JER_EffectiveNP_6__1up", &METInfo_METPhi_JET_JER_EffectiveNP_6__1up, &b_METInfo_METPhi_JET_JER_EffectiveNP_6__1up);
   fChain->SetBranchAddress("EventInfo_jvt_effSF_JET_JER_EffectiveNP_6__1down", &EventInfo_jvt_effSF_JET_JER_EffectiveNP_6__1down, &b_EventInfo_jvt_effSF_JET_JER_EffectiveNP_6__1down);
   fChain->SetBranchAddress("AnalysisElectrons_el_selected_JET_JER_EffectiveNP_6__1down", &AnalysisElectrons_el_selected_JET_JER_EffectiveNP_6__1down, &b_AnalysisElectrons_el_selected_JET_JER_EffectiveNP_6__1down);
   fChain->SetBranchAddress("AnalysisMuons_mu_selected_JET_JER_EffectiveNP_6__1down", &AnalysisMuons_mu_selected_JET_JER_EffectiveNP_6__1down, &b_AnalysisMuons_mu_selected_JET_JER_EffectiveNP_6__1down);
   fChain->SetBranchAddress("AnalysisJets_pt_JET_JER_EffectiveNP_6__1down", &AnalysisJets_pt_JET_JER_EffectiveNP_6__1down, &b_AnalysisJets_pt_JET_JER_EffectiveNP_6__1down);
   fChain->SetBranchAddress("AnalysisJets_jet_selected_JET_JER_EffectiveNP_6__1down", &AnalysisJets_jet_selected_JET_JER_EffectiveNP_6__1down, &b_AnalysisJets_jet_selected_JET_JER_EffectiveNP_6__1down);
   fChain->SetBranchAddress("AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_JET_JER_EffectiveNP_6__1down", &AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_JET_JER_EffectiveNP_6__1down, &b_AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_JET_JER_EffectiveNP_6__1down);
   fChain->SetBranchAddress("METInfo_MET_JET_JER_EffectiveNP_6__1down", &METInfo_MET_JET_JER_EffectiveNP_6__1down, &b_METInfo_MET_JET_JER_EffectiveNP_6__1down);
   fChain->SetBranchAddress("METInfo_METPhi_JET_JER_EffectiveNP_6__1down", &METInfo_METPhi_JET_JER_EffectiveNP_6__1down, &b_METInfo_METPhi_JET_JER_EffectiveNP_6__1down);
   fChain->SetBranchAddress("EventInfo_jvt_effSF_JET_JER_EffectiveNP_7restTerm__1up", &EventInfo_jvt_effSF_JET_JER_EffectiveNP_7restTerm__1up, &b_EventInfo_jvt_effSF_JET_JER_EffectiveNP_7restTerm__1up);
   fChain->SetBranchAddress("AnalysisElectrons_el_selected_JET_JER_EffectiveNP_7restTerm__1up", &AnalysisElectrons_el_selected_JET_JER_EffectiveNP_7restTerm__1up, &b_AnalysisElectrons_el_selected_JET_JER_EffectiveNP_7restTerm__1up);
   fChain->SetBranchAddress("AnalysisMuons_mu_selected_JET_JER_EffectiveNP_7restTerm__1up", &AnalysisMuons_mu_selected_JET_JER_EffectiveNP_7restTerm__1up, &b_AnalysisMuons_mu_selected_JET_JER_EffectiveNP_7restTerm__1up);
   fChain->SetBranchAddress("AnalysisJets_pt_JET_JER_EffectiveNP_7restTerm__1up", &AnalysisJets_pt_JET_JER_EffectiveNP_7restTerm__1up, &b_AnalysisJets_pt_JET_JER_EffectiveNP_7restTerm__1up);
   fChain->SetBranchAddress("AnalysisJets_jet_selected_JET_JER_EffectiveNP_7restTerm__1up", &AnalysisJets_jet_selected_JET_JER_EffectiveNP_7restTerm__1up, &b_AnalysisJets_jet_selected_JET_JER_EffectiveNP_7restTerm__1up);
   fChain->SetBranchAddress("AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_JET_JER_EffectiveNP_7restTerm__1up", &AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_JET_JER_EffectiveNP_7restTerm__1up, &b_AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_JET_JER_EffectiveNP_7restTerm__1up);
   fChain->SetBranchAddress("METInfo_MET_JET_JER_EffectiveNP_7restTerm__1up", &METInfo_MET_JET_JER_EffectiveNP_7restTerm__1up, &b_METInfo_MET_JET_JER_EffectiveNP_7restTerm__1up);
   fChain->SetBranchAddress("METInfo_METPhi_JET_JER_EffectiveNP_7restTerm__1up", &METInfo_METPhi_JET_JER_EffectiveNP_7restTerm__1up, &b_METInfo_METPhi_JET_JER_EffectiveNP_7restTerm__1up);
   fChain->SetBranchAddress("EventInfo_jvt_effSF_JET_JER_EffectiveNP_7restTerm__1down", &EventInfo_jvt_effSF_JET_JER_EffectiveNP_7restTerm__1down, &b_EventInfo_jvt_effSF_JET_JER_EffectiveNP_7restTerm__1down);
   fChain->SetBranchAddress("AnalysisElectrons_el_selected_JET_JER_EffectiveNP_7restTerm__1down", &AnalysisElectrons_el_selected_JET_JER_EffectiveNP_7restTerm__1down, &b_AnalysisElectrons_el_selected_JET_JER_EffectiveNP_7restTerm__1down);
   fChain->SetBranchAddress("AnalysisMuons_mu_selected_JET_JER_EffectiveNP_7restTerm__1down", &AnalysisMuons_mu_selected_JET_JER_EffectiveNP_7restTerm__1down, &b_AnalysisMuons_mu_selected_JET_JER_EffectiveNP_7restTerm__1down);
   fChain->SetBranchAddress("AnalysisJets_pt_JET_JER_EffectiveNP_7restTerm__1down", &AnalysisJets_pt_JET_JER_EffectiveNP_7restTerm__1down, &b_AnalysisJets_pt_JET_JER_EffectiveNP_7restTerm__1down);
   fChain->SetBranchAddress("AnalysisJets_jet_selected_JET_JER_EffectiveNP_7restTerm__1down", &AnalysisJets_jet_selected_JET_JER_EffectiveNP_7restTerm__1down, &b_AnalysisJets_jet_selected_JET_JER_EffectiveNP_7restTerm__1down);
   fChain->SetBranchAddress("AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_JET_JER_EffectiveNP_7restTerm__1down", &AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_JET_JER_EffectiveNP_7restTerm__1down, &b_AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_JET_JER_EffectiveNP_7restTerm__1down);
   fChain->SetBranchAddress("METInfo_MET_JET_JER_EffectiveNP_7restTerm__1down", &METInfo_MET_JET_JER_EffectiveNP_7restTerm__1down, &b_METInfo_MET_JET_JER_EffectiveNP_7restTerm__1down);
   fChain->SetBranchAddress("METInfo_METPhi_JET_JER_EffectiveNP_7restTerm__1down", &METInfo_METPhi_JET_JER_EffectiveNP_7restTerm__1down, &b_METInfo_METPhi_JET_JER_EffectiveNP_7restTerm__1down);
   fChain->SetBranchAddress("EventInfo_jvt_effSF_JET_JvtEfficiency__1down", &EventInfo_jvt_effSF_JET_JvtEfficiency__1down, &b_EventInfo_jvt_effSF_JET_JvtEfficiency__1down);
   fChain->SetBranchAddress("EventInfo_jvt_effSF_JET_JvtEfficiency__1up", &EventInfo_jvt_effSF_JET_JvtEfficiency__1up, &b_EventInfo_jvt_effSF_JET_JvtEfficiency__1up);
   fChain->SetBranchAddress("EventInfo_jvt_effSF_JET_Pileup_OffsetMu__1up", &EventInfo_jvt_effSF_JET_Pileup_OffsetMu__1up, &b_EventInfo_jvt_effSF_JET_Pileup_OffsetMu__1up);
   fChain->SetBranchAddress("AnalysisElectrons_el_selected_JET_Pileup_OffsetMu__1up", &AnalysisElectrons_el_selected_JET_Pileup_OffsetMu__1up, &b_AnalysisElectrons_el_selected_JET_Pileup_OffsetMu__1up);
   fChain->SetBranchAddress("AnalysisMuons_mu_selected_JET_Pileup_OffsetMu__1up", &AnalysisMuons_mu_selected_JET_Pileup_OffsetMu__1up, &b_AnalysisMuons_mu_selected_JET_Pileup_OffsetMu__1up);
   fChain->SetBranchAddress("AnalysisJets_pt_JET_Pileup_OffsetMu__1up", &AnalysisJets_pt_JET_Pileup_OffsetMu__1up, &b_AnalysisJets_pt_JET_Pileup_OffsetMu__1up);
   fChain->SetBranchAddress("AnalysisJets_jet_selected_JET_Pileup_OffsetMu__1up", &AnalysisJets_jet_selected_JET_Pileup_OffsetMu__1up, &b_AnalysisJets_jet_selected_JET_Pileup_OffsetMu__1up);
   fChain->SetBranchAddress("AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_JET_Pileup_OffsetMu__1up", &AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_JET_Pileup_OffsetMu__1up, &b_AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_JET_Pileup_OffsetMu__1up);
   fChain->SetBranchAddress("METInfo_MET_JET_Pileup_OffsetMu__1up", &METInfo_MET_JET_Pileup_OffsetMu__1up, &b_METInfo_MET_JET_Pileup_OffsetMu__1up);
   fChain->SetBranchAddress("METInfo_METPhi_JET_Pileup_OffsetMu__1up", &METInfo_METPhi_JET_Pileup_OffsetMu__1up, &b_METInfo_METPhi_JET_Pileup_OffsetMu__1up);
   fChain->SetBranchAddress("EventInfo_jvt_effSF_JET_Pileup_OffsetMu__1down", &EventInfo_jvt_effSF_JET_Pileup_OffsetMu__1down, &b_EventInfo_jvt_effSF_JET_Pileup_OffsetMu__1down);
   fChain->SetBranchAddress("AnalysisElectrons_el_selected_JET_Pileup_OffsetMu__1down", &AnalysisElectrons_el_selected_JET_Pileup_OffsetMu__1down, &b_AnalysisElectrons_el_selected_JET_Pileup_OffsetMu__1down);
   fChain->SetBranchAddress("AnalysisMuons_mu_selected_JET_Pileup_OffsetMu__1down", &AnalysisMuons_mu_selected_JET_Pileup_OffsetMu__1down, &b_AnalysisMuons_mu_selected_JET_Pileup_OffsetMu__1down);
   fChain->SetBranchAddress("AnalysisJets_pt_JET_Pileup_OffsetMu__1down", &AnalysisJets_pt_JET_Pileup_OffsetMu__1down, &b_AnalysisJets_pt_JET_Pileup_OffsetMu__1down);
   fChain->SetBranchAddress("AnalysisJets_jet_selected_JET_Pileup_OffsetMu__1down", &AnalysisJets_jet_selected_JET_Pileup_OffsetMu__1down, &b_AnalysisJets_jet_selected_JET_Pileup_OffsetMu__1down);
   fChain->SetBranchAddress("AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_JET_Pileup_OffsetMu__1down", &AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_JET_Pileup_OffsetMu__1down, &b_AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_JET_Pileup_OffsetMu__1down);
   fChain->SetBranchAddress("METInfo_MET_JET_Pileup_OffsetMu__1down", &METInfo_MET_JET_Pileup_OffsetMu__1down, &b_METInfo_MET_JET_Pileup_OffsetMu__1down);
   fChain->SetBranchAddress("METInfo_METPhi_JET_Pileup_OffsetMu__1down", &METInfo_METPhi_JET_Pileup_OffsetMu__1down, &b_METInfo_METPhi_JET_Pileup_OffsetMu__1down);
   fChain->SetBranchAddress("EventInfo_jvt_effSF_JET_Pileup_OffsetNPV__1up", &EventInfo_jvt_effSF_JET_Pileup_OffsetNPV__1up, &b_EventInfo_jvt_effSF_JET_Pileup_OffsetNPV__1up);
   fChain->SetBranchAddress("AnalysisElectrons_el_selected_JET_Pileup_OffsetNPV__1up", &AnalysisElectrons_el_selected_JET_Pileup_OffsetNPV__1up, &b_AnalysisElectrons_el_selected_JET_Pileup_OffsetNPV__1up);
   fChain->SetBranchAddress("AnalysisMuons_mu_selected_JET_Pileup_OffsetNPV__1up", &AnalysisMuons_mu_selected_JET_Pileup_OffsetNPV__1up, &b_AnalysisMuons_mu_selected_JET_Pileup_OffsetNPV__1up);
   fChain->SetBranchAddress("AnalysisJets_pt_JET_Pileup_OffsetNPV__1up", &AnalysisJets_pt_JET_Pileup_OffsetNPV__1up, &b_AnalysisJets_pt_JET_Pileup_OffsetNPV__1up);
   fChain->SetBranchAddress("AnalysisJets_jet_selected_JET_Pileup_OffsetNPV__1up", &AnalysisJets_jet_selected_JET_Pileup_OffsetNPV__1up, &b_AnalysisJets_jet_selected_JET_Pileup_OffsetNPV__1up);
   fChain->SetBranchAddress("AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_JET_Pileup_OffsetNPV__1up", &AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_JET_Pileup_OffsetNPV__1up, &b_AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_JET_Pileup_OffsetNPV__1up);
   fChain->SetBranchAddress("METInfo_MET_JET_Pileup_OffsetNPV__1up", &METInfo_MET_JET_Pileup_OffsetNPV__1up, &b_METInfo_MET_JET_Pileup_OffsetNPV__1up);
   fChain->SetBranchAddress("METInfo_METPhi_JET_Pileup_OffsetNPV__1up", &METInfo_METPhi_JET_Pileup_OffsetNPV__1up, &b_METInfo_METPhi_JET_Pileup_OffsetNPV__1up);
   fChain->SetBranchAddress("EventInfo_jvt_effSF_JET_Pileup_OffsetNPV__1down", &EventInfo_jvt_effSF_JET_Pileup_OffsetNPV__1down, &b_EventInfo_jvt_effSF_JET_Pileup_OffsetNPV__1down);
   fChain->SetBranchAddress("AnalysisElectrons_el_selected_JET_Pileup_OffsetNPV__1down", &AnalysisElectrons_el_selected_JET_Pileup_OffsetNPV__1down, &b_AnalysisElectrons_el_selected_JET_Pileup_OffsetNPV__1down);
   fChain->SetBranchAddress("AnalysisMuons_mu_selected_JET_Pileup_OffsetNPV__1down", &AnalysisMuons_mu_selected_JET_Pileup_OffsetNPV__1down, &b_AnalysisMuons_mu_selected_JET_Pileup_OffsetNPV__1down);
   fChain->SetBranchAddress("AnalysisJets_pt_JET_Pileup_OffsetNPV__1down", &AnalysisJets_pt_JET_Pileup_OffsetNPV__1down, &b_AnalysisJets_pt_JET_Pileup_OffsetNPV__1down);
   fChain->SetBranchAddress("AnalysisJets_jet_selected_JET_Pileup_OffsetNPV__1down", &AnalysisJets_jet_selected_JET_Pileup_OffsetNPV__1down, &b_AnalysisJets_jet_selected_JET_Pileup_OffsetNPV__1down);
   fChain->SetBranchAddress("AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_JET_Pileup_OffsetNPV__1down", &AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_JET_Pileup_OffsetNPV__1down, &b_AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_JET_Pileup_OffsetNPV__1down);
   fChain->SetBranchAddress("METInfo_MET_JET_Pileup_OffsetNPV__1down", &METInfo_MET_JET_Pileup_OffsetNPV__1down, &b_METInfo_MET_JET_Pileup_OffsetNPV__1down);
   fChain->SetBranchAddress("METInfo_METPhi_JET_Pileup_OffsetNPV__1down", &METInfo_METPhi_JET_Pileup_OffsetNPV__1down, &b_METInfo_METPhi_JET_Pileup_OffsetNPV__1down);
   fChain->SetBranchAddress("EventInfo_jvt_effSF_JET_Pileup_PtTerm__1up", &EventInfo_jvt_effSF_JET_Pileup_PtTerm__1up, &b_EventInfo_jvt_effSF_JET_Pileup_PtTerm__1up);
   fChain->SetBranchAddress("AnalysisElectrons_el_selected_JET_Pileup_PtTerm__1up", &AnalysisElectrons_el_selected_JET_Pileup_PtTerm__1up, &b_AnalysisElectrons_el_selected_JET_Pileup_PtTerm__1up);
   fChain->SetBranchAddress("AnalysisMuons_mu_selected_JET_Pileup_PtTerm__1up", &AnalysisMuons_mu_selected_JET_Pileup_PtTerm__1up, &b_AnalysisMuons_mu_selected_JET_Pileup_PtTerm__1up);
   fChain->SetBranchAddress("AnalysisJets_pt_JET_Pileup_PtTerm__1up", &AnalysisJets_pt_JET_Pileup_PtTerm__1up, &b_AnalysisJets_pt_JET_Pileup_PtTerm__1up);
   fChain->SetBranchAddress("AnalysisJets_jet_selected_JET_Pileup_PtTerm__1up", &AnalysisJets_jet_selected_JET_Pileup_PtTerm__1up, &b_AnalysisJets_jet_selected_JET_Pileup_PtTerm__1up);
   fChain->SetBranchAddress("AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_JET_Pileup_PtTerm__1up", &AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_JET_Pileup_PtTerm__1up, &b_AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_JET_Pileup_PtTerm__1up);
   fChain->SetBranchAddress("METInfo_MET_JET_Pileup_PtTerm__1up", &METInfo_MET_JET_Pileup_PtTerm__1up, &b_METInfo_MET_JET_Pileup_PtTerm__1up);
   fChain->SetBranchAddress("METInfo_METPhi_JET_Pileup_PtTerm__1up", &METInfo_METPhi_JET_Pileup_PtTerm__1up, &b_METInfo_METPhi_JET_Pileup_PtTerm__1up);
   fChain->SetBranchAddress("EventInfo_jvt_effSF_JET_Pileup_PtTerm__1down", &EventInfo_jvt_effSF_JET_Pileup_PtTerm__1down, &b_EventInfo_jvt_effSF_JET_Pileup_PtTerm__1down);
   fChain->SetBranchAddress("AnalysisElectrons_el_selected_JET_Pileup_PtTerm__1down", &AnalysisElectrons_el_selected_JET_Pileup_PtTerm__1down, &b_AnalysisElectrons_el_selected_JET_Pileup_PtTerm__1down);
   fChain->SetBranchAddress("AnalysisMuons_mu_selected_JET_Pileup_PtTerm__1down", &AnalysisMuons_mu_selected_JET_Pileup_PtTerm__1down, &b_AnalysisMuons_mu_selected_JET_Pileup_PtTerm__1down);
   fChain->SetBranchAddress("AnalysisJets_pt_JET_Pileup_PtTerm__1down", &AnalysisJets_pt_JET_Pileup_PtTerm__1down, &b_AnalysisJets_pt_JET_Pileup_PtTerm__1down);
   fChain->SetBranchAddress("AnalysisJets_jet_selected_JET_Pileup_PtTerm__1down", &AnalysisJets_jet_selected_JET_Pileup_PtTerm__1down, &b_AnalysisJets_jet_selected_JET_Pileup_PtTerm__1down);
   fChain->SetBranchAddress("AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_JET_Pileup_PtTerm__1down", &AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_JET_Pileup_PtTerm__1down, &b_AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_JET_Pileup_PtTerm__1down);
   fChain->SetBranchAddress("METInfo_MET_JET_Pileup_PtTerm__1down", &METInfo_MET_JET_Pileup_PtTerm__1down, &b_METInfo_MET_JET_Pileup_PtTerm__1down);
   fChain->SetBranchAddress("METInfo_METPhi_JET_Pileup_PtTerm__1down", &METInfo_METPhi_JET_Pileup_PtTerm__1down, &b_METInfo_METPhi_JET_Pileup_PtTerm__1down);
   fChain->SetBranchAddress("EventInfo_jvt_effSF_JET_Pileup_RhoTopology__1up", &EventInfo_jvt_effSF_JET_Pileup_RhoTopology__1up, &b_EventInfo_jvt_effSF_JET_Pileup_RhoTopology__1up);
   fChain->SetBranchAddress("AnalysisElectrons_el_selected_JET_Pileup_RhoTopology__1up", &AnalysisElectrons_el_selected_JET_Pileup_RhoTopology__1up, &b_AnalysisElectrons_el_selected_JET_Pileup_RhoTopology__1up);
   fChain->SetBranchAddress("AnalysisMuons_mu_selected_JET_Pileup_RhoTopology__1up", &AnalysisMuons_mu_selected_JET_Pileup_RhoTopology__1up, &b_AnalysisMuons_mu_selected_JET_Pileup_RhoTopology__1up);
   fChain->SetBranchAddress("AnalysisJets_pt_JET_Pileup_RhoTopology__1up", &AnalysisJets_pt_JET_Pileup_RhoTopology__1up, &b_AnalysisJets_pt_JET_Pileup_RhoTopology__1up);
   fChain->SetBranchAddress("AnalysisJets_jet_selected_JET_Pileup_RhoTopology__1up", &AnalysisJets_jet_selected_JET_Pileup_RhoTopology__1up, &b_AnalysisJets_jet_selected_JET_Pileup_RhoTopology__1up);
   fChain->SetBranchAddress("AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_JET_Pileup_RhoTopology__1up", &AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_JET_Pileup_RhoTopology__1up, &b_AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_JET_Pileup_RhoTopology__1up);
   fChain->SetBranchAddress("METInfo_MET_JET_Pileup_RhoTopology__1up", &METInfo_MET_JET_Pileup_RhoTopology__1up, &b_METInfo_MET_JET_Pileup_RhoTopology__1up);
   fChain->SetBranchAddress("METInfo_METPhi_JET_Pileup_RhoTopology__1up", &METInfo_METPhi_JET_Pileup_RhoTopology__1up, &b_METInfo_METPhi_JET_Pileup_RhoTopology__1up);
   fChain->SetBranchAddress("EventInfo_jvt_effSF_JET_Pileup_RhoTopology__1down", &EventInfo_jvt_effSF_JET_Pileup_RhoTopology__1down, &b_EventInfo_jvt_effSF_JET_Pileup_RhoTopology__1down);
   fChain->SetBranchAddress("AnalysisElectrons_el_selected_JET_Pileup_RhoTopology__1down", &AnalysisElectrons_el_selected_JET_Pileup_RhoTopology__1down, &b_AnalysisElectrons_el_selected_JET_Pileup_RhoTopology__1down);
   fChain->SetBranchAddress("AnalysisMuons_mu_selected_JET_Pileup_RhoTopology__1down", &AnalysisMuons_mu_selected_JET_Pileup_RhoTopology__1down, &b_AnalysisMuons_mu_selected_JET_Pileup_RhoTopology__1down);
   fChain->SetBranchAddress("AnalysisJets_pt_JET_Pileup_RhoTopology__1down", &AnalysisJets_pt_JET_Pileup_RhoTopology__1down, &b_AnalysisJets_pt_JET_Pileup_RhoTopology__1down);
   fChain->SetBranchAddress("AnalysisJets_jet_selected_JET_Pileup_RhoTopology__1down", &AnalysisJets_jet_selected_JET_Pileup_RhoTopology__1down, &b_AnalysisJets_jet_selected_JET_Pileup_RhoTopology__1down);
   fChain->SetBranchAddress("AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_JET_Pileup_RhoTopology__1down", &AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_JET_Pileup_RhoTopology__1down, &b_AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_JET_Pileup_RhoTopology__1down);
   fChain->SetBranchAddress("METInfo_MET_JET_Pileup_RhoTopology__1down", &METInfo_MET_JET_Pileup_RhoTopology__1down, &b_METInfo_MET_JET_Pileup_RhoTopology__1down);
   fChain->SetBranchAddress("METInfo_METPhi_JET_Pileup_RhoTopology__1down", &METInfo_METPhi_JET_Pileup_RhoTopology__1down, &b_METInfo_METPhi_JET_Pileup_RhoTopology__1down);
   fChain->SetBranchAddress("EventInfo_jvt_effSF_JET_PunchThrough_MC16__1up", &EventInfo_jvt_effSF_JET_PunchThrough_MC16__1up, &b_EventInfo_jvt_effSF_JET_PunchThrough_MC16__1up);
   fChain->SetBranchAddress("AnalysisElectrons_el_selected_JET_PunchThrough_MC16__1up", &AnalysisElectrons_el_selected_JET_PunchThrough_MC16__1up, &b_AnalysisElectrons_el_selected_JET_PunchThrough_MC16__1up);
   fChain->SetBranchAddress("AnalysisMuons_mu_selected_JET_PunchThrough_MC16__1up", &AnalysisMuons_mu_selected_JET_PunchThrough_MC16__1up, &b_AnalysisMuons_mu_selected_JET_PunchThrough_MC16__1up);
   fChain->SetBranchAddress("AnalysisJets_pt_JET_PunchThrough_MC16__1up", &AnalysisJets_pt_JET_PunchThrough_MC16__1up, &b_AnalysisJets_pt_JET_PunchThrough_MC16__1up);
   fChain->SetBranchAddress("AnalysisJets_jet_selected_JET_PunchThrough_MC16__1up", &AnalysisJets_jet_selected_JET_PunchThrough_MC16__1up, &b_AnalysisJets_jet_selected_JET_PunchThrough_MC16__1up);
   fChain->SetBranchAddress("AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_JET_PunchThrough_MC16__1up", &AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_JET_PunchThrough_MC16__1up, &b_AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_JET_PunchThrough_MC16__1up);
   fChain->SetBranchAddress("METInfo_MET_JET_PunchThrough_MC16__1up", &METInfo_MET_JET_PunchThrough_MC16__1up, &b_METInfo_MET_JET_PunchThrough_MC16__1up);
   fChain->SetBranchAddress("METInfo_METPhi_JET_PunchThrough_MC16__1up", &METInfo_METPhi_JET_PunchThrough_MC16__1up, &b_METInfo_METPhi_JET_PunchThrough_MC16__1up);
   fChain->SetBranchAddress("EventInfo_jvt_effSF_JET_PunchThrough_MC16__1down", &EventInfo_jvt_effSF_JET_PunchThrough_MC16__1down, &b_EventInfo_jvt_effSF_JET_PunchThrough_MC16__1down);
   fChain->SetBranchAddress("AnalysisElectrons_el_selected_JET_PunchThrough_MC16__1down", &AnalysisElectrons_el_selected_JET_PunchThrough_MC16__1down, &b_AnalysisElectrons_el_selected_JET_PunchThrough_MC16__1down);
   fChain->SetBranchAddress("AnalysisMuons_mu_selected_JET_PunchThrough_MC16__1down", &AnalysisMuons_mu_selected_JET_PunchThrough_MC16__1down, &b_AnalysisMuons_mu_selected_JET_PunchThrough_MC16__1down);
   fChain->SetBranchAddress("AnalysisJets_pt_JET_PunchThrough_MC16__1down", &AnalysisJets_pt_JET_PunchThrough_MC16__1down, &b_AnalysisJets_pt_JET_PunchThrough_MC16__1down);
   fChain->SetBranchAddress("AnalysisJets_jet_selected_JET_PunchThrough_MC16__1down", &AnalysisJets_jet_selected_JET_PunchThrough_MC16__1down, &b_AnalysisJets_jet_selected_JET_PunchThrough_MC16__1down);
   fChain->SetBranchAddress("AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_JET_PunchThrough_MC16__1down", &AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_JET_PunchThrough_MC16__1down, &b_AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_JET_PunchThrough_MC16__1down);
   fChain->SetBranchAddress("METInfo_MET_JET_PunchThrough_MC16__1down", &METInfo_MET_JET_PunchThrough_MC16__1down, &b_METInfo_MET_JET_PunchThrough_MC16__1down);
   fChain->SetBranchAddress("METInfo_METPhi_JET_PunchThrough_MC16__1down", &METInfo_METPhi_JET_PunchThrough_MC16__1down, &b_METInfo_METPhi_JET_PunchThrough_MC16__1down);
   fChain->SetBranchAddress("METInfo_MET_MET_SoftTrk_ResoPara", &METInfo_MET_MET_SoftTrk_ResoPara, &b_METInfo_MET_MET_SoftTrk_ResoPara);
   fChain->SetBranchAddress("METInfo_METPhi_MET_SoftTrk_ResoPara", &METInfo_METPhi_MET_SoftTrk_ResoPara, &b_METInfo_METPhi_MET_SoftTrk_ResoPara);
   fChain->SetBranchAddress("METInfo_MET_MET_SoftTrk_ResoPerp", &METInfo_MET_MET_SoftTrk_ResoPerp, &b_METInfo_MET_MET_SoftTrk_ResoPerp);
   fChain->SetBranchAddress("METInfo_METPhi_MET_SoftTrk_ResoPerp", &METInfo_METPhi_MET_SoftTrk_ResoPerp, &b_METInfo_METPhi_MET_SoftTrk_ResoPerp);
   fChain->SetBranchAddress("METInfo_MET_MET_SoftTrk_Scale__1down", &METInfo_MET_MET_SoftTrk_Scale__1down, &b_METInfo_MET_MET_SoftTrk_Scale__1down);
   fChain->SetBranchAddress("METInfo_METPhi_MET_SoftTrk_Scale__1down", &METInfo_METPhi_MET_SoftTrk_Scale__1down, &b_METInfo_METPhi_MET_SoftTrk_Scale__1down);
   fChain->SetBranchAddress("METInfo_MET_MET_SoftTrk_Scale__1up", &METInfo_MET_MET_SoftTrk_Scale__1up, &b_METInfo_MET_MET_SoftTrk_Scale__1up);
   fChain->SetBranchAddress("METInfo_METPhi_MET_SoftTrk_Scale__1up", &METInfo_METPhi_MET_SoftTrk_Scale__1up, &b_METInfo_METPhi_MET_SoftTrk_Scale__1up);
   fChain->SetBranchAddress("AnalysisElectrons_el_selected_MUON_CB__1down", &AnalysisElectrons_el_selected_MUON_CB__1down, &b_AnalysisElectrons_el_selected_MUON_CB__1down);
   fChain->SetBranchAddress("AnalysisMuons_pt_MUON_CB__1down", &AnalysisMuons_pt_MUON_CB__1down, &b_AnalysisMuons_pt_MUON_CB__1down);
   fChain->SetBranchAddress("AnalysisMuons_mu_selected_MUON_CB__1down", &AnalysisMuons_mu_selected_MUON_CB__1down, &b_AnalysisMuons_mu_selected_MUON_CB__1down);
   fChain->SetBranchAddress("AnalysisMuons_isQuality_Loose_MUON_CB__1down", &AnalysisMuons_isQuality_Loose_MUON_CB__1down, &b_AnalysisMuons_isQuality_Loose_MUON_CB__1down);
   fChain->SetBranchAddress("AnalysisMuons_isQuality_Medium_MUON_CB__1down", &AnalysisMuons_isQuality_Medium_MUON_CB__1down, &b_AnalysisMuons_isQuality_Medium_MUON_CB__1down);
   fChain->SetBranchAddress("AnalysisMuons_isQuality_Tight_MUON_CB__1down", &AnalysisMuons_isQuality_Tight_MUON_CB__1down, &b_AnalysisMuons_isQuality_Tight_MUON_CB__1down);
   fChain->SetBranchAddress("AnalysisMuons_isIsolated_PflowTight_VarRad_MUON_CB__1down", &AnalysisMuons_isIsolated_PflowTight_VarRad_MUON_CB__1down, &b_AnalysisMuons_isIsolated_PflowTight_VarRad_MUON_CB__1down);
   fChain->SetBranchAddress("AnalysisMuons_muon_effSF_MUON_CB__1down", &AnalysisMuons_muon_effSF_MUON_CB__1down, &b_AnalysisMuons_muon_effSF_MUON_CB__1down);
   fChain->SetBranchAddress("AnalysisMuons_muon_effSF_TTVA_MUON_CB__1down", &AnalysisMuons_muon_effSF_TTVA_MUON_CB__1down, &b_AnalysisMuons_muon_effSF_TTVA_MUON_CB__1down);
   fChain->SetBranchAddress("AnalysisMuons_muon_effSF_Quality_Loose_MUON_CB__1down", &AnalysisMuons_muon_effSF_Quality_Loose_MUON_CB__1down, &b_AnalysisMuons_muon_effSF_Quality_Loose_MUON_CB__1down);
   fChain->SetBranchAddress("AnalysisMuons_muon_effSF_Quality_Medium_MUON_CB__1down", &AnalysisMuons_muon_effSF_Quality_Medium_MUON_CB__1down, &b_AnalysisMuons_muon_effSF_Quality_Medium_MUON_CB__1down);
   fChain->SetBranchAddress("AnalysisMuons_muon_effSF_Quality_Tight_MUON_CB__1down", &AnalysisMuons_muon_effSF_Quality_Tight_MUON_CB__1down, &b_AnalysisMuons_muon_effSF_Quality_Tight_MUON_CB__1down);
   fChain->SetBranchAddress("AnalysisMuons_muon_effSF_Isol_PflowTight_VarRad_MUON_CB__1down", &AnalysisMuons_muon_effSF_Isol_PflowTight_VarRad_MUON_CB__1down, &b_AnalysisMuons_muon_effSF_Isol_PflowTight_VarRad_MUON_CB__1down);
   fChain->SetBranchAddress("AnalysisMuons_muon_effMC_Trig_Loose_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_CB__1down", &AnalysisMuons_muon_effMC_Trig_Loose_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_CB__1down, &b_AnalysisMuons_muon_effMC_Trig_Loose_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_CB__1down);
   fChain->SetBranchAddress("AnalysisMuons_muon_effData_Trig_Loose_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_CB__1down", &AnalysisMuons_muon_effData_Trig_Loose_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_CB__1down, &b_AnalysisMuons_muon_effData_Trig_Loose_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_CB__1down);
   fChain->SetBranchAddress("AnalysisMuons_muon_effMC_Trig_Loose_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_CB__1down", &AnalysisMuons_muon_effMC_Trig_Loose_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_CB__1down, &b_AnalysisMuons_muon_effMC_Trig_Loose_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_CB__1down);
   fChain->SetBranchAddress("AnalysisMuons_muon_effData_Trig_Loose_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_CB__1down", &AnalysisMuons_muon_effData_Trig_Loose_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_CB__1down, &b_AnalysisMuons_muon_effData_Trig_Loose_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_CB__1down);
   fChain->SetBranchAddress("AnalysisMuons_muon_effMC_Trig_Medium_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_CB__1down", &AnalysisMuons_muon_effMC_Trig_Medium_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_CB__1down, &b_AnalysisMuons_muon_effMC_Trig_Medium_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_CB__1down);
   fChain->SetBranchAddress("AnalysisMuons_muon_effData_Trig_Medium_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_CB__1down", &AnalysisMuons_muon_effData_Trig_Medium_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_CB__1down, &b_AnalysisMuons_muon_effData_Trig_Medium_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_CB__1down);
   fChain->SetBranchAddress("AnalysisMuons_muon_effMC_Trig_Medium_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_CB__1down", &AnalysisMuons_muon_effMC_Trig_Medium_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_CB__1down, &b_AnalysisMuons_muon_effMC_Trig_Medium_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_CB__1down);
   fChain->SetBranchAddress("AnalysisMuons_muon_effData_Trig_Medium_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_CB__1down", &AnalysisMuons_muon_effData_Trig_Medium_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_CB__1down, &b_AnalysisMuons_muon_effData_Trig_Medium_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_CB__1down);
   fChain->SetBranchAddress("AnalysisMuons_muon_effMC_Trig_Tight_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_CB__1down", &AnalysisMuons_muon_effMC_Trig_Tight_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_CB__1down, &b_AnalysisMuons_muon_effMC_Trig_Tight_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_CB__1down);
   fChain->SetBranchAddress("AnalysisMuons_muon_effData_Trig_Tight_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_CB__1down", &AnalysisMuons_muon_effData_Trig_Tight_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_CB__1down, &b_AnalysisMuons_muon_effData_Trig_Tight_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_CB__1down);
   fChain->SetBranchAddress("AnalysisMuons_muon_effMC_Trig_Tight_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_CB__1down", &AnalysisMuons_muon_effMC_Trig_Tight_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_CB__1down, &b_AnalysisMuons_muon_effMC_Trig_Tight_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_CB__1down);
   fChain->SetBranchAddress("AnalysisMuons_muon_effData_Trig_Tight_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_CB__1down", &AnalysisMuons_muon_effData_Trig_Tight_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_CB__1down, &b_AnalysisMuons_muon_effData_Trig_Tight_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_CB__1down);
   fChain->SetBranchAddress("AnalysisJets_jet_selected_MUON_CB__1down", &AnalysisJets_jet_selected_MUON_CB__1down, &b_AnalysisJets_jet_selected_MUON_CB__1down);
   fChain->SetBranchAddress("METInfo_MET_MUON_CB__1down", &METInfo_MET_MUON_CB__1down, &b_METInfo_MET_MUON_CB__1down);
   fChain->SetBranchAddress("METInfo_METPhi_MUON_CB__1down", &METInfo_METPhi_MUON_CB__1down, &b_METInfo_METPhi_MUON_CB__1down);
   fChain->SetBranchAddress("AnalysisElectrons_el_selected_MUON_CB__1up", &AnalysisElectrons_el_selected_MUON_CB__1up, &b_AnalysisElectrons_el_selected_MUON_CB__1up);
   fChain->SetBranchAddress("AnalysisMuons_pt_MUON_CB__1up", &AnalysisMuons_pt_MUON_CB__1up, &b_AnalysisMuons_pt_MUON_CB__1up);
   fChain->SetBranchAddress("AnalysisMuons_mu_selected_MUON_CB__1up", &AnalysisMuons_mu_selected_MUON_CB__1up, &b_AnalysisMuons_mu_selected_MUON_CB__1up);
   fChain->SetBranchAddress("AnalysisMuons_isQuality_Loose_MUON_CB__1up", &AnalysisMuons_isQuality_Loose_MUON_CB__1up, &b_AnalysisMuons_isQuality_Loose_MUON_CB__1up);
   fChain->SetBranchAddress("AnalysisMuons_isQuality_Medium_MUON_CB__1up", &AnalysisMuons_isQuality_Medium_MUON_CB__1up, &b_AnalysisMuons_isQuality_Medium_MUON_CB__1up);
   fChain->SetBranchAddress("AnalysisMuons_isQuality_Tight_MUON_CB__1up", &AnalysisMuons_isQuality_Tight_MUON_CB__1up, &b_AnalysisMuons_isQuality_Tight_MUON_CB__1up);
   fChain->SetBranchAddress("AnalysisMuons_isIsolated_PflowTight_VarRad_MUON_CB__1up", &AnalysisMuons_isIsolated_PflowTight_VarRad_MUON_CB__1up, &b_AnalysisMuons_isIsolated_PflowTight_VarRad_MUON_CB__1up);
   fChain->SetBranchAddress("AnalysisMuons_muon_effSF_MUON_CB__1up", &AnalysisMuons_muon_effSF_MUON_CB__1up, &b_AnalysisMuons_muon_effSF_MUON_CB__1up);
   fChain->SetBranchAddress("AnalysisMuons_muon_effSF_TTVA_MUON_CB__1up", &AnalysisMuons_muon_effSF_TTVA_MUON_CB__1up, &b_AnalysisMuons_muon_effSF_TTVA_MUON_CB__1up);
   fChain->SetBranchAddress("AnalysisMuons_muon_effSF_Quality_Loose_MUON_CB__1up", &AnalysisMuons_muon_effSF_Quality_Loose_MUON_CB__1up, &b_AnalysisMuons_muon_effSF_Quality_Loose_MUON_CB__1up);
   fChain->SetBranchAddress("AnalysisMuons_muon_effSF_Quality_Medium_MUON_CB__1up", &AnalysisMuons_muon_effSF_Quality_Medium_MUON_CB__1up, &b_AnalysisMuons_muon_effSF_Quality_Medium_MUON_CB__1up);
   fChain->SetBranchAddress("AnalysisMuons_muon_effSF_Quality_Tight_MUON_CB__1up", &AnalysisMuons_muon_effSF_Quality_Tight_MUON_CB__1up, &b_AnalysisMuons_muon_effSF_Quality_Tight_MUON_CB__1up);
   fChain->SetBranchAddress("AnalysisMuons_muon_effSF_Isol_PflowTight_VarRad_MUON_CB__1up", &AnalysisMuons_muon_effSF_Isol_PflowTight_VarRad_MUON_CB__1up, &b_AnalysisMuons_muon_effSF_Isol_PflowTight_VarRad_MUON_CB__1up);
   fChain->SetBranchAddress("AnalysisMuons_muon_effMC_Trig_Loose_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_CB__1up", &AnalysisMuons_muon_effMC_Trig_Loose_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_CB__1up, &b_AnalysisMuons_muon_effMC_Trig_Loose_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_CB__1up);
   fChain->SetBranchAddress("AnalysisMuons_muon_effData_Trig_Loose_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_CB__1up", &AnalysisMuons_muon_effData_Trig_Loose_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_CB__1up, &b_AnalysisMuons_muon_effData_Trig_Loose_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_CB__1up);
   fChain->SetBranchAddress("AnalysisMuons_muon_effMC_Trig_Loose_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_CB__1up", &AnalysisMuons_muon_effMC_Trig_Loose_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_CB__1up, &b_AnalysisMuons_muon_effMC_Trig_Loose_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_CB__1up);
   fChain->SetBranchAddress("AnalysisMuons_muon_effData_Trig_Loose_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_CB__1up", &AnalysisMuons_muon_effData_Trig_Loose_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_CB__1up, &b_AnalysisMuons_muon_effData_Trig_Loose_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_CB__1up);
   fChain->SetBranchAddress("AnalysisMuons_muon_effMC_Trig_Medium_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_CB__1up", &AnalysisMuons_muon_effMC_Trig_Medium_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_CB__1up, &b_AnalysisMuons_muon_effMC_Trig_Medium_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_CB__1up);
   fChain->SetBranchAddress("AnalysisMuons_muon_effData_Trig_Medium_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_CB__1up", &AnalysisMuons_muon_effData_Trig_Medium_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_CB__1up, &b_AnalysisMuons_muon_effData_Trig_Medium_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_CB__1up);
   fChain->SetBranchAddress("AnalysisMuons_muon_effMC_Trig_Medium_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_CB__1up", &AnalysisMuons_muon_effMC_Trig_Medium_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_CB__1up, &b_AnalysisMuons_muon_effMC_Trig_Medium_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_CB__1up);
   fChain->SetBranchAddress("AnalysisMuons_muon_effData_Trig_Medium_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_CB__1up", &AnalysisMuons_muon_effData_Trig_Medium_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_CB__1up, &b_AnalysisMuons_muon_effData_Trig_Medium_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_CB__1up);
   fChain->SetBranchAddress("AnalysisMuons_muon_effMC_Trig_Tight_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_CB__1up", &AnalysisMuons_muon_effMC_Trig_Tight_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_CB__1up, &b_AnalysisMuons_muon_effMC_Trig_Tight_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_CB__1up);
   fChain->SetBranchAddress("AnalysisMuons_muon_effData_Trig_Tight_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_CB__1up", &AnalysisMuons_muon_effData_Trig_Tight_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_CB__1up, &b_AnalysisMuons_muon_effData_Trig_Tight_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_CB__1up);
   fChain->SetBranchAddress("AnalysisMuons_muon_effMC_Trig_Tight_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_CB__1up", &AnalysisMuons_muon_effMC_Trig_Tight_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_CB__1up, &b_AnalysisMuons_muon_effMC_Trig_Tight_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_CB__1up);
   fChain->SetBranchAddress("AnalysisMuons_muon_effData_Trig_Tight_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_CB__1up", &AnalysisMuons_muon_effData_Trig_Tight_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_CB__1up, &b_AnalysisMuons_muon_effData_Trig_Tight_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_CB__1up);
   fChain->SetBranchAddress("AnalysisJets_jet_selected_MUON_CB__1up", &AnalysisJets_jet_selected_MUON_CB__1up, &b_AnalysisJets_jet_selected_MUON_CB__1up);
   fChain->SetBranchAddress("METInfo_MET_MUON_CB__1up", &METInfo_MET_MUON_CB__1up, &b_METInfo_MET_MUON_CB__1up);
   fChain->SetBranchAddress("METInfo_METPhi_MUON_CB__1up", &METInfo_METPhi_MUON_CB__1up, &b_METInfo_METPhi_MUON_CB__1up);
   fChain->SetBranchAddress("AnalysisMuons_muon_effSF_Isol_PflowTight_VarRad_MUON_EFF_ISO_STAT__1down", &AnalysisMuons_muon_effSF_Isol_PflowTight_VarRad_MUON_EFF_ISO_STAT__1down, &b_AnalysisMuons_muon_effSF_Isol_PflowTight_VarRad_MUON_EFF_ISO_STAT__1down);
   fChain->SetBranchAddress("AnalysisMuons_muon_effSF_Isol_PflowTight_VarRad_MUON_EFF_ISO_STAT__1up", &AnalysisMuons_muon_effSF_Isol_PflowTight_VarRad_MUON_EFF_ISO_STAT__1up, &b_AnalysisMuons_muon_effSF_Isol_PflowTight_VarRad_MUON_EFF_ISO_STAT__1up);
   fChain->SetBranchAddress("AnalysisMuons_muon_effSF_Isol_PflowTight_VarRad_MUON_EFF_ISO_SYS__1down", &AnalysisMuons_muon_effSF_Isol_PflowTight_VarRad_MUON_EFF_ISO_SYS__1down, &b_AnalysisMuons_muon_effSF_Isol_PflowTight_VarRad_MUON_EFF_ISO_SYS__1down);
   fChain->SetBranchAddress("AnalysisMuons_muon_effSF_Isol_PflowTight_VarRad_MUON_EFF_ISO_SYS__1up", &AnalysisMuons_muon_effSF_Isol_PflowTight_VarRad_MUON_EFF_ISO_SYS__1up, &b_AnalysisMuons_muon_effSF_Isol_PflowTight_VarRad_MUON_EFF_ISO_SYS__1up);
   fChain->SetBranchAddress("AnalysisMuons_muon_effSF_MUON_EFF_RECO_STAT__1down", &AnalysisMuons_muon_effSF_MUON_EFF_RECO_STAT__1down, &b_AnalysisMuons_muon_effSF_MUON_EFF_RECO_STAT__1down);
   fChain->SetBranchAddress("AnalysisMuons_muon_effSF_Quality_Loose_MUON_EFF_RECO_STAT__1down", &AnalysisMuons_muon_effSF_Quality_Loose_MUON_EFF_RECO_STAT__1down, &b_AnalysisMuons_muon_effSF_Quality_Loose_MUON_EFF_RECO_STAT__1down);
   fChain->SetBranchAddress("AnalysisMuons_muon_effSF_Quality_Medium_MUON_EFF_RECO_STAT__1down", &AnalysisMuons_muon_effSF_Quality_Medium_MUON_EFF_RECO_STAT__1down, &b_AnalysisMuons_muon_effSF_Quality_Medium_MUON_EFF_RECO_STAT__1down);
   fChain->SetBranchAddress("AnalysisMuons_muon_effSF_Quality_Tight_MUON_EFF_RECO_STAT__1down", &AnalysisMuons_muon_effSF_Quality_Tight_MUON_EFF_RECO_STAT__1down, &b_AnalysisMuons_muon_effSF_Quality_Tight_MUON_EFF_RECO_STAT__1down);
   fChain->SetBranchAddress("AnalysisMuons_muon_effSF_MUON_EFF_RECO_STAT__1up", &AnalysisMuons_muon_effSF_MUON_EFF_RECO_STAT__1up, &b_AnalysisMuons_muon_effSF_MUON_EFF_RECO_STAT__1up);
   fChain->SetBranchAddress("AnalysisMuons_muon_effSF_Quality_Loose_MUON_EFF_RECO_STAT__1up", &AnalysisMuons_muon_effSF_Quality_Loose_MUON_EFF_RECO_STAT__1up, &b_AnalysisMuons_muon_effSF_Quality_Loose_MUON_EFF_RECO_STAT__1up);
   fChain->SetBranchAddress("AnalysisMuons_muon_effSF_Quality_Medium_MUON_EFF_RECO_STAT__1up", &AnalysisMuons_muon_effSF_Quality_Medium_MUON_EFF_RECO_STAT__1up, &b_AnalysisMuons_muon_effSF_Quality_Medium_MUON_EFF_RECO_STAT__1up);
   fChain->SetBranchAddress("AnalysisMuons_muon_effSF_Quality_Tight_MUON_EFF_RECO_STAT__1up", &AnalysisMuons_muon_effSF_Quality_Tight_MUON_EFF_RECO_STAT__1up, &b_AnalysisMuons_muon_effSF_Quality_Tight_MUON_EFF_RECO_STAT__1up);
   fChain->SetBranchAddress("AnalysisMuons_muon_effSF_MUON_EFF_RECO_STAT_LOWPT__1down", &AnalysisMuons_muon_effSF_MUON_EFF_RECO_STAT_LOWPT__1down, &b_AnalysisMuons_muon_effSF_MUON_EFF_RECO_STAT_LOWPT__1down);
   fChain->SetBranchAddress("AnalysisMuons_muon_effSF_Quality_Loose_MUON_EFF_RECO_STAT_LOWPT__1down", &AnalysisMuons_muon_effSF_Quality_Loose_MUON_EFF_RECO_STAT_LOWPT__1down, &b_AnalysisMuons_muon_effSF_Quality_Loose_MUON_EFF_RECO_STAT_LOWPT__1down);
   fChain->SetBranchAddress("AnalysisMuons_muon_effSF_Quality_Medium_MUON_EFF_RECO_STAT_LOWPT__1down", &AnalysisMuons_muon_effSF_Quality_Medium_MUON_EFF_RECO_STAT_LOWPT__1down, &b_AnalysisMuons_muon_effSF_Quality_Medium_MUON_EFF_RECO_STAT_LOWPT__1down);
   fChain->SetBranchAddress("AnalysisMuons_muon_effSF_Quality_Tight_MUON_EFF_RECO_STAT_LOWPT__1down", &AnalysisMuons_muon_effSF_Quality_Tight_MUON_EFF_RECO_STAT_LOWPT__1down, &b_AnalysisMuons_muon_effSF_Quality_Tight_MUON_EFF_RECO_STAT_LOWPT__1down);
   fChain->SetBranchAddress("AnalysisMuons_muon_effSF_MUON_EFF_RECO_STAT_LOWPT__1up", &AnalysisMuons_muon_effSF_MUON_EFF_RECO_STAT_LOWPT__1up, &b_AnalysisMuons_muon_effSF_MUON_EFF_RECO_STAT_LOWPT__1up);
   fChain->SetBranchAddress("AnalysisMuons_muon_effSF_Quality_Loose_MUON_EFF_RECO_STAT_LOWPT__1up", &AnalysisMuons_muon_effSF_Quality_Loose_MUON_EFF_RECO_STAT_LOWPT__1up, &b_AnalysisMuons_muon_effSF_Quality_Loose_MUON_EFF_RECO_STAT_LOWPT__1up);
   fChain->SetBranchAddress("AnalysisMuons_muon_effSF_Quality_Medium_MUON_EFF_RECO_STAT_LOWPT__1up", &AnalysisMuons_muon_effSF_Quality_Medium_MUON_EFF_RECO_STAT_LOWPT__1up, &b_AnalysisMuons_muon_effSF_Quality_Medium_MUON_EFF_RECO_STAT_LOWPT__1up);
   fChain->SetBranchAddress("AnalysisMuons_muon_effSF_Quality_Tight_MUON_EFF_RECO_STAT_LOWPT__1up", &AnalysisMuons_muon_effSF_Quality_Tight_MUON_EFF_RECO_STAT_LOWPT__1up, &b_AnalysisMuons_muon_effSF_Quality_Tight_MUON_EFF_RECO_STAT_LOWPT__1up);
   fChain->SetBranchAddress("AnalysisMuons_muon_effSF_MUON_EFF_RECO_SYS__1down", &AnalysisMuons_muon_effSF_MUON_EFF_RECO_SYS__1down, &b_AnalysisMuons_muon_effSF_MUON_EFF_RECO_SYS__1down);
   fChain->SetBranchAddress("AnalysisMuons_muon_effSF_Quality_Loose_MUON_EFF_RECO_SYS__1down", &AnalysisMuons_muon_effSF_Quality_Loose_MUON_EFF_RECO_SYS__1down, &b_AnalysisMuons_muon_effSF_Quality_Loose_MUON_EFF_RECO_SYS__1down);
   fChain->SetBranchAddress("AnalysisMuons_muon_effSF_Quality_Medium_MUON_EFF_RECO_SYS__1down", &AnalysisMuons_muon_effSF_Quality_Medium_MUON_EFF_RECO_SYS__1down, &b_AnalysisMuons_muon_effSF_Quality_Medium_MUON_EFF_RECO_SYS__1down);
   fChain->SetBranchAddress("AnalysisMuons_muon_effSF_Quality_Tight_MUON_EFF_RECO_SYS__1down", &AnalysisMuons_muon_effSF_Quality_Tight_MUON_EFF_RECO_SYS__1down, &b_AnalysisMuons_muon_effSF_Quality_Tight_MUON_EFF_RECO_SYS__1down);
   fChain->SetBranchAddress("AnalysisMuons_muon_effSF_MUON_EFF_RECO_SYS__1up", &AnalysisMuons_muon_effSF_MUON_EFF_RECO_SYS__1up, &b_AnalysisMuons_muon_effSF_MUON_EFF_RECO_SYS__1up);
   fChain->SetBranchAddress("AnalysisMuons_muon_effSF_Quality_Loose_MUON_EFF_RECO_SYS__1up", &AnalysisMuons_muon_effSF_Quality_Loose_MUON_EFF_RECO_SYS__1up, &b_AnalysisMuons_muon_effSF_Quality_Loose_MUON_EFF_RECO_SYS__1up);
   fChain->SetBranchAddress("AnalysisMuons_muon_effSF_Quality_Medium_MUON_EFF_RECO_SYS__1up", &AnalysisMuons_muon_effSF_Quality_Medium_MUON_EFF_RECO_SYS__1up, &b_AnalysisMuons_muon_effSF_Quality_Medium_MUON_EFF_RECO_SYS__1up);
   fChain->SetBranchAddress("AnalysisMuons_muon_effSF_Quality_Tight_MUON_EFF_RECO_SYS__1up", &AnalysisMuons_muon_effSF_Quality_Tight_MUON_EFF_RECO_SYS__1up, &b_AnalysisMuons_muon_effSF_Quality_Tight_MUON_EFF_RECO_SYS__1up);
   fChain->SetBranchAddress("AnalysisMuons_muon_effSF_MUON_EFF_RECO_SYS_LOWPT__1down", &AnalysisMuons_muon_effSF_MUON_EFF_RECO_SYS_LOWPT__1down, &b_AnalysisMuons_muon_effSF_MUON_EFF_RECO_SYS_LOWPT__1down);
   fChain->SetBranchAddress("AnalysisMuons_muon_effSF_Quality_Loose_MUON_EFF_RECO_SYS_LOWPT__1down", &AnalysisMuons_muon_effSF_Quality_Loose_MUON_EFF_RECO_SYS_LOWPT__1down, &b_AnalysisMuons_muon_effSF_Quality_Loose_MUON_EFF_RECO_SYS_LOWPT__1down);
   fChain->SetBranchAddress("AnalysisMuons_muon_effSF_Quality_Medium_MUON_EFF_RECO_SYS_LOWPT__1down", &AnalysisMuons_muon_effSF_Quality_Medium_MUON_EFF_RECO_SYS_LOWPT__1down, &b_AnalysisMuons_muon_effSF_Quality_Medium_MUON_EFF_RECO_SYS_LOWPT__1down);
   fChain->SetBranchAddress("AnalysisMuons_muon_effSF_Quality_Tight_MUON_EFF_RECO_SYS_LOWPT__1down", &AnalysisMuons_muon_effSF_Quality_Tight_MUON_EFF_RECO_SYS_LOWPT__1down, &b_AnalysisMuons_muon_effSF_Quality_Tight_MUON_EFF_RECO_SYS_LOWPT__1down);
   fChain->SetBranchAddress("AnalysisMuons_muon_effSF_MUON_EFF_RECO_SYS_LOWPT__1up", &AnalysisMuons_muon_effSF_MUON_EFF_RECO_SYS_LOWPT__1up, &b_AnalysisMuons_muon_effSF_MUON_EFF_RECO_SYS_LOWPT__1up);
   fChain->SetBranchAddress("AnalysisMuons_muon_effSF_Quality_Loose_MUON_EFF_RECO_SYS_LOWPT__1up", &AnalysisMuons_muon_effSF_Quality_Loose_MUON_EFF_RECO_SYS_LOWPT__1up, &b_AnalysisMuons_muon_effSF_Quality_Loose_MUON_EFF_RECO_SYS_LOWPT__1up);
   fChain->SetBranchAddress("AnalysisMuons_muon_effSF_Quality_Medium_MUON_EFF_RECO_SYS_LOWPT__1up", &AnalysisMuons_muon_effSF_Quality_Medium_MUON_EFF_RECO_SYS_LOWPT__1up, &b_AnalysisMuons_muon_effSF_Quality_Medium_MUON_EFF_RECO_SYS_LOWPT__1up);
   fChain->SetBranchAddress("AnalysisMuons_muon_effSF_Quality_Tight_MUON_EFF_RECO_SYS_LOWPT__1up", &AnalysisMuons_muon_effSF_Quality_Tight_MUON_EFF_RECO_SYS_LOWPT__1up, &b_AnalysisMuons_muon_effSF_Quality_Tight_MUON_EFF_RECO_SYS_LOWPT__1up);
   fChain->SetBranchAddress("AnalysisMuons_muon_effSF_TTVA_MUON_EFF_TTVA_STAT__1down", &AnalysisMuons_muon_effSF_TTVA_MUON_EFF_TTVA_STAT__1down, &b_AnalysisMuons_muon_effSF_TTVA_MUON_EFF_TTVA_STAT__1down);
   fChain->SetBranchAddress("AnalysisMuons_muon_effSF_TTVA_MUON_EFF_TTVA_STAT__1up", &AnalysisMuons_muon_effSF_TTVA_MUON_EFF_TTVA_STAT__1up, &b_AnalysisMuons_muon_effSF_TTVA_MUON_EFF_TTVA_STAT__1up);
   fChain->SetBranchAddress("AnalysisMuons_muon_effSF_TTVA_MUON_EFF_TTVA_SYS__1down", &AnalysisMuons_muon_effSF_TTVA_MUON_EFF_TTVA_SYS__1down, &b_AnalysisMuons_muon_effSF_TTVA_MUON_EFF_TTVA_SYS__1down);
   fChain->SetBranchAddress("AnalysisMuons_muon_effSF_TTVA_MUON_EFF_TTVA_SYS__1up", &AnalysisMuons_muon_effSF_TTVA_MUON_EFF_TTVA_SYS__1up, &b_AnalysisMuons_muon_effSF_TTVA_MUON_EFF_TTVA_SYS__1up);
   fChain->SetBranchAddress("AnalysisMuons_muon_effMC_Trig_Loose_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_EFF_TrigStatUncertainty__1down", &AnalysisMuons_muon_effMC_Trig_Loose_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_EFF_TrigStatUncertainty__1down, &b_AnalysisMuons_muon_effMC_Trig_Loose_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_EFF_TrigStatUncertainty__1down);
   fChain->SetBranchAddress("AnalysisMuons_muon_effData_Trig_Loose_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_EFF_TrigStatUncertainty__1down", &AnalysisMuons_muon_effData_Trig_Loose_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_EFF_TrigStatUncertainty__1down, &b_AnalysisMuons_muon_effData_Trig_Loose_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_EFF_TrigStatUncertainty__1down);
   fChain->SetBranchAddress("AnalysisMuons_muon_effMC_Trig_Loose_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_EFF_TrigStatUncertainty__1down", &AnalysisMuons_muon_effMC_Trig_Loose_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_EFF_TrigStatUncertainty__1down, &b_AnalysisMuons_muon_effMC_Trig_Loose_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_EFF_TrigStatUncertainty__1down);
   fChain->SetBranchAddress("AnalysisMuons_muon_effData_Trig_Loose_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_EFF_TrigStatUncertainty__1down", &AnalysisMuons_muon_effData_Trig_Loose_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_EFF_TrigStatUncertainty__1down, &b_AnalysisMuons_muon_effData_Trig_Loose_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_EFF_TrigStatUncertainty__1down);
   fChain->SetBranchAddress("AnalysisMuons_muon_effMC_Trig_Medium_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_EFF_TrigStatUncertainty__1down", &AnalysisMuons_muon_effMC_Trig_Medium_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_EFF_TrigStatUncertainty__1down, &b_AnalysisMuons_muon_effMC_Trig_Medium_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_EFF_TrigStatUncertainty__1down);
   fChain->SetBranchAddress("AnalysisMuons_muon_effData_Trig_Medium_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_EFF_TrigStatUncertainty__1down", &AnalysisMuons_muon_effData_Trig_Medium_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_EFF_TrigStatUncertainty__1down, &b_AnalysisMuons_muon_effData_Trig_Medium_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_EFF_TrigStatUncertainty__1down);
   fChain->SetBranchAddress("AnalysisMuons_muon_effMC_Trig_Medium_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_EFF_TrigStatUncertainty__1down", &AnalysisMuons_muon_effMC_Trig_Medium_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_EFF_TrigStatUncertainty__1down, &b_AnalysisMuons_muon_effMC_Trig_Medium_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_EFF_TrigStatUncertainty__1down);
   fChain->SetBranchAddress("AnalysisMuons_muon_effData_Trig_Medium_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_EFF_TrigStatUncertainty__1down", &AnalysisMuons_muon_effData_Trig_Medium_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_EFF_TrigStatUncertainty__1down, &b_AnalysisMuons_muon_effData_Trig_Medium_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_EFF_TrigStatUncertainty__1down);
   fChain->SetBranchAddress("AnalysisMuons_muon_effMC_Trig_Tight_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_EFF_TrigStatUncertainty__1down", &AnalysisMuons_muon_effMC_Trig_Tight_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_EFF_TrigStatUncertainty__1down, &b_AnalysisMuons_muon_effMC_Trig_Tight_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_EFF_TrigStatUncertainty__1down);
   fChain->SetBranchAddress("AnalysisMuons_muon_effData_Trig_Tight_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_EFF_TrigStatUncertainty__1down", &AnalysisMuons_muon_effData_Trig_Tight_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_EFF_TrigStatUncertainty__1down, &b_AnalysisMuons_muon_effData_Trig_Tight_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_EFF_TrigStatUncertainty__1down);
   fChain->SetBranchAddress("AnalysisMuons_muon_effMC_Trig_Tight_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_EFF_TrigStatUncertainty__1down", &AnalysisMuons_muon_effMC_Trig_Tight_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_EFF_TrigStatUncertainty__1down, &b_AnalysisMuons_muon_effMC_Trig_Tight_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_EFF_TrigStatUncertainty__1down);
   fChain->SetBranchAddress("AnalysisMuons_muon_effData_Trig_Tight_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_EFF_TrigStatUncertainty__1down", &AnalysisMuons_muon_effData_Trig_Tight_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_EFF_TrigStatUncertainty__1down, &b_AnalysisMuons_muon_effData_Trig_Tight_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_EFF_TrigStatUncertainty__1down);
   fChain->SetBranchAddress("AnalysisMuons_muon_effMC_Trig_Loose_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_EFF_TrigStatUncertainty__1up", &AnalysisMuons_muon_effMC_Trig_Loose_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_EFF_TrigStatUncertainty__1up, &b_AnalysisMuons_muon_effMC_Trig_Loose_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_EFF_TrigStatUncertainty__1up);
   fChain->SetBranchAddress("AnalysisMuons_muon_effData_Trig_Loose_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_EFF_TrigStatUncertainty__1up", &AnalysisMuons_muon_effData_Trig_Loose_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_EFF_TrigStatUncertainty__1up, &b_AnalysisMuons_muon_effData_Trig_Loose_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_EFF_TrigStatUncertainty__1up);
   fChain->SetBranchAddress("AnalysisMuons_muon_effMC_Trig_Loose_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_EFF_TrigStatUncertainty__1up", &AnalysisMuons_muon_effMC_Trig_Loose_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_EFF_TrigStatUncertainty__1up, &b_AnalysisMuons_muon_effMC_Trig_Loose_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_EFF_TrigStatUncertainty__1up);
   fChain->SetBranchAddress("AnalysisMuons_muon_effData_Trig_Loose_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_EFF_TrigStatUncertainty__1up", &AnalysisMuons_muon_effData_Trig_Loose_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_EFF_TrigStatUncertainty__1up, &b_AnalysisMuons_muon_effData_Trig_Loose_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_EFF_TrigStatUncertainty__1up);
   fChain->SetBranchAddress("AnalysisMuons_muon_effMC_Trig_Medium_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_EFF_TrigStatUncertainty__1up", &AnalysisMuons_muon_effMC_Trig_Medium_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_EFF_TrigStatUncertainty__1up, &b_AnalysisMuons_muon_effMC_Trig_Medium_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_EFF_TrigStatUncertainty__1up);
   fChain->SetBranchAddress("AnalysisMuons_muon_effData_Trig_Medium_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_EFF_TrigStatUncertainty__1up", &AnalysisMuons_muon_effData_Trig_Medium_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_EFF_TrigStatUncertainty__1up, &b_AnalysisMuons_muon_effData_Trig_Medium_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_EFF_TrigStatUncertainty__1up);
   fChain->SetBranchAddress("AnalysisMuons_muon_effMC_Trig_Medium_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_EFF_TrigStatUncertainty__1up", &AnalysisMuons_muon_effMC_Trig_Medium_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_EFF_TrigStatUncertainty__1up, &b_AnalysisMuons_muon_effMC_Trig_Medium_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_EFF_TrigStatUncertainty__1up);
   fChain->SetBranchAddress("AnalysisMuons_muon_effData_Trig_Medium_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_EFF_TrigStatUncertainty__1up", &AnalysisMuons_muon_effData_Trig_Medium_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_EFF_TrigStatUncertainty__1up, &b_AnalysisMuons_muon_effData_Trig_Medium_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_EFF_TrigStatUncertainty__1up);
   fChain->SetBranchAddress("AnalysisMuons_muon_effMC_Trig_Tight_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_EFF_TrigStatUncertainty__1up", &AnalysisMuons_muon_effMC_Trig_Tight_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_EFF_TrigStatUncertainty__1up, &b_AnalysisMuons_muon_effMC_Trig_Tight_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_EFF_TrigStatUncertainty__1up);
   fChain->SetBranchAddress("AnalysisMuons_muon_effData_Trig_Tight_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_EFF_TrigStatUncertainty__1up", &AnalysisMuons_muon_effData_Trig_Tight_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_EFF_TrigStatUncertainty__1up, &b_AnalysisMuons_muon_effData_Trig_Tight_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_EFF_TrigStatUncertainty__1up);
   fChain->SetBranchAddress("AnalysisMuons_muon_effMC_Trig_Tight_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_EFF_TrigStatUncertainty__1up", &AnalysisMuons_muon_effMC_Trig_Tight_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_EFF_TrigStatUncertainty__1up, &b_AnalysisMuons_muon_effMC_Trig_Tight_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_EFF_TrigStatUncertainty__1up);
   fChain->SetBranchAddress("AnalysisMuons_muon_effData_Trig_Tight_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_EFF_TrigStatUncertainty__1up", &AnalysisMuons_muon_effData_Trig_Tight_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_EFF_TrigStatUncertainty__1up, &b_AnalysisMuons_muon_effData_Trig_Tight_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_EFF_TrigStatUncertainty__1up);
   fChain->SetBranchAddress("AnalysisMuons_muon_effMC_Trig_Loose_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_EFF_TrigSystUncertainty__1down", &AnalysisMuons_muon_effMC_Trig_Loose_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_EFF_TrigSystUncertainty__1down, &b_AnalysisMuons_muon_effMC_Trig_Loose_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_EFF_TrigSystUncertainty__1down);
   fChain->SetBranchAddress("AnalysisMuons_muon_effData_Trig_Loose_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_EFF_TrigSystUncertainty__1down", &AnalysisMuons_muon_effData_Trig_Loose_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_EFF_TrigSystUncertainty__1down, &b_AnalysisMuons_muon_effData_Trig_Loose_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_EFF_TrigSystUncertainty__1down);
   fChain->SetBranchAddress("AnalysisMuons_muon_effMC_Trig_Loose_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_EFF_TrigSystUncertainty__1down", &AnalysisMuons_muon_effMC_Trig_Loose_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_EFF_TrigSystUncertainty__1down, &b_AnalysisMuons_muon_effMC_Trig_Loose_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_EFF_TrigSystUncertainty__1down);
   fChain->SetBranchAddress("AnalysisMuons_muon_effData_Trig_Loose_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_EFF_TrigSystUncertainty__1down", &AnalysisMuons_muon_effData_Trig_Loose_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_EFF_TrigSystUncertainty__1down, &b_AnalysisMuons_muon_effData_Trig_Loose_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_EFF_TrigSystUncertainty__1down);
   fChain->SetBranchAddress("AnalysisMuons_muon_effMC_Trig_Medium_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_EFF_TrigSystUncertainty__1down", &AnalysisMuons_muon_effMC_Trig_Medium_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_EFF_TrigSystUncertainty__1down, &b_AnalysisMuons_muon_effMC_Trig_Medium_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_EFF_TrigSystUncertainty__1down);
   fChain->SetBranchAddress("AnalysisMuons_muon_effData_Trig_Medium_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_EFF_TrigSystUncertainty__1down", &AnalysisMuons_muon_effData_Trig_Medium_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_EFF_TrigSystUncertainty__1down, &b_AnalysisMuons_muon_effData_Trig_Medium_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_EFF_TrigSystUncertainty__1down);
   fChain->SetBranchAddress("AnalysisMuons_muon_effMC_Trig_Medium_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_EFF_TrigSystUncertainty__1down", &AnalysisMuons_muon_effMC_Trig_Medium_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_EFF_TrigSystUncertainty__1down, &b_AnalysisMuons_muon_effMC_Trig_Medium_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_EFF_TrigSystUncertainty__1down);
   fChain->SetBranchAddress("AnalysisMuons_muon_effData_Trig_Medium_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_EFF_TrigSystUncertainty__1down", &AnalysisMuons_muon_effData_Trig_Medium_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_EFF_TrigSystUncertainty__1down, &b_AnalysisMuons_muon_effData_Trig_Medium_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_EFF_TrigSystUncertainty__1down);
   fChain->SetBranchAddress("AnalysisMuons_muon_effMC_Trig_Tight_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_EFF_TrigSystUncertainty__1down", &AnalysisMuons_muon_effMC_Trig_Tight_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_EFF_TrigSystUncertainty__1down, &b_AnalysisMuons_muon_effMC_Trig_Tight_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_EFF_TrigSystUncertainty__1down);
   fChain->SetBranchAddress("AnalysisMuons_muon_effData_Trig_Tight_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_EFF_TrigSystUncertainty__1down", &AnalysisMuons_muon_effData_Trig_Tight_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_EFF_TrigSystUncertainty__1down, &b_AnalysisMuons_muon_effData_Trig_Tight_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_EFF_TrigSystUncertainty__1down);
   fChain->SetBranchAddress("AnalysisMuons_muon_effMC_Trig_Tight_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_EFF_TrigSystUncertainty__1down", &AnalysisMuons_muon_effMC_Trig_Tight_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_EFF_TrigSystUncertainty__1down, &b_AnalysisMuons_muon_effMC_Trig_Tight_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_EFF_TrigSystUncertainty__1down);
   fChain->SetBranchAddress("AnalysisMuons_muon_effData_Trig_Tight_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_EFF_TrigSystUncertainty__1down", &AnalysisMuons_muon_effData_Trig_Tight_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_EFF_TrigSystUncertainty__1down, &b_AnalysisMuons_muon_effData_Trig_Tight_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_EFF_TrigSystUncertainty__1down);
   fChain->SetBranchAddress("AnalysisMuons_muon_effMC_Trig_Loose_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_EFF_TrigSystUncertainty__1up", &AnalysisMuons_muon_effMC_Trig_Loose_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_EFF_TrigSystUncertainty__1up, &b_AnalysisMuons_muon_effMC_Trig_Loose_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_EFF_TrigSystUncertainty__1up);
   fChain->SetBranchAddress("AnalysisMuons_muon_effData_Trig_Loose_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_EFF_TrigSystUncertainty__1up", &AnalysisMuons_muon_effData_Trig_Loose_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_EFF_TrigSystUncertainty__1up, &b_AnalysisMuons_muon_effData_Trig_Loose_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_EFF_TrigSystUncertainty__1up);
   fChain->SetBranchAddress("AnalysisMuons_muon_effMC_Trig_Loose_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_EFF_TrigSystUncertainty__1up", &AnalysisMuons_muon_effMC_Trig_Loose_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_EFF_TrigSystUncertainty__1up, &b_AnalysisMuons_muon_effMC_Trig_Loose_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_EFF_TrigSystUncertainty__1up);
   fChain->SetBranchAddress("AnalysisMuons_muon_effData_Trig_Loose_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_EFF_TrigSystUncertainty__1up", &AnalysisMuons_muon_effData_Trig_Loose_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_EFF_TrigSystUncertainty__1up, &b_AnalysisMuons_muon_effData_Trig_Loose_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_EFF_TrigSystUncertainty__1up);
   fChain->SetBranchAddress("AnalysisMuons_muon_effMC_Trig_Medium_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_EFF_TrigSystUncertainty__1up", &AnalysisMuons_muon_effMC_Trig_Medium_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_EFF_TrigSystUncertainty__1up, &b_AnalysisMuons_muon_effMC_Trig_Medium_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_EFF_TrigSystUncertainty__1up);
   fChain->SetBranchAddress("AnalysisMuons_muon_effData_Trig_Medium_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_EFF_TrigSystUncertainty__1up", &AnalysisMuons_muon_effData_Trig_Medium_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_EFF_TrigSystUncertainty__1up, &b_AnalysisMuons_muon_effData_Trig_Medium_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_EFF_TrigSystUncertainty__1up);
   fChain->SetBranchAddress("AnalysisMuons_muon_effMC_Trig_Medium_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_EFF_TrigSystUncertainty__1up", &AnalysisMuons_muon_effMC_Trig_Medium_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_EFF_TrigSystUncertainty__1up, &b_AnalysisMuons_muon_effMC_Trig_Medium_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_EFF_TrigSystUncertainty__1up);
   fChain->SetBranchAddress("AnalysisMuons_muon_effData_Trig_Medium_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_EFF_TrigSystUncertainty__1up", &AnalysisMuons_muon_effData_Trig_Medium_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_EFF_TrigSystUncertainty__1up, &b_AnalysisMuons_muon_effData_Trig_Medium_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_EFF_TrigSystUncertainty__1up);
   fChain->SetBranchAddress("AnalysisMuons_muon_effMC_Trig_Tight_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_EFF_TrigSystUncertainty__1up", &AnalysisMuons_muon_effMC_Trig_Tight_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_EFF_TrigSystUncertainty__1up, &b_AnalysisMuons_muon_effMC_Trig_Tight_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_EFF_TrigSystUncertainty__1up);
   fChain->SetBranchAddress("AnalysisMuons_muon_effData_Trig_Tight_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_EFF_TrigSystUncertainty__1up", &AnalysisMuons_muon_effData_Trig_Tight_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_EFF_TrigSystUncertainty__1up, &b_AnalysisMuons_muon_effData_Trig_Tight_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_EFF_TrigSystUncertainty__1up);
   fChain->SetBranchAddress("AnalysisMuons_muon_effMC_Trig_Tight_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_EFF_TrigSystUncertainty__1up", &AnalysisMuons_muon_effMC_Trig_Tight_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_EFF_TrigSystUncertainty__1up, &b_AnalysisMuons_muon_effMC_Trig_Tight_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_EFF_TrigSystUncertainty__1up);
   fChain->SetBranchAddress("AnalysisMuons_muon_effData_Trig_Tight_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_EFF_TrigSystUncertainty__1up", &AnalysisMuons_muon_effData_Trig_Tight_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_EFF_TrigSystUncertainty__1up, &b_AnalysisMuons_muon_effData_Trig_Tight_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_EFF_TrigSystUncertainty__1up);
   fChain->SetBranchAddress("AnalysisElectrons_el_selected_MUON_SAGITTA_DATASTAT__1down", &AnalysisElectrons_el_selected_MUON_SAGITTA_DATASTAT__1down, &b_AnalysisElectrons_el_selected_MUON_SAGITTA_DATASTAT__1down);
   fChain->SetBranchAddress("AnalysisMuons_pt_MUON_SAGITTA_DATASTAT__1down", &AnalysisMuons_pt_MUON_SAGITTA_DATASTAT__1down, &b_AnalysisMuons_pt_MUON_SAGITTA_DATASTAT__1down);
   fChain->SetBranchAddress("AnalysisMuons_mu_selected_MUON_SAGITTA_DATASTAT__1down", &AnalysisMuons_mu_selected_MUON_SAGITTA_DATASTAT__1down, &b_AnalysisMuons_mu_selected_MUON_SAGITTA_DATASTAT__1down);
   fChain->SetBranchAddress("AnalysisMuons_isQuality_Loose_MUON_SAGITTA_DATASTAT__1down", &AnalysisMuons_isQuality_Loose_MUON_SAGITTA_DATASTAT__1down, &b_AnalysisMuons_isQuality_Loose_MUON_SAGITTA_DATASTAT__1down);
   fChain->SetBranchAddress("AnalysisMuons_isQuality_Medium_MUON_SAGITTA_DATASTAT__1down", &AnalysisMuons_isQuality_Medium_MUON_SAGITTA_DATASTAT__1down, &b_AnalysisMuons_isQuality_Medium_MUON_SAGITTA_DATASTAT__1down);
   fChain->SetBranchAddress("AnalysisMuons_isQuality_Tight_MUON_SAGITTA_DATASTAT__1down", &AnalysisMuons_isQuality_Tight_MUON_SAGITTA_DATASTAT__1down, &b_AnalysisMuons_isQuality_Tight_MUON_SAGITTA_DATASTAT__1down);
   fChain->SetBranchAddress("AnalysisMuons_isIsolated_PflowTight_VarRad_MUON_SAGITTA_DATASTAT__1down", &AnalysisMuons_isIsolated_PflowTight_VarRad_MUON_SAGITTA_DATASTAT__1down, &b_AnalysisMuons_isIsolated_PflowTight_VarRad_MUON_SAGITTA_DATASTAT__1down);
   fChain->SetBranchAddress("AnalysisMuons_muon_effSF_MUON_SAGITTA_DATASTAT__1down", &AnalysisMuons_muon_effSF_MUON_SAGITTA_DATASTAT__1down, &b_AnalysisMuons_muon_effSF_MUON_SAGITTA_DATASTAT__1down);
   fChain->SetBranchAddress("AnalysisMuons_muon_effSF_TTVA_MUON_SAGITTA_DATASTAT__1down", &AnalysisMuons_muon_effSF_TTVA_MUON_SAGITTA_DATASTAT__1down, &b_AnalysisMuons_muon_effSF_TTVA_MUON_SAGITTA_DATASTAT__1down);
   fChain->SetBranchAddress("AnalysisMuons_muon_effSF_Quality_Loose_MUON_SAGITTA_DATASTAT__1down", &AnalysisMuons_muon_effSF_Quality_Loose_MUON_SAGITTA_DATASTAT__1down, &b_AnalysisMuons_muon_effSF_Quality_Loose_MUON_SAGITTA_DATASTAT__1down);
   fChain->SetBranchAddress("AnalysisMuons_muon_effSF_Quality_Medium_MUON_SAGITTA_DATASTAT__1down", &AnalysisMuons_muon_effSF_Quality_Medium_MUON_SAGITTA_DATASTAT__1down, &b_AnalysisMuons_muon_effSF_Quality_Medium_MUON_SAGITTA_DATASTAT__1down);
   fChain->SetBranchAddress("AnalysisMuons_muon_effSF_Quality_Tight_MUON_SAGITTA_DATASTAT__1down", &AnalysisMuons_muon_effSF_Quality_Tight_MUON_SAGITTA_DATASTAT__1down, &b_AnalysisMuons_muon_effSF_Quality_Tight_MUON_SAGITTA_DATASTAT__1down);
   fChain->SetBranchAddress("AnalysisMuons_muon_effSF_Isol_PflowTight_VarRad_MUON_SAGITTA_DATASTAT__1down", &AnalysisMuons_muon_effSF_Isol_PflowTight_VarRad_MUON_SAGITTA_DATASTAT__1down, &b_AnalysisMuons_muon_effSF_Isol_PflowTight_VarRad_MUON_SAGITTA_DATASTAT__1down);
   fChain->SetBranchAddress("AnalysisMuons_muon_effMC_Trig_Loose_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_SAGITTA_DATASTAT__1down", &AnalysisMuons_muon_effMC_Trig_Loose_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_SAGITTA_DATASTAT__1down, &b_AnalysisMuons_muon_effMC_Trig_Loose_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_SAGITTA_DATASTAT__1down);
   fChain->SetBranchAddress("AnalysisMuons_muon_effData_Trig_Loose_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_SAGITTA_DATASTAT__1down", &AnalysisMuons_muon_effData_Trig_Loose_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_SAGITTA_DATASTAT__1down, &b_AnalysisMuons_muon_effData_Trig_Loose_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_SAGITTA_DATASTAT__1down);
   fChain->SetBranchAddress("AnalysisMuons_muon_effMC_Trig_Loose_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_SAGITTA_DATASTAT__1down", &AnalysisMuons_muon_effMC_Trig_Loose_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_SAGITTA_DATASTAT__1down, &b_AnalysisMuons_muon_effMC_Trig_Loose_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_SAGITTA_DATASTAT__1down);
   fChain->SetBranchAddress("AnalysisMuons_muon_effData_Trig_Loose_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_SAGITTA_DATASTAT__1down", &AnalysisMuons_muon_effData_Trig_Loose_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_SAGITTA_DATASTAT__1down, &b_AnalysisMuons_muon_effData_Trig_Loose_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_SAGITTA_DATASTAT__1down);
   fChain->SetBranchAddress("AnalysisMuons_muon_effMC_Trig_Medium_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_SAGITTA_DATASTAT__1down", &AnalysisMuons_muon_effMC_Trig_Medium_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_SAGITTA_DATASTAT__1down, &b_AnalysisMuons_muon_effMC_Trig_Medium_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_SAGITTA_DATASTAT__1down);
   fChain->SetBranchAddress("AnalysisMuons_muon_effData_Trig_Medium_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_SAGITTA_DATASTAT__1down", &AnalysisMuons_muon_effData_Trig_Medium_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_SAGITTA_DATASTAT__1down, &b_AnalysisMuons_muon_effData_Trig_Medium_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_SAGITTA_DATASTAT__1down);
   fChain->SetBranchAddress("AnalysisMuons_muon_effMC_Trig_Medium_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_SAGITTA_DATASTAT__1down", &AnalysisMuons_muon_effMC_Trig_Medium_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_SAGITTA_DATASTAT__1down, &b_AnalysisMuons_muon_effMC_Trig_Medium_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_SAGITTA_DATASTAT__1down);
   fChain->SetBranchAddress("AnalysisMuons_muon_effData_Trig_Medium_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_SAGITTA_DATASTAT__1down", &AnalysisMuons_muon_effData_Trig_Medium_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_SAGITTA_DATASTAT__1down, &b_AnalysisMuons_muon_effData_Trig_Medium_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_SAGITTA_DATASTAT__1down);
   fChain->SetBranchAddress("AnalysisMuons_muon_effMC_Trig_Tight_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_SAGITTA_DATASTAT__1down", &AnalysisMuons_muon_effMC_Trig_Tight_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_SAGITTA_DATASTAT__1down, &b_AnalysisMuons_muon_effMC_Trig_Tight_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_SAGITTA_DATASTAT__1down);
   fChain->SetBranchAddress("AnalysisMuons_muon_effData_Trig_Tight_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_SAGITTA_DATASTAT__1down", &AnalysisMuons_muon_effData_Trig_Tight_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_SAGITTA_DATASTAT__1down, &b_AnalysisMuons_muon_effData_Trig_Tight_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_SAGITTA_DATASTAT__1down);
   fChain->SetBranchAddress("AnalysisMuons_muon_effMC_Trig_Tight_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_SAGITTA_DATASTAT__1down", &AnalysisMuons_muon_effMC_Trig_Tight_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_SAGITTA_DATASTAT__1down, &b_AnalysisMuons_muon_effMC_Trig_Tight_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_SAGITTA_DATASTAT__1down);
   fChain->SetBranchAddress("AnalysisMuons_muon_effData_Trig_Tight_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_SAGITTA_DATASTAT__1down", &AnalysisMuons_muon_effData_Trig_Tight_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_SAGITTA_DATASTAT__1down, &b_AnalysisMuons_muon_effData_Trig_Tight_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_SAGITTA_DATASTAT__1down);
   fChain->SetBranchAddress("AnalysisJets_jet_selected_MUON_SAGITTA_DATASTAT__1down", &AnalysisJets_jet_selected_MUON_SAGITTA_DATASTAT__1down, &b_AnalysisJets_jet_selected_MUON_SAGITTA_DATASTAT__1down);
   fChain->SetBranchAddress("METInfo_MET_MUON_SAGITTA_DATASTAT__1down", &METInfo_MET_MUON_SAGITTA_DATASTAT__1down, &b_METInfo_MET_MUON_SAGITTA_DATASTAT__1down);
   fChain->SetBranchAddress("METInfo_METPhi_MUON_SAGITTA_DATASTAT__1down", &METInfo_METPhi_MUON_SAGITTA_DATASTAT__1down, &b_METInfo_METPhi_MUON_SAGITTA_DATASTAT__1down);
   fChain->SetBranchAddress("AnalysisElectrons_el_selected_MUON_SAGITTA_DATASTAT__1up", &AnalysisElectrons_el_selected_MUON_SAGITTA_DATASTAT__1up, &b_AnalysisElectrons_el_selected_MUON_SAGITTA_DATASTAT__1up);
   fChain->SetBranchAddress("AnalysisMuons_pt_MUON_SAGITTA_DATASTAT__1up", &AnalysisMuons_pt_MUON_SAGITTA_DATASTAT__1up, &b_AnalysisMuons_pt_MUON_SAGITTA_DATASTAT__1up);
   fChain->SetBranchAddress("AnalysisMuons_mu_selected_MUON_SAGITTA_DATASTAT__1up", &AnalysisMuons_mu_selected_MUON_SAGITTA_DATASTAT__1up, &b_AnalysisMuons_mu_selected_MUON_SAGITTA_DATASTAT__1up);
   fChain->SetBranchAddress("AnalysisMuons_isQuality_Loose_MUON_SAGITTA_DATASTAT__1up", &AnalysisMuons_isQuality_Loose_MUON_SAGITTA_DATASTAT__1up, &b_AnalysisMuons_isQuality_Loose_MUON_SAGITTA_DATASTAT__1up);
   fChain->SetBranchAddress("AnalysisMuons_isQuality_Medium_MUON_SAGITTA_DATASTAT__1up", &AnalysisMuons_isQuality_Medium_MUON_SAGITTA_DATASTAT__1up, &b_AnalysisMuons_isQuality_Medium_MUON_SAGITTA_DATASTAT__1up);
   fChain->SetBranchAddress("AnalysisMuons_isQuality_Tight_MUON_SAGITTA_DATASTAT__1up", &AnalysisMuons_isQuality_Tight_MUON_SAGITTA_DATASTAT__1up, &b_AnalysisMuons_isQuality_Tight_MUON_SAGITTA_DATASTAT__1up);
   fChain->SetBranchAddress("AnalysisMuons_isIsolated_PflowTight_VarRad_MUON_SAGITTA_DATASTAT__1up", &AnalysisMuons_isIsolated_PflowTight_VarRad_MUON_SAGITTA_DATASTAT__1up, &b_AnalysisMuons_isIsolated_PflowTight_VarRad_MUON_SAGITTA_DATASTAT__1up);
   fChain->SetBranchAddress("AnalysisMuons_muon_effSF_MUON_SAGITTA_DATASTAT__1up", &AnalysisMuons_muon_effSF_MUON_SAGITTA_DATASTAT__1up, &b_AnalysisMuons_muon_effSF_MUON_SAGITTA_DATASTAT__1up);
   fChain->SetBranchAddress("AnalysisMuons_muon_effSF_TTVA_MUON_SAGITTA_DATASTAT__1up", &AnalysisMuons_muon_effSF_TTVA_MUON_SAGITTA_DATASTAT__1up, &b_AnalysisMuons_muon_effSF_TTVA_MUON_SAGITTA_DATASTAT__1up);
   fChain->SetBranchAddress("AnalysisMuons_muon_effSF_Quality_Loose_MUON_SAGITTA_DATASTAT__1up", &AnalysisMuons_muon_effSF_Quality_Loose_MUON_SAGITTA_DATASTAT__1up, &b_AnalysisMuons_muon_effSF_Quality_Loose_MUON_SAGITTA_DATASTAT__1up);
   fChain->SetBranchAddress("AnalysisMuons_muon_effSF_Quality_Medium_MUON_SAGITTA_DATASTAT__1up", &AnalysisMuons_muon_effSF_Quality_Medium_MUON_SAGITTA_DATASTAT__1up, &b_AnalysisMuons_muon_effSF_Quality_Medium_MUON_SAGITTA_DATASTAT__1up);
   fChain->SetBranchAddress("AnalysisMuons_muon_effSF_Quality_Tight_MUON_SAGITTA_DATASTAT__1up", &AnalysisMuons_muon_effSF_Quality_Tight_MUON_SAGITTA_DATASTAT__1up, &b_AnalysisMuons_muon_effSF_Quality_Tight_MUON_SAGITTA_DATASTAT__1up);
   fChain->SetBranchAddress("AnalysisMuons_muon_effSF_Isol_PflowTight_VarRad_MUON_SAGITTA_DATASTAT__1up", &AnalysisMuons_muon_effSF_Isol_PflowTight_VarRad_MUON_SAGITTA_DATASTAT__1up, &b_AnalysisMuons_muon_effSF_Isol_PflowTight_VarRad_MUON_SAGITTA_DATASTAT__1up);
   fChain->SetBranchAddress("AnalysisMuons_muon_effMC_Trig_Loose_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_SAGITTA_DATASTAT__1up", &AnalysisMuons_muon_effMC_Trig_Loose_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_SAGITTA_DATASTAT__1up, &b_AnalysisMuons_muon_effMC_Trig_Loose_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_SAGITTA_DATASTAT__1up);
   fChain->SetBranchAddress("AnalysisMuons_muon_effData_Trig_Loose_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_SAGITTA_DATASTAT__1up", &AnalysisMuons_muon_effData_Trig_Loose_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_SAGITTA_DATASTAT__1up, &b_AnalysisMuons_muon_effData_Trig_Loose_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_SAGITTA_DATASTAT__1up);
   fChain->SetBranchAddress("AnalysisMuons_muon_effMC_Trig_Loose_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_SAGITTA_DATASTAT__1up", &AnalysisMuons_muon_effMC_Trig_Loose_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_SAGITTA_DATASTAT__1up, &b_AnalysisMuons_muon_effMC_Trig_Loose_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_SAGITTA_DATASTAT__1up);
   fChain->SetBranchAddress("AnalysisMuons_muon_effData_Trig_Loose_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_SAGITTA_DATASTAT__1up", &AnalysisMuons_muon_effData_Trig_Loose_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_SAGITTA_DATASTAT__1up, &b_AnalysisMuons_muon_effData_Trig_Loose_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_SAGITTA_DATASTAT__1up);
   fChain->SetBranchAddress("AnalysisMuons_muon_effMC_Trig_Medium_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_SAGITTA_DATASTAT__1up", &AnalysisMuons_muon_effMC_Trig_Medium_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_SAGITTA_DATASTAT__1up, &b_AnalysisMuons_muon_effMC_Trig_Medium_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_SAGITTA_DATASTAT__1up);
   fChain->SetBranchAddress("AnalysisMuons_muon_effData_Trig_Medium_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_SAGITTA_DATASTAT__1up", &AnalysisMuons_muon_effData_Trig_Medium_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_SAGITTA_DATASTAT__1up, &b_AnalysisMuons_muon_effData_Trig_Medium_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_SAGITTA_DATASTAT__1up);
   fChain->SetBranchAddress("AnalysisMuons_muon_effMC_Trig_Medium_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_SAGITTA_DATASTAT__1up", &AnalysisMuons_muon_effMC_Trig_Medium_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_SAGITTA_DATASTAT__1up, &b_AnalysisMuons_muon_effMC_Trig_Medium_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_SAGITTA_DATASTAT__1up);
   fChain->SetBranchAddress("AnalysisMuons_muon_effData_Trig_Medium_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_SAGITTA_DATASTAT__1up", &AnalysisMuons_muon_effData_Trig_Medium_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_SAGITTA_DATASTAT__1up, &b_AnalysisMuons_muon_effData_Trig_Medium_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_SAGITTA_DATASTAT__1up);
   fChain->SetBranchAddress("AnalysisMuons_muon_effMC_Trig_Tight_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_SAGITTA_DATASTAT__1up", &AnalysisMuons_muon_effMC_Trig_Tight_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_SAGITTA_DATASTAT__1up, &b_AnalysisMuons_muon_effMC_Trig_Tight_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_SAGITTA_DATASTAT__1up);
   fChain->SetBranchAddress("AnalysisMuons_muon_effData_Trig_Tight_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_SAGITTA_DATASTAT__1up", &AnalysisMuons_muon_effData_Trig_Tight_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_SAGITTA_DATASTAT__1up, &b_AnalysisMuons_muon_effData_Trig_Tight_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_SAGITTA_DATASTAT__1up);
   fChain->SetBranchAddress("AnalysisMuons_muon_effMC_Trig_Tight_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_SAGITTA_DATASTAT__1up", &AnalysisMuons_muon_effMC_Trig_Tight_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_SAGITTA_DATASTAT__1up, &b_AnalysisMuons_muon_effMC_Trig_Tight_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_SAGITTA_DATASTAT__1up);
   fChain->SetBranchAddress("AnalysisMuons_muon_effData_Trig_Tight_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_SAGITTA_DATASTAT__1up", &AnalysisMuons_muon_effData_Trig_Tight_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_SAGITTA_DATASTAT__1up, &b_AnalysisMuons_muon_effData_Trig_Tight_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_SAGITTA_DATASTAT__1up);
   fChain->SetBranchAddress("AnalysisJets_jet_selected_MUON_SAGITTA_DATASTAT__1up", &AnalysisJets_jet_selected_MUON_SAGITTA_DATASTAT__1up, &b_AnalysisJets_jet_selected_MUON_SAGITTA_DATASTAT__1up);
   fChain->SetBranchAddress("METInfo_MET_MUON_SAGITTA_DATASTAT__1up", &METInfo_MET_MUON_SAGITTA_DATASTAT__1up, &b_METInfo_MET_MUON_SAGITTA_DATASTAT__1up);
   fChain->SetBranchAddress("METInfo_METPhi_MUON_SAGITTA_DATASTAT__1up", &METInfo_METPhi_MUON_SAGITTA_DATASTAT__1up, &b_METInfo_METPhi_MUON_SAGITTA_DATASTAT__1up);
   fChain->SetBranchAddress("AnalysisElectrons_el_selected_MUON_SAGITTA_RESBIAS__1down", &AnalysisElectrons_el_selected_MUON_SAGITTA_RESBIAS__1down, &b_AnalysisElectrons_el_selected_MUON_SAGITTA_RESBIAS__1down);
   fChain->SetBranchAddress("AnalysisMuons_pt_MUON_SAGITTA_RESBIAS__1down", &AnalysisMuons_pt_MUON_SAGITTA_RESBIAS__1down, &b_AnalysisMuons_pt_MUON_SAGITTA_RESBIAS__1down);
   fChain->SetBranchAddress("AnalysisMuons_mu_selected_MUON_SAGITTA_RESBIAS__1down", &AnalysisMuons_mu_selected_MUON_SAGITTA_RESBIAS__1down, &b_AnalysisMuons_mu_selected_MUON_SAGITTA_RESBIAS__1down);
   fChain->SetBranchAddress("AnalysisMuons_isQuality_Loose_MUON_SAGITTA_RESBIAS__1down", &AnalysisMuons_isQuality_Loose_MUON_SAGITTA_RESBIAS__1down, &b_AnalysisMuons_isQuality_Loose_MUON_SAGITTA_RESBIAS__1down);
   fChain->SetBranchAddress("AnalysisMuons_isQuality_Medium_MUON_SAGITTA_RESBIAS__1down", &AnalysisMuons_isQuality_Medium_MUON_SAGITTA_RESBIAS__1down, &b_AnalysisMuons_isQuality_Medium_MUON_SAGITTA_RESBIAS__1down);
   fChain->SetBranchAddress("AnalysisMuons_isQuality_Tight_MUON_SAGITTA_RESBIAS__1down", &AnalysisMuons_isQuality_Tight_MUON_SAGITTA_RESBIAS__1down, &b_AnalysisMuons_isQuality_Tight_MUON_SAGITTA_RESBIAS__1down);
   fChain->SetBranchAddress("AnalysisMuons_isIsolated_PflowTight_VarRad_MUON_SAGITTA_RESBIAS__1down", &AnalysisMuons_isIsolated_PflowTight_VarRad_MUON_SAGITTA_RESBIAS__1down, &b_AnalysisMuons_isIsolated_PflowTight_VarRad_MUON_SAGITTA_RESBIAS__1down);
   fChain->SetBranchAddress("AnalysisMuons_muon_effSF_MUON_SAGITTA_RESBIAS__1down", &AnalysisMuons_muon_effSF_MUON_SAGITTA_RESBIAS__1down, &b_AnalysisMuons_muon_effSF_MUON_SAGITTA_RESBIAS__1down);
   fChain->SetBranchAddress("AnalysisMuons_muon_effSF_TTVA_MUON_SAGITTA_RESBIAS__1down", &AnalysisMuons_muon_effSF_TTVA_MUON_SAGITTA_RESBIAS__1down, &b_AnalysisMuons_muon_effSF_TTVA_MUON_SAGITTA_RESBIAS__1down);
   fChain->SetBranchAddress("AnalysisMuons_muon_effSF_Quality_Loose_MUON_SAGITTA_RESBIAS__1down", &AnalysisMuons_muon_effSF_Quality_Loose_MUON_SAGITTA_RESBIAS__1down, &b_AnalysisMuons_muon_effSF_Quality_Loose_MUON_SAGITTA_RESBIAS__1down);
   fChain->SetBranchAddress("AnalysisMuons_muon_effSF_Quality_Medium_MUON_SAGITTA_RESBIAS__1down", &AnalysisMuons_muon_effSF_Quality_Medium_MUON_SAGITTA_RESBIAS__1down, &b_AnalysisMuons_muon_effSF_Quality_Medium_MUON_SAGITTA_RESBIAS__1down);
   fChain->SetBranchAddress("AnalysisMuons_muon_effSF_Quality_Tight_MUON_SAGITTA_RESBIAS__1down", &AnalysisMuons_muon_effSF_Quality_Tight_MUON_SAGITTA_RESBIAS__1down, &b_AnalysisMuons_muon_effSF_Quality_Tight_MUON_SAGITTA_RESBIAS__1down);
   fChain->SetBranchAddress("AnalysisMuons_muon_effSF_Isol_PflowTight_VarRad_MUON_SAGITTA_RESBIAS__1down", &AnalysisMuons_muon_effSF_Isol_PflowTight_VarRad_MUON_SAGITTA_RESBIAS__1down, &b_AnalysisMuons_muon_effSF_Isol_PflowTight_VarRad_MUON_SAGITTA_RESBIAS__1down);
   fChain->SetBranchAddress("AnalysisMuons_muon_effMC_Trig_Loose_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_SAGITTA_RESBIAS__1down", &AnalysisMuons_muon_effMC_Trig_Loose_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_SAGITTA_RESBIAS__1down, &b_AnalysisMuons_muon_effMC_Trig_Loose_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_SAGITTA_RESBIAS__1down);
   fChain->SetBranchAddress("AnalysisMuons_muon_effData_Trig_Loose_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_SAGITTA_RESBIAS__1down", &AnalysisMuons_muon_effData_Trig_Loose_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_SAGITTA_RESBIAS__1down, &b_AnalysisMuons_muon_effData_Trig_Loose_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_SAGITTA_RESBIAS__1down);
   fChain->SetBranchAddress("AnalysisMuons_muon_effMC_Trig_Loose_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_SAGITTA_RESBIAS__1down", &AnalysisMuons_muon_effMC_Trig_Loose_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_SAGITTA_RESBIAS__1down, &b_AnalysisMuons_muon_effMC_Trig_Loose_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_SAGITTA_RESBIAS__1down);
   fChain->SetBranchAddress("AnalysisMuons_muon_effData_Trig_Loose_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_SAGITTA_RESBIAS__1down", &AnalysisMuons_muon_effData_Trig_Loose_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_SAGITTA_RESBIAS__1down, &b_AnalysisMuons_muon_effData_Trig_Loose_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_SAGITTA_RESBIAS__1down);
   fChain->SetBranchAddress("AnalysisMuons_muon_effMC_Trig_Medium_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_SAGITTA_RESBIAS__1down", &AnalysisMuons_muon_effMC_Trig_Medium_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_SAGITTA_RESBIAS__1down, &b_AnalysisMuons_muon_effMC_Trig_Medium_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_SAGITTA_RESBIAS__1down);
   fChain->SetBranchAddress("AnalysisMuons_muon_effData_Trig_Medium_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_SAGITTA_RESBIAS__1down", &AnalysisMuons_muon_effData_Trig_Medium_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_SAGITTA_RESBIAS__1down, &b_AnalysisMuons_muon_effData_Trig_Medium_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_SAGITTA_RESBIAS__1down);
   fChain->SetBranchAddress("AnalysisMuons_muon_effMC_Trig_Medium_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_SAGITTA_RESBIAS__1down", &AnalysisMuons_muon_effMC_Trig_Medium_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_SAGITTA_RESBIAS__1down, &b_AnalysisMuons_muon_effMC_Trig_Medium_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_SAGITTA_RESBIAS__1down);
   fChain->SetBranchAddress("AnalysisMuons_muon_effData_Trig_Medium_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_SAGITTA_RESBIAS__1down", &AnalysisMuons_muon_effData_Trig_Medium_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_SAGITTA_RESBIAS__1down, &b_AnalysisMuons_muon_effData_Trig_Medium_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_SAGITTA_RESBIAS__1down);
   fChain->SetBranchAddress("AnalysisMuons_muon_effMC_Trig_Tight_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_SAGITTA_RESBIAS__1down", &AnalysisMuons_muon_effMC_Trig_Tight_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_SAGITTA_RESBIAS__1down, &b_AnalysisMuons_muon_effMC_Trig_Tight_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_SAGITTA_RESBIAS__1down);
   fChain->SetBranchAddress("AnalysisMuons_muon_effData_Trig_Tight_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_SAGITTA_RESBIAS__1down", &AnalysisMuons_muon_effData_Trig_Tight_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_SAGITTA_RESBIAS__1down, &b_AnalysisMuons_muon_effData_Trig_Tight_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_SAGITTA_RESBIAS__1down);
   fChain->SetBranchAddress("AnalysisMuons_muon_effMC_Trig_Tight_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_SAGITTA_RESBIAS__1down", &AnalysisMuons_muon_effMC_Trig_Tight_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_SAGITTA_RESBIAS__1down, &b_AnalysisMuons_muon_effMC_Trig_Tight_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_SAGITTA_RESBIAS__1down);
   fChain->SetBranchAddress("AnalysisMuons_muon_effData_Trig_Tight_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_SAGITTA_RESBIAS__1down", &AnalysisMuons_muon_effData_Trig_Tight_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_SAGITTA_RESBIAS__1down, &b_AnalysisMuons_muon_effData_Trig_Tight_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_SAGITTA_RESBIAS__1down);
   fChain->SetBranchAddress("AnalysisJets_jet_selected_MUON_SAGITTA_RESBIAS__1down", &AnalysisJets_jet_selected_MUON_SAGITTA_RESBIAS__1down, &b_AnalysisJets_jet_selected_MUON_SAGITTA_RESBIAS__1down);
   fChain->SetBranchAddress("METInfo_MET_MUON_SAGITTA_RESBIAS__1down", &METInfo_MET_MUON_SAGITTA_RESBIAS__1down, &b_METInfo_MET_MUON_SAGITTA_RESBIAS__1down);
   fChain->SetBranchAddress("METInfo_METPhi_MUON_SAGITTA_RESBIAS__1down", &METInfo_METPhi_MUON_SAGITTA_RESBIAS__1down, &b_METInfo_METPhi_MUON_SAGITTA_RESBIAS__1down);
   fChain->SetBranchAddress("AnalysisElectrons_el_selected_MUON_SAGITTA_RESBIAS__1up", &AnalysisElectrons_el_selected_MUON_SAGITTA_RESBIAS__1up, &b_AnalysisElectrons_el_selected_MUON_SAGITTA_RESBIAS__1up);
   fChain->SetBranchAddress("AnalysisMuons_pt_MUON_SAGITTA_RESBIAS__1up", &AnalysisMuons_pt_MUON_SAGITTA_RESBIAS__1up, &b_AnalysisMuons_pt_MUON_SAGITTA_RESBIAS__1up);
   fChain->SetBranchAddress("AnalysisMuons_mu_selected_MUON_SAGITTA_RESBIAS__1up", &AnalysisMuons_mu_selected_MUON_SAGITTA_RESBIAS__1up, &b_AnalysisMuons_mu_selected_MUON_SAGITTA_RESBIAS__1up);
   fChain->SetBranchAddress("AnalysisMuons_isQuality_Loose_MUON_SAGITTA_RESBIAS__1up", &AnalysisMuons_isQuality_Loose_MUON_SAGITTA_RESBIAS__1up, &b_AnalysisMuons_isQuality_Loose_MUON_SAGITTA_RESBIAS__1up);
   fChain->SetBranchAddress("AnalysisMuons_isQuality_Medium_MUON_SAGITTA_RESBIAS__1up", &AnalysisMuons_isQuality_Medium_MUON_SAGITTA_RESBIAS__1up, &b_AnalysisMuons_isQuality_Medium_MUON_SAGITTA_RESBIAS__1up);
   fChain->SetBranchAddress("AnalysisMuons_isQuality_Tight_MUON_SAGITTA_RESBIAS__1up", &AnalysisMuons_isQuality_Tight_MUON_SAGITTA_RESBIAS__1up, &b_AnalysisMuons_isQuality_Tight_MUON_SAGITTA_RESBIAS__1up);
   fChain->SetBranchAddress("AnalysisMuons_isIsolated_PflowTight_VarRad_MUON_SAGITTA_RESBIAS__1up", &AnalysisMuons_isIsolated_PflowTight_VarRad_MUON_SAGITTA_RESBIAS__1up, &b_AnalysisMuons_isIsolated_PflowTight_VarRad_MUON_SAGITTA_RESBIAS__1up);
   fChain->SetBranchAddress("AnalysisMuons_muon_effSF_MUON_SAGITTA_RESBIAS__1up", &AnalysisMuons_muon_effSF_MUON_SAGITTA_RESBIAS__1up, &b_AnalysisMuons_muon_effSF_MUON_SAGITTA_RESBIAS__1up);
   fChain->SetBranchAddress("AnalysisMuons_muon_effSF_TTVA_MUON_SAGITTA_RESBIAS__1up", &AnalysisMuons_muon_effSF_TTVA_MUON_SAGITTA_RESBIAS__1up, &b_AnalysisMuons_muon_effSF_TTVA_MUON_SAGITTA_RESBIAS__1up);
   fChain->SetBranchAddress("AnalysisMuons_muon_effSF_Quality_Loose_MUON_SAGITTA_RESBIAS__1up", &AnalysisMuons_muon_effSF_Quality_Loose_MUON_SAGITTA_RESBIAS__1up, &b_AnalysisMuons_muon_effSF_Quality_Loose_MUON_SAGITTA_RESBIAS__1up);
   fChain->SetBranchAddress("AnalysisMuons_muon_effSF_Quality_Medium_MUON_SAGITTA_RESBIAS__1up", &AnalysisMuons_muon_effSF_Quality_Medium_MUON_SAGITTA_RESBIAS__1up, &b_AnalysisMuons_muon_effSF_Quality_Medium_MUON_SAGITTA_RESBIAS__1up);
   fChain->SetBranchAddress("AnalysisMuons_muon_effSF_Quality_Tight_MUON_SAGITTA_RESBIAS__1up", &AnalysisMuons_muon_effSF_Quality_Tight_MUON_SAGITTA_RESBIAS__1up, &b_AnalysisMuons_muon_effSF_Quality_Tight_MUON_SAGITTA_RESBIAS__1up);
   fChain->SetBranchAddress("AnalysisMuons_muon_effSF_Isol_PflowTight_VarRad_MUON_SAGITTA_RESBIAS__1up", &AnalysisMuons_muon_effSF_Isol_PflowTight_VarRad_MUON_SAGITTA_RESBIAS__1up, &b_AnalysisMuons_muon_effSF_Isol_PflowTight_VarRad_MUON_SAGITTA_RESBIAS__1up);
   fChain->SetBranchAddress("AnalysisMuons_muon_effMC_Trig_Loose_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_SAGITTA_RESBIAS__1up", &AnalysisMuons_muon_effMC_Trig_Loose_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_SAGITTA_RESBIAS__1up, &b_AnalysisMuons_muon_effMC_Trig_Loose_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_SAGITTA_RESBIAS__1up);
   fChain->SetBranchAddress("AnalysisMuons_muon_effData_Trig_Loose_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_SAGITTA_RESBIAS__1up", &AnalysisMuons_muon_effData_Trig_Loose_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_SAGITTA_RESBIAS__1up, &b_AnalysisMuons_muon_effData_Trig_Loose_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_SAGITTA_RESBIAS__1up);
   fChain->SetBranchAddress("AnalysisMuons_muon_effMC_Trig_Loose_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_SAGITTA_RESBIAS__1up", &AnalysisMuons_muon_effMC_Trig_Loose_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_SAGITTA_RESBIAS__1up, &b_AnalysisMuons_muon_effMC_Trig_Loose_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_SAGITTA_RESBIAS__1up);
   fChain->SetBranchAddress("AnalysisMuons_muon_effData_Trig_Loose_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_SAGITTA_RESBIAS__1up", &AnalysisMuons_muon_effData_Trig_Loose_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_SAGITTA_RESBIAS__1up, &b_AnalysisMuons_muon_effData_Trig_Loose_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_SAGITTA_RESBIAS__1up);
   fChain->SetBranchAddress("AnalysisMuons_muon_effMC_Trig_Medium_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_SAGITTA_RESBIAS__1up", &AnalysisMuons_muon_effMC_Trig_Medium_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_SAGITTA_RESBIAS__1up, &b_AnalysisMuons_muon_effMC_Trig_Medium_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_SAGITTA_RESBIAS__1up);
   fChain->SetBranchAddress("AnalysisMuons_muon_effData_Trig_Medium_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_SAGITTA_RESBIAS__1up", &AnalysisMuons_muon_effData_Trig_Medium_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_SAGITTA_RESBIAS__1up, &b_AnalysisMuons_muon_effData_Trig_Medium_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_SAGITTA_RESBIAS__1up);
   fChain->SetBranchAddress("AnalysisMuons_muon_effMC_Trig_Medium_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_SAGITTA_RESBIAS__1up", &AnalysisMuons_muon_effMC_Trig_Medium_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_SAGITTA_RESBIAS__1up, &b_AnalysisMuons_muon_effMC_Trig_Medium_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_SAGITTA_RESBIAS__1up);
   fChain->SetBranchAddress("AnalysisMuons_muon_effData_Trig_Medium_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_SAGITTA_RESBIAS__1up", &AnalysisMuons_muon_effData_Trig_Medium_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_SAGITTA_RESBIAS__1up, &b_AnalysisMuons_muon_effData_Trig_Medium_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_SAGITTA_RESBIAS__1up);
   fChain->SetBranchAddress("AnalysisMuons_muon_effMC_Trig_Tight_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_SAGITTA_RESBIAS__1up", &AnalysisMuons_muon_effMC_Trig_Tight_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_SAGITTA_RESBIAS__1up, &b_AnalysisMuons_muon_effMC_Trig_Tight_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_SAGITTA_RESBIAS__1up);
   fChain->SetBranchAddress("AnalysisMuons_muon_effData_Trig_Tight_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_SAGITTA_RESBIAS__1up", &AnalysisMuons_muon_effData_Trig_Tight_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_SAGITTA_RESBIAS__1up, &b_AnalysisMuons_muon_effData_Trig_Tight_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_SAGITTA_RESBIAS__1up);
   fChain->SetBranchAddress("AnalysisMuons_muon_effMC_Trig_Tight_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_SAGITTA_RESBIAS__1up", &AnalysisMuons_muon_effMC_Trig_Tight_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_SAGITTA_RESBIAS__1up, &b_AnalysisMuons_muon_effMC_Trig_Tight_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_SAGITTA_RESBIAS__1up);
   fChain->SetBranchAddress("AnalysisMuons_muon_effData_Trig_Tight_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_SAGITTA_RESBIAS__1up", &AnalysisMuons_muon_effData_Trig_Tight_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_SAGITTA_RESBIAS__1up, &b_AnalysisMuons_muon_effData_Trig_Tight_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_SAGITTA_RESBIAS__1up);
   fChain->SetBranchAddress("AnalysisJets_jet_selected_MUON_SAGITTA_RESBIAS__1up", &AnalysisJets_jet_selected_MUON_SAGITTA_RESBIAS__1up, &b_AnalysisJets_jet_selected_MUON_SAGITTA_RESBIAS__1up);
   fChain->SetBranchAddress("METInfo_MET_MUON_SAGITTA_RESBIAS__1up", &METInfo_MET_MUON_SAGITTA_RESBIAS__1up, &b_METInfo_MET_MUON_SAGITTA_RESBIAS__1up);
   fChain->SetBranchAddress("METInfo_METPhi_MUON_SAGITTA_RESBIAS__1up", &METInfo_METPhi_MUON_SAGITTA_RESBIAS__1up, &b_METInfo_METPhi_MUON_SAGITTA_RESBIAS__1up);
   fChain->SetBranchAddress("AnalysisElectrons_el_selected_MUON_SCALE__1down", &AnalysisElectrons_el_selected_MUON_SCALE__1down, &b_AnalysisElectrons_el_selected_MUON_SCALE__1down);
   fChain->SetBranchAddress("AnalysisMuons_pt_MUON_SCALE__1down", &AnalysisMuons_pt_MUON_SCALE__1down, &b_AnalysisMuons_pt_MUON_SCALE__1down);
   fChain->SetBranchAddress("AnalysisMuons_mu_selected_MUON_SCALE__1down", &AnalysisMuons_mu_selected_MUON_SCALE__1down, &b_AnalysisMuons_mu_selected_MUON_SCALE__1down);
   fChain->SetBranchAddress("AnalysisMuons_isQuality_Loose_MUON_SCALE__1down", &AnalysisMuons_isQuality_Loose_MUON_SCALE__1down, &b_AnalysisMuons_isQuality_Loose_MUON_SCALE__1down);
   fChain->SetBranchAddress("AnalysisMuons_isQuality_Medium_MUON_SCALE__1down", &AnalysisMuons_isQuality_Medium_MUON_SCALE__1down, &b_AnalysisMuons_isQuality_Medium_MUON_SCALE__1down);
   fChain->SetBranchAddress("AnalysisMuons_isQuality_Tight_MUON_SCALE__1down", &AnalysisMuons_isQuality_Tight_MUON_SCALE__1down, &b_AnalysisMuons_isQuality_Tight_MUON_SCALE__1down);
   fChain->SetBranchAddress("AnalysisMuons_isIsolated_PflowTight_VarRad_MUON_SCALE__1down", &AnalysisMuons_isIsolated_PflowTight_VarRad_MUON_SCALE__1down, &b_AnalysisMuons_isIsolated_PflowTight_VarRad_MUON_SCALE__1down);
   fChain->SetBranchAddress("AnalysisMuons_muon_effSF_MUON_SCALE__1down", &AnalysisMuons_muon_effSF_MUON_SCALE__1down, &b_AnalysisMuons_muon_effSF_MUON_SCALE__1down);
   fChain->SetBranchAddress("AnalysisMuons_muon_effSF_TTVA_MUON_SCALE__1down", &AnalysisMuons_muon_effSF_TTVA_MUON_SCALE__1down, &b_AnalysisMuons_muon_effSF_TTVA_MUON_SCALE__1down);
   fChain->SetBranchAddress("AnalysisMuons_muon_effSF_Quality_Loose_MUON_SCALE__1down", &AnalysisMuons_muon_effSF_Quality_Loose_MUON_SCALE__1down, &b_AnalysisMuons_muon_effSF_Quality_Loose_MUON_SCALE__1down);
   fChain->SetBranchAddress("AnalysisMuons_muon_effSF_Quality_Medium_MUON_SCALE__1down", &AnalysisMuons_muon_effSF_Quality_Medium_MUON_SCALE__1down, &b_AnalysisMuons_muon_effSF_Quality_Medium_MUON_SCALE__1down);
   fChain->SetBranchAddress("AnalysisMuons_muon_effSF_Quality_Tight_MUON_SCALE__1down", &AnalysisMuons_muon_effSF_Quality_Tight_MUON_SCALE__1down, &b_AnalysisMuons_muon_effSF_Quality_Tight_MUON_SCALE__1down);
   fChain->SetBranchAddress("AnalysisMuons_muon_effSF_Isol_PflowTight_VarRad_MUON_SCALE__1down", &AnalysisMuons_muon_effSF_Isol_PflowTight_VarRad_MUON_SCALE__1down, &b_AnalysisMuons_muon_effSF_Isol_PflowTight_VarRad_MUON_SCALE__1down);
   fChain->SetBranchAddress("AnalysisMuons_muon_effMC_Trig_Loose_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_SCALE__1down", &AnalysisMuons_muon_effMC_Trig_Loose_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_SCALE__1down, &b_AnalysisMuons_muon_effMC_Trig_Loose_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_SCALE__1down);
   fChain->SetBranchAddress("AnalysisMuons_muon_effData_Trig_Loose_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_SCALE__1down", &AnalysisMuons_muon_effData_Trig_Loose_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_SCALE__1down, &b_AnalysisMuons_muon_effData_Trig_Loose_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_SCALE__1down);
   fChain->SetBranchAddress("AnalysisMuons_muon_effMC_Trig_Loose_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_SCALE__1down", &AnalysisMuons_muon_effMC_Trig_Loose_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_SCALE__1down, &b_AnalysisMuons_muon_effMC_Trig_Loose_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_SCALE__1down);
   fChain->SetBranchAddress("AnalysisMuons_muon_effData_Trig_Loose_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_SCALE__1down", &AnalysisMuons_muon_effData_Trig_Loose_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_SCALE__1down, &b_AnalysisMuons_muon_effData_Trig_Loose_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_SCALE__1down);
   fChain->SetBranchAddress("AnalysisMuons_muon_effMC_Trig_Medium_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_SCALE__1down", &AnalysisMuons_muon_effMC_Trig_Medium_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_SCALE__1down, &b_AnalysisMuons_muon_effMC_Trig_Medium_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_SCALE__1down);
   fChain->SetBranchAddress("AnalysisMuons_muon_effData_Trig_Medium_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_SCALE__1down", &AnalysisMuons_muon_effData_Trig_Medium_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_SCALE__1down, &b_AnalysisMuons_muon_effData_Trig_Medium_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_SCALE__1down);
   fChain->SetBranchAddress("AnalysisMuons_muon_effMC_Trig_Medium_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_SCALE__1down", &AnalysisMuons_muon_effMC_Trig_Medium_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_SCALE__1down, &b_AnalysisMuons_muon_effMC_Trig_Medium_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_SCALE__1down);
   fChain->SetBranchAddress("AnalysisMuons_muon_effData_Trig_Medium_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_SCALE__1down", &AnalysisMuons_muon_effData_Trig_Medium_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_SCALE__1down, &b_AnalysisMuons_muon_effData_Trig_Medium_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_SCALE__1down);
   fChain->SetBranchAddress("AnalysisMuons_muon_effMC_Trig_Tight_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_SCALE__1down", &AnalysisMuons_muon_effMC_Trig_Tight_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_SCALE__1down, &b_AnalysisMuons_muon_effMC_Trig_Tight_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_SCALE__1down);
   fChain->SetBranchAddress("AnalysisMuons_muon_effData_Trig_Tight_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_SCALE__1down", &AnalysisMuons_muon_effData_Trig_Tight_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_SCALE__1down, &b_AnalysisMuons_muon_effData_Trig_Tight_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_SCALE__1down);
   fChain->SetBranchAddress("AnalysisMuons_muon_effMC_Trig_Tight_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_SCALE__1down", &AnalysisMuons_muon_effMC_Trig_Tight_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_SCALE__1down, &b_AnalysisMuons_muon_effMC_Trig_Tight_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_SCALE__1down);
   fChain->SetBranchAddress("AnalysisMuons_muon_effData_Trig_Tight_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_SCALE__1down", &AnalysisMuons_muon_effData_Trig_Tight_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_SCALE__1down, &b_AnalysisMuons_muon_effData_Trig_Tight_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_SCALE__1down);
   fChain->SetBranchAddress("AnalysisJets_jet_selected_MUON_SCALE__1down", &AnalysisJets_jet_selected_MUON_SCALE__1down, &b_AnalysisJets_jet_selected_MUON_SCALE__1down);
   fChain->SetBranchAddress("METInfo_MET_MUON_SCALE__1down", &METInfo_MET_MUON_SCALE__1down, &b_METInfo_MET_MUON_SCALE__1down);
   fChain->SetBranchAddress("METInfo_METPhi_MUON_SCALE__1down", &METInfo_METPhi_MUON_SCALE__1down, &b_METInfo_METPhi_MUON_SCALE__1down);
   fChain->SetBranchAddress("AnalysisElectrons_el_selected_MUON_SCALE__1up", &AnalysisElectrons_el_selected_MUON_SCALE__1up, &b_AnalysisElectrons_el_selected_MUON_SCALE__1up);
   fChain->SetBranchAddress("AnalysisMuons_pt_MUON_SCALE__1up", &AnalysisMuons_pt_MUON_SCALE__1up, &b_AnalysisMuons_pt_MUON_SCALE__1up);
   fChain->SetBranchAddress("AnalysisMuons_mu_selected_MUON_SCALE__1up", &AnalysisMuons_mu_selected_MUON_SCALE__1up, &b_AnalysisMuons_mu_selected_MUON_SCALE__1up);
   fChain->SetBranchAddress("AnalysisMuons_isQuality_Loose_MUON_SCALE__1up", &AnalysisMuons_isQuality_Loose_MUON_SCALE__1up, &b_AnalysisMuons_isQuality_Loose_MUON_SCALE__1up);
   fChain->SetBranchAddress("AnalysisMuons_isQuality_Medium_MUON_SCALE__1up", &AnalysisMuons_isQuality_Medium_MUON_SCALE__1up, &b_AnalysisMuons_isQuality_Medium_MUON_SCALE__1up);
   fChain->SetBranchAddress("AnalysisMuons_isQuality_Tight_MUON_SCALE__1up", &AnalysisMuons_isQuality_Tight_MUON_SCALE__1up, &b_AnalysisMuons_isQuality_Tight_MUON_SCALE__1up);
   fChain->SetBranchAddress("AnalysisMuons_isIsolated_PflowTight_VarRad_MUON_SCALE__1up", &AnalysisMuons_isIsolated_PflowTight_VarRad_MUON_SCALE__1up, &b_AnalysisMuons_isIsolated_PflowTight_VarRad_MUON_SCALE__1up);
   fChain->SetBranchAddress("AnalysisMuons_muon_effSF_MUON_SCALE__1up", &AnalysisMuons_muon_effSF_MUON_SCALE__1up, &b_AnalysisMuons_muon_effSF_MUON_SCALE__1up);
   fChain->SetBranchAddress("AnalysisMuons_muon_effSF_TTVA_MUON_SCALE__1up", &AnalysisMuons_muon_effSF_TTVA_MUON_SCALE__1up, &b_AnalysisMuons_muon_effSF_TTVA_MUON_SCALE__1up);
   fChain->SetBranchAddress("AnalysisMuons_muon_effSF_Quality_Loose_MUON_SCALE__1up", &AnalysisMuons_muon_effSF_Quality_Loose_MUON_SCALE__1up, &b_AnalysisMuons_muon_effSF_Quality_Loose_MUON_SCALE__1up);
   fChain->SetBranchAddress("AnalysisMuons_muon_effSF_Quality_Medium_MUON_SCALE__1up", &AnalysisMuons_muon_effSF_Quality_Medium_MUON_SCALE__1up, &b_AnalysisMuons_muon_effSF_Quality_Medium_MUON_SCALE__1up);
   fChain->SetBranchAddress("AnalysisMuons_muon_effSF_Quality_Tight_MUON_SCALE__1up", &AnalysisMuons_muon_effSF_Quality_Tight_MUON_SCALE__1up, &b_AnalysisMuons_muon_effSF_Quality_Tight_MUON_SCALE__1up);
   fChain->SetBranchAddress("AnalysisMuons_muon_effSF_Isol_PflowTight_VarRad_MUON_SCALE__1up", &AnalysisMuons_muon_effSF_Isol_PflowTight_VarRad_MUON_SCALE__1up, &b_AnalysisMuons_muon_effSF_Isol_PflowTight_VarRad_MUON_SCALE__1up);
   fChain->SetBranchAddress("AnalysisMuons_muon_effMC_Trig_Loose_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_SCALE__1up", &AnalysisMuons_muon_effMC_Trig_Loose_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_SCALE__1up, &b_AnalysisMuons_muon_effMC_Trig_Loose_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_SCALE__1up);
   fChain->SetBranchAddress("AnalysisMuons_muon_effData_Trig_Loose_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_SCALE__1up", &AnalysisMuons_muon_effData_Trig_Loose_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_SCALE__1up, &b_AnalysisMuons_muon_effData_Trig_Loose_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_SCALE__1up);
   fChain->SetBranchAddress("AnalysisMuons_muon_effMC_Trig_Loose_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_SCALE__1up", &AnalysisMuons_muon_effMC_Trig_Loose_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_SCALE__1up, &b_AnalysisMuons_muon_effMC_Trig_Loose_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_SCALE__1up);
   fChain->SetBranchAddress("AnalysisMuons_muon_effData_Trig_Loose_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_SCALE__1up", &AnalysisMuons_muon_effData_Trig_Loose_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_SCALE__1up, &b_AnalysisMuons_muon_effData_Trig_Loose_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_SCALE__1up);
   fChain->SetBranchAddress("AnalysisMuons_muon_effMC_Trig_Medium_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_SCALE__1up", &AnalysisMuons_muon_effMC_Trig_Medium_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_SCALE__1up, &b_AnalysisMuons_muon_effMC_Trig_Medium_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_SCALE__1up);
   fChain->SetBranchAddress("AnalysisMuons_muon_effData_Trig_Medium_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_SCALE__1up", &AnalysisMuons_muon_effData_Trig_Medium_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_SCALE__1up, &b_AnalysisMuons_muon_effData_Trig_Medium_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_SCALE__1up);
   fChain->SetBranchAddress("AnalysisMuons_muon_effMC_Trig_Medium_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_SCALE__1up", &AnalysisMuons_muon_effMC_Trig_Medium_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_SCALE__1up, &b_AnalysisMuons_muon_effMC_Trig_Medium_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_SCALE__1up);
   fChain->SetBranchAddress("AnalysisMuons_muon_effData_Trig_Medium_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_SCALE__1up", &AnalysisMuons_muon_effData_Trig_Medium_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_SCALE__1up, &b_AnalysisMuons_muon_effData_Trig_Medium_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_SCALE__1up);
   fChain->SetBranchAddress("AnalysisMuons_muon_effMC_Trig_Tight_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_SCALE__1up", &AnalysisMuons_muon_effMC_Trig_Tight_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_SCALE__1up, &b_AnalysisMuons_muon_effMC_Trig_Tight_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_SCALE__1up);
   fChain->SetBranchAddress("AnalysisMuons_muon_effData_Trig_Tight_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_SCALE__1up", &AnalysisMuons_muon_effData_Trig_Tight_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_SCALE__1up, &b_AnalysisMuons_muon_effData_Trig_Tight_HLT_mu26_ivarmedium_OR_HLT_mu50_MUON_SCALE__1up);
   fChain->SetBranchAddress("AnalysisMuons_muon_effMC_Trig_Tight_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_SCALE__1up", &AnalysisMuons_muon_effMC_Trig_Tight_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_SCALE__1up, &b_AnalysisMuons_muon_effMC_Trig_Tight_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_SCALE__1up);
   fChain->SetBranchAddress("AnalysisMuons_muon_effData_Trig_Tight_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_SCALE__1up", &AnalysisMuons_muon_effData_Trig_Tight_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_SCALE__1up, &b_AnalysisMuons_muon_effData_Trig_Tight_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_MUON_SCALE__1up);
   fChain->SetBranchAddress("AnalysisJets_jet_selected_MUON_SCALE__1up", &AnalysisJets_jet_selected_MUON_SCALE__1up, &b_AnalysisJets_jet_selected_MUON_SCALE__1up);
   fChain->SetBranchAddress("METInfo_MET_MUON_SCALE__1up", &METInfo_MET_MUON_SCALE__1up, &b_METInfo_MET_MUON_SCALE__1up);
   fChain->SetBranchAddress("METInfo_METPhi_MUON_SCALE__1up", &METInfo_METPhi_MUON_SCALE__1up, &b_METInfo_METPhi_MUON_SCALE__1up);
   fChain->SetBranchAddress("EventInfo_prodFracWeight_PROD_FRAC_BOTTOM_EIG_1", &EventInfo_prodFracWeight_PROD_FRAC_BOTTOM_EIG_1, &b_EventInfo_prodFracWeight_PROD_FRAC_BOTTOM_EIG_1);
   fChain->SetBranchAddress("EventInfo_prodFracWeight_PROD_FRAC_BOTTOM_EIG_2", &EventInfo_prodFracWeight_PROD_FRAC_BOTTOM_EIG_2, &b_EventInfo_prodFracWeight_PROD_FRAC_BOTTOM_EIG_2);
   fChain->SetBranchAddress("EventInfo_prodFracWeight_PROD_FRAC_CHARM_EIG_1", &EventInfo_prodFracWeight_PROD_FRAC_CHARM_EIG_1, &b_EventInfo_prodFracWeight_PROD_FRAC_CHARM_EIG_1);
   fChain->SetBranchAddress("EventInfo_prodFracWeight_PROD_FRAC_CHARM_EIG_2", &EventInfo_prodFracWeight_PROD_FRAC_CHARM_EIG_2, &b_EventInfo_prodFracWeight_PROD_FRAC_CHARM_EIG_2);
   fChain->SetBranchAddress("EventInfo_prodFracWeight_PROD_FRAC_CHARM_EIG_3", &EventInfo_prodFracWeight_PROD_FRAC_CHARM_EIG_3, &b_EventInfo_prodFracWeight_PROD_FRAC_CHARM_EIG_3);
   fChain->SetBranchAddress("EventInfo_PileupWeight_PRW_DATASF__1down", &EventInfo_PileupWeight_PRW_DATASF__1down, &b_EventInfo_PileupWeight_PRW_DATASF__1down);
   fChain->SetBranchAddress("EventInfo_PileupWeight_PRW_DATASF__1up", &EventInfo_PileupWeight_PRW_DATASF__1up, &b_EventInfo_PileupWeight_PRW_DATASF__1up);
   fChain->SetBranchAddress("DMesons_costhetastar_TRK_BIAS_D0_WM", &DMesons_costhetastar_TRK_BIAS_D0_WM, &b_DMesons_costhetastar_TRK_BIAS_D0_WM);
   fChain->SetBranchAddress("DMesons_D0Index_TRK_BIAS_D0_WM", &DMesons_D0Index_TRK_BIAS_D0_WM, &b_DMesons_D0Index_TRK_BIAS_D0_WM);
   fChain->SetBranchAddress("DMesons_daughterInfo__eta_TRK_BIAS_D0_WM", &DMesons_daughterInfo__eta_TRK_BIAS_D0_WM, &b_DMesons_daughterInfo__eta_TRK_BIAS_D0_WM);
   fChain->SetBranchAddress("DMesons_daughterInfo__passTight_TRK_BIAS_D0_WM", &DMesons_daughterInfo__passTight_TRK_BIAS_D0_WM, &b_DMesons_daughterInfo__passTight_TRK_BIAS_D0_WM);
   fChain->SetBranchAddress("DMesons_daughterInfo__pdgId_TRK_BIAS_D0_WM", &DMesons_daughterInfo__pdgId_TRK_BIAS_D0_WM, &b_DMesons_daughterInfo__pdgId_TRK_BIAS_D0_WM);
   fChain->SetBranchAddress("DMesons_daughterInfo__phi_TRK_BIAS_D0_WM", &DMesons_daughterInfo__phi_TRK_BIAS_D0_WM, &b_DMesons_daughterInfo__phi_TRK_BIAS_D0_WM);
   fChain->SetBranchAddress("DMesons_daughterInfo__pt_TRK_BIAS_D0_WM", &DMesons_daughterInfo__pt_TRK_BIAS_D0_WM, &b_DMesons_daughterInfo__pt_TRK_BIAS_D0_WM);
   fChain->SetBranchAddress("DMesons_daughterInfo__trackId_TRK_BIAS_D0_WM", &DMesons_daughterInfo__trackId_TRK_BIAS_D0_WM, &b_DMesons_daughterInfo__trackId_TRK_BIAS_D0_WM);
   fChain->SetBranchAddress("DMesons_daughterInfo__truthBarcode_TRK_BIAS_D0_WM", &DMesons_daughterInfo__truthBarcode_TRK_BIAS_D0_WM, &b_DMesons_daughterInfo__truthBarcode_TRK_BIAS_D0_WM);
   fChain->SetBranchAddress("DMesons_daughterInfo__truthDBarcode_TRK_BIAS_D0_WM", &DMesons_daughterInfo__truthDBarcode_TRK_BIAS_D0_WM, &b_DMesons_daughterInfo__truthDBarcode_TRK_BIAS_D0_WM);
   fChain->SetBranchAddress("DMesons_daughterInfo__z0SinTheta_TRK_BIAS_D0_WM", &DMesons_daughterInfo__z0SinTheta_TRK_BIAS_D0_WM, &b_DMesons_daughterInfo__z0SinTheta_TRK_BIAS_D0_WM);
   fChain->SetBranchAddress("DMesons_daughterInfo__z0SinThetaPV_TRK_BIAS_D0_WM", &DMesons_daughterInfo__z0SinThetaPV_TRK_BIAS_D0_WM, &b_DMesons_daughterInfo__z0SinThetaPV_TRK_BIAS_D0_WM);
   fChain->SetBranchAddress("DMesons_decayType_TRK_BIAS_D0_WM", &DMesons_decayType_TRK_BIAS_D0_WM, &b_DMesons_decayType_TRK_BIAS_D0_WM);
   fChain->SetBranchAddress("DMesons_DeltaMass_TRK_BIAS_D0_WM", &DMesons_DeltaMass_TRK_BIAS_D0_WM, &b_DMesons_DeltaMass_TRK_BIAS_D0_WM);
   fChain->SetBranchAddress("DMesons_eta_TRK_BIAS_D0_WM", &DMesons_eta_TRK_BIAS_D0_WM, &b_DMesons_eta_TRK_BIAS_D0_WM);
   fChain->SetBranchAddress("DMesons_fitOutput__Charge_TRK_BIAS_D0_WM", &DMesons_fitOutput__Charge_TRK_BIAS_D0_WM, &b_DMesons_fitOutput__Charge_TRK_BIAS_D0_WM);
   fChain->SetBranchAddress("DMesons_fitOutput__Chi2_TRK_BIAS_D0_WM", &DMesons_fitOutput__Chi2_TRK_BIAS_D0_WM, &b_DMesons_fitOutput__Chi2_TRK_BIAS_D0_WM);
   fChain->SetBranchAddress("DMesons_fitOutput__Impact_TRK_BIAS_D0_WM", &DMesons_fitOutput__Impact_TRK_BIAS_D0_WM, &b_DMesons_fitOutput__Impact_TRK_BIAS_D0_WM);
   fChain->SetBranchAddress("DMesons_fitOutput__ImpactSignificance_TRK_BIAS_D0_WM", &DMesons_fitOutput__ImpactSignificance_TRK_BIAS_D0_WM, &b_DMesons_fitOutput__ImpactSignificance_TRK_BIAS_D0_WM);
   fChain->SetBranchAddress("DMesons_fitOutput__ImpactZ0SinTheta_TRK_BIAS_D0_WM", &DMesons_fitOutput__ImpactZ0SinTheta_TRK_BIAS_D0_WM, &b_DMesons_fitOutput__ImpactZ0SinTheta_TRK_BIAS_D0_WM);
   fChain->SetBranchAddress("DMesons_fitOutput__Lxy_TRK_BIAS_D0_WM", &DMesons_fitOutput__Lxy_TRK_BIAS_D0_WM, &b_DMesons_fitOutput__Lxy_TRK_BIAS_D0_WM);
   fChain->SetBranchAddress("DMesons_fitOutput__VertexPosition_TRK_BIAS_D0_WM", &DMesons_fitOutput__VertexPosition_TRK_BIAS_D0_WM, &b_DMesons_fitOutput__VertexPosition_TRK_BIAS_D0_WM);
   fChain->SetBranchAddress("DMesons_m_TRK_BIAS_D0_WM", &DMesons_m_TRK_BIAS_D0_WM, &b_DMesons_m_TRK_BIAS_D0_WM);
   fChain->SetBranchAddress("DMesons_mKpi1_TRK_BIAS_D0_WM", &DMesons_mKpi1_TRK_BIAS_D0_WM, &b_DMesons_mKpi1_TRK_BIAS_D0_WM);
   fChain->SetBranchAddress("DMesons_mKpi2_TRK_BIAS_D0_WM", &DMesons_mKpi2_TRK_BIAS_D0_WM, &b_DMesons_mKpi2_TRK_BIAS_D0_WM);
   fChain->SetBranchAddress("DMesons_mPhi1_TRK_BIAS_D0_WM", &DMesons_mPhi1_TRK_BIAS_D0_WM, &b_DMesons_mPhi1_TRK_BIAS_D0_WM);
   fChain->SetBranchAddress("DMesons_mPhi2_TRK_BIAS_D0_WM", &DMesons_mPhi2_TRK_BIAS_D0_WM, &b_DMesons_mPhi2_TRK_BIAS_D0_WM);
   fChain->SetBranchAddress("DMesons_pdgId_TRK_BIAS_D0_WM", &DMesons_pdgId_TRK_BIAS_D0_WM, &b_DMesons_pdgId_TRK_BIAS_D0_WM);
   fChain->SetBranchAddress("DMesons_phi_TRK_BIAS_D0_WM", &DMesons_phi_TRK_BIAS_D0_WM, &b_DMesons_phi_TRK_BIAS_D0_WM);
   fChain->SetBranchAddress("DMesons_pt_TRK_BIAS_D0_WM", &DMesons_pt_TRK_BIAS_D0_WM, &b_DMesons_pt_TRK_BIAS_D0_WM);
   fChain->SetBranchAddress("DMesons_ptcone40_TRK_BIAS_D0_WM", &DMesons_ptcone40_TRK_BIAS_D0_WM, &b_DMesons_ptcone40_TRK_BIAS_D0_WM);
   fChain->SetBranchAddress("DMesons_SlowPionD0_TRK_BIAS_D0_WM", &DMesons_SlowPionD0_TRK_BIAS_D0_WM, &b_DMesons_SlowPionD0_TRK_BIAS_D0_WM);
   fChain->SetBranchAddress("DMesons_SlowPionZ0SinTheta_TRK_BIAS_D0_WM", &DMesons_SlowPionZ0SinTheta_TRK_BIAS_D0_WM, &b_DMesons_SlowPionZ0SinTheta_TRK_BIAS_D0_WM);
   fChain->SetBranchAddress("DMesons_truthBarcode_TRK_BIAS_D0_WM", &DMesons_truthBarcode_TRK_BIAS_D0_WM, &b_DMesons_truthBarcode_TRK_BIAS_D0_WM);
   fChain->SetBranchAddress("MesonTracks_d0sig_TRK_BIAS_D0_WM", &MesonTracks_d0sig_TRK_BIAS_D0_WM, &b_MesonTracks_d0sig_TRK_BIAS_D0_WM);
   fChain->SetBranchAddress("MesonTracks_d0sigPV_TRK_BIAS_D0_WM", &MesonTracks_d0sigPV_TRK_BIAS_D0_WM, &b_MesonTracks_d0sigPV_TRK_BIAS_D0_WM);
   fChain->SetBranchAddress("MesonTracks_eta_TRK_BIAS_D0_WM", &MesonTracks_eta_TRK_BIAS_D0_WM, &b_MesonTracks_eta_TRK_BIAS_D0_WM);
   fChain->SetBranchAddress("MesonTracks_passTight_TRK_BIAS_D0_WM", &MesonTracks_passTight_TRK_BIAS_D0_WM, &b_MesonTracks_passTight_TRK_BIAS_D0_WM);
   fChain->SetBranchAddress("MesonTracks_phi_TRK_BIAS_D0_WM", &MesonTracks_phi_TRK_BIAS_D0_WM, &b_MesonTracks_phi_TRK_BIAS_D0_WM);
   fChain->SetBranchAddress("MesonTracks_pt_TRK_BIAS_D0_WM", &MesonTracks_pt_TRK_BIAS_D0_WM, &b_MesonTracks_pt_TRK_BIAS_D0_WM);
   fChain->SetBranchAddress("MesonTracks_trackId_TRK_BIAS_D0_WM", &MesonTracks_trackId_TRK_BIAS_D0_WM, &b_MesonTracks_trackId_TRK_BIAS_D0_WM);
   fChain->SetBranchAddress("MesonTracks_z0sinTheta_TRK_BIAS_D0_WM", &MesonTracks_z0sinTheta_TRK_BIAS_D0_WM, &b_MesonTracks_z0sinTheta_TRK_BIAS_D0_WM);
   fChain->SetBranchAddress("MesonTracks_truthMatchProbability_TRK_BIAS_D0_WM", &MesonTracks_truthMatchProbability_TRK_BIAS_D0_WM, &b_MesonTracks_truthMatchProbability_TRK_BIAS_D0_WM);
   fChain->SetBranchAddress("AntiKt6PV0TrackJets_pt_TRK_BIAS_D0_WM", &AntiKt6PV0TrackJets_pt_TRK_BIAS_D0_WM, &b_AntiKt6PV0TrackJets_pt_TRK_BIAS_D0_WM);
   fChain->SetBranchAddress("AntiKt6PV0TrackJets_eta_TRK_BIAS_D0_WM", &AntiKt6PV0TrackJets_eta_TRK_BIAS_D0_WM, &b_AntiKt6PV0TrackJets_eta_TRK_BIAS_D0_WM);
   fChain->SetBranchAddress("AntiKt6PV0TrackJets_phi_TRK_BIAS_D0_WM", &AntiKt6PV0TrackJets_phi_TRK_BIAS_D0_WM, &b_AntiKt6PV0TrackJets_phi_TRK_BIAS_D0_WM);
   fChain->SetBranchAddress("AntiKt6PV0TrackJets_m_TRK_BIAS_D0_WM", &AntiKt6PV0TrackJets_m_TRK_BIAS_D0_WM, &b_AntiKt6PV0TrackJets_m_TRK_BIAS_D0_WM);
   fChain->SetBranchAddress("AntiKt6PV0TrackJets_daughter__pt_TRK_BIAS_D0_WM", &AntiKt6PV0TrackJets_daughter__pt_TRK_BIAS_D0_WM, &b_AntiKt6PV0TrackJets_daughter__pt_TRK_BIAS_D0_WM);
   fChain->SetBranchAddress("AntiKt6PV0TrackJets_daughter__eta_TRK_BIAS_D0_WM", &AntiKt6PV0TrackJets_daughter__eta_TRK_BIAS_D0_WM, &b_AntiKt6PV0TrackJets_daughter__eta_TRK_BIAS_D0_WM);
   fChain->SetBranchAddress("AntiKt6PV0TrackJets_daughter__phi_TRK_BIAS_D0_WM", &AntiKt6PV0TrackJets_daughter__phi_TRK_BIAS_D0_WM, &b_AntiKt6PV0TrackJets_daughter__phi_TRK_BIAS_D0_WM);
   fChain->SetBranchAddress("AntiKt6PV0TrackJets_daughter__trackId_TRK_BIAS_D0_WM", &AntiKt6PV0TrackJets_daughter__trackId_TRK_BIAS_D0_WM, &b_AntiKt6PV0TrackJets_daughter__trackId_TRK_BIAS_D0_WM);
   fChain->SetBranchAddress("AntiKt8PV0TrackJets_pt_TRK_BIAS_D0_WM", &AntiKt8PV0TrackJets_pt_TRK_BIAS_D0_WM, &b_AntiKt8PV0TrackJets_pt_TRK_BIAS_D0_WM);
   fChain->SetBranchAddress("AntiKt8PV0TrackJets_eta_TRK_BIAS_D0_WM", &AntiKt8PV0TrackJets_eta_TRK_BIAS_D0_WM, &b_AntiKt8PV0TrackJets_eta_TRK_BIAS_D0_WM);
   fChain->SetBranchAddress("AntiKt8PV0TrackJets_phi_TRK_BIAS_D0_WM", &AntiKt8PV0TrackJets_phi_TRK_BIAS_D0_WM, &b_AntiKt8PV0TrackJets_phi_TRK_BIAS_D0_WM);
   fChain->SetBranchAddress("AntiKt8PV0TrackJets_m_TRK_BIAS_D0_WM", &AntiKt8PV0TrackJets_m_TRK_BIAS_D0_WM, &b_AntiKt8PV0TrackJets_m_TRK_BIAS_D0_WM);
   fChain->SetBranchAddress("AntiKt8PV0TrackJets_daughter__pt_TRK_BIAS_D0_WM", &AntiKt8PV0TrackJets_daughter__pt_TRK_BIAS_D0_WM, &b_AntiKt8PV0TrackJets_daughter__pt_TRK_BIAS_D0_WM);
   fChain->SetBranchAddress("AntiKt8PV0TrackJets_daughter__eta_TRK_BIAS_D0_WM", &AntiKt8PV0TrackJets_daughter__eta_TRK_BIAS_D0_WM, &b_AntiKt8PV0TrackJets_daughter__eta_TRK_BIAS_D0_WM);
   fChain->SetBranchAddress("AntiKt8PV0TrackJets_daughter__phi_TRK_BIAS_D0_WM", &AntiKt8PV0TrackJets_daughter__phi_TRK_BIAS_D0_WM, &b_AntiKt8PV0TrackJets_daughter__phi_TRK_BIAS_D0_WM);
   fChain->SetBranchAddress("AntiKt8PV0TrackJets_daughter__trackId_TRK_BIAS_D0_WM", &AntiKt8PV0TrackJets_daughter__trackId_TRK_BIAS_D0_WM, &b_AntiKt8PV0TrackJets_daughter__trackId_TRK_BIAS_D0_WM);
   fChain->SetBranchAddress("AntiKt10PV0TrackJets_pt_TRK_BIAS_D0_WM", &AntiKt10PV0TrackJets_pt_TRK_BIAS_D0_WM, &b_AntiKt10PV0TrackJets_pt_TRK_BIAS_D0_WM);
   fChain->SetBranchAddress("AntiKt10PV0TrackJets_eta_TRK_BIAS_D0_WM", &AntiKt10PV0TrackJets_eta_TRK_BIAS_D0_WM, &b_AntiKt10PV0TrackJets_eta_TRK_BIAS_D0_WM);
   fChain->SetBranchAddress("AntiKt10PV0TrackJets_phi_TRK_BIAS_D0_WM", &AntiKt10PV0TrackJets_phi_TRK_BIAS_D0_WM, &b_AntiKt10PV0TrackJets_phi_TRK_BIAS_D0_WM);
   fChain->SetBranchAddress("AntiKt10PV0TrackJets_m_TRK_BIAS_D0_WM", &AntiKt10PV0TrackJets_m_TRK_BIAS_D0_WM, &b_AntiKt10PV0TrackJets_m_TRK_BIAS_D0_WM);
   fChain->SetBranchAddress("AntiKt10PV0TrackJets_daughter__pt_TRK_BIAS_D0_WM", &AntiKt10PV0TrackJets_daughter__pt_TRK_BIAS_D0_WM, &b_AntiKt10PV0TrackJets_daughter__pt_TRK_BIAS_D0_WM);
   fChain->SetBranchAddress("AntiKt10PV0TrackJets_daughter__eta_TRK_BIAS_D0_WM", &AntiKt10PV0TrackJets_daughter__eta_TRK_BIAS_D0_WM, &b_AntiKt10PV0TrackJets_daughter__eta_TRK_BIAS_D0_WM);
   fChain->SetBranchAddress("AntiKt10PV0TrackJets_daughter__phi_TRK_BIAS_D0_WM", &AntiKt10PV0TrackJets_daughter__phi_TRK_BIAS_D0_WM, &b_AntiKt10PV0TrackJets_daughter__phi_TRK_BIAS_D0_WM);
   fChain->SetBranchAddress("AntiKt10PV0TrackJets_daughter__trackId_TRK_BIAS_D0_WM", &AntiKt10PV0TrackJets_daughter__trackId_TRK_BIAS_D0_WM, &b_AntiKt10PV0TrackJets_daughter__trackId_TRK_BIAS_D0_WM);
   fChain->SetBranchAddress("AntiKtVR30Rmax4Rmin02TrackJets_pt_TRK_BIAS_D0_WM", &AntiKtVR30Rmax4Rmin02TrackJets_pt_TRK_BIAS_D0_WM, &b_AntiKtVR30Rmax4Rmin02TrackJets_pt_TRK_BIAS_D0_WM);
   fChain->SetBranchAddress("AntiKtVR30Rmax4Rmin02TrackJets_eta_TRK_BIAS_D0_WM", &AntiKtVR30Rmax4Rmin02TrackJets_eta_TRK_BIAS_D0_WM, &b_AntiKtVR30Rmax4Rmin02TrackJets_eta_TRK_BIAS_D0_WM);
   fChain->SetBranchAddress("AntiKtVR30Rmax4Rmin02TrackJets_phi_TRK_BIAS_D0_WM", &AntiKtVR30Rmax4Rmin02TrackJets_phi_TRK_BIAS_D0_WM, &b_AntiKtVR30Rmax4Rmin02TrackJets_phi_TRK_BIAS_D0_WM);
   fChain->SetBranchAddress("AntiKtVR30Rmax4Rmin02TrackJets_m_TRK_BIAS_D0_WM", &AntiKtVR30Rmax4Rmin02TrackJets_m_TRK_BIAS_D0_WM, &b_AntiKtVR30Rmax4Rmin02TrackJets_m_TRK_BIAS_D0_WM);
   fChain->SetBranchAddress("AntiKtVR30Rmax4Rmin02TrackJets_daughter__pt_TRK_BIAS_D0_WM", &AntiKtVR30Rmax4Rmin02TrackJets_daughter__pt_TRK_BIAS_D0_WM, &b_AntiKtVR30Rmax4Rmin02TrackJets_daughter__pt_TRK_BIAS_D0_WM);
   fChain->SetBranchAddress("AntiKtVR30Rmax4Rmin02TrackJets_daughter__eta_TRK_BIAS_D0_WM", &AntiKtVR30Rmax4Rmin02TrackJets_daughter__eta_TRK_BIAS_D0_WM, &b_AntiKtVR30Rmax4Rmin02TrackJets_daughter__eta_TRK_BIAS_D0_WM);
   fChain->SetBranchAddress("AntiKtVR30Rmax4Rmin02TrackJets_daughter__phi_TRK_BIAS_D0_WM", &AntiKtVR30Rmax4Rmin02TrackJets_daughter__phi_TRK_BIAS_D0_WM, &b_AntiKtVR30Rmax4Rmin02TrackJets_daughter__phi_TRK_BIAS_D0_WM);
   fChain->SetBranchAddress("AntiKtVR30Rmax4Rmin02TrackJets_daughter__trackId_TRK_BIAS_D0_WM", &AntiKtVR30Rmax4Rmin02TrackJets_daughter__trackId_TRK_BIAS_D0_WM, &b_AntiKtVR30Rmax4Rmin02TrackJets_daughter__trackId_TRK_BIAS_D0_WM);
   fChain->SetBranchAddress("DMesons_costhetastar_TRK_BIAS_QOVERP_SAGITTA_WM", &DMesons_costhetastar_TRK_BIAS_QOVERP_SAGITTA_WM, &b_DMesons_costhetastar_TRK_BIAS_QOVERP_SAGITTA_WM);
   fChain->SetBranchAddress("DMesons_D0Index_TRK_BIAS_QOVERP_SAGITTA_WM", &DMesons_D0Index_TRK_BIAS_QOVERP_SAGITTA_WM, &b_DMesons_D0Index_TRK_BIAS_QOVERP_SAGITTA_WM);
   fChain->SetBranchAddress("DMesons_daughterInfo__eta_TRK_BIAS_QOVERP_SAGITTA_WM", &DMesons_daughterInfo__eta_TRK_BIAS_QOVERP_SAGITTA_WM, &b_DMesons_daughterInfo__eta_TRK_BIAS_QOVERP_SAGITTA_WM);
   fChain->SetBranchAddress("DMesons_daughterInfo__passTight_TRK_BIAS_QOVERP_SAGITTA_WM", &DMesons_daughterInfo__passTight_TRK_BIAS_QOVERP_SAGITTA_WM, &b_DMesons_daughterInfo__passTight_TRK_BIAS_QOVERP_SAGITTA_WM);
   fChain->SetBranchAddress("DMesons_daughterInfo__pdgId_TRK_BIAS_QOVERP_SAGITTA_WM", &DMesons_daughterInfo__pdgId_TRK_BIAS_QOVERP_SAGITTA_WM, &b_DMesons_daughterInfo__pdgId_TRK_BIAS_QOVERP_SAGITTA_WM);
   fChain->SetBranchAddress("DMesons_daughterInfo__phi_TRK_BIAS_QOVERP_SAGITTA_WM", &DMesons_daughterInfo__phi_TRK_BIAS_QOVERP_SAGITTA_WM, &b_DMesons_daughterInfo__phi_TRK_BIAS_QOVERP_SAGITTA_WM);
   fChain->SetBranchAddress("DMesons_daughterInfo__pt_TRK_BIAS_QOVERP_SAGITTA_WM", &DMesons_daughterInfo__pt_TRK_BIAS_QOVERP_SAGITTA_WM, &b_DMesons_daughterInfo__pt_TRK_BIAS_QOVERP_SAGITTA_WM);
   fChain->SetBranchAddress("DMesons_daughterInfo__trackId_TRK_BIAS_QOVERP_SAGITTA_WM", &DMesons_daughterInfo__trackId_TRK_BIAS_QOVERP_SAGITTA_WM, &b_DMesons_daughterInfo__trackId_TRK_BIAS_QOVERP_SAGITTA_WM);
   fChain->SetBranchAddress("DMesons_daughterInfo__truthBarcode_TRK_BIAS_QOVERP_SAGITTA_WM", &DMesons_daughterInfo__truthBarcode_TRK_BIAS_QOVERP_SAGITTA_WM, &b_DMesons_daughterInfo__truthBarcode_TRK_BIAS_QOVERP_SAGITTA_WM);
   fChain->SetBranchAddress("DMesons_daughterInfo__truthDBarcode_TRK_BIAS_QOVERP_SAGITTA_WM", &DMesons_daughterInfo__truthDBarcode_TRK_BIAS_QOVERP_SAGITTA_WM, &b_DMesons_daughterInfo__truthDBarcode_TRK_BIAS_QOVERP_SAGITTA_WM);
   fChain->SetBranchAddress("DMesons_daughterInfo__z0SinTheta_TRK_BIAS_QOVERP_SAGITTA_WM", &DMesons_daughterInfo__z0SinTheta_TRK_BIAS_QOVERP_SAGITTA_WM, &b_DMesons_daughterInfo__z0SinTheta_TRK_BIAS_QOVERP_SAGITTA_WM);
   fChain->SetBranchAddress("DMesons_daughterInfo__z0SinThetaPV_TRK_BIAS_QOVERP_SAGITTA_WM", &DMesons_daughterInfo__z0SinThetaPV_TRK_BIAS_QOVERP_SAGITTA_WM, &b_DMesons_daughterInfo__z0SinThetaPV_TRK_BIAS_QOVERP_SAGITTA_WM);
   fChain->SetBranchAddress("DMesons_decayType_TRK_BIAS_QOVERP_SAGITTA_WM", &DMesons_decayType_TRK_BIAS_QOVERP_SAGITTA_WM, &b_DMesons_decayType_TRK_BIAS_QOVERP_SAGITTA_WM);
   fChain->SetBranchAddress("DMesons_DeltaMass_TRK_BIAS_QOVERP_SAGITTA_WM", &DMesons_DeltaMass_TRK_BIAS_QOVERP_SAGITTA_WM, &b_DMesons_DeltaMass_TRK_BIAS_QOVERP_SAGITTA_WM);
   fChain->SetBranchAddress("DMesons_eta_TRK_BIAS_QOVERP_SAGITTA_WM", &DMesons_eta_TRK_BIAS_QOVERP_SAGITTA_WM, &b_DMesons_eta_TRK_BIAS_QOVERP_SAGITTA_WM);
   fChain->SetBranchAddress("DMesons_fitOutput__Charge_TRK_BIAS_QOVERP_SAGITTA_WM", &DMesons_fitOutput__Charge_TRK_BIAS_QOVERP_SAGITTA_WM, &b_DMesons_fitOutput__Charge_TRK_BIAS_QOVERP_SAGITTA_WM);
   fChain->SetBranchAddress("DMesons_fitOutput__Chi2_TRK_BIAS_QOVERP_SAGITTA_WM", &DMesons_fitOutput__Chi2_TRK_BIAS_QOVERP_SAGITTA_WM, &b_DMesons_fitOutput__Chi2_TRK_BIAS_QOVERP_SAGITTA_WM);
   fChain->SetBranchAddress("DMesons_fitOutput__Impact_TRK_BIAS_QOVERP_SAGITTA_WM", &DMesons_fitOutput__Impact_TRK_BIAS_QOVERP_SAGITTA_WM, &b_DMesons_fitOutput__Impact_TRK_BIAS_QOVERP_SAGITTA_WM);
   fChain->SetBranchAddress("DMesons_fitOutput__ImpactSignificance_TRK_BIAS_QOVERP_SAGITTA_WM", &DMesons_fitOutput__ImpactSignificance_TRK_BIAS_QOVERP_SAGITTA_WM, &b_DMesons_fitOutput__ImpactSignificance_TRK_BIAS_QOVERP_SAGITTA_WM);
   fChain->SetBranchAddress("DMesons_fitOutput__ImpactZ0SinTheta_TRK_BIAS_QOVERP_SAGITTA_WM", &DMesons_fitOutput__ImpactZ0SinTheta_TRK_BIAS_QOVERP_SAGITTA_WM, &b_DMesons_fitOutput__ImpactZ0SinTheta_TRK_BIAS_QOVERP_SAGITTA_WM);
   fChain->SetBranchAddress("DMesons_fitOutput__Lxy_TRK_BIAS_QOVERP_SAGITTA_WM", &DMesons_fitOutput__Lxy_TRK_BIAS_QOVERP_SAGITTA_WM, &b_DMesons_fitOutput__Lxy_TRK_BIAS_QOVERP_SAGITTA_WM);
   fChain->SetBranchAddress("DMesons_fitOutput__VertexPosition_TRK_BIAS_QOVERP_SAGITTA_WM", &DMesons_fitOutput__VertexPosition_TRK_BIAS_QOVERP_SAGITTA_WM, &b_DMesons_fitOutput__VertexPosition_TRK_BIAS_QOVERP_SAGITTA_WM);
   fChain->SetBranchAddress("DMesons_m_TRK_BIAS_QOVERP_SAGITTA_WM", &DMesons_m_TRK_BIAS_QOVERP_SAGITTA_WM, &b_DMesons_m_TRK_BIAS_QOVERP_SAGITTA_WM);
   fChain->SetBranchAddress("DMesons_mKpi1_TRK_BIAS_QOVERP_SAGITTA_WM", &DMesons_mKpi1_TRK_BIAS_QOVERP_SAGITTA_WM, &b_DMesons_mKpi1_TRK_BIAS_QOVERP_SAGITTA_WM);
   fChain->SetBranchAddress("DMesons_mKpi2_TRK_BIAS_QOVERP_SAGITTA_WM", &DMesons_mKpi2_TRK_BIAS_QOVERP_SAGITTA_WM, &b_DMesons_mKpi2_TRK_BIAS_QOVERP_SAGITTA_WM);
   fChain->SetBranchAddress("DMesons_mPhi1_TRK_BIAS_QOVERP_SAGITTA_WM", &DMesons_mPhi1_TRK_BIAS_QOVERP_SAGITTA_WM, &b_DMesons_mPhi1_TRK_BIAS_QOVERP_SAGITTA_WM);
   fChain->SetBranchAddress("DMesons_mPhi2_TRK_BIAS_QOVERP_SAGITTA_WM", &DMesons_mPhi2_TRK_BIAS_QOVERP_SAGITTA_WM, &b_DMesons_mPhi2_TRK_BIAS_QOVERP_SAGITTA_WM);
   fChain->SetBranchAddress("DMesons_pdgId_TRK_BIAS_QOVERP_SAGITTA_WM", &DMesons_pdgId_TRK_BIAS_QOVERP_SAGITTA_WM, &b_DMesons_pdgId_TRK_BIAS_QOVERP_SAGITTA_WM);
   fChain->SetBranchAddress("DMesons_phi_TRK_BIAS_QOVERP_SAGITTA_WM", &DMesons_phi_TRK_BIAS_QOVERP_SAGITTA_WM, &b_DMesons_phi_TRK_BIAS_QOVERP_SAGITTA_WM);
   fChain->SetBranchAddress("DMesons_pt_TRK_BIAS_QOVERP_SAGITTA_WM", &DMesons_pt_TRK_BIAS_QOVERP_SAGITTA_WM, &b_DMesons_pt_TRK_BIAS_QOVERP_SAGITTA_WM);
   fChain->SetBranchAddress("DMesons_ptcone40_TRK_BIAS_QOVERP_SAGITTA_WM", &DMesons_ptcone40_TRK_BIAS_QOVERP_SAGITTA_WM, &b_DMesons_ptcone40_TRK_BIAS_QOVERP_SAGITTA_WM);
   fChain->SetBranchAddress("DMesons_SlowPionD0_TRK_BIAS_QOVERP_SAGITTA_WM", &DMesons_SlowPionD0_TRK_BIAS_QOVERP_SAGITTA_WM, &b_DMesons_SlowPionD0_TRK_BIAS_QOVERP_SAGITTA_WM);
   fChain->SetBranchAddress("DMesons_SlowPionZ0SinTheta_TRK_BIAS_QOVERP_SAGITTA_WM", &DMesons_SlowPionZ0SinTheta_TRK_BIAS_QOVERP_SAGITTA_WM, &b_DMesons_SlowPionZ0SinTheta_TRK_BIAS_QOVERP_SAGITTA_WM);
   fChain->SetBranchAddress("DMesons_truthBarcode_TRK_BIAS_QOVERP_SAGITTA_WM", &DMesons_truthBarcode_TRK_BIAS_QOVERP_SAGITTA_WM, &b_DMesons_truthBarcode_TRK_BIAS_QOVERP_SAGITTA_WM);
   fChain->SetBranchAddress("MesonTracks_d0sig_TRK_BIAS_QOVERP_SAGITTA_WM", &MesonTracks_d0sig_TRK_BIAS_QOVERP_SAGITTA_WM, &b_MesonTracks_d0sig_TRK_BIAS_QOVERP_SAGITTA_WM);
   fChain->SetBranchAddress("MesonTracks_d0sigPV_TRK_BIAS_QOVERP_SAGITTA_WM", &MesonTracks_d0sigPV_TRK_BIAS_QOVERP_SAGITTA_WM, &b_MesonTracks_d0sigPV_TRK_BIAS_QOVERP_SAGITTA_WM);
   fChain->SetBranchAddress("MesonTracks_eta_TRK_BIAS_QOVERP_SAGITTA_WM", &MesonTracks_eta_TRK_BIAS_QOVERP_SAGITTA_WM, &b_MesonTracks_eta_TRK_BIAS_QOVERP_SAGITTA_WM);
   fChain->SetBranchAddress("MesonTracks_passTight_TRK_BIAS_QOVERP_SAGITTA_WM", &MesonTracks_passTight_TRK_BIAS_QOVERP_SAGITTA_WM, &b_MesonTracks_passTight_TRK_BIAS_QOVERP_SAGITTA_WM);
   fChain->SetBranchAddress("MesonTracks_phi_TRK_BIAS_QOVERP_SAGITTA_WM", &MesonTracks_phi_TRK_BIAS_QOVERP_SAGITTA_WM, &b_MesonTracks_phi_TRK_BIAS_QOVERP_SAGITTA_WM);
   fChain->SetBranchAddress("MesonTracks_pt_TRK_BIAS_QOVERP_SAGITTA_WM", &MesonTracks_pt_TRK_BIAS_QOVERP_SAGITTA_WM, &b_MesonTracks_pt_TRK_BIAS_QOVERP_SAGITTA_WM);
   fChain->SetBranchAddress("MesonTracks_trackId_TRK_BIAS_QOVERP_SAGITTA_WM", &MesonTracks_trackId_TRK_BIAS_QOVERP_SAGITTA_WM, &b_MesonTracks_trackId_TRK_BIAS_QOVERP_SAGITTA_WM);
   fChain->SetBranchAddress("MesonTracks_z0sinTheta_TRK_BIAS_QOVERP_SAGITTA_WM", &MesonTracks_z0sinTheta_TRK_BIAS_QOVERP_SAGITTA_WM, &b_MesonTracks_z0sinTheta_TRK_BIAS_QOVERP_SAGITTA_WM);
   fChain->SetBranchAddress("MesonTracks_truthMatchProbability_TRK_BIAS_QOVERP_SAGITTA_WM", &MesonTracks_truthMatchProbability_TRK_BIAS_QOVERP_SAGITTA_WM, &b_MesonTracks_truthMatchProbability_TRK_BIAS_QOVERP_SAGITTA_WM);
   fChain->SetBranchAddress("AntiKt6PV0TrackJets_pt_TRK_BIAS_QOVERP_SAGITTA_WM", &AntiKt6PV0TrackJets_pt_TRK_BIAS_QOVERP_SAGITTA_WM, &b_AntiKt6PV0TrackJets_pt_TRK_BIAS_QOVERP_SAGITTA_WM);
   fChain->SetBranchAddress("AntiKt6PV0TrackJets_eta_TRK_BIAS_QOVERP_SAGITTA_WM", &AntiKt6PV0TrackJets_eta_TRK_BIAS_QOVERP_SAGITTA_WM, &b_AntiKt6PV0TrackJets_eta_TRK_BIAS_QOVERP_SAGITTA_WM);
   fChain->SetBranchAddress("AntiKt6PV0TrackJets_phi_TRK_BIAS_QOVERP_SAGITTA_WM", &AntiKt6PV0TrackJets_phi_TRK_BIAS_QOVERP_SAGITTA_WM, &b_AntiKt6PV0TrackJets_phi_TRK_BIAS_QOVERP_SAGITTA_WM);
   fChain->SetBranchAddress("AntiKt6PV0TrackJets_m_TRK_BIAS_QOVERP_SAGITTA_WM", &AntiKt6PV0TrackJets_m_TRK_BIAS_QOVERP_SAGITTA_WM, &b_AntiKt6PV0TrackJets_m_TRK_BIAS_QOVERP_SAGITTA_WM);
   fChain->SetBranchAddress("AntiKt6PV0TrackJets_daughter__pt_TRK_BIAS_QOVERP_SAGITTA_WM", &AntiKt6PV0TrackJets_daughter__pt_TRK_BIAS_QOVERP_SAGITTA_WM, &b_AntiKt6PV0TrackJets_daughter__pt_TRK_BIAS_QOVERP_SAGITTA_WM);
   fChain->SetBranchAddress("AntiKt6PV0TrackJets_daughter__eta_TRK_BIAS_QOVERP_SAGITTA_WM", &AntiKt6PV0TrackJets_daughter__eta_TRK_BIAS_QOVERP_SAGITTA_WM, &b_AntiKt6PV0TrackJets_daughter__eta_TRK_BIAS_QOVERP_SAGITTA_WM);
   fChain->SetBranchAddress("AntiKt6PV0TrackJets_daughter__phi_TRK_BIAS_QOVERP_SAGITTA_WM", &AntiKt6PV0TrackJets_daughter__phi_TRK_BIAS_QOVERP_SAGITTA_WM, &b_AntiKt6PV0TrackJets_daughter__phi_TRK_BIAS_QOVERP_SAGITTA_WM);
   fChain->SetBranchAddress("AntiKt6PV0TrackJets_daughter__trackId_TRK_BIAS_QOVERP_SAGITTA_WM", &AntiKt6PV0TrackJets_daughter__trackId_TRK_BIAS_QOVERP_SAGITTA_WM, &b_AntiKt6PV0TrackJets_daughter__trackId_TRK_BIAS_QOVERP_SAGITTA_WM);
   fChain->SetBranchAddress("AntiKt8PV0TrackJets_pt_TRK_BIAS_QOVERP_SAGITTA_WM", &AntiKt8PV0TrackJets_pt_TRK_BIAS_QOVERP_SAGITTA_WM, &b_AntiKt8PV0TrackJets_pt_TRK_BIAS_QOVERP_SAGITTA_WM);
   fChain->SetBranchAddress("AntiKt8PV0TrackJets_eta_TRK_BIAS_QOVERP_SAGITTA_WM", &AntiKt8PV0TrackJets_eta_TRK_BIAS_QOVERP_SAGITTA_WM, &b_AntiKt8PV0TrackJets_eta_TRK_BIAS_QOVERP_SAGITTA_WM);
   fChain->SetBranchAddress("AntiKt8PV0TrackJets_phi_TRK_BIAS_QOVERP_SAGITTA_WM", &AntiKt8PV0TrackJets_phi_TRK_BIAS_QOVERP_SAGITTA_WM, &b_AntiKt8PV0TrackJets_phi_TRK_BIAS_QOVERP_SAGITTA_WM);
   fChain->SetBranchAddress("AntiKt8PV0TrackJets_m_TRK_BIAS_QOVERP_SAGITTA_WM", &AntiKt8PV0TrackJets_m_TRK_BIAS_QOVERP_SAGITTA_WM, &b_AntiKt8PV0TrackJets_m_TRK_BIAS_QOVERP_SAGITTA_WM);
   fChain->SetBranchAddress("AntiKt8PV0TrackJets_daughter__pt_TRK_BIAS_QOVERP_SAGITTA_WM", &AntiKt8PV0TrackJets_daughter__pt_TRK_BIAS_QOVERP_SAGITTA_WM, &b_AntiKt8PV0TrackJets_daughter__pt_TRK_BIAS_QOVERP_SAGITTA_WM);
   fChain->SetBranchAddress("AntiKt8PV0TrackJets_daughter__eta_TRK_BIAS_QOVERP_SAGITTA_WM", &AntiKt8PV0TrackJets_daughter__eta_TRK_BIAS_QOVERP_SAGITTA_WM, &b_AntiKt8PV0TrackJets_daughter__eta_TRK_BIAS_QOVERP_SAGITTA_WM);
   fChain->SetBranchAddress("AntiKt8PV0TrackJets_daughter__phi_TRK_BIAS_QOVERP_SAGITTA_WM", &AntiKt8PV0TrackJets_daughter__phi_TRK_BIAS_QOVERP_SAGITTA_WM, &b_AntiKt8PV0TrackJets_daughter__phi_TRK_BIAS_QOVERP_SAGITTA_WM);
   fChain->SetBranchAddress("AntiKt8PV0TrackJets_daughter__trackId_TRK_BIAS_QOVERP_SAGITTA_WM", &AntiKt8PV0TrackJets_daughter__trackId_TRK_BIAS_QOVERP_SAGITTA_WM, &b_AntiKt8PV0TrackJets_daughter__trackId_TRK_BIAS_QOVERP_SAGITTA_WM);
   fChain->SetBranchAddress("AntiKt10PV0TrackJets_pt_TRK_BIAS_QOVERP_SAGITTA_WM", &AntiKt10PV0TrackJets_pt_TRK_BIAS_QOVERP_SAGITTA_WM, &b_AntiKt10PV0TrackJets_pt_TRK_BIAS_QOVERP_SAGITTA_WM);
   fChain->SetBranchAddress("AntiKt10PV0TrackJets_eta_TRK_BIAS_QOVERP_SAGITTA_WM", &AntiKt10PV0TrackJets_eta_TRK_BIAS_QOVERP_SAGITTA_WM, &b_AntiKt10PV0TrackJets_eta_TRK_BIAS_QOVERP_SAGITTA_WM);
   fChain->SetBranchAddress("AntiKt10PV0TrackJets_phi_TRK_BIAS_QOVERP_SAGITTA_WM", &AntiKt10PV0TrackJets_phi_TRK_BIAS_QOVERP_SAGITTA_WM, &b_AntiKt10PV0TrackJets_phi_TRK_BIAS_QOVERP_SAGITTA_WM);
   fChain->SetBranchAddress("AntiKt10PV0TrackJets_m_TRK_BIAS_QOVERP_SAGITTA_WM", &AntiKt10PV0TrackJets_m_TRK_BIAS_QOVERP_SAGITTA_WM, &b_AntiKt10PV0TrackJets_m_TRK_BIAS_QOVERP_SAGITTA_WM);
   fChain->SetBranchAddress("AntiKt10PV0TrackJets_daughter__pt_TRK_BIAS_QOVERP_SAGITTA_WM", &AntiKt10PV0TrackJets_daughter__pt_TRK_BIAS_QOVERP_SAGITTA_WM, &b_AntiKt10PV0TrackJets_daughter__pt_TRK_BIAS_QOVERP_SAGITTA_WM);
   fChain->SetBranchAddress("AntiKt10PV0TrackJets_daughter__eta_TRK_BIAS_QOVERP_SAGITTA_WM", &AntiKt10PV0TrackJets_daughter__eta_TRK_BIAS_QOVERP_SAGITTA_WM, &b_AntiKt10PV0TrackJets_daughter__eta_TRK_BIAS_QOVERP_SAGITTA_WM);
   fChain->SetBranchAddress("AntiKt10PV0TrackJets_daughter__phi_TRK_BIAS_QOVERP_SAGITTA_WM", &AntiKt10PV0TrackJets_daughter__phi_TRK_BIAS_QOVERP_SAGITTA_WM, &b_AntiKt10PV0TrackJets_daughter__phi_TRK_BIAS_QOVERP_SAGITTA_WM);
   fChain->SetBranchAddress("AntiKt10PV0TrackJets_daughter__trackId_TRK_BIAS_QOVERP_SAGITTA_WM", &AntiKt10PV0TrackJets_daughter__trackId_TRK_BIAS_QOVERP_SAGITTA_WM, &b_AntiKt10PV0TrackJets_daughter__trackId_TRK_BIAS_QOVERP_SAGITTA_WM);
   fChain->SetBranchAddress("AntiKtVR30Rmax4Rmin02TrackJets_pt_TRK_BIAS_QOVERP_SAGITTA_WM", &AntiKtVR30Rmax4Rmin02TrackJets_pt_TRK_BIAS_QOVERP_SAGITTA_WM, &b_AntiKtVR30Rmax4Rmin02TrackJets_pt_TRK_BIAS_QOVERP_SAGITTA_WM);
   fChain->SetBranchAddress("AntiKtVR30Rmax4Rmin02TrackJets_eta_TRK_BIAS_QOVERP_SAGITTA_WM", &AntiKtVR30Rmax4Rmin02TrackJets_eta_TRK_BIAS_QOVERP_SAGITTA_WM, &b_AntiKtVR30Rmax4Rmin02TrackJets_eta_TRK_BIAS_QOVERP_SAGITTA_WM);
   fChain->SetBranchAddress("AntiKtVR30Rmax4Rmin02TrackJets_phi_TRK_BIAS_QOVERP_SAGITTA_WM", &AntiKtVR30Rmax4Rmin02TrackJets_phi_TRK_BIAS_QOVERP_SAGITTA_WM, &b_AntiKtVR30Rmax4Rmin02TrackJets_phi_TRK_BIAS_QOVERP_SAGITTA_WM);
   fChain->SetBranchAddress("AntiKtVR30Rmax4Rmin02TrackJets_m_TRK_BIAS_QOVERP_SAGITTA_WM", &AntiKtVR30Rmax4Rmin02TrackJets_m_TRK_BIAS_QOVERP_SAGITTA_WM, &b_AntiKtVR30Rmax4Rmin02TrackJets_m_TRK_BIAS_QOVERP_SAGITTA_WM);
   fChain->SetBranchAddress("AntiKtVR30Rmax4Rmin02TrackJets_daughter__pt_TRK_BIAS_QOVERP_SAGITTA_WM", &AntiKtVR30Rmax4Rmin02TrackJets_daughter__pt_TRK_BIAS_QOVERP_SAGITTA_WM, &b_AntiKtVR30Rmax4Rmin02TrackJets_daughter__pt_TRK_BIAS_QOVERP_SAGITTA_WM);
   fChain->SetBranchAddress("AntiKtVR30Rmax4Rmin02TrackJets_daughter__eta_TRK_BIAS_QOVERP_SAGITTA_WM", &AntiKtVR30Rmax4Rmin02TrackJets_daughter__eta_TRK_BIAS_QOVERP_SAGITTA_WM, &b_AntiKtVR30Rmax4Rmin02TrackJets_daughter__eta_TRK_BIAS_QOVERP_SAGITTA_WM);
   fChain->SetBranchAddress("AntiKtVR30Rmax4Rmin02TrackJets_daughter__phi_TRK_BIAS_QOVERP_SAGITTA_WM", &AntiKtVR30Rmax4Rmin02TrackJets_daughter__phi_TRK_BIAS_QOVERP_SAGITTA_WM, &b_AntiKtVR30Rmax4Rmin02TrackJets_daughter__phi_TRK_BIAS_QOVERP_SAGITTA_WM);
   fChain->SetBranchAddress("AntiKtVR30Rmax4Rmin02TrackJets_daughter__trackId_TRK_BIAS_QOVERP_SAGITTA_WM", &AntiKtVR30Rmax4Rmin02TrackJets_daughter__trackId_TRK_BIAS_QOVERP_SAGITTA_WM, &b_AntiKtVR30Rmax4Rmin02TrackJets_daughter__trackId_TRK_BIAS_QOVERP_SAGITTA_WM);
   fChain->SetBranchAddress("DMesons_costhetastar_TRK_BIAS_Z0_WM", &DMesons_costhetastar_TRK_BIAS_Z0_WM, &b_DMesons_costhetastar_TRK_BIAS_Z0_WM);
   fChain->SetBranchAddress("DMesons_D0Index_TRK_BIAS_Z0_WM", &DMesons_D0Index_TRK_BIAS_Z0_WM, &b_DMesons_D0Index_TRK_BIAS_Z0_WM);
   fChain->SetBranchAddress("DMesons_daughterInfo__eta_TRK_BIAS_Z0_WM", &DMesons_daughterInfo__eta_TRK_BIAS_Z0_WM, &b_DMesons_daughterInfo__eta_TRK_BIAS_Z0_WM);
   fChain->SetBranchAddress("DMesons_daughterInfo__passTight_TRK_BIAS_Z0_WM", &DMesons_daughterInfo__passTight_TRK_BIAS_Z0_WM, &b_DMesons_daughterInfo__passTight_TRK_BIAS_Z0_WM);
   fChain->SetBranchAddress("DMesons_daughterInfo__pdgId_TRK_BIAS_Z0_WM", &DMesons_daughterInfo__pdgId_TRK_BIAS_Z0_WM, &b_DMesons_daughterInfo__pdgId_TRK_BIAS_Z0_WM);
   fChain->SetBranchAddress("DMesons_daughterInfo__phi_TRK_BIAS_Z0_WM", &DMesons_daughterInfo__phi_TRK_BIAS_Z0_WM, &b_DMesons_daughterInfo__phi_TRK_BIAS_Z0_WM);
   fChain->SetBranchAddress("DMesons_daughterInfo__pt_TRK_BIAS_Z0_WM", &DMesons_daughterInfo__pt_TRK_BIAS_Z0_WM, &b_DMesons_daughterInfo__pt_TRK_BIAS_Z0_WM);
   fChain->SetBranchAddress("DMesons_daughterInfo__trackId_TRK_BIAS_Z0_WM", &DMesons_daughterInfo__trackId_TRK_BIAS_Z0_WM, &b_DMesons_daughterInfo__trackId_TRK_BIAS_Z0_WM);
   fChain->SetBranchAddress("DMesons_daughterInfo__truthBarcode_TRK_BIAS_Z0_WM", &DMesons_daughterInfo__truthBarcode_TRK_BIAS_Z0_WM, &b_DMesons_daughterInfo__truthBarcode_TRK_BIAS_Z0_WM);
   fChain->SetBranchAddress("DMesons_daughterInfo__truthDBarcode_TRK_BIAS_Z0_WM", &DMesons_daughterInfo__truthDBarcode_TRK_BIAS_Z0_WM, &b_DMesons_daughterInfo__truthDBarcode_TRK_BIAS_Z0_WM);
   fChain->SetBranchAddress("DMesons_daughterInfo__z0SinTheta_TRK_BIAS_Z0_WM", &DMesons_daughterInfo__z0SinTheta_TRK_BIAS_Z0_WM, &b_DMesons_daughterInfo__z0SinTheta_TRK_BIAS_Z0_WM);
   fChain->SetBranchAddress("DMesons_daughterInfo__z0SinThetaPV_TRK_BIAS_Z0_WM", &DMesons_daughterInfo__z0SinThetaPV_TRK_BIAS_Z0_WM, &b_DMesons_daughterInfo__z0SinThetaPV_TRK_BIAS_Z0_WM);
   fChain->SetBranchAddress("DMesons_decayType_TRK_BIAS_Z0_WM", &DMesons_decayType_TRK_BIAS_Z0_WM, &b_DMesons_decayType_TRK_BIAS_Z0_WM);
   fChain->SetBranchAddress("DMesons_DeltaMass_TRK_BIAS_Z0_WM", &DMesons_DeltaMass_TRK_BIAS_Z0_WM, &b_DMesons_DeltaMass_TRK_BIAS_Z0_WM);
   fChain->SetBranchAddress("DMesons_eta_TRK_BIAS_Z0_WM", &DMesons_eta_TRK_BIAS_Z0_WM, &b_DMesons_eta_TRK_BIAS_Z0_WM);
   fChain->SetBranchAddress("DMesons_fitOutput__Charge_TRK_BIAS_Z0_WM", &DMesons_fitOutput__Charge_TRK_BIAS_Z0_WM, &b_DMesons_fitOutput__Charge_TRK_BIAS_Z0_WM);
   fChain->SetBranchAddress("DMesons_fitOutput__Chi2_TRK_BIAS_Z0_WM", &DMesons_fitOutput__Chi2_TRK_BIAS_Z0_WM, &b_DMesons_fitOutput__Chi2_TRK_BIAS_Z0_WM);
   fChain->SetBranchAddress("DMesons_fitOutput__Impact_TRK_BIAS_Z0_WM", &DMesons_fitOutput__Impact_TRK_BIAS_Z0_WM, &b_DMesons_fitOutput__Impact_TRK_BIAS_Z0_WM);
   fChain->SetBranchAddress("DMesons_fitOutput__ImpactSignificance_TRK_BIAS_Z0_WM", &DMesons_fitOutput__ImpactSignificance_TRK_BIAS_Z0_WM, &b_DMesons_fitOutput__ImpactSignificance_TRK_BIAS_Z0_WM);
   fChain->SetBranchAddress("DMesons_fitOutput__ImpactZ0SinTheta_TRK_BIAS_Z0_WM", &DMesons_fitOutput__ImpactZ0SinTheta_TRK_BIAS_Z0_WM, &b_DMesons_fitOutput__ImpactZ0SinTheta_TRK_BIAS_Z0_WM);
   fChain->SetBranchAddress("DMesons_fitOutput__Lxy_TRK_BIAS_Z0_WM", &DMesons_fitOutput__Lxy_TRK_BIAS_Z0_WM, &b_DMesons_fitOutput__Lxy_TRK_BIAS_Z0_WM);
   fChain->SetBranchAddress("DMesons_fitOutput__VertexPosition_TRK_BIAS_Z0_WM", &DMesons_fitOutput__VertexPosition_TRK_BIAS_Z0_WM, &b_DMesons_fitOutput__VertexPosition_TRK_BIAS_Z0_WM);
   fChain->SetBranchAddress("DMesons_m_TRK_BIAS_Z0_WM", &DMesons_m_TRK_BIAS_Z0_WM, &b_DMesons_m_TRK_BIAS_Z0_WM);
   fChain->SetBranchAddress("DMesons_mKpi1_TRK_BIAS_Z0_WM", &DMesons_mKpi1_TRK_BIAS_Z0_WM, &b_DMesons_mKpi1_TRK_BIAS_Z0_WM);
   fChain->SetBranchAddress("DMesons_mKpi2_TRK_BIAS_Z0_WM", &DMesons_mKpi2_TRK_BIAS_Z0_WM, &b_DMesons_mKpi2_TRK_BIAS_Z0_WM);
   fChain->SetBranchAddress("DMesons_mPhi1_TRK_BIAS_Z0_WM", &DMesons_mPhi1_TRK_BIAS_Z0_WM, &b_DMesons_mPhi1_TRK_BIAS_Z0_WM);
   fChain->SetBranchAddress("DMesons_mPhi2_TRK_BIAS_Z0_WM", &DMesons_mPhi2_TRK_BIAS_Z0_WM, &b_DMesons_mPhi2_TRK_BIAS_Z0_WM);
   fChain->SetBranchAddress("DMesons_pdgId_TRK_BIAS_Z0_WM", &DMesons_pdgId_TRK_BIAS_Z0_WM, &b_DMesons_pdgId_TRK_BIAS_Z0_WM);
   fChain->SetBranchAddress("DMesons_phi_TRK_BIAS_Z0_WM", &DMesons_phi_TRK_BIAS_Z0_WM, &b_DMesons_phi_TRK_BIAS_Z0_WM);
   fChain->SetBranchAddress("DMesons_pt_TRK_BIAS_Z0_WM", &DMesons_pt_TRK_BIAS_Z0_WM, &b_DMesons_pt_TRK_BIAS_Z0_WM);
   fChain->SetBranchAddress("DMesons_ptcone40_TRK_BIAS_Z0_WM", &DMesons_ptcone40_TRK_BIAS_Z0_WM, &b_DMesons_ptcone40_TRK_BIAS_Z0_WM);
   fChain->SetBranchAddress("DMesons_SlowPionD0_TRK_BIAS_Z0_WM", &DMesons_SlowPionD0_TRK_BIAS_Z0_WM, &b_DMesons_SlowPionD0_TRK_BIAS_Z0_WM);
   fChain->SetBranchAddress("DMesons_SlowPionZ0SinTheta_TRK_BIAS_Z0_WM", &DMesons_SlowPionZ0SinTheta_TRK_BIAS_Z0_WM, &b_DMesons_SlowPionZ0SinTheta_TRK_BIAS_Z0_WM);
   fChain->SetBranchAddress("DMesons_truthBarcode_TRK_BIAS_Z0_WM", &DMesons_truthBarcode_TRK_BIAS_Z0_WM, &b_DMesons_truthBarcode_TRK_BIAS_Z0_WM);
   fChain->SetBranchAddress("MesonTracks_d0sig_TRK_BIAS_Z0_WM", &MesonTracks_d0sig_TRK_BIAS_Z0_WM, &b_MesonTracks_d0sig_TRK_BIAS_Z0_WM);
   fChain->SetBranchAddress("MesonTracks_d0sigPV_TRK_BIAS_Z0_WM", &MesonTracks_d0sigPV_TRK_BIAS_Z0_WM, &b_MesonTracks_d0sigPV_TRK_BIAS_Z0_WM);
   fChain->SetBranchAddress("MesonTracks_eta_TRK_BIAS_Z0_WM", &MesonTracks_eta_TRK_BIAS_Z0_WM, &b_MesonTracks_eta_TRK_BIAS_Z0_WM);
   fChain->SetBranchAddress("MesonTracks_passTight_TRK_BIAS_Z0_WM", &MesonTracks_passTight_TRK_BIAS_Z0_WM, &b_MesonTracks_passTight_TRK_BIAS_Z0_WM);
   fChain->SetBranchAddress("MesonTracks_phi_TRK_BIAS_Z0_WM", &MesonTracks_phi_TRK_BIAS_Z0_WM, &b_MesonTracks_phi_TRK_BIAS_Z0_WM);
   fChain->SetBranchAddress("MesonTracks_pt_TRK_BIAS_Z0_WM", &MesonTracks_pt_TRK_BIAS_Z0_WM, &b_MesonTracks_pt_TRK_BIAS_Z0_WM);
   fChain->SetBranchAddress("MesonTracks_trackId_TRK_BIAS_Z0_WM", &MesonTracks_trackId_TRK_BIAS_Z0_WM, &b_MesonTracks_trackId_TRK_BIAS_Z0_WM);
   fChain->SetBranchAddress("MesonTracks_z0sinTheta_TRK_BIAS_Z0_WM", &MesonTracks_z0sinTheta_TRK_BIAS_Z0_WM, &b_MesonTracks_z0sinTheta_TRK_BIAS_Z0_WM);
   fChain->SetBranchAddress("MesonTracks_truthMatchProbability_TRK_BIAS_Z0_WM", &MesonTracks_truthMatchProbability_TRK_BIAS_Z0_WM, &b_MesonTracks_truthMatchProbability_TRK_BIAS_Z0_WM);
   fChain->SetBranchAddress("AntiKt6PV0TrackJets_pt_TRK_BIAS_Z0_WM", &AntiKt6PV0TrackJets_pt_TRK_BIAS_Z0_WM, &b_AntiKt6PV0TrackJets_pt_TRK_BIAS_Z0_WM);
   fChain->SetBranchAddress("AntiKt6PV0TrackJets_eta_TRK_BIAS_Z0_WM", &AntiKt6PV0TrackJets_eta_TRK_BIAS_Z0_WM, &b_AntiKt6PV0TrackJets_eta_TRK_BIAS_Z0_WM);
   fChain->SetBranchAddress("AntiKt6PV0TrackJets_phi_TRK_BIAS_Z0_WM", &AntiKt6PV0TrackJets_phi_TRK_BIAS_Z0_WM, &b_AntiKt6PV0TrackJets_phi_TRK_BIAS_Z0_WM);
   fChain->SetBranchAddress("AntiKt6PV0TrackJets_m_TRK_BIAS_Z0_WM", &AntiKt6PV0TrackJets_m_TRK_BIAS_Z0_WM, &b_AntiKt6PV0TrackJets_m_TRK_BIAS_Z0_WM);
   fChain->SetBranchAddress("AntiKt6PV0TrackJets_daughter__pt_TRK_BIAS_Z0_WM", &AntiKt6PV0TrackJets_daughter__pt_TRK_BIAS_Z0_WM, &b_AntiKt6PV0TrackJets_daughter__pt_TRK_BIAS_Z0_WM);
   fChain->SetBranchAddress("AntiKt6PV0TrackJets_daughter__eta_TRK_BIAS_Z0_WM", &AntiKt6PV0TrackJets_daughter__eta_TRK_BIAS_Z0_WM, &b_AntiKt6PV0TrackJets_daughter__eta_TRK_BIAS_Z0_WM);
   fChain->SetBranchAddress("AntiKt6PV0TrackJets_daughter__phi_TRK_BIAS_Z0_WM", &AntiKt6PV0TrackJets_daughter__phi_TRK_BIAS_Z0_WM, &b_AntiKt6PV0TrackJets_daughter__phi_TRK_BIAS_Z0_WM);
   fChain->SetBranchAddress("AntiKt6PV0TrackJets_daughter__trackId_TRK_BIAS_Z0_WM", &AntiKt6PV0TrackJets_daughter__trackId_TRK_BIAS_Z0_WM, &b_AntiKt6PV0TrackJets_daughter__trackId_TRK_BIAS_Z0_WM);
   fChain->SetBranchAddress("AntiKt8PV0TrackJets_pt_TRK_BIAS_Z0_WM", &AntiKt8PV0TrackJets_pt_TRK_BIAS_Z0_WM, &b_AntiKt8PV0TrackJets_pt_TRK_BIAS_Z0_WM);
   fChain->SetBranchAddress("AntiKt8PV0TrackJets_eta_TRK_BIAS_Z0_WM", &AntiKt8PV0TrackJets_eta_TRK_BIAS_Z0_WM, &b_AntiKt8PV0TrackJets_eta_TRK_BIAS_Z0_WM);
   fChain->SetBranchAddress("AntiKt8PV0TrackJets_phi_TRK_BIAS_Z0_WM", &AntiKt8PV0TrackJets_phi_TRK_BIAS_Z0_WM, &b_AntiKt8PV0TrackJets_phi_TRK_BIAS_Z0_WM);
   fChain->SetBranchAddress("AntiKt8PV0TrackJets_m_TRK_BIAS_Z0_WM", &AntiKt8PV0TrackJets_m_TRK_BIAS_Z0_WM, &b_AntiKt8PV0TrackJets_m_TRK_BIAS_Z0_WM);
   fChain->SetBranchAddress("AntiKt8PV0TrackJets_daughter__pt_TRK_BIAS_Z0_WM", &AntiKt8PV0TrackJets_daughter__pt_TRK_BIAS_Z0_WM, &b_AntiKt8PV0TrackJets_daughter__pt_TRK_BIAS_Z0_WM);
   fChain->SetBranchAddress("AntiKt8PV0TrackJets_daughter__eta_TRK_BIAS_Z0_WM", &AntiKt8PV0TrackJets_daughter__eta_TRK_BIAS_Z0_WM, &b_AntiKt8PV0TrackJets_daughter__eta_TRK_BIAS_Z0_WM);
   fChain->SetBranchAddress("AntiKt8PV0TrackJets_daughter__phi_TRK_BIAS_Z0_WM", &AntiKt8PV0TrackJets_daughter__phi_TRK_BIAS_Z0_WM, &b_AntiKt8PV0TrackJets_daughter__phi_TRK_BIAS_Z0_WM);
   fChain->SetBranchAddress("AntiKt8PV0TrackJets_daughter__trackId_TRK_BIAS_Z0_WM", &AntiKt8PV0TrackJets_daughter__trackId_TRK_BIAS_Z0_WM, &b_AntiKt8PV0TrackJets_daughter__trackId_TRK_BIAS_Z0_WM);
   fChain->SetBranchAddress("AntiKt10PV0TrackJets_pt_TRK_BIAS_Z0_WM", &AntiKt10PV0TrackJets_pt_TRK_BIAS_Z0_WM, &b_AntiKt10PV0TrackJets_pt_TRK_BIAS_Z0_WM);
   fChain->SetBranchAddress("AntiKt10PV0TrackJets_eta_TRK_BIAS_Z0_WM", &AntiKt10PV0TrackJets_eta_TRK_BIAS_Z0_WM, &b_AntiKt10PV0TrackJets_eta_TRK_BIAS_Z0_WM);
   fChain->SetBranchAddress("AntiKt10PV0TrackJets_phi_TRK_BIAS_Z0_WM", &AntiKt10PV0TrackJets_phi_TRK_BIAS_Z0_WM, &b_AntiKt10PV0TrackJets_phi_TRK_BIAS_Z0_WM);
   fChain->SetBranchAddress("AntiKt10PV0TrackJets_m_TRK_BIAS_Z0_WM", &AntiKt10PV0TrackJets_m_TRK_BIAS_Z0_WM, &b_AntiKt10PV0TrackJets_m_TRK_BIAS_Z0_WM);
   fChain->SetBranchAddress("AntiKt10PV0TrackJets_daughter__pt_TRK_BIAS_Z0_WM", &AntiKt10PV0TrackJets_daughter__pt_TRK_BIAS_Z0_WM, &b_AntiKt10PV0TrackJets_daughter__pt_TRK_BIAS_Z0_WM);
   fChain->SetBranchAddress("AntiKt10PV0TrackJets_daughter__eta_TRK_BIAS_Z0_WM", &AntiKt10PV0TrackJets_daughter__eta_TRK_BIAS_Z0_WM, &b_AntiKt10PV0TrackJets_daughter__eta_TRK_BIAS_Z0_WM);
   fChain->SetBranchAddress("AntiKt10PV0TrackJets_daughter__phi_TRK_BIAS_Z0_WM", &AntiKt10PV0TrackJets_daughter__phi_TRK_BIAS_Z0_WM, &b_AntiKt10PV0TrackJets_daughter__phi_TRK_BIAS_Z0_WM);
   fChain->SetBranchAddress("AntiKt10PV0TrackJets_daughter__trackId_TRK_BIAS_Z0_WM", &AntiKt10PV0TrackJets_daughter__trackId_TRK_BIAS_Z0_WM, &b_AntiKt10PV0TrackJets_daughter__trackId_TRK_BIAS_Z0_WM);
   fChain->SetBranchAddress("AntiKtVR30Rmax4Rmin02TrackJets_pt_TRK_BIAS_Z0_WM", &AntiKtVR30Rmax4Rmin02TrackJets_pt_TRK_BIAS_Z0_WM, &b_AntiKtVR30Rmax4Rmin02TrackJets_pt_TRK_BIAS_Z0_WM);
   fChain->SetBranchAddress("AntiKtVR30Rmax4Rmin02TrackJets_eta_TRK_BIAS_Z0_WM", &AntiKtVR30Rmax4Rmin02TrackJets_eta_TRK_BIAS_Z0_WM, &b_AntiKtVR30Rmax4Rmin02TrackJets_eta_TRK_BIAS_Z0_WM);
   fChain->SetBranchAddress("AntiKtVR30Rmax4Rmin02TrackJets_phi_TRK_BIAS_Z0_WM", &AntiKtVR30Rmax4Rmin02TrackJets_phi_TRK_BIAS_Z0_WM, &b_AntiKtVR30Rmax4Rmin02TrackJets_phi_TRK_BIAS_Z0_WM);
   fChain->SetBranchAddress("AntiKtVR30Rmax4Rmin02TrackJets_m_TRK_BIAS_Z0_WM", &AntiKtVR30Rmax4Rmin02TrackJets_m_TRK_BIAS_Z0_WM, &b_AntiKtVR30Rmax4Rmin02TrackJets_m_TRK_BIAS_Z0_WM);
   fChain->SetBranchAddress("AntiKtVR30Rmax4Rmin02TrackJets_daughter__pt_TRK_BIAS_Z0_WM", &AntiKtVR30Rmax4Rmin02TrackJets_daughter__pt_TRK_BIAS_Z0_WM, &b_AntiKtVR30Rmax4Rmin02TrackJets_daughter__pt_TRK_BIAS_Z0_WM);
   fChain->SetBranchAddress("AntiKtVR30Rmax4Rmin02TrackJets_daughter__eta_TRK_BIAS_Z0_WM", &AntiKtVR30Rmax4Rmin02TrackJets_daughter__eta_TRK_BIAS_Z0_WM, &b_AntiKtVR30Rmax4Rmin02TrackJets_daughter__eta_TRK_BIAS_Z0_WM);
   fChain->SetBranchAddress("AntiKtVR30Rmax4Rmin02TrackJets_daughter__phi_TRK_BIAS_Z0_WM", &AntiKtVR30Rmax4Rmin02TrackJets_daughter__phi_TRK_BIAS_Z0_WM, &b_AntiKtVR30Rmax4Rmin02TrackJets_daughter__phi_TRK_BIAS_Z0_WM);
   fChain->SetBranchAddress("AntiKtVR30Rmax4Rmin02TrackJets_daughter__trackId_TRK_BIAS_Z0_WM", &AntiKtVR30Rmax4Rmin02TrackJets_daughter__trackId_TRK_BIAS_Z0_WM, &b_AntiKtVR30Rmax4Rmin02TrackJets_daughter__trackId_TRK_BIAS_Z0_WM);
   fChain->SetBranchAddress("DMesons_costhetastar_TRK_EFF_LOOSE_GLOBAL", &DMesons_costhetastar_TRK_EFF_LOOSE_GLOBAL, &b_DMesons_costhetastar_TRK_EFF_LOOSE_GLOBAL);
   fChain->SetBranchAddress("DMesons_D0Index_TRK_EFF_LOOSE_GLOBAL", &DMesons_D0Index_TRK_EFF_LOOSE_GLOBAL, &b_DMesons_D0Index_TRK_EFF_LOOSE_GLOBAL);
   fChain->SetBranchAddress("DMesons_daughterInfo__eta_TRK_EFF_LOOSE_GLOBAL", &DMesons_daughterInfo__eta_TRK_EFF_LOOSE_GLOBAL, &b_DMesons_daughterInfo__eta_TRK_EFF_LOOSE_GLOBAL);
   fChain->SetBranchAddress("DMesons_daughterInfo__passTight_TRK_EFF_LOOSE_GLOBAL", &DMesons_daughterInfo__passTight_TRK_EFF_LOOSE_GLOBAL, &b_DMesons_daughterInfo__passTight_TRK_EFF_LOOSE_GLOBAL);
   fChain->SetBranchAddress("DMesons_daughterInfo__pdgId_TRK_EFF_LOOSE_GLOBAL", &DMesons_daughterInfo__pdgId_TRK_EFF_LOOSE_GLOBAL, &b_DMesons_daughterInfo__pdgId_TRK_EFF_LOOSE_GLOBAL);
   fChain->SetBranchAddress("DMesons_daughterInfo__phi_TRK_EFF_LOOSE_GLOBAL", &DMesons_daughterInfo__phi_TRK_EFF_LOOSE_GLOBAL, &b_DMesons_daughterInfo__phi_TRK_EFF_LOOSE_GLOBAL);
   fChain->SetBranchAddress("DMesons_daughterInfo__pt_TRK_EFF_LOOSE_GLOBAL", &DMesons_daughterInfo__pt_TRK_EFF_LOOSE_GLOBAL, &b_DMesons_daughterInfo__pt_TRK_EFF_LOOSE_GLOBAL);
   fChain->SetBranchAddress("DMesons_daughterInfo__trackId_TRK_EFF_LOOSE_GLOBAL", &DMesons_daughterInfo__trackId_TRK_EFF_LOOSE_GLOBAL, &b_DMesons_daughterInfo__trackId_TRK_EFF_LOOSE_GLOBAL);
   fChain->SetBranchAddress("DMesons_daughterInfo__truthBarcode_TRK_EFF_LOOSE_GLOBAL", &DMesons_daughterInfo__truthBarcode_TRK_EFF_LOOSE_GLOBAL, &b_DMesons_daughterInfo__truthBarcode_TRK_EFF_LOOSE_GLOBAL);
   fChain->SetBranchAddress("DMesons_daughterInfo__truthDBarcode_TRK_EFF_LOOSE_GLOBAL", &DMesons_daughterInfo__truthDBarcode_TRK_EFF_LOOSE_GLOBAL, &b_DMesons_daughterInfo__truthDBarcode_TRK_EFF_LOOSE_GLOBAL);
   fChain->SetBranchAddress("DMesons_daughterInfo__z0SinTheta_TRK_EFF_LOOSE_GLOBAL", &DMesons_daughterInfo__z0SinTheta_TRK_EFF_LOOSE_GLOBAL, &b_DMesons_daughterInfo__z0SinTheta_TRK_EFF_LOOSE_GLOBAL);
   fChain->SetBranchAddress("DMesons_daughterInfo__z0SinThetaPV_TRK_EFF_LOOSE_GLOBAL", &DMesons_daughterInfo__z0SinThetaPV_TRK_EFF_LOOSE_GLOBAL, &b_DMesons_daughterInfo__z0SinThetaPV_TRK_EFF_LOOSE_GLOBAL);
   fChain->SetBranchAddress("DMesons_decayType_TRK_EFF_LOOSE_GLOBAL", &DMesons_decayType_TRK_EFF_LOOSE_GLOBAL, &b_DMesons_decayType_TRK_EFF_LOOSE_GLOBAL);
   fChain->SetBranchAddress("DMesons_DeltaMass_TRK_EFF_LOOSE_GLOBAL", &DMesons_DeltaMass_TRK_EFF_LOOSE_GLOBAL, &b_DMesons_DeltaMass_TRK_EFF_LOOSE_GLOBAL);
   fChain->SetBranchAddress("DMesons_eta_TRK_EFF_LOOSE_GLOBAL", &DMesons_eta_TRK_EFF_LOOSE_GLOBAL, &b_DMesons_eta_TRK_EFF_LOOSE_GLOBAL);
   fChain->SetBranchAddress("DMesons_fitOutput__Charge_TRK_EFF_LOOSE_GLOBAL", &DMesons_fitOutput__Charge_TRK_EFF_LOOSE_GLOBAL, &b_DMesons_fitOutput__Charge_TRK_EFF_LOOSE_GLOBAL);
   fChain->SetBranchAddress("DMesons_fitOutput__Chi2_TRK_EFF_LOOSE_GLOBAL", &DMesons_fitOutput__Chi2_TRK_EFF_LOOSE_GLOBAL, &b_DMesons_fitOutput__Chi2_TRK_EFF_LOOSE_GLOBAL);
   fChain->SetBranchAddress("DMesons_fitOutput__Impact_TRK_EFF_LOOSE_GLOBAL", &DMesons_fitOutput__Impact_TRK_EFF_LOOSE_GLOBAL, &b_DMesons_fitOutput__Impact_TRK_EFF_LOOSE_GLOBAL);
   fChain->SetBranchAddress("DMesons_fitOutput__ImpactSignificance_TRK_EFF_LOOSE_GLOBAL", &DMesons_fitOutput__ImpactSignificance_TRK_EFF_LOOSE_GLOBAL, &b_DMesons_fitOutput__ImpactSignificance_TRK_EFF_LOOSE_GLOBAL);
   fChain->SetBranchAddress("DMesons_fitOutput__ImpactZ0SinTheta_TRK_EFF_LOOSE_GLOBAL", &DMesons_fitOutput__ImpactZ0SinTheta_TRK_EFF_LOOSE_GLOBAL, &b_DMesons_fitOutput__ImpactZ0SinTheta_TRK_EFF_LOOSE_GLOBAL);
   fChain->SetBranchAddress("DMesons_fitOutput__Lxy_TRK_EFF_LOOSE_GLOBAL", &DMesons_fitOutput__Lxy_TRK_EFF_LOOSE_GLOBAL, &b_DMesons_fitOutput__Lxy_TRK_EFF_LOOSE_GLOBAL);
   fChain->SetBranchAddress("DMesons_fitOutput__VertexPosition_TRK_EFF_LOOSE_GLOBAL", &DMesons_fitOutput__VertexPosition_TRK_EFF_LOOSE_GLOBAL, &b_DMesons_fitOutput__VertexPosition_TRK_EFF_LOOSE_GLOBAL);
   fChain->SetBranchAddress("DMesons_m_TRK_EFF_LOOSE_GLOBAL", &DMesons_m_TRK_EFF_LOOSE_GLOBAL, &b_DMesons_m_TRK_EFF_LOOSE_GLOBAL);
   fChain->SetBranchAddress("DMesons_mKpi1_TRK_EFF_LOOSE_GLOBAL", &DMesons_mKpi1_TRK_EFF_LOOSE_GLOBAL, &b_DMesons_mKpi1_TRK_EFF_LOOSE_GLOBAL);
   fChain->SetBranchAddress("DMesons_mKpi2_TRK_EFF_LOOSE_GLOBAL", &DMesons_mKpi2_TRK_EFF_LOOSE_GLOBAL, &b_DMesons_mKpi2_TRK_EFF_LOOSE_GLOBAL);
   fChain->SetBranchAddress("DMesons_mPhi1_TRK_EFF_LOOSE_GLOBAL", &DMesons_mPhi1_TRK_EFF_LOOSE_GLOBAL, &b_DMesons_mPhi1_TRK_EFF_LOOSE_GLOBAL);
   fChain->SetBranchAddress("DMesons_mPhi2_TRK_EFF_LOOSE_GLOBAL", &DMesons_mPhi2_TRK_EFF_LOOSE_GLOBAL, &b_DMesons_mPhi2_TRK_EFF_LOOSE_GLOBAL);
   fChain->SetBranchAddress("DMesons_pdgId_TRK_EFF_LOOSE_GLOBAL", &DMesons_pdgId_TRK_EFF_LOOSE_GLOBAL, &b_DMesons_pdgId_TRK_EFF_LOOSE_GLOBAL);
   fChain->SetBranchAddress("DMesons_phi_TRK_EFF_LOOSE_GLOBAL", &DMesons_phi_TRK_EFF_LOOSE_GLOBAL, &b_DMesons_phi_TRK_EFF_LOOSE_GLOBAL);
   fChain->SetBranchAddress("DMesons_pt_TRK_EFF_LOOSE_GLOBAL", &DMesons_pt_TRK_EFF_LOOSE_GLOBAL, &b_DMesons_pt_TRK_EFF_LOOSE_GLOBAL);
   fChain->SetBranchAddress("DMesons_ptcone40_TRK_EFF_LOOSE_GLOBAL", &DMesons_ptcone40_TRK_EFF_LOOSE_GLOBAL, &b_DMesons_ptcone40_TRK_EFF_LOOSE_GLOBAL);
   fChain->SetBranchAddress("DMesons_SlowPionD0_TRK_EFF_LOOSE_GLOBAL", &DMesons_SlowPionD0_TRK_EFF_LOOSE_GLOBAL, &b_DMesons_SlowPionD0_TRK_EFF_LOOSE_GLOBAL);
   fChain->SetBranchAddress("DMesons_SlowPionZ0SinTheta_TRK_EFF_LOOSE_GLOBAL", &DMesons_SlowPionZ0SinTheta_TRK_EFF_LOOSE_GLOBAL, &b_DMesons_SlowPionZ0SinTheta_TRK_EFF_LOOSE_GLOBAL);
   fChain->SetBranchAddress("DMesons_truthBarcode_TRK_EFF_LOOSE_GLOBAL", &DMesons_truthBarcode_TRK_EFF_LOOSE_GLOBAL, &b_DMesons_truthBarcode_TRK_EFF_LOOSE_GLOBAL);
   fChain->SetBranchAddress("MesonTracks_d0sig_TRK_EFF_LOOSE_GLOBAL", &MesonTracks_d0sig_TRK_EFF_LOOSE_GLOBAL, &b_MesonTracks_d0sig_TRK_EFF_LOOSE_GLOBAL);
   fChain->SetBranchAddress("MesonTracks_d0sigPV_TRK_EFF_LOOSE_GLOBAL", &MesonTracks_d0sigPV_TRK_EFF_LOOSE_GLOBAL, &b_MesonTracks_d0sigPV_TRK_EFF_LOOSE_GLOBAL);
   fChain->SetBranchAddress("MesonTracks_eta_TRK_EFF_LOOSE_GLOBAL", &MesonTracks_eta_TRK_EFF_LOOSE_GLOBAL, &b_MesonTracks_eta_TRK_EFF_LOOSE_GLOBAL);
   fChain->SetBranchAddress("MesonTracks_passTight_TRK_EFF_LOOSE_GLOBAL", &MesonTracks_passTight_TRK_EFF_LOOSE_GLOBAL, &b_MesonTracks_passTight_TRK_EFF_LOOSE_GLOBAL);
   fChain->SetBranchAddress("MesonTracks_phi_TRK_EFF_LOOSE_GLOBAL", &MesonTracks_phi_TRK_EFF_LOOSE_GLOBAL, &b_MesonTracks_phi_TRK_EFF_LOOSE_GLOBAL);
   fChain->SetBranchAddress("MesonTracks_pt_TRK_EFF_LOOSE_GLOBAL", &MesonTracks_pt_TRK_EFF_LOOSE_GLOBAL, &b_MesonTracks_pt_TRK_EFF_LOOSE_GLOBAL);
   fChain->SetBranchAddress("MesonTracks_trackId_TRK_EFF_LOOSE_GLOBAL", &MesonTracks_trackId_TRK_EFF_LOOSE_GLOBAL, &b_MesonTracks_trackId_TRK_EFF_LOOSE_GLOBAL);
   fChain->SetBranchAddress("MesonTracks_z0sinTheta_TRK_EFF_LOOSE_GLOBAL", &MesonTracks_z0sinTheta_TRK_EFF_LOOSE_GLOBAL, &b_MesonTracks_z0sinTheta_TRK_EFF_LOOSE_GLOBAL);
   fChain->SetBranchAddress("MesonTracks_truthMatchProbability_TRK_EFF_LOOSE_GLOBAL", &MesonTracks_truthMatchProbability_TRK_EFF_LOOSE_GLOBAL, &b_MesonTracks_truthMatchProbability_TRK_EFF_LOOSE_GLOBAL);
   fChain->SetBranchAddress("AntiKt6PV0TrackJets_pt_TRK_EFF_LOOSE_GLOBAL", &AntiKt6PV0TrackJets_pt_TRK_EFF_LOOSE_GLOBAL, &b_AntiKt6PV0TrackJets_pt_TRK_EFF_LOOSE_GLOBAL);
   fChain->SetBranchAddress("AntiKt6PV0TrackJets_eta_TRK_EFF_LOOSE_GLOBAL", &AntiKt6PV0TrackJets_eta_TRK_EFF_LOOSE_GLOBAL, &b_AntiKt6PV0TrackJets_eta_TRK_EFF_LOOSE_GLOBAL);
   fChain->SetBranchAddress("AntiKt6PV0TrackJets_phi_TRK_EFF_LOOSE_GLOBAL", &AntiKt6PV0TrackJets_phi_TRK_EFF_LOOSE_GLOBAL, &b_AntiKt6PV0TrackJets_phi_TRK_EFF_LOOSE_GLOBAL);
   fChain->SetBranchAddress("AntiKt6PV0TrackJets_m_TRK_EFF_LOOSE_GLOBAL", &AntiKt6PV0TrackJets_m_TRK_EFF_LOOSE_GLOBAL, &b_AntiKt6PV0TrackJets_m_TRK_EFF_LOOSE_GLOBAL);
   fChain->SetBranchAddress("AntiKt6PV0TrackJets_daughter__pt_TRK_EFF_LOOSE_GLOBAL", &AntiKt6PV0TrackJets_daughter__pt_TRK_EFF_LOOSE_GLOBAL, &b_AntiKt6PV0TrackJets_daughter__pt_TRK_EFF_LOOSE_GLOBAL);
   fChain->SetBranchAddress("AntiKt6PV0TrackJets_daughter__eta_TRK_EFF_LOOSE_GLOBAL", &AntiKt6PV0TrackJets_daughter__eta_TRK_EFF_LOOSE_GLOBAL, &b_AntiKt6PV0TrackJets_daughter__eta_TRK_EFF_LOOSE_GLOBAL);
   fChain->SetBranchAddress("AntiKt6PV0TrackJets_daughter__phi_TRK_EFF_LOOSE_GLOBAL", &AntiKt6PV0TrackJets_daughter__phi_TRK_EFF_LOOSE_GLOBAL, &b_AntiKt6PV0TrackJets_daughter__phi_TRK_EFF_LOOSE_GLOBAL);
   fChain->SetBranchAddress("AntiKt6PV0TrackJets_daughter__trackId_TRK_EFF_LOOSE_GLOBAL", &AntiKt6PV0TrackJets_daughter__trackId_TRK_EFF_LOOSE_GLOBAL, &b_AntiKt6PV0TrackJets_daughter__trackId_TRK_EFF_LOOSE_GLOBAL);
   fChain->SetBranchAddress("AntiKt8PV0TrackJets_pt_TRK_EFF_LOOSE_GLOBAL", &AntiKt8PV0TrackJets_pt_TRK_EFF_LOOSE_GLOBAL, &b_AntiKt8PV0TrackJets_pt_TRK_EFF_LOOSE_GLOBAL);
   fChain->SetBranchAddress("AntiKt8PV0TrackJets_eta_TRK_EFF_LOOSE_GLOBAL", &AntiKt8PV0TrackJets_eta_TRK_EFF_LOOSE_GLOBAL, &b_AntiKt8PV0TrackJets_eta_TRK_EFF_LOOSE_GLOBAL);
   fChain->SetBranchAddress("AntiKt8PV0TrackJets_phi_TRK_EFF_LOOSE_GLOBAL", &AntiKt8PV0TrackJets_phi_TRK_EFF_LOOSE_GLOBAL, &b_AntiKt8PV0TrackJets_phi_TRK_EFF_LOOSE_GLOBAL);
   fChain->SetBranchAddress("AntiKt8PV0TrackJets_m_TRK_EFF_LOOSE_GLOBAL", &AntiKt8PV0TrackJets_m_TRK_EFF_LOOSE_GLOBAL, &b_AntiKt8PV0TrackJets_m_TRK_EFF_LOOSE_GLOBAL);
   fChain->SetBranchAddress("AntiKt8PV0TrackJets_daughter__pt_TRK_EFF_LOOSE_GLOBAL", &AntiKt8PV0TrackJets_daughter__pt_TRK_EFF_LOOSE_GLOBAL, &b_AntiKt8PV0TrackJets_daughter__pt_TRK_EFF_LOOSE_GLOBAL);
   fChain->SetBranchAddress("AntiKt8PV0TrackJets_daughter__eta_TRK_EFF_LOOSE_GLOBAL", &AntiKt8PV0TrackJets_daughter__eta_TRK_EFF_LOOSE_GLOBAL, &b_AntiKt8PV0TrackJets_daughter__eta_TRK_EFF_LOOSE_GLOBAL);
   fChain->SetBranchAddress("AntiKt8PV0TrackJets_daughter__phi_TRK_EFF_LOOSE_GLOBAL", &AntiKt8PV0TrackJets_daughter__phi_TRK_EFF_LOOSE_GLOBAL, &b_AntiKt8PV0TrackJets_daughter__phi_TRK_EFF_LOOSE_GLOBAL);
   fChain->SetBranchAddress("AntiKt8PV0TrackJets_daughter__trackId_TRK_EFF_LOOSE_GLOBAL", &AntiKt8PV0TrackJets_daughter__trackId_TRK_EFF_LOOSE_GLOBAL, &b_AntiKt8PV0TrackJets_daughter__trackId_TRK_EFF_LOOSE_GLOBAL);
   fChain->SetBranchAddress("AntiKt10PV0TrackJets_pt_TRK_EFF_LOOSE_GLOBAL", &AntiKt10PV0TrackJets_pt_TRK_EFF_LOOSE_GLOBAL, &b_AntiKt10PV0TrackJets_pt_TRK_EFF_LOOSE_GLOBAL);
   fChain->SetBranchAddress("AntiKt10PV0TrackJets_eta_TRK_EFF_LOOSE_GLOBAL", &AntiKt10PV0TrackJets_eta_TRK_EFF_LOOSE_GLOBAL, &b_AntiKt10PV0TrackJets_eta_TRK_EFF_LOOSE_GLOBAL);
   fChain->SetBranchAddress("AntiKt10PV0TrackJets_phi_TRK_EFF_LOOSE_GLOBAL", &AntiKt10PV0TrackJets_phi_TRK_EFF_LOOSE_GLOBAL, &b_AntiKt10PV0TrackJets_phi_TRK_EFF_LOOSE_GLOBAL);
   fChain->SetBranchAddress("AntiKt10PV0TrackJets_m_TRK_EFF_LOOSE_GLOBAL", &AntiKt10PV0TrackJets_m_TRK_EFF_LOOSE_GLOBAL, &b_AntiKt10PV0TrackJets_m_TRK_EFF_LOOSE_GLOBAL);
   fChain->SetBranchAddress("AntiKt10PV0TrackJets_daughter__pt_TRK_EFF_LOOSE_GLOBAL", &AntiKt10PV0TrackJets_daughter__pt_TRK_EFF_LOOSE_GLOBAL, &b_AntiKt10PV0TrackJets_daughter__pt_TRK_EFF_LOOSE_GLOBAL);
   fChain->SetBranchAddress("AntiKt10PV0TrackJets_daughter__eta_TRK_EFF_LOOSE_GLOBAL", &AntiKt10PV0TrackJets_daughter__eta_TRK_EFF_LOOSE_GLOBAL, &b_AntiKt10PV0TrackJets_daughter__eta_TRK_EFF_LOOSE_GLOBAL);
   fChain->SetBranchAddress("AntiKt10PV0TrackJets_daughter__phi_TRK_EFF_LOOSE_GLOBAL", &AntiKt10PV0TrackJets_daughter__phi_TRK_EFF_LOOSE_GLOBAL, &b_AntiKt10PV0TrackJets_daughter__phi_TRK_EFF_LOOSE_GLOBAL);
   fChain->SetBranchAddress("AntiKt10PV0TrackJets_daughter__trackId_TRK_EFF_LOOSE_GLOBAL", &AntiKt10PV0TrackJets_daughter__trackId_TRK_EFF_LOOSE_GLOBAL, &b_AntiKt10PV0TrackJets_daughter__trackId_TRK_EFF_LOOSE_GLOBAL);
   fChain->SetBranchAddress("AntiKtVR30Rmax4Rmin02TrackJets_pt_TRK_EFF_LOOSE_GLOBAL", &AntiKtVR30Rmax4Rmin02TrackJets_pt_TRK_EFF_LOOSE_GLOBAL, &b_AntiKtVR30Rmax4Rmin02TrackJets_pt_TRK_EFF_LOOSE_GLOBAL);
   fChain->SetBranchAddress("AntiKtVR30Rmax4Rmin02TrackJets_eta_TRK_EFF_LOOSE_GLOBAL", &AntiKtVR30Rmax4Rmin02TrackJets_eta_TRK_EFF_LOOSE_GLOBAL, &b_AntiKtVR30Rmax4Rmin02TrackJets_eta_TRK_EFF_LOOSE_GLOBAL);
   fChain->SetBranchAddress("AntiKtVR30Rmax4Rmin02TrackJets_phi_TRK_EFF_LOOSE_GLOBAL", &AntiKtVR30Rmax4Rmin02TrackJets_phi_TRK_EFF_LOOSE_GLOBAL, &b_AntiKtVR30Rmax4Rmin02TrackJets_phi_TRK_EFF_LOOSE_GLOBAL);
   fChain->SetBranchAddress("AntiKtVR30Rmax4Rmin02TrackJets_m_TRK_EFF_LOOSE_GLOBAL", &AntiKtVR30Rmax4Rmin02TrackJets_m_TRK_EFF_LOOSE_GLOBAL, &b_AntiKtVR30Rmax4Rmin02TrackJets_m_TRK_EFF_LOOSE_GLOBAL);
   fChain->SetBranchAddress("AntiKtVR30Rmax4Rmin02TrackJets_daughter__pt_TRK_EFF_LOOSE_GLOBAL", &AntiKtVR30Rmax4Rmin02TrackJets_daughter__pt_TRK_EFF_LOOSE_GLOBAL, &b_AntiKtVR30Rmax4Rmin02TrackJets_daughter__pt_TRK_EFF_LOOSE_GLOBAL);
   fChain->SetBranchAddress("AntiKtVR30Rmax4Rmin02TrackJets_daughter__eta_TRK_EFF_LOOSE_GLOBAL", &AntiKtVR30Rmax4Rmin02TrackJets_daughter__eta_TRK_EFF_LOOSE_GLOBAL, &b_AntiKtVR30Rmax4Rmin02TrackJets_daughter__eta_TRK_EFF_LOOSE_GLOBAL);
   fChain->SetBranchAddress("AntiKtVR30Rmax4Rmin02TrackJets_daughter__phi_TRK_EFF_LOOSE_GLOBAL", &AntiKtVR30Rmax4Rmin02TrackJets_daughter__phi_TRK_EFF_LOOSE_GLOBAL, &b_AntiKtVR30Rmax4Rmin02TrackJets_daughter__phi_TRK_EFF_LOOSE_GLOBAL);
   fChain->SetBranchAddress("AntiKtVR30Rmax4Rmin02TrackJets_daughter__trackId_TRK_EFF_LOOSE_GLOBAL", &AntiKtVR30Rmax4Rmin02TrackJets_daughter__trackId_TRK_EFF_LOOSE_GLOBAL, &b_AntiKtVR30Rmax4Rmin02TrackJets_daughter__trackId_TRK_EFF_LOOSE_GLOBAL);
   fChain->SetBranchAddress("DMesons_costhetastar_TRK_EFF_LOOSE_IBL", &DMesons_costhetastar_TRK_EFF_LOOSE_IBL, &b_DMesons_costhetastar_TRK_EFF_LOOSE_IBL);
   fChain->SetBranchAddress("DMesons_D0Index_TRK_EFF_LOOSE_IBL", &DMesons_D0Index_TRK_EFF_LOOSE_IBL, &b_DMesons_D0Index_TRK_EFF_LOOSE_IBL);
   fChain->SetBranchAddress("DMesons_daughterInfo__eta_TRK_EFF_LOOSE_IBL", &DMesons_daughterInfo__eta_TRK_EFF_LOOSE_IBL, &b_DMesons_daughterInfo__eta_TRK_EFF_LOOSE_IBL);
   fChain->SetBranchAddress("DMesons_daughterInfo__passTight_TRK_EFF_LOOSE_IBL", &DMesons_daughterInfo__passTight_TRK_EFF_LOOSE_IBL, &b_DMesons_daughterInfo__passTight_TRK_EFF_LOOSE_IBL);
   fChain->SetBranchAddress("DMesons_daughterInfo__pdgId_TRK_EFF_LOOSE_IBL", &DMesons_daughterInfo__pdgId_TRK_EFF_LOOSE_IBL, &b_DMesons_daughterInfo__pdgId_TRK_EFF_LOOSE_IBL);
   fChain->SetBranchAddress("DMesons_daughterInfo__phi_TRK_EFF_LOOSE_IBL", &DMesons_daughterInfo__phi_TRK_EFF_LOOSE_IBL, &b_DMesons_daughterInfo__phi_TRK_EFF_LOOSE_IBL);
   fChain->SetBranchAddress("DMesons_daughterInfo__pt_TRK_EFF_LOOSE_IBL", &DMesons_daughterInfo__pt_TRK_EFF_LOOSE_IBL, &b_DMesons_daughterInfo__pt_TRK_EFF_LOOSE_IBL);
   fChain->SetBranchAddress("DMesons_daughterInfo__trackId_TRK_EFF_LOOSE_IBL", &DMesons_daughterInfo__trackId_TRK_EFF_LOOSE_IBL, &b_DMesons_daughterInfo__trackId_TRK_EFF_LOOSE_IBL);
   fChain->SetBranchAddress("DMesons_daughterInfo__truthBarcode_TRK_EFF_LOOSE_IBL", &DMesons_daughterInfo__truthBarcode_TRK_EFF_LOOSE_IBL, &b_DMesons_daughterInfo__truthBarcode_TRK_EFF_LOOSE_IBL);
   fChain->SetBranchAddress("DMesons_daughterInfo__truthDBarcode_TRK_EFF_LOOSE_IBL", &DMesons_daughterInfo__truthDBarcode_TRK_EFF_LOOSE_IBL, &b_DMesons_daughterInfo__truthDBarcode_TRK_EFF_LOOSE_IBL);
   fChain->SetBranchAddress("DMesons_daughterInfo__z0SinTheta_TRK_EFF_LOOSE_IBL", &DMesons_daughterInfo__z0SinTheta_TRK_EFF_LOOSE_IBL, &b_DMesons_daughterInfo__z0SinTheta_TRK_EFF_LOOSE_IBL);
   fChain->SetBranchAddress("DMesons_daughterInfo__z0SinThetaPV_TRK_EFF_LOOSE_IBL", &DMesons_daughterInfo__z0SinThetaPV_TRK_EFF_LOOSE_IBL, &b_DMesons_daughterInfo__z0SinThetaPV_TRK_EFF_LOOSE_IBL);
   fChain->SetBranchAddress("DMesons_decayType_TRK_EFF_LOOSE_IBL", &DMesons_decayType_TRK_EFF_LOOSE_IBL, &b_DMesons_decayType_TRK_EFF_LOOSE_IBL);
   fChain->SetBranchAddress("DMesons_DeltaMass_TRK_EFF_LOOSE_IBL", &DMesons_DeltaMass_TRK_EFF_LOOSE_IBL, &b_DMesons_DeltaMass_TRK_EFF_LOOSE_IBL);
   fChain->SetBranchAddress("DMesons_eta_TRK_EFF_LOOSE_IBL", &DMesons_eta_TRK_EFF_LOOSE_IBL, &b_DMesons_eta_TRK_EFF_LOOSE_IBL);
   fChain->SetBranchAddress("DMesons_fitOutput__Charge_TRK_EFF_LOOSE_IBL", &DMesons_fitOutput__Charge_TRK_EFF_LOOSE_IBL, &b_DMesons_fitOutput__Charge_TRK_EFF_LOOSE_IBL);
   fChain->SetBranchAddress("DMesons_fitOutput__Chi2_TRK_EFF_LOOSE_IBL", &DMesons_fitOutput__Chi2_TRK_EFF_LOOSE_IBL, &b_DMesons_fitOutput__Chi2_TRK_EFF_LOOSE_IBL);
   fChain->SetBranchAddress("DMesons_fitOutput__Impact_TRK_EFF_LOOSE_IBL", &DMesons_fitOutput__Impact_TRK_EFF_LOOSE_IBL, &b_DMesons_fitOutput__Impact_TRK_EFF_LOOSE_IBL);
   fChain->SetBranchAddress("DMesons_fitOutput__ImpactSignificance_TRK_EFF_LOOSE_IBL", &DMesons_fitOutput__ImpactSignificance_TRK_EFF_LOOSE_IBL, &b_DMesons_fitOutput__ImpactSignificance_TRK_EFF_LOOSE_IBL);
   fChain->SetBranchAddress("DMesons_fitOutput__ImpactZ0SinTheta_TRK_EFF_LOOSE_IBL", &DMesons_fitOutput__ImpactZ0SinTheta_TRK_EFF_LOOSE_IBL, &b_DMesons_fitOutput__ImpactZ0SinTheta_TRK_EFF_LOOSE_IBL);
   fChain->SetBranchAddress("DMesons_fitOutput__Lxy_TRK_EFF_LOOSE_IBL", &DMesons_fitOutput__Lxy_TRK_EFF_LOOSE_IBL, &b_DMesons_fitOutput__Lxy_TRK_EFF_LOOSE_IBL);
   fChain->SetBranchAddress("DMesons_fitOutput__VertexPosition_TRK_EFF_LOOSE_IBL", &DMesons_fitOutput__VertexPosition_TRK_EFF_LOOSE_IBL, &b_DMesons_fitOutput__VertexPosition_TRK_EFF_LOOSE_IBL);
   fChain->SetBranchAddress("DMesons_m_TRK_EFF_LOOSE_IBL", &DMesons_m_TRK_EFF_LOOSE_IBL, &b_DMesons_m_TRK_EFF_LOOSE_IBL);
   fChain->SetBranchAddress("DMesons_mKpi1_TRK_EFF_LOOSE_IBL", &DMesons_mKpi1_TRK_EFF_LOOSE_IBL, &b_DMesons_mKpi1_TRK_EFF_LOOSE_IBL);
   fChain->SetBranchAddress("DMesons_mKpi2_TRK_EFF_LOOSE_IBL", &DMesons_mKpi2_TRK_EFF_LOOSE_IBL, &b_DMesons_mKpi2_TRK_EFF_LOOSE_IBL);
   fChain->SetBranchAddress("DMesons_mPhi1_TRK_EFF_LOOSE_IBL", &DMesons_mPhi1_TRK_EFF_LOOSE_IBL, &b_DMesons_mPhi1_TRK_EFF_LOOSE_IBL);
   fChain->SetBranchAddress("DMesons_mPhi2_TRK_EFF_LOOSE_IBL", &DMesons_mPhi2_TRK_EFF_LOOSE_IBL, &b_DMesons_mPhi2_TRK_EFF_LOOSE_IBL);
   fChain->SetBranchAddress("DMesons_pdgId_TRK_EFF_LOOSE_IBL", &DMesons_pdgId_TRK_EFF_LOOSE_IBL, &b_DMesons_pdgId_TRK_EFF_LOOSE_IBL);
   fChain->SetBranchAddress("DMesons_phi_TRK_EFF_LOOSE_IBL", &DMesons_phi_TRK_EFF_LOOSE_IBL, &b_DMesons_phi_TRK_EFF_LOOSE_IBL);
   fChain->SetBranchAddress("DMesons_pt_TRK_EFF_LOOSE_IBL", &DMesons_pt_TRK_EFF_LOOSE_IBL, &b_DMesons_pt_TRK_EFF_LOOSE_IBL);
   fChain->SetBranchAddress("DMesons_ptcone40_TRK_EFF_LOOSE_IBL", &DMesons_ptcone40_TRK_EFF_LOOSE_IBL, &b_DMesons_ptcone40_TRK_EFF_LOOSE_IBL);
   fChain->SetBranchAddress("DMesons_SlowPionD0_TRK_EFF_LOOSE_IBL", &DMesons_SlowPionD0_TRK_EFF_LOOSE_IBL, &b_DMesons_SlowPionD0_TRK_EFF_LOOSE_IBL);
   fChain->SetBranchAddress("DMesons_SlowPionZ0SinTheta_TRK_EFF_LOOSE_IBL", &DMesons_SlowPionZ0SinTheta_TRK_EFF_LOOSE_IBL, &b_DMesons_SlowPionZ0SinTheta_TRK_EFF_LOOSE_IBL);
   fChain->SetBranchAddress("DMesons_truthBarcode_TRK_EFF_LOOSE_IBL", &DMesons_truthBarcode_TRK_EFF_LOOSE_IBL, &b_DMesons_truthBarcode_TRK_EFF_LOOSE_IBL);
   fChain->SetBranchAddress("MesonTracks_d0sig_TRK_EFF_LOOSE_IBL", &MesonTracks_d0sig_TRK_EFF_LOOSE_IBL, &b_MesonTracks_d0sig_TRK_EFF_LOOSE_IBL);
   fChain->SetBranchAddress("MesonTracks_d0sigPV_TRK_EFF_LOOSE_IBL", &MesonTracks_d0sigPV_TRK_EFF_LOOSE_IBL, &b_MesonTracks_d0sigPV_TRK_EFF_LOOSE_IBL);
   fChain->SetBranchAddress("MesonTracks_eta_TRK_EFF_LOOSE_IBL", &MesonTracks_eta_TRK_EFF_LOOSE_IBL, &b_MesonTracks_eta_TRK_EFF_LOOSE_IBL);
   fChain->SetBranchAddress("MesonTracks_passTight_TRK_EFF_LOOSE_IBL", &MesonTracks_passTight_TRK_EFF_LOOSE_IBL, &b_MesonTracks_passTight_TRK_EFF_LOOSE_IBL);
   fChain->SetBranchAddress("MesonTracks_phi_TRK_EFF_LOOSE_IBL", &MesonTracks_phi_TRK_EFF_LOOSE_IBL, &b_MesonTracks_phi_TRK_EFF_LOOSE_IBL);
   fChain->SetBranchAddress("MesonTracks_pt_TRK_EFF_LOOSE_IBL", &MesonTracks_pt_TRK_EFF_LOOSE_IBL, &b_MesonTracks_pt_TRK_EFF_LOOSE_IBL);
   fChain->SetBranchAddress("MesonTracks_trackId_TRK_EFF_LOOSE_IBL", &MesonTracks_trackId_TRK_EFF_LOOSE_IBL, &b_MesonTracks_trackId_TRK_EFF_LOOSE_IBL);
   fChain->SetBranchAddress("MesonTracks_z0sinTheta_TRK_EFF_LOOSE_IBL", &MesonTracks_z0sinTheta_TRK_EFF_LOOSE_IBL, &b_MesonTracks_z0sinTheta_TRK_EFF_LOOSE_IBL);
   fChain->SetBranchAddress("MesonTracks_truthMatchProbability_TRK_EFF_LOOSE_IBL", &MesonTracks_truthMatchProbability_TRK_EFF_LOOSE_IBL, &b_MesonTracks_truthMatchProbability_TRK_EFF_LOOSE_IBL);
   fChain->SetBranchAddress("AntiKt6PV0TrackJets_pt_TRK_EFF_LOOSE_IBL", &AntiKt6PV0TrackJets_pt_TRK_EFF_LOOSE_IBL, &b_AntiKt6PV0TrackJets_pt_TRK_EFF_LOOSE_IBL);
   fChain->SetBranchAddress("AntiKt6PV0TrackJets_eta_TRK_EFF_LOOSE_IBL", &AntiKt6PV0TrackJets_eta_TRK_EFF_LOOSE_IBL, &b_AntiKt6PV0TrackJets_eta_TRK_EFF_LOOSE_IBL);
   fChain->SetBranchAddress("AntiKt6PV0TrackJets_phi_TRK_EFF_LOOSE_IBL", &AntiKt6PV0TrackJets_phi_TRK_EFF_LOOSE_IBL, &b_AntiKt6PV0TrackJets_phi_TRK_EFF_LOOSE_IBL);
   fChain->SetBranchAddress("AntiKt6PV0TrackJets_m_TRK_EFF_LOOSE_IBL", &AntiKt6PV0TrackJets_m_TRK_EFF_LOOSE_IBL, &b_AntiKt6PV0TrackJets_m_TRK_EFF_LOOSE_IBL);
   fChain->SetBranchAddress("AntiKt6PV0TrackJets_daughter__pt_TRK_EFF_LOOSE_IBL", &AntiKt6PV0TrackJets_daughter__pt_TRK_EFF_LOOSE_IBL, &b_AntiKt6PV0TrackJets_daughter__pt_TRK_EFF_LOOSE_IBL);
   fChain->SetBranchAddress("AntiKt6PV0TrackJets_daughter__eta_TRK_EFF_LOOSE_IBL", &AntiKt6PV0TrackJets_daughter__eta_TRK_EFF_LOOSE_IBL, &b_AntiKt6PV0TrackJets_daughter__eta_TRK_EFF_LOOSE_IBL);
   fChain->SetBranchAddress("AntiKt6PV0TrackJets_daughter__phi_TRK_EFF_LOOSE_IBL", &AntiKt6PV0TrackJets_daughter__phi_TRK_EFF_LOOSE_IBL, &b_AntiKt6PV0TrackJets_daughter__phi_TRK_EFF_LOOSE_IBL);
   fChain->SetBranchAddress("AntiKt6PV0TrackJets_daughter__trackId_TRK_EFF_LOOSE_IBL", &AntiKt6PV0TrackJets_daughter__trackId_TRK_EFF_LOOSE_IBL, &b_AntiKt6PV0TrackJets_daughter__trackId_TRK_EFF_LOOSE_IBL);
   fChain->SetBranchAddress("AntiKt8PV0TrackJets_pt_TRK_EFF_LOOSE_IBL", &AntiKt8PV0TrackJets_pt_TRK_EFF_LOOSE_IBL, &b_AntiKt8PV0TrackJets_pt_TRK_EFF_LOOSE_IBL);
   fChain->SetBranchAddress("AntiKt8PV0TrackJets_eta_TRK_EFF_LOOSE_IBL", &AntiKt8PV0TrackJets_eta_TRK_EFF_LOOSE_IBL, &b_AntiKt8PV0TrackJets_eta_TRK_EFF_LOOSE_IBL);
   fChain->SetBranchAddress("AntiKt8PV0TrackJets_phi_TRK_EFF_LOOSE_IBL", &AntiKt8PV0TrackJets_phi_TRK_EFF_LOOSE_IBL, &b_AntiKt8PV0TrackJets_phi_TRK_EFF_LOOSE_IBL);
   fChain->SetBranchAddress("AntiKt8PV0TrackJets_m_TRK_EFF_LOOSE_IBL", &AntiKt8PV0TrackJets_m_TRK_EFF_LOOSE_IBL, &b_AntiKt8PV0TrackJets_m_TRK_EFF_LOOSE_IBL);
   fChain->SetBranchAddress("AntiKt8PV0TrackJets_daughter__pt_TRK_EFF_LOOSE_IBL", &AntiKt8PV0TrackJets_daughter__pt_TRK_EFF_LOOSE_IBL, &b_AntiKt8PV0TrackJets_daughter__pt_TRK_EFF_LOOSE_IBL);
   fChain->SetBranchAddress("AntiKt8PV0TrackJets_daughter__eta_TRK_EFF_LOOSE_IBL", &AntiKt8PV0TrackJets_daughter__eta_TRK_EFF_LOOSE_IBL, &b_AntiKt8PV0TrackJets_daughter__eta_TRK_EFF_LOOSE_IBL);
   fChain->SetBranchAddress("AntiKt8PV0TrackJets_daughter__phi_TRK_EFF_LOOSE_IBL", &AntiKt8PV0TrackJets_daughter__phi_TRK_EFF_LOOSE_IBL, &b_AntiKt8PV0TrackJets_daughter__phi_TRK_EFF_LOOSE_IBL);
   fChain->SetBranchAddress("AntiKt8PV0TrackJets_daughter__trackId_TRK_EFF_LOOSE_IBL", &AntiKt8PV0TrackJets_daughter__trackId_TRK_EFF_LOOSE_IBL, &b_AntiKt8PV0TrackJets_daughter__trackId_TRK_EFF_LOOSE_IBL);
   fChain->SetBranchAddress("AntiKt10PV0TrackJets_pt_TRK_EFF_LOOSE_IBL", &AntiKt10PV0TrackJets_pt_TRK_EFF_LOOSE_IBL, &b_AntiKt10PV0TrackJets_pt_TRK_EFF_LOOSE_IBL);
   fChain->SetBranchAddress("AntiKt10PV0TrackJets_eta_TRK_EFF_LOOSE_IBL", &AntiKt10PV0TrackJets_eta_TRK_EFF_LOOSE_IBL, &b_AntiKt10PV0TrackJets_eta_TRK_EFF_LOOSE_IBL);
   fChain->SetBranchAddress("AntiKt10PV0TrackJets_phi_TRK_EFF_LOOSE_IBL", &AntiKt10PV0TrackJets_phi_TRK_EFF_LOOSE_IBL, &b_AntiKt10PV0TrackJets_phi_TRK_EFF_LOOSE_IBL);
   fChain->SetBranchAddress("AntiKt10PV0TrackJets_m_TRK_EFF_LOOSE_IBL", &AntiKt10PV0TrackJets_m_TRK_EFF_LOOSE_IBL, &b_AntiKt10PV0TrackJets_m_TRK_EFF_LOOSE_IBL);
   fChain->SetBranchAddress("AntiKt10PV0TrackJets_daughter__pt_TRK_EFF_LOOSE_IBL", &AntiKt10PV0TrackJets_daughter__pt_TRK_EFF_LOOSE_IBL, &b_AntiKt10PV0TrackJets_daughter__pt_TRK_EFF_LOOSE_IBL);
   fChain->SetBranchAddress("AntiKt10PV0TrackJets_daughter__eta_TRK_EFF_LOOSE_IBL", &AntiKt10PV0TrackJets_daughter__eta_TRK_EFF_LOOSE_IBL, &b_AntiKt10PV0TrackJets_daughter__eta_TRK_EFF_LOOSE_IBL);
   fChain->SetBranchAddress("AntiKt10PV0TrackJets_daughter__phi_TRK_EFF_LOOSE_IBL", &AntiKt10PV0TrackJets_daughter__phi_TRK_EFF_LOOSE_IBL, &b_AntiKt10PV0TrackJets_daughter__phi_TRK_EFF_LOOSE_IBL);
   fChain->SetBranchAddress("AntiKt10PV0TrackJets_daughter__trackId_TRK_EFF_LOOSE_IBL", &AntiKt10PV0TrackJets_daughter__trackId_TRK_EFF_LOOSE_IBL, &b_AntiKt10PV0TrackJets_daughter__trackId_TRK_EFF_LOOSE_IBL);
   fChain->SetBranchAddress("AntiKtVR30Rmax4Rmin02TrackJets_pt_TRK_EFF_LOOSE_IBL", &AntiKtVR30Rmax4Rmin02TrackJets_pt_TRK_EFF_LOOSE_IBL, &b_AntiKtVR30Rmax4Rmin02TrackJets_pt_TRK_EFF_LOOSE_IBL);
   fChain->SetBranchAddress("AntiKtVR30Rmax4Rmin02TrackJets_eta_TRK_EFF_LOOSE_IBL", &AntiKtVR30Rmax4Rmin02TrackJets_eta_TRK_EFF_LOOSE_IBL, &b_AntiKtVR30Rmax4Rmin02TrackJets_eta_TRK_EFF_LOOSE_IBL);
   fChain->SetBranchAddress("AntiKtVR30Rmax4Rmin02TrackJets_phi_TRK_EFF_LOOSE_IBL", &AntiKtVR30Rmax4Rmin02TrackJets_phi_TRK_EFF_LOOSE_IBL, &b_AntiKtVR30Rmax4Rmin02TrackJets_phi_TRK_EFF_LOOSE_IBL);
   fChain->SetBranchAddress("AntiKtVR30Rmax4Rmin02TrackJets_m_TRK_EFF_LOOSE_IBL", &AntiKtVR30Rmax4Rmin02TrackJets_m_TRK_EFF_LOOSE_IBL, &b_AntiKtVR30Rmax4Rmin02TrackJets_m_TRK_EFF_LOOSE_IBL);
   fChain->SetBranchAddress("AntiKtVR30Rmax4Rmin02TrackJets_daughter__pt_TRK_EFF_LOOSE_IBL", &AntiKtVR30Rmax4Rmin02TrackJets_daughter__pt_TRK_EFF_LOOSE_IBL, &b_AntiKtVR30Rmax4Rmin02TrackJets_daughter__pt_TRK_EFF_LOOSE_IBL);
   fChain->SetBranchAddress("AntiKtVR30Rmax4Rmin02TrackJets_daughter__eta_TRK_EFF_LOOSE_IBL", &AntiKtVR30Rmax4Rmin02TrackJets_daughter__eta_TRK_EFF_LOOSE_IBL, &b_AntiKtVR30Rmax4Rmin02TrackJets_daughter__eta_TRK_EFF_LOOSE_IBL);
   fChain->SetBranchAddress("AntiKtVR30Rmax4Rmin02TrackJets_daughter__phi_TRK_EFF_LOOSE_IBL", &AntiKtVR30Rmax4Rmin02TrackJets_daughter__phi_TRK_EFF_LOOSE_IBL, &b_AntiKtVR30Rmax4Rmin02TrackJets_daughter__phi_TRK_EFF_LOOSE_IBL);
   fChain->SetBranchAddress("AntiKtVR30Rmax4Rmin02TrackJets_daughter__trackId_TRK_EFF_LOOSE_IBL", &AntiKtVR30Rmax4Rmin02TrackJets_daughter__trackId_TRK_EFF_LOOSE_IBL, &b_AntiKtVR30Rmax4Rmin02TrackJets_daughter__trackId_TRK_EFF_LOOSE_IBL);
   fChain->SetBranchAddress("DMesons_costhetastar_TRK_EFF_LOOSE_PHYSMODEL", &DMesons_costhetastar_TRK_EFF_LOOSE_PHYSMODEL, &b_DMesons_costhetastar_TRK_EFF_LOOSE_PHYSMODEL);
   fChain->SetBranchAddress("DMesons_D0Index_TRK_EFF_LOOSE_PHYSMODEL", &DMesons_D0Index_TRK_EFF_LOOSE_PHYSMODEL, &b_DMesons_D0Index_TRK_EFF_LOOSE_PHYSMODEL);
   fChain->SetBranchAddress("DMesons_daughterInfo__eta_TRK_EFF_LOOSE_PHYSMODEL", &DMesons_daughterInfo__eta_TRK_EFF_LOOSE_PHYSMODEL, &b_DMesons_daughterInfo__eta_TRK_EFF_LOOSE_PHYSMODEL);
   fChain->SetBranchAddress("DMesons_daughterInfo__passTight_TRK_EFF_LOOSE_PHYSMODEL", &DMesons_daughterInfo__passTight_TRK_EFF_LOOSE_PHYSMODEL, &b_DMesons_daughterInfo__passTight_TRK_EFF_LOOSE_PHYSMODEL);
   fChain->SetBranchAddress("DMesons_daughterInfo__pdgId_TRK_EFF_LOOSE_PHYSMODEL", &DMesons_daughterInfo__pdgId_TRK_EFF_LOOSE_PHYSMODEL, &b_DMesons_daughterInfo__pdgId_TRK_EFF_LOOSE_PHYSMODEL);
   fChain->SetBranchAddress("DMesons_daughterInfo__phi_TRK_EFF_LOOSE_PHYSMODEL", &DMesons_daughterInfo__phi_TRK_EFF_LOOSE_PHYSMODEL, &b_DMesons_daughterInfo__phi_TRK_EFF_LOOSE_PHYSMODEL);
   fChain->SetBranchAddress("DMesons_daughterInfo__pt_TRK_EFF_LOOSE_PHYSMODEL", &DMesons_daughterInfo__pt_TRK_EFF_LOOSE_PHYSMODEL, &b_DMesons_daughterInfo__pt_TRK_EFF_LOOSE_PHYSMODEL);
   fChain->SetBranchAddress("DMesons_daughterInfo__trackId_TRK_EFF_LOOSE_PHYSMODEL", &DMesons_daughterInfo__trackId_TRK_EFF_LOOSE_PHYSMODEL, &b_DMesons_daughterInfo__trackId_TRK_EFF_LOOSE_PHYSMODEL);
   fChain->SetBranchAddress("DMesons_daughterInfo__truthBarcode_TRK_EFF_LOOSE_PHYSMODEL", &DMesons_daughterInfo__truthBarcode_TRK_EFF_LOOSE_PHYSMODEL, &b_DMesons_daughterInfo__truthBarcode_TRK_EFF_LOOSE_PHYSMODEL);
   fChain->SetBranchAddress("DMesons_daughterInfo__truthDBarcode_TRK_EFF_LOOSE_PHYSMODEL", &DMesons_daughterInfo__truthDBarcode_TRK_EFF_LOOSE_PHYSMODEL, &b_DMesons_daughterInfo__truthDBarcode_TRK_EFF_LOOSE_PHYSMODEL);
   fChain->SetBranchAddress("DMesons_daughterInfo__z0SinTheta_TRK_EFF_LOOSE_PHYSMODEL", &DMesons_daughterInfo__z0SinTheta_TRK_EFF_LOOSE_PHYSMODEL, &b_DMesons_daughterInfo__z0SinTheta_TRK_EFF_LOOSE_PHYSMODEL);
   fChain->SetBranchAddress("DMesons_daughterInfo__z0SinThetaPV_TRK_EFF_LOOSE_PHYSMODEL", &DMesons_daughterInfo__z0SinThetaPV_TRK_EFF_LOOSE_PHYSMODEL, &b_DMesons_daughterInfo__z0SinThetaPV_TRK_EFF_LOOSE_PHYSMODEL);
   fChain->SetBranchAddress("DMesons_decayType_TRK_EFF_LOOSE_PHYSMODEL", &DMesons_decayType_TRK_EFF_LOOSE_PHYSMODEL, &b_DMesons_decayType_TRK_EFF_LOOSE_PHYSMODEL);
   fChain->SetBranchAddress("DMesons_DeltaMass_TRK_EFF_LOOSE_PHYSMODEL", &DMesons_DeltaMass_TRK_EFF_LOOSE_PHYSMODEL, &b_DMesons_DeltaMass_TRK_EFF_LOOSE_PHYSMODEL);
   fChain->SetBranchAddress("DMesons_eta_TRK_EFF_LOOSE_PHYSMODEL", &DMesons_eta_TRK_EFF_LOOSE_PHYSMODEL, &b_DMesons_eta_TRK_EFF_LOOSE_PHYSMODEL);
   fChain->SetBranchAddress("DMesons_fitOutput__Charge_TRK_EFF_LOOSE_PHYSMODEL", &DMesons_fitOutput__Charge_TRK_EFF_LOOSE_PHYSMODEL, &b_DMesons_fitOutput__Charge_TRK_EFF_LOOSE_PHYSMODEL);
   fChain->SetBranchAddress("DMesons_fitOutput__Chi2_TRK_EFF_LOOSE_PHYSMODEL", &DMesons_fitOutput__Chi2_TRK_EFF_LOOSE_PHYSMODEL, &b_DMesons_fitOutput__Chi2_TRK_EFF_LOOSE_PHYSMODEL);
   fChain->SetBranchAddress("DMesons_fitOutput__Impact_TRK_EFF_LOOSE_PHYSMODEL", &DMesons_fitOutput__Impact_TRK_EFF_LOOSE_PHYSMODEL, &b_DMesons_fitOutput__Impact_TRK_EFF_LOOSE_PHYSMODEL);
   fChain->SetBranchAddress("DMesons_fitOutput__ImpactSignificance_TRK_EFF_LOOSE_PHYSMODEL", &DMesons_fitOutput__ImpactSignificance_TRK_EFF_LOOSE_PHYSMODEL, &b_DMesons_fitOutput__ImpactSignificance_TRK_EFF_LOOSE_PHYSMODEL);
   fChain->SetBranchAddress("DMesons_fitOutput__ImpactZ0SinTheta_TRK_EFF_LOOSE_PHYSMODEL", &DMesons_fitOutput__ImpactZ0SinTheta_TRK_EFF_LOOSE_PHYSMODEL, &b_DMesons_fitOutput__ImpactZ0SinTheta_TRK_EFF_LOOSE_PHYSMODEL);
   fChain->SetBranchAddress("DMesons_fitOutput__Lxy_TRK_EFF_LOOSE_PHYSMODEL", &DMesons_fitOutput__Lxy_TRK_EFF_LOOSE_PHYSMODEL, &b_DMesons_fitOutput__Lxy_TRK_EFF_LOOSE_PHYSMODEL);
   fChain->SetBranchAddress("DMesons_fitOutput__VertexPosition_TRK_EFF_LOOSE_PHYSMODEL", &DMesons_fitOutput__VertexPosition_TRK_EFF_LOOSE_PHYSMODEL, &b_DMesons_fitOutput__VertexPosition_TRK_EFF_LOOSE_PHYSMODEL);
   fChain->SetBranchAddress("DMesons_m_TRK_EFF_LOOSE_PHYSMODEL", &DMesons_m_TRK_EFF_LOOSE_PHYSMODEL, &b_DMesons_m_TRK_EFF_LOOSE_PHYSMODEL);
   fChain->SetBranchAddress("DMesons_mKpi1_TRK_EFF_LOOSE_PHYSMODEL", &DMesons_mKpi1_TRK_EFF_LOOSE_PHYSMODEL, &b_DMesons_mKpi1_TRK_EFF_LOOSE_PHYSMODEL);
   fChain->SetBranchAddress("DMesons_mKpi2_TRK_EFF_LOOSE_PHYSMODEL", &DMesons_mKpi2_TRK_EFF_LOOSE_PHYSMODEL, &b_DMesons_mKpi2_TRK_EFF_LOOSE_PHYSMODEL);
   fChain->SetBranchAddress("DMesons_mPhi1_TRK_EFF_LOOSE_PHYSMODEL", &DMesons_mPhi1_TRK_EFF_LOOSE_PHYSMODEL, &b_DMesons_mPhi1_TRK_EFF_LOOSE_PHYSMODEL);
   fChain->SetBranchAddress("DMesons_mPhi2_TRK_EFF_LOOSE_PHYSMODEL", &DMesons_mPhi2_TRK_EFF_LOOSE_PHYSMODEL, &b_DMesons_mPhi2_TRK_EFF_LOOSE_PHYSMODEL);
   fChain->SetBranchAddress("DMesons_pdgId_TRK_EFF_LOOSE_PHYSMODEL", &DMesons_pdgId_TRK_EFF_LOOSE_PHYSMODEL, &b_DMesons_pdgId_TRK_EFF_LOOSE_PHYSMODEL);
   fChain->SetBranchAddress("DMesons_phi_TRK_EFF_LOOSE_PHYSMODEL", &DMesons_phi_TRK_EFF_LOOSE_PHYSMODEL, &b_DMesons_phi_TRK_EFF_LOOSE_PHYSMODEL);
   fChain->SetBranchAddress("DMesons_pt_TRK_EFF_LOOSE_PHYSMODEL", &DMesons_pt_TRK_EFF_LOOSE_PHYSMODEL, &b_DMesons_pt_TRK_EFF_LOOSE_PHYSMODEL);
   fChain->SetBranchAddress("DMesons_ptcone40_TRK_EFF_LOOSE_PHYSMODEL", &DMesons_ptcone40_TRK_EFF_LOOSE_PHYSMODEL, &b_DMesons_ptcone40_TRK_EFF_LOOSE_PHYSMODEL);
   fChain->SetBranchAddress("DMesons_SlowPionD0_TRK_EFF_LOOSE_PHYSMODEL", &DMesons_SlowPionD0_TRK_EFF_LOOSE_PHYSMODEL, &b_DMesons_SlowPionD0_TRK_EFF_LOOSE_PHYSMODEL);
   fChain->SetBranchAddress("DMesons_SlowPionZ0SinTheta_TRK_EFF_LOOSE_PHYSMODEL", &DMesons_SlowPionZ0SinTheta_TRK_EFF_LOOSE_PHYSMODEL, &b_DMesons_SlowPionZ0SinTheta_TRK_EFF_LOOSE_PHYSMODEL);
   fChain->SetBranchAddress("DMesons_truthBarcode_TRK_EFF_LOOSE_PHYSMODEL", &DMesons_truthBarcode_TRK_EFF_LOOSE_PHYSMODEL, &b_DMesons_truthBarcode_TRK_EFF_LOOSE_PHYSMODEL);
   fChain->SetBranchAddress("MesonTracks_d0sig_TRK_EFF_LOOSE_PHYSMODEL", &MesonTracks_d0sig_TRK_EFF_LOOSE_PHYSMODEL, &b_MesonTracks_d0sig_TRK_EFF_LOOSE_PHYSMODEL);
   fChain->SetBranchAddress("MesonTracks_d0sigPV_TRK_EFF_LOOSE_PHYSMODEL", &MesonTracks_d0sigPV_TRK_EFF_LOOSE_PHYSMODEL, &b_MesonTracks_d0sigPV_TRK_EFF_LOOSE_PHYSMODEL);
   fChain->SetBranchAddress("MesonTracks_eta_TRK_EFF_LOOSE_PHYSMODEL", &MesonTracks_eta_TRK_EFF_LOOSE_PHYSMODEL, &b_MesonTracks_eta_TRK_EFF_LOOSE_PHYSMODEL);
   fChain->SetBranchAddress("MesonTracks_passTight_TRK_EFF_LOOSE_PHYSMODEL", &MesonTracks_passTight_TRK_EFF_LOOSE_PHYSMODEL, &b_MesonTracks_passTight_TRK_EFF_LOOSE_PHYSMODEL);
   fChain->SetBranchAddress("MesonTracks_phi_TRK_EFF_LOOSE_PHYSMODEL", &MesonTracks_phi_TRK_EFF_LOOSE_PHYSMODEL, &b_MesonTracks_phi_TRK_EFF_LOOSE_PHYSMODEL);
   fChain->SetBranchAddress("MesonTracks_pt_TRK_EFF_LOOSE_PHYSMODEL", &MesonTracks_pt_TRK_EFF_LOOSE_PHYSMODEL, &b_MesonTracks_pt_TRK_EFF_LOOSE_PHYSMODEL);
   fChain->SetBranchAddress("MesonTracks_trackId_TRK_EFF_LOOSE_PHYSMODEL", &MesonTracks_trackId_TRK_EFF_LOOSE_PHYSMODEL, &b_MesonTracks_trackId_TRK_EFF_LOOSE_PHYSMODEL);
   fChain->SetBranchAddress("MesonTracks_z0sinTheta_TRK_EFF_LOOSE_PHYSMODEL", &MesonTracks_z0sinTheta_TRK_EFF_LOOSE_PHYSMODEL, &b_MesonTracks_z0sinTheta_TRK_EFF_LOOSE_PHYSMODEL);
   fChain->SetBranchAddress("MesonTracks_truthMatchProbability_TRK_EFF_LOOSE_PHYSMODEL", &MesonTracks_truthMatchProbability_TRK_EFF_LOOSE_PHYSMODEL, &b_MesonTracks_truthMatchProbability_TRK_EFF_LOOSE_PHYSMODEL);
   fChain->SetBranchAddress("AntiKt6PV0TrackJets_pt_TRK_EFF_LOOSE_PHYSMODEL", &AntiKt6PV0TrackJets_pt_TRK_EFF_LOOSE_PHYSMODEL, &b_AntiKt6PV0TrackJets_pt_TRK_EFF_LOOSE_PHYSMODEL);
   fChain->SetBranchAddress("AntiKt6PV0TrackJets_eta_TRK_EFF_LOOSE_PHYSMODEL", &AntiKt6PV0TrackJets_eta_TRK_EFF_LOOSE_PHYSMODEL, &b_AntiKt6PV0TrackJets_eta_TRK_EFF_LOOSE_PHYSMODEL);
   fChain->SetBranchAddress("AntiKt6PV0TrackJets_phi_TRK_EFF_LOOSE_PHYSMODEL", &AntiKt6PV0TrackJets_phi_TRK_EFF_LOOSE_PHYSMODEL, &b_AntiKt6PV0TrackJets_phi_TRK_EFF_LOOSE_PHYSMODEL);
   fChain->SetBranchAddress("AntiKt6PV0TrackJets_m_TRK_EFF_LOOSE_PHYSMODEL", &AntiKt6PV0TrackJets_m_TRK_EFF_LOOSE_PHYSMODEL, &b_AntiKt6PV0TrackJets_m_TRK_EFF_LOOSE_PHYSMODEL);
   fChain->SetBranchAddress("AntiKt6PV0TrackJets_daughter__pt_TRK_EFF_LOOSE_PHYSMODEL", &AntiKt6PV0TrackJets_daughter__pt_TRK_EFF_LOOSE_PHYSMODEL, &b_AntiKt6PV0TrackJets_daughter__pt_TRK_EFF_LOOSE_PHYSMODEL);
   fChain->SetBranchAddress("AntiKt6PV0TrackJets_daughter__eta_TRK_EFF_LOOSE_PHYSMODEL", &AntiKt6PV0TrackJets_daughter__eta_TRK_EFF_LOOSE_PHYSMODEL, &b_AntiKt6PV0TrackJets_daughter__eta_TRK_EFF_LOOSE_PHYSMODEL);
   fChain->SetBranchAddress("AntiKt6PV0TrackJets_daughter__phi_TRK_EFF_LOOSE_PHYSMODEL", &AntiKt6PV0TrackJets_daughter__phi_TRK_EFF_LOOSE_PHYSMODEL, &b_AntiKt6PV0TrackJets_daughter__phi_TRK_EFF_LOOSE_PHYSMODEL);
   fChain->SetBranchAddress("AntiKt6PV0TrackJets_daughter__trackId_TRK_EFF_LOOSE_PHYSMODEL", &AntiKt6PV0TrackJets_daughter__trackId_TRK_EFF_LOOSE_PHYSMODEL, &b_AntiKt6PV0TrackJets_daughter__trackId_TRK_EFF_LOOSE_PHYSMODEL);
   fChain->SetBranchAddress("AntiKt8PV0TrackJets_pt_TRK_EFF_LOOSE_PHYSMODEL", &AntiKt8PV0TrackJets_pt_TRK_EFF_LOOSE_PHYSMODEL, &b_AntiKt8PV0TrackJets_pt_TRK_EFF_LOOSE_PHYSMODEL);
   fChain->SetBranchAddress("AntiKt8PV0TrackJets_eta_TRK_EFF_LOOSE_PHYSMODEL", &AntiKt8PV0TrackJets_eta_TRK_EFF_LOOSE_PHYSMODEL, &b_AntiKt8PV0TrackJets_eta_TRK_EFF_LOOSE_PHYSMODEL);
   fChain->SetBranchAddress("AntiKt8PV0TrackJets_phi_TRK_EFF_LOOSE_PHYSMODEL", &AntiKt8PV0TrackJets_phi_TRK_EFF_LOOSE_PHYSMODEL, &b_AntiKt8PV0TrackJets_phi_TRK_EFF_LOOSE_PHYSMODEL);
   fChain->SetBranchAddress("AntiKt8PV0TrackJets_m_TRK_EFF_LOOSE_PHYSMODEL", &AntiKt8PV0TrackJets_m_TRK_EFF_LOOSE_PHYSMODEL, &b_AntiKt8PV0TrackJets_m_TRK_EFF_LOOSE_PHYSMODEL);
   fChain->SetBranchAddress("AntiKt8PV0TrackJets_daughter__pt_TRK_EFF_LOOSE_PHYSMODEL", &AntiKt8PV0TrackJets_daughter__pt_TRK_EFF_LOOSE_PHYSMODEL, &b_AntiKt8PV0TrackJets_daughter__pt_TRK_EFF_LOOSE_PHYSMODEL);
   fChain->SetBranchAddress("AntiKt8PV0TrackJets_daughter__eta_TRK_EFF_LOOSE_PHYSMODEL", &AntiKt8PV0TrackJets_daughter__eta_TRK_EFF_LOOSE_PHYSMODEL, &b_AntiKt8PV0TrackJets_daughter__eta_TRK_EFF_LOOSE_PHYSMODEL);
   fChain->SetBranchAddress("AntiKt8PV0TrackJets_daughter__phi_TRK_EFF_LOOSE_PHYSMODEL", &AntiKt8PV0TrackJets_daughter__phi_TRK_EFF_LOOSE_PHYSMODEL, &b_AntiKt8PV0TrackJets_daughter__phi_TRK_EFF_LOOSE_PHYSMODEL);
   fChain->SetBranchAddress("AntiKt8PV0TrackJets_daughter__trackId_TRK_EFF_LOOSE_PHYSMODEL", &AntiKt8PV0TrackJets_daughter__trackId_TRK_EFF_LOOSE_PHYSMODEL, &b_AntiKt8PV0TrackJets_daughter__trackId_TRK_EFF_LOOSE_PHYSMODEL);
   fChain->SetBranchAddress("AntiKt10PV0TrackJets_pt_TRK_EFF_LOOSE_PHYSMODEL", &AntiKt10PV0TrackJets_pt_TRK_EFF_LOOSE_PHYSMODEL, &b_AntiKt10PV0TrackJets_pt_TRK_EFF_LOOSE_PHYSMODEL);
   fChain->SetBranchAddress("AntiKt10PV0TrackJets_eta_TRK_EFF_LOOSE_PHYSMODEL", &AntiKt10PV0TrackJets_eta_TRK_EFF_LOOSE_PHYSMODEL, &b_AntiKt10PV0TrackJets_eta_TRK_EFF_LOOSE_PHYSMODEL);
   fChain->SetBranchAddress("AntiKt10PV0TrackJets_phi_TRK_EFF_LOOSE_PHYSMODEL", &AntiKt10PV0TrackJets_phi_TRK_EFF_LOOSE_PHYSMODEL, &b_AntiKt10PV0TrackJets_phi_TRK_EFF_LOOSE_PHYSMODEL);
   fChain->SetBranchAddress("AntiKt10PV0TrackJets_m_TRK_EFF_LOOSE_PHYSMODEL", &AntiKt10PV0TrackJets_m_TRK_EFF_LOOSE_PHYSMODEL, &b_AntiKt10PV0TrackJets_m_TRK_EFF_LOOSE_PHYSMODEL);
   fChain->SetBranchAddress("AntiKt10PV0TrackJets_daughter__pt_TRK_EFF_LOOSE_PHYSMODEL", &AntiKt10PV0TrackJets_daughter__pt_TRK_EFF_LOOSE_PHYSMODEL, &b_AntiKt10PV0TrackJets_daughter__pt_TRK_EFF_LOOSE_PHYSMODEL);
   fChain->SetBranchAddress("AntiKt10PV0TrackJets_daughter__eta_TRK_EFF_LOOSE_PHYSMODEL", &AntiKt10PV0TrackJets_daughter__eta_TRK_EFF_LOOSE_PHYSMODEL, &b_AntiKt10PV0TrackJets_daughter__eta_TRK_EFF_LOOSE_PHYSMODEL);
   fChain->SetBranchAddress("AntiKt10PV0TrackJets_daughter__phi_TRK_EFF_LOOSE_PHYSMODEL", &AntiKt10PV0TrackJets_daughter__phi_TRK_EFF_LOOSE_PHYSMODEL, &b_AntiKt10PV0TrackJets_daughter__phi_TRK_EFF_LOOSE_PHYSMODEL);
   fChain->SetBranchAddress("AntiKt10PV0TrackJets_daughter__trackId_TRK_EFF_LOOSE_PHYSMODEL", &AntiKt10PV0TrackJets_daughter__trackId_TRK_EFF_LOOSE_PHYSMODEL, &b_AntiKt10PV0TrackJets_daughter__trackId_TRK_EFF_LOOSE_PHYSMODEL);
   fChain->SetBranchAddress("AntiKtVR30Rmax4Rmin02TrackJets_pt_TRK_EFF_LOOSE_PHYSMODEL", &AntiKtVR30Rmax4Rmin02TrackJets_pt_TRK_EFF_LOOSE_PHYSMODEL, &b_AntiKtVR30Rmax4Rmin02TrackJets_pt_TRK_EFF_LOOSE_PHYSMODEL);
   fChain->SetBranchAddress("AntiKtVR30Rmax4Rmin02TrackJets_eta_TRK_EFF_LOOSE_PHYSMODEL", &AntiKtVR30Rmax4Rmin02TrackJets_eta_TRK_EFF_LOOSE_PHYSMODEL, &b_AntiKtVR30Rmax4Rmin02TrackJets_eta_TRK_EFF_LOOSE_PHYSMODEL);
   fChain->SetBranchAddress("AntiKtVR30Rmax4Rmin02TrackJets_phi_TRK_EFF_LOOSE_PHYSMODEL", &AntiKtVR30Rmax4Rmin02TrackJets_phi_TRK_EFF_LOOSE_PHYSMODEL, &b_AntiKtVR30Rmax4Rmin02TrackJets_phi_TRK_EFF_LOOSE_PHYSMODEL);
   fChain->SetBranchAddress("AntiKtVR30Rmax4Rmin02TrackJets_m_TRK_EFF_LOOSE_PHYSMODEL", &AntiKtVR30Rmax4Rmin02TrackJets_m_TRK_EFF_LOOSE_PHYSMODEL, &b_AntiKtVR30Rmax4Rmin02TrackJets_m_TRK_EFF_LOOSE_PHYSMODEL);
   fChain->SetBranchAddress("AntiKtVR30Rmax4Rmin02TrackJets_daughter__pt_TRK_EFF_LOOSE_PHYSMODEL", &AntiKtVR30Rmax4Rmin02TrackJets_daughter__pt_TRK_EFF_LOOSE_PHYSMODEL, &b_AntiKtVR30Rmax4Rmin02TrackJets_daughter__pt_TRK_EFF_LOOSE_PHYSMODEL);
   fChain->SetBranchAddress("AntiKtVR30Rmax4Rmin02TrackJets_daughter__eta_TRK_EFF_LOOSE_PHYSMODEL", &AntiKtVR30Rmax4Rmin02TrackJets_daughter__eta_TRK_EFF_LOOSE_PHYSMODEL, &b_AntiKtVR30Rmax4Rmin02TrackJets_daughter__eta_TRK_EFF_LOOSE_PHYSMODEL);
   fChain->SetBranchAddress("AntiKtVR30Rmax4Rmin02TrackJets_daughter__phi_TRK_EFF_LOOSE_PHYSMODEL", &AntiKtVR30Rmax4Rmin02TrackJets_daughter__phi_TRK_EFF_LOOSE_PHYSMODEL, &b_AntiKtVR30Rmax4Rmin02TrackJets_daughter__phi_TRK_EFF_LOOSE_PHYSMODEL);
   fChain->SetBranchAddress("AntiKtVR30Rmax4Rmin02TrackJets_daughter__trackId_TRK_EFF_LOOSE_PHYSMODEL", &AntiKtVR30Rmax4Rmin02TrackJets_daughter__trackId_TRK_EFF_LOOSE_PHYSMODEL, &b_AntiKtVR30Rmax4Rmin02TrackJets_daughter__trackId_TRK_EFF_LOOSE_PHYSMODEL);
   fChain->SetBranchAddress("DMesons_costhetastar_TRK_EFF_LOOSE_PP0", &DMesons_costhetastar_TRK_EFF_LOOSE_PP0, &b_DMesons_costhetastar_TRK_EFF_LOOSE_PP0);
   fChain->SetBranchAddress("DMesons_D0Index_TRK_EFF_LOOSE_PP0", &DMesons_D0Index_TRK_EFF_LOOSE_PP0, &b_DMesons_D0Index_TRK_EFF_LOOSE_PP0);
   fChain->SetBranchAddress("DMesons_daughterInfo__eta_TRK_EFF_LOOSE_PP0", &DMesons_daughterInfo__eta_TRK_EFF_LOOSE_PP0, &b_DMesons_daughterInfo__eta_TRK_EFF_LOOSE_PP0);
   fChain->SetBranchAddress("DMesons_daughterInfo__passTight_TRK_EFF_LOOSE_PP0", &DMesons_daughterInfo__passTight_TRK_EFF_LOOSE_PP0, &b_DMesons_daughterInfo__passTight_TRK_EFF_LOOSE_PP0);
   fChain->SetBranchAddress("DMesons_daughterInfo__pdgId_TRK_EFF_LOOSE_PP0", &DMesons_daughterInfo__pdgId_TRK_EFF_LOOSE_PP0, &b_DMesons_daughterInfo__pdgId_TRK_EFF_LOOSE_PP0);
   fChain->SetBranchAddress("DMesons_daughterInfo__phi_TRK_EFF_LOOSE_PP0", &DMesons_daughterInfo__phi_TRK_EFF_LOOSE_PP0, &b_DMesons_daughterInfo__phi_TRK_EFF_LOOSE_PP0);
   fChain->SetBranchAddress("DMesons_daughterInfo__pt_TRK_EFF_LOOSE_PP0", &DMesons_daughterInfo__pt_TRK_EFF_LOOSE_PP0, &b_DMesons_daughterInfo__pt_TRK_EFF_LOOSE_PP0);
   fChain->SetBranchAddress("DMesons_daughterInfo__trackId_TRK_EFF_LOOSE_PP0", &DMesons_daughterInfo__trackId_TRK_EFF_LOOSE_PP0, &b_DMesons_daughterInfo__trackId_TRK_EFF_LOOSE_PP0);
   fChain->SetBranchAddress("DMesons_daughterInfo__truthBarcode_TRK_EFF_LOOSE_PP0", &DMesons_daughterInfo__truthBarcode_TRK_EFF_LOOSE_PP0, &b_DMesons_daughterInfo__truthBarcode_TRK_EFF_LOOSE_PP0);
   fChain->SetBranchAddress("DMesons_daughterInfo__truthDBarcode_TRK_EFF_LOOSE_PP0", &DMesons_daughterInfo__truthDBarcode_TRK_EFF_LOOSE_PP0, &b_DMesons_daughterInfo__truthDBarcode_TRK_EFF_LOOSE_PP0);
   fChain->SetBranchAddress("DMesons_daughterInfo__z0SinTheta_TRK_EFF_LOOSE_PP0", &DMesons_daughterInfo__z0SinTheta_TRK_EFF_LOOSE_PP0, &b_DMesons_daughterInfo__z0SinTheta_TRK_EFF_LOOSE_PP0);
   fChain->SetBranchAddress("DMesons_daughterInfo__z0SinThetaPV_TRK_EFF_LOOSE_PP0", &DMesons_daughterInfo__z0SinThetaPV_TRK_EFF_LOOSE_PP0, &b_DMesons_daughterInfo__z0SinThetaPV_TRK_EFF_LOOSE_PP0);
   fChain->SetBranchAddress("DMesons_decayType_TRK_EFF_LOOSE_PP0", &DMesons_decayType_TRK_EFF_LOOSE_PP0, &b_DMesons_decayType_TRK_EFF_LOOSE_PP0);
   fChain->SetBranchAddress("DMesons_DeltaMass_TRK_EFF_LOOSE_PP0", &DMesons_DeltaMass_TRK_EFF_LOOSE_PP0, &b_DMesons_DeltaMass_TRK_EFF_LOOSE_PP0);
   fChain->SetBranchAddress("DMesons_eta_TRK_EFF_LOOSE_PP0", &DMesons_eta_TRK_EFF_LOOSE_PP0, &b_DMesons_eta_TRK_EFF_LOOSE_PP0);
   fChain->SetBranchAddress("DMesons_fitOutput__Charge_TRK_EFF_LOOSE_PP0", &DMesons_fitOutput__Charge_TRK_EFF_LOOSE_PP0, &b_DMesons_fitOutput__Charge_TRK_EFF_LOOSE_PP0);
   fChain->SetBranchAddress("DMesons_fitOutput__Chi2_TRK_EFF_LOOSE_PP0", &DMesons_fitOutput__Chi2_TRK_EFF_LOOSE_PP0, &b_DMesons_fitOutput__Chi2_TRK_EFF_LOOSE_PP0);
   fChain->SetBranchAddress("DMesons_fitOutput__Impact_TRK_EFF_LOOSE_PP0", &DMesons_fitOutput__Impact_TRK_EFF_LOOSE_PP0, &b_DMesons_fitOutput__Impact_TRK_EFF_LOOSE_PP0);
   fChain->SetBranchAddress("DMesons_fitOutput__ImpactSignificance_TRK_EFF_LOOSE_PP0", &DMesons_fitOutput__ImpactSignificance_TRK_EFF_LOOSE_PP0, &b_DMesons_fitOutput__ImpactSignificance_TRK_EFF_LOOSE_PP0);
   fChain->SetBranchAddress("DMesons_fitOutput__ImpactZ0SinTheta_TRK_EFF_LOOSE_PP0", &DMesons_fitOutput__ImpactZ0SinTheta_TRK_EFF_LOOSE_PP0, &b_DMesons_fitOutput__ImpactZ0SinTheta_TRK_EFF_LOOSE_PP0);
   fChain->SetBranchAddress("DMesons_fitOutput__Lxy_TRK_EFF_LOOSE_PP0", &DMesons_fitOutput__Lxy_TRK_EFF_LOOSE_PP0, &b_DMesons_fitOutput__Lxy_TRK_EFF_LOOSE_PP0);
   fChain->SetBranchAddress("DMesons_fitOutput__VertexPosition_TRK_EFF_LOOSE_PP0", &DMesons_fitOutput__VertexPosition_TRK_EFF_LOOSE_PP0, &b_DMesons_fitOutput__VertexPosition_TRK_EFF_LOOSE_PP0);
   fChain->SetBranchAddress("DMesons_m_TRK_EFF_LOOSE_PP0", &DMesons_m_TRK_EFF_LOOSE_PP0, &b_DMesons_m_TRK_EFF_LOOSE_PP0);
   fChain->SetBranchAddress("DMesons_mKpi1_TRK_EFF_LOOSE_PP0", &DMesons_mKpi1_TRK_EFF_LOOSE_PP0, &b_DMesons_mKpi1_TRK_EFF_LOOSE_PP0);
   fChain->SetBranchAddress("DMesons_mKpi2_TRK_EFF_LOOSE_PP0", &DMesons_mKpi2_TRK_EFF_LOOSE_PP0, &b_DMesons_mKpi2_TRK_EFF_LOOSE_PP0);
   fChain->SetBranchAddress("DMesons_mPhi1_TRK_EFF_LOOSE_PP0", &DMesons_mPhi1_TRK_EFF_LOOSE_PP0, &b_DMesons_mPhi1_TRK_EFF_LOOSE_PP0);
   fChain->SetBranchAddress("DMesons_mPhi2_TRK_EFF_LOOSE_PP0", &DMesons_mPhi2_TRK_EFF_LOOSE_PP0, &b_DMesons_mPhi2_TRK_EFF_LOOSE_PP0);
   fChain->SetBranchAddress("DMesons_pdgId_TRK_EFF_LOOSE_PP0", &DMesons_pdgId_TRK_EFF_LOOSE_PP0, &b_DMesons_pdgId_TRK_EFF_LOOSE_PP0);
   fChain->SetBranchAddress("DMesons_phi_TRK_EFF_LOOSE_PP0", &DMesons_phi_TRK_EFF_LOOSE_PP0, &b_DMesons_phi_TRK_EFF_LOOSE_PP0);
   fChain->SetBranchAddress("DMesons_pt_TRK_EFF_LOOSE_PP0", &DMesons_pt_TRK_EFF_LOOSE_PP0, &b_DMesons_pt_TRK_EFF_LOOSE_PP0);
   fChain->SetBranchAddress("DMesons_ptcone40_TRK_EFF_LOOSE_PP0", &DMesons_ptcone40_TRK_EFF_LOOSE_PP0, &b_DMesons_ptcone40_TRK_EFF_LOOSE_PP0);
   fChain->SetBranchAddress("DMesons_SlowPionD0_TRK_EFF_LOOSE_PP0", &DMesons_SlowPionD0_TRK_EFF_LOOSE_PP0, &b_DMesons_SlowPionD0_TRK_EFF_LOOSE_PP0);
   fChain->SetBranchAddress("DMesons_SlowPionZ0SinTheta_TRK_EFF_LOOSE_PP0", &DMesons_SlowPionZ0SinTheta_TRK_EFF_LOOSE_PP0, &b_DMesons_SlowPionZ0SinTheta_TRK_EFF_LOOSE_PP0);
   fChain->SetBranchAddress("DMesons_truthBarcode_TRK_EFF_LOOSE_PP0", &DMesons_truthBarcode_TRK_EFF_LOOSE_PP0, &b_DMesons_truthBarcode_TRK_EFF_LOOSE_PP0);
   fChain->SetBranchAddress("MesonTracks_d0sig_TRK_EFF_LOOSE_PP0", &MesonTracks_d0sig_TRK_EFF_LOOSE_PP0, &b_MesonTracks_d0sig_TRK_EFF_LOOSE_PP0);
   fChain->SetBranchAddress("MesonTracks_d0sigPV_TRK_EFF_LOOSE_PP0", &MesonTracks_d0sigPV_TRK_EFF_LOOSE_PP0, &b_MesonTracks_d0sigPV_TRK_EFF_LOOSE_PP0);
   fChain->SetBranchAddress("MesonTracks_eta_TRK_EFF_LOOSE_PP0", &MesonTracks_eta_TRK_EFF_LOOSE_PP0, &b_MesonTracks_eta_TRK_EFF_LOOSE_PP0);
   fChain->SetBranchAddress("MesonTracks_passTight_TRK_EFF_LOOSE_PP0", &MesonTracks_passTight_TRK_EFF_LOOSE_PP0, &b_MesonTracks_passTight_TRK_EFF_LOOSE_PP0);
   fChain->SetBranchAddress("MesonTracks_phi_TRK_EFF_LOOSE_PP0", &MesonTracks_phi_TRK_EFF_LOOSE_PP0, &b_MesonTracks_phi_TRK_EFF_LOOSE_PP0);
   fChain->SetBranchAddress("MesonTracks_pt_TRK_EFF_LOOSE_PP0", &MesonTracks_pt_TRK_EFF_LOOSE_PP0, &b_MesonTracks_pt_TRK_EFF_LOOSE_PP0);
   fChain->SetBranchAddress("MesonTracks_trackId_TRK_EFF_LOOSE_PP0", &MesonTracks_trackId_TRK_EFF_LOOSE_PP0, &b_MesonTracks_trackId_TRK_EFF_LOOSE_PP0);
   fChain->SetBranchAddress("MesonTracks_z0sinTheta_TRK_EFF_LOOSE_PP0", &MesonTracks_z0sinTheta_TRK_EFF_LOOSE_PP0, &b_MesonTracks_z0sinTheta_TRK_EFF_LOOSE_PP0);
   fChain->SetBranchAddress("MesonTracks_truthMatchProbability_TRK_EFF_LOOSE_PP0", &MesonTracks_truthMatchProbability_TRK_EFF_LOOSE_PP0, &b_MesonTracks_truthMatchProbability_TRK_EFF_LOOSE_PP0);
   fChain->SetBranchAddress("AntiKt6PV0TrackJets_pt_TRK_EFF_LOOSE_PP0", &AntiKt6PV0TrackJets_pt_TRK_EFF_LOOSE_PP0, &b_AntiKt6PV0TrackJets_pt_TRK_EFF_LOOSE_PP0);
   fChain->SetBranchAddress("AntiKt6PV0TrackJets_eta_TRK_EFF_LOOSE_PP0", &AntiKt6PV0TrackJets_eta_TRK_EFF_LOOSE_PP0, &b_AntiKt6PV0TrackJets_eta_TRK_EFF_LOOSE_PP0);
   fChain->SetBranchAddress("AntiKt6PV0TrackJets_phi_TRK_EFF_LOOSE_PP0", &AntiKt6PV0TrackJets_phi_TRK_EFF_LOOSE_PP0, &b_AntiKt6PV0TrackJets_phi_TRK_EFF_LOOSE_PP0);
   fChain->SetBranchAddress("AntiKt6PV0TrackJets_m_TRK_EFF_LOOSE_PP0", &AntiKt6PV0TrackJets_m_TRK_EFF_LOOSE_PP0, &b_AntiKt6PV0TrackJets_m_TRK_EFF_LOOSE_PP0);
   fChain->SetBranchAddress("AntiKt6PV0TrackJets_daughter__pt_TRK_EFF_LOOSE_PP0", &AntiKt6PV0TrackJets_daughter__pt_TRK_EFF_LOOSE_PP0, &b_AntiKt6PV0TrackJets_daughter__pt_TRK_EFF_LOOSE_PP0);
   fChain->SetBranchAddress("AntiKt6PV0TrackJets_daughter__eta_TRK_EFF_LOOSE_PP0", &AntiKt6PV0TrackJets_daughter__eta_TRK_EFF_LOOSE_PP0, &b_AntiKt6PV0TrackJets_daughter__eta_TRK_EFF_LOOSE_PP0);
   fChain->SetBranchAddress("AntiKt6PV0TrackJets_daughter__phi_TRK_EFF_LOOSE_PP0", &AntiKt6PV0TrackJets_daughter__phi_TRK_EFF_LOOSE_PP0, &b_AntiKt6PV0TrackJets_daughter__phi_TRK_EFF_LOOSE_PP0);
   fChain->SetBranchAddress("AntiKt6PV0TrackJets_daughter__trackId_TRK_EFF_LOOSE_PP0", &AntiKt6PV0TrackJets_daughter__trackId_TRK_EFF_LOOSE_PP0, &b_AntiKt6PV0TrackJets_daughter__trackId_TRK_EFF_LOOSE_PP0);
   fChain->SetBranchAddress("AntiKt8PV0TrackJets_pt_TRK_EFF_LOOSE_PP0", &AntiKt8PV0TrackJets_pt_TRK_EFF_LOOSE_PP0, &b_AntiKt8PV0TrackJets_pt_TRK_EFF_LOOSE_PP0);
   fChain->SetBranchAddress("AntiKt8PV0TrackJets_eta_TRK_EFF_LOOSE_PP0", &AntiKt8PV0TrackJets_eta_TRK_EFF_LOOSE_PP0, &b_AntiKt8PV0TrackJets_eta_TRK_EFF_LOOSE_PP0);
   fChain->SetBranchAddress("AntiKt8PV0TrackJets_phi_TRK_EFF_LOOSE_PP0", &AntiKt8PV0TrackJets_phi_TRK_EFF_LOOSE_PP0, &b_AntiKt8PV0TrackJets_phi_TRK_EFF_LOOSE_PP0);
   fChain->SetBranchAddress("AntiKt8PV0TrackJets_m_TRK_EFF_LOOSE_PP0", &AntiKt8PV0TrackJets_m_TRK_EFF_LOOSE_PP0, &b_AntiKt8PV0TrackJets_m_TRK_EFF_LOOSE_PP0);
   fChain->SetBranchAddress("AntiKt8PV0TrackJets_daughter__pt_TRK_EFF_LOOSE_PP0", &AntiKt8PV0TrackJets_daughter__pt_TRK_EFF_LOOSE_PP0, &b_AntiKt8PV0TrackJets_daughter__pt_TRK_EFF_LOOSE_PP0);
   fChain->SetBranchAddress("AntiKt8PV0TrackJets_daughter__eta_TRK_EFF_LOOSE_PP0", &AntiKt8PV0TrackJets_daughter__eta_TRK_EFF_LOOSE_PP0, &b_AntiKt8PV0TrackJets_daughter__eta_TRK_EFF_LOOSE_PP0);
   fChain->SetBranchAddress("AntiKt8PV0TrackJets_daughter__phi_TRK_EFF_LOOSE_PP0", &AntiKt8PV0TrackJets_daughter__phi_TRK_EFF_LOOSE_PP0, &b_AntiKt8PV0TrackJets_daughter__phi_TRK_EFF_LOOSE_PP0);
   fChain->SetBranchAddress("AntiKt8PV0TrackJets_daughter__trackId_TRK_EFF_LOOSE_PP0", &AntiKt8PV0TrackJets_daughter__trackId_TRK_EFF_LOOSE_PP0, &b_AntiKt8PV0TrackJets_daughter__trackId_TRK_EFF_LOOSE_PP0);
   fChain->SetBranchAddress("AntiKt10PV0TrackJets_pt_TRK_EFF_LOOSE_PP0", &AntiKt10PV0TrackJets_pt_TRK_EFF_LOOSE_PP0, &b_AntiKt10PV0TrackJets_pt_TRK_EFF_LOOSE_PP0);
   fChain->SetBranchAddress("AntiKt10PV0TrackJets_eta_TRK_EFF_LOOSE_PP0", &AntiKt10PV0TrackJets_eta_TRK_EFF_LOOSE_PP0, &b_AntiKt10PV0TrackJets_eta_TRK_EFF_LOOSE_PP0);
   fChain->SetBranchAddress("AntiKt10PV0TrackJets_phi_TRK_EFF_LOOSE_PP0", &AntiKt10PV0TrackJets_phi_TRK_EFF_LOOSE_PP0, &b_AntiKt10PV0TrackJets_phi_TRK_EFF_LOOSE_PP0);
   fChain->SetBranchAddress("AntiKt10PV0TrackJets_m_TRK_EFF_LOOSE_PP0", &AntiKt10PV0TrackJets_m_TRK_EFF_LOOSE_PP0, &b_AntiKt10PV0TrackJets_m_TRK_EFF_LOOSE_PP0);
   fChain->SetBranchAddress("AntiKt10PV0TrackJets_daughter__pt_TRK_EFF_LOOSE_PP0", &AntiKt10PV0TrackJets_daughter__pt_TRK_EFF_LOOSE_PP0, &b_AntiKt10PV0TrackJets_daughter__pt_TRK_EFF_LOOSE_PP0);
   fChain->SetBranchAddress("AntiKt10PV0TrackJets_daughter__eta_TRK_EFF_LOOSE_PP0", &AntiKt10PV0TrackJets_daughter__eta_TRK_EFF_LOOSE_PP0, &b_AntiKt10PV0TrackJets_daughter__eta_TRK_EFF_LOOSE_PP0);
   fChain->SetBranchAddress("AntiKt10PV0TrackJets_daughter__phi_TRK_EFF_LOOSE_PP0", &AntiKt10PV0TrackJets_daughter__phi_TRK_EFF_LOOSE_PP0, &b_AntiKt10PV0TrackJets_daughter__phi_TRK_EFF_LOOSE_PP0);
   fChain->SetBranchAddress("AntiKt10PV0TrackJets_daughter__trackId_TRK_EFF_LOOSE_PP0", &AntiKt10PV0TrackJets_daughter__trackId_TRK_EFF_LOOSE_PP0, &b_AntiKt10PV0TrackJets_daughter__trackId_TRK_EFF_LOOSE_PP0);
   fChain->SetBranchAddress("AntiKtVR30Rmax4Rmin02TrackJets_pt_TRK_EFF_LOOSE_PP0", &AntiKtVR30Rmax4Rmin02TrackJets_pt_TRK_EFF_LOOSE_PP0, &b_AntiKtVR30Rmax4Rmin02TrackJets_pt_TRK_EFF_LOOSE_PP0);
   fChain->SetBranchAddress("AntiKtVR30Rmax4Rmin02TrackJets_eta_TRK_EFF_LOOSE_PP0", &AntiKtVR30Rmax4Rmin02TrackJets_eta_TRK_EFF_LOOSE_PP0, &b_AntiKtVR30Rmax4Rmin02TrackJets_eta_TRK_EFF_LOOSE_PP0);
   fChain->SetBranchAddress("AntiKtVR30Rmax4Rmin02TrackJets_phi_TRK_EFF_LOOSE_PP0", &AntiKtVR30Rmax4Rmin02TrackJets_phi_TRK_EFF_LOOSE_PP0, &b_AntiKtVR30Rmax4Rmin02TrackJets_phi_TRK_EFF_LOOSE_PP0);
   fChain->SetBranchAddress("AntiKtVR30Rmax4Rmin02TrackJets_m_TRK_EFF_LOOSE_PP0", &AntiKtVR30Rmax4Rmin02TrackJets_m_TRK_EFF_LOOSE_PP0, &b_AntiKtVR30Rmax4Rmin02TrackJets_m_TRK_EFF_LOOSE_PP0);
   fChain->SetBranchAddress("AntiKtVR30Rmax4Rmin02TrackJets_daughter__pt_TRK_EFF_LOOSE_PP0", &AntiKtVR30Rmax4Rmin02TrackJets_daughter__pt_TRK_EFF_LOOSE_PP0, &b_AntiKtVR30Rmax4Rmin02TrackJets_daughter__pt_TRK_EFF_LOOSE_PP0);
   fChain->SetBranchAddress("AntiKtVR30Rmax4Rmin02TrackJets_daughter__eta_TRK_EFF_LOOSE_PP0", &AntiKtVR30Rmax4Rmin02TrackJets_daughter__eta_TRK_EFF_LOOSE_PP0, &b_AntiKtVR30Rmax4Rmin02TrackJets_daughter__eta_TRK_EFF_LOOSE_PP0);
   fChain->SetBranchAddress("AntiKtVR30Rmax4Rmin02TrackJets_daughter__phi_TRK_EFF_LOOSE_PP0", &AntiKtVR30Rmax4Rmin02TrackJets_daughter__phi_TRK_EFF_LOOSE_PP0, &b_AntiKtVR30Rmax4Rmin02TrackJets_daughter__phi_TRK_EFF_LOOSE_PP0);
   fChain->SetBranchAddress("AntiKtVR30Rmax4Rmin02TrackJets_daughter__trackId_TRK_EFF_LOOSE_PP0", &AntiKtVR30Rmax4Rmin02TrackJets_daughter__trackId_TRK_EFF_LOOSE_PP0, &b_AntiKtVR30Rmax4Rmin02TrackJets_daughter__trackId_TRK_EFF_LOOSE_PP0);
   fChain->SetBranchAddress("DMesons_costhetastar_TRK_FAKE_RATE_LOOSE", &DMesons_costhetastar_TRK_FAKE_RATE_LOOSE, &b_DMesons_costhetastar_TRK_FAKE_RATE_LOOSE);
   fChain->SetBranchAddress("DMesons_D0Index_TRK_FAKE_RATE_LOOSE", &DMesons_D0Index_TRK_FAKE_RATE_LOOSE, &b_DMesons_D0Index_TRK_FAKE_RATE_LOOSE);
   fChain->SetBranchAddress("DMesons_daughterInfo__eta_TRK_FAKE_RATE_LOOSE", &DMesons_daughterInfo__eta_TRK_FAKE_RATE_LOOSE, &b_DMesons_daughterInfo__eta_TRK_FAKE_RATE_LOOSE);
   fChain->SetBranchAddress("DMesons_daughterInfo__passTight_TRK_FAKE_RATE_LOOSE", &DMesons_daughterInfo__passTight_TRK_FAKE_RATE_LOOSE, &b_DMesons_daughterInfo__passTight_TRK_FAKE_RATE_LOOSE);
   fChain->SetBranchAddress("DMesons_daughterInfo__pdgId_TRK_FAKE_RATE_LOOSE", &DMesons_daughterInfo__pdgId_TRK_FAKE_RATE_LOOSE, &b_DMesons_daughterInfo__pdgId_TRK_FAKE_RATE_LOOSE);
   fChain->SetBranchAddress("DMesons_daughterInfo__phi_TRK_FAKE_RATE_LOOSE", &DMesons_daughterInfo__phi_TRK_FAKE_RATE_LOOSE, &b_DMesons_daughterInfo__phi_TRK_FAKE_RATE_LOOSE);
   fChain->SetBranchAddress("DMesons_daughterInfo__pt_TRK_FAKE_RATE_LOOSE", &DMesons_daughterInfo__pt_TRK_FAKE_RATE_LOOSE, &b_DMesons_daughterInfo__pt_TRK_FAKE_RATE_LOOSE);
   fChain->SetBranchAddress("DMesons_daughterInfo__trackId_TRK_FAKE_RATE_LOOSE", &DMesons_daughterInfo__trackId_TRK_FAKE_RATE_LOOSE, &b_DMesons_daughterInfo__trackId_TRK_FAKE_RATE_LOOSE);
   fChain->SetBranchAddress("DMesons_daughterInfo__truthBarcode_TRK_FAKE_RATE_LOOSE", &DMesons_daughterInfo__truthBarcode_TRK_FAKE_RATE_LOOSE, &b_DMesons_daughterInfo__truthBarcode_TRK_FAKE_RATE_LOOSE);
   fChain->SetBranchAddress("DMesons_daughterInfo__truthDBarcode_TRK_FAKE_RATE_LOOSE", &DMesons_daughterInfo__truthDBarcode_TRK_FAKE_RATE_LOOSE, &b_DMesons_daughterInfo__truthDBarcode_TRK_FAKE_RATE_LOOSE);
   fChain->SetBranchAddress("DMesons_daughterInfo__z0SinTheta_TRK_FAKE_RATE_LOOSE", &DMesons_daughterInfo__z0SinTheta_TRK_FAKE_RATE_LOOSE, &b_DMesons_daughterInfo__z0SinTheta_TRK_FAKE_RATE_LOOSE);
   fChain->SetBranchAddress("DMesons_daughterInfo__z0SinThetaPV_TRK_FAKE_RATE_LOOSE", &DMesons_daughterInfo__z0SinThetaPV_TRK_FAKE_RATE_LOOSE, &b_DMesons_daughterInfo__z0SinThetaPV_TRK_FAKE_RATE_LOOSE);
   fChain->SetBranchAddress("DMesons_decayType_TRK_FAKE_RATE_LOOSE", &DMesons_decayType_TRK_FAKE_RATE_LOOSE, &b_DMesons_decayType_TRK_FAKE_RATE_LOOSE);
   fChain->SetBranchAddress("DMesons_DeltaMass_TRK_FAKE_RATE_LOOSE", &DMesons_DeltaMass_TRK_FAKE_RATE_LOOSE, &b_DMesons_DeltaMass_TRK_FAKE_RATE_LOOSE);
   fChain->SetBranchAddress("DMesons_eta_TRK_FAKE_RATE_LOOSE", &DMesons_eta_TRK_FAKE_RATE_LOOSE, &b_DMesons_eta_TRK_FAKE_RATE_LOOSE);
   fChain->SetBranchAddress("DMesons_fitOutput__Charge_TRK_FAKE_RATE_LOOSE", &DMesons_fitOutput__Charge_TRK_FAKE_RATE_LOOSE, &b_DMesons_fitOutput__Charge_TRK_FAKE_RATE_LOOSE);
   fChain->SetBranchAddress("DMesons_fitOutput__Chi2_TRK_FAKE_RATE_LOOSE", &DMesons_fitOutput__Chi2_TRK_FAKE_RATE_LOOSE, &b_DMesons_fitOutput__Chi2_TRK_FAKE_RATE_LOOSE);
   fChain->SetBranchAddress("DMesons_fitOutput__Impact_TRK_FAKE_RATE_LOOSE", &DMesons_fitOutput__Impact_TRK_FAKE_RATE_LOOSE, &b_DMesons_fitOutput__Impact_TRK_FAKE_RATE_LOOSE);
   fChain->SetBranchAddress("DMesons_fitOutput__ImpactSignificance_TRK_FAKE_RATE_LOOSE", &DMesons_fitOutput__ImpactSignificance_TRK_FAKE_RATE_LOOSE, &b_DMesons_fitOutput__ImpactSignificance_TRK_FAKE_RATE_LOOSE);
   fChain->SetBranchAddress("DMesons_fitOutput__ImpactZ0SinTheta_TRK_FAKE_RATE_LOOSE", &DMesons_fitOutput__ImpactZ0SinTheta_TRK_FAKE_RATE_LOOSE, &b_DMesons_fitOutput__ImpactZ0SinTheta_TRK_FAKE_RATE_LOOSE);
   fChain->SetBranchAddress("DMesons_fitOutput__Lxy_TRK_FAKE_RATE_LOOSE", &DMesons_fitOutput__Lxy_TRK_FAKE_RATE_LOOSE, &b_DMesons_fitOutput__Lxy_TRK_FAKE_RATE_LOOSE);
   fChain->SetBranchAddress("DMesons_fitOutput__VertexPosition_TRK_FAKE_RATE_LOOSE", &DMesons_fitOutput__VertexPosition_TRK_FAKE_RATE_LOOSE, &b_DMesons_fitOutput__VertexPosition_TRK_FAKE_RATE_LOOSE);
   fChain->SetBranchAddress("DMesons_m_TRK_FAKE_RATE_LOOSE", &DMesons_m_TRK_FAKE_RATE_LOOSE, &b_DMesons_m_TRK_FAKE_RATE_LOOSE);
   fChain->SetBranchAddress("DMesons_mKpi1_TRK_FAKE_RATE_LOOSE", &DMesons_mKpi1_TRK_FAKE_RATE_LOOSE, &b_DMesons_mKpi1_TRK_FAKE_RATE_LOOSE);
   fChain->SetBranchAddress("DMesons_mKpi2_TRK_FAKE_RATE_LOOSE", &DMesons_mKpi2_TRK_FAKE_RATE_LOOSE, &b_DMesons_mKpi2_TRK_FAKE_RATE_LOOSE);
   fChain->SetBranchAddress("DMesons_mPhi1_TRK_FAKE_RATE_LOOSE", &DMesons_mPhi1_TRK_FAKE_RATE_LOOSE, &b_DMesons_mPhi1_TRK_FAKE_RATE_LOOSE);
   fChain->SetBranchAddress("DMesons_mPhi2_TRK_FAKE_RATE_LOOSE", &DMesons_mPhi2_TRK_FAKE_RATE_LOOSE, &b_DMesons_mPhi2_TRK_FAKE_RATE_LOOSE);
   fChain->SetBranchAddress("DMesons_pdgId_TRK_FAKE_RATE_LOOSE", &DMesons_pdgId_TRK_FAKE_RATE_LOOSE, &b_DMesons_pdgId_TRK_FAKE_RATE_LOOSE);
   fChain->SetBranchAddress("DMesons_phi_TRK_FAKE_RATE_LOOSE", &DMesons_phi_TRK_FAKE_RATE_LOOSE, &b_DMesons_phi_TRK_FAKE_RATE_LOOSE);
   fChain->SetBranchAddress("DMesons_pt_TRK_FAKE_RATE_LOOSE", &DMesons_pt_TRK_FAKE_RATE_LOOSE, &b_DMesons_pt_TRK_FAKE_RATE_LOOSE);
   fChain->SetBranchAddress("DMesons_ptcone40_TRK_FAKE_RATE_LOOSE", &DMesons_ptcone40_TRK_FAKE_RATE_LOOSE, &b_DMesons_ptcone40_TRK_FAKE_RATE_LOOSE);
   fChain->SetBranchAddress("DMesons_SlowPionD0_TRK_FAKE_RATE_LOOSE", &DMesons_SlowPionD0_TRK_FAKE_RATE_LOOSE, &b_DMesons_SlowPionD0_TRK_FAKE_RATE_LOOSE);
   fChain->SetBranchAddress("DMesons_SlowPionZ0SinTheta_TRK_FAKE_RATE_LOOSE", &DMesons_SlowPionZ0SinTheta_TRK_FAKE_RATE_LOOSE, &b_DMesons_SlowPionZ0SinTheta_TRK_FAKE_RATE_LOOSE);
   fChain->SetBranchAddress("DMesons_truthBarcode_TRK_FAKE_RATE_LOOSE", &DMesons_truthBarcode_TRK_FAKE_RATE_LOOSE, &b_DMesons_truthBarcode_TRK_FAKE_RATE_LOOSE);
   fChain->SetBranchAddress("MesonTracks_d0sig_TRK_FAKE_RATE_LOOSE", &MesonTracks_d0sig_TRK_FAKE_RATE_LOOSE, &b_MesonTracks_d0sig_TRK_FAKE_RATE_LOOSE);
   fChain->SetBranchAddress("MesonTracks_d0sigPV_TRK_FAKE_RATE_LOOSE", &MesonTracks_d0sigPV_TRK_FAKE_RATE_LOOSE, &b_MesonTracks_d0sigPV_TRK_FAKE_RATE_LOOSE);
   fChain->SetBranchAddress("MesonTracks_eta_TRK_FAKE_RATE_LOOSE", &MesonTracks_eta_TRK_FAKE_RATE_LOOSE, &b_MesonTracks_eta_TRK_FAKE_RATE_LOOSE);
   fChain->SetBranchAddress("MesonTracks_passTight_TRK_FAKE_RATE_LOOSE", &MesonTracks_passTight_TRK_FAKE_RATE_LOOSE, &b_MesonTracks_passTight_TRK_FAKE_RATE_LOOSE);
   fChain->SetBranchAddress("MesonTracks_phi_TRK_FAKE_RATE_LOOSE", &MesonTracks_phi_TRK_FAKE_RATE_LOOSE, &b_MesonTracks_phi_TRK_FAKE_RATE_LOOSE);
   fChain->SetBranchAddress("MesonTracks_pt_TRK_FAKE_RATE_LOOSE", &MesonTracks_pt_TRK_FAKE_RATE_LOOSE, &b_MesonTracks_pt_TRK_FAKE_RATE_LOOSE);
   fChain->SetBranchAddress("MesonTracks_trackId_TRK_FAKE_RATE_LOOSE", &MesonTracks_trackId_TRK_FAKE_RATE_LOOSE, &b_MesonTracks_trackId_TRK_FAKE_RATE_LOOSE);
   fChain->SetBranchAddress("MesonTracks_z0sinTheta_TRK_FAKE_RATE_LOOSE", &MesonTracks_z0sinTheta_TRK_FAKE_RATE_LOOSE, &b_MesonTracks_z0sinTheta_TRK_FAKE_RATE_LOOSE);
   fChain->SetBranchAddress("MesonTracks_truthMatchProbability_TRK_FAKE_RATE_LOOSE", &MesonTracks_truthMatchProbability_TRK_FAKE_RATE_LOOSE, &b_MesonTracks_truthMatchProbability_TRK_FAKE_RATE_LOOSE);
   fChain->SetBranchAddress("AntiKt6PV0TrackJets_pt_TRK_FAKE_RATE_LOOSE", &AntiKt6PV0TrackJets_pt_TRK_FAKE_RATE_LOOSE, &b_AntiKt6PV0TrackJets_pt_TRK_FAKE_RATE_LOOSE);
   fChain->SetBranchAddress("AntiKt6PV0TrackJets_eta_TRK_FAKE_RATE_LOOSE", &AntiKt6PV0TrackJets_eta_TRK_FAKE_RATE_LOOSE, &b_AntiKt6PV0TrackJets_eta_TRK_FAKE_RATE_LOOSE);
   fChain->SetBranchAddress("AntiKt6PV0TrackJets_phi_TRK_FAKE_RATE_LOOSE", &AntiKt6PV0TrackJets_phi_TRK_FAKE_RATE_LOOSE, &b_AntiKt6PV0TrackJets_phi_TRK_FAKE_RATE_LOOSE);
   fChain->SetBranchAddress("AntiKt6PV0TrackJets_m_TRK_FAKE_RATE_LOOSE", &AntiKt6PV0TrackJets_m_TRK_FAKE_RATE_LOOSE, &b_AntiKt6PV0TrackJets_m_TRK_FAKE_RATE_LOOSE);
   fChain->SetBranchAddress("AntiKt6PV0TrackJets_daughter__pt_TRK_FAKE_RATE_LOOSE", &AntiKt6PV0TrackJets_daughter__pt_TRK_FAKE_RATE_LOOSE, &b_AntiKt6PV0TrackJets_daughter__pt_TRK_FAKE_RATE_LOOSE);
   fChain->SetBranchAddress("AntiKt6PV0TrackJets_daughter__eta_TRK_FAKE_RATE_LOOSE", &AntiKt6PV0TrackJets_daughter__eta_TRK_FAKE_RATE_LOOSE, &b_AntiKt6PV0TrackJets_daughter__eta_TRK_FAKE_RATE_LOOSE);
   fChain->SetBranchAddress("AntiKt6PV0TrackJets_daughter__phi_TRK_FAKE_RATE_LOOSE", &AntiKt6PV0TrackJets_daughter__phi_TRK_FAKE_RATE_LOOSE, &b_AntiKt6PV0TrackJets_daughter__phi_TRK_FAKE_RATE_LOOSE);
   fChain->SetBranchAddress("AntiKt6PV0TrackJets_daughter__trackId_TRK_FAKE_RATE_LOOSE", &AntiKt6PV0TrackJets_daughter__trackId_TRK_FAKE_RATE_LOOSE, &b_AntiKt6PV0TrackJets_daughter__trackId_TRK_FAKE_RATE_LOOSE);
   fChain->SetBranchAddress("AntiKt8PV0TrackJets_pt_TRK_FAKE_RATE_LOOSE", &AntiKt8PV0TrackJets_pt_TRK_FAKE_RATE_LOOSE, &b_AntiKt8PV0TrackJets_pt_TRK_FAKE_RATE_LOOSE);
   fChain->SetBranchAddress("AntiKt8PV0TrackJets_eta_TRK_FAKE_RATE_LOOSE", &AntiKt8PV0TrackJets_eta_TRK_FAKE_RATE_LOOSE, &b_AntiKt8PV0TrackJets_eta_TRK_FAKE_RATE_LOOSE);
   fChain->SetBranchAddress("AntiKt8PV0TrackJets_phi_TRK_FAKE_RATE_LOOSE", &AntiKt8PV0TrackJets_phi_TRK_FAKE_RATE_LOOSE, &b_AntiKt8PV0TrackJets_phi_TRK_FAKE_RATE_LOOSE);
   fChain->SetBranchAddress("AntiKt8PV0TrackJets_m_TRK_FAKE_RATE_LOOSE", &AntiKt8PV0TrackJets_m_TRK_FAKE_RATE_LOOSE, &b_AntiKt8PV0TrackJets_m_TRK_FAKE_RATE_LOOSE);
   fChain->SetBranchAddress("AntiKt8PV0TrackJets_daughter__pt_TRK_FAKE_RATE_LOOSE", &AntiKt8PV0TrackJets_daughter__pt_TRK_FAKE_RATE_LOOSE, &b_AntiKt8PV0TrackJets_daughter__pt_TRK_FAKE_RATE_LOOSE);
   fChain->SetBranchAddress("AntiKt8PV0TrackJets_daughter__eta_TRK_FAKE_RATE_LOOSE", &AntiKt8PV0TrackJets_daughter__eta_TRK_FAKE_RATE_LOOSE, &b_AntiKt8PV0TrackJets_daughter__eta_TRK_FAKE_RATE_LOOSE);
   fChain->SetBranchAddress("AntiKt8PV0TrackJets_daughter__phi_TRK_FAKE_RATE_LOOSE", &AntiKt8PV0TrackJets_daughter__phi_TRK_FAKE_RATE_LOOSE, &b_AntiKt8PV0TrackJets_daughter__phi_TRK_FAKE_RATE_LOOSE);
   fChain->SetBranchAddress("AntiKt8PV0TrackJets_daughter__trackId_TRK_FAKE_RATE_LOOSE", &AntiKt8PV0TrackJets_daughter__trackId_TRK_FAKE_RATE_LOOSE, &b_AntiKt8PV0TrackJets_daughter__trackId_TRK_FAKE_RATE_LOOSE);
   fChain->SetBranchAddress("AntiKt10PV0TrackJets_pt_TRK_FAKE_RATE_LOOSE", &AntiKt10PV0TrackJets_pt_TRK_FAKE_RATE_LOOSE, &b_AntiKt10PV0TrackJets_pt_TRK_FAKE_RATE_LOOSE);
   fChain->SetBranchAddress("AntiKt10PV0TrackJets_eta_TRK_FAKE_RATE_LOOSE", &AntiKt10PV0TrackJets_eta_TRK_FAKE_RATE_LOOSE, &b_AntiKt10PV0TrackJets_eta_TRK_FAKE_RATE_LOOSE);
   fChain->SetBranchAddress("AntiKt10PV0TrackJets_phi_TRK_FAKE_RATE_LOOSE", &AntiKt10PV0TrackJets_phi_TRK_FAKE_RATE_LOOSE, &b_AntiKt10PV0TrackJets_phi_TRK_FAKE_RATE_LOOSE);
   fChain->SetBranchAddress("AntiKt10PV0TrackJets_m_TRK_FAKE_RATE_LOOSE", &AntiKt10PV0TrackJets_m_TRK_FAKE_RATE_LOOSE, &b_AntiKt10PV0TrackJets_m_TRK_FAKE_RATE_LOOSE);
   fChain->SetBranchAddress("AntiKt10PV0TrackJets_daughter__pt_TRK_FAKE_RATE_LOOSE", &AntiKt10PV0TrackJets_daughter__pt_TRK_FAKE_RATE_LOOSE, &b_AntiKt10PV0TrackJets_daughter__pt_TRK_FAKE_RATE_LOOSE);
   fChain->SetBranchAddress("AntiKt10PV0TrackJets_daughter__eta_TRK_FAKE_RATE_LOOSE", &AntiKt10PV0TrackJets_daughter__eta_TRK_FAKE_RATE_LOOSE, &b_AntiKt10PV0TrackJets_daughter__eta_TRK_FAKE_RATE_LOOSE);
   fChain->SetBranchAddress("AntiKt10PV0TrackJets_daughter__phi_TRK_FAKE_RATE_LOOSE", &AntiKt10PV0TrackJets_daughter__phi_TRK_FAKE_RATE_LOOSE, &b_AntiKt10PV0TrackJets_daughter__phi_TRK_FAKE_RATE_LOOSE);
   fChain->SetBranchAddress("AntiKt10PV0TrackJets_daughter__trackId_TRK_FAKE_RATE_LOOSE", &AntiKt10PV0TrackJets_daughter__trackId_TRK_FAKE_RATE_LOOSE, &b_AntiKt10PV0TrackJets_daughter__trackId_TRK_FAKE_RATE_LOOSE);
   fChain->SetBranchAddress("AntiKtVR30Rmax4Rmin02TrackJets_pt_TRK_FAKE_RATE_LOOSE", &AntiKtVR30Rmax4Rmin02TrackJets_pt_TRK_FAKE_RATE_LOOSE, &b_AntiKtVR30Rmax4Rmin02TrackJets_pt_TRK_FAKE_RATE_LOOSE);
   fChain->SetBranchAddress("AntiKtVR30Rmax4Rmin02TrackJets_eta_TRK_FAKE_RATE_LOOSE", &AntiKtVR30Rmax4Rmin02TrackJets_eta_TRK_FAKE_RATE_LOOSE, &b_AntiKtVR30Rmax4Rmin02TrackJets_eta_TRK_FAKE_RATE_LOOSE);
   fChain->SetBranchAddress("AntiKtVR30Rmax4Rmin02TrackJets_phi_TRK_FAKE_RATE_LOOSE", &AntiKtVR30Rmax4Rmin02TrackJets_phi_TRK_FAKE_RATE_LOOSE, &b_AntiKtVR30Rmax4Rmin02TrackJets_phi_TRK_FAKE_RATE_LOOSE);
   fChain->SetBranchAddress("AntiKtVR30Rmax4Rmin02TrackJets_m_TRK_FAKE_RATE_LOOSE", &AntiKtVR30Rmax4Rmin02TrackJets_m_TRK_FAKE_RATE_LOOSE, &b_AntiKtVR30Rmax4Rmin02TrackJets_m_TRK_FAKE_RATE_LOOSE);
   fChain->SetBranchAddress("AntiKtVR30Rmax4Rmin02TrackJets_daughter__pt_TRK_FAKE_RATE_LOOSE", &AntiKtVR30Rmax4Rmin02TrackJets_daughter__pt_TRK_FAKE_RATE_LOOSE, &b_AntiKtVR30Rmax4Rmin02TrackJets_daughter__pt_TRK_FAKE_RATE_LOOSE);
   fChain->SetBranchAddress("AntiKtVR30Rmax4Rmin02TrackJets_daughter__eta_TRK_FAKE_RATE_LOOSE", &AntiKtVR30Rmax4Rmin02TrackJets_daughter__eta_TRK_FAKE_RATE_LOOSE, &b_AntiKtVR30Rmax4Rmin02TrackJets_daughter__eta_TRK_FAKE_RATE_LOOSE);
   fChain->SetBranchAddress("AntiKtVR30Rmax4Rmin02TrackJets_daughter__phi_TRK_FAKE_RATE_LOOSE", &AntiKtVR30Rmax4Rmin02TrackJets_daughter__phi_TRK_FAKE_RATE_LOOSE, &b_AntiKtVR30Rmax4Rmin02TrackJets_daughter__phi_TRK_FAKE_RATE_LOOSE);
   fChain->SetBranchAddress("AntiKtVR30Rmax4Rmin02TrackJets_daughter__trackId_TRK_FAKE_RATE_LOOSE", &AntiKtVR30Rmax4Rmin02TrackJets_daughter__trackId_TRK_FAKE_RATE_LOOSE, &b_AntiKtVR30Rmax4Rmin02TrackJets_daughter__trackId_TRK_FAKE_RATE_LOOSE);
   fChain->SetBranchAddress("DMesons_costhetastar_TRK_RES_D0_DEAD", &DMesons_costhetastar_TRK_RES_D0_DEAD, &b_DMesons_costhetastar_TRK_RES_D0_DEAD);
   fChain->SetBranchAddress("DMesons_D0Index_TRK_RES_D0_DEAD", &DMesons_D0Index_TRK_RES_D0_DEAD, &b_DMesons_D0Index_TRK_RES_D0_DEAD);
   fChain->SetBranchAddress("DMesons_daughterInfo__eta_TRK_RES_D0_DEAD", &DMesons_daughterInfo__eta_TRK_RES_D0_DEAD, &b_DMesons_daughterInfo__eta_TRK_RES_D0_DEAD);
   fChain->SetBranchAddress("DMesons_daughterInfo__passTight_TRK_RES_D0_DEAD", &DMesons_daughterInfo__passTight_TRK_RES_D0_DEAD, &b_DMesons_daughterInfo__passTight_TRK_RES_D0_DEAD);
   fChain->SetBranchAddress("DMesons_daughterInfo__pdgId_TRK_RES_D0_DEAD", &DMesons_daughterInfo__pdgId_TRK_RES_D0_DEAD, &b_DMesons_daughterInfo__pdgId_TRK_RES_D0_DEAD);
   fChain->SetBranchAddress("DMesons_daughterInfo__phi_TRK_RES_D0_DEAD", &DMesons_daughterInfo__phi_TRK_RES_D0_DEAD, &b_DMesons_daughterInfo__phi_TRK_RES_D0_DEAD);
   fChain->SetBranchAddress("DMesons_daughterInfo__pt_TRK_RES_D0_DEAD", &DMesons_daughterInfo__pt_TRK_RES_D0_DEAD, &b_DMesons_daughterInfo__pt_TRK_RES_D0_DEAD);
   fChain->SetBranchAddress("DMesons_daughterInfo__trackId_TRK_RES_D0_DEAD", &DMesons_daughterInfo__trackId_TRK_RES_D0_DEAD, &b_DMesons_daughterInfo__trackId_TRK_RES_D0_DEAD);
   fChain->SetBranchAddress("DMesons_daughterInfo__truthBarcode_TRK_RES_D0_DEAD", &DMesons_daughterInfo__truthBarcode_TRK_RES_D0_DEAD, &b_DMesons_daughterInfo__truthBarcode_TRK_RES_D0_DEAD);
   fChain->SetBranchAddress("DMesons_daughterInfo__truthDBarcode_TRK_RES_D0_DEAD", &DMesons_daughterInfo__truthDBarcode_TRK_RES_D0_DEAD, &b_DMesons_daughterInfo__truthDBarcode_TRK_RES_D0_DEAD);
   fChain->SetBranchAddress("DMesons_daughterInfo__z0SinTheta_TRK_RES_D0_DEAD", &DMesons_daughterInfo__z0SinTheta_TRK_RES_D0_DEAD, &b_DMesons_daughterInfo__z0SinTheta_TRK_RES_D0_DEAD);
   fChain->SetBranchAddress("DMesons_daughterInfo__z0SinThetaPV_TRK_RES_D0_DEAD", &DMesons_daughterInfo__z0SinThetaPV_TRK_RES_D0_DEAD, &b_DMesons_daughterInfo__z0SinThetaPV_TRK_RES_D0_DEAD);
   fChain->SetBranchAddress("DMesons_decayType_TRK_RES_D0_DEAD", &DMesons_decayType_TRK_RES_D0_DEAD, &b_DMesons_decayType_TRK_RES_D0_DEAD);
   fChain->SetBranchAddress("DMesons_DeltaMass_TRK_RES_D0_DEAD", &DMesons_DeltaMass_TRK_RES_D0_DEAD, &b_DMesons_DeltaMass_TRK_RES_D0_DEAD);
   fChain->SetBranchAddress("DMesons_eta_TRK_RES_D0_DEAD", &DMesons_eta_TRK_RES_D0_DEAD, &b_DMesons_eta_TRK_RES_D0_DEAD);
   fChain->SetBranchAddress("DMesons_fitOutput__Charge_TRK_RES_D0_DEAD", &DMesons_fitOutput__Charge_TRK_RES_D0_DEAD, &b_DMesons_fitOutput__Charge_TRK_RES_D0_DEAD);
   fChain->SetBranchAddress("DMesons_fitOutput__Chi2_TRK_RES_D0_DEAD", &DMesons_fitOutput__Chi2_TRK_RES_D0_DEAD, &b_DMesons_fitOutput__Chi2_TRK_RES_D0_DEAD);
   fChain->SetBranchAddress("DMesons_fitOutput__Impact_TRK_RES_D0_DEAD", &DMesons_fitOutput__Impact_TRK_RES_D0_DEAD, &b_DMesons_fitOutput__Impact_TRK_RES_D0_DEAD);
   fChain->SetBranchAddress("DMesons_fitOutput__ImpactSignificance_TRK_RES_D0_DEAD", &DMesons_fitOutput__ImpactSignificance_TRK_RES_D0_DEAD, &b_DMesons_fitOutput__ImpactSignificance_TRK_RES_D0_DEAD);
   fChain->SetBranchAddress("DMesons_fitOutput__ImpactZ0SinTheta_TRK_RES_D0_DEAD", &DMesons_fitOutput__ImpactZ0SinTheta_TRK_RES_D0_DEAD, &b_DMesons_fitOutput__ImpactZ0SinTheta_TRK_RES_D0_DEAD);
   fChain->SetBranchAddress("DMesons_fitOutput__Lxy_TRK_RES_D0_DEAD", &DMesons_fitOutput__Lxy_TRK_RES_D0_DEAD, &b_DMesons_fitOutput__Lxy_TRK_RES_D0_DEAD);
   fChain->SetBranchAddress("DMesons_fitOutput__VertexPosition_TRK_RES_D0_DEAD", &DMesons_fitOutput__VertexPosition_TRK_RES_D0_DEAD, &b_DMesons_fitOutput__VertexPosition_TRK_RES_D0_DEAD);
   fChain->SetBranchAddress("DMesons_m_TRK_RES_D0_DEAD", &DMesons_m_TRK_RES_D0_DEAD, &b_DMesons_m_TRK_RES_D0_DEAD);
   fChain->SetBranchAddress("DMesons_mKpi1_TRK_RES_D0_DEAD", &DMesons_mKpi1_TRK_RES_D0_DEAD, &b_DMesons_mKpi1_TRK_RES_D0_DEAD);
   fChain->SetBranchAddress("DMesons_mKpi2_TRK_RES_D0_DEAD", &DMesons_mKpi2_TRK_RES_D0_DEAD, &b_DMesons_mKpi2_TRK_RES_D0_DEAD);
   fChain->SetBranchAddress("DMesons_mPhi1_TRK_RES_D0_DEAD", &DMesons_mPhi1_TRK_RES_D0_DEAD, &b_DMesons_mPhi1_TRK_RES_D0_DEAD);
   fChain->SetBranchAddress("DMesons_mPhi2_TRK_RES_D0_DEAD", &DMesons_mPhi2_TRK_RES_D0_DEAD, &b_DMesons_mPhi2_TRK_RES_D0_DEAD);
   fChain->SetBranchAddress("DMesons_pdgId_TRK_RES_D0_DEAD", &DMesons_pdgId_TRK_RES_D0_DEAD, &b_DMesons_pdgId_TRK_RES_D0_DEAD);
   fChain->SetBranchAddress("DMesons_phi_TRK_RES_D0_DEAD", &DMesons_phi_TRK_RES_D0_DEAD, &b_DMesons_phi_TRK_RES_D0_DEAD);
   fChain->SetBranchAddress("DMesons_pt_TRK_RES_D0_DEAD", &DMesons_pt_TRK_RES_D0_DEAD, &b_DMesons_pt_TRK_RES_D0_DEAD);
   fChain->SetBranchAddress("DMesons_ptcone40_TRK_RES_D0_DEAD", &DMesons_ptcone40_TRK_RES_D0_DEAD, &b_DMesons_ptcone40_TRK_RES_D0_DEAD);
   fChain->SetBranchAddress("DMesons_SlowPionD0_TRK_RES_D0_DEAD", &DMesons_SlowPionD0_TRK_RES_D0_DEAD, &b_DMesons_SlowPionD0_TRK_RES_D0_DEAD);
   fChain->SetBranchAddress("DMesons_SlowPionZ0SinTheta_TRK_RES_D0_DEAD", &DMesons_SlowPionZ0SinTheta_TRK_RES_D0_DEAD, &b_DMesons_SlowPionZ0SinTheta_TRK_RES_D0_DEAD);
   fChain->SetBranchAddress("DMesons_truthBarcode_TRK_RES_D0_DEAD", &DMesons_truthBarcode_TRK_RES_D0_DEAD, &b_DMesons_truthBarcode_TRK_RES_D0_DEAD);
   fChain->SetBranchAddress("MesonTracks_d0sig_TRK_RES_D0_DEAD", &MesonTracks_d0sig_TRK_RES_D0_DEAD, &b_MesonTracks_d0sig_TRK_RES_D0_DEAD);
   fChain->SetBranchAddress("MesonTracks_d0sigPV_TRK_RES_D0_DEAD", &MesonTracks_d0sigPV_TRK_RES_D0_DEAD, &b_MesonTracks_d0sigPV_TRK_RES_D0_DEAD);
   fChain->SetBranchAddress("MesonTracks_eta_TRK_RES_D0_DEAD", &MesonTracks_eta_TRK_RES_D0_DEAD, &b_MesonTracks_eta_TRK_RES_D0_DEAD);
   fChain->SetBranchAddress("MesonTracks_passTight_TRK_RES_D0_DEAD", &MesonTracks_passTight_TRK_RES_D0_DEAD, &b_MesonTracks_passTight_TRK_RES_D0_DEAD);
   fChain->SetBranchAddress("MesonTracks_phi_TRK_RES_D0_DEAD", &MesonTracks_phi_TRK_RES_D0_DEAD, &b_MesonTracks_phi_TRK_RES_D0_DEAD);
   fChain->SetBranchAddress("MesonTracks_pt_TRK_RES_D0_DEAD", &MesonTracks_pt_TRK_RES_D0_DEAD, &b_MesonTracks_pt_TRK_RES_D0_DEAD);
   fChain->SetBranchAddress("MesonTracks_trackId_TRK_RES_D0_DEAD", &MesonTracks_trackId_TRK_RES_D0_DEAD, &b_MesonTracks_trackId_TRK_RES_D0_DEAD);
   fChain->SetBranchAddress("MesonTracks_z0sinTheta_TRK_RES_D0_DEAD", &MesonTracks_z0sinTheta_TRK_RES_D0_DEAD, &b_MesonTracks_z0sinTheta_TRK_RES_D0_DEAD);
   fChain->SetBranchAddress("MesonTracks_truthMatchProbability_TRK_RES_D0_DEAD", &MesonTracks_truthMatchProbability_TRK_RES_D0_DEAD, &b_MesonTracks_truthMatchProbability_TRK_RES_D0_DEAD);
   fChain->SetBranchAddress("AntiKt6PV0TrackJets_pt_TRK_RES_D0_DEAD", &AntiKt6PV0TrackJets_pt_TRK_RES_D0_DEAD, &b_AntiKt6PV0TrackJets_pt_TRK_RES_D0_DEAD);
   fChain->SetBranchAddress("AntiKt6PV0TrackJets_eta_TRK_RES_D0_DEAD", &AntiKt6PV0TrackJets_eta_TRK_RES_D0_DEAD, &b_AntiKt6PV0TrackJets_eta_TRK_RES_D0_DEAD);
   fChain->SetBranchAddress("AntiKt6PV0TrackJets_phi_TRK_RES_D0_DEAD", &AntiKt6PV0TrackJets_phi_TRK_RES_D0_DEAD, &b_AntiKt6PV0TrackJets_phi_TRK_RES_D0_DEAD);
   fChain->SetBranchAddress("AntiKt6PV0TrackJets_m_TRK_RES_D0_DEAD", &AntiKt6PV0TrackJets_m_TRK_RES_D0_DEAD, &b_AntiKt6PV0TrackJets_m_TRK_RES_D0_DEAD);
   fChain->SetBranchAddress("AntiKt6PV0TrackJets_daughter__pt_TRK_RES_D0_DEAD", &AntiKt6PV0TrackJets_daughter__pt_TRK_RES_D0_DEAD, &b_AntiKt6PV0TrackJets_daughter__pt_TRK_RES_D0_DEAD);
   fChain->SetBranchAddress("AntiKt6PV0TrackJets_daughter__eta_TRK_RES_D0_DEAD", &AntiKt6PV0TrackJets_daughter__eta_TRK_RES_D0_DEAD, &b_AntiKt6PV0TrackJets_daughter__eta_TRK_RES_D0_DEAD);
   fChain->SetBranchAddress("AntiKt6PV0TrackJets_daughter__phi_TRK_RES_D0_DEAD", &AntiKt6PV0TrackJets_daughter__phi_TRK_RES_D0_DEAD, &b_AntiKt6PV0TrackJets_daughter__phi_TRK_RES_D0_DEAD);
   fChain->SetBranchAddress("AntiKt6PV0TrackJets_daughter__trackId_TRK_RES_D0_DEAD", &AntiKt6PV0TrackJets_daughter__trackId_TRK_RES_D0_DEAD, &b_AntiKt6PV0TrackJets_daughter__trackId_TRK_RES_D0_DEAD);
   fChain->SetBranchAddress("AntiKt8PV0TrackJets_pt_TRK_RES_D0_DEAD", &AntiKt8PV0TrackJets_pt_TRK_RES_D0_DEAD, &b_AntiKt8PV0TrackJets_pt_TRK_RES_D0_DEAD);
   fChain->SetBranchAddress("AntiKt8PV0TrackJets_eta_TRK_RES_D0_DEAD", &AntiKt8PV0TrackJets_eta_TRK_RES_D0_DEAD, &b_AntiKt8PV0TrackJets_eta_TRK_RES_D0_DEAD);
   fChain->SetBranchAddress("AntiKt8PV0TrackJets_phi_TRK_RES_D0_DEAD", &AntiKt8PV0TrackJets_phi_TRK_RES_D0_DEAD, &b_AntiKt8PV0TrackJets_phi_TRK_RES_D0_DEAD);
   fChain->SetBranchAddress("AntiKt8PV0TrackJets_m_TRK_RES_D0_DEAD", &AntiKt8PV0TrackJets_m_TRK_RES_D0_DEAD, &b_AntiKt8PV0TrackJets_m_TRK_RES_D0_DEAD);
   fChain->SetBranchAddress("AntiKt8PV0TrackJets_daughter__pt_TRK_RES_D0_DEAD", &AntiKt8PV0TrackJets_daughter__pt_TRK_RES_D0_DEAD, &b_AntiKt8PV0TrackJets_daughter__pt_TRK_RES_D0_DEAD);
   fChain->SetBranchAddress("AntiKt8PV0TrackJets_daughter__eta_TRK_RES_D0_DEAD", &AntiKt8PV0TrackJets_daughter__eta_TRK_RES_D0_DEAD, &b_AntiKt8PV0TrackJets_daughter__eta_TRK_RES_D0_DEAD);
   fChain->SetBranchAddress("AntiKt8PV0TrackJets_daughter__phi_TRK_RES_D0_DEAD", &AntiKt8PV0TrackJets_daughter__phi_TRK_RES_D0_DEAD, &b_AntiKt8PV0TrackJets_daughter__phi_TRK_RES_D0_DEAD);
   fChain->SetBranchAddress("AntiKt8PV0TrackJets_daughter__trackId_TRK_RES_D0_DEAD", &AntiKt8PV0TrackJets_daughter__trackId_TRK_RES_D0_DEAD, &b_AntiKt8PV0TrackJets_daughter__trackId_TRK_RES_D0_DEAD);
   fChain->SetBranchAddress("AntiKt10PV0TrackJets_pt_TRK_RES_D0_DEAD", &AntiKt10PV0TrackJets_pt_TRK_RES_D0_DEAD, &b_AntiKt10PV0TrackJets_pt_TRK_RES_D0_DEAD);
   fChain->SetBranchAddress("AntiKt10PV0TrackJets_eta_TRK_RES_D0_DEAD", &AntiKt10PV0TrackJets_eta_TRK_RES_D0_DEAD, &b_AntiKt10PV0TrackJets_eta_TRK_RES_D0_DEAD);
   fChain->SetBranchAddress("AntiKt10PV0TrackJets_phi_TRK_RES_D0_DEAD", &AntiKt10PV0TrackJets_phi_TRK_RES_D0_DEAD, &b_AntiKt10PV0TrackJets_phi_TRK_RES_D0_DEAD);
   fChain->SetBranchAddress("AntiKt10PV0TrackJets_m_TRK_RES_D0_DEAD", &AntiKt10PV0TrackJets_m_TRK_RES_D0_DEAD, &b_AntiKt10PV0TrackJets_m_TRK_RES_D0_DEAD);
   fChain->SetBranchAddress("AntiKt10PV0TrackJets_daughter__pt_TRK_RES_D0_DEAD", &AntiKt10PV0TrackJets_daughter__pt_TRK_RES_D0_DEAD, &b_AntiKt10PV0TrackJets_daughter__pt_TRK_RES_D0_DEAD);
   fChain->SetBranchAddress("AntiKt10PV0TrackJets_daughter__eta_TRK_RES_D0_DEAD", &AntiKt10PV0TrackJets_daughter__eta_TRK_RES_D0_DEAD, &b_AntiKt10PV0TrackJets_daughter__eta_TRK_RES_D0_DEAD);
   fChain->SetBranchAddress("AntiKt10PV0TrackJets_daughter__phi_TRK_RES_D0_DEAD", &AntiKt10PV0TrackJets_daughter__phi_TRK_RES_D0_DEAD, &b_AntiKt10PV0TrackJets_daughter__phi_TRK_RES_D0_DEAD);
   fChain->SetBranchAddress("AntiKt10PV0TrackJets_daughter__trackId_TRK_RES_D0_DEAD", &AntiKt10PV0TrackJets_daughter__trackId_TRK_RES_D0_DEAD, &b_AntiKt10PV0TrackJets_daughter__trackId_TRK_RES_D0_DEAD);
   fChain->SetBranchAddress("AntiKtVR30Rmax4Rmin02TrackJets_pt_TRK_RES_D0_DEAD", &AntiKtVR30Rmax4Rmin02TrackJets_pt_TRK_RES_D0_DEAD, &b_AntiKtVR30Rmax4Rmin02TrackJets_pt_TRK_RES_D0_DEAD);
   fChain->SetBranchAddress("AntiKtVR30Rmax4Rmin02TrackJets_eta_TRK_RES_D0_DEAD", &AntiKtVR30Rmax4Rmin02TrackJets_eta_TRK_RES_D0_DEAD, &b_AntiKtVR30Rmax4Rmin02TrackJets_eta_TRK_RES_D0_DEAD);
   fChain->SetBranchAddress("AntiKtVR30Rmax4Rmin02TrackJets_phi_TRK_RES_D0_DEAD", &AntiKtVR30Rmax4Rmin02TrackJets_phi_TRK_RES_D0_DEAD, &b_AntiKtVR30Rmax4Rmin02TrackJets_phi_TRK_RES_D0_DEAD);
   fChain->SetBranchAddress("AntiKtVR30Rmax4Rmin02TrackJets_m_TRK_RES_D0_DEAD", &AntiKtVR30Rmax4Rmin02TrackJets_m_TRK_RES_D0_DEAD, &b_AntiKtVR30Rmax4Rmin02TrackJets_m_TRK_RES_D0_DEAD);
   fChain->SetBranchAddress("AntiKtVR30Rmax4Rmin02TrackJets_daughter__pt_TRK_RES_D0_DEAD", &AntiKtVR30Rmax4Rmin02TrackJets_daughter__pt_TRK_RES_D0_DEAD, &b_AntiKtVR30Rmax4Rmin02TrackJets_daughter__pt_TRK_RES_D0_DEAD);
   fChain->SetBranchAddress("AntiKtVR30Rmax4Rmin02TrackJets_daughter__eta_TRK_RES_D0_DEAD", &AntiKtVR30Rmax4Rmin02TrackJets_daughter__eta_TRK_RES_D0_DEAD, &b_AntiKtVR30Rmax4Rmin02TrackJets_daughter__eta_TRK_RES_D0_DEAD);
   fChain->SetBranchAddress("AntiKtVR30Rmax4Rmin02TrackJets_daughter__phi_TRK_RES_D0_DEAD", &AntiKtVR30Rmax4Rmin02TrackJets_daughter__phi_TRK_RES_D0_DEAD, &b_AntiKtVR30Rmax4Rmin02TrackJets_daughter__phi_TRK_RES_D0_DEAD);
   fChain->SetBranchAddress("AntiKtVR30Rmax4Rmin02TrackJets_daughter__trackId_TRK_RES_D0_DEAD", &AntiKtVR30Rmax4Rmin02TrackJets_daughter__trackId_TRK_RES_D0_DEAD, &b_AntiKtVR30Rmax4Rmin02TrackJets_daughter__trackId_TRK_RES_D0_DEAD);
   fChain->SetBranchAddress("DMesons_costhetastar_TRK_RES_D0_MEAS", &DMesons_costhetastar_TRK_RES_D0_MEAS, &b_DMesons_costhetastar_TRK_RES_D0_MEAS);
   fChain->SetBranchAddress("DMesons_D0Index_TRK_RES_D0_MEAS", &DMesons_D0Index_TRK_RES_D0_MEAS, &b_DMesons_D0Index_TRK_RES_D0_MEAS);
   fChain->SetBranchAddress("DMesons_daughterInfo__eta_TRK_RES_D0_MEAS", &DMesons_daughterInfo__eta_TRK_RES_D0_MEAS, &b_DMesons_daughterInfo__eta_TRK_RES_D0_MEAS);
   fChain->SetBranchAddress("DMesons_daughterInfo__passTight_TRK_RES_D0_MEAS", &DMesons_daughterInfo__passTight_TRK_RES_D0_MEAS, &b_DMesons_daughterInfo__passTight_TRK_RES_D0_MEAS);
   fChain->SetBranchAddress("DMesons_daughterInfo__pdgId_TRK_RES_D0_MEAS", &DMesons_daughterInfo__pdgId_TRK_RES_D0_MEAS, &b_DMesons_daughterInfo__pdgId_TRK_RES_D0_MEAS);
   fChain->SetBranchAddress("DMesons_daughterInfo__phi_TRK_RES_D0_MEAS", &DMesons_daughterInfo__phi_TRK_RES_D0_MEAS, &b_DMesons_daughterInfo__phi_TRK_RES_D0_MEAS);
   fChain->SetBranchAddress("DMesons_daughterInfo__pt_TRK_RES_D0_MEAS", &DMesons_daughterInfo__pt_TRK_RES_D0_MEAS, &b_DMesons_daughterInfo__pt_TRK_RES_D0_MEAS);
   fChain->SetBranchAddress("DMesons_daughterInfo__trackId_TRK_RES_D0_MEAS", &DMesons_daughterInfo__trackId_TRK_RES_D0_MEAS, &b_DMesons_daughterInfo__trackId_TRK_RES_D0_MEAS);
   fChain->SetBranchAddress("DMesons_daughterInfo__truthBarcode_TRK_RES_D0_MEAS", &DMesons_daughterInfo__truthBarcode_TRK_RES_D0_MEAS, &b_DMesons_daughterInfo__truthBarcode_TRK_RES_D0_MEAS);
   fChain->SetBranchAddress("DMesons_daughterInfo__truthDBarcode_TRK_RES_D0_MEAS", &DMesons_daughterInfo__truthDBarcode_TRK_RES_D0_MEAS, &b_DMesons_daughterInfo__truthDBarcode_TRK_RES_D0_MEAS);
   fChain->SetBranchAddress("DMesons_daughterInfo__z0SinTheta_TRK_RES_D0_MEAS", &DMesons_daughterInfo__z0SinTheta_TRK_RES_D0_MEAS, &b_DMesons_daughterInfo__z0SinTheta_TRK_RES_D0_MEAS);
   fChain->SetBranchAddress("DMesons_daughterInfo__z0SinThetaPV_TRK_RES_D0_MEAS", &DMesons_daughterInfo__z0SinThetaPV_TRK_RES_D0_MEAS, &b_DMesons_daughterInfo__z0SinThetaPV_TRK_RES_D0_MEAS);
   fChain->SetBranchAddress("DMesons_decayType_TRK_RES_D0_MEAS", &DMesons_decayType_TRK_RES_D0_MEAS, &b_DMesons_decayType_TRK_RES_D0_MEAS);
   fChain->SetBranchAddress("DMesons_DeltaMass_TRK_RES_D0_MEAS", &DMesons_DeltaMass_TRK_RES_D0_MEAS, &b_DMesons_DeltaMass_TRK_RES_D0_MEAS);
   fChain->SetBranchAddress("DMesons_eta_TRK_RES_D0_MEAS", &DMesons_eta_TRK_RES_D0_MEAS, &b_DMesons_eta_TRK_RES_D0_MEAS);
   fChain->SetBranchAddress("DMesons_fitOutput__Charge_TRK_RES_D0_MEAS", &DMesons_fitOutput__Charge_TRK_RES_D0_MEAS, &b_DMesons_fitOutput__Charge_TRK_RES_D0_MEAS);
   fChain->SetBranchAddress("DMesons_fitOutput__Chi2_TRK_RES_D0_MEAS", &DMesons_fitOutput__Chi2_TRK_RES_D0_MEAS, &b_DMesons_fitOutput__Chi2_TRK_RES_D0_MEAS);
   fChain->SetBranchAddress("DMesons_fitOutput__Impact_TRK_RES_D0_MEAS", &DMesons_fitOutput__Impact_TRK_RES_D0_MEAS, &b_DMesons_fitOutput__Impact_TRK_RES_D0_MEAS);
   fChain->SetBranchAddress("DMesons_fitOutput__ImpactSignificance_TRK_RES_D0_MEAS", &DMesons_fitOutput__ImpactSignificance_TRK_RES_D0_MEAS, &b_DMesons_fitOutput__ImpactSignificance_TRK_RES_D0_MEAS);
   fChain->SetBranchAddress("DMesons_fitOutput__ImpactZ0SinTheta_TRK_RES_D0_MEAS", &DMesons_fitOutput__ImpactZ0SinTheta_TRK_RES_D0_MEAS, &b_DMesons_fitOutput__ImpactZ0SinTheta_TRK_RES_D0_MEAS);
   fChain->SetBranchAddress("DMesons_fitOutput__Lxy_TRK_RES_D0_MEAS", &DMesons_fitOutput__Lxy_TRK_RES_D0_MEAS, &b_DMesons_fitOutput__Lxy_TRK_RES_D0_MEAS);
   fChain->SetBranchAddress("DMesons_fitOutput__VertexPosition_TRK_RES_D0_MEAS", &DMesons_fitOutput__VertexPosition_TRK_RES_D0_MEAS, &b_DMesons_fitOutput__VertexPosition_TRK_RES_D0_MEAS);
   fChain->SetBranchAddress("DMesons_m_TRK_RES_D0_MEAS", &DMesons_m_TRK_RES_D0_MEAS, &b_DMesons_m_TRK_RES_D0_MEAS);
   fChain->SetBranchAddress("DMesons_mKpi1_TRK_RES_D0_MEAS", &DMesons_mKpi1_TRK_RES_D0_MEAS, &b_DMesons_mKpi1_TRK_RES_D0_MEAS);
   fChain->SetBranchAddress("DMesons_mKpi2_TRK_RES_D0_MEAS", &DMesons_mKpi2_TRK_RES_D0_MEAS, &b_DMesons_mKpi2_TRK_RES_D0_MEAS);
   fChain->SetBranchAddress("DMesons_mPhi1_TRK_RES_D0_MEAS", &DMesons_mPhi1_TRK_RES_D0_MEAS, &b_DMesons_mPhi1_TRK_RES_D0_MEAS);
   fChain->SetBranchAddress("DMesons_mPhi2_TRK_RES_D0_MEAS", &DMesons_mPhi2_TRK_RES_D0_MEAS, &b_DMesons_mPhi2_TRK_RES_D0_MEAS);
   fChain->SetBranchAddress("DMesons_pdgId_TRK_RES_D0_MEAS", &DMesons_pdgId_TRK_RES_D0_MEAS, &b_DMesons_pdgId_TRK_RES_D0_MEAS);
   fChain->SetBranchAddress("DMesons_phi_TRK_RES_D0_MEAS", &DMesons_phi_TRK_RES_D0_MEAS, &b_DMesons_phi_TRK_RES_D0_MEAS);
   fChain->SetBranchAddress("DMesons_pt_TRK_RES_D0_MEAS", &DMesons_pt_TRK_RES_D0_MEAS, &b_DMesons_pt_TRK_RES_D0_MEAS);
   fChain->SetBranchAddress("DMesons_ptcone40_TRK_RES_D0_MEAS", &DMesons_ptcone40_TRK_RES_D0_MEAS, &b_DMesons_ptcone40_TRK_RES_D0_MEAS);
   fChain->SetBranchAddress("DMesons_SlowPionD0_TRK_RES_D0_MEAS", &DMesons_SlowPionD0_TRK_RES_D0_MEAS, &b_DMesons_SlowPionD0_TRK_RES_D0_MEAS);
   fChain->SetBranchAddress("DMesons_SlowPionZ0SinTheta_TRK_RES_D0_MEAS", &DMesons_SlowPionZ0SinTheta_TRK_RES_D0_MEAS, &b_DMesons_SlowPionZ0SinTheta_TRK_RES_D0_MEAS);
   fChain->SetBranchAddress("DMesons_truthBarcode_TRK_RES_D0_MEAS", &DMesons_truthBarcode_TRK_RES_D0_MEAS, &b_DMesons_truthBarcode_TRK_RES_D0_MEAS);
   fChain->SetBranchAddress("MesonTracks_d0sig_TRK_RES_D0_MEAS", &MesonTracks_d0sig_TRK_RES_D0_MEAS, &b_MesonTracks_d0sig_TRK_RES_D0_MEAS);
   fChain->SetBranchAddress("MesonTracks_d0sigPV_TRK_RES_D0_MEAS", &MesonTracks_d0sigPV_TRK_RES_D0_MEAS, &b_MesonTracks_d0sigPV_TRK_RES_D0_MEAS);
   fChain->SetBranchAddress("MesonTracks_eta_TRK_RES_D0_MEAS", &MesonTracks_eta_TRK_RES_D0_MEAS, &b_MesonTracks_eta_TRK_RES_D0_MEAS);
   fChain->SetBranchAddress("MesonTracks_passTight_TRK_RES_D0_MEAS", &MesonTracks_passTight_TRK_RES_D0_MEAS, &b_MesonTracks_passTight_TRK_RES_D0_MEAS);
   fChain->SetBranchAddress("MesonTracks_phi_TRK_RES_D0_MEAS", &MesonTracks_phi_TRK_RES_D0_MEAS, &b_MesonTracks_phi_TRK_RES_D0_MEAS);
   fChain->SetBranchAddress("MesonTracks_pt_TRK_RES_D0_MEAS", &MesonTracks_pt_TRK_RES_D0_MEAS, &b_MesonTracks_pt_TRK_RES_D0_MEAS);
   fChain->SetBranchAddress("MesonTracks_trackId_TRK_RES_D0_MEAS", &MesonTracks_trackId_TRK_RES_D0_MEAS, &b_MesonTracks_trackId_TRK_RES_D0_MEAS);
   fChain->SetBranchAddress("MesonTracks_z0sinTheta_TRK_RES_D0_MEAS", &MesonTracks_z0sinTheta_TRK_RES_D0_MEAS, &b_MesonTracks_z0sinTheta_TRK_RES_D0_MEAS);
   fChain->SetBranchAddress("MesonTracks_truthMatchProbability_TRK_RES_D0_MEAS", &MesonTracks_truthMatchProbability_TRK_RES_D0_MEAS, &b_MesonTracks_truthMatchProbability_TRK_RES_D0_MEAS);
   fChain->SetBranchAddress("AntiKt6PV0TrackJets_pt_TRK_RES_D0_MEAS", &AntiKt6PV0TrackJets_pt_TRK_RES_D0_MEAS, &b_AntiKt6PV0TrackJets_pt_TRK_RES_D0_MEAS);
   fChain->SetBranchAddress("AntiKt6PV0TrackJets_eta_TRK_RES_D0_MEAS", &AntiKt6PV0TrackJets_eta_TRK_RES_D0_MEAS, &b_AntiKt6PV0TrackJets_eta_TRK_RES_D0_MEAS);
   fChain->SetBranchAddress("AntiKt6PV0TrackJets_phi_TRK_RES_D0_MEAS", &AntiKt6PV0TrackJets_phi_TRK_RES_D0_MEAS, &b_AntiKt6PV0TrackJets_phi_TRK_RES_D0_MEAS);
   fChain->SetBranchAddress("AntiKt6PV0TrackJets_m_TRK_RES_D0_MEAS", &AntiKt6PV0TrackJets_m_TRK_RES_D0_MEAS, &b_AntiKt6PV0TrackJets_m_TRK_RES_D0_MEAS);
   fChain->SetBranchAddress("AntiKt6PV0TrackJets_daughter__pt_TRK_RES_D0_MEAS", &AntiKt6PV0TrackJets_daughter__pt_TRK_RES_D0_MEAS, &b_AntiKt6PV0TrackJets_daughter__pt_TRK_RES_D0_MEAS);
   fChain->SetBranchAddress("AntiKt6PV0TrackJets_daughter__eta_TRK_RES_D0_MEAS", &AntiKt6PV0TrackJets_daughter__eta_TRK_RES_D0_MEAS, &b_AntiKt6PV0TrackJets_daughter__eta_TRK_RES_D0_MEAS);
   fChain->SetBranchAddress("AntiKt6PV0TrackJets_daughter__phi_TRK_RES_D0_MEAS", &AntiKt6PV0TrackJets_daughter__phi_TRK_RES_D0_MEAS, &b_AntiKt6PV0TrackJets_daughter__phi_TRK_RES_D0_MEAS);
   fChain->SetBranchAddress("AntiKt6PV0TrackJets_daughter__trackId_TRK_RES_D0_MEAS", &AntiKt6PV0TrackJets_daughter__trackId_TRK_RES_D0_MEAS, &b_AntiKt6PV0TrackJets_daughter__trackId_TRK_RES_D0_MEAS);
   fChain->SetBranchAddress("AntiKt8PV0TrackJets_pt_TRK_RES_D0_MEAS", &AntiKt8PV0TrackJets_pt_TRK_RES_D0_MEAS, &b_AntiKt8PV0TrackJets_pt_TRK_RES_D0_MEAS);
   fChain->SetBranchAddress("AntiKt8PV0TrackJets_eta_TRK_RES_D0_MEAS", &AntiKt8PV0TrackJets_eta_TRK_RES_D0_MEAS, &b_AntiKt8PV0TrackJets_eta_TRK_RES_D0_MEAS);
   fChain->SetBranchAddress("AntiKt8PV0TrackJets_phi_TRK_RES_D0_MEAS", &AntiKt8PV0TrackJets_phi_TRK_RES_D0_MEAS, &b_AntiKt8PV0TrackJets_phi_TRK_RES_D0_MEAS);
   fChain->SetBranchAddress("AntiKt8PV0TrackJets_m_TRK_RES_D0_MEAS", &AntiKt8PV0TrackJets_m_TRK_RES_D0_MEAS, &b_AntiKt8PV0TrackJets_m_TRK_RES_D0_MEAS);
   fChain->SetBranchAddress("AntiKt8PV0TrackJets_daughter__pt_TRK_RES_D0_MEAS", &AntiKt8PV0TrackJets_daughter__pt_TRK_RES_D0_MEAS, &b_AntiKt8PV0TrackJets_daughter__pt_TRK_RES_D0_MEAS);
   fChain->SetBranchAddress("AntiKt8PV0TrackJets_daughter__eta_TRK_RES_D0_MEAS", &AntiKt8PV0TrackJets_daughter__eta_TRK_RES_D0_MEAS, &b_AntiKt8PV0TrackJets_daughter__eta_TRK_RES_D0_MEAS);
   fChain->SetBranchAddress("AntiKt8PV0TrackJets_daughter__phi_TRK_RES_D0_MEAS", &AntiKt8PV0TrackJets_daughter__phi_TRK_RES_D0_MEAS, &b_AntiKt8PV0TrackJets_daughter__phi_TRK_RES_D0_MEAS);
   fChain->SetBranchAddress("AntiKt8PV0TrackJets_daughter__trackId_TRK_RES_D0_MEAS", &AntiKt8PV0TrackJets_daughter__trackId_TRK_RES_D0_MEAS, &b_AntiKt8PV0TrackJets_daughter__trackId_TRK_RES_D0_MEAS);
   fChain->SetBranchAddress("AntiKt10PV0TrackJets_pt_TRK_RES_D0_MEAS", &AntiKt10PV0TrackJets_pt_TRK_RES_D0_MEAS, &b_AntiKt10PV0TrackJets_pt_TRK_RES_D0_MEAS);
   fChain->SetBranchAddress("AntiKt10PV0TrackJets_eta_TRK_RES_D0_MEAS", &AntiKt10PV0TrackJets_eta_TRK_RES_D0_MEAS, &b_AntiKt10PV0TrackJets_eta_TRK_RES_D0_MEAS);
   fChain->SetBranchAddress("AntiKt10PV0TrackJets_phi_TRK_RES_D0_MEAS", &AntiKt10PV0TrackJets_phi_TRK_RES_D0_MEAS, &b_AntiKt10PV0TrackJets_phi_TRK_RES_D0_MEAS);
   fChain->SetBranchAddress("AntiKt10PV0TrackJets_m_TRK_RES_D0_MEAS", &AntiKt10PV0TrackJets_m_TRK_RES_D0_MEAS, &b_AntiKt10PV0TrackJets_m_TRK_RES_D0_MEAS);
   fChain->SetBranchAddress("AntiKt10PV0TrackJets_daughter__pt_TRK_RES_D0_MEAS", &AntiKt10PV0TrackJets_daughter__pt_TRK_RES_D0_MEAS, &b_AntiKt10PV0TrackJets_daughter__pt_TRK_RES_D0_MEAS);
   fChain->SetBranchAddress("AntiKt10PV0TrackJets_daughter__eta_TRK_RES_D0_MEAS", &AntiKt10PV0TrackJets_daughter__eta_TRK_RES_D0_MEAS, &b_AntiKt10PV0TrackJets_daughter__eta_TRK_RES_D0_MEAS);
   fChain->SetBranchAddress("AntiKt10PV0TrackJets_daughter__phi_TRK_RES_D0_MEAS", &AntiKt10PV0TrackJets_daughter__phi_TRK_RES_D0_MEAS, &b_AntiKt10PV0TrackJets_daughter__phi_TRK_RES_D0_MEAS);
   fChain->SetBranchAddress("AntiKt10PV0TrackJets_daughter__trackId_TRK_RES_D0_MEAS", &AntiKt10PV0TrackJets_daughter__trackId_TRK_RES_D0_MEAS, &b_AntiKt10PV0TrackJets_daughter__trackId_TRK_RES_D0_MEAS);
   fChain->SetBranchAddress("AntiKtVR30Rmax4Rmin02TrackJets_pt_TRK_RES_D0_MEAS", &AntiKtVR30Rmax4Rmin02TrackJets_pt_TRK_RES_D0_MEAS, &b_AntiKtVR30Rmax4Rmin02TrackJets_pt_TRK_RES_D0_MEAS);
   fChain->SetBranchAddress("AntiKtVR30Rmax4Rmin02TrackJets_eta_TRK_RES_D0_MEAS", &AntiKtVR30Rmax4Rmin02TrackJets_eta_TRK_RES_D0_MEAS, &b_AntiKtVR30Rmax4Rmin02TrackJets_eta_TRK_RES_D0_MEAS);
   fChain->SetBranchAddress("AntiKtVR30Rmax4Rmin02TrackJets_phi_TRK_RES_D0_MEAS", &AntiKtVR30Rmax4Rmin02TrackJets_phi_TRK_RES_D0_MEAS, &b_AntiKtVR30Rmax4Rmin02TrackJets_phi_TRK_RES_D0_MEAS);
   fChain->SetBranchAddress("AntiKtVR30Rmax4Rmin02TrackJets_m_TRK_RES_D0_MEAS", &AntiKtVR30Rmax4Rmin02TrackJets_m_TRK_RES_D0_MEAS, &b_AntiKtVR30Rmax4Rmin02TrackJets_m_TRK_RES_D0_MEAS);
   fChain->SetBranchAddress("AntiKtVR30Rmax4Rmin02TrackJets_daughter__pt_TRK_RES_D0_MEAS", &AntiKtVR30Rmax4Rmin02TrackJets_daughter__pt_TRK_RES_D0_MEAS, &b_AntiKtVR30Rmax4Rmin02TrackJets_daughter__pt_TRK_RES_D0_MEAS);
   fChain->SetBranchAddress("AntiKtVR30Rmax4Rmin02TrackJets_daughter__eta_TRK_RES_D0_MEAS", &AntiKtVR30Rmax4Rmin02TrackJets_daughter__eta_TRK_RES_D0_MEAS, &b_AntiKtVR30Rmax4Rmin02TrackJets_daughter__eta_TRK_RES_D0_MEAS);
   fChain->SetBranchAddress("AntiKtVR30Rmax4Rmin02TrackJets_daughter__phi_TRK_RES_D0_MEAS", &AntiKtVR30Rmax4Rmin02TrackJets_daughter__phi_TRK_RES_D0_MEAS, &b_AntiKtVR30Rmax4Rmin02TrackJets_daughter__phi_TRK_RES_D0_MEAS);
   fChain->SetBranchAddress("AntiKtVR30Rmax4Rmin02TrackJets_daughter__trackId_TRK_RES_D0_MEAS", &AntiKtVR30Rmax4Rmin02TrackJets_daughter__trackId_TRK_RES_D0_MEAS, &b_AntiKtVR30Rmax4Rmin02TrackJets_daughter__trackId_TRK_RES_D0_MEAS);
   fChain->SetBranchAddress("DMesons_costhetastar_TRK_RES_Z0_DEAD", &DMesons_costhetastar_TRK_RES_Z0_DEAD, &b_DMesons_costhetastar_TRK_RES_Z0_DEAD);
   fChain->SetBranchAddress("DMesons_D0Index_TRK_RES_Z0_DEAD", &DMesons_D0Index_TRK_RES_Z0_DEAD, &b_DMesons_D0Index_TRK_RES_Z0_DEAD);
   fChain->SetBranchAddress("DMesons_daughterInfo__eta_TRK_RES_Z0_DEAD", &DMesons_daughterInfo__eta_TRK_RES_Z0_DEAD, &b_DMesons_daughterInfo__eta_TRK_RES_Z0_DEAD);
   fChain->SetBranchAddress("DMesons_daughterInfo__passTight_TRK_RES_Z0_DEAD", &DMesons_daughterInfo__passTight_TRK_RES_Z0_DEAD, &b_DMesons_daughterInfo__passTight_TRK_RES_Z0_DEAD);
   fChain->SetBranchAddress("DMesons_daughterInfo__pdgId_TRK_RES_Z0_DEAD", &DMesons_daughterInfo__pdgId_TRK_RES_Z0_DEAD, &b_DMesons_daughterInfo__pdgId_TRK_RES_Z0_DEAD);
   fChain->SetBranchAddress("DMesons_daughterInfo__phi_TRK_RES_Z0_DEAD", &DMesons_daughterInfo__phi_TRK_RES_Z0_DEAD, &b_DMesons_daughterInfo__phi_TRK_RES_Z0_DEAD);
   fChain->SetBranchAddress("DMesons_daughterInfo__pt_TRK_RES_Z0_DEAD", &DMesons_daughterInfo__pt_TRK_RES_Z0_DEAD, &b_DMesons_daughterInfo__pt_TRK_RES_Z0_DEAD);
   fChain->SetBranchAddress("DMesons_daughterInfo__trackId_TRK_RES_Z0_DEAD", &DMesons_daughterInfo__trackId_TRK_RES_Z0_DEAD, &b_DMesons_daughterInfo__trackId_TRK_RES_Z0_DEAD);
   fChain->SetBranchAddress("DMesons_daughterInfo__truthBarcode_TRK_RES_Z0_DEAD", &DMesons_daughterInfo__truthBarcode_TRK_RES_Z0_DEAD, &b_DMesons_daughterInfo__truthBarcode_TRK_RES_Z0_DEAD);
   fChain->SetBranchAddress("DMesons_daughterInfo__truthDBarcode_TRK_RES_Z0_DEAD", &DMesons_daughterInfo__truthDBarcode_TRK_RES_Z0_DEAD, &b_DMesons_daughterInfo__truthDBarcode_TRK_RES_Z0_DEAD);
   fChain->SetBranchAddress("DMesons_daughterInfo__z0SinTheta_TRK_RES_Z0_DEAD", &DMesons_daughterInfo__z0SinTheta_TRK_RES_Z0_DEAD, &b_DMesons_daughterInfo__z0SinTheta_TRK_RES_Z0_DEAD);
   fChain->SetBranchAddress("DMesons_daughterInfo__z0SinThetaPV_TRK_RES_Z0_DEAD", &DMesons_daughterInfo__z0SinThetaPV_TRK_RES_Z0_DEAD, &b_DMesons_daughterInfo__z0SinThetaPV_TRK_RES_Z0_DEAD);
   fChain->SetBranchAddress("DMesons_decayType_TRK_RES_Z0_DEAD", &DMesons_decayType_TRK_RES_Z0_DEAD, &b_DMesons_decayType_TRK_RES_Z0_DEAD);
   fChain->SetBranchAddress("DMesons_DeltaMass_TRK_RES_Z0_DEAD", &DMesons_DeltaMass_TRK_RES_Z0_DEAD, &b_DMesons_DeltaMass_TRK_RES_Z0_DEAD);
   fChain->SetBranchAddress("DMesons_eta_TRK_RES_Z0_DEAD", &DMesons_eta_TRK_RES_Z0_DEAD, &b_DMesons_eta_TRK_RES_Z0_DEAD);
   fChain->SetBranchAddress("DMesons_fitOutput__Charge_TRK_RES_Z0_DEAD", &DMesons_fitOutput__Charge_TRK_RES_Z0_DEAD, &b_DMesons_fitOutput__Charge_TRK_RES_Z0_DEAD);
   fChain->SetBranchAddress("DMesons_fitOutput__Chi2_TRK_RES_Z0_DEAD", &DMesons_fitOutput__Chi2_TRK_RES_Z0_DEAD, &b_DMesons_fitOutput__Chi2_TRK_RES_Z0_DEAD);
   fChain->SetBranchAddress("DMesons_fitOutput__Impact_TRK_RES_Z0_DEAD", &DMesons_fitOutput__Impact_TRK_RES_Z0_DEAD, &b_DMesons_fitOutput__Impact_TRK_RES_Z0_DEAD);
   fChain->SetBranchAddress("DMesons_fitOutput__ImpactSignificance_TRK_RES_Z0_DEAD", &DMesons_fitOutput__ImpactSignificance_TRK_RES_Z0_DEAD, &b_DMesons_fitOutput__ImpactSignificance_TRK_RES_Z0_DEAD);
   fChain->SetBranchAddress("DMesons_fitOutput__ImpactZ0SinTheta_TRK_RES_Z0_DEAD", &DMesons_fitOutput__ImpactZ0SinTheta_TRK_RES_Z0_DEAD, &b_DMesons_fitOutput__ImpactZ0SinTheta_TRK_RES_Z0_DEAD);
   fChain->SetBranchAddress("DMesons_fitOutput__Lxy_TRK_RES_Z0_DEAD", &DMesons_fitOutput__Lxy_TRK_RES_Z0_DEAD, &b_DMesons_fitOutput__Lxy_TRK_RES_Z0_DEAD);
   fChain->SetBranchAddress("DMesons_fitOutput__VertexPosition_TRK_RES_Z0_DEAD", &DMesons_fitOutput__VertexPosition_TRK_RES_Z0_DEAD, &b_DMesons_fitOutput__VertexPosition_TRK_RES_Z0_DEAD);
   fChain->SetBranchAddress("DMesons_m_TRK_RES_Z0_DEAD", &DMesons_m_TRK_RES_Z0_DEAD, &b_DMesons_m_TRK_RES_Z0_DEAD);
   fChain->SetBranchAddress("DMesons_mKpi1_TRK_RES_Z0_DEAD", &DMesons_mKpi1_TRK_RES_Z0_DEAD, &b_DMesons_mKpi1_TRK_RES_Z0_DEAD);
   fChain->SetBranchAddress("DMesons_mKpi2_TRK_RES_Z0_DEAD", &DMesons_mKpi2_TRK_RES_Z0_DEAD, &b_DMesons_mKpi2_TRK_RES_Z0_DEAD);
   fChain->SetBranchAddress("DMesons_mPhi1_TRK_RES_Z0_DEAD", &DMesons_mPhi1_TRK_RES_Z0_DEAD, &b_DMesons_mPhi1_TRK_RES_Z0_DEAD);
   fChain->SetBranchAddress("DMesons_mPhi2_TRK_RES_Z0_DEAD", &DMesons_mPhi2_TRK_RES_Z0_DEAD, &b_DMesons_mPhi2_TRK_RES_Z0_DEAD);
   fChain->SetBranchAddress("DMesons_pdgId_TRK_RES_Z0_DEAD", &DMesons_pdgId_TRK_RES_Z0_DEAD, &b_DMesons_pdgId_TRK_RES_Z0_DEAD);
   fChain->SetBranchAddress("DMesons_phi_TRK_RES_Z0_DEAD", &DMesons_phi_TRK_RES_Z0_DEAD, &b_DMesons_phi_TRK_RES_Z0_DEAD);
   fChain->SetBranchAddress("DMesons_pt_TRK_RES_Z0_DEAD", &DMesons_pt_TRK_RES_Z0_DEAD, &b_DMesons_pt_TRK_RES_Z0_DEAD);
   fChain->SetBranchAddress("DMesons_ptcone40_TRK_RES_Z0_DEAD", &DMesons_ptcone40_TRK_RES_Z0_DEAD, &b_DMesons_ptcone40_TRK_RES_Z0_DEAD);
   fChain->SetBranchAddress("DMesons_SlowPionD0_TRK_RES_Z0_DEAD", &DMesons_SlowPionD0_TRK_RES_Z0_DEAD, &b_DMesons_SlowPionD0_TRK_RES_Z0_DEAD);
   fChain->SetBranchAddress("DMesons_SlowPionZ0SinTheta_TRK_RES_Z0_DEAD", &DMesons_SlowPionZ0SinTheta_TRK_RES_Z0_DEAD, &b_DMesons_SlowPionZ0SinTheta_TRK_RES_Z0_DEAD);
   fChain->SetBranchAddress("DMesons_truthBarcode_TRK_RES_Z0_DEAD", &DMesons_truthBarcode_TRK_RES_Z0_DEAD, &b_DMesons_truthBarcode_TRK_RES_Z0_DEAD);
   fChain->SetBranchAddress("MesonTracks_d0sig_TRK_RES_Z0_DEAD", &MesonTracks_d0sig_TRK_RES_Z0_DEAD, &b_MesonTracks_d0sig_TRK_RES_Z0_DEAD);
   fChain->SetBranchAddress("MesonTracks_d0sigPV_TRK_RES_Z0_DEAD", &MesonTracks_d0sigPV_TRK_RES_Z0_DEAD, &b_MesonTracks_d0sigPV_TRK_RES_Z0_DEAD);
   fChain->SetBranchAddress("MesonTracks_eta_TRK_RES_Z0_DEAD", &MesonTracks_eta_TRK_RES_Z0_DEAD, &b_MesonTracks_eta_TRK_RES_Z0_DEAD);
   fChain->SetBranchAddress("MesonTracks_passTight_TRK_RES_Z0_DEAD", &MesonTracks_passTight_TRK_RES_Z0_DEAD, &b_MesonTracks_passTight_TRK_RES_Z0_DEAD);
   fChain->SetBranchAddress("MesonTracks_phi_TRK_RES_Z0_DEAD", &MesonTracks_phi_TRK_RES_Z0_DEAD, &b_MesonTracks_phi_TRK_RES_Z0_DEAD);
   fChain->SetBranchAddress("MesonTracks_pt_TRK_RES_Z0_DEAD", &MesonTracks_pt_TRK_RES_Z0_DEAD, &b_MesonTracks_pt_TRK_RES_Z0_DEAD);
   fChain->SetBranchAddress("MesonTracks_trackId_TRK_RES_Z0_DEAD", &MesonTracks_trackId_TRK_RES_Z0_DEAD, &b_MesonTracks_trackId_TRK_RES_Z0_DEAD);
   fChain->SetBranchAddress("MesonTracks_z0sinTheta_TRK_RES_Z0_DEAD", &MesonTracks_z0sinTheta_TRK_RES_Z0_DEAD, &b_MesonTracks_z0sinTheta_TRK_RES_Z0_DEAD);
   fChain->SetBranchAddress("MesonTracks_truthMatchProbability_TRK_RES_Z0_DEAD", &MesonTracks_truthMatchProbability_TRK_RES_Z0_DEAD, &b_MesonTracks_truthMatchProbability_TRK_RES_Z0_DEAD);
   fChain->SetBranchAddress("AntiKt6PV0TrackJets_pt_TRK_RES_Z0_DEAD", &AntiKt6PV0TrackJets_pt_TRK_RES_Z0_DEAD, &b_AntiKt6PV0TrackJets_pt_TRK_RES_Z0_DEAD);
   fChain->SetBranchAddress("AntiKt6PV0TrackJets_eta_TRK_RES_Z0_DEAD", &AntiKt6PV0TrackJets_eta_TRK_RES_Z0_DEAD, &b_AntiKt6PV0TrackJets_eta_TRK_RES_Z0_DEAD);
   fChain->SetBranchAddress("AntiKt6PV0TrackJets_phi_TRK_RES_Z0_DEAD", &AntiKt6PV0TrackJets_phi_TRK_RES_Z0_DEAD, &b_AntiKt6PV0TrackJets_phi_TRK_RES_Z0_DEAD);
   fChain->SetBranchAddress("AntiKt6PV0TrackJets_m_TRK_RES_Z0_DEAD", &AntiKt6PV0TrackJets_m_TRK_RES_Z0_DEAD, &b_AntiKt6PV0TrackJets_m_TRK_RES_Z0_DEAD);
   fChain->SetBranchAddress("AntiKt6PV0TrackJets_daughter__pt_TRK_RES_Z0_DEAD", &AntiKt6PV0TrackJets_daughter__pt_TRK_RES_Z0_DEAD, &b_AntiKt6PV0TrackJets_daughter__pt_TRK_RES_Z0_DEAD);
   fChain->SetBranchAddress("AntiKt6PV0TrackJets_daughter__eta_TRK_RES_Z0_DEAD", &AntiKt6PV0TrackJets_daughter__eta_TRK_RES_Z0_DEAD, &b_AntiKt6PV0TrackJets_daughter__eta_TRK_RES_Z0_DEAD);
   fChain->SetBranchAddress("AntiKt6PV0TrackJets_daughter__phi_TRK_RES_Z0_DEAD", &AntiKt6PV0TrackJets_daughter__phi_TRK_RES_Z0_DEAD, &b_AntiKt6PV0TrackJets_daughter__phi_TRK_RES_Z0_DEAD);
   fChain->SetBranchAddress("AntiKt6PV0TrackJets_daughter__trackId_TRK_RES_Z0_DEAD", &AntiKt6PV0TrackJets_daughter__trackId_TRK_RES_Z0_DEAD, &b_AntiKt6PV0TrackJets_daughter__trackId_TRK_RES_Z0_DEAD);
   fChain->SetBranchAddress("AntiKt8PV0TrackJets_pt_TRK_RES_Z0_DEAD", &AntiKt8PV0TrackJets_pt_TRK_RES_Z0_DEAD, &b_AntiKt8PV0TrackJets_pt_TRK_RES_Z0_DEAD);
   fChain->SetBranchAddress("AntiKt8PV0TrackJets_eta_TRK_RES_Z0_DEAD", &AntiKt8PV0TrackJets_eta_TRK_RES_Z0_DEAD, &b_AntiKt8PV0TrackJets_eta_TRK_RES_Z0_DEAD);
   fChain->SetBranchAddress("AntiKt8PV0TrackJets_phi_TRK_RES_Z0_DEAD", &AntiKt8PV0TrackJets_phi_TRK_RES_Z0_DEAD, &b_AntiKt8PV0TrackJets_phi_TRK_RES_Z0_DEAD);
   fChain->SetBranchAddress("AntiKt8PV0TrackJets_m_TRK_RES_Z0_DEAD", &AntiKt8PV0TrackJets_m_TRK_RES_Z0_DEAD, &b_AntiKt8PV0TrackJets_m_TRK_RES_Z0_DEAD);
   fChain->SetBranchAddress("AntiKt8PV0TrackJets_daughter__pt_TRK_RES_Z0_DEAD", &AntiKt8PV0TrackJets_daughter__pt_TRK_RES_Z0_DEAD, &b_AntiKt8PV0TrackJets_daughter__pt_TRK_RES_Z0_DEAD);
   fChain->SetBranchAddress("AntiKt8PV0TrackJets_daughter__eta_TRK_RES_Z0_DEAD", &AntiKt8PV0TrackJets_daughter__eta_TRK_RES_Z0_DEAD, &b_AntiKt8PV0TrackJets_daughter__eta_TRK_RES_Z0_DEAD);
   fChain->SetBranchAddress("AntiKt8PV0TrackJets_daughter__phi_TRK_RES_Z0_DEAD", &AntiKt8PV0TrackJets_daughter__phi_TRK_RES_Z0_DEAD, &b_AntiKt8PV0TrackJets_daughter__phi_TRK_RES_Z0_DEAD);
   fChain->SetBranchAddress("AntiKt8PV0TrackJets_daughter__trackId_TRK_RES_Z0_DEAD", &AntiKt8PV0TrackJets_daughter__trackId_TRK_RES_Z0_DEAD, &b_AntiKt8PV0TrackJets_daughter__trackId_TRK_RES_Z0_DEAD);
   fChain->SetBranchAddress("AntiKt10PV0TrackJets_pt_TRK_RES_Z0_DEAD", &AntiKt10PV0TrackJets_pt_TRK_RES_Z0_DEAD, &b_AntiKt10PV0TrackJets_pt_TRK_RES_Z0_DEAD);
   fChain->SetBranchAddress("AntiKt10PV0TrackJets_eta_TRK_RES_Z0_DEAD", &AntiKt10PV0TrackJets_eta_TRK_RES_Z0_DEAD, &b_AntiKt10PV0TrackJets_eta_TRK_RES_Z0_DEAD);
   fChain->SetBranchAddress("AntiKt10PV0TrackJets_phi_TRK_RES_Z0_DEAD", &AntiKt10PV0TrackJets_phi_TRK_RES_Z0_DEAD, &b_AntiKt10PV0TrackJets_phi_TRK_RES_Z0_DEAD);
   fChain->SetBranchAddress("AntiKt10PV0TrackJets_m_TRK_RES_Z0_DEAD", &AntiKt10PV0TrackJets_m_TRK_RES_Z0_DEAD, &b_AntiKt10PV0TrackJets_m_TRK_RES_Z0_DEAD);
   fChain->SetBranchAddress("AntiKt10PV0TrackJets_daughter__pt_TRK_RES_Z0_DEAD", &AntiKt10PV0TrackJets_daughter__pt_TRK_RES_Z0_DEAD, &b_AntiKt10PV0TrackJets_daughter__pt_TRK_RES_Z0_DEAD);
   fChain->SetBranchAddress("AntiKt10PV0TrackJets_daughter__eta_TRK_RES_Z0_DEAD", &AntiKt10PV0TrackJets_daughter__eta_TRK_RES_Z0_DEAD, &b_AntiKt10PV0TrackJets_daughter__eta_TRK_RES_Z0_DEAD);
   fChain->SetBranchAddress("AntiKt10PV0TrackJets_daughter__phi_TRK_RES_Z0_DEAD", &AntiKt10PV0TrackJets_daughter__phi_TRK_RES_Z0_DEAD, &b_AntiKt10PV0TrackJets_daughter__phi_TRK_RES_Z0_DEAD);
   fChain->SetBranchAddress("AntiKt10PV0TrackJets_daughter__trackId_TRK_RES_Z0_DEAD", &AntiKt10PV0TrackJets_daughter__trackId_TRK_RES_Z0_DEAD, &b_AntiKt10PV0TrackJets_daughter__trackId_TRK_RES_Z0_DEAD);
   fChain->SetBranchAddress("AntiKtVR30Rmax4Rmin02TrackJets_pt_TRK_RES_Z0_DEAD", &AntiKtVR30Rmax4Rmin02TrackJets_pt_TRK_RES_Z0_DEAD, &b_AntiKtVR30Rmax4Rmin02TrackJets_pt_TRK_RES_Z0_DEAD);
   fChain->SetBranchAddress("AntiKtVR30Rmax4Rmin02TrackJets_eta_TRK_RES_Z0_DEAD", &AntiKtVR30Rmax4Rmin02TrackJets_eta_TRK_RES_Z0_DEAD, &b_AntiKtVR30Rmax4Rmin02TrackJets_eta_TRK_RES_Z0_DEAD);
   fChain->SetBranchAddress("AntiKtVR30Rmax4Rmin02TrackJets_phi_TRK_RES_Z0_DEAD", &AntiKtVR30Rmax4Rmin02TrackJets_phi_TRK_RES_Z0_DEAD, &b_AntiKtVR30Rmax4Rmin02TrackJets_phi_TRK_RES_Z0_DEAD);
   fChain->SetBranchAddress("AntiKtVR30Rmax4Rmin02TrackJets_m_TRK_RES_Z0_DEAD", &AntiKtVR30Rmax4Rmin02TrackJets_m_TRK_RES_Z0_DEAD, &b_AntiKtVR30Rmax4Rmin02TrackJets_m_TRK_RES_Z0_DEAD);
   fChain->SetBranchAddress("AntiKtVR30Rmax4Rmin02TrackJets_daughter__pt_TRK_RES_Z0_DEAD", &AntiKtVR30Rmax4Rmin02TrackJets_daughter__pt_TRK_RES_Z0_DEAD, &b_AntiKtVR30Rmax4Rmin02TrackJets_daughter__pt_TRK_RES_Z0_DEAD);
   fChain->SetBranchAddress("AntiKtVR30Rmax4Rmin02TrackJets_daughter__eta_TRK_RES_Z0_DEAD", &AntiKtVR30Rmax4Rmin02TrackJets_daughter__eta_TRK_RES_Z0_DEAD, &b_AntiKtVR30Rmax4Rmin02TrackJets_daughter__eta_TRK_RES_Z0_DEAD);
   fChain->SetBranchAddress("AntiKtVR30Rmax4Rmin02TrackJets_daughter__phi_TRK_RES_Z0_DEAD", &AntiKtVR30Rmax4Rmin02TrackJets_daughter__phi_TRK_RES_Z0_DEAD, &b_AntiKtVR30Rmax4Rmin02TrackJets_daughter__phi_TRK_RES_Z0_DEAD);
   fChain->SetBranchAddress("AntiKtVR30Rmax4Rmin02TrackJets_daughter__trackId_TRK_RES_Z0_DEAD", &AntiKtVR30Rmax4Rmin02TrackJets_daughter__trackId_TRK_RES_Z0_DEAD, &b_AntiKtVR30Rmax4Rmin02TrackJets_daughter__trackId_TRK_RES_Z0_DEAD);
   fChain->SetBranchAddress("DMesons_costhetastar_TRK_RES_Z0_MEAS", &DMesons_costhetastar_TRK_RES_Z0_MEAS, &b_DMesons_costhetastar_TRK_RES_Z0_MEAS);
   fChain->SetBranchAddress("DMesons_D0Index_TRK_RES_Z0_MEAS", &DMesons_D0Index_TRK_RES_Z0_MEAS, &b_DMesons_D0Index_TRK_RES_Z0_MEAS);
   fChain->SetBranchAddress("DMesons_daughterInfo__eta_TRK_RES_Z0_MEAS", &DMesons_daughterInfo__eta_TRK_RES_Z0_MEAS, &b_DMesons_daughterInfo__eta_TRK_RES_Z0_MEAS);
   fChain->SetBranchAddress("DMesons_daughterInfo__passTight_TRK_RES_Z0_MEAS", &DMesons_daughterInfo__passTight_TRK_RES_Z0_MEAS, &b_DMesons_daughterInfo__passTight_TRK_RES_Z0_MEAS);
   fChain->SetBranchAddress("DMesons_daughterInfo__pdgId_TRK_RES_Z0_MEAS", &DMesons_daughterInfo__pdgId_TRK_RES_Z0_MEAS, &b_DMesons_daughterInfo__pdgId_TRK_RES_Z0_MEAS);
   fChain->SetBranchAddress("DMesons_daughterInfo__phi_TRK_RES_Z0_MEAS", &DMesons_daughterInfo__phi_TRK_RES_Z0_MEAS, &b_DMesons_daughterInfo__phi_TRK_RES_Z0_MEAS);
   fChain->SetBranchAddress("DMesons_daughterInfo__pt_TRK_RES_Z0_MEAS", &DMesons_daughterInfo__pt_TRK_RES_Z0_MEAS, &b_DMesons_daughterInfo__pt_TRK_RES_Z0_MEAS);
   fChain->SetBranchAddress("DMesons_daughterInfo__trackId_TRK_RES_Z0_MEAS", &DMesons_daughterInfo__trackId_TRK_RES_Z0_MEAS, &b_DMesons_daughterInfo__trackId_TRK_RES_Z0_MEAS);
   fChain->SetBranchAddress("DMesons_daughterInfo__truthBarcode_TRK_RES_Z0_MEAS", &DMesons_daughterInfo__truthBarcode_TRK_RES_Z0_MEAS, &b_DMesons_daughterInfo__truthBarcode_TRK_RES_Z0_MEAS);
   fChain->SetBranchAddress("DMesons_daughterInfo__truthDBarcode_TRK_RES_Z0_MEAS", &DMesons_daughterInfo__truthDBarcode_TRK_RES_Z0_MEAS, &b_DMesons_daughterInfo__truthDBarcode_TRK_RES_Z0_MEAS);
   fChain->SetBranchAddress("DMesons_daughterInfo__z0SinTheta_TRK_RES_Z0_MEAS", &DMesons_daughterInfo__z0SinTheta_TRK_RES_Z0_MEAS, &b_DMesons_daughterInfo__z0SinTheta_TRK_RES_Z0_MEAS);
   fChain->SetBranchAddress("DMesons_daughterInfo__z0SinThetaPV_TRK_RES_Z0_MEAS", &DMesons_daughterInfo__z0SinThetaPV_TRK_RES_Z0_MEAS, &b_DMesons_daughterInfo__z0SinThetaPV_TRK_RES_Z0_MEAS);
   fChain->SetBranchAddress("DMesons_decayType_TRK_RES_Z0_MEAS", &DMesons_decayType_TRK_RES_Z0_MEAS, &b_DMesons_decayType_TRK_RES_Z0_MEAS);
   fChain->SetBranchAddress("DMesons_DeltaMass_TRK_RES_Z0_MEAS", &DMesons_DeltaMass_TRK_RES_Z0_MEAS, &b_DMesons_DeltaMass_TRK_RES_Z0_MEAS);
   fChain->SetBranchAddress("DMesons_eta_TRK_RES_Z0_MEAS", &DMesons_eta_TRK_RES_Z0_MEAS, &b_DMesons_eta_TRK_RES_Z0_MEAS);
   fChain->SetBranchAddress("DMesons_fitOutput__Charge_TRK_RES_Z0_MEAS", &DMesons_fitOutput__Charge_TRK_RES_Z0_MEAS, &b_DMesons_fitOutput__Charge_TRK_RES_Z0_MEAS);
   fChain->SetBranchAddress("DMesons_fitOutput__Chi2_TRK_RES_Z0_MEAS", &DMesons_fitOutput__Chi2_TRK_RES_Z0_MEAS, &b_DMesons_fitOutput__Chi2_TRK_RES_Z0_MEAS);
   fChain->SetBranchAddress("DMesons_fitOutput__Impact_TRK_RES_Z0_MEAS", &DMesons_fitOutput__Impact_TRK_RES_Z0_MEAS, &b_DMesons_fitOutput__Impact_TRK_RES_Z0_MEAS);
   fChain->SetBranchAddress("DMesons_fitOutput__ImpactSignificance_TRK_RES_Z0_MEAS", &DMesons_fitOutput__ImpactSignificance_TRK_RES_Z0_MEAS, &b_DMesons_fitOutput__ImpactSignificance_TRK_RES_Z0_MEAS);
   fChain->SetBranchAddress("DMesons_fitOutput__ImpactZ0SinTheta_TRK_RES_Z0_MEAS", &DMesons_fitOutput__ImpactZ0SinTheta_TRK_RES_Z0_MEAS, &b_DMesons_fitOutput__ImpactZ0SinTheta_TRK_RES_Z0_MEAS);
   fChain->SetBranchAddress("DMesons_fitOutput__Lxy_TRK_RES_Z0_MEAS", &DMesons_fitOutput__Lxy_TRK_RES_Z0_MEAS, &b_DMesons_fitOutput__Lxy_TRK_RES_Z0_MEAS);
   fChain->SetBranchAddress("DMesons_fitOutput__VertexPosition_TRK_RES_Z0_MEAS", &DMesons_fitOutput__VertexPosition_TRK_RES_Z0_MEAS, &b_DMesons_fitOutput__VertexPosition_TRK_RES_Z0_MEAS);
   fChain->SetBranchAddress("DMesons_m_TRK_RES_Z0_MEAS", &DMesons_m_TRK_RES_Z0_MEAS, &b_DMesons_m_TRK_RES_Z0_MEAS);
   fChain->SetBranchAddress("DMesons_mKpi1_TRK_RES_Z0_MEAS", &DMesons_mKpi1_TRK_RES_Z0_MEAS, &b_DMesons_mKpi1_TRK_RES_Z0_MEAS);
   fChain->SetBranchAddress("DMesons_mKpi2_TRK_RES_Z0_MEAS", &DMesons_mKpi2_TRK_RES_Z0_MEAS, &b_DMesons_mKpi2_TRK_RES_Z0_MEAS);
   fChain->SetBranchAddress("DMesons_mPhi1_TRK_RES_Z0_MEAS", &DMesons_mPhi1_TRK_RES_Z0_MEAS, &b_DMesons_mPhi1_TRK_RES_Z0_MEAS);
   fChain->SetBranchAddress("DMesons_mPhi2_TRK_RES_Z0_MEAS", &DMesons_mPhi2_TRK_RES_Z0_MEAS, &b_DMesons_mPhi2_TRK_RES_Z0_MEAS);
   fChain->SetBranchAddress("DMesons_pdgId_TRK_RES_Z0_MEAS", &DMesons_pdgId_TRK_RES_Z0_MEAS, &b_DMesons_pdgId_TRK_RES_Z0_MEAS);
   fChain->SetBranchAddress("DMesons_phi_TRK_RES_Z0_MEAS", &DMesons_phi_TRK_RES_Z0_MEAS, &b_DMesons_phi_TRK_RES_Z0_MEAS);
   fChain->SetBranchAddress("DMesons_pt_TRK_RES_Z0_MEAS", &DMesons_pt_TRK_RES_Z0_MEAS, &b_DMesons_pt_TRK_RES_Z0_MEAS);
   fChain->SetBranchAddress("DMesons_ptcone40_TRK_RES_Z0_MEAS", &DMesons_ptcone40_TRK_RES_Z0_MEAS, &b_DMesons_ptcone40_TRK_RES_Z0_MEAS);
   fChain->SetBranchAddress("DMesons_SlowPionD0_TRK_RES_Z0_MEAS", &DMesons_SlowPionD0_TRK_RES_Z0_MEAS, &b_DMesons_SlowPionD0_TRK_RES_Z0_MEAS);
   fChain->SetBranchAddress("DMesons_SlowPionZ0SinTheta_TRK_RES_Z0_MEAS", &DMesons_SlowPionZ0SinTheta_TRK_RES_Z0_MEAS, &b_DMesons_SlowPionZ0SinTheta_TRK_RES_Z0_MEAS);
   fChain->SetBranchAddress("DMesons_truthBarcode_TRK_RES_Z0_MEAS", &DMesons_truthBarcode_TRK_RES_Z0_MEAS, &b_DMesons_truthBarcode_TRK_RES_Z0_MEAS);
   fChain->SetBranchAddress("MesonTracks_d0sig_TRK_RES_Z0_MEAS", &MesonTracks_d0sig_TRK_RES_Z0_MEAS, &b_MesonTracks_d0sig_TRK_RES_Z0_MEAS);
   fChain->SetBranchAddress("MesonTracks_d0sigPV_TRK_RES_Z0_MEAS", &MesonTracks_d0sigPV_TRK_RES_Z0_MEAS, &b_MesonTracks_d0sigPV_TRK_RES_Z0_MEAS);
   fChain->SetBranchAddress("MesonTracks_eta_TRK_RES_Z0_MEAS", &MesonTracks_eta_TRK_RES_Z0_MEAS, &b_MesonTracks_eta_TRK_RES_Z0_MEAS);
   fChain->SetBranchAddress("MesonTracks_passTight_TRK_RES_Z0_MEAS", &MesonTracks_passTight_TRK_RES_Z0_MEAS, &b_MesonTracks_passTight_TRK_RES_Z0_MEAS);
   fChain->SetBranchAddress("MesonTracks_phi_TRK_RES_Z0_MEAS", &MesonTracks_phi_TRK_RES_Z0_MEAS, &b_MesonTracks_phi_TRK_RES_Z0_MEAS);
   fChain->SetBranchAddress("MesonTracks_pt_TRK_RES_Z0_MEAS", &MesonTracks_pt_TRK_RES_Z0_MEAS, &b_MesonTracks_pt_TRK_RES_Z0_MEAS);
   fChain->SetBranchAddress("MesonTracks_trackId_TRK_RES_Z0_MEAS", &MesonTracks_trackId_TRK_RES_Z0_MEAS, &b_MesonTracks_trackId_TRK_RES_Z0_MEAS);
   fChain->SetBranchAddress("MesonTracks_z0sinTheta_TRK_RES_Z0_MEAS", &MesonTracks_z0sinTheta_TRK_RES_Z0_MEAS, &b_MesonTracks_z0sinTheta_TRK_RES_Z0_MEAS);
   fChain->SetBranchAddress("MesonTracks_truthMatchProbability_TRK_RES_Z0_MEAS", &MesonTracks_truthMatchProbability_TRK_RES_Z0_MEAS, &b_MesonTracks_truthMatchProbability_TRK_RES_Z0_MEAS);
   fChain->SetBranchAddress("AntiKt6PV0TrackJets_pt_TRK_RES_Z0_MEAS", &AntiKt6PV0TrackJets_pt_TRK_RES_Z0_MEAS, &b_AntiKt6PV0TrackJets_pt_TRK_RES_Z0_MEAS);
   fChain->SetBranchAddress("AntiKt6PV0TrackJets_eta_TRK_RES_Z0_MEAS", &AntiKt6PV0TrackJets_eta_TRK_RES_Z0_MEAS, &b_AntiKt6PV0TrackJets_eta_TRK_RES_Z0_MEAS);
   fChain->SetBranchAddress("AntiKt6PV0TrackJets_phi_TRK_RES_Z0_MEAS", &AntiKt6PV0TrackJets_phi_TRK_RES_Z0_MEAS, &b_AntiKt6PV0TrackJets_phi_TRK_RES_Z0_MEAS);
   fChain->SetBranchAddress("AntiKt6PV0TrackJets_m_TRK_RES_Z0_MEAS", &AntiKt6PV0TrackJets_m_TRK_RES_Z0_MEAS, &b_AntiKt6PV0TrackJets_m_TRK_RES_Z0_MEAS);
   fChain->SetBranchAddress("AntiKt6PV0TrackJets_daughter__pt_TRK_RES_Z0_MEAS", &AntiKt6PV0TrackJets_daughter__pt_TRK_RES_Z0_MEAS, &b_AntiKt6PV0TrackJets_daughter__pt_TRK_RES_Z0_MEAS);
   fChain->SetBranchAddress("AntiKt6PV0TrackJets_daughter__eta_TRK_RES_Z0_MEAS", &AntiKt6PV0TrackJets_daughter__eta_TRK_RES_Z0_MEAS, &b_AntiKt6PV0TrackJets_daughter__eta_TRK_RES_Z0_MEAS);
   fChain->SetBranchAddress("AntiKt6PV0TrackJets_daughter__phi_TRK_RES_Z0_MEAS", &AntiKt6PV0TrackJets_daughter__phi_TRK_RES_Z0_MEAS, &b_AntiKt6PV0TrackJets_daughter__phi_TRK_RES_Z0_MEAS);
   fChain->SetBranchAddress("AntiKt6PV0TrackJets_daughter__trackId_TRK_RES_Z0_MEAS", &AntiKt6PV0TrackJets_daughter__trackId_TRK_RES_Z0_MEAS, &b_AntiKt6PV0TrackJets_daughter__trackId_TRK_RES_Z0_MEAS);
   fChain->SetBranchAddress("AntiKt8PV0TrackJets_pt_TRK_RES_Z0_MEAS", &AntiKt8PV0TrackJets_pt_TRK_RES_Z0_MEAS, &b_AntiKt8PV0TrackJets_pt_TRK_RES_Z0_MEAS);
   fChain->SetBranchAddress("AntiKt8PV0TrackJets_eta_TRK_RES_Z0_MEAS", &AntiKt8PV0TrackJets_eta_TRK_RES_Z0_MEAS, &b_AntiKt8PV0TrackJets_eta_TRK_RES_Z0_MEAS);
   fChain->SetBranchAddress("AntiKt8PV0TrackJets_phi_TRK_RES_Z0_MEAS", &AntiKt8PV0TrackJets_phi_TRK_RES_Z0_MEAS, &b_AntiKt8PV0TrackJets_phi_TRK_RES_Z0_MEAS);
   fChain->SetBranchAddress("AntiKt8PV0TrackJets_m_TRK_RES_Z0_MEAS", &AntiKt8PV0TrackJets_m_TRK_RES_Z0_MEAS, &b_AntiKt8PV0TrackJets_m_TRK_RES_Z0_MEAS);
   fChain->SetBranchAddress("AntiKt8PV0TrackJets_daughter__pt_TRK_RES_Z0_MEAS", &AntiKt8PV0TrackJets_daughter__pt_TRK_RES_Z0_MEAS, &b_AntiKt8PV0TrackJets_daughter__pt_TRK_RES_Z0_MEAS);
   fChain->SetBranchAddress("AntiKt8PV0TrackJets_daughter__eta_TRK_RES_Z0_MEAS", &AntiKt8PV0TrackJets_daughter__eta_TRK_RES_Z0_MEAS, &b_AntiKt8PV0TrackJets_daughter__eta_TRK_RES_Z0_MEAS);
   fChain->SetBranchAddress("AntiKt8PV0TrackJets_daughter__phi_TRK_RES_Z0_MEAS", &AntiKt8PV0TrackJets_daughter__phi_TRK_RES_Z0_MEAS, &b_AntiKt8PV0TrackJets_daughter__phi_TRK_RES_Z0_MEAS);
   fChain->SetBranchAddress("AntiKt8PV0TrackJets_daughter__trackId_TRK_RES_Z0_MEAS", &AntiKt8PV0TrackJets_daughter__trackId_TRK_RES_Z0_MEAS, &b_AntiKt8PV0TrackJets_daughter__trackId_TRK_RES_Z0_MEAS);
   fChain->SetBranchAddress("AntiKt10PV0TrackJets_pt_TRK_RES_Z0_MEAS", &AntiKt10PV0TrackJets_pt_TRK_RES_Z0_MEAS, &b_AntiKt10PV0TrackJets_pt_TRK_RES_Z0_MEAS);
   fChain->SetBranchAddress("AntiKt10PV0TrackJets_eta_TRK_RES_Z0_MEAS", &AntiKt10PV0TrackJets_eta_TRK_RES_Z0_MEAS, &b_AntiKt10PV0TrackJets_eta_TRK_RES_Z0_MEAS);
   fChain->SetBranchAddress("AntiKt10PV0TrackJets_phi_TRK_RES_Z0_MEAS", &AntiKt10PV0TrackJets_phi_TRK_RES_Z0_MEAS, &b_AntiKt10PV0TrackJets_phi_TRK_RES_Z0_MEAS);
   fChain->SetBranchAddress("AntiKt10PV0TrackJets_m_TRK_RES_Z0_MEAS", &AntiKt10PV0TrackJets_m_TRK_RES_Z0_MEAS, &b_AntiKt10PV0TrackJets_m_TRK_RES_Z0_MEAS);
   fChain->SetBranchAddress("AntiKt10PV0TrackJets_daughter__pt_TRK_RES_Z0_MEAS", &AntiKt10PV0TrackJets_daughter__pt_TRK_RES_Z0_MEAS, &b_AntiKt10PV0TrackJets_daughter__pt_TRK_RES_Z0_MEAS);
   fChain->SetBranchAddress("AntiKt10PV0TrackJets_daughter__eta_TRK_RES_Z0_MEAS", &AntiKt10PV0TrackJets_daughter__eta_TRK_RES_Z0_MEAS, &b_AntiKt10PV0TrackJets_daughter__eta_TRK_RES_Z0_MEAS);
   fChain->SetBranchAddress("AntiKt10PV0TrackJets_daughter__phi_TRK_RES_Z0_MEAS", &AntiKt10PV0TrackJets_daughter__phi_TRK_RES_Z0_MEAS, &b_AntiKt10PV0TrackJets_daughter__phi_TRK_RES_Z0_MEAS);
   fChain->SetBranchAddress("AntiKt10PV0TrackJets_daughter__trackId_TRK_RES_Z0_MEAS", &AntiKt10PV0TrackJets_daughter__trackId_TRK_RES_Z0_MEAS, &b_AntiKt10PV0TrackJets_daughter__trackId_TRK_RES_Z0_MEAS);
   fChain->SetBranchAddress("AntiKtVR30Rmax4Rmin02TrackJets_pt_TRK_RES_Z0_MEAS", &AntiKtVR30Rmax4Rmin02TrackJets_pt_TRK_RES_Z0_MEAS, &b_AntiKtVR30Rmax4Rmin02TrackJets_pt_TRK_RES_Z0_MEAS);
   fChain->SetBranchAddress("AntiKtVR30Rmax4Rmin02TrackJets_eta_TRK_RES_Z0_MEAS", &AntiKtVR30Rmax4Rmin02TrackJets_eta_TRK_RES_Z0_MEAS, &b_AntiKtVR30Rmax4Rmin02TrackJets_eta_TRK_RES_Z0_MEAS);
   fChain->SetBranchAddress("AntiKtVR30Rmax4Rmin02TrackJets_phi_TRK_RES_Z0_MEAS", &AntiKtVR30Rmax4Rmin02TrackJets_phi_TRK_RES_Z0_MEAS, &b_AntiKtVR30Rmax4Rmin02TrackJets_phi_TRK_RES_Z0_MEAS);
   fChain->SetBranchAddress("AntiKtVR30Rmax4Rmin02TrackJets_m_TRK_RES_Z0_MEAS", &AntiKtVR30Rmax4Rmin02TrackJets_m_TRK_RES_Z0_MEAS, &b_AntiKtVR30Rmax4Rmin02TrackJets_m_TRK_RES_Z0_MEAS);
   fChain->SetBranchAddress("AntiKtVR30Rmax4Rmin02TrackJets_daughter__pt_TRK_RES_Z0_MEAS", &AntiKtVR30Rmax4Rmin02TrackJets_daughter__pt_TRK_RES_Z0_MEAS, &b_AntiKtVR30Rmax4Rmin02TrackJets_daughter__pt_TRK_RES_Z0_MEAS);
   fChain->SetBranchAddress("AntiKtVR30Rmax4Rmin02TrackJets_daughter__eta_TRK_RES_Z0_MEAS", &AntiKtVR30Rmax4Rmin02TrackJets_daughter__eta_TRK_RES_Z0_MEAS, &b_AntiKtVR30Rmax4Rmin02TrackJets_daughter__eta_TRK_RES_Z0_MEAS);
   fChain->SetBranchAddress("AntiKtVR30Rmax4Rmin02TrackJets_daughter__phi_TRK_RES_Z0_MEAS", &AntiKtVR30Rmax4Rmin02TrackJets_daughter__phi_TRK_RES_Z0_MEAS, &b_AntiKtVR30Rmax4Rmin02TrackJets_daughter__phi_TRK_RES_Z0_MEAS);
   fChain->SetBranchAddress("AntiKtVR30Rmax4Rmin02TrackJets_daughter__trackId_TRK_RES_Z0_MEAS", &AntiKtVR30Rmax4Rmin02TrackJets_daughter__trackId_TRK_RES_Z0_MEAS, &b_AntiKtVR30Rmax4Rmin02TrackJets_daughter__trackId_TRK_RES_Z0_MEAS);
   Notify();
}

Bool_t CharmAnalysis::Notify()
{
   // The Notify() function is called when a new file is opened. This
   // can be either for a new TTree in a TChain or when when a new TTree
   // is started when using PROOF. It is normally not necessary to make changes
   // to the generated code, but the routine can be extended by the
   // user if needed. The return value is currently not used.

   return kTRUE;
}

void CharmAnalysis::Show(Long64_t entry)
{
// Print contents of entry.
// If entry is not specified, print current entry
   if (!fChain) return;
   fChain->Show(entry);
}
Int_t CharmAnalysis::Cut(Long64_t entry)
{
// This function may be called from Loop.
// returns  1 if entry is accepted.
// returns -1 otherwise.
   return 1;
}
#endif // #ifdef CharmAnalysis_cxx
