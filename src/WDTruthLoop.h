#ifndef WD_TRUTH_LOOP_H
#define WD_TRUTH_LOOP_H

#include "EventLoopBase.h"
#include "HelperFunctions.h"
#include "TruthInfo.h"

namespace Charm {
class WDTruthLoop : public EventLoopBase, public TruthInfo {
 public:
  WDTruthLoop(TString input_file, TString out_path,
              TString tree_name = "CharmAnalysis");

 private:
  virtual void connect_branches() override;

  virtual void initialize() override;

  virtual bool preselection() override;

  virtual int execute() override;

  void connect_output_branches();

  void fill_truth_branches(int truthIndex);

  float dR_min(std::vector<unsigned int> *selected, bool OS = false);

  void fill_histograms(std::string channel = "");

  void fill_D_histograms(std::string channel, unsigned int i);

  void fill_jet_histograms(std::string channel, unsigned int i);

  void fill_Dplus_histograms(std::string channel = "",
                             bool requireDecay = false);

  void fill_Dstar_histograms(std::string channel = "",
                             bool requireDecay = false);

  void fill_Dzero_histograms(std::string channel = "",
                             bool requireDecay = false);

  void fill_Ds_histograms(std::string channel = "", bool requireDecay = false);

  void fill_LambdaC_histograms(std::string channel = "",
                               bool requireDecay = false);

  void configure();

  // event info
  Float_t m_EventInfo_generatorWeight_NOSYS{};
  Float_t m_EventInfo_PileupWeight_NOSYS{};
  Float_t m_EventInfo_prodFracWeight_NOSYS{};

  // systematics
  sys_br<Float_t> m_sys_br_GEN{};
  sys_br<Float_t> m_sys_br_PROD_FRAC{};

  // fiducial met cuts
  bool m_do_fiducial_met{false};
  bool m_do_prod_fraction_rw{false};
  bool m_do_prod_fraction_rw_old{false};
  bool m_do_wjets_rw{false};
  bool m_track_jet_selection{false};
  bool m_split_flavor{false};
  bool m_reweight_sherpa{false};
  bool m_save_output_tree{false};

  // output tree
  Float_t m_out_truth_Dmeson_m{};
  Float_t m_out_truth_Dmeson_pt{};
  Float_t m_out_truth_Dmeson_eta{};
  Float_t m_out_truth_Dmeson_phi{};
  Int_t m_out_truth_Dmeson_pdgId{};
  Float_t m_out_truth_Dmeson_track_jet_6_pt{};
  Float_t m_out_truth_Dmeson_track_jet_6_eta{};
  Float_t m_out_truth_Dmeson_track_jet_6_phi{};
  Float_t m_out_truth_Dmeson_track_jet_6_zt{};
  Float_t m_out_truth_Dmeson_track_jet_6_zl{};
  Float_t m_out_truth_Dmeson_track_jet_8_pt{};
  Float_t m_out_truth_Dmeson_track_jet_8_eta{};
  Float_t m_out_truth_Dmeson_track_jet_8_phi{};
  Float_t m_out_truth_Dmeson_track_jet_8_zt{};
  Float_t m_out_truth_Dmeson_track_jet_8_zl{};
  Float_t m_out_truth_Dmeson_track_jet_10_pt{};
  Float_t m_out_truth_Dmeson_track_jet_10_eta{};
  Float_t m_out_truth_Dmeson_track_jet_10_phi{};
  Float_t m_out_truth_Dmeson_track_jet_10_zt{};
  Float_t m_out_truth_Dmeson_track_jet_10_zl{};
};

}  // namespace Charm

#endif  // WD_TRUTH_LOOP_H
