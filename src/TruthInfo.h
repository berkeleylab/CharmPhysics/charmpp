#ifndef TRUTH_INFO_H
#define TRUTH_INFO_H

#include "DMesonRec.h"
#include "EventLoopBase.h"

namespace Charm {
// Forward declaration
class EventLoopBase;
class DMesonRec;

class TruthInfo {
 private:
  friend class DMesonRec;

 public:
  TruthInfo();

  int check_daughters(int index);

  bool fill_truth_D_histograms(EventLoopBase *elb, std::string channel,
                               unsigned int i);

  bool get_truth_wjets(std::string sample_type = "");

  bool get_truth_zjets();

  bool lep_pass_truth_fiducial(bool zplusd = false, bool met_cut = false);

  float diphoton_resonance_mass(std::vector<float> &diphoton_mass,
                                float target);

  float truth_daughter_mass(int index);

  std::string get_decay_mode(int index);

  std::string get_truth_category(EventLoopBase *elb, unsigned int Dplus_index,
                                 std::string decayString = "",
                                 bool leptonTruth = true, bool zplusd = false,
                                 bool simplified = false);

  std::vector<float> get_diphoton_masses(int index);

  void connect_truth_D_meson_branches(EventLoopBase *elb,
                                      bool do_truth_vjets = true,
                                      bool do_track_jets = false);

  void fill_stand_alone_truth(EventLoopBase *elb);

  void reweight_production_fractions(EventLoopBase *elb);

  void reweight_wjets(EventLoopBase *elb);

  // quantities
  float m_truth_D_eta;
  float m_truth_D_mass;
  float m_truth_D_pt;
  float m_truth_D_phi;
  float m_truth_abs_lep_eta;
  float m_truth_mt;
  int m_truth_lep1_charge;
  int m_truth_lep2_charge;
  int m_truth_lep1_pdgId;
  int m_truth_lep2_pdgId;
  std::string m_truth_channel;
  TLorentzVector m_truth_lep1;
  TLorentzVector m_truth_lep2;
  TLorentzVector m_truth_v;
  TLorentzVector m_truth_Z;

  // never gets set
  float m_truth_v_pt;

 private:
  int truthMatch(EventLoopBase *elb, int index, std::string decayString = "",
                 bool leptonTruth = true, bool zplusd = false);

 private:
  bool m_full_mis_match_info{};
  bool m_partial_mis_match_info{};
  bool m_dressed_containers{};

 protected:
  bool pass_track_truth(unsigned int index);
  bool pass_truth_fiducial(unsigned int index);
  float truth_daughter_maxTrackDR(unsigned int index);
  float truth_daughter_maxTrackEta(unsigned int index);
  float truth_daughter_minTrackDEta(unsigned int index);
  float truth_daughter_minTrackDPhi(unsigned int index);
  float phi_resonance_mass(unsigned int index);
  float truth_daughter_minTrackDR(unsigned int index);
  float truth_daughter_minTrackPt(unsigned int index);
  int getMatchedBarcode(int recoCode);
  int checkGeoMatch(DMesonRec *DMR, int index, int &truth_index);
  int nonZeroDaughters(DMesonRec *DMR, int index);
  void get_charmed_hadrons();
  void get_truth_D_mesons(int pdgID);

 protected:
  std::vector<unsigned int> m_truth_D_plus_mesons;
  std::vector<unsigned int> m_truth_D_s_mesons;
  std::vector<unsigned int> m_truth_D_star_mesons;
  std::vector<unsigned int> m_truth_D_star_pi0_mesons;
  std::vector<unsigned int> m_truth_charmed_hadrons;
  std::vector<unsigned int> m_truth_charmed_hadrons_from_B;

 public:
  std::vector<bool> *m_TruthParticles_Selected_isWlepT{};
  std::vector<Char_t> *m_TruthParticles_Selected_fromBdecay{};
  std::vector<Float_t> *m_TruthParticles_Selected_cosThetaStarT{};
  std::vector<Float_t> *m_TruthParticles_Selected_eta{};
  std::vector<Float_t> *m_TruthParticles_Selected_ImpactT{};
  std::vector<Float_t> *m_TruthParticles_Selected_LxyT{};
  std::vector<Float_t> *m_TruthParticles_Selected_m{};
  std::vector<Float_t> *m_TruthParticles_Selected_phi{};
  std::vector<Float_t> *m_TruthParticles_Selected_pt{};
  std::map<std::string, std::vector<float> *> m_AntiKtTruthChargedJets_eta{};
  std::map<std::string, std::vector<float> *> m_AntiKtTruthChargedJets_m{};
  std::map<std::string, std::vector<float> *> m_AntiKtTruthChargedJets_phi{};
  std::map<std::string, std::vector<float> *> m_AntiKtTruthChargedJets_pt{};
  std::vector<float> *m_TruthLeptons_e_dressed{};
  std::vector<float> *m_TruthLeptons_e{};
  std::vector<float> *m_TruthLeptons_eta_dressed{};
  std::vector<float> *m_TruthLeptons_eta{};
  std::vector<float> *m_TruthLeptons_m{};
  std::vector<float> *m_TruthLeptons_phi_dressed{};
  std::vector<float> *m_TruthLeptons_phi{};
  std::vector<float> *m_TruthLeptons_pt_dressed{};
  std::vector<float> *m_TruthLeptons_pt{};
  std::vector<Int_t> *m_TruthParticles_Selected_barcode{};
  std::vector<Int_t> *m_TruthParticles_Selected_decayMode{};
  std::vector<Int_t> *m_TruthParticles_Selected_pdgId{};
  std::vector<int> *m_TruthLeptons_barcode{};
  std::vector<int> *m_TruthLeptons_classifierParticleOrigin{};
  std::vector<int> *m_TruthLeptons_classifierParticleType{};
  std::vector<int> *m_TruthLeptons_pdgId{};
  std::vector<std::vector<Float_t>>
      *m_TruthParticles_Selected_daughterInfoT__eta{};
  std::vector<std::vector<Float_t>>
      *m_TruthParticles_Selected_daughterInfoT__phi{};
  std::vector<std::vector<Float_t>>
      *m_TruthParticles_Selected_daughterInfoT__pt{};
  std::vector<std::vector<Int_t>>
      *m_TruthParticles_Selected_daughterInfoT__barcode{};
  std::vector<std::vector<Int_t>>
      *m_TruthParticles_Selected_daughterInfoT__pdgId{};

 private:
  std::set<std::string> m_created_channels;
  bool m_branches_already_added{false};
};

}  // namespace Charm

#endif  // TRUTH_INFO_H
