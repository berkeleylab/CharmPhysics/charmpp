# champp

Histograming code for LBNL Charm project

## Contact Details

| Contact Person | Contact E-mail        |
|----------------|-----------------------|
| Miha Muskinja  | miha.muskinja@cern.ch |

## Setup

Clone the charmpp project:
```
git clone --recursive ssh://git@gitlab.cern.ch:7999/berkeleylab/CharmPhysics/charmpp.git
```

Copy the `example.sh` file and specify the following paths as you prefer:

```
export CHARMPP_BUILD_PATH=<SOME_PATH>

export CHARMPP_RUN_PATH=<SOME_OTHER_PATH>

export CHARMPP_NTUPLE_PATH=<PATH_TO_NTUPLES>

export CHARMPP_DATA_PATH=/global/cfs/cdirs/atlas/wcharm/charmpp_data
```

`CHARMPP_BUILD_PATH` is the directory where the charmpp project will be built, for example a path under `$HOME`. `CHARMPP_RUN_PATH` is the path where the output histograms from the charmpp execution will appear. The recommended path for `CHARMPP_RUN_PATH` is a user folder under `$CSCRATCH`.

Source the setup script from the root folder (i.e. where the charmpp project is cloned):

```
source your_script_name.sh
```

### Recommended ntuple paths

Standard MC16a/d/e MC samples:
```
export CHARMPP_NTUPLE_PATH=/global/cfs/cdirs/atlas/wcharm/v9/merged
```

Single Particle Gun samples:
```
CHARMPP_NTUPLE_PATH=/global/cfs/cdirs/atlas/wcharm/v9_spg/merged
```

## Environment Setup (recommended on Perlmutter)

To build the project on Perlmutter please follow the instructions below.

```
source perlmutter-image-setup.sh
```

This will automatically start a centos7 image and setup the correct LCG release. After this, also source the setup file with the `CHARMPP` paths. For the first time create a copy of the `example.sh` file and adjust the paths as you prefer. Build the project with `cmake`:

```
cd $CHARMPP_BUILD_PATH
cmake $CHARMPP_SOURCE_PATH
make -j8
```

## Running charmpp

If $CHARMPP_BUILD_PATH is set correctly, a binary called 'charmpp' should be available in your path. It can be run directly, e.g.:

```
charmpp $CHARMPP_NTUPLE_PATH/Sherpa_Wjets.MC16e.700339.0.root . WDCharm default default 0 100000
```

