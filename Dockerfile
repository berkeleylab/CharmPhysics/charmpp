# stolen from Tadej https://gitlab.cern.ch/tadej/TNAnalysis/-/tree/master
FROM ntadej/root-ubuntu:6.28.08-lhapdf

# Set TERM
ENV TERM=xterm-256color

# Install python3-pip and numpy
RUN apt-get update && apt-get install -y python3-pip && \
    pip3 install numpy

# Start the image with BASH by default.
CMD cat /etc/motd && /bin/bash
